import warnings
from cmath import nan

warnings.filterwarnings('ignore')  #suppress some warnings about future code changes

import datetime  # https://docs.python.org/3/library/datetime.html

import cartopy.crs as ccrs
import matplotlib
import matplotlib.pyplot as plt
import netCDF4 as nc
import numpy as np
import pandas as pd
import scipy.ndimage as si  # Another package for signal analysis
import scipy.signal as sg  # Package for signal analysis
import xarray as xr
from matplotlib import colors
from matplotlib.pyplot import cm
from mpl_toolkits.axes_grid1 import \
    make_axes_locatable  # For plotting interior colobars
from netCDF4 import Dataset
from scipy import stats  # Used for 2D binned statistics
from scipy.interpolate import \
    interp1d  # for converting cell to grid-centered coordinates

import gsw
import xrft

ccrs_proj = ccrs.PlateCarree()
import glob
import os
import sys
from os import path
from pathlib import Path

import matplotlib.patches as patches
import pyicon as pyic
import scipy
from matplotlib.legend import Legend
from matplotlib.legend_handler import HandlerLine2D, HandlerTuple
from scipy import fft as spfft
from scipy.fft import fft
from scipy.special import digamma
from scipy.stats import chi2, t
import gcm_filters

import smt_modules.tools as tools
#sys.path.append("../")
from smt_modules.icon_smt_levels import depthc, depthi, dzt, dzw

print('Load my smt functions')
##################################################################
def calc_coriolis_parameter(lat):
    # print('in rad/s')
    return 2*2*np.pi/86400*np.sin(lat*np.pi/180.)

def corfreq(lat):
    print('in rad/day')
    """
    The Coriolis frequency in rad / day at a given latitude.
    
    Args: 
        lat: Latitude in degree
        
    Returns:
        The Coriolis frequecy at latitude lat
    """    
    omega = 7.2921159e-5 # rad/s
    return 2*np.sin(np.abs(lat)*2*np.pi/360)*omega*(3600)*24


def tidefreq():
    """
    Eight major tidal frequencies in rad / day.  See Gill (1982) page 335.
    
    Args: 
        None
        
    Returns:
        An array of eight frequencies
    """    
    f = 24*2*np.pi/np.array([327.85,25.8194,24.0659,23.9344,12.6584,12.4206,12.0000,11.9673])
    names = ['Mr','O1','P1', 'K1', 'N2', 'M2', 'S2', 'K2']
    return f

def tidefreqnames():
    """
    Eight major tidal frequencies in rad / day.  See Gill (1982) page 335.
    
    Args: 
        None
        
    Returns:
        An array of eight frequencies
    """    
    f = 24*2*np.pi/np.array([327.85,25.8194,24.0659,23.9344,12.6584,12.4206,12.0000,11.9673])
    names = ['Mr','O1','P1', 'K1', 'N2', 'M2', 'S2', 'K2']
    return names

def convert_degree_to_meter(lat):
    r        = 6371000
    delta    = 2*np.pi*r/360*np.cos(lat/90* np.pi/2)
    return(delta)

def pyic_sample(data, lat_reg, lon_reg, savefig, fig_path):
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1,  asp=asp, plot_cb=True, fig_size_fac=2, projection=ccrs_proj)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1
    cmap = 'Spectral_r'
    hm1 = pyic.shade(data.lon, data.lat, data, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
        # ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        # ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        # hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
        # hm2[1].formatter.set_useMathText(True)
    fname = f'xx'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250,format='png', bbox_inches='tight')


def calc_satellite_spectra(ds, n, k, horizontal, path_dat):
    print('for satellite')
    for kk in np.arange(n):
        print(kk)
        if horizontal == True: Lat, Lon = get_lon_lat_horizontal_sst(k,n)
        else: Lat, Lon = get_lon_lat_vertical_sst(k, n)
        lat        = Lat[kk,:]
        lon        = Lon[kk,:]
        ds_section = ds.where((ds.lat > lat[0]) & (ds.lat < lat[1]) & (ds.lon > lon[0]) & (ds.lon < lon[1]), drop=True)
        ds_lat     = int(ds_section.lat[0].data)
        if horizontal == True: 
            ds_section = ds_section.isel(lat=0)
            dm_sat     = np.abs((ds_section.lon[1] - ds_section.lon[0])) * delta[ds_lat]
            ds_size    = int(ds_section.lon.size /2) +1
        else: 
            ds_section = ds_section.isel(lon=0)
            dm_sat     = np.abs((ds_section.lat[1] - ds_section.lat[0])) * delta[ds_lat]
            ds_size    = int(ds_section.lat.size /2) +1
        
        dm_sat = dm_sat.data
        ###############################################################
        # satellite
        iii = 0
        # t = 79
        A_sat = np.zeros((1,ds_size))
        for ii in np.arange(1)[::1]:
            y  = ds_section.sst_night
            cv = y.data.compute()
            nans, x = nan_helper(cv) 
            cv[nans]= np.interp(x(nans), x(~nans), cv[~nans])
            f, S    = sg.periodogram(cv-np.nanmean(cv), fs=1/dm_sat)
            S             = S.squeeze()
            A_sat[iii,:]  = S
            iii          += 1
        f_sat = f
        A_sat_mean = np.mean(A_sat, axis=0) # average of spectra from realisations of different timesteps
        std_sat = np.std(A_sat, axis=0)
        np.save(f'{path_dat}data_sat_{Lat[kk,0]}_{Lon[kk,0]}', [A_sat, f_sat, dm_sat, A_sat_mean, std_sat])

def calc_satellite_spectra_SA(ds, n, k, horizontal, path_dat):
    print('for satellite')
    for kk in np.arange(n):
        print(kk)
        if horizontal == True: Lat, Lon = get_lon_lat_horizontal_sst_wave(k,n)
        else: Lat, Lon = get_lon_lat_vertical_sst(k, n)
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ds_section = ds.where((ds.lat > lat[0]) & (ds.lat < lat[1]) & (ds.lon > lon[0]) & (ds.lon < lon[1]), drop=True)
        ds_lat = int(ds_section.lat[0].data)
        if horizontal == True: 
            ds_section = ds_section.isel(lat=0)
            dm_sat = np.abs((ds_section.lon[1] - ds_section.lon[0])) * delta[np.abs(ds_lat)]
            ds_size = int(ds_section.lon.size /2) +1
        else: 
            ds_section = ds_section.isel(lon=0)
            dm_sat = np.abs((ds_section.lat[1] - ds_section.lat[0])) * delta[np.abs(ds_lat)]
            ds_size = int(ds_section.lat.size /2) +1
        
        dm_sat = dm_sat.data
        ###############################################################
        # satellite
        iii = 0
        A_sat = np.zeros((1,ds_size))
        # for ii in np.arange(1)[::1]:
        #     #t_sat = ds_section.time[ii].data
        y = ds_section.sst_night
        cv = y.data.compute() 
        nans, x= nan_helper(cv) 
        # check if all points are empty
        if np.all(np.isnan(cv)):
            # jump to next section
            print('all nan')
            continue
        cv[nans]= np.interp(x(nans), x(~nans), cv[~nans])
        f, S = sg.periodogram(cv-np.nanmean(cv), fs=1/dm_sat)
        S = S.squeeze()
        A_sat[iii,:] = S
        # iii += 1

        f_sat = f
        A_sat_mean = np.mean(A_sat, axis=0) # average of spectra from realisations of different timesteps
        std_sat = np.std(A_sat, axis=0)
        np.save(f'{path_dat}data_sat_{Lat[kk,0]}_{Lon[kk,0]}', [A_sat, f_sat, dm_sat, A_sat_mean, std_sat])


def calc_satellite_spectra_welch(ds, n, k, horizontal, path_dat):
    print('for satellite')
    for kk in np.arange(n):
        print(kk)
        if horizontal == True: Lat, Lon = get_lon_lat_horizontal_sst(k,n)
        else: Lat, Lon = get_lon_lat_vertical_sst(k, n)
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ds_section = ds.where((ds.lat > lat[0]) & (ds.lat < lat[1]) & (ds.lon > lon[0]) & (ds.lon < lon[1]), drop=True)
        ds_lat = int(ds_section.lat[0].data)
        if horizontal == True: 
            ds_section = ds_section.isel(lat=0)
            dm_sat = np.abs((ds_section.lon[1] - ds_section.lon[0])) * delta[ds_lat]
            ds_size = int(ds_section.lon.size /2) +1
        else: 
            ds_section = ds_section.isel(lon=0)
            dm_sat = np.abs((ds_section.lat[1] - ds_section.lat[0])) * delta[ds_lat]
            ds_size = int(ds_section.lat.size /2) +1
        
        dm_sat = dm_sat.data
        ###############################################################
        # satellite
        ds_size_sat = ds_size
        iii = 0
        t = 79
        P = 2
        A_sat = np.zeros((1,int(ds_size/P/2+1)))
        for ii in np.arange(1)[::1]:
            #t_sat = ds_section.time[ii].data
            y = ds_section.isel(time=t).sst_night
            cv = y.data.compute() 
            nans, x= nan_helper(cv) 
            cv[nans]= np.interp(x(nans), x(~nans), cv[~nans])
            #f, S = sg.periodogram(cv-np.nanmean(cv), fs=1/dm_sat)
            f, S = sg.welch(cv, fs=1/dm_sat, window='hanning', nperseg=int(ds_size/P), noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=- 1, average='mean')
            S = S.squeeze()
            A_sat[iii,:] = S
            iii += 1
        f_sat = f
        A_sat_mean = np.mean(A_sat, axis=0) # average of spectra from realisations of different timesteps
        std_sat = np.std(A_sat, axis=0)
        np.save(f'{path_dat}data_sat_{Lat[kk,0]}_{Lon[kk,0]}', [A_sat, f_sat, dm_sat, A_sat_mean, std_sat])

def calc_smt_spectra(smt, n, k, horizontal, path_dat):
    print('and for smt model')
    sname         = 'A'
    tgname        = 'SMT'
    gname         = 'smtwv_oce_2022_tgrid'
    path_tgrid    = f'/home/m/m300602/work/icon/grids/smtwv_oce_2022/'
    fname_tgrid   = f'{gname}.nc'
    # path = '/home/m/m300602/work/icon/grids/smtwv_oce_2022/smtwv_oce_2022_tgrid.nc'
    path_scratch  = f'/scratch/m/m300878/slices/'
    path_ckdtree  = path_scratch # where grid is stored
    
    #compute grid
    sgrid = xr.open_dataset(path_tgrid + fname_tgrid)
    grid  = sgrid.cell_area_p.compute()
    Clon  = grid.clon * 180/np.pi
    grid  = grid.assign_coords(clon=Clon)
    Clat  = grid.clat * 180/np.pi
    grid  = grid.assign_coords(clat=Clat)

    if horizontal == True: Lat, Lon = get_lon_lat_horizontal_sst(k, n)
    else: Lat, Lon = get_lon_lat_vertical_sst(k, n)
    for kk in np.arange(n):
        print(kk)
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ##############################################################
        ######## calculate nearest neighbour of section
        if horizontal == True: npoints = 250
        else: npoints = 850 #exp1 = 1700 #2000ssh
        
        if False: #Path(sec_path ).is_file():
            print ("section File exist")
            data = xr.open_dataset(sec_path)
            ickdtree = data.ickdtree_c.data
            lon_sec = data.lon_sec.data
            lat_sec = data.lat_sec.data
        else:
            print ("section file does not exist")
            if horizontal == True:
                latii = Lat[kk,:]
                dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lon[1],latii[0]], p2=[lon[0],latii[0]], npoints=npoints,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_ckdtree,
                          sname        = sname,
                          gname        = gname,
                          tgname       = tgname,
                          load_egrid   = False,
                          load_vgrid   = False,
                          )
            else:
                lonii = Lon[kk,:]
                dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lonii[0],lat[1]], p2=[lonii[0],lat[0]], npoints=npoints,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_ckdtree,
                          sname        = sname,
                          gname        = gname,
                          tgname       = tgname,
                          load_egrid   = False,
                          load_vgrid   = False,
                          )
            # check grid
            grid_sel = np.sqrt(grid.isel(cell=ickdtree))
            grid_sel = grid_sel.assign_coords({"lon_sec": ("cell", lon_sec)})
            grid_smt = grid_sel.assign_coords({"lat_sec": ("cell", lat_sec)})
            # section distance
            if horizontal == True:
                lat_min = int(grid_smt.lat_sec[0])
                d_smt_sec = (grid_smt.lon_sec[0] - grid_smt.lon_sec[1]).data *delta[lat_min]
            else:
                lat_min = int(grid_smt.lat_sec[0])
                d_smt_sec = (grid_smt.lat_sec[0] - grid_smt.lat_sec[1]).data *delta[lat_min]

            print('max',grid_smt.max().data, 'min', grid_smt.min().data, 'distance section', d_smt_sec, 'distance pyic', (dist_sec[1]-dist_sec[0]))
            if grid_smt.max().data - d_smt_sec > 0:
                print('grid oversampled')
                warnings.warn('grid oversampled')
                # raise ValueError('grid oversampled')


        # select data
        smt_sec = smt.isel(ncells=ickdtree)
        if horizontal == True:
            smt_sec = smt_sec.assign_coords({"clon": ("ncells", lon_sec)})
        else:
            smt_sec = smt_sec.assign_coords({"clat": ("ncells", lat_sec)})
        
        ds_size = int(npoints /2) +1
        iii = 0   
        A_smt = np.zeros((1,ds_size))
        for ii in np.arange(1)[::1]:
            y             = smt_sec#.isel(time=tsmt)
            cv            = y.data.compute()
            f_smt, S      = sg.periodogram(cv-np.mean(cv), fs=1/d_smt_sec)
            S             = S.squeeze()
            A_smt[iii,:]  = S
            iii          += 1

        A_smt_mean = np.mean(A_smt, axis=0)
        std_smt    = np.std(A_smt, axis=0)

        print(f'save averaged data in {path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}')
        np.save(f'{path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}', [A_smt, f_smt, d_smt_sec, A_smt_mean, std_smt]) #type: ignore



def calc_smt_spectra_SA(smt, n, k, horizontal, path_dat):
    print('and for smt model')
    sname         = 'A'
    tgname        = 'SMT'
    gname         = 'smtwv_oce_2022_tgrid'
    path_tgrid    = f'/home/m/m300602/work/icon/grids/smtwv_oce_2022/'
    fname_tgrid   = f'{gname}.nc'
    # path = '/home/m/m300602/work/icon/grids/smtwv_oce_2022/smtwv_oce_2022_tgrid.nc'
    path_scratch  = f'/scratch/m/m300878/slices/'
    path_ckdtree  = path_scratch # where grid is stored
    
    #compute grid
    sgrid = xr.open_dataset(path_tgrid + fname_tgrid)
    grid  = sgrid.cell_area_p.compute()
    Clon  = grid.clon * 180/np.pi
    grid  = grid.assign_coords(clon=Clon)
    Clat  = grid.clat * 180/np.pi
    grid  = grid.assign_coords(clat=Clat)

    if horizontal == True: Lat, Lon = get_lon_lat_horizontal_sst_wave(k, n)
    else: Lat, Lon = get_lon_lat_vertical_sst(k, n)
    for kk in np.arange(n):
        print(kk)
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ##############################################################
        ######## calculate nearest neighbour of section
        if horizontal == True: npoints = 1900
        else: npoints = 850 #exp1 = 1700 #2000ssh
        
        if False: #Path(sec_path ).is_file():
            print ("section File exist")
            data = xr.open_dataset(sec_path)
            ickdtree = data.ickdtree_c.data
            lon_sec = data.lon_sec.data
            lat_sec = data.lat_sec.data
        else:
            print ("section file does not exist")
            if horizontal == True:
                latii = Lat[kk,:]
                dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lon[1],latii[0]], p2=[lon[0],latii[0]], npoints=npoints,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_ckdtree,
                          sname        = sname,
                          gname        = gname,
                          tgname       = tgname,
                          load_egrid   = False,
                          load_vgrid   = False,
                          )
            else:
                lonii = Lon[kk,:]
                dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lonii[0],lat[1]], p2=[lonii[0],lat[0]], npoints=npoints,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_ckdtree,
                          sname        = sname,
                          gname        = gname,
                          tgname       = tgname,
                          load_egrid   = False,
                          load_vgrid   = False,
                          )
            # check grid
            grid_sel = np.sqrt(grid.isel(cell=ickdtree))
            grid_sel = grid_sel.assign_coords({"lon_sec": ("cell", lon_sec)})
            grid_smt = grid_sel.assign_coords({"lat_sec": ("cell", lat_sec)})
            # section distance
            if horizontal == True:
                lat_min = int(grid_smt.lat_sec[0])
                d_smt_sec = (grid_smt.lon_sec[0] - grid_smt.lon_sec[1]).data *delta[np.abs(lat_min)]
            else:
                lat_min = int(grid_smt.lat_sec[0])
                d_smt_sec = (grid_smt.lat_sec[0] - grid_smt.lat_sec[1]).data *delta[np.abs(lat_min)]

            print('max',grid_smt.max().data, 'min', grid_smt.min().data, 'distance section', d_smt_sec, 'distance pyic', (dist_sec[1]-dist_sec[0]))
            if grid_smt.max() - (dist_sec[1]-dist_sec[0]) > 0:
                print('grid oversampled')
                warnings.warn('grid oversampled')



        # select data
        smt_sec = smt.isel(ncells=ickdtree)
        if horizontal == True:
            smt_sec = smt_sec.assign_coords({"clon": ("ncells", lon_sec)})
        else:
            smt_sec = smt_sec.assign_coords({"clat": ("ncells", lat_sec)})
        
        ds_size = int(npoints /2) +1
        iii = 0   
        A_smt = np.zeros((1,ds_size))
        for ii in np.arange(1)[::1]:
            y             = smt_sec#.isel(time=tsmt)
            cv            = y.data.compute()
            f_smt, S      = sg.periodogram(cv-np.mean(cv), fs=1/d_smt_sec)
            S             = S.squeeze()
            A_smt[iii,:]  = S
            iii          += 1

        A_smt_mean = np.mean(A_smt, axis=0)
        std_smt    = np.std(A_smt, axis=0)

        print(f'save averaged data in {path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}')
        np.save(f'{path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}', [A_smt, f_smt, d_smt_sec, A_smt_mean, std_smt]) #type: ignore


def calc_smt_spectra_welch(smt, n, k, horizontal, path_dat):
    print('and for smt model')
    Lat, Lon = get_lon_lat_vertical_sst(k, n)
    for kk in np.arange(n):
        print(kk)
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ##############################################################
        ######## calculate nearest neighbour of section
        if horizontal == True: npoints = 2300
        else: npoints = 1700 #exp2 = 850 #exp1 = 1700 #2000ssh
        
        sname         = 'A'
        tgname        = 'SMT'
        gname         = 'OceanOnly_SubmesoNA_2500m_srtm30'
        path_tgrid    = f'/pool/data/ICON/oes/grids/OceanOnly/'
        fname_tgrid   = f'{gname}.nc'
        path_scratch  = f'/scratch/m/m300878/slices/'
        path_ckdtree  = path_scratch # where grid is stored
        if False: #Path(sec_path ).is_file():
            print ("section File exist")
            data = xr.open_dataset(sec_path)
            ickdtree = data.ickdtree_c.data
            lon_sec = data.lon_sec.data
            lat_sec = data.lat_sec.data
        else:
            print ("section file does not exist")
            if horizontal == True:
                latii = Lat[kk,:]
                dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lon[1],latii[0]], p2=[lon[0],latii[0]], npoints=npoints,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_ckdtree,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_egrid=False,
                          load_vgrid=False,
                          )
            else:
                lonii = Lon[kk,:]
                dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lonii[0],lat[1]], p2=[lonii[0],lat[0]], npoints=npoints,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_ckdtree,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_egrid=False,
                          load_vgrid=False,
                          )
            # check grid
            sgrid    = xr.open_dataset(path_tgrid + fname_tgrid)
            grid     = sgrid.cell_area_p.compute()
            Clon     = grid.clon * 180/np.pi
            grid     = grid.assign_coords(clon=Clon)
            Clat     = grid.clat * 180/np.pi
            grid     = grid.assign_coords(clat=Clat)
            grid     = np.sqrt(grid.isel(cell=ickdtree))
            grid     = grid.assign_coords({"lon_sec": ("cell", lon_sec)})
            grid_smt = grid.assign_coords({"lat_sec": ("cell", lat_sec)})
            # section distance
            if horizontal == True:
                lat_min = int(grid_smt.lat_sec[0])
                d_smt_sec = (grid_smt.lon_sec[0] - grid_smt.lon_sec[1]).data *delta[lat_min]
            else:
                lat_min = int(grid_smt.lat_sec[0])
                d_smt_sec = (grid_smt.lat_sec[0] - grid_smt.lat_sec[1]).data *delta[lat_min]
            d_smt_hi_res = grid_smt.min().data
            print('max',grid_smt.max().data, 'min', grid_smt.min().data, 'distance section', d_smt_sec)
        # select data
        smt_sec = smt.isel(ncells=ickdtree)
        if horizontal == True:
            smt_sec = smt_sec.assign_coords({"clon": ("ncells", lon_sec)})
        else:
            smt_sec = smt_sec.assign_coords({"clat": ("ncells", lat_sec)})
        
        dm_smt = d_smt_sec
        # smt
        tsmt = 71*12
        #N = 984
        #l = 11
        #ll = int(N/l) +1
        ds_size = int(npoints /2) +1
        iii = 0   
        P = 4
        A_smt = np.zeros((1,int(ds_size/P/2+1)))
        for ii in np.arange(1)[::1]:
            t_smt = smt_sec.time[tsmt].data
            y = smt_sec.isel(time=tsmt)
            cv = y.data.compute() 
            #f, S = sg.periodogram(cv-np.mean(cv), fs=1/dm_smt)
            f, S = sg.welch(cv, fs=1/dm_smt, window='hanning', nperseg=int(ds_size/P), noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=- 1, average='mean')
            S = S.squeeze()
            A_smt[iii,:] = S
            iii += 1
        f_smt = f
        A_smt_mean = np.mean(A_smt, axis=0)
        std_smt = np.std(A_smt, axis=0)
        print(f'save averaged data in {path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}')
        np.save(f'{path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}', [A_smt, f_smt, dm_smt, A_smt_mean, std_smt])


def calc_r2b8_spectra(r2b8, n, k, horizontal, path_dat):
    print('and for r2b8 model')
    if horizontal == True: Lat, Lon = get_lon_lat_horizontal_r2b8(k, n)
    else: Lat, Lon = get_lon_lat_vertical_sst_r2b8_exp2(k, n)
    for kk in np.arange(n):
        print(kk)
        data = r2b8
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ds_section = data.where((data.lat > lat[0]) & (data.lat < lat[1]) & (data.lon > lon[0]) & (data.lon < lon[1]), drop=True)
        ds_lat = int(ds_section.lat[0].data)
        if horizontal == True: 
            ds_section = ds_section.isel(lat=0)
            dm_r2b8 = np.abs((ds_section.lon[1] - ds_section.lon[0])) * delta[ds_lat]
            ds_size = int(ds_section.lon.size /2) +1
        else: 
            ds_section = ds_section.isel(lon=0)
            dm_r2b8 = np.abs((ds_section.lat[1] - ds_section.lat[0])) * delta[ds_lat]
            ds_size = int(ds_section.lat.size /2) +1
        
        dm_r2b8 = dm_r2b8.data
        ###############################################################
        # r2b8
        ds_size_r2b8 = ds_size
        iii = 0
        t = 79
        A_r2b8 = np.zeros((1,ds_size))
        for ii in np.arange(1)[::1]:
            y = ds_section#.isel(time=t)
            cv = y.data
            f, S = sg.periodogram(cv-np.nanmean(cv), fs=1/dm_r2b8)
            S = S.squeeze()
            A_r2b8[iii,:] = S
            iii += 1
        f_r2b8 = f
        A_r2b8_mean = np.mean(A_r2b8, axis=0) # average of spectra from realisations of different timesteps
        std_r2b8 = np.std(A_r2b8, axis=0)
        np.save(f'{path_dat}data_r2b8_{Lat[kk,0]}_{Lon[kk,0]}', [A_r2b8, f_r2b8, dm_r2b8, A_r2b8_mean, std_r2b8])


def calc_r2b8_spectra_SA(r2b8, n, k, horizontal, path_dat):
    print('and for r2b8 model')
    if horizontal == True: Lat, Lon = get_lon_lat_horizontal_r2b8_wave(k, n)
    else: Lat, Lon = get_lon_lat_vertical_sst_r2b8_exp2(k, n)
    for kk in np.arange(n):
        print(kk)
        data       = r2b8
        lat        = Lat[kk,:]
        lon        = Lon[kk,:]
        ds_section = data.where((data.lat > lat[0]) & (data.lat < lat[1]) & (data.lon > lon[0]) & (data.lon < lon[1]), drop=True)
        ds_lat     = int(ds_section.lat[0].data)
        if horizontal == True: 
            ds_section = ds_section.isel(lat=0)
            dm_r2b8 = np.abs((ds_section.lon[1] - ds_section.lon[0])) * delta[np.abs(ds_lat)]
            ds_size = int(ds_section.lon.size /2) +1
        else: 
            ds_section = ds_section.isel(lon=0)
            dm_r2b8 = np.abs((ds_section.lat[1] - ds_section.lat[0])) * delta[np.abs(ds_lat)]
            ds_size = int(ds_section.lat.size /2) +1
        
        dm_r2b8 = dm_r2b8.data
        ###############################################################
        # r2b8
        iii = 0
        A_r2b8 = np.zeros((1,ds_size))
        for ii in np.arange(1)[::1]:
            y = ds_section
            cv = y.data
            f, S = sg.periodogram(cv-np.nanmean(cv), fs=1/dm_r2b8)
            S = S.squeeze()
            A_r2b8[iii,:] = S
            iii += 1
        f_r2b8 = f
        A_r2b8_mean = np.mean(A_r2b8, axis=0) # average of spectra from realisations of different timesteps
        std_r2b8 = np.std(A_r2b8, axis=0)
        np.save(f'{path_dat}data_r2b8_{Lat[kk,0]}_{Lon[kk,0]}', [A_r2b8, f_r2b8, dm_r2b8, A_r2b8_mean, std_r2b8])


def calc_r2b8_spectra_welch(r2b8, n, k, horizontal, path_dat):
    print('and for r2b8 model')
    if horizontal == True: Lat, Lon = get_lon_lat_horizontal_r2b8(k, n)
    else: Lat, Lon = get_lon_lat_vertical_sst_r2b8(k, n)
    for kk in np.arange(n):
        print(kk)
        data = r2b8
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ds_section = data.where((data.lat > lat[0]) & (data.lat < lat[1]) & (data.lon > lon[0]) & (data.lon < lon[1]), drop=True)
        ds_lat = int(ds_section.lat[0].data)
        if horizontal == True: 
            ds_section = ds_section.isel(lat=0)
            dm_r2b8 = np.abs((ds_section.lon[1] - ds_section.lon[0])) * delta[ds_lat]
            ds_size = int(ds_section.lon.size /2) +1
        else: 
            ds_section = ds_section.isel(lon=0)
            dm_r2b8 = np.abs((ds_section.lat[1] - ds_section.lat[0])) * delta[ds_lat]
            ds_size = int(ds_section.lat.size /2) +1
        
        dm_r2b8 = dm_r2b8.data
        ###############################################################
        # r2b8
        ds_size_r2b8 = ds_size
        iii = 0
        t = 79
        P = 2
        A_r2b8 = np.zeros((1,ds_size))
        A_r2b8 = np.zeros((1,int(ds_size/P/2+1)))
        for ii in np.arange(1)[::1]:
            y = ds_section.isel(time=t)
            cv = y.data
            #f, S = sg.periodogram(cv-np.nanmean(cv), fs=1/dm_r2b8)
            f, S = sg.welch(cv, fs=1/dm_r2b8, window='hanning', nperseg=int(ds_size/P), noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=- 1, average='mean')
            S = S.squeeze()
            A_r2b8[iii,:] = S
            iii += 1
        f_r2b8 = f
        A_r2b8_mean = np.mean(A_r2b8, axis=0) # average of spectra from realisations of different timesteps
        std_r2b8 = np.std(A_r2b8, axis=0)
        np.save(f'{path_dat}data_r2b8_{Lat[kk,0]}_{Lon[kk,0]}', [A_r2b8, f_r2b8, dm_r2b8, A_r2b8_mean, std_r2b8])

def calc_mld_montegut(rho_mean, depthc):
    """ 
    applies Montegut 2004 Threshold Method
    if salinity can be negelcted it is equivalent to dT = 0.2 
    """

    rho_surf_mean = rho_mean.isel(depthc=2) #should be 10m
    diff = np.sqrt((rho_surf_mean - rho_mean)**2)
    
    d = diff.where(diff<=0.03)
    dd = np.sum(~np.isnan(d), axis=2)
    
    return(depthc[dd-1])   

def calc_mld_montegut_xr(rho_mean, depthc):
    """ 
    applies Montegut 2004 Threshold Method
    if salinity can be negelcted it is equivalent to dT = 0.2 
    avoid conversion to np
    no loops!
    """

    rho_surf_mean = rho_mean.isel(depthc=2) #should be 10m
    diff = xr.ufuncs.sqrt((rho_surf_mean - rho_mean)**2)
    
    d = diff.where(diff<=0.03) #  d = diff.where(diff<=0.03)
    dd = np.sum(~np.isnan(d), axis=rho_mean.ndim-1)

    def func(a):
        return(depthc[a-1])

    mldx = xr.apply_ufunc(func, dd)

    return(dd, ~np.isnan(d), mldx)  

def calc_mld_xr(rho_mean, depth, threshold=0.2):
    """ 
    Treshold modiefied to suite selected front MLD
    avoid conversion to np
    Montegut Threshold is 0.03 kg/m3
    no loops!
    """

    #if rho_mean has depthi as dimension
    if 'depthi' in rho_mean.dims:
        rho_surf_mean = rho_mean.isel(depthi=2) #should be 10m
    elif 'depthc' in rho_mean.dims:
        rho_surf_mean = rho_mean.isel(depthc=2)
    elif 'depth' in rho_mean.dims or 'depth' in rho_mean.coords:
        rho_surf_mean = rho_mean.isel(depth=2)
    else:
        raise ValueError('depth dimension not found')
    # rho_surf_mean = rho_mean.isel(depthc=2) #should be 10m data on cell centersq

    diff = np.sqrt((rho_surf_mean - rho_mean)**2)  # type: ignore
    d    = diff.where(diff<=threshold) 

    mld_int = np.sum(~np.isnan(d), axis=rho_mean.ndim-1) - 1

    def func(a):
        return(depth[a])

    mld = xr.apply_ufunc(func, mld_int)
    mld_mask = ~np.isnan(d)

    return(mld_int, mld_mask, mld)   

def calc_mld_xr_sign(rho_mean, depth, threshold=0.2):
    """ 
    Treshold modiefied to suite selected front MLD
    avoid conversion to np
    Montegut Threshold is 0.03 kg/m3
    no loops!
    """

    #if rho_mean has depthi as dimension
    if 'depthi' in rho_mean.dims:
        rho_surf_mean = rho_mean.isel(depthi=2) #should be 10m
    elif 'depthc' in rho_mean.dims:
        rho_surf_mean = rho_mean.isel(depthc=2)
    # rho_surf_mean = rho_mean.isel(depthc=2) #should be 10m data on cell centersq

    diff = np.sqrt((rho_surf_mean - rho_mean)**2)  # type: ignore
    d    = diff.where((diff<=threshold) & ((rho_surf_mean - rho_mean)<0)) #  d = diff.where(diff<=0.03)

    mld_int = np.sum(~np.isnan(d), axis=rho_mean.ndim-1) - 1

    def func(a):
        return(depth[a])

    mld = xr.apply_ufunc(func, mld_int)
    mld_mask = ~np.isnan(d)

    return(mld_int, mld_mask, mld)   


def calc_topography(ds, depthc):
    """ 
    calculates topography based on depthc
    """

    sum_idx = np.sum(~np.isnan(ds), axis=2)
    def func(a):
        return(depthc[a-1])
    depth = xr.apply_ufunc(func, sum_idx)
    return(depth)

# def calc_mld_montegut_xr2(rho_mean, depth):
#     """ 
#     applies Montegut 2004 Threshold Method
#     if salinity can be negelcted it is equivalent to dT = 0.2 
#     avoid conversion to np
#     no loops!
#     """

#     rho_surf_mean = rho_mean.isel(depth=2) #should be 10m #changed depth for SMT-NATL
#     diff          = np.sqrt((rho_surf_mean - rho_mean)**2)  # type: ignore 
    
#     d = diff.where(diff<=0.03)
#     dd = np.sum(~np.isnan(d), axis=rho_mean.ndim-1)

#     def func(a):
#         return(depth[a-1])

#     mldx = xr.apply_ufunc(func, dd)

#     return(dd, ~np.isnan(d), mldx)   

def calc_mld_montegut_xr3(rho_mean, depthc):
    """ 
    Treshold modiefied to suite selected front MLD
    avoid conversion to np
    no loops!
    """

    rho_surf_mean = rho_mean.isel(depthi=2) #should be 10m
    # rho_surf_mean = rho_mean.isel(depthc=2) #should be 10m
    # diff          = xr.ufuncs.sqrt((rho_surf_mean - rho_mean)**2)  # type: ignore    
    diff          = np.sqrt((rho_surf_mean - rho_mean)**2)  # type: ignore    
    d = diff.where(diff<=0.03) #  d = diff.where(diff<=0.03)
    # d = diff.where(diff<=0.03) #  d = diff.where(diff<=0.03)

    dd = np.sum(~np.isnan(d), axis=rho_mean.ndim-1)

    def func(a):
        return(depthc[a-1])

    mldx = xr.apply_ufunc(func, dd)

    return(dd, ~np.isnan(d), mldx)   

def calc_mld_via_wb(wbb, depthi, prozent = 0.3):
        print(f'use {prozent} of wb_max to diagonise mld')
        wbb        = wbb.where(wbb.depthi < depthi[80]) # cut off values close to bottom
        data       = wbb.fillna(-1000)
        idx_wb_max = data.argmax(dim='depthi')
        data       = wbb.fillna(1000)
        idx_wb_min = data.argmin(dim='depthi')
        wb_max     = wbb[idx_wb_max]

        # idx_wb_max = wbb.argmax(dim='depthi')
        # idx_wb_min = wbb.argmin(dim='depthi')
        # wb_max     = wbb[idx_wb_max]

        # create mask to exclude values above and below wb_max and wb_min respectively
        shape = idx_wb_max.shape
        array = np.zeros(shape=(depthi.size, shape[0], shape[1]))
        for d in np.arange(depthi.size):
            array[d,:,:] = d

        mask_up  = array - idx_wb_max.data
        mask_up  = mask_up >= 0
        mask_low = array - idx_wb_min.data
        mask_low = mask_low <= 0
        mask     = mask_up * mask_low
        wbb      = wbb.where(mask, 10000) # hard coded value to make argmin work
        wbb      = wbb.fillna(10000)
        wb_max   = wb_max.fillna(10000)
        idx      = np.abs(wbb-prozent*wb_max).argmin(dim='depthi')
        mld      = depthi[idx]

        # wbb.isel(lat=500).plot(vmin=-1e-7, vmax=1e-7)
        # plt.ylim(500,0)
        if False: 
            # a few seconds # trick is to apply same calc to whole dataset and then apply a where statement
            # the loop needs 8min in comparison
            for i in np.arange(shape[0]):
                for j in np.arange(shape[1]):
                    wbb[0:idx_wb_max[i,j].data,i,j] = np.nan
            # in a 1d problem 
            # wbb    = wbb.where(wbb.depthi > depthi[idx_wb_max]) # dont use values above wb_max
        return(mld)

def calc_richardsonnumber(lat, N2, M2):
    """calculates Richardssonumber with respect to latitude"""
    f = calc_coriolis_parameter(lat)
    f2 = f**2
    # M4 = xr.ufunc.power(M2,2) #np calc
    # M4 = xr.ufuncs.square(M2)  # type: ignore
    M4 = np.power(M2,2)
    
    return N2 * f2  / M4 

def nan_helper(y):
    """Helper to handle indices and logical indices of NaNs.

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        >>> # linear interpolation of NaNs
        >>> nans, x= nan_helper(y)
        >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
    """

    return np.isnan(y), lambda z: z.nonzero()[0]

#-------------------Plots----------------#

def plot_spatial_spectra_horizontal(A_smt_mean, std_smt, f_smt, A_sat_mean, std_sat, f_sat, k, figname, path_save_name, horizontal):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))
    #l=2.5

    # smt
    kernel_size = k
    kernel = np.ones(kernel_size) / kernel_size
    #smean_m_std = A_smt_mean - std_smt #np.abs(A_smt_mean - std_smt)
    smean_m_std = np.abs(A_smt_mean - std_smt)
    smean_m_std = np.convolve(smean_m_std, kernel, mode='same')
    #smean_p_std = A_smt_mean + std_smt#np.abs(A_smt_mean + std_smt)
    smean_p_std = np.abs(A_smt_mean + std_smt)
    smean_p_std = np.convolve(smean_p_std, kernel, mode='same')

    plt.loglog(f_smt, smean_p_std, color='royalblue', linestyle='--',alpha=0.5)
    plt.loglog(f_smt, A_smt_mean, label='smt_ssh_mean', color='royalblue', linewidth=2)
    plt.fill_between(f_smt, A_smt_mean, smean_p_std, color='royalblue', alpha=0.25)
    plt.loglog(f_smt, smean_m_std, linestyle='--',  color='royalblue', alpha=0.5)
    plt.fill_between(f_smt, A_smt_mean, smean_m_std, color='royalblue', alpha=0.25)

    # satellite
    kernel_size = k
    kernel = np.ones(kernel_size) / kernel_size
    mean_m_std = np.abs(A_sat_mean - std_sat)
    mean_m_std = np.convolve(mean_m_std, kernel, mode='same')
    mean_p_std = np.abs(A_sat_mean + std_sat)
    mean_p_std = np.convolve(mean_p_std, kernel, mode='same')

    plt.loglog(f_sat, mean_p_std, color='tomato', linestyle='--', alpha=0.5)
    plt.loglog(f_sat, A_sat_mean, label='sat_adt_mean', color='tomato', linewidth=2)
    plt.fill_between(f_sat, A_sat_mean, mean_p_std, color='wheat', alpha=0.5)
    plt.loglog(f_sat, mean_m_std, linestyle='--',  color='tomato', alpha=0.5)
    plt.fill_between(f_sat, A_sat_mean, mean_m_std, color='wheat', alpha=0.5)

    #####################
    fx = np.linspace(1e-4,5e-4,100)
    pp = 10000
    y2 = pp* 3e-9*np.power(fx, (-2))
    y3 = pp* 1e-13*np.power(fx, (-3))
    y5 = pp* 1e-7*np.power(fx, (-5/3))

    plt.loglog(fx,y3, label='slope -3')
    plt.loglog(fx,y5, label='slope -5/3')

    # slope satellite
    xdata = f_sat[8:]
    ydata = A_sat_mean[8:]
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="limegreen")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="limegreen", label=f'slope {index:3.3}') 


    # slope smt
    if horizontal == True: 
        xdata = f_smt[50::700]
        ydata = A_smt_mean[50::700]
    else:
        xdata = f_smt[32::600]
        ydata = A_smt_mean[32::600]

    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="gold")  
    plt.plot(fx, powerlaw(fx, 5*pp*amp, index), linestyle='dashed', color="gold", label=f'slope {index:3.3}') 

    # slope smt
    if horizontal == True:
        xdata = f_smt[8:48]
        ydata = A_smt_mean[8:48]
    else:
        xdata = f_smt[6:30]
        ydata = A_smt_mean[6:30]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index), linewidth=4, linestyle='--', color="aqua")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linewidth=2, linestyle='--', color="aqua", label=f'slope {index:3.3}') 

    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(8e-4, 5e4)
    #ax.autoscale(enable=True, tight=True)
    #ax.set_xlim(1e-6, 1e10)
    fig.tight_layout()

    plt.legend(fontsize=15, loc='lower left')
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    plt.title(f'Averaged Spectral Estimates of SSH {figname}',fontsize=15)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight')

# def plot_spatial_spectra_sst(A_smt_mean, std_smt, f_smt, A_sat_mean, std_sat, f_sat, k, figname, path_save_name, horizontal):
#     powerlaw = lambda x, amp, index: amp * (x**index)
#     fig, ax = plt.subplots(1, 1, figsize=(12, 6))

#     plt.loglog(f_smt, A_smt_mean, label='smt_sst_mean', color='royalblue', linewidth=2)
#     plt.loglog(f_sat, A_sat_mean, label='sat_sst_mean', color='tomato', linewidth=2)

#     add_slopes(f_sat, A_sat_mean, f_smt, A_smt_mean, horizontal)

#     x = f_smt
#     def forward(x):
#         return 1 / x / 1000
#     def inverse(x):
#         return 1 / x 
#     secax = ax.secondary_xaxis('top', functions=(forward, inverse))
#     secax.set_xlabel('wavelength in [km]', fontsize=15)

#     ax.autoscale(enable=True, tight=True)
#     ax.set_ylim(1e-2, 1e7)
#     #ax.autoscale(enable=True, tight=True)
#     #ax.set_xlim(1e-6, 1e10)
#     fig.tight_layout()

#     plt.legend(fontsize=15, loc='lower left')
#     ax.tick_params(labelsize=15)
#     secax.tick_params(labelsize=15)
#     plt.xlabel('Wavenumber (1/m)', fontsize=15)
#     plt.ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
#     plt.title(f'Averaged Spectral Estimates of SST {figname}',fontsize=15)
#     plt.savefig(f'{path_save_name}.png', bbox_inches='tight')


def add_slopes(f_sat, A_sat_mean, f_smt, A_smt_mean, horizontal):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fx = np.linspace(1e-4,5e-4,100)
    pp = 1000
    y2 = pp* 3e-5*np.power(fx, (-2))
    y3 = pp* 1e-10*np.power(fx, (-3))
    y5 = pp* 1e-3*np.power(fx, (-5/3))

    plt.loglog(fx,y3, label='slope -3')
    plt.loglog(fx,y5, label='slope -5/3')

    # slope satellite
    xdata = f_sat[15:]
    ydata = A_sat_mean[15:]
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="limegreen")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="limegreen", label=f'slope {index:3.3}') 


    # slope smt
    if horizontal == True: 
        xdata = f_smt[300:]
        ydata = A_smt_mean[300:]
    else:
        xdata = f_smt[100::600]
        ydata = A_smt_mean[100::600]

    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="gold")  
    plt.plot(fx, powerlaw(fx, 5*pp*amp, index), linestyle='dashed', color="gold", label=f'slope {index:3.3}') 

    # slope smt
    if horizontal == True:
        xdata = f_smt[20:300]
        ydata = A_smt_mean[20:300]
    else:
        xdata = f_smt[15:145]
        ydata = A_smt_mean[15:145]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index), linewidth=4, linestyle='--', color="aqua")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linewidth=2, linestyle='--', color="aqua", label=f'slope {index:3.3}') 




def plot_spatial_spectra_sst_horizontal(A_smt_mean, std_smt, f_smt, A_sat_mean, std_sat, f_sat, figname, path_save_name, horizontal):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))
    #l=2.5

    # # smt
    # kernel_size = k
    # kernel = np.ones(kernel_size) / kernel_size
    # #smean_m_std = A_smt_mean - std_smt #np.abs(A_smt_mean - std_smt)
    # smean_m_std = np.abs(A_smt_mean - std_smt)
    # smean_m_std = np.convolve(smean_m_std, kernel, mode='same')
    # #smean_p_std = A_smt_mean + std_smt#np.abs(A_smt_mean + std_smt)
    # smean_p_std = np.abs(A_smt_mean + std_smt)
    # smean_p_std = np.convolve(smean_p_std, kernel, mode='same')

    #plt.loglog(f_smt, smean_p_std, color='royalblue', linestyle='--',alpha=0.5)
    plt.loglog(f_smt, A_smt_mean, label='smt_sst_mean', color='royalblue', linewidth=2)
    #plt.fill_between(f_smt, A_smt_mean, smean_p_std, color='royalblue', alpha=0.25)
    #plt.loglog(f_smt, smean_m_std, linestyle='--',  color='royalblue', alpha=0.5)
    #plt.fill_between(f_smt, A_smt_mean, smean_m_std, color='royalblue', alpha=0.25)

    # # satellite
    # kernel_size = k
    # kernel = np.ones(kernel_size) / kernel_size
    # mean_m_std = np.abs(A_sat_mean - std_sat)
    # mean_m_std = np.convolve(mean_m_std, kernel, mode='same')
    # mean_p_std = np.abs(A_sat_mean + std_sat)
    # mean_p_std = np.convolve(mean_p_std, kernel, mode='same')

    #plt.loglog(f_sat, mean_p_std, color='tomato', linestyle='--', alpha=0.5)
    plt.loglog(f_sat, A_sat_mean, label='sat_sst_mean', color='tomato', linewidth=2)
    #plt.fill_between(f_sat, A_sat_mean, mean_p_std, color='wheat', alpha=0.5)
    #plt.loglog(f_sat, mean_m_std, linestyle='--',  color='tomato', alpha=0.5)
    #plt.fill_between(f_sat, A_sat_mean, mean_m_std, color='wheat', alpha=0.5)

    #####################
    fx = np.linspace(1e-4,5e-4,100)
    pp = 100000
    y2 = pp* 3e-8*np.power(fx, (-2))
    y3 = pp* 1e-12*np.power(fx, (-3))
    y5 = pp* 1e-6*np.power(fx, (-5/3))

    plt.loglog(fx,y3, label='slope -3')
    plt.loglog(fx,y5, label='slope -5/3')

    # slope satellite
    xdata = f_sat[20:]
    ydata = A_sat_mean[20:]
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="limegreen")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="limegreen", label=f'slope {index:3.3}') 


    # slope smt
    if horizontal == True: 
        xdata = f_smt[300:]
        ydata = A_smt_mean[300:]
    else:
        xdata = f_smt[32::600]
        ydata = A_smt_mean[32::600]

    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="gold")  
    plt.plot(fx, powerlaw(fx, 5*pp*amp, index), linestyle='dashed', color="gold", label=f'slope {index:3.3}') 

    # slope smt
    if horizontal == True:
        xdata = f_smt[20:300]
        ydata = A_smt_mean[20:300]
    else:
        xdata = f_smt[6:30]
        ydata = A_smt_mean[6:30]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index), linewidth=4, linestyle='--', color="aqua")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linewidth=2, linestyle='--', color="aqua", label=f'slope {index:3.3}') 

    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    # ax.set_ylim(1e-2, 1e7)
    #ax.autoscale(enable=True, tight=True)
    #ax.set_xlim(1e-6, 1e10)
    fig.tight_layout()

    plt.legend(fontsize=15, loc='lower left')
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    plt.title(f'Averaged Spectral Estimates of SST {figname}',fontsize=15)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight')


def plot_spatial_spectra_sst_all(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name, path_save_name, secs, horizontal, n, nr, K, savefig):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))

    plot_slopes(horizontal, f_sat, BM, f_r2b8, CM, f_smt, AM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)
    plt.legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.3,0.0), frameon=False)

    s=0.02
    alpha = 0.8
    for ii in np.arange(secs*n):
        plt.scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(secs*n):
        plt.scatter(f_sat, BB[ii,:], color='wheat', s=s, alpha=alpha, rasterized=True)

    for ii in np.arange(secs*nr):
        plt.scatter(f_r2b8, CC[ii,:], color='mediumseagreen', s=s, alpha=alpha, rasterized=True)

    confidence = 0.95 
    n = K*n
    CI = calc_CI(AM, AM_std, n-1, confidence, n)
    l1, = ax.loglog(f_smt, AM, label='ICON-SMT', color='royalblue', linewidth=1)
    f = ax.fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='royalblue', alpha=0.5)

    CI = calc_CI(BM, BM_std, n-1, confidence, n)
    l2, = ax.loglog(f_sat, BM, label='Modis Aqua', color='tomato', linewidth=1)
    f2 = ax.fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tomato', alpha=0.5)

    nr = K*nr
    CI = calc_CI(CM, CM_std, nr-1, confidence, nr)
    l3, = ax.loglog(f_r2b8, CM, label='ICON-R2B8', color='tab:green', linewidth=1)
    f3 = ax.fill_between(f_r2b8,  CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:green', alpha=0.5)
        
    #---------------------------------------#
    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-1, 1e6)
    # ax.set_xlim(7e-7, 7e-4)
    fig.tight_layout()

    leg = Legend(ax,
    [(l1, f, f), (l2, f2, f2),(l3, f3, f3)],
    [f"ICON-SMT", f"Modis Aqua", f"ICON-R2B8"],
    fontsize=15,
    loc="lower left",
    frameon=False,
    #handlelength=3.0,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    ax.add_artist(leg) #add artificial legend 
    
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.grid(ls=':')
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density K^2/(1/m)',fontsize=15)
    plt.title(f'Spatially averaged spectral estimates of {reg_name} SST sections',fontsize=15)
    # plt.savefig(f'{path_save_name}.pdf', bbox_inches='tight', dpi=200)
    if savefig == True: plt.savefig(f'{path_save_name}.png', bbox_inches='tight', dpi=200)

def plot_spatial_spectra_ssh_all(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, reg_name, path_save_name, secs, horizontal, n, K):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))

    # plot_slopes_ssh(horizontal, f_sat, BM, f_r2b8, CM, f_smt, AM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)
    plot_slopes_ssh(horizontal, f_sat, BM, f_smt, AM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)
    plt.legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.3,0.0), frameon=False)

    s=0.05
    alpha = 0.8
    for ii in np.arange(K*n):
        plt.scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(K*n):
        plt.scatter(f_sat, BB[ii,:], color='wheat', s=s, alpha=alpha, rasterized=True)

    confidence = 0.95
    n_smt = K*n#*int(984/11)
    CI = calc_CI(AM, AM_std, n_smt-1, confidence, n_smt)
    l1, = ax.loglog(f_smt, AM, label='smt', color='royalblue', linewidth=1)
    f = ax.fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='royalblue', alpha=0.5, label=f'CI={confidence}')

    n_sat = K*n#*90
    CI = calc_CI(BM, BM_std, n_sat-1, confidence, n_sat)
    l2, = ax.loglog(f_sat, BM, label='sat', color='tomato', linewidth=1)
    f2 = ax.fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tomato', alpha=0.5, label=f'CI={confidence}')

    #---------------------------------------#
    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-2, 1e5)
    fig.tight_layout()

    leg = Legend(ax,
    [(l1, f, f), (l2, f2, f2)],
    [f"smt CI={confidence}", f"sat CI={confidence}"],
    fontsize=15,
    loc="lower left",
    frameon=False,
    #handlelength=3.0,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    ax.add_artist(leg) #add artificial legend 
    
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    plt.title(f'Temporal and spatially averaged spectral estimates of {reg_name} SSH sections',fontsize=15)
    plt.savefig(f'{path_save_name}.pdf', bbox_inches='tight', dpi=200)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight', dpi=200)

def plot_spatial_spectra_sst_section(A_smt, f_smt, A_sat, f_sat, A_r2b8, f_r2b8, figname, path_save_name):
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))

    plt.loglog(f_smt, A_smt, label='smt_sst', color='royalblue', linewidth=1)

    plt.loglog(f_sat, A_sat, label='sat_sst', color='tomato', linewidth=1)

    plt.loglog(f_r2b8, A_r2b8, label='r2b8_sst', color='tab:green', linewidth=1)

    #---------------------------------------#
    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    # ax.set_ylim(1e-1, 1e6)

    fig.tight_layout()

    plt.legend(fontsize=15, loc='lower left')
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density K^2/(1/m)',fontsize=15)
    plt.title(f'Averaged Spectral Estimates of SST {figname}',fontsize=15)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight')

def plot_spatial_spectra_all_lat_dep(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, reg_name, path_save_name, secs, horizontal, regions):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))
    color = iter(cm.viridis(np.linspace(0, 1, secs)))

    for ii in np.arange(secs):
        c = next(color)
        plt.scatter(f_smt, AA[ii,:], c=c, label=f'sst_smt_{regions[ii]}', alpha =0.5, s=6)
    
    color = iter(cm.viridis(np.linspace(0, 1, secs)))
    for ii in np.arange(secs):
        c = next(color)
        plt.scatter(f_sat, BB[ii,:], c=c, label=f'sst_sat_{regions[ii]}', alpha=1, s=10, marker='s')

    plt.loglog(f_smt, AM, label='smt_sst_mean', color='royalblue', linewidth=3)
    plt.loglog(f_sat, BM, label='sat_sst_mean', color='tomato', linewidth=3)

    
    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-1, 1e6)
    fig.tight_layout()

    plt.legend(fontsize=15, loc='lower left')
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    plt.title(f'Latitude dependence of SST Spectra {reg_name}',fontsize=15)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight')

def calc_CI(m, s, dof, confidence, realisations):
    t_crit = np.abs( t.ppf((1-confidence)/2, dof))
    A = (m-s*t_crit/np.sqrt(realisations), m+s*t_crit/np.sqrt(realisations))
    return A

def plot_spatial_spectra_ci(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name,  K, axs, horizontal, n, nr):
    powerlaw = lambda x, amp, index: amp * (x**index)
    
    if   K == 0: idx1, idx2 = 0,0
    elif K == 1: idx1, idx2 = 0,1
    elif K == 2: idx1, idx2 = 1,0
    elif K == 3: idx1, idx2 = 1,1
    elif K == 4: idx1, idx2 = 2,0
    elif K == 5: idx1, idx2 = 2,1
    else: raise ValueError('K must be between 0 and 5')

    plot_slopes(horizontal, f_sat, BM, f_r2b8, CM, f_smt, AM, powerlaw, axs, idx1, idx2, REG = True)
    axs[idx1,idx2].legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.5,0.0), frameon=False)

    s=0.05
    alpha = 0.8
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_sat, BB[ii,:], color='wheat', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(nr):
        axs[idx1,idx2].scatter(f_r2b8, CC[ii,:], color='mediumseagreen', s=s, alpha=alpha, rasterized=True)

    confidence = 0.95 
    
    CI = calc_CI(AM, AM_std, n-1, confidence, n)

    l1, = axs[idx1,idx2].loglog(f_smt, AM, label='smt', color='royalblue', linewidth=1)
    f1 = axs[idx1,idx2].fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0,  color='royalblue', alpha=0.5, label=f'CI={confidence}')

    CI = calc_CI(BM, BM_std, n-1, confidence, n)

    l2, = axs[idx1,idx2].loglog(f_sat, BM, label='sat', color='tomato', linewidth=1)
    f2 = axs[idx1,idx2].fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tomato', alpha=0.5, label=f'CI={confidence}')

    CI = calc_CI(CM, CM_std, nr-1, confidence, nr)

    l3, = axs[idx1,idx2].loglog(f_r2b8, CM, label='r2b8', color='tab:green', linewidth=1)
    f3 = axs[idx1,idx2].fill_between(f_r2b8,  CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:green', alpha=0.5, label=f'CI={confidence}')
    
    axs[idx1,idx2].autoscale(enable=True, tight=True)
    axs[idx1,idx2].set_ylim(1e-1, 1e6)

    leg = Legend(axs[idx1,idx2],
    [(l1, f1, f1), (l2, f2, f2),(l3, f3, f3)],
    [f"smt CI={confidence}", f"sat CI={confidence}", f"r2b8 CI={confidence}"],
    fontsize=15,
    loc="lower left",
    frameon=False,
    #handlelength=3.0,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    axs[idx1,idx2].add_artist(leg) #add artificial legend 

    #axs[idx1,idx2].legend(fontsize=15, loc='lower left')
    axs[idx1,idx2].tick_params(labelsize=15)
    if K >= 4: axs[idx1,idx2].set_xlabel('Wavenumber (1/m)', fontsize=15)
    if is_odd(K)== False: axs[idx1,idx2].set_ylabel('Power Spectral Density K^2/(1/m)',fontsize=15)
    axs[idx1,idx2].set_title(f'{reg_name}',fontsize=15)

def plot_spatial_spectra_ssh_ci(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, reg_name,  K, axs, horizontal, n, n_minus_flag):
    powerlaw = lambda x, amp, index: amp * (x**index)
    
    if K == 0: idx1, idx2 = 0,0
    if K == 1: idx1, idx2 = 0,1
    if K == 2: idx1, idx2 = 1,0
    if K == 3: idx1, idx2 = 1,1
    if K == 4: idx1, idx2 = 2,0
    if K == 5: idx1, idx2 = 2,1

    plot_slopes_ssh(horizontal, f_sat, BM, f_smt, AM, powerlaw, axs, idx1, idx2, REG=True)
    axs[idx1,idx2].legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.5,0.0), frameon=False)

    s=0.05
    alpha = 0.8
    for ii in np.arange(n_minus_flag):
        axs[idx1,idx2].scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_sat, BB[ii,:], color='wheat', s=s, alpha=alpha, rasterized=True)

    confidence = 0.95
    n_smt = n_minus_flag#*int(984/11)
    CI = calc_CI(AM, AM_std, n_smt-1, confidence, n_smt)

    l1, = axs[idx1,idx2].loglog(f_smt, AM, label='smt', color='royalblue', linewidth=1)
    f1 = axs[idx1,idx2].fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0,  color='royalblue', alpha=0.5, label=f'CI={confidence}')
    n_sat = n#*90
    CI = calc_CI(BM, BM_std, n_sat-1, confidence, n_sat)

    l2, = axs[idx1,idx2].loglog(f_sat, BM, label='sat', color='tomato', linewidth=1)
    f2 = axs[idx1,idx2].fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tomato', alpha=0.5, label=f'CI={confidence}')

    axs[idx1,idx2].autoscale(enable=True, tight=True)
    axs[idx1,idx2].set_ylim(1e-2, 1e5)

    leg = Legend(axs[idx1,idx2],
    [(l1, f1, f1), (l2, f2, f2)],
    [f"smt CI={confidence}", f"sat CI={confidence}"],
    fontsize=15,
    loc="lower left",
    frameon=False,
    #handlelength=3.0,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    axs[idx1,idx2].add_artist(leg) #add artificial legend 

    #axs[idx1,idx2].legend(fontsize=15, loc='lower left')
    axs[idx1,idx2].tick_params(labelsize=15)
    if K >= 4: axs[idx1,idx2].set_xlabel('Wavenumber (1/m)', fontsize=15)
    if is_odd(K)== False: axs[idx1,idx2].set_ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    axs[idx1,idx2].set_title(f'{reg_name}',fontsize=15)


def plot_slopes(horizontal, f_sat, BM, f_r2b8, CM, f_smt, AM, powerlaw, axs, idx1, idx2, REG):
    # shift regressions
    shift = 80

    # slope smt
    if horizontal == True:
        start = 20
        end =  -int(f_smt.size/2)
        xdata = f_smt[start:end]
        ydata = AM[start:end]
    else:
        #xdata = f_smt[15:145]
        #ydata = AM[15:145]
        #welch
        #xdata = f_smt[3:35]
        #ydata = AM[3:35]
        start = 20
        end =  -int(f_smt.size/2)
        xdata = f_smt[start:end]
        ydata = AM[start:end]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  

    # slope satellite
    if horizontal == True:
        start = 20
        end =  -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata =  BM[start:end]
    else:
        #xdata = f_sat[15:]
        #ydata =  BM[15:]
        #welch
        #xdata = f_sat[6:]
        #ydata =  BM[6:]
        start = 20
        end =  -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata =  BM[start:end]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tomato", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tomato", label=f'slope {index:3.3}')  

    # else:
    if True:
        # slope r2b8
        if horizontal == True:
            start = 20
            end =  -int(f_r2b8.size/4)
            xdata = f_r2b8[start:end]
            ydata = CM[start:end]
        else:
            #xdata = f_r2b8[15:]
            #ydata = CM[15:]
            #welch
            #xdata = f_r2b8[4:]
            #ydata = CM[4:]
            start = 20
            end =  -int(f_r2b8.size/4)
            xdata = f_r2b8[start:end]
            ydata = CM[start:end]

        logx = np.log10(xdata)
        logy = np.log10(ydata)
        res = stats.linregress(logx, logy)
        index = res.slope
        amp = 10**res.intercept
        if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tab:green", label=f'slope {index:3.3}') 
        else: plt.plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tab:green", label=f'slope {index:3.3}') 

def plot_slopes_ssh(horizontal, f_sat, BM, f_smt, AM, powerlaw, axs, idx1, idx2, REG):
    # shift regressions
    shift = 80
    start = 10
    # slope smt
    if horizontal == True:
        end =  -int(39*f_smt.size/40)
        xdata = f_smt[start:end]
        ydata = AM[start:end]
    else:
        end =  -int(39*f_smt.size/40)
        xdata = f_smt[start-2:end]
        ydata = AM[start-2:end]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  

    # slope satellite
    if horizontal == True:
        end =  -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata =  BM[start:end]
    else:
        end =  -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata =  BM[start:end]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tomato", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tomato", label=f'slope {index:3.3}')  

def plot_spatial_spectra6(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, reg_name,  K, axs, horizontal, n):
    powerlaw = lambda x, amp, index: amp * (x**index)
    
    if K == 0: idx1, idx2 = 0,0
    if K == 1: idx1, idx2 = 0,1
    if K == 2: idx1, idx2 = 1,0
    if K == 3: idx1, idx2 = 1,1
    if K == 4: idx1, idx2 = 2,0
    if K == 5: idx1, idx2 = 2,1
 
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_smt, AA[ii,:], color='lightgrey')
    
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_sat, BB[ii,:], color='wheat')

    #axs[idx1,idx2].loglog(f_smt, AM+AM_std, color='royalblue', linestyle='--',alpha=0.5)
    axs[idx1,idx2].loglog(f_smt, AM, label='smt_ssh_mean', color='royalblue', linewidth=2)
    #axs[idx1,idx2].fill_between(f_smt, AM, AM+AM_std, color='royalblue', alpha=0.25)
    #axs[idx1,idx2].loglog(f_smt, AM-AM_std, linestyle='--',  color='royalblue', alpha=0.5)
    #axs[idx1,idx2].fill_between(f_smt, AM, AM-AM_std, color='royalblue', alpha=0.25)

    #axs[idx1,idx2].loglog(f_sat, BM+BM_std, color='tomato', linestyle='--', alpha=0.5)
    axs[idx1,idx2].loglog(f_sat, BM, label='sat_adt_mean', color='tomato', linewidth=2)
    #axs[idx1,idx2].fill_between(f_sat, BM, BM+BM_std, color='wheat', alpha=0.5)
    #axs[idx1,idx2].loglog(f_sat, BM-BM_std, linestyle='--',  color='tomato', alpha=0.5)
    #axs[idx1,idx2].fill_between(f_sat, BM, BM-BM_std, color='wheat', alpha=0.5)

    #####################
    fx = np.linspace(1e-4,5e-4,100)
    if horizontal == True: 
        pp = 10000
        y2 = pp* 3e-9*np.power(fx, (-2))
        y3 = pp* 6e-13*np.power(fx, (-3))
        y5 = pp* 1e-6*np.power(fx, (-5/3))
    else:
        pp = 1000
        y2 = pp* 3e-5*np.power(fx, (-2))
        y3 = pp* 1e-10*np.power(fx, (-3))
        y5 = pp* 1e-3*np.power(fx, (-5/3))

    axs[idx1,idx2].loglog(fx,y3, label='slope -3',)
    axs[idx1,idx2].loglog(fx,y5, label='slope -5/3 = 1.67')

    # slope satellite
    if horizontal == True: 
        xdata = f_sat[20:]
        ydata =  BM[20:]
    else:
        xdata = f_sat[15:]
        ydata =  BM[15:]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    axs[idx1,idx2].plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="red")  
    axs[idx1,idx2].plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="red", label=f'slope {index:3.3}') 


    # slope smt
    if horizontal == True: 
        xdata = f_smt[300:]
        ydata = AM[300:]
    else:
        xdata = f_smt[100::600]
        ydata = AM[100::600]

    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    axs[idx1,idx2].plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="gold")  
    axs[idx1,idx2].plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="gold", label=f'slope {index:3.3}') 

    # slope smt
    if horizontal == True:
        xdata = f_smt[20:300]
        ydata = AM[20:300]
    else:
        xdata = f_smt[15:145]
        ydata = AM[15:145]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    axs[idx1,idx2].plot(xdata, powerlaw(xdata, amp, index), linewidth=4, linestyle='--', color="aqua")  
    axs[idx1,idx2].plot(fx, powerlaw(fx, pp*amp, index), linewidth=2, linestyle='--', color="aqua", label=f'slope {index:3.3}') 


    axs[idx1,idx2].autoscale(enable=True, tight=True)
    axs[idx1,idx2].set_ylim(1e-1, 1e6)

    axs[idx1,idx2].legend(fontsize=15, loc='lower left')
    axs[idx1,idx2].tick_params(labelsize=15)
    if K >= 4: axs[idx1,idx2].set_xlabel('Wavenumber (1/m)', fontsize=15)
    if is_odd(K)== False: axs[idx1,idx2].set_ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    axs[idx1,idx2].set_title(f'{reg_name}',fontsize=15)

def is_odd(num):
    return num & 0x1

#horizontal for ssh OLD
def get_lon_lat_horizontal(k,n):
    #n = 10
    #lon1 = -49, - 25
    lon1 = -70, - 48
    #if k > 2: lon1 = -75, -51; k=k-3
    lati = 25.8, 26.2
    lati = lati + np.ones(2)*3*k
    Lat = np.zeros((n, 2))
    Lon = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lati
        Lon[ii,:] = lon1
        lati += np.ones(2)*0.25 #0.5
    return(Lat, Lon)

#horizontal for sst
def get_lon_lat_horizontal_sst(k, n):
    lon1  = -72.5, - 56
    dlat  = 0.04166412
    y1    = 28.98
    lati  = y1,    y1+dlat
    #lati = 25.8,  26.2
    lati  = lati + np.ones(2)*1.7*k
    Lat   = np.zeros((n, 2))
    Lon   = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lati
        Lon[ii,:] = lon1
        lati += np.ones(2)*0.05 # slightly larger then grid spacing
    return(Lat, Lon)

def get_lon_lat_horizontal_sst_wave(k, n):
    lon1  = -1, 12
    dlat  = 0.04166412
    y1    = -35
    lati  = y1,    y1+dlat
    lati  = lati + np.ones(2)*1.7*k
    Lat   = np.zeros((n, 2))
    Lon   = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lati
        Lon[ii,:] = lon1
        lati += np.ones(2)*0.05 # slightly larger then grid spacing
    return(Lat, Lon)

def get_lon_lat_horizontal_r2b8(k, n):
    lon1 = -72.5, - 56
    dlat = 0.1
    y1   = 28.98
    lati = y1,    y1+dlat
    lati = lati + np.ones(2)*1.7*k
    Lat  = np.zeros((n, 2))
    Lon  = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lati
        Lon[ii,:] = lon1
        lati += np.ones(2)*0.1 # slightly larger then grid spacing
    return(Lat, Lon)

def get_lon_lat_horizontal_r2b8_wave(k, n):
    lon1 = -1, 12
    dlat = 0.1
    y1   = -35
    lati = y1,    y1+dlat
    lati = lati + np.ones(2)*1.7*k
    Lat  = np.zeros((n, 2))
    Lon  = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lati
        Lon[ii,:] = lon1
        lati += np.ones(2)*0.1 # slightly larger then grid spacing
    return(Lat, Lon)

#old!
def get_lon_lat_vertical(k, n):
    #n = 10
    lat1 = 25, 43
    loni = -68.2, -67.8
    loni = loni + np.ones(2)*3*k
    Lat = np.zeros((n, 2))
    Lon = np.zeros((n, 2))
    for ii in np.arange(n):
        Lon[ii,:] = loni
        Lat[ii,:] = lat1
        loni += np.ones(2)*0.25 #0.5
    return(Lat, Lon)

#used for smt and satellite
def get_lon_lat_vertical_sst(k,n):
    lat1 = 28, 40
    dlon = 0.04166412
    y1   = -68
    loni = y1, y1+dlon
    loni = loni + np.ones(2)*1.7*k
    Lat  = np.zeros((n, 2))
    Lon  = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lat1
        Lon[ii,:] = loni
        loni += np.ones(2)*0.05 # slightly larger then grid spacing
    return(Lat, Lon)

def get_lon_lat_vertical_sst_r2b8(k,n):
    lat1 = 28, 40
    dlon = 0.1
    y1   = -68
    loni = y1, y1+dlon
    loni = loni + np.ones(2)*1.7*k
    Lat  = np.zeros((n, 2))
    Lon  = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lat1
        Lon[ii,:] = loni
        loni += np.ones(2)*0.1 # slightly larger then grid spacing
    return(Lat, Lon)

def get_lon_lat_vertical_sst_r2b8_exp2(k,n):
    lat1 = 31, 37
    dlon = 0.1
    y1   = -70
    loni = y1, y1+dlon
    loni = loni + np.ones(2)*3.3*k
    Lat  = np.zeros((n, 2))
    Lon  = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lat1
        Lon[ii,:] = loni
        loni += np.ones(2)*0.1 # slightly larger then grid spacing
    return(Lat, Lon)

def get_lon_lat_vertical_sst_exp2(k,n):
    lat1 = 31, 37
    dlon = 0.04166412
    y1   = -70
    loni = y1, y1+dlon
    loni = loni + np.ones(2)*3.3*k
    Lat  = np.zeros((n, 2))
    Lon  = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lat1
        Lon[ii,:] = loni
        loni += np.ones(2)*0.05 # slightly larger then grid spacing
    return(Lat, Lon)   


def plot_regions_horizontal(smt, S, n, regions, parent_imag, savefig ):
    fpath_tgrid = '/work/mh0033/from_Mistral/mh0033/m300602/icon/grids/smt/smt_tgrid.nc'
    gg = xr.open_dataset(fpath_tgrid)
    res = np.sqrt(gg.cell_area_p)
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'

    ddnpz = np.load(fpath_ckdtree)
    lon_010deg = ddnpz['lon']
    lat_010deg = ddnpz['lat']
    res_010deg = pyic.apply_ckdtree(res, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_010deg.size, lon_010deg.size)
    res_010deg[res_010deg==0.] = np.ma.masked

    # plot region
    lon_reg = [-90, -20]
    lat_reg = [20, 60]
    #regions = 'A','B','C','D','E','F'
    #n = 10
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    lon, lat, data = pyic.interp_to_rectgrid(smt.isel(time=50), fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    pyic.shade(lon, lat, data, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False)
    levels = 500 + np.arange(11)*100
    CS = ax.contour(lon_010deg, lat_010deg, res_010deg,  levels=levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)

    for k in range(S):
        #print(k)
        Lat, Lon = get_lon_lat_horizontal_sst(k, n) #TODO wrong name for regions
        #print(Lat)
        for ii in np.arange(n):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=0.3)

        loc = Lon[ii,0]+(Lon[ii,1]-Lon[ii,0])/2, Lat[5,0]
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         #arrowprops=dict(arrowstyle="->", facecolor='black', linewidth=2, mutation_scale=30),
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)

    ax.set_title(f'Visualization sections', fontsize=15)
    #ax.set_xlabel('lon')
    #ax.set_ylabel('lat')
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig == True: plt.savefig(f'{parent_imag}vis_reg', bbox_inches='tight')   

def plot_regions_horizontal_wave_NA(smt, S, n, regions, parent_imag, savefig, lon_reg = [-90, -20], lat_reg = [20, 60] ):
    gg  = load_smt_wave_grid()
    res = np.sqrt(gg.cell_area_p)
    res = res.rename(cell='ncells')
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
    res_data = pyic.interp_to_rectgrid_xr(res, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

    data = pyic.interp_to_rectgrid_xr(smt, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False)
    levels = 4000 + np.arange(22)*300
    CS = ax.contour(res_data.lon, res_data.lat, res_data,  levels=levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)

    for k in range(S):
        Lat, Lon = get_lon_lat_horizontal_sst(k, n) #TODO wrong name for regions
        for ii in np.arange(n):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=0.3)

        loc = Lon[ii,0]+(Lon[ii,1]-Lon[ii,0])/2, Lat[5,0]
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         #arrowprops=dict(arrowstyle="->", facecolor='black', linewidth=2, mutation_scale=30),
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)

    ax.set_title(f'Visualization sections', fontsize=15)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig == True: plt.savefig(f'{parent_imag}vis_reg', bbox_inches='tight')   

def plot_regions_horizontal_wave_SA(smt, S, n, regions, parent_imag, savefig, lon_reg = [-90, -20], lat_reg = [20, 60] ):
    gg            = load_smt_wave_grid()
    res           = np.sqrt(gg.cell_area_p)
    res           = res.rename(cell='ncells')
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'
    res_data      = pyic.interp_to_rectgrid_xr(res, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
    data = pyic.interp_to_rectgrid_xr(smt, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False)
    levels = 500 + np.arange(22)*100
    CS = ax.contour(res_data.lon, res_data.lat, res_data,  levels=levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)

    for k in range(S):
        Lat, Lon = get_lon_lat_horizontal_sst_wave(k, n) #TODO wrong name for regions
        for ii in np.arange(n):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=0.3)

        loc = Lon[ii,0]+(Lon[ii,1]-Lon[ii,0])/2, Lat[5,0]
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         #arrowprops=dict(arrowstyle="->", facecolor='black', linewidth=2, mutation_scale=30),
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)

    ax.set_title(f'Visualization sections', fontsize=15)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig == True: plt.savefig(f'{parent_imag}vis_reg', bbox_inches='tight')   



def plot_regions_horizontal_satellite_SA(sata, S, n, regions, parent_imag, savefig):
    lon_reg = [-40, 30]
    lat_reg = [-55, -15]
    clim = 0, 24

    gg            = load_smt_wave_grid()
    res           = np.sqrt(gg.cell_area_p)
    res           = res.rename(cell='ncells')
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'

    res_data      = pyic.interp_to_rectgrid_xr(res, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)


    sst_sat = sata.where((sata.lat > lat_reg[0]) & (sata.lat < lat_reg[1]) & (sata.lon > lon_reg[0]) & (sata.lon < lon_reg[1]), drop=True)

    # t=70
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(sst_sat.lon, sst_sat.lat, sst_sat.sst_night, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False)

    levels = 500 + np.arange(22)*100
    CS = ax.contour(res_data.lon, res_data.lat, res_data,  levels=levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)

    for k in range(S):
        Lat, Lon = get_lon_lat_horizontal_sst_wave(k, n) # TODO:wrong name for regions
        for ii in np.arange(n):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=0.3)

        loc = Lon[ii,0]+(Lon[ii,1]-Lon[ii,0])/2, Lat[5,0]
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig==True: plt.savefig(f'{parent_imag}vis_reg_sat', bbox_inches='tight') 


def plot_regions_horizontal_satellite(sata, S, n, regions, parent_imag, savefig):
    lon_reg = [-90, -20]
    lat_reg = [20, 60]
    clim = 0, 24

    sst_sat = sata.where((sata.lat > lat_reg[0]) & (sata.lat < lat_reg[1]) & (sata.lon > lon_reg[0]) & (sata.lon < lon_reg[1]), drop=True)

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(sst_sat.lon, sst_sat.lat, sst_sat.sst_night, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False)

    for k in range(S):
        Lat, Lon = get_lon_lat_horizontal_sst(k, n) # TODO:wrong name for regions
        for ii in np.arange(n):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=0.3)

        loc = Lon[ii,0]+(Lon[ii,1]-Lon[ii,0])/2, Lat[5,0]
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig==True: plt.savefig(f'{parent_imag}vis_reg_sat', bbox_inches='tight') 


def plot_regions_vertical_satellite(sata, S, n, regions, parent_imag):
    lon_reg = [-90, -20]
    lat_reg = [20, 60]
    clim = 0, 24

    sst_sat = sata.where((sata.lat > lat_reg[0]) & (sata.lat < lat_reg[1]) & (sata.lon > lon_reg[0]) & (sata.lon < lon_reg[1]), drop=True)

    t=79
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(sst_sat.lon, sst_sat.lat, sst_sat.isel(time=t).sst_night, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False)

    for k in range(S):
        Lat, Lon = get_lon_lat_vertical_sst(k, n)
        for ii in np.arange(n):
            ax.plot((Lon[ii,0],Lon[ii,0]), (Lat[ii,1], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=0.3)

        loc = Lon[5,0], (Lat[0,0] + (Lat[0,1]-Lat[0,0])/2)
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    plt.savefig(f'{parent_imag}vis_reg_sat', bbox_inches='tight') 

def plot_regions_horizontal_r2b8(data, S, n, regions, parent_imag, savefig):
    # data = data.isel(time=79)
    lon_reg = [-90, -20]
    lat_reg = [20, 60]
    clim = 0, 24
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b8_oce_r0004/ckdtree/rectgrids/r2b8_oce_r0004_res0.10_180W-180E_90S-90N.nc'

    r2b8_inter = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=5, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(r2b8_inter.lon, r2b8_inter.lat, r2b8_inter, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False)

    for k in range(S):
        Lat, Lon = get_lon_lat_horizontal_r2b8(k, n)
        for ii in np.arange(n):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=1)

        loc = Lon[ii,0]+(Lon[ii,1]-Lon[ii,0])/2, Lat[5,0]
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig==True: plt.savefig(f'{parent_imag}vis_reg_r2b8.png', bbox_inches='tight', format='png') 


def plot_regions_horizontal_r2b8_wave_SA(data, S, n, regions, parent_imag, savefig, lon_reg = [-90, -20], lat_reg = [20, 60] ):
    clim = 0, 24
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b8_oce_r0004/ckdtree/rectgrids/r2b8_oce_r0004_res0.10_180W-180E_90S-90N.nc'

    r2b8_inter = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=5, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(r2b8_inter.lon, r2b8_inter.lat, r2b8_inter, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False)

    for k in range(S):
        Lat, Lon = get_lon_lat_horizontal_r2b8_wave(k, n)
        for ii in np.arange(n):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=1)

        loc = Lon[ii,0]+(Lon[ii,1]-Lon[ii,0])/2, Lat[5,0]
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig==True: plt.savefig(f'{parent_imag}vis_reg_r2b8.png', bbox_inches='tight', format='png') 


def plot_regions_vertical_r2b8(data, S, n, regions, parent_imag):
    data = data.isel(time=79)
    lon_reg = [-90, -20]
    lat_reg = [20, 60]
    clim = 0, 24
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b8_oce_r0004/ckdtree/rectgrids/r2b8_oce_r0004_res0.10_180W-180E_90S-90N.nc'

    r2b8_inter = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=5, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(r2b8_inter.lon, r2b8_inter.lat, r2b8_inter, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False)

    for k in range(S):
        Lat, Lon = get_lon_lat_vertical_sst_r2b8(k, n)
        for ii in np.arange(n):
            ax.plot((Lon[ii,0],Lon[ii,0]), (Lat[ii,1], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=1)

        loc = Lon[5,0], (Lat[0,0] + (Lat[0,1]-Lat[0,0])/2)
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    plt.savefig(f'{parent_imag}vis_reg_r2b8.png', bbox_inches='tight', format='png') 

def plot_regions_vertical(smt, S, n, regions, parent_imag):
    fpath_tgrid   = '/work/mh0033/from_Mistral/mh0033/m300602/icon/grids/smt/smt_tgrid.nc'
    gg            = xr.open_dataset(fpath_tgrid)
    res           = np.sqrt(gg.cell_area_p)
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'

    ddnpz                      = np.load(fpath_ckdtree)
    lon_010deg                 = ddnpz['lon']
    lat_010deg                 = ddnpz['lat']
    res_010deg                 = pyic.apply_ckdtree(res, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_010deg.size, lon_010deg.size)
    res_010deg[res_010deg==0.] = np.ma.masked

    # plot region
    lon_reg = [-90, -20]
    lat_reg = [20, 60]

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    lon, lat, data = pyic.interp_to_rectgrid(smt.isel(time=50), fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    pyic.shade(lon, lat, data, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False)
    levels = 500 + np.arange(11)*100
    CS = ax.contour(lon_010deg, lat_010deg, res_010deg,  levels=levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)

    for k in range(S):
        Lat, Lon = get_lon_lat_vertical_sst(k, n)
        for ii in np.arange(n):
            ax.plot((Lon[ii,0],Lon[ii,0]), (Lat[ii,0], Lat[ii,1]), transform=ccrs_proj, color='darkviolet', linewidth=0.3)

        loc = Lon[5,0], (Lat[0,0] + (Lat[0,1]-Lat[0,0])/2)
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=20,        
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)

    ax.set_title(f'Visualization sections', fontsize=15)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    plt.savefig(f'{parent_imag}vis_reg2', bbox_inches='tight')      

def load_satellite_sst():
    #satellite
    #TODO change to ICDC aviso ssh folder
    #path_data  = '/pool/data/ICDC/ocean/modis_aqua_sst/DATA/daily/2010/'
    path_data  = '/work/mh0033/m300878/observation/satellite/sst/2010/'
    search_str = f'MODIS-AQUA__C6__SST_v2019.0__4km__2010*.nc' 

    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(ds)

def load_satellite_sst_2019():
    path_data  = '/pool/data/ICDC/ocean/modis_aqua_sst/DATA/daily/2019/'
    search_str = f'MODIS-AQUA__C6__SST_v2019.0__4km__2019*.nc' 

    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(ds)

def load_r2b8_sst_mep():
    # data of R2B8 run of Mia, TKE mixing scheme  like smt model, diffrent spinup
    path_data  = '/work/mh0033/m300878/run_icon/icon-oes-1.3.01_mia/experiments/exp.ocean_era51h_r2b8_mep23214-ERA/outdata/'
    search_str = f'exp.ocean_era51h_r2b8_mep23214-ERA_PT1H_to*' 
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()
    r2b8 = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(r2b8.to)

def load_satellite_monthly_sst():
    path_data  = '/pool/data/ICDC/ocean/modis_aqua_sst/DATA/monthly/2010/'
    search_str = f'MODIS-AQUA__C6__SST_v2019.0__4km__2010*.nc' 

    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(ds)

def load_r2b8_sst():
    # data of R2B8 run of Mia, TKE mixing scheme  like smt model, diffrent spinup
    # not available
    # alternative /work/mh0033/m211054/projects/mia/icon-oes-1.3.01_mia/experiments/exp.ocean_era51h_r2b8_jse22126-ERA/outdata

    path_data  = '/work/mh0033/m211054/projects/mia/icon-oes-1.3.01_mia/experiments/exp.ocean_era51h_r2b8_msp22063-ERA/outdata/'
    search_str = f'exp.ocean_era51h_r2b8_msp22063-ERA_3du_P1D_2010*' 
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()
    r2b8 = xr.open_mfdataset(flist[:89], combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    r2b8 = r2b8.rename(ncells_2 = 'ncells')
    return(r2b8.isel(depth=0).to)

def interpolate_to_rect_grid_r2b8(r2b8, lon_reg = [-80, -40], lat_reg = [25, 40]):
    print('interpolate to rectangular grid')
    #fpath_ckdtree = '/work/mh0033/from_Mistral/mh0033/m300602/icon/grids/r2b8_oce_r0004/ckdtree/rectgrids/r2b8_oce_r0004_res0.10_180W-180E_90S-90N.nc'
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b8_oce_r0004/ckdtree/rectgrids/r2b8_oce_r0004_res0.10_180W-180E_90S-90N.nc'
    r2b8_ = pyic.interp_to_rectgrid_xr(r2b8, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) 
    return(r2b8_)

def load_smt_w():
    search_str = f'pp_calc_w_*.nc'
    path_data  = '/work/mh0033/from_Mistral/mh0033/u241317/smt/w/'
    flist1     = np.array(glob.glob(path_data+search_str))
    flist1.sort() 
    path_dat2 = '/work/mh0033/m300878/smt/pp_calc_w2_2010-03-22T21:00:00.nc' #TODO: last timestep is hampered and was recalculated
    flist2    = np.array(glob.glob(path_dat2))
    flist     = [*flist1[:-1], *flist2]
    
    data  = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depthi=1)) # type: ignore
    times = data.time.size
    time0 = np.datetime64('2010-03-15T21:00:00')
    dti   = pd.date_range(time0, periods=times, freq="2h")
    data  = data.assign_coords(time=dti)
    data  = data.rename({'cc': 'ncells'})
    grid  = load_smt_grid()
    data  = data.assign_coords({"clon": ("ncells", grid.clon.data)})
    data  = data.assign_coords({"clat": ("ncells", grid.clat.data)})
    return(data)

def load_smt_v():
    """
    load horizontal velocities from recalculation
    """
    search_str = f'pp_calc_v_*.nc'
    path_data  = '/work/mh0033/m300878/smt/v/'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort() 
    
    data  = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depthc=1)) # type: ignore
    times = data.time.size
    time0 = np.datetime64('2010-03-09T01:00:00')
    dti   = pd.date_range(time0, periods=times, freq="2h")
    data  = data.assign_coords(time=dti)
    # grid  = load_smt_grid()
    # data  = data.assign_coords({"clon": ("ncells", grid.clon.data)})
    # data  = data.assign_coords({"clat": ("ncells", grid.clat.data)})
    return(data)

def load_smt_v_crop_interp():
    """
    load horizontal interpolated and cropped velocities from recalculation
    """
    search_str = f'v_h_crop_interp2010*'
    path_data  = '/work/mh0033/m300878/crop_interpolate/hor_velocities/depthc_3/'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort() 
    
    data  = xr.open_mfdataset(flist, combine='nested', concat_dim='time', chunks=dict(time=1)) # type: ignore
    times = data.time.size
    time0 = np.datetime64('2010-01-09T01:00:00')
    dti   = pd.date_range(time0, periods=times, freq="2h")
    data  = data.assign_coords(time=dti)
    return(data)

def load_smt_b():
    search_str = f'pp_calc_b_*.nc'
    path_data  = '/work/mh0033/from_Mistral/mh0033/u241317/smt/db/'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    data  = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depthi=1)) # type: ignore
    times = data.time.size
    time0 = np.datetime64('2010-03-15T21:00:00')
    dti   = pd.date_range(time0, periods=times, freq="2h")
    data  = data.assign_coords(time=dti)
    data  = data.rename({'cc': 'ncells'})
    # grid  = load_smt_grid()
    # data  = data.assign_coords({"clon": ("ncells", grid.clon.data)})
    # data  = data.assign_coords({"clat": ("ncells", grid.clat.data)})
    return(data)

def load_smt_b_fast():
    search_str = f'pp_calc_b_*.nc'
    path_data  = '/work/mh0033/from_Mistral/mh0033/u241317/smt/db/'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    data  = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depthi=1), decode_cf=False) # type: ignore
    data  = xr.decode_cf(data)
    times = data.time.size
    time0 = np.datetime64('2010-03-15T21:00:00')
    dti   = pd.date_range(time0, periods=times, freq="2h")
    data  = data.assign_coords(time=dti)
    data  = data.rename({'cc': 'ncells'})
    return(data)

def load_smt_b_depthc_no_interp(string):
    search_str = f'pp_calc_{string}_*.nc'
    path_data  = '/work/mh0033/m300878/smt/b/b_depthc/'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    data                  = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depthc=1), decode_cf=False) # type: ignore
    data                  = xr.decode_cf(data)
    times                 = data.time.size
    time0                 = np.datetime64('2010-03-15T21:00:00')
    dti                   = pd.date_range(time0, periods=times, freq="2h")
    data                  = data.assign_coords(time=dti)
    data.coords['depthc'] = depthc
    return(data)

def load_smt_b_depthi_interp(string):
    search_str = f'{string}_*.nc'
    path_data  = '/work/mh0033/m300878/smt/b/b_depthi/'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    data                  = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depthi=1), decode_cf=False) # type: ignore
    data                  = xr.decode_cf(data)
    times                 = data.time.size
    time0                 = np.datetime64('2010-03-15T21:00:00')
    dti                   = pd.date_range(time0, periods=times, freq="2h")
    data                  = data.assign_coords(time=dti)
    data.coords['depthi'] = depthi
    return(data)


def load_smt_b_depthc():
    """interpolated from depthi to depthc"""
    def remove_coords(xda):
        xda = xda.drop_vars('time')
        xda = xda.drop_vars('clon')
        xda = xda.drop_vars('clat')
        return xda

    search_str = f'b_deptc_*.nc'
    path_data  = '/work/mh0033/m300878/smt/b/b_depthc_inaccurate/'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()
    data  = xr.open_mfdataset(flist, preprocess=remove_coords, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depthc=1)) # type: ignore
    times = data.time.size
    time0 = np.datetime64('2010-03-15T21:00:00')
    dti   = pd.date_range(time0, periods=times, freq="2h")
    data  = data.assign_coords(time=dti)
    # grid  = load_smt_grid()
    # data  = data.assign_coords({"clon": ("ncells", grid.clon.data)})
    # data  = data.assign_coords({"clat": ("ncells", grid.clat.data)})
    return(data)

def load_smt_N2():
    search_str = f'pp_calc_N2_*.nc'
    path_data  = '/work/mh0033/from_Mistral/mh0033/u241317/smt/N2/'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    data  = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depthi=1)) # type: ignore
    times = data.time.size
    time0 = np.datetime64('2010-03-15T21:00:00')
    dti   = pd.date_range(time0, periods=times, freq="2h")
    data  = data.assign_coords(time=dti)
    data  = data.rename({'cc': 'ncells'})
    # grid  = load_smt_grid()
    # data  = data.assign_coords({"clon": ("ncells", grid.clon.data)})
    # data  = data.assign_coords({"clat": ("ncells", grid.clat.data)})
    return(data)

def load_smt_sst():
    run        = 'ngSMT_tke'
    search_str = f'_T_S_sp_001-016_*.nc'

    month     = '01'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist1    = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month     = '02'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist2    = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month     = '03'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist3    = np.array(glob.glob(path_data+search_str))
    flist3.sort()

    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    smt = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    smt = smt.assign_coords(time=pd.to_datetime(tools.icon2datetime(smt.time)))
    smt = smt.T001_sp
    return(smt)

def load_smt_T():
    layers = 'sp_001-016', 'sp_017-032', 'sp_033-048', 'sp_049-064', 'sp_065-080', 'sp_081-096', 'sp_097-112'
    tt     = 'a',          'b',          'c',          'd',          'e',          'f',          'g'
    run    = 'ngSMT_tke'

    ii = 0
    for ll in enumerate(layers):
        ii += 1
        search_str = f'_T_S_{ll[1]}_2010*.nc'
        month      = '01'
        path_data  = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        flist1     = np.array(glob.glob(path_data+search_str))
        flist1.sort()
        month = '02'
        #path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        flist2    = np.array(glob.glob(path_data+search_str))
        flist2.sort()
        month     = '03'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        flist3    = np.array(glob.glob(path_data+search_str))
        flist3.sort()

        flist = [*flist1, *flist2, *flist3]
        flist.sort()

        globals()[f"{tt[ii-1]}"] = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))

    to = xr.merge([a,b,c,d,e,f,g]) #type: ignore

    #select only temperature 
    Tlist = np.empty(depthc.size, dtype='object')
    for dd in range(depthc.size):
            level = "{0:03}".format(dd+1) 
            Tlist[dd]= f'T{level}_sp'

    to = to.assign_coords(time=pd.to_datetime(tools.icon2datetime(to.time)))
    # time0 = np.datetime64('2010-01-09T01:00:00')
    # dti = pd.date_range(time0, periods=984, freq="2h")
    # to = to.assign_coords(time=dti)

    ls = [   to[Tlist[0]],  to[Tlist[1]],  to[Tlist[2]],  to[Tlist[3]],  to[Tlist[4]],  to[Tlist[5]],  to[Tlist[6]],  to[Tlist[7]],  to[Tlist[8]],  to[Tlist[9]],
            to[Tlist[10]], to[Tlist[11]], to[Tlist[12]], to[Tlist[13]], to[Tlist[14]], to[Tlist[15]], to[Tlist[16]], to[Tlist[17]], to[Tlist[18]], to[Tlist[19]],
            to[Tlist[20]], to[Tlist[21]], to[Tlist[22]], to[Tlist[23]], to[Tlist[24]], to[Tlist[25]], to[Tlist[26]], to[Tlist[27]], to[Tlist[28]], to[Tlist[29]],
            to[Tlist[30]], to[Tlist[31]], to[Tlist[32]], to[Tlist[33]], to[Tlist[34]], to[Tlist[35]], to[Tlist[36]], to[Tlist[37]], to[Tlist[38]], to[Tlist[39]],
            to[Tlist[40]], to[Tlist[41]], to[Tlist[42]], to[Tlist[43]], to[Tlist[44]], to[Tlist[45]], to[Tlist[46]], to[Tlist[47]], to[Tlist[48]], to[Tlist[49]],
            to[Tlist[50]], to[Tlist[51]], to[Tlist[52]], to[Tlist[53]], to[Tlist[54]], to[Tlist[55]], to[Tlist[56]], to[Tlist[57]], to[Tlist[58]], to[Tlist[59]],
            to[Tlist[60]], to[Tlist[61]], to[Tlist[62]], to[Tlist[63]], to[Tlist[64]], to[Tlist[65]], to[Tlist[66]], to[Tlist[67]], to[Tlist[68]], to[Tlist[69]],
            to[Tlist[70]], to[Tlist[71]], to[Tlist[72]], to[Tlist[73]], to[Tlist[74]], to[Tlist[75]], to[Tlist[76]], to[Tlist[77]], to[Tlist[78]], to[Tlist[79]],
            to[Tlist[80]], to[Tlist[81]], to[Tlist[82]], to[Tlist[83]], to[Tlist[84]], to[Tlist[85]], to[Tlist[86]], to[Tlist[87]], to[Tlist[88]], to[Tlist[89]],
            to[Tlist[90]], to[Tlist[91]], to[Tlist[92]], to[Tlist[93]], to[Tlist[94]], to[Tlist[95]], to[Tlist[96]], to[Tlist[97]], to[Tlist[98]], to[Tlist[99]],
            to[Tlist[100]], to[Tlist[101]], to[Tlist[102]], to[Tlist[103]], to[Tlist[104]], to[Tlist[105]], to[Tlist[106]], to[Tlist[107]], to[Tlist[108]], to[Tlist[109]],
            to[Tlist[110]], to[Tlist[111]], 
     ]
    TO = xr.concat( ls, dim="depthc")
    TO = TO.assign_coords(depthc=depthc)

    ####### String of smt t_s data names
    # Slist = np.empty(depthc.size, dtype='object')
    # for d in range(depthc.size):
    #     level = "{0:03}".format(d+1) 
    #     Slist[d]= f'S{level}_sp'
    # # list on how data is stored
    # # list built for mixed layer depth analysis
    # lis = np.empty(2*depthc.size, dtype='object')#
    # k=0
    # for ii in range(112):
    #     lis[ii+k]   = Tlist[ii]
    #     lis[ii+1+k] = Slist[ii]
    #     k += 1
    # lis

    return TO  #to[tList]

# def load_smt_T_calc(): #function not necessary! same as stored temperature - however data is stored in different shape TODO!
#     search_str = f'pp_calc_T_period*.nc' 
#     path_data = '/work/mh0033/from_Mistral/mh0033/u241317/smt/T/'
#     flist      = np.array(glob.glob(path_data+search_str))
#     flist.sort()

#     data = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
#     times = data.time.size
#     time0 = np.datetime64('2010-03-15T21:00:00')
#     dti = pd.date_range(time0, periods=times, freq="2h")
#     data = data.assign_coords(time=dti)
#     data = data.rename({'temperature': 'ncells'}) 
#     return(data)

def load_smt_S():
    layers  =   'sp_001-016' , 'sp_017-032',  'sp_033-048',  'sp_049-064',  'sp_065-080',  'sp_081-096',  'sp_097-112'
    tt = 'a','b','c','d','e','f','g'
    run      = 'ngSMT_tke'

    ii = 0
    for ll in enumerate(layers):
        ii += 1
        month = '01'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        search_str = f'_T_S_{ll[1]}_2010*.nc' 
        flist1      = np.array(glob.glob(path_data+search_str))
        flist1.sort()
        month = '02'
        #path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        flist2      = np.array(glob.glob(path_data+search_str))
        flist2.sort()
        month = '03'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        flist3      = np.array(glob.glob(path_data+search_str))
        flist3.sort()

        flist = [*flist1, *flist2, *flist3]
        flist.sort()

        globals()[f"{tt[ii-1]}"] = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))

    so = xr.merge([a,b,c,d,e,f,g]) #type: ignore

    #select only salinity
    Slist = np.empty(depthc.size, dtype='object')
    for dd in range(depthc.size):
        level = "{0:03}".format(dd+1) 
        Slist[dd]= f'S{level}_sp'

    so = so.assign_coords(time=pd.to_datetime(tools.icon2datetime(so.time)))
    # time0 = np.datetime64('2010-01-09T01:00:00')
    # dti = pd.date_range(time0, periods=984, freq="2h")
    # so = so.assign_coords(time=dti)

    ls = [   so[Slist[0]],  so[Slist[1]],  so[Slist[2]],  so[Slist[3]],  so[Slist[4]],  so[Slist[5]],  so[Slist[6]],  so[Slist[7]],  so[Slist[8]],  so[Slist[9]],
            so[Slist[10]], so[Slist[11]], so[Slist[12]], so[Slist[13]], so[Slist[14]], so[Slist[15]], so[Slist[16]], so[Slist[17]], so[Slist[18]], so[Slist[19]],
            so[Slist[20]], so[Slist[21]], so[Slist[22]], so[Slist[23]], so[Slist[24]], so[Slist[25]], so[Slist[26]], so[Slist[27]], so[Slist[28]], so[Slist[29]],
            so[Slist[30]], so[Slist[31]], so[Slist[32]], so[Slist[33]], so[Slist[34]], so[Slist[35]], so[Slist[36]], so[Slist[37]], so[Slist[38]], so[Slist[39]],
            so[Slist[40]], so[Slist[41]], so[Slist[42]], so[Slist[43]], so[Slist[44]], so[Slist[45]], so[Slist[46]], so[Slist[47]], so[Slist[48]], so[Slist[49]],
            so[Slist[50]], so[Slist[51]], so[Slist[52]], so[Slist[53]], so[Slist[54]], so[Slist[55]], so[Slist[56]], so[Slist[57]], so[Slist[58]], so[Slist[59]],
            so[Slist[60]], so[Slist[61]], so[Slist[62]], so[Slist[63]], so[Slist[64]], so[Slist[65]], so[Slist[66]], so[Slist[67]], so[Slist[68]], so[Slist[69]],
            so[Slist[70]], so[Slist[71]], so[Slist[72]], so[Slist[73]], so[Slist[74]], so[Slist[75]], so[Slist[76]], so[Slist[77]], so[Slist[78]], so[Slist[79]],
            so[Slist[80]], so[Slist[81]], so[Slist[82]], so[Slist[83]], so[Slist[84]], so[Slist[85]], so[Slist[86]], so[Slist[87]], so[Slist[88]], so[Slist[89]],
            so[Slist[90]], so[Slist[91]], so[Slist[92]], so[Slist[93]], so[Slist[94]], so[Slist[95]], so[Slist[96]], so[Slist[97]], so[Slist[98]], so[Slist[99]],
            so[Slist[100]], so[Slist[101]], so[Slist[102]], so[Slist[103]], so[Slist[104]], so[Slist[105]], so[Slist[106]], so[Slist[107]], so[Slist[108]], so[Slist[109]],
            so[Slist[110]], so[Slist[111]], 
     ]
    SO = xr.concat( ls, dim="depthc")
    SO = SO.assign_coords(depthc=depthc)

    return SO #so[Slist]

def load_smt_vn():
    """Load velocity on edges"""
    layers  =   'sp_001-016' , 'sp_017-032',  'sp_033-048',  'sp_049-064',  'sp_065-080',  'sp_081-096',  'sp_097-112'
    tt = 'a','b','c','d','e','f','g'
    run      = 'ngSMT_tke'

    ii = 0
    for ll in enumerate(layers):
        ii += 1
        month      = '01'
        path_data  = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        search_str = f'_vn_{ll[1]}_2010*.nc'
        flist1     = np.array(glob.glob(path_data+search_str))
        flist1.sort()
        month     = '02'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        flist2    = np.array(glob.glob(path_data+search_str))
        flist2.sort()
        month     = '03'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        flist3    = np.array(glob.glob(path_data+search_str))
        flist3.sort()

        flist = [*flist1, *flist2, *flist3]
        flist.sort()

        globals()[f"{tt[ii-1]}"] = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))

    data = xr.merge([a,b,c,d,e,f,g]) #type: ignore
    #merge single arrays indata matrix
    array_list = np.empty(depthc.size, dtype='object')
    for dd in range(depthc.size):
            level = "{0:03}".format(dd+1) 
            array_list[dd]= f'vn{level}_sp'

    data = data.assign_coords(time=pd.to_datetime(tools.icon2datetime(data.time)))

    ls = [   data[array_list[0]],  data[array_list[1]],  data[array_list[2]],  data[array_list[3]],  data[array_list[4]],  data[array_list[5]],  data[array_list[6]],  data[array_list[7]],  data[array_list[8]],  data[array_list[9]],
            data[array_list[10]], data[array_list[11]], data[array_list[12]], data[array_list[13]], data[array_list[14]], data[array_list[15]], data[array_list[16]], data[array_list[17]], data[array_list[18]], data[array_list[19]],
            data[array_list[20]], data[array_list[21]], data[array_list[22]], data[array_list[23]], data[array_list[24]], data[array_list[25]], data[array_list[26]], data[array_list[27]], data[array_list[28]], data[array_list[29]],
            data[array_list[30]], data[array_list[31]], data[array_list[32]], data[array_list[33]], data[array_list[34]], data[array_list[35]], data[array_list[36]], data[array_list[37]], data[array_list[38]], data[array_list[39]],
            data[array_list[40]], data[array_list[41]], data[array_list[42]], data[array_list[43]], data[array_list[44]], data[array_list[45]], data[array_list[46]], data[array_list[47]], data[array_list[48]], data[array_list[49]],
            data[array_list[50]], data[array_list[51]], data[array_list[52]], data[array_list[53]], data[array_list[54]], data[array_list[55]], data[array_list[56]], data[array_list[57]], data[array_list[58]], data[array_list[59]],
            data[array_list[60]], data[array_list[61]], data[array_list[62]], data[array_list[63]], data[array_list[64]], data[array_list[65]], data[array_list[66]], data[array_list[67]], data[array_list[68]], data[array_list[69]],
            data[array_list[70]], data[array_list[71]], data[array_list[72]], data[array_list[73]], data[array_list[74]], data[array_list[75]], data[array_list[76]], data[array_list[77]], data[array_list[78]], data[array_list[79]],
            data[array_list[80]], data[array_list[81]], data[array_list[82]], data[array_list[83]], data[array_list[84]], data[array_list[85]], data[array_list[86]], data[array_list[87]], data[array_list[88]], data[array_list[89]],
            data[array_list[90]], data[array_list[91]], data[array_list[92]], data[array_list[93]], data[array_list[94]], data[array_list[95]], data[array_list[96]], data[array_list[97]], data[array_list[98]], data[array_list[99]],
            data[array_list[100]], data[array_list[101]], data[array_list[102]], data[array_list[103]], data[array_list[104]], data[array_list[105]], data[array_list[106]], data[array_list[107]], data[array_list[108]], data[array_list[109]],
            data[array_list[110]], data[array_list[111]], 
     ]
    DATA = xr.concat( ls, dim="depthc")
    DATA = DATA.assign_coords(depthc=depthc)

    return DATA

def load_smt_run2_toso():
    path       = '/work/mh0287/m300056/SubmesoNA/intel_omp/experiments'
    run        = 'SubmesoNA_oce_3du200m'
    search_str = f'_toso_*.nc'
    month      = '01'
    path_data  = f'{path}/SubmesoNA_2020-{month}/{run}'
    print(path_data+search_str)
    flist1     = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month     = '02'
    path_data = f'{path}/SubmesoNA_2020-{month}/{run}'
    flist2    = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month     = '03'
    path_data = f'{path}/SubmesoNA_2020-{month}/{run}'
    flist3    = np.array(glob.glob(path_data+search_str))
    flist3.sort()

    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    smt = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depth=1))
    smt = smt.assign_coords(time=pd.to_datetime(tools.icon2datetime(smt.time)))

    # grid = load_smt_grid()
    # smt = smt.assign_coords({"clon": ("ncells", grid.clon.data)})
    # smt = smt.assign_coords({"clat": ("ncells", grid.clat.data)})
    return(smt)



def load_smt_eddy_fluxes():
    path     = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
    ub       = xr.open_dataset(path+'ub_mean.nc', chunks={'depthc': 1})
    vb       = xr.open_dataset(path+'vb_mean.nc', chunks={'depthc': 1})
    wb       = xr.open_dataset(path+'wb_mean.nc', chunks={'depthi': 1})
    ub_prime = xr.open_dataset(path+'ub_prime.nc', chunks={'depthc': 1})
    vb_prime = xr.open_dataset(path+'vb_prime.nc', chunks={'depthc': 1})
    wb_prime = xr.open_dataset(path+'wb_prime.nc', chunks={'depthi': 1})
    u        = xr.open_dataset(path+'u.nc', chunks={'depthc': 1})
    v        = xr.open_dataset(path+'v.nc', chunks={'depthc': 1})
    w        = xr.open_dataset(path+'w.nc', chunks={'depthi': 1})
    vort     = xr.open_dataset(path+'vort_mean.nc')
    dbdx     = xr.open_dataset(path+'bx.nc', chunks={'depthi': 1})
    dbdy     = xr.open_dataset(path+'by.nc', chunks={'depthi': 1})
    to       = xr.open_dataset(path+'t.nc', chunks={'depthc': 1})
    so       = xr.open_dataset(path+'s.nc', chunks={'depthc': 1})
    n2       = xr.open_dataset(path+'n2.nc', chunks={'depthi': 1})

    ds              = xr.Dataset()
    ds['u_mean']    = u.u
    ds['v_mean']    = v.v
    ds['w_mean']    = w.w
    ds['vort_mean'] = vort.vort_f_cells_50m
    ds['dbdx_mean'] = dbdx.dbdx
    ds['dbdy_mean'] = dbdy.dbdy
    ds['to_mean']   = to.T001_sp
    ds['so_mean']   = so.S001_sp
    ds['n2_mean']   = n2.N2
    ds['ub_mean']   = ub.__xarray_dataarray_variable__
    ds['vb_mean']   = vb.__xarray_dataarray_variable__
    ds['wb_mean']   = wb.__xarray_dataarray_variable__
    ds['ub_prime']  = ub_prime.__xarray_dataarray_variable__
    ds['vb_prime']  = vb_prime.__xarray_dataarray_variable__
    ds['wb_prime']  = wb_prime.__xarray_dataarray_variable__
    
    ds              = ds.drop(['clon', 'clat'])
    ds.attrs['time_averaged'] = '1 week'

    return ds


def load_satellite(): #old
    #satellite
    #TODO change to ICDC aviso ssh folder ---- see below
    search_str = f'dt_global_twosat_phy_l4_*.nc' 
    month = '01'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    path_data = f'/work/mh0033/from_Mistral/mh0033/u241317/satellite/ssh/ssh_sat_temp/{month}/'
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month = '02'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    path_data = f'/work/mh0033/from_Mistral/mh0033/u241317/satellite/ssh/ssh_sat_temp/{month}/'
    flist2      = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month = '03'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    path_data = f'/work/mh0033/from_Mistral/mh0033/u241317/satellite/ssh/ssh_sat_temp/{month}/'
    flist3      = np.array(glob.glob(path_data+search_str))
    flist3.sort()

    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(ds)

def load_aviso_aqua(): #updated
    path_data = '/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/01/'
    month = '01'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    search_str = f'dt_global_twosat_phy_l4_*.nc' 
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month = '02'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    search_str = f'dt_global_twosat_phy_l4_*.nc' 
    flist2      = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month = '03'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    search_str = f'dt_global_twosat_phy_l4_*.nc' 
    flist3      = np.array(glob.glob(path_data+search_str))
    flist3.sort()
    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(ds)

def load_smt_ssh():
    run      = 'ngSMT_tke'
    search_str = f'_h_sp_*.nc' 
    month = '01'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month = '02'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist2      = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month = '03'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist3      = np.array(glob.glob(path_data+search_str))
    flist3.sort()

    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    smt = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    smt = smt.assign_coords(time=pd.to_datetime(tools.icon2datetime(smt.time)))
    return(smt)

def load_smt_vorticity():
    run      = 'ngSMT_tke'
    search_str = f'_vort_f_50m*.nc' 
    month = '01'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month = '02'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist2      = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month = '03'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist3      = np.array(glob.glob(path_data+search_str))
    flist3.sort()

    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    smt = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    smt = smt.assign_coords(time=pd.to_datetime(tools.icon2datetime(smt.time)))

    grid = load_smt_grid()
    smt = smt.assign_coords({"clon": ("ncells", grid.clon.data)})
    smt = smt.assign_coords({"clat": ("ncells", grid.clat.data)})
    return(smt)

def load_smt_run2_vorticity():
    path = '/work/mh0287/m300056/SubmesoNA/intel_omp/experiments/'
    run      = 'SubmesoNA_oce_3du200m'
    search_str = f'_vort*.nc' 
    month = '01'
    path_data = f'{path}/SubmesoNA_2020-{month}/{run}'
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month = '02'
    path_data = f'{path}/SubmesoNA_2020-{month}/{run}'
    flist2      = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month = '03'
    path_data = f'{path}/SubmesoNA_2020-{month}/{run}'
    flist3      = np.array(glob.glob(path_data+search_str))
    flist3.sort()

    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    smt = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depth=1))
    smt = smt.assign_coords(time=pd.to_datetime(tools.icon2datetime(smt.time)))

    # grid = load_smt_grid()
    # smt = smt.assign_coords({"clon": ("ncells", grid.clon.data)})
    # smt = smt.assign_coords({"clat": ("ncells", grid.clat.data)})
    return(smt)


def load_smt_wave_quick(variable, exp, t1=800, t2=801, remove_bnds=False):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    if exp == 7: print('exp: Jul-Aug 2019')
    elif exp==8: print('exp: Feb-Mar 2022')
    elif exp==9: print('exp: July 2019; no tides!')

    if exp ==9: folder    = f'smtwv000{exp}'
    else: folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce*.nc'
    if exp == 9: path_data = f'{path}/smtwv000{exp}_oce_3d_{variable}*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    if variable == '2d' or variable == 'forcing':
        chunks = dict(time=1)
    else:
        chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    print('flist size', flist.size)
    flist = flist[t1:t2]

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp
    if remove_bnds==True:
        if 'clon_bnds' in ds:
            ds = ds.drop(['clon_bnds', 'clat_bnds'])
        else:
            print('no bnds to remove')

    return(ds)


def load_smt_wave_all(variable, exp, remove_bnds=False, it=1):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    if exp == 7: print('exp: Jul-Aug 2019')
    elif exp==8: print('exp: Feb-Mar 2022')
    elif exp==9: print('exp: July 2019; no tides!')

    # if exp ==10: folder    = f'smtwv000{exp}'
    folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce*.nc'
    # if exp == 10: path_data = f'{path}/smtwv000{exp}_oce_3d_{variable}*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()
    # print('remove first 6 files, saved in different fashion')
    # if exp==7 or exp ==8: 
    #     flist = flist[6:]
    #     print('flist size', flist.size)

    if variable == '2d' or variable == 'forcing':
        chunks = dict(time=1)
    else:
        chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    # time0 = f'{flist[0]}'[106:117]
    flist = flist[::it]
    # size  = flist.size
    # pd.to_datetime(time0)
    # print('time0', time0)
    # timestring = pd.date_range(time0, periods=size, freq=f"{it}h")
    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp
    # print('override timestamp')
    # ds['time'] = timestring

    if remove_bnds==True:
        if 'clon_bnds' in ds:
            ds = ds.drop(['clon_bnds', 'clat_bnds'])
        else:
            print('no bnds to remove')

    return(ds)



def load_smt_wave_all_ext(variable, exp, remove_bnds=False, it=1):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    if exp == 7: print('exp: Jul-Aug 2019')
    elif exp==8: print('exp: Feb-Mar 2022')
    elif exp==9: print('exp: July 2019; no tides!')

    folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()


    if variable == '2d' or variable == 'forcing':
        chunks = dict(time=1)
    else:
        chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    flist = flist[::it]

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp

    if remove_bnds==True:
        if 'clon_bnds' in ds:
            ds = ds.drop(['clon_bnds', 'clat_bnds'])
        else:
            print('no bnds to remove')

    return(ds)


def load_smt_wave_levels(exp, it=1):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    if exp == 7: print('exp: Jul-Aug 2019')
    elif exp==8: print('exp: Feb-Mar 2022')
    elif exp==9: print('exp: July 2019; no tides!')

    folder    = f'smtwv000{exp}/outdata_lev'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    if it !=1: flist = flist[1::it]

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp

    return(ds)


def load_smt_wave_levels_2(exp, it=1):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''


    folder    = f'smtwv000{exp}/outdata_lev_2'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    if it !=1: flist = flist[1::it]

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp

    return(ds)


def load_smt_wave_9(variable, it=1):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    exp = 9
    if exp == 7: print('exp: Jul-Aug 2019')
    elif exp==8: print('exp: Feb-Mar 2022')
    elif exp==9: print('exp: July 2019; no tides!')

    folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    # path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv0009/'

    print('path to data', path)
    if variable == '2d':
        path_data = f'{path}/smtwv000{exp}_oce_2d_PT1H*.nc'
    else:
        path_data = f'{path}/smtwv000{exp}_oce_3d_{variable}*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    chunks = dict(time=1, depth=1)
    if variable == '2d':
        chunks = dict(time=1)
    print('chunks =', chunks)

    flist = flist[1::it]

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp

    return(ds)


def load_smt_wave_to_exp7_08(remove_bnds=False, it=1):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    variable = 'to'
    exp      = 7
    chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    if exp == 7: print('exp: Jul-Aug 2019')
    folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce_3d_to_PT1H_201908*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    print('flist size', flist.size)

    time0 = f'{flist[0]}'[106:117]
    flist = flist[::it]
    size  = flist.size
    pd.to_datetime(time0)
    print('time0', time0)
    timestring = pd.date_range(time0, periods=size, freq=f"{it}h")
    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp
    print('override timestamp')
    ds['time'] = timestring

    return(ds)

def load_smt_wave_to_exp7():
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    variable = 'to'
    exp      = 7
    chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    if exp == 7: print('exp: Jul-Aug 2019')
    folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce_3d_to_PT1H_2019*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    print('flist size', flist.size)

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp
    return(ds)

def load_smt_wave_to_exp9(it=2):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    variable = 'to'
    exp      = 9
    chunks = dict(time=1, depth=1)
    print('chunks =', chunks)


    folder    = f'smtwv000{exp}/'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce_3d_{variable}_PT1H_2019*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    print('flist size', flist.size)
    flist = flist[1::it]

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp
    return(ds)

def load_smt_wave_so_exp7():
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    variable = 'so'
    exp      = 7
    chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    if exp == 7: print('exp: Jul-Aug 2019')
    folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce_3d_{variable}_PT1H_2019*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    print('flist size', flist.size)

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp
    return(ds)

def load_smt_wave_so_exp9(it=2):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    variable = 'so'
    exp      = 9
    chunks = dict(time=1, depth=1)
    print('chunks =', chunks)


    folder    = f'smtwv000{exp}/'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce_3d_{variable}_PT1H_2019*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    print('flist size', flist.size)
    flist = flist[1::it]

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp
    return(ds)

def load_smt_wave_tke_exp7_08(remove_bnds=False, it=1):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    variable = 'tke'
    exp      = 7
    chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    if exp == 7: print('exp: Jul-Aug 2019')
    folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce_3d_{variable}_PT1H_201908*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    print('flist size', flist.size)

    time0 = f'{flist[0]}'[108:119]
    flist = flist[::it]
    # size  = flist.size
    # pd.to_datetime(time0)
    # print('time0', time0)
    # timestring = pd.date_range(time0, periods=size, freq=f"{it}h")
    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp
    # print('override timestamp')
    # ds['time'] = timestring

    return(ds)

def load_smt_wave_so_exp7_08(it=1):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    variable = 'so'
    exp      = 7
    chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    if exp == 7: print('exp: Jul-Aug 2019')
    folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce_3d_{variable}_PT1H_201908*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    print('flist size', flist.size)

    time0 = f'{flist[0]}'[106:117]
    flist = flist[::it]
    size  = flist.size
    pd.to_datetime(time0)
    print('time0', time0)
    timestring = pd.date_range(time0, periods=size, freq=f"{it}h")
    ds         = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds         = xr.decode_cf(ds) #restores timestamp
    print('override timestamp')
    ds['time'] = timestring

    return(ds)


def load_smt_wave_2d_exp9_07(    it=2):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    print('last file defect')
    exp      = 9
    variable = '2d'
    chunks = dict(time=1)
    print('chunks =', chunks)

    # folder    = f'smtwv000{exp}/outdata_{variable}'
    # path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'

    folder    = f'smtwv000{exp}/'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/{folder}'

    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce_{variable}_PT1H*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()
    print('drop last broken file')
    flist = flist[:-1]
    print('flist size', flist.size)

    flist = flist[::it]
 
    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp


    return(ds)

def load_smt_wave_2d_exp7_07(it=2):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''

    exp=7
    variable='2d'
    if exp == 7: print('exp: Jul-Aug 2019')
    elif exp==8: print('exp: Feb-Mar 2022')
    elif exp==9: print('exp: July 2019; no tides!')

    if exp ==9: folder    = f'smtwv000{exp}'
    else: folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce_{variable}_PT1H_201907*.nc'
    if exp == 9: path_data = f'{path}/smtwv000{exp}_oce_2d_{variable}*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()
    print('remove first 6 files, saved in different fashion')
    flist = flist[6:]
    print('flist size', flist.size)

    if variable == '2d' or variable == 'forcing':
        chunks = dict(time=1)
    else:
        chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    flist = flist[::it]

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp

    return(ds)

def load_smt_wave_2d_exp7_07_part2(it=2):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''

    exp=7
    variable='2d'
    if exp == 7: print('exp: Jul-Aug 2019')
    elif exp==8: print('exp: Feb-Mar 2022')
    elif exp==9: print('exp: July 2019; no tides!')

    if exp ==9: folder    = f'smtwv000{exp}'
    else: folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce*.nc'
    if exp == 9: path_data = f'{path}/smtwv000{exp}_oce_2d_{variable}*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()
    print('remove first 288 files, saved in different fashion')
    # flist = flist[288:1344]
    print('flist size', flist.size)

    if variable == '2d' or variable == 'forcing':
        chunks = dict(time=1)
    else:
        chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    flist = flist[::it]

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp

    return(ds)

def load_smt_wave_2d_exp7_07_part3(it=2):
    exp=7
    variable='2d'

    if exp ==9: folder    = f'smtwv000{exp}'
    else: folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce*.nc'
    if exp == 9: path_data = f'{path}/smtwv000{exp}_oce_2d_{variable}*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    print('flist size', flist.size)

    if variable == '2d' or variable == 'forcing':
        chunks = dict(time=1)
    else:
        chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    flist = flist[::it]

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp

    return(ds)

def load_smt_wave_b():
    '''
    load b from smt data in 2h intervals in first 7 days of august
    '''
    chunks = dict(time=1, depth=1)
    print('chunks =', chunks)
    path =  '/work/bm1239/m300878/smt_data/buoyancy/tides_notides/b_tides/'
    path_data = f'{path}/pp_calc_b_2019-0*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    print('flist size', flist.size)

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks)

    return(ds)

def load_smt_wave_b_notides():
    '''
    load b from smt data in 2h intervals in first 7 days of august
    '''
    chunks = dict(time=1, depth=1)
    print('chunks =', chunks)
    path =  '/work/bm1239/m300878/smt_data/buoyancy/tides_notides/b_notides/'
    path_data = f'{path}/pp_calc_b_2019-0*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    print('flist size', flist.size)

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks)

    return(ds)

def load_wb_variables(tid):
    if tid == 'tides' or tid == 'notides':
        interval = '84H'
        tave     = '2019_07_T31H07_08_T07H05'
    elif tid == '':
        tave     = '2019_07_mean_T05-T12'
        interval = '7D'
    else:
        raise ValueError('strings not valid')

    path_wb = f'/work/bm1239/m300878/smt_data/tave/jul19_mean/{tave}/{tid}/'

    chunks  = {'depth': 1, 'time': 1}
    path    = f'{path_wb}wb/wb_{interval}_mean.nc'
    var     = xr.open_dataset(path, chunks=chunks)
    wb_mean = var.rename({'__xarray_dataarray_variable__': 'wb_mean'})

    path   = f'{path_wb}w/w_{interval}_mean.nc'
    w_mean = xr.open_dataset(path, chunks=chunks)

    path   = f'{path_wb}/b/b_{interval}_mean.nc'
    b_mean = xr.open_dataset(path, chunks=chunks)
    b_mean['depth'] = w_mean.depth

    return wb_mean, w_mean, b_mean, tave

def load_mld_mean(tid):
    beginn   = '2019-07-31-T07:15:00'
    end      = '2019-08-07-T05:15:00'
    if tid == 'tides':   ds_2d    = load_smt_wave_all('2d', 7, remove_bnds=True, it=2)
    if tid == 'notides': ds_2d    = load_smt_wave_9('2d', it=1)
    ds_mld   = ds_2d.mlotst.sel(time=slice(f'{beginn}', f'{end}'))
    mld_mean = ds_mld.mean(dim='time')
    return mld_mean.compute()

def load_smt_grid():
    path = '/home/m/m300602/work/icon/grids/smt/smt_tgrid.nc'
    grid = xr.open_dataset(path)
    return grid

def load_smt_wave_grid():
    path = '/home/m/m300602/work/icon/grids/smtwv_oce_2022/smtwv_oce_2022_tgrid.nc'
    grid = xr.open_dataset(path)
    return grid

def load_r2b9_ssh():
    path_data  = '/work/mh0033/m300878/run_icon/icon-oes-zstar-r2b9/experiments/exp.ocean_era51h_zstar_r2b9_22255-ERA/outdata_2d/'
    search_str = f'exp.ocean_era51h_zstar_r2b9_22255-ERA_oce*'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()
    r2b9 = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    r2b9 = r2b9.zos
    return(r2b9)

def load_r2b9(folder):
    """outdata_2d, outdata_2d_lev_0, outdata_2d_lev_1, outdata_2d_lev_3
    0: to, so, u, v, w my depths
    1: vort tke normal_vort
    3: to, so, u, v, w Florians depths"""
    path_data  = f'/work/mh0033/m300878/run_icon/icon-oes-zstar-r2b9/experiments/exp.ocean_era51h_zstar_r2b9_22255-ERA/{folder}/'
    search_str = f'exp.ocean_era51h_zstar_r2b9_22255-ERA_oce*'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()
    r2b9 = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    r2b9 = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depth=1, depth_2=1))
    return(r2b9)

def compute_density(buoyancy):
    g    = 9.80665
    rho0 = 1025.022
    rho  = rho0 - buoyancy / g * rho0
    return rho

def mconf(K,gamma,str):
    """
    Compute symmetric confidence intervals for multitaper spectral estimation.
    
    Args: 
        K: Number of tapers used in the spectral estimate, normally 2*P-1
        gamma: confidence level, e.g., 0.95
        str: 'lin' to return confidence intervals for linear axis 
             'log' to return confidence intervals for log10 axis 
    
    Returns:
        ra,rb:  Ratio factors for confidence interval
        
    If S0 is the true value of the spectrum and S is the spectral estimate,
    then the confindence interval is defined such that 
    
        Probability that ra < S/S0 < ra = gamma     (linear case)
        Probability that ra < log10(S)/log10(S0) < ra = gamma (log10 case)

    The confidence interval will be S*ra to S*rb for the spectral values
    in linear space, or S*10^ra to S*10^rb in log10 space.  If log10(S) 
    is plotted rather than S with a logarithmic axis, the latter would 
    become log10(S)+ra to log10(S)*rb.    
    """

    dx=0.0001
    #Compute pdf symmetrically about unity

    if str=='lin':
    
        x1=np.arange(1-dx/2,-1,step=-dx)*2*K  #from one to minus one
        x2=np.arange(1+dx/2,3,step=dx)*2*K #from 1 to three 
        fx=(chi2.pdf(x1,2*K)+chi2.pdf(x2,2*K))*2*K
        sumfx=np.cumsum(fx)*dx

        ii=np.where(sumfx>=gamma)[0][0]
        ra=x1[ii]/2/K
        rb=x1[ii]/2/K
    
    elif str=='log':
        
        xo=np.log(2)+np.log(1/2/K)+digamma(K) #see pav15-arxiv
        c=np.log(10)
        xo=xo/c  #change of base rule
    
        x1=np.power(10,np.arange(xo-dx/2,xo-2,step=-dx))   
        x2=np.power(10,np.arange(xo+dx/2,xo+2,step=dx))  
    
        fx1=c*2*K*np.multiply(x1,chi2.pdf(2*K*x1,2*K))
        fx2=c*2*K*np.multiply(x2,chi2.pdf(2*K*x2,2*K))

        sumfx=np.cumsum(fx1+fx2)*dx
    
        ii=np.where(sumfx>=gamma)[0][0]
        ra=np.log10(x1[ii])
        rb=np.log10(x2[ii])
    
    return ra,rb    

def draw_rect(lat, lon, color):
    """Draw rectangle into horizontal image
    Example use:
    rect, right, top = eva.draw_rect(lat_reg_R5_f, lon_reg_R5_f,  color='black')
    ax.add_patch(rect)
    ax.text(right, top, f'R1f', fontsize=fs)
    """
    height = lat[1] - lat[0]
    width = lon[1] - lon[0]
    right = lon[0] + width
    top = lat[0] + height
    rect = patches.Rectangle((lon[0], lat[0]), width, height, linewidth=2, edgecolor=color, facecolor='none', zorder=20)
    return  rect, right, top

def calc_log10_for_slope(x_data, y_data):
    """Remove NaNs from xdata and ydata, zeros from xdata, inf and NaN values after taking log10"""
    mask = ~np.isnan(x_data) & ~np.isnan(y_data) #remove nans from original data
    x_data = x_data[mask]
    y_data = y_data[mask]

    mask = np.ma.masked_where((x_data >= 0), x_data) # remove zeros and negative numbers from xdata
    x_data = x_data[mask.mask]
    y_data = y_data[mask.mask]

    logx = np.log10(np.abs(x_data)) #not nessecary
    logy = np.log10(np.abs(y_data))
    mask = ~np.isnan(logx) & ~np.isnan(logy) & ~np.isinf(logy) #remove inf numbers and nans again after taking log
    logx = logx[mask]
    logy = logy[mask]
    return(logx,logy)

def plot_slope_log_wb_ri(logx, logy, figname, fpath, xlim=None, ylim=None, savefig=False):
    """plot data and slope of logdata wb ~ Ri"""
    fig, ax = plt.subplots(figsize = (10, 5))
    plt.plot(logx, logy , 'ro', markersize=0.5)

    # powerlaw = lambda x, amp, index: amp * (x**index)
    res = stats.linregress(logx, logy)
    plt.plot(logx, res.intercept + res.slope*logx, 'b', label=f'slope={res.slope:.2f}')

    ax.set_xlabel(r'$log_{10} \overline{Ri}$', fontsize=30)
    ax.set_ylabel(r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $', fontsize=30)
    ax.set_title(f'{figname}')
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

    plt.legend(title=f'intercept = {res.intercept:.1}')

    if savefig == True: plt.savefig(f'{fpath}slope_{figname}.png', dpi=150, format='png', bbox_inches='tight')

    return res

def plot_slice_front(x, y, lhs, alpha, wb, ri, mldx, mldx_monte, argo_mld, rho, m2, ylim=None, title=None, fig_path=None, savefig=False):
    """plot vertical parameters of front"""
    fig, ax = plt.subplots(1, 5, figsize=(22,10), sharey=True)
    lw = 4
    color= 'r'
    ls='dashed'
    fs=20
    depth_s = 80
    fig.suptitle(f"{title}", fontsize="x-large")

    i=0
    if lhs.isel(depthi=slice(0,depth_s)).min() > 0: 
        cf = ax[i].pcolormesh(x, y, lhs, vmin=lhs.isel(depthi=slice(0,depth_s)).min(), vmax=lhs.isel(depthi=slice(0,depth_s)).max(),  cmap='OrRd')
    else:
        divnorm=colors.TwoSlopeNorm(vmin=lhs.isel(depthi=slice(0,depth_s)).min(), vcenter=0., vmax=lhs.isel(depthi=slice(0,depth_s)).max())
        cf = ax[i].pcolormesh(x, y, lhs, norm=divnorm,   cmap='RdYlBu_r')
        # cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), lhs.isel(depthi=slice(0,depth_s)), norm=divnorm,   cmap='RdBu_r', extend='max')

    cb = fig.colorbar(cf, ax=ax[i])
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls)
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw)
    ax[i].set_ylim(ylim)
    ax[i].set_title(r'$ \overline{w^{\prime} b^{\prime}}/(H^2 f^3 \alpha^2) $',fontsize=fs)
    ax[i].legend(loc='lower right')

    i=1
    cf = ax[i].pcolormesh(x, y, alpha, cmap='BuPu_r', vmin=alpha.isel(depthi=slice(0,depth_s)).min(), vmax=alpha.isel(depthi=slice(0,depth_s)).max())
    # cf = ax[i].contourf(x, y, alpha, cmap='BuPu', vmin=alpha.isel(depthi=slice(0,depth_s)).min(), vmax=alpha.isel(depthi=slice(0,depth_s)).max())
    cb = fig.colorbar(cf, ax=ax[i])
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls)
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw)
    ax[i].set_ylim(ylim)
    ax[i].set_title(r'$\alpha$',fontsize=fs)

    i=2
    divnorm=colors.TwoSlopeNorm(vmin=wb.min(), vcenter=0., vmax=wb.max())
    # divnorm=colors.TwoSlopeNorm(vmin=wb.min(), vcenter=0., vmax=5e-7)
    cf = ax[i].pcolormesh(x, y, wb,  cmap='RdYlBu_r', norm = divnorm)
    # cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), wb.isel(depthi=slice(0,depth_s)), norm=divnorm,   cmap='RdBu_r', extend='max')
    cb = fig.colorbar(cf, ax=ax[i])
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls)
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw)
    ax[i].set_ylim(ylim)
    ax[i].set_title(r'$ \overline{w^{\prime} b^{\prime}}$',fontsize=fs)
    i=3
    ri10 = np.log10(ri)
    cf = ax[i].contourf(x, y, ri10, cmap='Blues_r', extend='max')
    cb = fig.colorbar(cf, ax=ax[i])
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls)
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw)
    ax[i].set_ylim(ylim)
    ax[i].set_title('log10(Ri)',fontsize=fs)
    i=4
    psi = calc_streamfunc_held_schneider(wb, m2)
    # max = psi.max()
    # if max > 8: max = 6
    divnorm=colors.TwoSlopeNorm(vmin=psi.min(), vcenter=0., vmax=psi.max())
    cf = ax[i].contourf(x, psi.depthi, psi.data, norm=divnorm, cmap='PiYG_r', extend='both')
    # cf = ax[i].pcolormesh(x, psi.depthi, psi.data, vmin=-5, vmax=5, cmap='PiYG_r') # for alongfront

    cb = fig.colorbar(cf, ax=ax[i])
    # levels = np.linspace(1026,1027,100)
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title("$ \psi = - w'b'/M^2 $",fontsize=fs)

    if savefig == True: plt.savefig(fig_path, dpi=150, format='png', bbox_inches='tight')

def plot_slice_front_wide(x, y, lhs, alpha, wb, ri, mldx, mldx_monte, argo_mld, rho, m2, M2x_large, M2y_large, dl, ylim=None, title=None, fig_path=None, savefig=False):
    """plot vertical parameters of front"""
    fig, ax = plt.subplots(4, 1, figsize=(15,22))
    lw = 4
    color= 'r'
    ls='dashed'
    fs=20
    depth_s = 83
    # fig.suptitle(f"{title}", ha='left', fontsize="x-large")

    #i=0
    #clim=-8e-8,8e-8
    #idepth = 20
    #cf = ax[i].pcolormesh(M2x_large.lon, M2x_large.lat, M2x_large.isel(depthi=idepth).data, clim=clim, cmap='PuOr_r')
    #cb = fig.colorbar(cf, ax=ax[i])
    #rect, right, top = draw_rect([M2x_large.lat[0]+dl, M2x_large.lat[-1]-dl], [M2x_large.lon[0]+dl, M2x_large.lon[-1]-dl],  color='black')
    #ax[i].add_patch(rect)
    #ax[i].set_title(rf'$db/dx$ at {depthi[idepth]}m', fontsize=fs)
    
    i=0
    idepth = 20
    clim=-8e-8,8e-8
    cf = ax[i].pcolormesh(M2y_large.lon, M2y_large.lat, M2y_large.isel(depthi=idepth).data, clim=clim, cmap='PuOr_r')
    cb = fig.colorbar(cf, ax=ax[i])
    #ax[i].axhline(M2_large.lat[0]+dl, color='yellow', ls=ls, linewidth=lw)
    #ax[i].axhline(M2_large.lat[-1]-dl, color='yellow', ls=ls, linewidth=lw)
    #ax[i].axvline(M2_large.lon[0]+dl, color='yellow', ls=ls, linewidth=lw)
    #ax[i].axvline(M2_large.lon[-1]-dl, color='yellow', ls=ls, linewidth=lw)
    rect, right, top = draw_rect([M2y_large.lat[0]+dl, M2y_large.lat[-1]-dl], [M2y_large.lon[0]+dl, M2y_large.lon[-1]-dl],  color='black')
    ax[i].add_patch(rect)
    ax[i].set_title(rf'$db/dy$ at {depthi[idepth]}m', fontsize=fs)

    #i=1
    #if lhs.isel(depthi=slice(0,depth_s)).min() > 0: 
    #    cf = ax[i].pcolormesh(x, y, lhs, vmin=lhs.isel(depthi=slice(0,depth_s)).min(), vmax=lhs.isel(depthi=slice(0,depth_s)).max(),  cmap='OrRd')
    #else:
    #    divnorm=colors.TwoSlopeNorm(vmin=lhs.isel(depthi=slice(0,depth_s)).min(), vcenter=0., vmax=lhs.isel(depthi=slice(0,depth_s)).max())
     #   cf = ax[i].pcolormesh(x, y, lhs, norm=divnorm,   cmap='RdYlBu_r')
        # cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), lhs.isel(depthi=slice(0,depth_s)), norm=divnorm,   cmap='RdBu_r', extend='max')

    #cb = fig.colorbar(cf, ax=ax[i])
    #ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    #ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls)
    #ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw)
    #ax[i].axvline(wb.lat[0]+dl, color='black')
    #ax[i].axvline(wb.lat[-1]-dl, color='black')
    #ax[i].set_ylim(ylim)
    #ax[i].set_title(r'$ \overline{w^{\prime} b^{\prime}}/(H^2 f^3 \alpha^2) $',fontsize=fs)
    #ax[i].legend(loc='lower right')

    i=1
    #cf = ax[i].pcolormesh(x, y, alpha, cmap='BuPu_r', vmin=alpha.isel(depthi=slice(0,depth_s)).min(), vmax=alpha.isel(depthi=slice(0,depth_s)).max())
    divnorm=colors.TwoSlopeNorm(vmin=alpha.isel(depthi=slice(0,depth_s)).min(), vcenter=0., vmax=alpha.isel(depthi=slice(0,depth_s)).max())
                       
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), alpha.isel(depthi=slice(0,depth_s)), cmap='PuOr_r', norm=divnorm, levels=19, extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls)
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw)
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r'$\alpha = db/dy ~ /f^2$',fontsize=fs)

    i=2
    #divnorm=colors.TwoSlopeNorm(vmin=-wb.max(), vcenter=0., vmax=wb.max())
    divnorm=colors.TwoSlopeNorm(vmin=wb.min(), vcenter=0., vmax=wb.max())
    #cf = ax[i].pcolormesh(x, y, wb,  cmap='RdBu_r', norm = divnorm)
    #levels = np.linspace(-10,10, 15)*1e-8
    #levels = np.array([-7,-6,-5,-4,-3,-2,-1,1,2,3,4,5,6,7,8])*1e-8
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), wb.isel(depthi=slice(0,depth_s)), norm=divnorm, levels=19,  cmap='RdBu_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls)
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw)
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r'$ \overline{w^{\prime} b^{\prime}}$',fontsize=fs)

    # i=3
    # ri10 = np.log10(ri)
    # cf = ax[i].contourf(x, y, ri10, cmap='Blues_r', extend='max')
    # cb = fig.colorbar(cf, ax=ax[i])
    # ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    # ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls)
    # ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw)
    # ax[i].set_ylim(ylim)
    # ax[i].set_title('log10(Ri)',fontsize=fs)

    i=3
    #lim = 6.0e-9
    #m2 = m2.where(np.abs(m2) > lim, np.nan)
    m2=-np.abs(m2)
    psi = calc_streamfunc_held_schneider(wb, m2)
    #min_psif = psi.where((psi.lat[0]+dl < psi.lat) & (psi.lat[-1]-dl > psi.lat) ).min()
    #max_psif = psi.where((psi.lat[0]+dl < psi.lat) & (psi.lat[-1]-dl > psi.lat) ).max()
    #psi = psi.where((psi.lat[0]+dl < psi.lat) & (psi.lat[-1]-dl > psi.lat) )
    psi = psi.isel(depthi=slice(0,depth_s))
    #print(psi.max(),psi.min())
    
    #if psi.max() < 15:
    #    divnorm=colors.TwoSlopeNorm(vmin=psi.min(), vcenter=0., vmax=psi.max())
    #    cf = ax[i].contourf(x, psi.depthi, psi.data, norm=divnorm, cmap='PiYG_r', extend='both')
    #else:
    #    levels = np.linspace(-10,10, 15)
    #    cf = ax[i].contourf(x, psi.depthi, psi.data, levels, cmap='PiYG_r', extend='both')
        
    levels = np.linspace(-5,5, 15)
    cf = ax[i].contourf(x, psi.depthi, psi.data, levels, cmap='PiYG_r', extend='both')
    
    #divnorm=colors.TwoSlopeNorm(vmin=min_psif, vcenter=0., vmax=max_psif)
    #cf = ax[i].contourf(x, psi.depthi, psi.data, norm=divnorm, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    # levels = np.linspace(1026,1027,100)
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title("$ \psi = - w'b'/~ -abs(db/dy) $",fontsize=fs)
    ax[i].set_xlabel(f"{title}")

    if savefig == True: plt.savefig(fig_path, dpi=150, format='png')

def calc_alpha(m2, f):
    """calcuate alpha:
    same like Rossby number over aspect ratio, Brüggemann 2014"""
    return m2 / np.power(f,2)

def calc_diapycnal_diffusivity(vb, m2, wb, n2, grad_b):
    """calc diapycnal diffusivity, see brüggemann 2014"""
    return - (vb * m2 + wb * n2) / np.power(grad_b,2)

def calc_lhs_vb(vb, mldx, f, alpha):
    """calculate lhs of Ri ~ vb_prime evaluation, Brüggemann 2014"""
    return vb / np.power(mldx,2) / np.power(f,3) / np.power(alpha,3)

def calc_lhs_wb(wb, mldx, f, alpha):
    """calculate lhs of Ri ~ wb_prime evaluation, Brüggemann 2014"""
    return wb / np.power(mldx,2) / np.power(f,3) / np.power(alpha,2)

def calc_lhs_psi(psi, mldx, f, alpha):
    """calculate lhs of Ri ~ streamfunction evaluation, Brüggemann 2014"""
    return psi / np.power(mldx,2) / f / alpha

def calc_lhs_diapycnal_diffusivity(K, mldx, f):
    """calculate lhs of Ri ~ diapycnal diffusivity evaluation, Brüggemann 2014"""
    return K / np.power(mldx,2) / f 

def calc_front_varaibles(wb, ri, mldx, mldx_monte, m2x, m2y, rho, n2, lon, lat, dim=None, ave=False):
    """calculate lhs and rhs and other variables of the front above mld"""
    if ave == False:
        wb         = wb.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        ri         = ri.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        mldx       = mldx.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        mldx_monte = mldx_monte.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        m2x        = m2x.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        m2y        = m2y.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        rho        = rho.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        n2         = n2.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        f          = calc_coriolis_parameter(m2y.lat)

    if ave == True:
        wb         = wb.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        ri         = ri.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        mldx       = mldx.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        mldx_monte = mldx_monte.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        m2x        = m2x.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        m2y        = m2y.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        rho        = rho.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        n2         = n2.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        if dim == 'lat': f = calc_coriolis_parameter((lat[1]-lat[0])/2+lat[0])
        else: f = calc_coriolis_parameter(m2y.lat)
  
    alpha = calc_alpha(m2y, f)
    lhs = calc_lhs_wb(wb, mldx, f, alpha)
    return wb, ri, mldx, mldx_monte, m2x, m2y, alpha, lhs, rho, n2

def calc_front_varaibles_2(wb, ub,  vb, ri, mldx, mldx_monte, m2x, m2y, rho, n2, lon, lat, dim=None, ave=False):
    """calculate lhs and rhs and other variables of the front above mld"""
    if ave == False:
        wb         = wb.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        ub         = ub.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        vb         = vb.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        ri         = ri.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        mldx       = mldx.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        mldx_monte = mldx_monte.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        m2x        = m2x.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        m2y        = m2y.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
    #   m2y_m      = m2y_m.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        rho        = rho.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        n2         = n2.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1]))
        f          = calc_coriolis_parameter(m2y.lat)

    if ave == True:
        wb         = wb.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        ub         = ub.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        vb         = vb.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        ri         = ri.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        mldx       = mldx.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        mldx_monte = mldx_monte.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        m2x        = m2x.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        m2y        = m2y.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        # m2y_m      = m2y_m.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        rho        = rho.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        n2         = n2.sel(lon=slice(lon[0], lon[1])).sel( lat=slice(lat[0], lat[1])).mean(dim=dim, skipna=True)
        if dim == 'lat': f = calc_coriolis_parameter((lat[1]-lat[0])/2+lat[0])
        else:            f = calc_coriolis_parameter(m2y.lat)
  
    alpha = calc_alpha(m2y, f) #type
    lhs   = calc_lhs_wb(wb, mldx, f, alpha)
    return wb, ub, vb, ri, mldx, mldx_monte, m2x, m2y, alpha, lhs, rho, n2

def get_lat_slice(data, lon_slice, lat_point, treshold=0.02):
    """get slice at const latitude, works on rectangular grid, todo: check native grid"""
    slice = data.where((data.lat > lat_point) & (data.lat < lat_point + treshold) & (data.lon < lon_slice[1]) & (data.lon > lon_slice[0]) , drop=True)
    return slice

def get_lon_slice(data, lat_slice, lon_point, treshold=0.02):
    """get slice at const longitude, works on rectangular grid, todo: check native grid"""
    slice = data.where((data.lon > lon_point) & (data.lon < lon_point + treshold) & (data.lat < lat_slice[1]) & (data.lat > lat_slice[0]) , drop=True)
    return slice

def calc_streamfunc_full(wb, m2, vb, n2, abs_grad_b):
    """calc streamfunction, see Brüggemann 2014
    m2 here is the crossfront gradient, vb the crossfront fluctuation"""
    return  (vb * n2 - wb * m2) / np.power(abs_grad_b, 2)

def calc_streamfunc_full_3d(ub, vb, wb, m2x, m2y, n2):
    """calc streamfunction 3D, see Olbers Willebrand"""
    b_grad_abs = calc_abs_grad_b(m2x, m2y, n2)
    c          = np.power(b_grad_abs,-2)
    psi_x   = -c * (vb * n2  - wb * m2y)
    psi_y   = -c * (wb * m2x - ub * n2 )
    psi_z   = -c * (ub * m2y - vb * m2x)

    return  psi_x, psi_y, psi_z

def calc_streamfunc_held_schneider(wb, m2x, m2y):
    """calc streamfunction like Held Schneider:
     here in version of brüggemann, only for zonal fronts, otherwise crossfront gradient should be calculated
     neglects d_z(b) >> vertical eddy flux only, residual flux is in horizontal direction and not in the isopycnal direction, see Brüggemann 2014"""
    return  - wb * m2y / np.power((np.abs(m2x) + np.abs(m2y)),2) # vorzeichen! ToDO

def calc_streamfunc(vb, n2):
    """calc streamfunction traditional, full formulation with M2 = 0"""
    return vb * n2 / np.power(np.abs(n2),2)  

def calc_abs_grad_b(m2x, m2y, n2):
    """calc absolut value of gradient b"""
    return np.sqrt( np.power(m2x,2) + np.power(m2y,2) + np.power(n2,2) )
    # return np.abs(m2x) + np.abs(m2y) + np.abs(n2)
    # return  xr.ufuncs.sqrt(xr.ufuncs.square(m2x) + xr.ufuncs.square(m2y) + xr.ufuncs.square(n2))  # type: ignore

def plot_streamfunc_over_m2(m2, psi, rho, mldx, ylim, figname, fig_path, savefig=False):
    fig, ax = plt.subplots(1, 1, figsize=(8,12))
    cf = ax.pcolormesh(m2.lat, m2.depthi, m2, vmin=0, vmax=8e-8,  cmap='BuPu')
    levels = np.linspace(psi.min(),psi.max(),15)
    plt.contour(psi.lat,psi.depthi, psi.data, levels, colors='yellow')
    plt.plot([], [], 'yellow', label=r"Contour of $ \psi = - <w'b'> / M^2$")

    levels = np.linspace(1026,1027,100)
    plt.contour(rho.lat, rho.depthc, rho.data, levels, colors='gray', label='rho contour')

    plt.plot([], [], 'grey', label="Isopycnal")
    plt.plot(m2.lat, mldx,'r', linewidth=3, label='Mixed Layer depth')

    plt.gca().invert_yaxis()
    cb = fig.colorbar(cf, ax=ax)
    plt.ylim(ylim)
    plt.title(f'{figname}', fontsize=30)
    plt.xlabel('lat', size=20)
    plt.ylabel('depth', size=20)
    plt.legend(loc='lower right', fontsize=20)

    if savefig == True: plt.savefig(f'{fig_path}streamfunc_{figname}_ag22.png', dpi=150, format='png', bbox_inches='tight')

def plot_quick(data, clim=np.array([None,None]),  lon_reg = np.array([-71,-56]), lat_reg = np.array([25,38])):
    """quick glance on data"""
    if clim.any()==None: clim=np.array([data.min(), data.max()])

    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, projection=ccrs_proj, fig_size_fac=2,  axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data  = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, transform=ccrs_proj, rasterized=False, cmap='RdYlBu_r')
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)   # type: ignore

def plot_quick_wave(data , clim = np.array([None,None]), cmap='RdYlBu_r'):
    """quick glance on data"""
    if clim.any()==None: clim=np.array([data.min(), data.max()])
    lon_reg = -45,25
    lat_reg = -40,-20
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, projection=ccrs_proj, fig_size_fac=2,  axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data  = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, transform=ccrs_proj, rasterized=False, cmap=cmap)
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)   # type: ignore



def plot_front_profiles(rho, n2, m2x, m2y, ri, wb, mldx, mldx_monte, argo_mld, fig_path, front, dim):
    print('MLD', mldx.mean().data)
    psi     = calc_streamfunc_held_schneider(wb, m2x, m2y)
    print('psi_max mean mean', np.nanmax(psi.mean(dim=dim).isel(depthi=slice(0,80)).data))
    # print('psi_max cross', np.nanmax(psi.isel(depthi=slice(0,80)).data))
    # a       = psi.where(psi < 6)
    # psi     = a.where(psi > -1.5)
    fig, ax = plt.subplots(1,5, figsize=(20,7), sharey=True, squeeze=True) #16,7 for 5
    fig.suptitle(f'{front} averaged variables', fontsize=25)
    ylim=1500, 0
    lw=3
    fs=30
    fl=15
    i=0
    ax[i].plot(rho.squeeze(), depthc, color='lightgrey')
    ax[i].plot(rho.mean(dim=dim).data, depthc, 'black', lw=lw)
    ax[i].axhline(mldx.mean(), color='r', ls=':', lw=lw, label=f'MLD rho 0.2 (week) {int(mldx.mean().data)}m')
    ax[i].axhline(mldx_monte.mean(), color='purple', ls=':', lw=lw, label=f'MLD rho 0.03 (week) {int(mldx_monte.mean().data)}m')
    ax[i].axhline(argo_mld, color='tab:green', ls=':', lw=lw, label=f'MLD Argo (month) {int(argo_mld)}m')
    ax[i].set_ylim(ylim)  # type: ignore
    R = rho.mean(dim=dim).isel(depthc=slice(0,98))
    ax[i].set_xlim(R.min()-0.1,R.max())
    # if front == 'Front_R4f': ax[i].set_xlim(1025,1028)
    # else:    ax[i].set_xlim(1026,1028)
    ax[i].set_xlabel(fr'$\rho$ (ref {depthc[2]}m)', fontsize=fs)
    ax[i].set_ylabel('depth [m]', fontsize=fs)
    ax[i].legend(loc='lower left', fontsize=11)
    ax[i].grid()
    # axx = plt.gca()
    ax[i].set_xticks(ax[i].get_xticks()[::2])
    ax[i].tick_params(labelsize=fl)
    i=1
    ax[i].plot(n2.squeeze(), depthi, color='lightgrey')
    ax[i].plot(n2.mean(dim=dim).data, depthi, 'black', lw=lw)
    ax[i].axhline(mldx.mean(), color='r', ls=':', lw=lw)
    ax[i].axhline(mldx_monte.mean(), color='purple', ls=':', lw=lw)
    ax[i].axhline(argo_mld, color='tab:green', ls=':', lw=lw)
    ax[i].set_ylim(ylim)  # type: ignore
    ax[i].set_xlim(-1e-5,4e-5)
    ax[i].set_xlabel('$N^2$ ', fontsize=fs)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)
    i=2
    ax[i].plot(m2y.squeeze(), depthc, color='lightgrey')
    ax[i].plot(m2x.mean(dim=dim).data, depthc, ls='dashed', color='black', lw=lw, label='$d_x b$')
    ax[i].plot(m2y.mean(dim=dim).data, depthc, 'black', lw=lw, label='$d_y b$')
    ax[i].axhline(mldx.mean(), color='r', ls=':', lw=lw)
    ax[i].axhline(mldx_monte.mean(), color='purple', ls=':', lw=lw)
    ax[i].axhline(argo_mld, color='tab:green', ls=':', lw=lw)
    ax[i].set_ylim(ylim)  # type: ignore
    ax[i].set_xlim(-7e-8,7e-8)
    ax[i].set_xlabel('$M^2$ ', fontsize=fs)
    ax[i].legend(loc='lower right', fontsize=15)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)
    i=3
    # ax[i].semilogx(ri.squeeze(), depthc, color='lightgrey')
    # ax[i].semilogx(ri.mean(dim=dim).data, depthc, 'black', lw=lw)
    # ax[i].axhline(mldx.mean(), color='r', ls=':')
    # ax[i].axhline(mldx_monte.mean(), color='purple', ls=':')
    # ax[i].axhline(argo_mld, color='tab:green', ls=':')
    # ax[i].set_ylim(ylim)
    # ax[i].set_xlim(1,1e5)
    # ax[i].set_xlabel('Ri ', fontsize=20)
    # ax[i].grid()
    # i=4
    ax[i].plot(wb.squeeze(), depthc, color='lightgrey')
    ax[i].plot(wb.mean(dim=dim).data, depthc, 'black', lw=lw)
    ax[i].axhline(mldx.mean(), color='r', ls=':', lw=lw)
    ax[i].axhline(mldx_monte.mean(), color='purple', ls=':', lw=lw)
    ax[i].axhline(argo_mld, color='tab:green', ls=':', lw=lw)
    ax[i].set_ylim(ylim)  # type: ignore
    ax[i].set_xlim(-0.5e-7,2e-7)
    ax[i].set_xlabel("$w^/ b^/$ ", fontsize=fs)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)
    i=4
    ax[i].plot(psi.squeeze(), depthc, color='lightgrey')
    ax[i].plot(psi.mean(dim=dim).data, depthc, 'black', lw=lw)
    ax[i].axhline(mldx.mean(), color='r', ls=':', lw=lw)
    ax[i].axhline(mldx_monte.mean(), color='purple', ls=':', lw=lw)
    ax[i].axhline(argo_mld, color='tab:green', ls=':', lw=lw)
    ax[i].set_ylim(ylim)  # type: ignore
    ax[i].set_xlim(-1,5)
    ax[i].set_xlabel("$ \psi = - w'b'/M^2 $", fontsize=fs)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)

    plt.savefig(f'{fig_path}front_{front}_averaged_var_ag22.png', dpi=150, format='png', bbox_inches='tight')
    # plt.savefig(f'{fig_path}alongfront_{front}_averaged_inst_ag22.png', dpi=150, format='png', bbox_inches='tight')



def calc_triangulation_obj(Data, lon_reg, lat_reg):
    """create triangulation object for highres plot
    Example:
    pyic.shade(Tri, data_reg...)
    Todo: make it a full xarray calculation
    """
    print('Deriving triangulation object, this can take a while...')
    f = Dataset('/home/m/m300602/work/icon/grids/smt/smt_tgrid.nc', 'r')
    clon = f.variables['clon'][:] * 180./np.pi
    clat = f.variables['clat'][:] * 180./np.pi
    vlon = f.variables['vlon'][:] * 180./np.pi
    vlat = f.variables['vlat'][:] * 180./np.pi
    vertex_of_cell = f.variables['vertex_of_cell'][:].transpose()-1
    f.close()

    ind_reg = np.where(   (clon>lon_reg[0])
                        & (clon<=lon_reg[1])
                        & (clat>lat_reg[0])
                        & (clat<=lat_reg[1]) )[0]
    vertex_of_cell_reg = vertex_of_cell[ind_reg,:]
    Tri = matplotlib.tri.Triangulation(vlon, vlat, triangles=vertex_of_cell_reg)
    data_reg = Data.compute().data[ind_reg]
    print('Done deriving triangulation object.')
    return Tri, data_reg

def calc_triangulation_obj_wave(Data, lon_reg, lat_reg):
    """create triangulation object for highres plot
    Example:
    pyic.shade(Tri, data_reg...)
    Todo: make it a full xarray calculation
    """ 
    print('Deriving triangulation object, this can take a while...')
    f = Dataset('/home/m/m300602/work/icon/grids/smtwv_oce_2022/smtwv_oce_2022_tgrid.nc', 'r')
    clon = f.variables['clon'][:] * 180./np.pi
    clat = f.variables['clat'][:] * 180./np.pi
    vlon = f.variables['vlon'][:] * 180./np.pi
    vlat = f.variables['vlat'][:] * 180./np.pi
    vertex_of_cell = f.variables['vertex_of_cell'][:].transpose()-1
    f.close()

    ind_reg = np.where(   (clon>lon_reg[0])
                        & (clon<=lon_reg[1])
                        & (clat>lat_reg[0])
                        & (clat<=lat_reg[1]) )[0]
    vertex_of_cell_reg = vertex_of_cell[ind_reg,:]
    Tri = matplotlib.tri.Triangulation(vlon, vlat, triangles=vertex_of_cell_reg)
    data_reg = Data.compute().data[ind_reg]
    print('Done deriving triangulation object.')
    return Tri, data_reg

def calc_triangulation_obj_wave_vertices(Data, ds_tg, lon_reg, lat_reg):
    """create triangulation object for highres plot
    Example:
    pyic.shade(Tri, data_reg...)
    """
    print('Deriving triangulation object, this can take a while...')

    clon = ds_tg.clon.compute().data * 180./np.pi
    clat = ds_tg.clat.compute().data * 180./np.pi
    ireg_c = np.where(
        (clon>lon_reg[0]) & (clon<=lon_reg[1]) & (clat>lat_reg[0]) & (clat<=lat_reg[1])
    )[0]
    ds_tg_cut = pyic.xr_crop_tgrid(ds_tg, ireg_c)
    ireg_v = ds_tg_cut['ireg_v'].data
    
    clon_bnds, clat_bnds, vlon_bnds, vlat_bnds, cells_of_vertex = pyic.patch_plot_derive_bnds(ds_tg_cut)
    patches_c, patches_v = pyic.patch_plot_patches_from_bnds(
        clon_bnds.compute(), clat_bnds.compute(), 
        vlon_bnds.compute(), vlat_bnds.compute(), 
        cells_of_vertex#.compute()
    )

    return patches_v, Data[ireg_v]

def load_argo_climatology():
    path_data = '/work/mh0033/from_Mistral/mh0033/u241317/argo/Argo_mixedlayers_monthlyclim_03172021.nc'
    da = xr.open_dataset(path_data)
    return da

def plot_argo_mld(data, lon_reg, lat_reg, contfs=None):
    """plot monthly mld climatology of Holte
    Example:
    contfs = np.arange(50.,550.,100.)
    """
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    months = 'January','February','March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
    clim = [contfs.min(), contfs.max()]
 
    hca, hcb = pyic.arrange_axes(3, 4, plot_cb='right', asp=asp, fig_size_fac=4, sharex=True, sharey=True, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    for tt in np.arange(12):
        ii+=1; ax=hca[ii]; cax=hcb[ii]
        pyic.shade(data.longrid, data.latgrid, data.mld_da_mean.isel(iMONTH=tt), ax=ax, cax=cax, contfs=contfs, clim = clim, transform=ccrs_proj, rasterized=False)
        ax.set_title(f'Argo MLD, {months[tt]} 2010', fontsize=15)
        # ax.set_ylabel('lat')
        # if tt > 8:  ax.set_xlabel('lon')

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.tick_params(labelsize=15)
        cax.tick_params(labelsize=15)

def mean_help(i):
    """Helper func for lon lat mean"""
    return  (i[0]+(i[1]-i[0])/2)

def calc_psi_fox_fox(lat_mean, H, M2, z=0, structure=False):
    """calc parameterization streamfunction of Fox Kemper 2008, from Fox kemper """
    Ce = 0.06 #0.06
    if structure == False: mu = 1
    else: mu = ( 1 - np.power((2*z/H +1 ),2) ) * ( 1 + 5 / 21 * np.power((2*z/H +1),2) )
    f = calc_coriolis_parameter(lat_mean)
    psi_fox = Ce * H * H * M2 * mu / np.abs(f)
    return psi_fox

def calc_psi_stone_fox(lat_mean, H, M2, Ri, Cs, z=0, structure=False):
    """calc parameterization streamfunction of Stone 1972b in Version of Killworth,
     here in the version fox kemper with alpha= M2/f^2 and Held Schneider Streamfunc psi= wb/m2, note the different sign of wb """
    # Cs = 1.1#0.09
    if structure == False: mu = 1
    else: mu = 1 - np.power((2*z/H +1),2)
    f = calc_coriolis_parameter(lat_mean)
    psi = Cs * H * H * M2 * mu / np.abs(f) * 1 / np.sqrt(1+Ri)
    return psi

# def calc_psi_fox_brueggemann(lat_mean, H, M2, z=0, Cf=0.06, structure=False):
#     """calc parameterization streamfunction of Fox Kemper in version of Brüggemann """
#     if structure == False: mu = 1
#     else: mu = - 4 * z / H * ( z / H +1) * (1+ np.power((5/21*(2*z/H+1)), 2))
#     f       = calc_coriolis_parameter(lat_mean)
#     psi_fox = - Cf * H * H * M2 * mu / np.abs(f)
#     return psi_fox

def calc_wb_fox_param(lat_mean, H, M2, z, Cf=0.06, structure=False):
    """calc of Fox Kemper parameterization for wb  (version: Brüggemann et al. 2014) """
    if structure == False: mu = 1 #only for const. vertical fronts no z depedense of m2
    else: mu = calc_mu_fox_kemper(H,z)
    f     = calc_coriolis_parameter(lat_mean)
    alpha = calc_alpha(M2,f)
    wb    = Cf * mu * np.power(alpha,2) * H * H * np.power(f,3) 
    return wb

def calc_vb_fox_param(lat_mean, H, M2, Ri, z, Cf=0.06, structure=False):
    """calc parameterization streamfunction of Fox Kemper in version of Brüggemann """
    if structure == False: mu = 1
    else: mu = calc_mu_fox_kemper(H,z) #(1-(2*z/H)**2) * (1+5/21*(2*z/H+1)**2)#mu = - 4 * z / H * ( z / H +1) * (1+ np.power((5/21*(2*z/H+1)), 2))
    f     = calc_coriolis_parameter(lat_mean)
    alpha = calc_alpha(M2,f)
    vb    = - 2 * Cf * mu * Ri * np.power(alpha,3) * H * H * np.power(f,3) 
    return vb

def calc_mu_fox_kemper(H,z):
    """calc mu of Fox Kemper"""
    # mu = (1-(2*z/H+1)**2) * (1+5/21*(2*z/H+1)**2) #like FK
    mu = -4 * z/H * (z/H +1) *  (1+5/21* (2*z/H +1)**2 )
    return mu

def calc_mu_stone(H,z):
    """calc mu of Stone"""
    mu = -4 * z / H * ( z / H +1)
    return mu

def calc_streamfunc_full_3d_fox(lat_mean, H, m2x, m2y, z, Cf=0.06, structure=False):
    if structure == False: mu = 1
    else: mu = - 4 * z / H * ( z / H +1) * (1+ np.power((5/21*(2*z/H+1)), 2))
    f     = calc_coriolis_parameter(lat_mean)
    psi_x =   Cf * mu * H * H / np.abs(f) *    m2y
    psi_y =   Cf * mu * H * H / np.abs(f) * (- m2x)
    return psi_x, psi_y

# def calc_psi_stone_brueggemann(lat_mean, H, M2, Ri, Cs, z=0, structure=False):
#     """calc parameterization streamfunction of Stone 1972b in Version of Killworth,
#      here in the version brüggeman with alpha= M2/f^2 and Held Schneider Streamfunc psi=-wb/m2 """
#     # Cs = 0.3
#     if structure == False: mu = 1
#     else: mu = -4 * z / H * ( z / H +1)
#     f   = calc_coriolis_parameter(lat_mean)
#     psi = - Cs * H * H * M2 * mu / np.abs(f) * 1 / np.sqrt(1+Ri)
#     return psi

# def calc_wb_stone_param(lat_mean, H, M2, Ri, Cs, z):
#     """calc parameterization streamfunction of Stone 1972b / Killworth,
#      here in the version brüggeman with alpha= M2/f^2 and Held Schneider Streamfunc psi=-wb/m2 """
#     mu    = -4 * z / H * ( z / H +1)
#     f     = calc_coriolis_parameter(lat_mean)
#     alpha = calc_alpha(M2,f)
#     wb    = Cs * 1 / np.sqrt(1+Ri) * mu * H * H * np.power(alpha,2)  * np.power(f,3) 
#     return wb

def calc_wb_stone_param(lat_mean, H, M2, Ri, Cs, z, structure):
    """calc parameterization streamfunction of Stone 1972b / Killworth,
     here in the version brüggeman with alpha= M2/f^2 and Held Schneider Streamfunc psi=-wb/m2 """
    if structure == False: mu = 1
    else: mu = calc_mu_stone(H,z)
    f     = calc_coriolis_parameter(lat_mean)
    alpha = calc_alpha(M2,f)
    wb    = Cs * 1 / np.sqrt(1+Ri) * mu * H * H * np.power(alpha,2)  * np.power(f,3) 
    return wb

def calc_vb_stone_param(lat_mean, H, M2, Ri, Cs):
    """calc parameterization streamfunction of Stone 1972b / Killworth,
     here in the version brüggeman with alpha= M2/f^2 and Held Schneider Streamfunc psi=-wb/m2 """
    f     = calc_coriolis_parameter(lat_mean)
    alpha = calc_alpha(M2,f)
    vb    = - 8/5 * Cs * np.sqrt(1+Ri) * H * H * np.power(alpha,3)  * np.power(f,3)
    return vb

def calc_cross_product(a_x, a_y, a_z, b_x, b_y, b_z):
    c_x =   a_y * b_z - a_z * b_y
    c_y = - a_x * b_z + a_z * b_x
    c_z =   a_x * b_y - a_y * b_x
    return c_x, c_y, c_z

def get_points_of_inclined_fronts():
    points = np.array([
       [-63.52200227,  35.00133776],
       [-61.74780872,  35.27190053],
       [-63.51034652,  32.74462865],
       [-61.91357233,  31.47839489],
       [-62.18978201,  32.85642603],
       [-60.69583039,  33.02282214],
       [-64.39704952,  31.83672991],
       [-63.40410597,  31.5228771 ],
       [-63.82749307,  34.095278  ],
       [-63.48475113,  33.52168493],
       [-60.4349622 ,  30.20891437],
       [-59.64463962,  29.85718277],
       [-58.35431704,  29.75436891],
       [-57.45915575,  29.78142519],
       [-59.9599433 ,  32.77168493],
       [-58.78050781,  32.06822173],
       [-62.43776588,  28.67861134],
       [-61.18574975,  28.60826502],
       [-60.02612903,  28.10220272],
       [-59.28419355,  27.65577415],
       [-66.28419355,  34.50804687],
       [-65.00193548,  34.012917  ],
       [-60.95354839,  35.80674817],
       [-59.92935484,  35.43337155],
       [-59.76806452,  35.5551248 ],
       [-58.95354839,  35.68499493],
       [-58.61483871,  35.36843649],
       [-58.25193548,  34.65415077],
       [-68.15004455,  26.73052907],
       [-67.85770584,  27.87771522],
       [-68.26093165,  26.29762864],
       [-66.86980262,  26.48161132],
       [-67.09157681,  28.64611348],
       [-66.67827036,  27.97511781],
       [-66.44641552,  27.73702258],
       [-65.62988326,  28.0616979 ],
       [-64.82343165,  28.91667626],
       [-64.53109294,  28.00758535],
       [-64.55125423,  27.98594032],
       [-63.66415745,  27.49892734],
       [-64.58149616,  26.30845115],
       [-63.54318971,  25.72403556],
       [-62.77966636,  27.81278015], 
       [-61.70305346,  27.20671955],
       [-59.11434378,  26.21104855],
       [-58.0740212 ,  26.11364595],
       [-63.97724701,  26.00542084],
       [-63.16676314,  25.42100526],
       [-63.98934378,  25.24784509],
       [-63.09418249,  25.02057236],
       [-56.92532258,  28.28445211],
       [-56.24991935,  27.65674648],
       [-56.24991935,  27.65674648],
       [-55.58459677,  27.60263393],
       [-55.58459677,  27.60263393],
       [-55.13096774,  26.99657332],
       [-57.87290323,  28.91215774],
       [-57.09669355,  28.5766599 ],
       [-58.10475806,  27.06150839],
       [-57.60072581,  26.73683306],
       [-58.9616129 ,  28.52254735],
       [-58.18540323,  28.63077246],
       [-59.5866129 ,  26.96410579],
       [-59.02209677,  26.81259064],
       [-58.39709677,  27.93813176],
       [-57.88298387,  27.29960363],
       [-57.96735082,  33.95886109],
       [-56.91896373,  33.62065763],
       [-56.49557663,  32.26784378],
       [-55.66896373,  31.38851477],
       [-55.90081857,  30.98267062],
       [-55.06412502,  30.71210785],
       [-57.01977018,  33.75593902],
       [-56.67702824,  32.99836326],
       [-58.52178631,  33.40420741],
       [-58.08831857,  32.60604724],
       [-57.36251211,  29.48104724],
       [-56.09235082,  29.68396932],
       [-62.10975163,  25.44360187],
       [-61.28717098,  24.75018057],
       [-61.14200969,  24.40927148],
       [-59.98071937,  24.60407668],
       [-59.81942905,  25.13167408],
       [-59.11781614,  24.41738837],
       [-60.86781614,  23.90602473],
       [-59.11781614,  24.04401174],
       [-63.73900083,  30.76779017],
       [-62.41238793,  29.88034429],
       [-65.06561373,  29.71800662],
       [-64.09182341,  29.71700662]])
    return points

def calc_line(point):
    m = (point[0,1] - point[1,1])/(point[0,0]-point[1,0])
    b = point[1,1] - m * point[1,0] 
    x = np.linspace(point[0,0],point[1,0],100)
    y = m * x + b
    return y, x, m, b

def get_eddies():
    """new Selected eddies around 16th of March"""
    lon_reg_R1_f  = np.array([-63.7,-61.4])
    lat_reg_R1_f  = np.array(  [34.9,35.4])
    lon_reg_R2_f  = np.array([-65,-63.5  ])
    lat_reg_R2_f  = np.array(  [36, 38])
    lon_reg_R3_f  = np.array([-62.75,-61.75])
    lat_reg_R3_f  = np.array(  [35.75,36.5])
    lon_reg_R4_f  = np.array([-67,-65.5])+0.2
    lat_reg_R4_f  = np.array( [25.25,26.75])
    lon_reg_R5_f  = np.array([-73.5,-71.75])
    lat_reg_R5_f  = np.array(  [30.5,32])
    lon_reg_R6_f  = np.array([-72, -69.25])
    lat_reg_R6_f  = np.array([33.5, 36])
    lon_reg_R7_f  = np.array([-65, -63.75])
    lat_reg_R7_f  = np.array([29.75, 31])
    lon_reg_R8_f  = np.array([-68,-66.75])+0.2
    lat_reg_R8_f  = np.array( [29.5, 30.5])+0.1
    lon_reg_R9_f  = np.array([-68.2,-66.5])-0.5
    lat_reg_R9_f  = np.array(  [31,32.1])
    lon_reg_R10_f = np.array([-65, -63.75])+2.2
    lat_reg_R10_f = np.array([30.5, 31.3])
    lon_reg_R11_f = np.array([-68,-66.75])+1.6
    lat_reg_R11_f = np.array( [29.6, 30.6])+0.1
    # lon_reg_R12_f = np.array([-64.4,-63.3])+0.4
    # lat_reg_R12_f = np.array(  [30.9,32.7])

    lon_reg_all = np.array([lon_reg_R1_f,
                           lon_reg_R2_f,
                           lon_reg_R3_f,
                           lon_reg_R4_f,
                           lon_reg_R5_f,
                           lon_reg_R6_f,
                           lon_reg_R7_f,
                           lon_reg_R8_f,
                           lon_reg_R9_f,
                           lon_reg_R10_f,
                           lon_reg_R11_f
                        #    lon_reg_R12_f
                            ])

    lat_reg_all = np.array([lat_reg_R1_f,
                           lat_reg_R2_f,
                           lat_reg_R3_f,
                           lat_reg_R4_f,
                           lat_reg_R5_f,
                           lat_reg_R6_f,
                           lat_reg_R7_f,
                           lat_reg_R8_f,
                           lat_reg_R9_f,
                           lat_reg_R10_f,
                           lat_reg_R11_f
                        #    lat_reg_R12_f#])
                            ])
    
    eddies = lon_reg_all, lat_reg_all
    
    e0 = np.array([eddies[0][0], eddies[1][0]]).T
    e2 = np.array([eddies[0][1], eddies[1][1]]).T
    e3 = np.array([eddies[0][2], eddies[1][2]]).T
    e4 = np.array([eddies[0][3], eddies[1][3]]).T
    e5 = np.array([eddies[0][4], eddies[1][4]]).T
    e6 = np.array([eddies[0][5], eddies[1][5]]).T
    e7 = np.array([eddies[0][6], eddies[1][6]]).T
    e8 = np.array([eddies[0][7], eddies[1][7]]).T
    e9 = np.array([eddies[0][8], eddies[1][8]]).T
    e10 = np.array([eddies[0][9], eddies[1][9]]).T
    e11 = np.array([eddies[0][10], eddies[1][10]]).T
    # e12 = np.array([eddies[0][11], eddies[1][11]]).T

    points = np.array([e0, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11])
    points = points.reshape(points.shape[0]*points.shape[1], points.shape[2])

    x = points.T[0,:]
    y = points.T[1,:]

    # take a meridional slice
    x_new = np.zeros(x.shape)
    i = 0
    for ii in np.arange(11):
        print(ii, i)
        ave = (x[i] + x[i+1])/2
        x_new[i]   = ave -0.08
        x_new[i+1] = ave -0.08
        i = i+2
    points = np.array([x_new, y]).T

    #take a zonal slice
    # y_new = np.zeros(y.shape)
    # i = 0
    # for ii in np.arange(8):
    #     print(ii, i)
    #     ave = (y[i] + y[i+1])/2
    #     y_new[i]   = ave 
    #     y_new[i+1] = ave 
    #     i = i+2
    # points = np.array([x, y_new]).T

    return points


def get_new_fronts():
    """new Selected fronts around 16th of March"""
    lon_reg_R1_f  = np.array([-63.7,-61.4])
    lat_reg_R1_f  = np.array(  [34.9,35.4])
    # lon_reg_R2_f  = np.array([-65.7,-65.3]) #v'b' negative in mld strong gradient in x too
    # lat_reg_R2_f  = np.array(  [34.05,34.5])
    lon_reg_R3_f  = np.array([-62.7,-62.4])
    lat_reg_R3_f  = np.array(  [31.7,32.2])
    lon_reg_R4_f  = np.array([-76.6,-74.6])
    lat_reg_R4_f  = np.array(  [26.7,27.3])
    lon_reg_R5_f  = np.array([-60.5,-58.9])
    lat_reg_R5_f  = np.array(  [35.4,35.8])
    lon_reg_R6_f  = np.array([-61.1,-60.65])
    lat_reg_R6_f  = np.array([37.1, 37.8])
    lon_reg_R7_f  = np.array([-58.4,-57.2])
    lat_reg_R7_f  = np.array(    [29.6,30])
    lon_reg_R8_f  = np.array([-62.5,-61])
    lat_reg_R8_f  = np.array( [28.4, 28.9])
    # lon_reg_R9_f  = np.array([-68.2,-66.5]) #v'b' negative in mld and u'b' mauch larger then v'b'
    # lat_reg_R9_f  = np.array(  [30.8,31.5])
    lon_reg_R10_f = np.array([-70.75,-69.5])
    lat_reg_R10_f = np.array(  [29.8,30.2])
    lon_reg_R11_f = np.array([-69.5,-68.3])
    lat_reg_R11_f = np.array(  [29.4,29.7])
    lon_reg_R12_f = np.array([-64.4,-63.3])
    lat_reg_R12_f = np.array(  [31.4,31.9])

    lon_reg_all = np.array([lon_reg_R1_f,
                        #    lon_reg_R2_f,
                           lon_reg_R3_f,
                           lon_reg_R4_f,
                           lon_reg_R5_f,
                           lon_reg_R6_f,
                           lon_reg_R7_f,
                           lon_reg_R8_f,
                        #    lon_reg_R9_f,
                           lon_reg_R10_f,
                           lon_reg_R11_f,
                           lon_reg_R12_f])

    lat_reg_all = np.array([lat_reg_R1_f,
                        #    lat_reg_R2_f,
                           lat_reg_R3_f,
                           lat_reg_R4_f,
                           lat_reg_R5_f,
                           lat_reg_R6_f,
                           lat_reg_R7_f,
                           lat_reg_R8_f,
                        #    lat_reg_R9_f,
                           lat_reg_R10_f,
                           lat_reg_R11_f,
                           lat_reg_R12_f])
    
    return lon_reg_all, lat_reg_all


def get_fronts():
    """Selected fronts around 16th of March"""
    lon_reg_R1_f  = np.array([-63.7,-61.4])
    lat_reg_R1_f  = np.array(  [34.9,35.4])
    lon_reg_R2_f  = np.array([-65.7,-63.6]) 
    lat_reg_R2_f  = np.array(  [33.8,34.2])
    lon_reg_R3_f  = np.array([-62.7,-60.6])
    lat_reg_R3_f  = np.array(  [32.7,33.1])
    lon_reg_R4_f  = np.array([-76.6,-74.6])
    lat_reg_R4_f  = np.array(  [26.7,27.3])
    lon_reg_R5_f  = np.array([-60.5,-58.9])
    lat_reg_R5_f  = np.array(  [35.4,35.8])
    lon_reg_R6_f  = np.array( [-63.25,-61])
    lat_reg_R6_f  = np.array([36.4, 36.95])
    lon_reg_R7_f  = np.array([-58.4,-57.2])
    lat_reg_R7_f  = np.array(    [29.6,30])
    lon_reg_R8_f  = np.array([-62.7,-60.6])
    lat_reg_R8_f  = np.array( [28.4, 28.9])
    lon_reg_R9_f  = np.array([-68.3,-65.5])
    lat_reg_R9_f  = np.array(  [30.9,31.4])
    lon_reg_R10_f = np.array([-70.9,-69.5])
    lat_reg_R10_f = np.array(  [29.8,30.2])
    lon_reg_R11_f = np.array([-69.9,-68.3])
    lat_reg_R11_f = np.array(  [29.3,29.7])
    lon_reg_R12_f = np.array([-64.7,-63.3])
    lat_reg_R12_f = np.array(  [31.4,31.9])

    lon_reg_all = np.array([lon_reg_R1_f,
                           lon_reg_R2_f,
                           lon_reg_R3_f,
                           lon_reg_R4_f,
                           lon_reg_R5_f,
                           lon_reg_R6_f,
                           lon_reg_R7_f,
                           lon_reg_R8_f,
                           lon_reg_R9_f,
                           lon_reg_R10_f,
                           lon_reg_R11_f,
                           lon_reg_R12_f])

    lat_reg_all = np.array([lat_reg_R1_f,
                           lat_reg_R2_f,
                           lat_reg_R3_f,
                           lat_reg_R4_f,
                           lat_reg_R5_f,
                           lat_reg_R6_f,
                           lat_reg_R7_f,
                           lat_reg_R8_f,
                           lat_reg_R9_f,
                           lat_reg_R10_f,
                           lat_reg_R11_f,
                           lat_reg_R12_f])
    
    return lon_reg_all, lat_reg_all


def add_string_like_pyic(hca, figstr, posx=[-0.00], posy=[1.05], fontdict=None):
    for nn, ax in enumerate(hca):
        ht = hca[nn].text(posx[0], posy[0], figstr[nn], 
                      transform = hca[nn].transAxes, 
                      horizontalalignment = 'right',
                      fontdict=fontdict)


def get_smt_wave_depth():
    depth = np.array([0.0000e+00, 2.0000e+00, 4.1000e+00, 6.3000e+00, 8.6000e+00, 1.1000e+01,
       1.3500e+01, 1.6100e+01, 1.8800e+01, 2.1600e+01, 2.4600e+01, 2.7700e+01,
       3.0900e+01, 3.4300e+01, 3.7800e+01, 4.1500e+01, 4.5400e+01, 4.9400e+01,
       5.3600e+01, 5.8000e+01, 6.2600e+01, 6.7400e+01, 7.2400e+01, 7.7700e+01,
       8.3200e+01, 8.9000e+01, 9.5000e+01, 1.0130e+02, 1.0790e+02, 1.1480e+02,
       1.2200e+02, 1.2950e+02, 1.3730e+02, 1.4550e+02, 1.5400e+02, 1.6290e+02,
       1.7220e+02, 1.8200e+02, 1.9220e+02, 2.0290e+02, 2.1400e+02, 2.2550e+02,
       2.3740e+02, 2.4970e+02, 2.6240e+02, 2.7550e+02, 2.8900e+02, 3.0300e+02,
       3.1750e+02, 3.3240e+02, 3.4780e+02, 3.6370e+02, 3.8020e+02, 3.9720e+02,
       4.1480e+02, 4.3300e+02, 4.5180e+02, 4.7120e+02, 4.9120e+02, 5.1190e+02,
       5.3330e+02, 5.5540e+02, 5.7820e+02, 6.0180e+02, 6.2620e+02, 6.5140e+02,
       6.7740e+02, 7.0430e+02, 7.3210e+02, 7.6080e+02, 7.9050e+02, 8.2110e+02,
       8.5280e+02, 8.8550e+02, 9.1930e+02, 9.5420e+02, 9.9030e+02, 1.0276e+03,
       1.0661e+03, 1.1059e+03, 1.1470e+03, 1.1895e+03, 1.2334e+03, 1.2787e+03,
       1.3255e+03, 1.3739e+03, 1.4239e+03, 1.4756e+03, 1.5290e+03, 1.5842e+03,
       1.6412e+03, 1.7001e+03, 1.7609e+03, 1.8238e+03, 1.8887e+03, 1.9558e+03,
       2.0251e+03, 2.0967e+03, 2.1707e+03, 2.2472e+03, 2.3262e+03, 2.4078e+03,
       2.4921e+03, 2.5792e+03, 2.6692e+03, 2.7622e+03, 2.8583e+03, 2.9576e+03,
       3.0602e+03, 3.1662e+03, 3.2757e+03, 3.3889e+03, 3.5058e+03, 3.6266e+03,
       3.7514e+03, 3.8803e+03, 4.0135e+03, 4.1511e+03, 4.2933e+03, 4.4402e+03,
       4.5920e+03, 4.7489e+03, 4.9110e+03, 5.0784e+03, 5.2514e+03, 5.4301e+03,
       5.6148e+03, 5.8056e+03, 6.0027e+03])
    return depth

def compute_ncell_idx(lon, lat):
    grid = load_smt_wave_grid()
    clon = np.rad2deg(grid.clon.data)
    clat = np.rad2deg(grid.clat.data)
    idx  = np.argmin(((clon-lon)**2 + (clat-lat)**2))
    return idx

def load_smt_wave_land_mask():
    # ds        = load_smt_wave_to_exp7_08(it=2)
    # sst       = ds.isel(time=0, depth=0).to
    # a         = sst.where(~(sst==0), np.nan)
    # mask_land = ~np.isnan(a)
    # mask_land = mask_land.where(mask_land==True, np.nan)
    # mask_land = mask_land.drop('time')

    path = '/work/mh0033/m300878/smtwv_oce_2022/masks/land_mask.nc'
    ds   = xr.open_dataarray(path, chunks={'depth': 1})
    ds   = ds.drop('depth')
    return ds

def load_smt_land_mask(depthc=0):
    dsT       = load_smt_T()
    sst       = dsT.isel(time=0, depthc=depthc)
    a         = sst.where(~(sst==0), np.nan)
    mask_land = ~np.isnan(a)
    mask_land = mask_land.where(mask_land==True, np.nan)
    mask_land = mask_land.drop('time')
    return mask_land

def load_mooring_data():
    path         = '/work/mh0033/m300878/spectra/KE/observation_sonett_cruise/et4_adcp.nc'
    ds_1         = xr.open_dataset(path)
    datenum      = ds_1.time.values
    timestamps   = pd.to_datetime(datenum-719529, unit='D')
    ds_1['time'] = timestamps
    ds_1         = ds_1.isel(longitude=0, latitude=0)
    KE           = ds_1.ucur**2 / 2 + ds_1.vcur**2 / 2
    ds_1['KE']   = KE

    path         = '/work/mh0033/m300878/spectra/KE/observation_sonett_cruise/et3_adcp.nc'
    ds_2         = xr.open_dataset(path)
    datenum      = ds_2.time.values
    timestamps   = pd.to_datetime(datenum-719529, unit='D')
    ds_2['time'] = timestamps
    ds_2         = ds_2.isel(longitude=0, latitude=0)
    KE           = ds_2.ucur**2 / 2 + ds_2.vcur**2 / 2
    ds_2['KE']   = KE

    return ds_1, ds_2

# def tidefreq():
#     """
#     Eight major tidal frequencies in rad / day.  See Gill (1982) page 335.
    
#     Args: 
#         None
        
#     Returns:
#         An array of eight frequencies
#     """    
#     f = 24*2*np.pi/np.array([327.85,25.8194,24.0659,23.9344,12.6584,12.4206,12.0000,11.9673])
#     names = ['Mr','O1','P1', 'K1', 'N2', 'M2', 'S2', 'K2']
#     return f

# def tidefreqnames():
#     """
#     Eight major tidal frequencies in rad / day.  See Gill (1982) page 335.
    
#     Args: 
#         None
        
#     Returns:
#         An array of eight frequencies
#     """    
#     f = 24*2*np.pi/np.array([327.85,25.8194,24.0659,23.9344,12.6584,12.4206,12.0000,11.9673])
#     names = ['Mr','O1','P1', 'K1', 'N2', 'M2', 'S2', 'K2']
#     return names

# def corfreq(lat):
#     """
#     The Coriolis frequency in rad / day at a given latitude.
    
#     Args: 
#         lat: Latitude in degree
        
#     Returns:
#         The Coriolis frequecy at latitude lat
#     """    
#     omega = 7.2921159e-5
#     return 2*np.sin(np.abs(lat)*2*np.pi/360)*omega*(3600)*24

def compute_spectra_uchida(data, samp_freq):
    data['time'] = np.arange(int(data.time.size))*3600*samp_freq
    P = xrft.power_spectrum(data, dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
    P = P.isel(freq_time=slice(len(P.freq_time)//2,None)) * 2
    return P
    
def compute_grid_vars_gcm(ds, wet_mask):
    xx, yy = np.meshgrid(ds.lon, 
                         ds.lat
                        )
    ny, nx = xx.shape
    # computes spacing between two grid points depending on latitude in m; extrapolation to maintain same shape as b
    dx = xr.DataArray(xr.DataArray(gsw.distance(xx, yy), dims=['lat','lon'],
                                   coords={'lat':np.arange(ny),'lon':np.arange(.5,nx-1,1)}
                                  ).interp(lon=np.arange(nx), method="linear",
                                           kwargs={"fill_value": "extrapolate"}).data,
                      dims=['lat','lon'], 
                      coords={'lat':ds.lat.data,'lon':ds.lon.data}
                     )
    # computes spacing between two grid points depending on longitude in m; extrapolation to maintain same shape as b
    # in this example constant since grid is only irregular in meridional direction
    dy = xr.DataArray(xr.DataArray(gsw.distance(xx, yy, axis=0), dims=['lat','lon'],
                                   coords={'lat':np.arange(.5,ny-1,1),'lon':np.arange(nx)}
                                  ).interp(lat=np.arange(ny), method="linear",
                                           kwargs={"fill_value": "extrapolate"}).data,
                      dims=['lat','lon'], 
                      coords={'lat':ds.lat.data,'lon':ds.lon.data}
                     )
    area = (dx * dy)

    # here no filtersize changes are applied; only the grid is irregular; Rossby Radisu dependent filter properties would go in here
    kappa_w = xr.ones_like(wet_mask)
    kappa_s = xr.ones_like(wet_mask)

    dlat = ds.lat.diff('lat')[0]
    dlon = ds.lon.diff('lon')[0]

    dxw = xr.DataArray(dx.interp(lon=np.arange(dx.lon.min(),dx.lon.max()+dlon,dlon),
                                 method='linear',
                                 kwargs={"fill_value": "extrapolate"}).data,
                       dims=dx.dims, coords=dx.coords
                      ) # x-spacing centered at western cell edge

    dyw = xr.DataArray(dy.interp(lon=np.arange(dy.lon.min(),dy.lon.max()+dlon,dlon),
                                 method='linear',
                                 kwargs={"fill_value": "extrapolate"}).data,
                       dims=dy.dims, coords=dy.coords
                      ) # y-spacing centered at western cell edge

    dxs = xr.DataArray(dx.interp(lat=np.arange(dx.lat.min(),dx.lat.max()+dlat,dlat),
                                 method='linear',
                                 kwargs={"fill_value": "extrapolate"}).data,
                       dims=dx.dims, coords=dx.coords
                      ) # x-spacing centered at southern cell edge

    dys = xr.DataArray(dy.interp(lat=np.arange(dy.lat.min(),dy.lat.max()+dlat,dlat),
                                 method='linear',
                                 kwargs={"fill_value": "extrapolate"}).data,
                       dims=dy.dims, coords=dy.coords
                      ) # y-spacing centered at southern cell edge

    return dxw, dyw, dxs, dys, area, kappa_w, kappa_s

def create_gcm_filter(ds, grid_type, mask_land, filter_scale=3e4):
    if grid_type == 'regular':
        conversion_factor = convert_degree_to_meter(int(ds.lat.mean().data))
        dx_min   = (ds.lat[1]-ds.lat[0]).data * conversion_factor
        wet_mask = mask_land
        print('return same mask')
        mask_land_100_new =  mask_land

        filter_30km = gcm_filters.Filter(
            filter_scale = filter_scale,   # 30 km
            dx_min       = dx_min,
            filter_shape = gcm_filters.FilterShape.GAUSSIAN,
            grid_type    = gcm_filters.GridType.REGULAR_WITH_LAND,
            grid_vars    = { 'wet_mask': wet_mask}
        )
    elif grid_type == 'irregular':
        wet_mask = mask_land
        wet_mask = wet_mask.fillna(0) #wet mask needs to be ones and zeros

        dxw, dyw, dxs, dys, area, kappa_w, kappa_s = compute_grid_vars_gcm(ds, wet_mask)

        dx_min         = min(dxw.min(), dyw.min(), dxs.min(), dys.min()).data
        wet_mask[0,:]  = np.zeros_like(wet_mask[0,:] )   # fix impact from southern boundary
        wet_mask[:,-1] = np.zeros_like(wet_mask[:,-1] )  # fix impact from eastern boundary

        mask_land_100_new =  wet_mask.where(wet_mask>0)

        filter_30km      = gcm_filters.Filter(
            filter_scale = filter_scale,
            dx_min       = dx_min,
            filter_shape = gcm_filters.FilterShape.GAUSSIAN,
            grid_type    = gcm_filters.GridType.IRREGULAR_WITH_LAND,
            grid_vars    = {'wet_mask': wet_mask, 
                            'dxw': dxw,
                            'dyw': dyw,
                            'dxs': dxs,
                            'dys': dys, 
                            'area': area,
                            'kappa_w': kappa_w, 
                            'kappa_s': kappa_s})
        print(filter_30km)
    return filter_30km, mask_land_100_new

def create_anim(x,y,data, fig_path, name, start, end, interval, vmin, vmax, cmap):
    import matplotlib.animation as animation
    from matplotlib.animation import PillowWriter
    fig, ax = plt.subplots(1, 1, subplot_kw={'projection': ccrs_proj}, figsize=(10, 10))

    def update(t):
        ax.scatter(x, y, c=data.isel(time=t), cmap=cmap, vmin=vmin, vmax=vmax)
        return ax

    ani = animation.FuncAnimation(fig, update, frames=range(start, end, interval), repeat=False)
    writer = PillowWriter(fps=8)
    ani.save(fig_path + f'{name}.gif', writer=writer)