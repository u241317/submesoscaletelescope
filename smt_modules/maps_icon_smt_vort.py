import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.patches import Rectangle
#import my_toolbox as my
import cartopy.crs as ccrs
import cartopy
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import sys
import pyicon as pyic

#reload(pyic)

#lon_reg = [-75, -60]
#lat_reg = [30, 40]
#lon_reg = [-72, -70]
#lat_reg = [33, 35]
dpi = 250.


def make_axes(lon_reg_1, lat_reg_1, lon_reg_2, lat_reg_2, lon_reg_3, lat_reg_3, dpi):
  hca = []
  hcb = []

  # --- figure
  length = 4096./dpi
  aspf = 0.527
  fig = plt.figure(figsize=(length,length*aspf))
  
  ccrs_proj = ccrs.PlateCarree()
  # --- 1st axes
  height = 0.9
  asp = 0.7*aspf / aspf
  ax = fig.add_axes([0.04, 0.04, height*asp, height], projection=ccrs_proj)
  hca.append(ax)
  
  ax.set_xlim(lon_reg_1)
  ax.set_ylim(lat_reg_1)

  if ccrs_proj!='none':
    ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.9')
    ax.coastlines(zorder=6)
    ax.set_xticks(np.arange(-90,0,20), crs=ccrs_proj)
    ax.set_yticks(np.arange(10,70,10), crs=ccrs_proj)
    lon_formatter = LongitudeFormatter()
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    #ax.stock_img()
  ax.xaxis.set_ticks_position('both')
  ax.yaxis.set_ticks_position('both')
  ax.set_title('ICON-O Submesoscale Telescope', loc='left', fontsize=20)
  
  
  lw = 2.
#   ax.add_patch(
#     Rectangle(xy=[lon_reg_2[0], lat_reg_2[0]], 
#               width=lon_reg_2[1]-lon_reg_2[0], height=lat_reg_2[1]-lat_reg_2[0],
#               edgecolor='k',
#               facecolor='none',
#               linewidth=lw,
#               transform=ccrs_proj) )
  
#   ax.add_patch(
#     Rectangle(xy=[lon_reg_3[0], lat_reg_3[0]], 
#               width=lon_reg_3[1]-lon_reg_3[0], height=lat_reg_3[1]-lat_reg_3[0],
#               edgecolor='r',
#               facecolor='none',
#               linewidth=lw,
#               transform=ccrs_proj) )
  
  bbox=dict(facecolor='w', alpha=1., edgecolor='none')
  ax.text(0.02, 0.04, 'mapped to 1/50$^o$ grid', transform=ax.transAxes, bbox=bbox, zorder=7)

  # --- colorbar
  height = 0.2
  asp = 0.1
  # cax = fig.add_axes([0.05, 0.48, height*asp, height])
#   cax.set_ylabel('rel. vort. / plan. vort.')
  cax.yaxis.tick_right()
  cax.yaxis.set_label_position("right")
  cax.set_xticks([])
  hcb.append(cax)
  # --- 2nd axes
  #ccrs_proj = None
  
  height = 0.45
  asp = 0.5 / aspf
  ax = fig.add_axes([0.55, 0.55, height*asp, height], projection=ccrs_proj)
  hca.append(ax)
  ax.set_xlim(lon_reg_2)
  ax.set_ylim(lat_reg_2)
  ax.set_xticks([])
  ax.set_yticks([])
  
#   ax.add_patch(
#     Rectangle(xy=[lon_reg_3[0], lat_reg_3[0]], 
#               width=lon_reg_3[1]-lon_reg_3[0], height=lat_reg_3[1]-lat_reg_3[0],
#               edgecolor='r',
#               facecolor='none',
#               linewidth=lw,
#               transform=ccrs_proj) )
  ax.text(0.02, 0.06, 'mapped to 1/50$^o$ grid', transform=ax.transAxes, bbox=bbox)
  
  # --- colorbar
#   height = 0.2
#   asp = 0.1
#   cax = fig.add_axes([0.92, 0.75, height*asp, height])
# #   cax.set_ylabel('rel. vort. / plan. vort.')
#   cax.yaxis.tick_right()
#   cax.yaxis.set_label_position("right")
#   cax.set_xticks([])
#   hcb.append(cax)
  # --- 3rd axes
  
  height = 0.45
  asp = 0.5 / aspf
  ax = fig.add_axes([0.55, 0.02, height*asp, height], projection=ccrs_proj)
  hca.append(ax)
  ax.set_xticks([])
  ax.set_yticks([])
  
  ax.set_xlim(lon_reg_3)
  ax.set_ylim(lat_reg_3)
  ax.text(0.02, 0.06, 'original grid', transform=ax.transAxes, bbox=bbox)
  
  # --- colorbar
#   height = 0.2
#   asp = 0.1
#   cax = fig.add_axes([0.92, 0.05, height*asp, height])
# #   cax.set_ylabel('rel. vort. / plan. vort.')
#   cax.yaxis.tick_right()
#   cax.yaxis.set_label_position("right")
#   cax.set_xticks([])
#   hcb.append(cax)
  
  #ax.text(0.005, 0.01, 'ICON-O Submesoscale Telescope', transform=fig.transFigure)
  return fig, hca, hcb



def make_axes2(lon_reg_1, lat_reg_1, lon_reg_2, lat_reg_2, lon_reg_3, lat_reg_3, dpi):
  hca = []
  hcb = []

  # --- figure
  length = 4096./dpi
  aspf = 0.527
  fig = plt.figure(figsize=(length,length*aspf))
  
  ccrs_proj = ccrs.PlateCarree()
  # --- 1st axes
  height = 0.9
  asp = 0.7*aspf / aspf
  ax = fig.add_axes([0.04, 0.04, height*asp, height], projection=ccrs_proj)
  hca.append(ax)
  
  ax.set_xlim(lon_reg_1)
  ax.set_ylim(lat_reg_1)

  if ccrs_proj!='none':
    ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.9')
    ax.coastlines(zorder=6)
    ax.set_xticks(np.arange(-90,0,20), crs=ccrs_proj)
    # ax.set_yticks(np.arange(10,70,10), crs=ccrs_proj)
    lon_formatter = LongitudeFormatter()
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    #ax.stock_img()
  ax.xaxis.set_ticks_position('both')
  ax.yaxis.set_ticks_position('both')
  # set y and x tick fontsize
  ax.tick_params(axis='both', which='major', labelsize=18)
  # ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
  ax.set_title('ICON-O Submesoscale Telescope', loc='left', fontsize=23)
  
  bbox=dict(facecolor='w', alpha=1., edgecolor='none')
  ax.text(0.02, 0.04, 'mapped to 1/50$^o$ grid', transform=ax.transAxes, bbox=bbox, zorder=7, fontsize=14)

  # --- colorbar
  height = 0.2
  asp = 0.1
  cax = fig.add_axes([0.05, 0.48, height*asp, height]) 

  # cax.set_ylabel('rel. vort. / plan. vort.')

  cax.yaxis.tick_right()
  cax.yaxis.set_label_position("right")
  cax.set_xticks([])
  hcb.append(cax)
  # --- 2nd axes
  #ccrs_proj = None
  
  height = 0.65
  asp = 0.5 / aspf
  # ax = fig.add_axes([0.4, 0.3, height*asp, height], projection=ccrs_proj) # right aligned
  ax = fig.add_axes([0.35, 0.3, height*asp, height], projection=ccrs_proj) # right aligned

  hca.append(ax)
  ax.set_xlim(lon_reg_2)
  ax.set_ylim(lat_reg_2)
  ax.set_xticks([])
  ax.set_yticks([])
  ax.text(0.02, 0.06, 'mapped to 1/50$^o$ grid', transform=ax.transAxes, bbox=bbox, fontsize=14)
  
  # --- 3rd axes
  
  height = 0.45
  asp = 0.5 / aspf
  ax = fig.add_axes([0.563, 0.02, height*asp, height], projection=ccrs_proj) # right aligned
  # ax = fig.add_axes([0.64, 0.02, height*asp, height], projection=ccrs_proj) # spread

  hca.append(ax)
  ax.set_xticks([])
  ax.set_yticks([])
  
  ax.set_xlim(lon_reg_3)
  ax.set_ylim(lat_reg_3)
  ax.text(0.02, 0.06, 'original grid', transform=ax.transAxes, bbox=bbox, fontsize=14)
  
  return fig, hca, hcb




def make_axes3(lon_reg_1, lat_reg_1, lon_reg_2, lat_reg_2, lon_reg_3, lat_reg_3, dpi):
  hca = []
  hcb = []

  # --- figure
  length = 4096./dpi
  aspf = 0.527
  fig = plt.figure(figsize=(length,length*aspf))
  
  ccrs_proj = ccrs.PlateCarree()
  # --- 1st axes
  height = 0.9
  asp = 0.7*aspf / aspf
  ax = fig.add_axes([0.04, 0.04, height*asp, height], projection=ccrs_proj)
  hca.append(ax)
  
  ax.set_xlim(lon_reg_1)
  ax.set_ylim(lat_reg_1)

  if ccrs_proj!='none':
    ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.9')
    ax.coastlines(zorder=6)
    ax.set_xticks(np.arange(-90,0,20), crs=ccrs_proj)
    # ax.set_yticks(np.arange(10,70,10), crs=ccrs_proj)
    lon_formatter = LongitudeFormatter()
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    #ax.stock_img()
  ax.xaxis.set_ticks_position('both')
  ax.yaxis.set_ticks_position('both')
  # set y and x tick fontsize
  ax.tick_params(axis='both', which='major', labelsize=18)
  # ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
  ax.set_title('ICON-O Submesoscale Telescope', loc='left', fontsize=23)
  
  bbox=dict(facecolor='w', alpha=1., edgecolor='none')
  ax.text(0.02, 0.04, 'mapped to 1/50$^o$ grid', transform=ax.transAxes, bbox=bbox, zorder=7, fontsize=14)

  # --- colorbar
  height = 0.2
  asp = 0.1
  cax = fig.add_axes([0.05, 0.39, height*asp, height]) 

  # cax.set_ylabel('rel. vort. / plan. vort.')

  cax.yaxis.tick_right()
  cax.yaxis.set_label_position("right")
  cax.set_xticks([])
  hcb.append(cax)
  # --- 2nd axes
  #ccrs_proj = None
  
  height = 0.65
  asp = 0.5 / aspf
  # ax = fig.add_axes([0.4, 0.3, height*asp, height], projection=ccrs_proj) # right aligned
  ax = fig.add_axes([0.5, 0.3, height*asp, height], projection=ccrs_proj) # right aligned

  hca.append(ax)
  ax.set_xlim(lon_reg_2)
  ax.set_ylim(lat_reg_2)
  ax.set_xticks([])
  ax.set_yticks([])
  ax.text(0.02, 0.06, 'mapped to 1/50$^o$ grid', transform=ax.transAxes, bbox=bbox, fontsize=14)
  
  # --- 3rd axes
  
  height = 0.45
  asp = 0.5 / aspf
  ax = fig.add_axes([0.7, 0.02, height*asp, height], projection=ccrs_proj) # right aligned
  # ax = fig.add_axes([0.64, 0.02, height*asp, height], projection=ccrs_proj) # spread

  hca.append(ax)
  ax.set_xticks([])
  ax.set_yticks([])
  
  ax.set_xlim(lon_reg_3)
  ax.set_ylim(lat_reg_3)
  ax.text(0.02, 0.06, 'original grid', transform=ax.transAxes, bbox=bbox, fontsize=14)
  
  return fig, hca, hcb


def make_axes_nils(lon_reg_1, lat_reg_1, lon_reg_2, lat_reg_2, lon_reg_3, lat_reg_3, dpi):
  hca = []

  # --- figure
  length = 4096./dpi
  aspf = 0.527
  fig = plt.figure(figsize=(length,length*aspf))
  
  ccrs_proj = ccrs.PlateCarree()
  
  # --- 1st axes
  height = 0.9
  asp = 0.7*aspf / aspf
  ax = fig.add_axes([0.04, 0.04, height*asp, height], projection=ccrs_proj)
  hca.append(ax)
  
  ax.set_xlim(lon_reg_1)
  ax.set_ylim(lat_reg_1)
  
  if ccrs_proj!='none':
    ax.coastlines()
    ax.add_feature(cartopy.feature.LAND, zorder=0, facecolor='0.9')
    ax.set_xticks(np.arange(-170,170,10), crs=ccrs_proj)
    ax.set_yticks(np.arange(-80,80,5), crs=ccrs_proj)
    lon_formatter = LongitudeFormatter()
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    #ax.stock_img()
  ax.xaxis.set_ticks_position('both')
  ax.yaxis.set_ticks_position('both')
  ax.set_title('ICON-O Sub-Mesoscale Telescope', loc='left')
  
  lw = 2.
  ax.add_patch(
    Rectangle(xy=[lon_reg_2[0], lat_reg_2[0]], 
              width=lon_reg_2[1]-lon_reg_2[0], height=lat_reg_2[1]-lat_reg_2[0],
              edgecolor='k',
              facecolor='none',
              linewidth=lw,
              transform=ccrs_proj) )
  
  ax.add_patch(
    Rectangle(xy=[lon_reg_3[0], lat_reg_3[0]], 
              width=lon_reg_3[1]-lon_reg_3[0], height=lat_reg_3[1]-lat_reg_3[0],
              edgecolor='r',
              facecolor='none',
              linewidth=lw,
              transform=ccrs_proj) )
  
  bbox=dict(facecolor='w', alpha=1., edgecolor='none')
  ax.text(0.02, 0.04, 'mapped to 1/50$^o$ grid', transform=ax.transAxes, bbox=bbox)

  # --- 2nd axes
  #ccrs_proj = None
  
  height = 0.45
  asp = 0.5 / aspf
  ax = fig.add_axes([0.55, 0.55, height*asp, height], projection=ccrs_proj)
  hca.append(ax)
  ax.set_xlim(lon_reg_2)
  ax.set_ylim(lat_reg_2)
  ax.set_xticks([])
  ax.set_yticks([])
  
  ax.add_patch(
    Rectangle(xy=[lon_reg_3[0], lat_reg_3[0]], 
              width=lon_reg_3[1]-lon_reg_3[0], height=lat_reg_3[1]-lat_reg_3[0],
              edgecolor='r',
              facecolor='none',
              linewidth=lw,
              transform=ccrs_proj) )
  ax.text(0.02, 0.06, 'mapped to 1/50$^o$ grid', transform=ax.transAxes, bbox=bbox)
  
  # --- 3rd axes
  
  height = 0.45
  asp = 0.5 / aspf
  ax = fig.add_axes([0.55, 0.02, height*asp, height], projection=ccrs_proj)
  hca.append(ax)
  ax.set_xticks([])
  ax.set_yticks([])
  
  ax.set_xlim(lon_reg_3)
  ax.set_ylim(lat_reg_3)
  ax.text(0.02, 0.06, 'original grid', transform=ax.transAxes, bbox=bbox)
  
  # --- colorbar
  height = 0.2
  asp = 0.1
  cax = fig.add_axes([0.05, 0.48, height*asp, height])
  cax.set_ylabel('rel. vort. / plan. vort.')
  cax.yaxis.tick_right()
  cax.yaxis.set_label_position("right")
  cax.set_xticks([])
  
  #ax.text(0.005, 0.01, 'ICON-O Submesoscale Telescope', transform=fig.transFigure)
  return fig, hca, cax
