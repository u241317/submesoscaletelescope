# %%
from collections import OrderedDict
from pathlib import Path
import xarray as xr
import pyicon as pyic
import numpy as np

from importlib import reload
from dask_jobqueue import SLURMCluster
from dask.distributed import Client
import dask.config

from tempfile import NamedTemporaryFile, TemporaryDirectory # Creating temporary Files/Dirs
from getpass import getuser # Libaray to copy things


def init_dask_slurm_cluster(scale = 2, processes = 16, walltime="00:15:00", memory="256GiB", account="mh0033", dash_address="8989", wait=True):
    dask.config.set(
        {
            "distributed.worker.data-directory": "/scratch/m/m300878/dask_temp",
            "distributed.worker.memory.target": 0.75,
            "distributed.worker.memory.spill": 0.85,
            "distributed.worker.memory.pause": 0.95,
            "distributed.worker.memory.terminate": 0.98,
        }
    )

    scluster = SLURMCluster(
        queue             = "compute",
        walltime          = walltime,
        memory            = memory,
        cores             = processes,
        processes         = processes,
        account           = account,   
        name              = "m300878-dask-cluster",
        interface         = "ib0",
        asynchronous      = False,
        scheduler_options = {'dashboard_address': f':{dash_address}'},
        log_directory     = "/scratch/m/m300878/dask_logs/",
        local_directory   = "/scratch/m/m300878/dask_temp/",
    )
    
    client = Client(scluster)
    scluster.scale(jobs=scale)
    print(scluster.job_script())
    if wait ==True: 
        nworkers = scale * processes
        client.wait_for_workers(nworkers)              # waits for all workers to be ready, can be submitted now

    return client, scluster

def init_my_cluster():
    job_name     = 'PostProc'       # Job name that is submitted via sbatch
    scratch_dir  = Path('/scratch') / getuser()[0] / getuser() # Define the users scratch dir # Create a temp directory where the output of distributed cluster will be written to, after this notebook # is closed the temp directory will be closed
    dask_tmp_dir = TemporaryDirectory(dir=scratch_dir, prefix=job_name)

    cluster = SLURMCluster(
        account           = 'mh0033',                       # Account that is going to be 'charged' fore the computation
        queue             = 'compute',                       # Name of the partition we want to use
        job_name          = 'PostProc',                     # Job name that is submitted via sbatch,
        memory            = "256GiB",                        # Max memory per node that is going to be used - this depends on the partition #for averaging max = 256ame,
        cores             = 16,                             # Max number of cores per task that are reserved - also partition dependend
        processes         = 16,                             # number of workers
        walltime          = '00:30:00',                     # Walltime - also partition dependent
        asynchronous      = False,
        scheduler_options = {'dashboard_address': ':8989'},
        log_directory     = f'{dask_tmp_dir.name}/',
        local_directory   = f'{dask_tmp_dir.name}/',
        # log file names are not working on levante
        # local_directory   = dask_tmp_dir.name,
        # job_extra         = [f'-J {job_name}', 
        #                      f'-D {dask_tmp_dir.name}',
        #                      f'--begin=now',
        #                      f'--output={dask_tmp_dir.name}/LOG_cluster.%j.o'
        #                     ],
        interface         = 'ib0')
    
    print('temporary directory:', dask_tmp_dir.name)
    print(cluster.job_script())
    cluster.scale(jobs=2)

    # unclear if this is works
    dask.config.set({'distributed.worker.memory.spill': False})
    dask.config.set({'distributed.worker.memory.target': False})
    dask.config.get('distributed.worker')

    client = Client(cluster)
    return client, cluster

def disable_spill():
    """
    Example Usage:
    client.register_worker_callbacks(setup=disable_spill)
    """
    dask.config.set(
        {'distributed.worker.memory.target': False, 
         'distributed.worker.memory.spill': False, 
         'distributed.worker.memory.pause':0.95,
         'distributed.worker.memory.terminate':0.96}
    )
    print(dask.config.config)
    
def init_dask_slurm_cluster_gpu(scale = 1, processes = 32, walltime="00:15:00", memory="512GiB", wait=True):
    dask.config.set(
        {
            "distributed.worker.data-directory": "/scratch/m/m300878/dask_temp",
            "distributed.worker.memory.target": 0.75,
            "distributed.worker.memory.spill": 0.85,
            "distributed.worker.memory.pause": 0.95,
            "distributed.worker.memory.terminate": 0.98,
        }
    )

    scluster = SLURMCluster(
        queue             = "gpu",
        walltime          = walltime,
        memory            = memory,
        cores             = processes,
        processes         = processes,
        account           = "bm1239",   
        name              = "m300878-dask-cluster",
        interface         = "ib0",
        asynchronous      = False,
        scheduler_options = {'dashboard_address': ':8989'},
        log_directory     = "/scratch/m/m300878/dask_logs/",
        local_directory   = "/scratch/m/m300878/dask_temp/",
    )
    
    client = Client(scluster)
    scluster.scale(jobs=scale)
    print(scluster.job_script())
    if wait ==True: 
        nworkers = scale * processes
        client.wait_for_workers(nworkers)              # waits for all workers to be ready, can be submitted now

    return client, scluster