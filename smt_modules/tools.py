import datetime

import cmocean
# import datashader as ds
import numpy as np
from math import atan2,degrees
import pandas as pd
# from datashader.mpl_ext import dsshow


import time, os, re
import xarray as xr
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib.ticker as mticker

import cartopy.crs as ccrs
import cartopy.feature as cfeature
from   cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


def icon2datetime(icon_dates):
    """
    From: esm_analysis lib
    Convert datetime objects in icon format to python datetime objects.
    :param icon_dates: collection
    :return dates:  pd.DatetimeIndex
    icon_dates i.e
        time = icon2datetime([20011201.5])
    """
    try:
        icon_dates = icon_dates.values
    except AttributeError:
        pass

    try:
        icon_dates = icon_dates[:]
    except TypeError:
        icon_dates = np.array([icon_dates])

    def _convert(icon_date):
        frac_day, date = np.modf(icon_date)
        frac_day *= 60 ** 2 * 24
        date = str(int(date))
        date_str = datetime.datetime.strptime(date, '%Y%m%d')
        td = datetime.timedelta(seconds=int(frac_day.round(0)))
        return date_str + td

    conv = np.vectorize(_convert)
    try:
        out = conv(icon_dates)
    except TypeError:
        out = icon_dates
    if len(out) == 1:
        return pd.DatetimeIndex(out)[0]
    return pd.DatetimeIndex(out)


def basic_plot(ds_variable, ax, cax, native_grid=False, time_pos=0, lon_range=None, lat_range=None, vminmax=None, cmap=cmocean.cm.dense):
    """
    Plotting high resolution model using datashade
    Author: Angel Peinado

    #Example Tropics - data_native is a xarray Dataset
    variable = 'prw'
    fig = plt.figure(figsize=(60, 12), facecolor='white')
    gs = fig.add_gridspec(1,1)
    ax = fig.add_subplot(gs[0, 0])
    ax = basic_plot(data_native[variable], ax, fig, native_grid=True, time_pos=0, lon_range=None, lat_range=[-40, 40], vminmax=[0, 80])

    Example:
    lon_reg=-90,0
    lat_reg=0,45
    #variable must be data array!
    var = ds_v_sel.isel(depthc=10)
    var1 = var.isel(time=0).u 
    var1.attrs["long_name"] = ""
    var1.attrs["units"] = "" 
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=0.5, fig_size_fac=5, axlab_kw=None)
    fig = plt.gcf()
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax = tools.basic_plot(var1, ax, cax, fig, native_grid=True, lon_range=lon_reg, lat_range=lat_reg, time_pos=0, vminmax=[-1,1], cmap='RdBu')
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    Example in pyicon: TODO: Fix for cartopy
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=5, axlab_kw=None)
    fig = plt.gcf()
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    var = vort.vort_f_cells_50m
    ax = tools.basic_plot(var, ax, cax, fig, native_grid=True, time_pos=0, lon_range=lon_reg, lat_range=lat_reg, vminmax=[-1,1], cmap='RdBu')
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    """
    # Basic assert and variable definition
    if lon_range is None:
        lon_range = [-180, 180]
    if lat_range is None:
        lat_range = [-90, 90]
    assert lon_range[1] > lon_range[0], f"Wrong range values in lon_range, first element should be smaller than second one"
    assert lat_range[1] > lat_range[0], f"Wrong range values in lat_range, first element should be smaller than second one"
    # assert time_pos < ds_variable.time.values.size, f"Time_pos given: {time_pos} is greater than time dimension"
    if vminmax is not None:
        assert vminmax[1] > vminmax[0], f"Wrong range in vmimax, first element should be smaller than second one"
    # Start the work
    if native_grid:
        # Get the data in a dataframe to use dasher plot
        xi = np.rad2deg(ds_variable.clon.values)
        yi = np.rad2deg(ds_variable.clat.values)
        data_set = pd.DataFrame(data={
            "variable": np.squeeze(ds_variable.values),
            # "variable": np.squeeze(ds_variable.isel(time=time_pos).values),
            "x": xi,
            "y": yi,
        },)
        data_set = data_set[(data_set['x']>lon_range[0]) & (data_set['x']<lon_range[1]) & 
                            (data_set['y']>lat_range[0]) & (data_set['y']<lat_range[1])]
        if vminmax is not None:
            artist = dsshow(data_set, ds.Point('x', 'y'), ds.mean('variable'), cmap=cmap, vmax=vminmax[1], vmin=vminmax[0], ax=ax)
        else:
            artist = dsshow(data_set, ds.Point('x', 'y'), ds.mean('variable'), cmap=cmap, ax=ax)
        # fig.colorbar(artist, label=("%s [%s]" % (ds_variable.attrs['long_name'], ds_variable.attrs['units'])), cax=cax)
        return ax
    else:
        data_set = ds_variable.isel(time=time_pos)
        data_set = data_set.sel(lat=slice(lat_range[0], lat_range[1]), 
                                lon=slice(lon_range[0], lon_range[1]))
        if vminmax is None:
            data_set.plot(ax=ax, cmap=cmap)
        else:
            data_set.plot(ax=ax, cmap=cmap, vmax=vminmax[1], vmin=vminmax[0])
        return ax

def plot_ds_with_cartopy(var, lon_range=None, lat_range=None, vmin=-1, vmax=1):
    #-- set Mollweide projection
    # projection = ccrs.Mollweide()
    projection = ccrs.PlateCarree()

    #-- transform radians to geodetic coordinates
    coords = projection.transform_points(
                            ccrs.Geodetic(),
                            np.rad2deg(var.clon.values),
                            np.rad2deg(var.clat.values))

    #-- create data frame of the variable values and the geodetic coordinates.
    df = pd.DataFrame({'val': var.values, 'x': coords[:,0], 'y': coords[:,1]})
    # df = pd.DataFrame({'val': var.values, 'x': coords[lon_range[0]:lon_range[1],0], 'y': coords[:,1]})


    #-- choose colormap
    colormap = 'RdBu_r'
    colormap = 'PRGn_r'
    import seaborn as sns
    hue_neg, hue_pos = 250, 20
    cmap  = f'sns–{hue_neg}-{hue_pos}-1'
    colormap = sns.diverging_palette(hue_neg, hue_pos, s=90, center="light", as_cmap=True)

    #-- create the plot
    fig, ax = plt.subplots(figsize=(10,10), subplot_kw={"projection": projection})
    #fig.canvas.draw_idle()

    # change color of land
    ax.add_feature(cfeature.LAND, facecolor='lightgrey', zorder=10)
    ax.add_feature(cfeature.COASTLINE, linewidth=0.8, zorder=11)

    # gl = ax.gridlines(crs=ccrs.PlateCarree(),
    #                   draw_labels=True,
    #                   color='black',
    #                   linewidth=0.5,
    #                   alpha=0.7,
    #                   x_inline=False)
    # gl.xlocator   = mticker.FixedLocator(range(-180, 180+1, 60))
    # gl.xformatter = LONGITUDE_FORMATTER

    df = df[(df['x']>lon_range[0]) & (df['x']<lon_range[1]) & 
                    (df['y']>lat_range[0]) & (df['y']<lat_range[1])]

    artist = dsshow(df,
                    ds.Point('x', 'y'),
                    ds.mean('val'),
                    vmin=vmin,
                    vmax=vmax,
                    cmap=colormap,
                    plot_width=1600,
                    plot_height=1400,
                    ax=ax)

    # fig.colorbar(artist, label='ta [K]', shrink=0.7, pad=0.02)

    # cax = fig.add_axes([0.14, 0.60, 0.01, 0.09])
    cax = fig.add_axes([0.13, 0.59, 0.01, 0.04])

    fig.colorbar(artist, cax=cax, label=r'$~\zeta /f$', extend='both', pad=5)
    cax.yaxis.set_major_locator(plt.MaxNLocator(2))

    # add longitude and latitude labels
    ax.set_xticks(np.arange(lon_range[0], lon_range[1], 5), crs=ccrs.PlateCarree())
    ax.set_yticks(np.arange(lat_range[0], lat_range[1], 5), crs=ccrs.PlateCarree())
    # apply the default formatter to the ticks
    ax.xaxis.set_major_formatter(LONGITUDE_FORMATTER)
    ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)



    # plt.title('ICON R02B09 - ta', fontsize=18)
    # plt.figtext(0.75, 0.33, "© 2022 DKRZ", ha="right", fontsize=5)

    # plt.savefig('plot_ICON_ta_'+date+'_r2b9_dsshow.png', bbox_inches='tight', dpi=150)
    return fig, ax, artist


def plot_ds_with_cartopy_w(var, lon_range=None, lat_range=None, vmin=-1, vmax=1):
    #-- set Mollweide projection
    # projection = ccrs.Mollweide()
    projection = ccrs.PlateCarree()

    #-- transform radians to geodetic coordinates
    coords = projection.transform_points(
                            ccrs.Geodetic(),
                            np.rad2deg(var.clon.values),
                            np.rad2deg(var.clat.values))

    #-- create data frame of the variable values and the geodetic coordinates.
    df = pd.DataFrame({'val': var.values, 'x': coords[:,0], 'y': coords[:,1]})
    # df = pd.DataFrame({'val': var.values, 'x': coords[lon_range[0]:lon_range[1],0], 'y': coords[:,1]})


    #-- choose colormap
    colormap = 'RdBu_r'
    colormap = 'PRGn_r'
    import seaborn as sns
    hue_neg, hue_pos = 250, 20
    cmap  = f'sns–{hue_neg}-{hue_pos}-1'
    colormap = sns.diverging_palette(hue_neg, hue_pos, s=90, center="light", as_cmap=True)

    #-- create the plot
    fig, ax = plt.subplots(figsize=(10,10), facecolor='white', subplot_kw={"projection": projection})

    ax.add_feature(cfeature.LAND, facecolor='lightgrey', zorder=10)
    ax.add_feature(cfeature.COASTLINE, linewidth=0.8, zorder=11)


    df = df[(df['x']>lon_range[0]) & (df['x']<lon_range[1]) & 
                    (df['y']>lat_range[0]) & (df['y']<lat_range[1])]

    artist = dsshow(df,
                    ds.Point('x', 'y'),
                    ds.mean('val'),
                    vmin=vmin,
                    vmax=vmax,
                    cmap=colormap,
                    plot_width=1600,
                    plot_height=1400,
                    ax=ax)

    fig.colorbar(artist, label='w in [m/s]', shrink=0.3)

    # cax = fig.add_axes([0.14, 0.60, 0.01, 0.09])
    # fig.colorbar(artist, cax=cax, label=r'$~\zeta /f$', extend='both', pad=5)
    # cax.yaxis.set_major_locator(plt.MaxNLocator(2))

    # add longitude and latitude labels
    ax.set_xticks(np.arange(lon_range[0], lon_range[1], 5), crs=ccrs.PlateCarree())
    ax.set_yticks(np.arange(lat_range[0], lat_range[1], 5), crs=ccrs.PlateCarree())
    # apply the default formatter to the ticks
    ax.xaxis.set_major_formatter(LONGITUDE_FORMATTER)
    ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)



    plt.title(f'Vertical velocity in {var.depth.data:.0}m', fontsize=18)
    # plt.figtext(0.75, 0.33, "© 2022 DKRZ", ha="right", fontsize=5)

    # plt.savefig('plot_ICON_ta_'+date+'_r2b9_dsshow.png', bbox_inches='tight', dpi=150)
    return fig, ax, artist

def labelLine(line,x,label=None,align=True,**kwargs):
    """Label line with line2D label data"""

    ax = line.axes
    xdata = line.get_xdata()
    ydata = line.get_ydata()

    if (x < xdata[0]) or (x > xdata[-1]):
        print('x label location is outside data range!')
        return

    #Find corresponding y co-ordinate and angle of the line
    ip = 1
    for i in range(len(xdata)):
        if x < xdata[i]:
            ip = i
            break

    y = ydata[ip-1] + (ydata[ip]-ydata[ip-1])*(x-xdata[ip-1])/(xdata[ip]-xdata[ip-1])

    if not label:
        label = line.get_label()

    if align:
        #Compute the slope
        dx = xdata[ip] - xdata[ip-1]
        dy = ydata[ip] - ydata[ip-1]
        ang = degrees(atan2(dy,dx))

        #Transform to screen co-ordinates
        pt = np.array([x,y]).reshape((1,2))
        trans_angle = ax.transData.transform_angles(np.array((ang,)),pt)[0]

    else:
        trans_angle = 0

    #Set a bunch of keyword arguments
    if 'color' not in kwargs:
        kwargs['color'] = line.get_color()

    if ('horizontalalignment' not in kwargs) and ('ha' not in kwargs):
        kwargs['ha'] = 'center'

    if ('verticalalignment' not in kwargs) and ('va' not in kwargs):
        kwargs['va'] = 'center'

    if 'backgroundcolor' not in kwargs:
        kwargs['backgroundcolor'] = ax.get_facecolor()

    if 'clip_on' not in kwargs:
        kwargs['clip_on'] = True

    if 'zorder' not in kwargs:
        kwargs['zorder'] = 2.5

    ax.text(x,y,label,rotation=trans_angle,**kwargs)

def labelLines(lines,align=True,xvals=None,**kwargs):

    ax = lines[0].axes
    labLines = []
    labels = []

    #Take only the lines which have labels other than the default ones
    for line in lines:
        label = line.get_label()
        if "_line" not in label:
            labLines.append(line)
            labels.append(label)

    if xvals is None:
        xmin,xmax = ax.get_xlim()
        xvals = np.linspace(xmin,xmax,len(labLines)+2)[1:-1]

    for line,x,label in zip(labLines,xvals,labels):
        labelLine(line,x,label,align,**kwargs)


    """Test
    
    from scipy.stats import loglaplace,chi2
    X = np.linspace(0,1,500)
    A = [1,2,5,10,20]
    funcs = [np.arctan,np.sin,loglaplace(4).pdf,chi2(5).pdf]
    
    plt.subplot(221)
    for a in A:
        plt.plot(X,np.arctan(a*X),label=str(a))
    
    tools.labelLines(plt.gca().get_lines(),zorder=2.5)
    
    plt.subplot(222)
    for a in A:
        plt.plot(X,np.sin(a*X),label=str(a))
    
    tools.labelLines(plt.gca().get_lines(),align=False,fontsize=14)
    
    plt.subplot(223)
    for a in A:
        plt.plot(X,loglaplace(4).pdf(a*X),label=str(a))
    
    xvals = [0.8,0.55,0.22,0.104,0.045]
    tools.labelLines(plt.gca().get_lines(),align=False,xvals=xvals,color='k')
"""

