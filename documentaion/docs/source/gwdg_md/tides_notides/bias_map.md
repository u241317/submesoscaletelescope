# Bias maps
## Differences: tides vs no tides
* after 4 days and 12 days of active tides
* we observe tidal signal + impact in the differences at snapshots

![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/MLD_tides_notides_2019-07-04T00.png)

![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/vort_tides_notides_2019-07-04T00.png)
* at depth level 51.5m

![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/u_tides_notides_2019-07-04T00.png)
* at depth level 1m

![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/v_tides_notides_2019-07-04T00.png)
* at depth level 1m

![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/SST_tides_notides_2019-07-04T00.png)

![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/SSH_tides_notides_2019-07-04T00.png)
* mean of selected region is substracted of each figure

![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/SSS_tides_notides_2019-07-04T00.png)

### Difference over a 3 day mean
* tidal signal is removed by substracting a 3 day mean

x | x
:--:|:--:
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact/MLD_tides_notides_impact_2019-07-09:2019-07-11.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact/vort_tides_notides_impact_2019-07-09:2019-07-11.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact/SST_tides_notides_impact_2019-07-09:2019-07-11.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact/SSS_tides_notides_impact_2019-07-09:2019-07-11.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact/u_tides_notides_impact_2019-07-09:2019-07-11.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact/v_tides_notides_impact_2019-07-09:2019-07-11.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact/SSH_tides_notides_impact_2019-07-09:2019-07-11.png) | blank

* it appears that some tidal signal is remaining in the velocities MLD and SSS (see tidal beam? or background SSH?)
* much weaker beams at 3rd week (most likely initial conditions impact)

#### Are the tidal beams visible in MLD? here larger region
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact/MLD_tidal_impact_20190709-11_large.png) 
* strong residual of tidal filtering or strong impact?

### Conclusions
* at eddie rims and large mesoscale fronts we observe the largest differences
* SST is cooler with tides than without tides (roughly 80% cooler to 20% warmer)
* velocity signal shows a signal of a tidal beam but overall diferences are weak apart from mesocale eddy
* impact on MLD is strong (at frontal features more then 50m) overall impact: deeper MLD >> Why do we observe a tidal beam in MLD?
* SSH diffrences shows remaining tidal signal (except mesoscale eddy)
* filtering of tidal signal has flaws and it is unclear how much of the remaining signal is simply tides and what is the impact of tides on eg mesoscale eddies or SM fronts


## larger evaluation and galnce on eddies
* here a glance on 3rd week 

### larger domain
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_large_20-23/MLD_tides_notides_impact_2019-07-20:2019-07-22.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_large_20-23/SST_tides_notides_impact_2019-07-20:2019-07-22.png) 
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_large_20-23/vort_tides_notides_impact_2019-07-20:2019-07-22.png) 
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_large_20-23/u_tides_notides_impact_2019-07-20:2019-07-22.png) 
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_large_20-23/v_tides_notides_impact_2019-07-20:2019-07-22.png) 
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_large_20-23/SSH_tides_notides_impact_2019-07-20:2019-07-22.png) 
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_large_20-23/SSS_tides_notides_impact_2019-07-20:2019-07-22.png)

* MLD increased significantly in the tidal at strong mesocale feature (at strong SST gradient)
* towards south african shelf MLD gets shallower
* SST is cooler in the tidal with the expection of south african shelf (here it gets warmer)

### mesoscale eddies
|  |  |
|:--:|:--:|
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_eddy_zoom/vort_tides_notides_impact_2019-07-20:2019-07-22.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_eddy_zoom/u_tides_notides_impact_2019-07-20:2019-07-22.png) 
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_eddy_zoom/v_tides_notides_impact_2019-07-20:2019-07-22.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_eddy_zoom/MLD_tides_notides_impact_2019-07-20:2019-07-22.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_eddy_zoom/SST_tides_notides_impact_2019-07-20:2019-07-22.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_eddy_zoom/SSH_tides_notides_impact_2019-07-20:2019-07-22.png) 
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_eddy_zoom/SSS_tides_notides_impact_2019-07-20:2019-07-22.png)

