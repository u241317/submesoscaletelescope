# Temperature Spectra (Spatial)
## Spectra North Atlantic vs South Atlantic 
* comparison with run 8
* here run 8 in Feb/March is evaluated and compared to spectra of NATL run

| run | region | Spectra
:--:|:--:|:--:
NATL 2010-03-21 | ![](../../../../../notebooks/images/model_evaluation/sst/exp1/images/horizontal/vis_reg_sat.png)  | ![](../../../../../notebooks/images/model_evaluation/sst/exp1/images/horizontal/sst_sec_ave_all.png)
SATL 2010-03-21 | ![](../../../../../notebooks/images/model_evaluation/smt_wave/sst/tides/images/horizontal/vis_reg.png)  | ![](../../../../../notebooks/images/model_evaluation/smt_wave/sst/tides/images/horizontal/sst_sec_ave_all.png) 
SATL 2010-03-13 | ![](../../../../../notebooks/images/model_evaluation/smt_wave/sst/tides_sa/images/horizontal/vis_reg_sat.png)  | ![](../../../../../notebooks/images/model_evaluation/smt_wave/sst/tides_sa/images/horizontal/sst_sec_ave_all.png) 
