# SSH spectra in NA and SA
## NA
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/spectra_ssh_uchida_na.png) 
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/spectra_ssh_uchida_na_notides.png) 
* only models without tidal forcing

## SA 
<!-- ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/spectra_ssh_uch1da_last6weeks_SA.png)  -->

![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/spectra_ssh_uch1da_pies_SA_25real.png)
* 5 PIES data sets:
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/spectra_ssh_pies.png)



## Spectral evaluation SSH (here run 7)
* timeseries has starts from 1 week after grid interpolation (**longer simulation run needed!**)

![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/square_S32_S42_5W_10E/ssh_spectra.png) 

| present +++++++++ | reference  |
|:--:|:--:|
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/square_S32_S42_5W_10E/ssh_spectra_asp2.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/square_S32_S42_5W_10E/ssh_spectra_uchida_2023.png)

differences:
* longer timeseries in uchida
* different time periods:
    * with tides  '2019-07-09T00:00:00' until '2019-08-31T23:15:00'
    * without tides '2019-07-09T00:00:00' until '2019-08-07T06:15:00'

single domain: 1650 spatial realisations with approx: 30km spacing in region below
```python 
lon_reg1 = np.array([-5, 10]) lat_reg1 = np.array([-42, -32]) 
```

### Method: [Uchida et al. 2023](https://www.jstage.jst.go.jp/article/jmsj/advpub/0/advpub_2020-001/_pdf/-char/en)

```python
def simple_spectra(y0, samp_freq):
    y0    = y0 - y0.mean() # remove mean
    scale = 1.0/(samp_freq*(y0.size/2))
    # use hann window
    y0    = y0 * np.hanning(y0.size)
    FT    = np.fft.fft(y0, n=None, axis=- 1, norm=None) #one d discrete Fast Fourier Transfom fft
    ffs   = np.fft.rfftfreq(n=y0.size, d=1/samp_freq) # get frequencies # Return the Discrete Fourier Transform sample frequencies
    FT    = FT * np.conjugate(FT) * scale # get power spectrum

    FT  = FT[0:int(y0.size/2)]
    ffs = ffs[1:]
    # same like         f, p = signal.welch(data_interp[:,i,j], fs=24, nperseg=time, detrend='linear', window='boxcar', scaling='density', axis=0)

    return ffs, FT
```

### in depth sensitivity of length of timeseries
| 1 | 2 |
|:--:|:--:|
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/square_S32_S42_5W_10E/spectra_ssh_last7days.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/square_S32_S42_5W_10E/spectra_ssh_last14days.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/square_S32_S42_5W_10E/spectra_ssh_august.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/square_S32_S42_5W_10E/spectra_ssh_last6weeks.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/square_S32_S42_5W_10E/spectra_ssh_all.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/square_S32_S42_5W_10E/ssh_spectra_uchida_2023.png)

(all: begin= '2019-07-09T00:15:00' end= '2019-08-31T23:15:00')

* Large difference when including 2nd week > most likely due to initialization problems
* last 6 weeks and 4 weeks do not differ muc > 3rd weeks is okay for evaluation

## Convergence of SSH spectra
### variable length of timeseries
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/convergence/spectra_ssh_convergence.png)

### Finding the bias week
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/convergence/spectra_ssh_convergence_exp7_1week_start0709.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/convergence/spectra_ssh_convergence_exp8_1week_0201.png)
<!-- ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/convergence/spectra_ssh_convergence_exp7_start3rd_week.png) -->
<!-- ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/convergence/spectra_ssh_convergence_exp7_start4th_week.png) -->



