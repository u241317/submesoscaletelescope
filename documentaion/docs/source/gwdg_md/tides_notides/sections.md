# Sections
* at 5th week

### Greenich Meridian
#### Large Section no tides (upper) vs tides (lower)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/notides_sec_wb_prime_nps3000_0E60S_0E10S.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/tides_sec_wb_prime_nps3000_0E60S_0E10S.png)

#### Intermediate Section no tides (upper) vs tides (lower)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/notides_sec_wb_prime_nps3800_0E45S_0E20S.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/tides_sec_wb_prime_nps3800_0E45S_0E20S.png)

#### ZOOM no tides (upper) vs tides (lower)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/notides_sec_wb_prime_nps3800_0E45S_0E20S_zoom.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/tides_sec_wb_prime_nps3800_0E45S_0E20S_zoom.png)

### 10 E
#### Large Section no tides (upper) vs tides (lower)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/10E/notides_sec_wb_prime_nps3000_10E60S_10E10S.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/10E/tides_sec_wb_prime_nps3000_10E60S_10E10S.png)

#### Intermediate Section no tides (upper) vs tides (lower)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/10E/notides_sec_wb_prime_nps3800_10E45S_10E20S.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/10E/tides_sec_wb_prime_nps3800_10E45S_10E20S.png)

#### ZOOM no tides (upper) vs tides (lower)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/10E/notides_sec_wb_prime_nps3800_10E45S_10E20S_zoom.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/sensitivity/section/10E/tides_sec_wb_prime_nps3800_10E45S_10E20S_zoom.png)
