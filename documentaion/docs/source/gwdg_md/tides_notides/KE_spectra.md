# Kinetic Energy Spectra in SA

* Mooring Data is depth averaged, to obtain a robust spectra 2 Week time sequences are averaged >> drawback no low freq. eval
*  1 Mooring with 1/15min and one with 1/30min sample freq

![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_mooring.png)
<!-- ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/hor_KE_spectra_obs.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/hor_KE_spectra_obs_98.15.png) -->
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/KE_spectra_50m.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/KE_spectra_100m.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/KE_spectra_dmean.png)

![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/hor_KE_spectra_obs_1008.95.png)

## Mooring DATA Sonett Cruise
### TimeSeries
Moorings
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/tseries/tseries_KE_moorings_all.png)

| 7/9 | 8 |
|:--:|:--:|
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/tseries/tseries_KE_e7_e9.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/tseries/tseries_KE_e8.png)
### Mooring 1
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/obs_spectra_m1.png)
### Mooring 2
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/obs_spectra_m2.png)
---

## Important remarks
* EXP8 timeseries covers only two months
* the selection oof the domain reveals a strong sensitivity; eg. narrowing down the domain to a 5 3 or 1 degree radius around the averaged mooring position changes the spectra significantly
* in a small domain the occurance of agulhas eddies varies strongly between Exp7/9 and Exp8
* an agulhas eddie leads to a shift of approx an order of magnitude in the spectra over all scales!
*


| 7 | 8 | 9 |
|:--:|:--:|:--:|
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/sections/spectral_section_exp7.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/sections/spectral_section_exp8.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/sections/spectral_section_exp9.png) 
* differences apper mainly because in exp7/9 an eddie is sliced and in exp8 not

---
## Domain Sensitivity

| Domain | Spectra |
|:--:|:--:|
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/domain_s.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/KE_spectra_s.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/domain_m.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/KE_spectra_m.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/domain_l.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/KE_spectra_l.png)

### aperatura
| Domain | Spectra |
|:--:|:--:|
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/domain_m_aps.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/KE_spectra_m_aps.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/domain_m_apm.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/KE_spectra_m_apm.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/domain_m_apl.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/KE_spectra_m_apl.png)

### KE Exp8 vs Exp7

| Exp7 | Exp8 |
|:--:|:--:|
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/domain_m_apm.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/res_var/imbuto_di_dante/domain_m_apm_e8.png)


* The Agulhas retroflection area is orders of magnitude more energetic than the rest of the domain!
* Sensitivity is high along the Agulhas path
* in ring path the sensitivity also dependes on the aperture of the domain
* Problem: What do we want to evaluate? Spectra at Agulhas ring after travelling / comparsion to Mooring data more realsitic including rings and no rings...

## Spectra of Single Eddy
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/inside_eddy/Eddy_ID_0_KE_100m.png)


---
## Selection of circle around mooring pos
* changing circle size and influence

| 3 | 2 | 1 |
|:--:|:--:|:--:|
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mooring_pos/single_pos_at_mooring_3.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mooring_pos/single_pos_at_mooring_2.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mooring_pos/single_pos_at_mooring_1.png)
| 0.5 | 0.1 | domain |
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mooring_pos/single_pos_at_mooring_0.5.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mooring_pos/single_pos_at_mooring_0.1.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mooring_pos/domain_2.png)

### circle around mean mooring pos 
* mean over large depths

| 5 | 3 | 1 |
|:--:|:--:|:--:|
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/KE_spectra_dmean_close.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/KE_spectra_dmean_3deg.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_var/KE_spectra_dmean_1deg.png)

### optimal selection
| domain | spectra |
|:--:|:--:|
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/domain_580m.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/KE_spectra_dmean_580_e79_alexa.png)


----
### Spectral evaluation hor. kin. energy
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/KE_spectra.png)
* 2 h output, above has 1 h output
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/KE_spectra_NA_Tasmania.png)
reference: https://hwpi.harvard.edu/files/carlwunsch/files/annurev2efluid2e402e1114062e102139.pdf

* different x and y axis scaling

### Spectral evaluation KE of vertical Velocity
* at 50m depth
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/vertical_KE_spectra.png)

* surface to 50m depth
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/smtwv0007_spectra_KE_w_2d.png)

More vertical KE in towards the mid of the Mixed Layer. 

#### TKE timeseries
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/tke_timeseries.png)



