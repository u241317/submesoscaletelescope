# Kinetic Energy Spectra at Mooring Location

* challange is to compare spectra at single location (2Year time series) with model (1/2Year timeseries)
* here simple Spectra from xrft (Uchida) is used in order to compare to model data which uses same method
* timeseries is segmented to average and obtain robustness
* 1 Mooring with 1/15min and one with 1/30min sample freq
* M1 has 16 and M2 has 32 depth levels
* gaps are fille with linear interpolation (large gaps introduce a weaker low frequency)
* many gaps close to water when eddies are present

## general overview

M1
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/obs/spectra_KE_M1_40.png)
M2
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/obs/spectra_KE_M2_40.png)

Comparison:
* including all depth levels
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/obs/spectra_KE.png)

+ subsampled for comparison 
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/obs/spectra_KE_subsampled_ave.png)
> Why is spread of M2 larger than M1?

+ depth averaged 
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/obs/spectra_KE_depth_averaged.png)


## impact subsampling on high freq.
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/obs//spectra_KE_subsampleinf_t40.png)
+ subsampled spectra is slightly weaker at intermediate scales
* M1 and subsampled M2 agree well except towards high freq. end

### EXP7 time max 1h Sep 19 - Jan 20
| 60min | 30min | max1 |
----|----|----
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mod_obs/subsampled/KE__98.15_1_30.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mod_obs/subsampled/KE__98.15_0.5_30.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mod_obs/subsampled/KE__98.15_max_30.png)


### present optimum
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mod_obs/subsampled/KE__98.15_0.5_30.png)
* at 100m depth 
* mooring averaged 2 depth levels
* subsampled to 30min
* 2Year time series observations and 1/2Year time series model

## depth dependence of Model KE Spectra
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mod_obs/KE_depths_0.5_30.png)

## Seasonality?
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/mod_obs/KE_seasons.png)
> not robust enough...