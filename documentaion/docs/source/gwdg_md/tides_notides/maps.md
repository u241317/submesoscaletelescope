# Maps

## Map on focus region
$\overline{w^\prime b^\prime}$  of 2019_07_T05_T12 (2nd week after spinup)
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/na_zoom_wb_prime.png)
and of 2019_07_T31H07_08_T07H05 (5th week after spinup)
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/sa_zoom_wb_prime_2019_07_T31H07_08_T07H05_tides.png)
### snapshot within 2nd week
TKE
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/na_zoom_tke2_new3.png)
$M^2$
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/na_zoom_m2_new3.png)
### week 2nd week
TKE
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/na_zoom_tke_mean_new3.png)
$M^2$
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/na_zoom_m2_mean_new3.png)
MLD
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/zoom_mld_mean.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/mld_mean.png)


## Difference of wb prime variable of 2019_07_T31H07_08_T07H05 (5th week)
no tides
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/sa_zoom_wb_prime_2019_07_T31H07_08_T07H05_notides.png)
tides
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/sa_zoom_wb_prime_2019_07_T31H07_08_T07H05_tides.png)
* what happens in depth?

bias
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/sa_zoom_wb_prime_2019_07_T31H07_08_T07H05_bias.png)
* tidal impact broadens the shape of fronts?! (figure is not helpfull)

## Sections
### at 5th week
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/slice_tidenotide.png)
* small buoyancy signal below 200m depth
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/slice_tidenotide_deep.png)

The buoyancy fluctuation does not seem to differ between the tidal and non tidal run in the Mixed Layer (tides filtered out in decomposition?). However below the MLD the buoyancy fluctuation is larger in the non tidal run. (counterintuitive)
From 2nd week tidal run we see that thhe buoyancy fluctuation is much stronger and dominantly postive over the entire water cloumn. Assumption: due to initial conditions from switch of resolutions.
Why does the buoyancy fluctuation in the non tidal run remains/(not cease) below the MLD? 

### slice at 2nd week
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/slice_mean2.png)

$\overline{w^\prime b^\prime}$ deep slice 
![](../../../../../../submesoscaletelescope/results/smt_wave/high_res/sa_overview/wb_prime_slice_deep.png)
* strongly positive buoyancy fluctuation MLD (due to initial conditions?) in later stage tide run does not show this behaviour
