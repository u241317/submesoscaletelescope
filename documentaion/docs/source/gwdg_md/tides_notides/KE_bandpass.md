# Band pass Filtered KE Fileds

### decomposed field
full | low | high
--|--|--
![](../../../../../../submesoscaletelescope/results/smt_wave/bandpass_filtered/KE_anti&cyclone.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/bandpass_filtered/low_pass_0.17_anti&cyclone2.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/bandpass_filtered/high_pass_0.17_anti&cyclone2.png)

## upper anticyclone tracked
anticyclone | cyclone
---|---
![](../../../../../../submesoscaletelescope/results/smt_wave/bandpass_filtered/high_pass_0.17_anticyclone.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/bandpass_filtered/high_pass_0.17_cyclone.png)
anticyclone | cyclone

![](../../../../../../submesoscaletelescope/results/smt_wave/bandpass_filtered/high_pass_0.17_anti&cyclone_3.png)


at 100m
![](../../../../../../submesoscaletelescope/results/smt_wave/bandpass_filtered/TKE_anti&cyclone_52.png)
at 50m
![](../../../../../../submesoscaletelescope/results/smt_wave/bandpass_filtered/TKE_anti&cyclone_52_50.png)

MLD
![](../../../../../../submesoscaletelescope/results/smt_wave/bandpass_filtered/TKE_anti&cyclone_mld.png)