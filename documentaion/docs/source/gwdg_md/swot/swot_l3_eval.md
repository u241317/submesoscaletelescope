# SWOT spectral evaluation
This evaluation is based on SWOT L3 product. The L3 product is a postprocessed product, which includes gap filling, data cleaning, flagging and corrections. The L3 product is available from AVISO+.

L3 description link: [SWOT L3](https://www.aviso.altimetry.fr/fileadmin/documents/data/tools/swot_l3_faqL3alphav0_3_rev1.pdf)

## Idea:
Use SWOT data to evaluate the SSHA spectra of ICON SMT. As a further reference ICON R2B8 is included. Full swot satellite path crossing the study area/zoom region are selected. Each swath has 69 alongtrack profiles with a 2km spacing in alongtrack direction. ICON data is sampled along the tracks, thus SMT is subsampled and R2B8 (approx 10km res) oversampled. SWOT data from 21 days in March 2024 is used. ICON data from 21 days in March 2010.

All samples are used to compute an average spectra. Single swath spectra are shown for camparison. 

### Domain
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l3/icon_swot_swaths/swot_map_large.png)


### Spectra of SWOT, SMT and R2B8:
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l3/icon_swot_swaths/spectra_select.png)

### Selected Swath of SWOT, SMT and R2B8:
tabular
|swot|smt|r2b8|
|:---:|:---:|:---:|
|![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l3/icon_swot_swaths/swot_map.png)|![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l3/icon_swot_swaths/smt_map.png)|![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l3/icon_swot_swaths/r2b8_map.png)|