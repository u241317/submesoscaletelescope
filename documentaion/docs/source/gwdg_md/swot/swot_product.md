# SWOT Introduction
get data using podac with domain selection
``` ! podaac-data-downloader -c SWOT_L2_LR_SSH_Basic_2.0 -d ./north_atlantic_basic -sd 2024-03-05T00:00:00Z -ed 2024-03-26T00:00:00Z -b="-80.0,20,-40,45" --subset ```
get AVISO+ data with aviso account:
```sh
module load lftp/4.8.1-gcc-11.2.0
lftp ftp-access.aviso.altimetry.fr
lftp ftp-access.aviso.altimetry.fr:~> lftp -u moritz.epke@uni-hamburg.de ftp://ftp-access.aviso.altimetry.fr/swot_products/l3_karin_nadir/l3_lr_ssh
# then mget or get to download files
```

## L2 products
How to use SOWT data to evaluate the spectra?

vis with scatter
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/multi_swot_globe.png)

vis with dshader
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/dshader_plot_map.png)

### interpolate on reg grid
raw|intep
:---:|:---:|
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/on_reg_grid.png) | ![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/on_reg_grid_interp_lat.png)

#### atrifacts due to interpolation:
can also be seen in tseries
lon|lat
:---:|:---:|
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/spectra_on_reg_grid.png) | ![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/spectra_on_reg_grid_lat.png)

Artifacts in spectra are dominant, therefore a regular grid interpolation is not suitable for spectral evaluation. Next approach is to to Spectra on "native grid". These howver contain gaps and errors.


### alongtrack spectra of single swaths
* cut of rows of nans
* fill remaining gaps with linear interpolation
* !! reminder: large gaps filled with linear interpolation lead to weaker low frequency !!

Single swath with filled gaps:
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/single_swath_vis.png)

Spectra computed from both wings
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/single_swath_spectra.png)

* select only swath with full domain coverage
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/swath_map.png)

mean spectra from all swaths crossing domain of interest
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/mean_spectra.png)

### comparison to earlier spectral evaluations:
* differences in domain selection; swaths are zonal and meridional
* "old Aviso" satellite product is L3 gridded data product
* zonal and meridional spectra differ in slope and amplitude

zonal|merid
:---:|:---:|
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/zonal.png)|![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l2/meridional.png)

selfmade L3 product does not lead to a satisfactory spectra; lets check AVISO+ L3 products

## L3 products
* L3 products include postrpocessing (filling gaps and cleaning data, applying falgs and corrections)
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l3/swath_map.png)

avilable L3 products (relevant for spectra) from Aviso plus:
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l3/all_spectra.png)
see also link to [details](https://duacs.cls.fr/faq/what-are-the-product-specification/altimeter-reference-period-and-absolute-reference/)
> l3 products also contains geostrophic and ageostrophic velocities
> assumptions SSHA is SLA
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l3/single_swath_spectra.png)

### comnparison to earlier spectral evaluations:
zonal|merid
:---:|:---:|
![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l3/zonal_spectra.png)|![](../../../../../../submesoscaletelescope/results/smt_natl/swot/l3/meridional_spectra.png)

> note that our L2 product evaluation is very close to L3
> results are poor: maybe evaluation of model data along same swaths is more suitable

#### notes
* full swath from south to north appears to has a more suitebale spectra "straight line"
* to evaluate nadir: how to select data? whats the spacing?
