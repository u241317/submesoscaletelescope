# Resolution of SMT, R2B8 and sst satellite data

## Global resolution
* satellite data not available past +-75° latitude, see green dotted line
* satellite resolution here is just horizontal whereas ICON resolutions are shown as sqrt(cell area)

![](https://pad.gwdg.de/uploads/84e5ddae-eca0-491a-90a4-9ea7f1743f93.png)
![](https://pad.gwdg.de/uploads/f4e06048-0657-47a5-a0a2-cdb70ee5d63b.png)
![](https://pad.gwdg.de/uploads/ad530423-1172-4576-bb1e-05e54391a8a1.png)
### Telescope
zoom1 smt
![](https://pad.gwdg.de/uploads/934c2e07-0916-415c-b545-b4f0c2d1289f.png)
zoom2 smt
![](https://pad.gwdg.de/uploads/3bf19810-bd5b-44da-8dc1-44df32be0e28.png)



## Resolution along zonal sections
Motivation: interpolate to section with equidistant spacing to enable easy use of FFT and analysis of spectral estimates

method: application of pyicon cdktree section, which makes use of a nearest neighbour method

![](https://pad.gwdg.de/uploads/8e34b7e3-ea48-4a09-aff6-675a4b292289.png)
![](https://pad.gwdg.de/uploads/f76edb9d-a5ec-43d1-897d-205cd1ab0ddd.png)

resolution of section with equidistant spacing should be smaller then original resolution to avoid double sampling

>Resolution at zonal section at 31.9°N
![](https://pad.gwdg.de/uploads/ed331ff5-fbf2-4351-8053-92851ca4801e.png)
