# Abstract

Submesoscale Instabilities in the mixed layer can lead to ocean re-stratification, and thus affect ocean-atmosphere feedbacks. 
This study uses a novel configuration of the ICON ocean model that exploits telescoping grid refinement to achieve a horizontal resolution finer than 600 m over wide areas of the North Atlantic. 
The ability of the model to simulate mesoscale to submesoscale turbulence is validated by comparing spatial power spectra of sea surface temperature and sea surface height with those of satellite products and an ICON simulation with 10 km horizontal resolution (often referred to as “mesoscale eddy resolving”).
We find more realistic variability in the refined grid simulation compared to the coarser simulation over a wide range of scales, including the mesoscale eddy regime. Furthermore, the high-resolution permits submesoscale baroclinic instabilities. 
At single fronts, we observe strong overturning re-stratifying the fronts. 
Overturning rates are diagnosed in terms of mean front characteristics such as mean horizontal and vertical density gradients and mixed layer depths. 
Finally, the diagnosed overturning rates are compared with recent parameterizations introduced by Stone (1971) and Fox-Kemper (2008). 
It turns out, that both parameterizations can capture the bunch of the submesoscale overturning. 
Extending the evaluation to a whole region, including non-frontal regions, leads us to the distinguish between a small and a  large Ri regime.
For the small Ri regime, we observe a Ri dependence closer to Stone than to Fox-Kemper.
However, the diagnosed overturning has a large spread which leads to significant uncertainties.
