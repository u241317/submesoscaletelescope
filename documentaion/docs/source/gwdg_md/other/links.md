#  **Important links**
## 
### VsCode
[VsCodeIntro](https://pad.gwdg.de/GSW4dgHZSGm6KUev6mQcdw#)

### Mistral inroduction
[MistralIntro](https://pad.gwdg.de/9oAvIXuaQlmqNBci43kEyw?both)

###   **TRR181**
Internal Area:
[https://www.trr-energytransfers.de/internal-area](https://)

Coffee Chat:
[https://discord.com/invite/D5aCEmut](https://)

Link and Package Collection:
[Spreadsheets](https://docs.google.com/spreadsheets/d/1dv1PexeQtPxDYeVPF1pABNdm_5S9W0-CKQ3zvj4V3UY/edit#gid=0
)

###   **MPI**
ICON Wiki:
[wiki.mpimet](https://wiki.mpimet.mpg.de/
)

Website:
[mpimet.mpg](https://mpimet.mpg.de/
)

Mail:
[email.gwdg.de](https://email.gwdg.de
)

###   **Documentation**
Latex:
[https://www.overleaf.com/](https://)

###   **Python**
**Tutorials & Introductions:**
Nils:
[/s/JaWydV0u7#Installing-your-own-python-environment-with-conda](https://)

Rabernat:
[https://rabernat.github.io/research_computing/](https://)
[https://rabernat.github.io/research_computing/python.html]

DKRZ:
[https://www.dkrz.de/up/systems/mistral/programming/python]

###   **Organization**
[studium/promotion](   https://www.geo.uni-hamburg.de/de/studium/promotion.html )