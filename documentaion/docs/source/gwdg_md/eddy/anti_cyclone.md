# Anticyclones and Cyclones

## Anticyclones ID0 - ID5
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID0/N2.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID1/N2_ID1.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID2/N2_ID2.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID3/N2_ID3.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID4/N2_ID4.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID5/N2_ID5.png)
## Cyclones ID0 - ID4 
**Eddy 1 and 3 are the same at different times**
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID0/N2_ID0.png)
<!-- ![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID1/N2_ID1.png) -->
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID2/N2_ID2.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID3/N2_ID3.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID4/N2_ID4.png)

> white color indicates negative stratification, also seems to match small wavenumber pattern in bandpassed filtered images. Also, ID0 is the only eddy with a mixed layer from surface to eddy pycnocline - in contrast to the other eddies which have second MLD roughly 100m.

## KE Spectra at 126m
Anticyclones
|ID0|ID1|
|:-----:|:-----:|
![ID0](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID0/spectra_126_0.png) | ![ID1](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID1/spectra_126_0.png)
|ID2|ID3|
![ID2](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID2/spectra_126_0.png) | ![ID3](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID3/spectra_126_0.png)
|ID4|ID5|
![ID4](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID4/spectra_126_0.png) | ![ID5](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID5/spectra_126_0.png)

Cycolnes
|ID0|ID2|
|:-----:|:-----:|
![ID0](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/cyclonic/ID0/spectra_126_0.png) | ![ID2](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/cyclonic/ID2/spectra_126_0.png)
|ID3|ID4|
![ID3](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/cyclonic/ID3/spectra_141_0.png) | ![ID4](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/cyclonic/ID4/spectra_126_0.png)
> Anticyclone ID0 has strong wide peak up to eddy freq. while all other spectra show weaker energy towards lower freq. / above diurnal band. Signal is most likely result from interaction with topography. Why is IDO anticyclone the only one not showing peaks at high freq? Does topography interaction generate wave over the entire internal wave band?

### Topography interaction
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID0/topo/spectra_126_topo.png)


##Anticyclones ID0, ID1, ID2
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID0/KE.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID1/KE_ID1.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID2/KE_ID2.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID3/KE_ID3.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID4/KE_ID4.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID5/KE_ID5.png)
## Cyclones
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID0/KE_ID0.png)
<!-- ![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID1/KE_ID1.png) -->
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID2/KE_ID2.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID3/KE_ID3.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID4/KE_ID4.png)


## Anticyclones ID0, ID1, ID2
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID0/TKE.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID1/TKE_ID1.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID2/TKE_ID2.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID3/TKE_ID3.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID4/TKE_ID4.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID5/TKE_ID5.png)

## Cyclones 
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID0/TKE_ID0.png)
<!-- ![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID1/TKE_ID1.png) -->
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID2/TKE_ID2.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID3/TKE_ID3.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/cyclonic/ID4/TKE_ID4.png)