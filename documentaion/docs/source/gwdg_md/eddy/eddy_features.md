# Eddy features
## Overview

![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/eddy_tracks_l_cyc.png)
* here Anticyclone with ID 0 is shown
## Eddy slices:
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID0/N2.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID0/KE.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID0/TKE.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/anticyclonic/ID0/w.png)


## R2B9 control run
|R2B9|SMT|
|:---:|:---:|
![](../../../../../../submesoscaletelescope/results/r2b9/reference_run/r2b9_vort0.png) | ![](../../../../../../submesoscaletelescope/results/r2b9/reference_run/smt_vort0.png)
![](../../../../../../submesoscaletelescope/results/r2b9/reference_run/r2b9_vort400.png) | ![](../../../../../../submesoscaletelescope/results/r2b9/reference_run/smt_vort400.png)
![](../../../../../../submesoscaletelescope/results/r2b9/reference_run/r2b9_vort2000.png) | ![](../../../../../../submesoscaletelescope/results/r2b9/reference_run/smt_vort2000.png)
* same initial state
* SMD emerge quickly

## Eddy Spectra
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID0/mask_51.png)
* red circel definse outside/inside eddy region

*preliminary*
|0|1|
|:---:|:---:|
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID0/spectra_1_0.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID0/spectra_26_0.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID0/spectra_52_0.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID0/spectra_126_0.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID0/spectra_356_0.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/spectra/anticyclonic/ID0/spectra_1671_0.png)

* eddy freq weakens with depth and time
* N should be included in the spectra
* what can we learn?


## Bandpassed filtered
### Hovmüller Plot
Locations of plots:
![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/hovmuller/depth_time/eddy_lon_lat.png)

type|center|rim|left|
|:---:|:---:|:---:|:---:|
diurnal |![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/hovmuller/depth_time/bandpass/diurnal_freq/KE_exp7_lon90_lat90_bandpass_zonal.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/hovmuller/depth_time/bandpass/diurnal_freq/KE_exp7_lon60_lat90_bandpass_zonal.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/hovmuller/depth_time/bandpass/diurnal_freq/KE_exp7_lon10_lat90_bandpass_zonal.png)
hifreq | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/hovmuller/depth_time/bandpass/hifreq/KE_exp7_lon90_lat90_bandpass_zonal.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/hovmuller/depth_time/bandpass/hifreq/KE_exp7_lon60_lat90_bandpass_zonal.png) | ![](../../../../../../submesoscaletelescope/results/smt_wave/spectra/KE/hovmuller/depth_time/bandpass/hifreq/KE_exp7_lon10_lat90_bandpass_zonal.png)

* enhanced bandpassed energy for both near inertial band and hi frequencies at negative vorticities
* no energy at center! likely due to weak stratification
* snake like weak energy features in rim might be due to positive vorticity created when eddy interacts with vema crest (see vorticity videos)

### unfiltered Hovmüller plots
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/circle.png)

**Temperature**
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_to_1.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_to_52.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_to_126.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_to_356.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_to_1399.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_to_2810.png)

**KE**
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_KE_1.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_KE_52.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_KE_126.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_KE_356.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_KE_1399.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_KE_2810.png)


**more**
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_zos_0.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_tke_122.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/hov_w_348.png)

**filtered**
**temperature**
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/bandpass/hov_to_0.04_0.05_52.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/bandpass/hov_to_0.07_0.10_52.png)

**filtered KE**
|1M0|1M2|2M2|
|:---:|:---:|:---:|
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/bandpass/hov_KE_0.04_0.05_52.png) |![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/bandpass/hov_KE_0.07_0.10_52.png) |![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/bandpass/hov_KE_0.14_0.20_52.png)
![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/bandpass/hov_KE_0.04_0.05_1399.png) |![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/bandpass/hov_KE_0.07_0.10_1399.png) |![](../../../../../../submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/bandpass/hov_KE_0.14_0.20_1399.png)
