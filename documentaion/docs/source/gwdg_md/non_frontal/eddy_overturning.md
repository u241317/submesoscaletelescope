# Overturning at eddy rim
> animation of Eddy 1
![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/eddy_front.gif)


![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/search_eddy_overview_n7.png)

![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/search_eddy_n3.png)

![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/band_mean_3.png)

![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/flux_streamfunc_3.png)

## Cyclones
Eddy 1
 ![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/streamfunc_comp_0.png)
 > full mismatch of streamfunc

Eddy 3
![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/streamfunc_comp_2.png)
> super round eddy, no overturning diag 

Eddy 4
![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/streamfunc_comp_3.png)
> best match 

Eddy 5
![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/streamfunc_comp_4.png)
> not unstable enough

Eddy 6
![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/streamfunc_comp_5.png)
> not unstable enough (weak)

## Anticyclones
Eddy 2
![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/streamfunc_comp_1.png)

Eddy 7
![](../../../../../../submesoscaletelescope/results/smt_natl/non_frontal/streamfunc_comp_6.png)