# **Report June**
##
##  Aim of Evaluation:
+ gain trust in SMT NATL ICON Model
+ in best case: evaluate high resolution feature of model
+ 
## Evaluation approaches:
procesed observation and reanalysis data:
* sst modis aqua
* ssh aviso
* MLD by ARGO Floats (Holte Climatology)
* salinity EN4
* surface velocity ORAS5

##  bias figures
data for comparison interpolated to a 0.3 degree regular grid
###  SST with modis aqua
![](https://pad.gwdg.de/uploads/482d400b-b93f-4aa7-9e26-f36c0ab578da.png)
Quantitative evaluation of SST
model issues:
+ second meandering jet - does it in reality exists?
+ GS detaches to far north (seems to be a typical modelling problem)
+ therefore sensible heat fluxes are to strong (they make use of climatology data, if the GS detaches at the wrong place the temperature gradients between atmosphere and ocean are wrong) > maybe a reason for the shift in fft of sst
+ sst evaluation in ocean model with forcing "inapropriate" due to restoring (you directly compare model results with forcing)
+ evaluation of model sst at 2.5m depth vs. satellite at sstskin at 10 micrometer
###  Surface Salinity with EN4
![](https://pad.gwdg.de/uploads/60b0adf1-bc31-45bd-8826-5da18e99292a.png)
* 1 degree reanalysis data
###  ssh with Aviso
+ global mean substracted
![](https://pad.gwdg.de/uploads/bc4ed6f4-d306-4819-8943-aa9ffdabfd3c.png)
###  mean surface flow with oras5 reanalysis data
![](https://pad.gwdg.de/uploads/94713548-5eef-4b4f-8b15-e5c7132084e1.png)

##  MLD
Here the Mixed lay depth of the ICON-SMT, ICON-R2B8 models and an argo float climatology of Holte et al. 2007 are evaluated. Three zonal sections through the north atlantic at 32, 35 and 38 degrees N are investigated. To compare the mixed layer depth only monthly means are evaluated.
Issues:
differences to R2B8 which make comparison difficult
* TKE vertical mixing scheme
* different calculation for windstress
* different grid diffusion
#####  comparison of 3 sections 32°N, 35°N, 38°N
>>>>>![](https://pad.gwdg.de/uploads/e05c6709-c337-41c2-abec-1348f61f44c4.png)
![](https://pad.gwdg.de/uploads/ed8565aa-d01b-48c6-820c-c8cbdd643502.png)

##  sst
In the following section the spatial spectral estimates of the sst are evaluated. Here data from the ICON SMT run will be compared with a coarser ICON R2B8 run and sst satellite data from Modis Aqua. Due to strong cloud cover over the north atlantic it is much more challenging to get a robust spectral estimate. Therefore averaging spatial spectras of different temporal realizations is not possible. To obtain more robust spectral estimates averages are taken of different spatial realizations.  The resolution of the two models and satellite data differ R2B8 (approx 10km), satellite (approx 4km in focus region) and SMT (approx 550 - 700 m in focus region). In order to have the same amount of realizations the satellite resolution is used to spatially average satellite and smt data. Consequently less realization can be used to investigate the spectra of the R2B8 Model (approx half), which in turn lead to weaker spectral estimates. Since the satellite data is stored on a rectangular grid only zonal and meridional sections are considered (a constant spacing is crucial for spectral analysis. !!! meridional sections do not have the exact same spacing - maybe one could say over a few degrees the impact is neglegible !!!). 

Method:
+ Only few days with small cloud cover could be found during the period of interest (one could investigate same season in different years...). 
* since there is no timeseries only spatialy averages are taken of one timesnapshot
* here 30 neighbouring sections in each region (spacing of satellite resolution, only 15 neighbours for r2b8 data (much coarser...))
* at least for the models more robust spectral estimates could be calculated (What is more important: same method, or the best spectral estimate of each dataset?)
* visualization needs to be adpted since estimates are less robust then ssh data (here more temporal realizations enable averaging of spectral estimates) and representation of errors/std/ci in loglog plots become ugly >> see example below
* r2b8 run uses same mixing scheme (TKE) like smt but started from a different spinup

* The Observation data determines the region, which can be analysed.
* Since the data gaps are different in every section (but less then approx 20 %) and gaps are filled with linear interpoaltion: Should these gaps be quantified?
* Is one timesnapshot of satellite data sufficient to make conclusions?


+ Selection of largest connected dataset in focus region.
+ smt and r2b8 data selected using nearest neighbour method (number of points selected with respect to resolution of data)
+ NaNs are filled using linear interpolation
+ detrand data by constant
###  zonal
+ strange (several archs in spectra) spectral estimates are flagged: FLAGS at region C, n =0 and excluded from averaging
![](https://pad.gwdg.de/uploads/eef143c0-2218-40b4-b4c0-2169ec3101d8.png)


![](https://pad.gwdg.de/uploads/b491f06a-ff21-4ee1-b86c-82b75c0f774b.png)
* PSD of R2B8 run is one order weaker
* PSD of SMT run has the same order (here in all regions weaker) like PSD of statellite, although slope is steeper
* R2B8: Why does the slope becomes shallower towards higher resolutions? - Where is grid diffusion? >> due to Interpolation?

![](https://pad.gwdg.de/uploads/48e1ddd2-27fd-4eb8-964c-efeb000bae94.png)
* PSD and slope of SMT model are closer to satellite data then coarser R2B8 model - impact of increased hor. resolution/diff. spinup/or other?

* there might be a latitude dependence of the overall power density, colorbar viridis >> shift in PSD, larger spread of data points compared to meridional eval. it increases towards larger latitudes
* not as clearly visible as in ssh (latitude steps are much smaller here)

* Why are we not seeing higher magnitudes towards larger wavenumbers?
###  meridional
* spacing is not constant!!! - spacing in degree however is more difficult to interpret...
* + strange (several archs in spectra) spectral estimates are flagged: FLAGS region B, n =0
![](https://pad.gwdg.de/uploads/8f89196d-1976-4520-bbec-553c62fe471f.png)


![](https://pad.gwdg.de/uploads/fedcc6a1-68d8-4422-9921-ef5638b171f4.png)
* PSD of SMT run in average weaker then satellite, however in region e,f stronger, same is true for R2B8 model
![](https://pad.gwdg.de/uploads/b884aeae-8716-4a17-b738-a9b5156434bf.png)
* spacing of satellite and r2b8 data varys >> might cause shift on x axis?

##  ssh
In the following section the spatial spectral estimates of the sea surface hight ssh of the ICON-SMT model is  evaluated using AVISO satellite ssh data. There is no larger data gaps in aviso data, therefore robust spectral estimates can be computed using all temporal realiztaions.  
Method:
+ First the spectral estimate of each temporal realization is computed and then all spectral estimates are averaged (90 temporal realizatins for satellite, approx 900 for smt)
+ then spatial averages over 10 sections are taken 
###  zonal
* two flags in region f
![](https://pad.gwdg.de/uploads/20f3658d-ea29-4c52-8ec9-17114abec614.png)

![](https://pad.gwdg.de/uploads/45e9dabd-9c32-48c2-83ee-f04fbef89383.png)

![](https://pad.gwdg.de/uploads/76c4b4d8-8ccf-40b5-99dd-b83584b0b5f9.png)

###  meridional
+ no falgs necessary
![](https://pad.gwdg.de/uploads/bf48d97f-9e66-462b-8031-4a89123ba50b.png)


![](https://pad.gwdg.de/uploads/8cdad5ee-9e8f-4f7c-88a6-2c4706a98b77.png)

![](https://pad.gwdg.de/uploads/5c33ce88-9b37-4a7a-9b9b-9814a84acaf5.png)
> slope of observation data is less steep then model
> good agreement in power spectra density

+ second meridional evaluation of sst shows no dependence of the length of the section!!

