# Report July22

## calculate correlation of RI ~ w'b'
### Method:
Killworth
$$\frac{\overline{w'b'}}{H^2f^3} = C_s \frac{1}{\sqrt{1+Ri}} \mu_s(z) \alpha^2$$
$$Ri = \frac{N^2  f^2}{ M^4}$$
$$ \alpha = \frac{M^2}{f^2}$$
* data is averaged over 7 days
* M2: ``` pyton data_M2_mean = np.sqrt(np.power(data_dbdx_mean,2) + np.power(data_dbdy_mean,2))```
* averaged over MLD
* before taking the logarithm zeros, nans, and small numbers are removed

###  area1
lon_reg = [-65, -60]
lat_reg = [38.75, 41.25]
![](https://pad.gwdg.de/uploads/6ca36e66-62b5-4a67-9967-a973a6e50441.png)

###  area2
lon_reg = [-65, -60]
lat_reg = [33, 35.5]
![](https://pad.gwdg.de/uploads/ad566878-ea02-40e4-b7d6-52d37bbbd996.png)

###  area3
lon_reg_large = [-72.5, -55]
lat_reg_large = [32, 42]
![](https://pad.gwdg.de/uploads/b8157292-afb5-4baa-96c5-8f9ef5dc24a7.png)
without alpha
![](https://pad.gwdg.de/uploads/75457ed7-f459-4042-a152-3f8856516641.png)

* Is it correct that slope is changed by divison with alpha?



## search front
![](https://pad.gwdg.de/uploads/6c972f48-6e3f-4f90-89c5-3b39c4e1bb0a.png)

## selction
![](https://pad.gwdg.de/uploads/20868b7e-cb62-4ae2-9d0d-35b714bfdb66.png)

## calc selected front
* not enough points to get an appropriate pvalue! > little confidence in slope
* A front with a deeper mixed layer would give more points...!
* estimate alpha = M^2/f^2 f=1e-4 > max alpha is 8
* maybe consider an alongfront average!

![](https://pad.gwdg.de/uploads/87854bee-f6cb-4854-8a31-cdaa07a7eaf6.png)
all points above mld

![](https://pad.gwdg.de/uploads/a5d2788d-de96-42a7-8518-04db492c27df.png)
averaged over mld
![](https://pad.gwdg.de/uploads/8f2d7893-2ce3-4061-9025-59408b38a5ec.png)


![](https://pad.gwdg.de/uploads/911439e0-c220-4fa2-a229-a64179f6b2b9.png)
![](https://pad.gwdg.de/uploads/6e36b66e-bd33-4330-a410-00e3bc507f0a.png)

* daily mean data shows completely different situations
* front does not stay in one location
* 