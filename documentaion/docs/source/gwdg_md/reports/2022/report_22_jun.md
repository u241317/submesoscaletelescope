
# Report June 22
## lateral Evaluation
## Overview
![](https://pad.gwdg.de/uploads/1e4d6d1c-9e47-4019-a76d-de2a30d9e817.png)
## Eddy selesction
![](https://pad.gwdg.de/uploads/a37f4ade-a411-4479-8e52-d37ecba969c1.png)
## w b correlation and means
upper right is M2
![](https://pad.gwdg.de/uploads/8a73914c-b59e-4192-96f1-c679cd739ab6.png)
## wb_prime and Ri
![](https://pad.gwdg.de/uploads/70700427-b3a6-43b4-8c1e-40e2bd2ff7d9.png)

## MLD
+ faster way of calculation then first evaluation!!!
![](https://pad.gwdg.de/uploads/8466f70e-ebf6-4bf0-8c19-cecc268f4ae4.png)
![](https://pad.gwdg.de/uploads/f4cb412d-c74d-4b86-9cd9-45832ca9b97c.png)

## Eady Growth rate one level
![](https://pad.gwdg.de/uploads/439151f0-dd64-4476-8d1e-01571a26d2f1.png)


## Ri - wb_prime correlation of section
### one layer
![](https://pad.gwdg.de/uploads/71915be4-9e2e-4b22-af97-d85973a072e4.png)

### vertically averaged 
* vertical average over MLD
* H = MLD
* nans and zeros are removed from data
* then absolute values of xdata and ydata is used to compute the logarithm
* then remove again nans and infs
* 
![](https://pad.gwdg.de/uploads/531c2a0f-cc11-40f1-a0ab-e762378519ca.png)

## vertical Evaluation
## b and w
![](https://pad.gwdg.de/uploads/df313a41-6835-4d7f-87c0-955b791bddac.png)
## M2 and N2
with M2 = sqrt(dbdx^2 + dbdy^2)
![](https://pad.gwdg.de/uploads/341290ce-f428-4c64-8134-eeb5a3ddfb55.png)

## Ri and wb
![](https://pad.gwdg.de/uploads/cee76285-4893-4a9a-817d-34b68bba3583.png)

## Eady Growth rate
### one slice
![](https://pad.gwdg.de/uploads/87019c7d-4084-4e2e-b15c-179e972ad3c7.png)


## wb and ri correlation
filtered data nan, zeros, inf
scaling is missing
all values of vertical section included, whole depth
![](https://pad.gwdg.de/uploads/abaa8449-c0bd-46a3-9e15-7ac40a78bc97.png)
