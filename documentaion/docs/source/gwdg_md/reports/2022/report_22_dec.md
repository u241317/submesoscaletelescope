# Report Dezember

###  coarse Graining
fine 0.01grid
![](https://pad.gwdg.de/uploads/a3ac017e-1a34-4ba3-a4cb-24a4d6d209e5.png)
coarse 0.1grid
![](https://pad.gwdg.de/uploads/443967a3-548a-434b-af51-713787a5534d.png)
fine
![](https://pad.gwdg.de/uploads/9412ed15-5e0c-434c-97c5-652fd337548d.png)
coarse
![](https://pad.gwdg.de/uploads/dcfaba8b-89fc-4776-9b44-470b8e1be413.png)


##  Evaluation of inclined submesoscale fronts!
![](https://pad.gwdg.de/uploads/2d718c72-00b4-42ec-b36e-e1655d39be53.png)

####  M2 gradient at 50m depth
![](https://pad.gwdg.de/uploads/a5a3c7ba-a9e6-44de-8d53-43e6d323e848.png)
![](https://pad.gwdg.de/uploads/5a2ec98f-171c-4bf6-bdb6-289b803cf0b3.png)
####  V'b' and M2 at 100m depth
![](https://pad.gwdg.de/uploads/5c9a2648-aef2-40da-bdb7-3edb64e23d75.png)


* Gradient of Front is in 'most' of the cases crossfront -> analysis can be done with magnitude of dbdx and dbdy!
* Vector of V'b' on ocean front points into various directions, no clear pattern like the buoyancy gradient -> only crossfront part of vector or magnitdude? Can overturning as well be along the front?


###  45 fronts evaluatioon
* crossfront and alonfront split up
* thershold method for mld
* m2 is always crossfront whereas fluctuation  varys alot...

####  Tuning
here for psi
![](https://pad.gwdg.de/uploads/3cfbb4e0-4585-48d6-93af-2b89022a1507.png)
![](https://pad.gwdg.de/uploads/12ed7055-3cf9-44b0-a99f-be353481609f.png)


![](https://pad.gwdg.de/uploads/c3090e8a-ab25-4cb7-9568-8e7a057e1f31.png)
![](https://pad.gwdg.de/uploads/fe07d9f8-fc0f-45f9-b698-af6a229ebb50.png)

no filter ri wb correlation
![](https://pad.gwdg.de/uploads/ee431f7b-d1cf-4952-9517-2cd08397fc60.png)
ri < 1e3
![](https://pad.gwdg.de/uploads/0207f852-cd01-4851-a7d6-e36edde15d69.png)
ri < 1e2
![](https://pad.gwdg.de/uploads/314162e4-14e3-4896-8ef6-27e3ce4a96cb.png)

##  Questions
How to deal with varying direction of fluctuation along front (in toy models always ccrossfront) - in Full Streamfunction crossfront fluctuation is needed
* in Held Schneider Formulation horizontal fluctuation not needed!!! (diagnostic is poor using Held Schneider fromulation (eg. weak m2 > strong overturning))
* horizontal fluctuation close to surface often superimosed by other processes 
* Should the 45 front evaluation be binned? (eg. by MLD or intensisity?)