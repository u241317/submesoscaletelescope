# Report August 22



## Regions and Fronts
![](https://pad.gwdg.de/uploads/2b13442b-6d9b-4945-99f9-5a5155378bdc.png)

![](https://pad.gwdg.de/uploads/b290b502-e7e6-48a3-b8b0-1a4d74edb8cf.png)


## Net Ekmanpumping at Ekman depth
* week starts with a couple of days lasting storm event
* double check units Ekman pumping to strong?
* approximation to find regions with strong ekman pumping 
* neglects relative velocity of ocean
* its not the local vertical velocity
![](https://pad.gwdg.de/uploads/9e7a2bce-b3ab-4369-a115-4b0cde7b8a8e.png)


## Evaluation Rectangles

###  -----------Day-----------------2Days-----------Week----------
###  R0 '32<->42N, 72.5<->55W'
![](https://pad.gwdg.de/uploads/1698b9fe-4b7f-4f99-886a-9bc680ee6b61.svg)
###  R1
![](https://pad.gwdg.de/uploads/7feba6bd-0664-45d2-b21b-f3dd23b05971.svg)

###  R2
![](https://pad.gwdg.de/uploads/a632f232-b159-4277-a303-fff6afa7a206.svg)
###  R3
![](https://pad.gwdg.de/uploads/143ddab7-7287-45cf-a725-5a60175e9f12.svg)
###  R4
![](https://pad.gwdg.de/uploads/99df46ed-4b0a-4e40-a47a-4284c710f711.svg)
####  interpolated (spatial mean) to 0.1 degree grid
![](https://pad.gwdg.de/uploads/0557f2e3-6746-4191-bc67-a2a1b867a655.svg)
 ####  interpolated (spatial mean) to 0.3  degree grid
 ![](https://pad.gwdg.de/uploads/07731772-ba70-4be4-b170-c308bd676f34.svg)


###  Evaluation Front
###  -----------Day-----------------2Days-----------Week----------
###  F1
![](https://pad.gwdg.de/uploads/20d6eed8-0d0f-402f-b9d8-e0cb897eae00.svg)
###  F2
![](https://pad.gwdg.de/uploads/7ce9d764-2b90-45be-b898-7d3d305a6555.svg)
###  F3
![](https://pad.gwdg.de/uploads/015549dc-bbab-4ddf-9bc4-5ac9c09629be.svg)
###  F4
![](https://pad.gwdg.de/uploads/28e30ab2-f096-4f89-8553-4c90ca951374.svg)

* slope gets steeper for larger time averages

Another View:
---

![](https://pad.gwdg.de/uploads/edbd8396-55e5-40b3-b909-8f4d7a915ae5.png)
![](https://pad.gwdg.de/uploads/43a23f63-6b51-4e00-8891-4cce78da3822.png)

###  Check MLD
* is there a dominant mesoscale background? mld more like 450m
at lat mid averaged over lon
![](https://pad.gwdg.de/uploads/a5c6ab59-1227-4e43-bbdb-5d2d15e7adf8.png)

###  Domain slope
![](https://pad.gwdg.de/uploads/0d557c3f-0c67-4846-9405-3d67149d439b.png)


###  Alongfront view
![](https://pad.gwdg.de/uploads/de215af2-4a29-4a54-a8d6-72f76d006ce5.png)
![](https://pad.gwdg.de/uploads/910e2807-7d9d-4c8a-bed1-98bdd0937653.png)

![](https://pad.gwdg.de/uploads/0ac1dc3a-8655-4e94-8d8d-74201ec2de57.png)
![](https://pad.gwdg.de/uploads/1297bc54-4929-4abb-a379-cd96ad42ac9b.png)

###  Crossfront view
![](https://pad.gwdg.de/uploads/e30427e4-51e7-4232-b459-45d1dc8715ec.png)

![](https://pad.gwdg.de/uploads/10b81f24-7e60-4553-b36b-db7cc96070c3.png)

![](https://pad.gwdg.de/uploads/1935693b-576f-4ffe-8ab4-08ad5f2b44bd.png)
![](https://pad.gwdg.de/uploads/9e2ea013-1376-43d2-a6e8-9280ac3f0fe0.png)

