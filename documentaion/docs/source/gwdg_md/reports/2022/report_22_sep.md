# Report September 22

## Evaluation
![](https://pad.gwdg.de/uploads/4d0649fb-8228-4c4e-a6bb-7509ab164541.png)
![](https://pad.gwdg.de/uploads/7f637fa7-ecf7-4070-8156-feefa001a2e9.png)

### prime vorticity
![](https://pad.gwdg.de/uploads/4956ea44-0c6b-46e8-a95c-6a76ce0f1827.png)
### prime sst
![](https://pad.gwdg.de/uploads/9722fe71-3fdd-41d5-9407-eb546b989c9f.png)

* crossfront images show crossfront averaged mld

![](https://pad.gwdg.de/uploads/3a8d91f6-ebc3-4798-887d-d13da930c7c3.png)



###  R1
![](https://pad.gwdg.de/uploads/127fe5f6-37df-4b40-abfb-4dfe8824f7ab.png)

![](https://pad.gwdg.de/uploads/f0f31574-cce2-45f0-afe4-1786bffde480.png)

![](https://pad.gwdg.de/uploads/2331a717-4cee-4b07-9a6e-be81315ec86b.png)
###  R2
![](https://pad.gwdg.de/uploads/3ce39fd1-22ae-4a45-833f-9f9799a14dd6.png)

![](https://pad.gwdg.de/uploads/8a880c82-1fc5-4040-b0db-1f9c9fa1cf5b.png)

![](https://pad.gwdg.de/uploads/96f3dea2-9716-4e50-aa5b-99af113a2d22.png)
###  R3
![](https://pad.gwdg.de/uploads/78d51278-5896-4332-bd11-bf172d2db026.png)

![](https://pad.gwdg.de/uploads/7379f146-00f7-4bca-a2d3-512efba55153.png)

![](https://pad.gwdg.de/uploads/cfb28186-362c-49be-9f5b-085c8080f74e.png)
###  R4
![](https://pad.gwdg.de/uploads/1e2896fc-3d25-4095-a4f2-bf22146eb6ef.png)

![](https://pad.gwdg.de/uploads/91d005b3-4e31-4f10-956b-09a1059c7f49.png)

![](https://pad.gwdg.de/uploads/b4e74c2c-3146-4f3f-8b59-2a42a6d06b23.png)
###  R5
![](https://pad.gwdg.de/uploads/387838a5-3985-4626-a50e-99c18a5bbe64.png)

![](https://pad.gwdg.de/uploads/ac0a6d12-f15e-451e-bd57-8cfbde4757b9.png)

and with larger crossfront slice (profiles get messy)
![](https://pad.gwdg.de/uploads/c50d34f5-a7f7-45dd-aa27-a6313d03f651.png)
![](https://pad.gwdg.de/uploads/d2619b8a-36f5-4e6e-a440-33d08007ed9d.png)


## updated variables
* only meridional buoyancy component used
* nneeds more data cleaning "dividing small numbers..."

## Profiles
![](https://pad.gwdg.de/uploads/c1502e72-4375-4740-b112-9e7bde08b4d7.png)
![](https://pad.gwdg.de/uploads/1c90ecdb-56f6-4f79-8ce3-374d33cf4f07.png)
![](https://pad.gwdg.de/uploads/5fd7855f-661e-4fc6-9dc4-1826db370101.png)
![](https://pad.gwdg.de/uploads/754abcd6-8703-49cf-b4b9-811743a477a7.png)
![](https://pad.gwdg.de/uploads/2cbf8622-95e0-4604-b891-9d7c3774c5f7.png)

![](https://pad.gwdg.de/uploads/33ce1d15-5c6b-48e9-bd72-147f4e90ba88.png)
![](https://pad.gwdg.de/uploads/13f48fe4-93d8-4127-8ffc-ba02ab95da37.png)
![](https://pad.gwdg.de/uploads/bbbed1d5-7d35-443a-ae90-7eb8c7916198.png)
![](https://pad.gwdg.de/uploads/cec40548-4c8b-4573-8dcc-95493419bce3.png)
![](https://pad.gwdg.de/uploads/93dbf4ac-85f9-4019-b31a-4ef5d63a6964.png)
![](https://pad.gwdg.de/uploads/6c6b0b3d-2008-4df1-b61e-0b4ec05fd1bf.png)
![](https://pad.gwdg.de/uploads/4099a022-6621-49dd-9f2f-5a27a5231961.png)


## Sections
![](https://pad.gwdg.de/uploads/f5cceced-ec6b-48b6-9230-efa409994042.png)
![](https://pad.gwdg.de/uploads/b7ab2a7d-a817-4288-8028-da82e02530e9.png)
![](https://pad.gwdg.de/uploads/30216cf2-2d94-43b8-8e8b-6757ae407e5e.png)
![](https://pad.gwdg.de/uploads/e9b96191-b8ab-4cce-8e96-d8efe65fee18.png)
![](https://pad.gwdg.de/uploads/dfb4c6d7-21f2-4ef3-a1d7-babf81054cd3.png)

![](https://pad.gwdg.de/uploads/adde21aa-608c-4e8c-bc9b-fd747dd5bb4b.png)
![](https://pad.gwdg.de/uploads/e00619d8-84ed-49f3-bcbb-5eaa27515903.png)
![](https://pad.gwdg.de/uploads/293053fe-b177-4f21-8f21-7130f2eac954.png)
![](https://pad.gwdg.de/uploads/a6bb8cf7-c6e0-4f88-b17a-36c0ea317e6b.png)
![](https://pad.gwdg.de/uploads/fced0421-779b-48f5-b26d-5dd845b0bb67.png)
![](https://pad.gwdg.de/uploads/e9ef5f96-4253-4ad8-ac0a-a19ce7dc517d.png)
![](https://pad.gwdg.de/uploads/569fb4bf-d2b5-4ebe-b520-8495bc5efe64.png)

What is the tuning coefficient of Stone? 
![](https://pad.gwdg.de/uploads/ab35c3ce-13a6-4f94-a875-36e5a482ae29.png)
