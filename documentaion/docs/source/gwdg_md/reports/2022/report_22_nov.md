
# Report November

### Scatter plot - evaluation of slope
evaluation of wb ~Ri over the MLD leads to following observations:
method: masking MLD, averaging over MLD, removing zeros and infs, taking log10 of absolute values, scatterplot, linear regression to calculate slope
* evaluation of small and large regions, but also selected ocean fronts
* without further modification the slope is between 1 and 0.7
* no lagre sensitivity if abs(M2) or dbdy or dbdx are evaluated
* filtering M2 eg. m2.where(abs(m2) > 3e-8) leads to a large sensitivity of the slope
* the slope goes from 1 to 0 to -0.5 (at above example) to -1 and to -1.5
* Guess:  when submesoscale fronts are strong and within a certain threshold, we get slopes which are much closer to expectations (between 0 and -0.5)
* an "optimal" threshold seems to slightly differe with respect to region or single fronts
* filtering N2 as well effects slope, however no pattern is found
* slope is sensitive to selection of Richardsonnumber regime eg. 0-100 or large 100: so far no pattern...

> * How to define a threshold? What could be an apropriate criteria? Does filtering here make sense?
> * Maybe I need to filter out certain processes (eg. mesoscale signal) and "different" dynamics (eg. eddies)
> as well the evaluation of diapycnal diffusivity and streamfunction now is possible as scatter plot

## Front F1 Sensitivity filtering strong fronts by M2
![](https://pad.gwdg.de/uploads/cb6f1a8a-35b5-4fbd-a31f-923ebee37284.png)
![](https://pad.gwdg.de/uploads/e5715df4-62ab-4ac0-a0cb-5153ac639b3b.png)
![](https://pad.gwdg.de/uploads/e677a00c-cf42-46d0-a91e-070412103f3c.png)
![](https://pad.gwdg.de/uploads/3c661eff-843a-48ba-8105-cbe0d67a0960.png)
![](https://pad.gwdg.de/uploads/8020178f-dd62-437b-93c6-f9d87eea2231.png)
![](https://pad.gwdg.de/uploads/c282fb2f-c61b-4f39-9ee8-1b3b777d7a0f.png)

> for streamfunction and diapycnal diffusivity last two lines have to few points!

### Streamfunction
comparison of Fox Kemper streamfunction $w'b'/M2$, full held schneider streamfunction $(v'b'*N^2 - w'b'*M^2)/M^4$ and traditional streamfunction $-v'b'/N^2$
* the weakness of fox kemper formulation: absence of lateral gradient leads to large values without physical meaning (eg. no ocean front, minimum w'b' large values) >> streamfunction indicates strong circulation even if there is no circulation
* the traditional streamfunction instead (still wrong sign > propably coord system) seems to better capture strong circulation at ocean fronts and has not the above mentioned weakness
* the full held schneider streamfunction looks almost exactly like the traditional streamfunction with an opposite sign 
* evaluating the order of the different terms shows that v'b'*N^2 is 6 orders larger then w'b'M2 >> which then gives us the traditional streamfunction with opposite sign

> * double check formulation of Held Schneider streamfunc >> I cannot find the formulation in nils paper in the held schneider paper
> * held schneider formulation made for atmosphere ratio of w'b'/v'b' might change in the ocean?

![](https://pad.gwdg.de/uploads/fa02bb93-e545-4f27-9a6e-c547e2e1585f.png)

![](https://pad.gwdg.de/uploads/919f919d-b892-48ab-8432-5fbce59b10bf.png)


### Vertical profiles
front averaged vertical profiles of rho, wb, vb, dbdy dbdx, psi:
* vb does not go to zero at the boundary, surface flux dominates streamfunction...
* easy detection of MLD
* held schneider maximum almost double of fox kemper
* shape of streamfunction cannot be so easyly compared due to surface flux
* vb and wb indicate same mld, and are usually strong at the same location >> overturning circulation
* results can be used for "diagnosed" variables to compare with predictions  of parameterizations
![](https://pad.gwdg.de/uploads/6e4a8cb7-5ce7-4ef5-932d-99e18da10b5f.png)

> vertical structure function can be included
> calculation of u'v'
> maybe full vector notation?
> evaluation of fronts which are not zonal or meridional (How much more effort on single ocean fronts?)

### Filtering M2 corrected signs
![](https://pad.gwdg.de/uploads/b8d7c463-a913-4d0e-9a5c-44f14436fc66.png)

![](https://pad.gwdg.de/uploads/fd003c33-add9-4e8c-94d8-efa261c6c364.png)

* filtering makes ovettunring in front visible with held schneider formulation
* green blop on the lower part of front indicates circulation in opposite direction, which works as well to restratify the ocean
* second purple blop outside front indicates weak stratification but no overturning

### sign sensitive unfiltered profiles
![](https://pad.gwdg.de/uploads/41ff5499-5f4a-49de-add7-19d474c9f90a.png)
* N^2 is noisy in the middle of the ML. It is visible in streamfunction! + shape of traditional streamfunction dependes strongly on n2 (zoom)
* what does the presence of both u'b' and v'b'. Overturning in two directions? Or the  need of clearly definded vectors?
* traditional streamfunction much cleaner, however can have "wrong" signal when n2 is weak >> overtunrning without a front? still better then held schneider streamfunction, n2 is more stable then m2
*  v'b' is often large at surface why? the eexpected shape cannot be ssen. can one filter surface fluctuaions out? here held schneider is more applicable, since w'b' go to zero at surface
*  vector calculus needed!
*  


### vertical structure function
Question: The parameterization is supposed to fit the held scheinder streamfunction?

Example:
![](https://pad.gwdg.de/uploads/b96e8be8-d829-4b2c-89c0-ff7ce028c3be.png)
stone fits full stramfunction, whereas fox kemper fits held schneider
![](https://pad.gwdg.de/uploads/9ee02f9a-c9e0-4081-ac62-81a3d5b2107c.png)

* a glance on 12 different fronts shows a very broad range of results:
eg. clean overturning - wrong magnitude in parameterization
![](https://pad.gwdg.de/uploads/125a033b-7452-480c-b580-68af2f8ffb88.png)
eg. wrong MLD, wrong magnitude
![](https://pad.gwdg.de/uploads/e02250c3-3c40-439a-a24f-cdd839262cb8.png)
eg. strong surface current, change of sign og v'b' etc.
![](https://pad.gwdg.de/uploads/970a9d7d-b3a0-4c2f-b37b-6995e9ec4595.png)

* How decide what is a good front?
* One could tune Cs to the desired magnitude. Its unlikely to find a tuning constant which fits several types of submesoscale ocean fronts
* why is tuning constant of fox kemper limited to small range?
* 
[](https://pad.gwdg.de/uploads/d13e0766-5c67-46e7-8915-3235cfeb5f14.png)

### Richardsonnumber of fronts
![](https://pad.gwdg.de/uploads/bc2c3a44-2b50-4762-95a7-053ee982ad5a.png)

### Filter M2
no filter
![](https://pad.gwdg.de/uploads/e01b04bf-5122-4984-8fe1-dc26766fcdde.png)
1e-8
![](https://pad.gwdg.de/uploads/67447fe7-b987-4032-be25-fe48ae0a46a3.png)
3e-8
![](https://pad.gwdg.de/uploads/6a6715e8-90f6-4bf2-97be-e09d78e3342b.png)
5e-8
![](https://pad.gwdg.de/uploads/233c3f5a-197b-437b-8469-44f1ed523b4e.png)

### all fronts with modified mld 
+ even without tuning streamfunction from parameterization meets the correct order
+ strong influences on surface
+ tendency of parameterization to be to strong
+ full streamfunc is  approx factor 3-6 larger
+ 
![](https://pad.gwdg.de/uploads/6db59a0d-62df-4d34-8d82-0142b3a1e7c0.png)
![](https://pad.gwdg.de/uploads/2a9ef0b4-d8e6-4691-8689-26906f50743a.png)
![](https://pad.gwdg.de/uploads/2b546f1b-b93c-452d-8f03-430a807bdd27.png)
![](https://pad.gwdg.de/uploads/bd9279b7-e2a2-484d-b4d1-5b2b9be72814.png)
![](https://pad.gwdg.de/uploads/b32deceb-a94e-4a2c-87b1-149928caa1cc.png)
![](https://pad.gwdg.de/uploads/6685a452-3b99-4e4b-b91d-2f22d76dd320.png)
![](https://pad.gwdg.de/uploads/5a8afb44-52fa-4bad-bc38-18be4c4cd74f.png)
![](https://pad.gwdg.de/uploads/9a4d8c30-5635-4ea0-bde6-d54fe9871a0f.png)
![](https://pad.gwdg.de/uploads/08eaf279-249c-48f7-ac09-486c5bb92f03.png)
![](https://pad.gwdg.de/uploads/11e29247-6ac3-4347-8c82-28aa69d54acd.png)


![](https://pad.gwdg.de/uploads/dc0e11e3-6f30-4f8d-8ca7-200e6f1e8145.png)
![](https://pad.gwdg.de/uploads/0bbff9ff-4c1b-4aa8-8290-9fe9233aea40.png)


### Locations
![](https://pad.gwdg.de/uploads/3396e083-e9ea-4a11-a7d3-2d9388bd1c29.png)



