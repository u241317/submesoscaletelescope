# Report April


## Evaluation sst meridional and zonal section
* n = 60, nr = 30 (r2b8 coarser resolution)
#### regions
* smt falgs at section B, 18 - 22
![](https://pad.gwdg.de/uploads/9098ed6e-bf79-4a48-b61c-1f3fb521125e.png)
![](https://pad.gwdg.de/uploads/d2be781f-8f72-4cee-b40d-c1d0f780032b.png)
![](https://pad.gwdg.de/uploads/669604d7-0013-4a6d-a5f9-1c46c227581d.png)

#### corresponding regional averages
![](https://pad.gwdg.de/uploads/5c76a32b-c62d-4cae-af7e-661470774d52.png)

* satellite: slope seems to get much shallower towards smaller spatial frequencys
* singals seem to be less noisy, smaller spread CI smaller - makes sense since double number of samples doubled
#### total average
![](https://pad.gwdg.de/uploads/c3b95465-eeb3-4b76-8a43-00197f9e710b.png)
* results are close to observation like in exp1 however here PSD shift is stronger
#### Longitude dependence
* no signifikant longitude dependence 
![](https://pad.gwdg.de/uploads/719126b4-0652-4bae-a23e-c7db88cba3b0.png)
