# Report May

bias figures
---
data for comparison interpolated to a 0.3 degree regular grid
## SST with modis aqua
![](https://pad.gwdg.de/uploads/482d400b-b93f-4aa7-9e26-f36c0ab578da.png)
## Surface Salinity with EN4
![](https://pad.gwdg.de/uploads/60b0adf1-bc31-45bd-8826-5da18e99292a.png)
* 1 degree reanalysis data
## ssh with Aviso
+ global mean substracted
![](https://pad.gwdg.de/uploads/bc4ed6f4-d306-4819-8943-aa9ffdabfd3c.png)
## mean surface flow with oras5 reanalysis data
*  resolution (0.25°) 
![](https://pad.gwdg.de/uploads/94713548-5eef-4b4f-8b15-e5c7132084e1.png)

