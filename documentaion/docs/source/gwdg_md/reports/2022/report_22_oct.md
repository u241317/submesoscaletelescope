# Report October


![](https://pad.gwdg.de/uploads/9b78efe0-a1e3-42b1-93c4-b6c0523072b3.png)


## Ekman pumping
* does ekmann pumping makes sense? 
* how to see convergence and divergence fields from this mesoscale windfields??
* scaling of Ekman pumping to strong??
![](https://pad.gwdg.de/uploads/228cb987-9eb5-479e-b696-b84187b21625.png)

![](https://pad.gwdg.de/uploads/8006198b-d749-4daf-8a54-9990f9f5cd99.png)


## Example of wide view on songle ocean front
* sign for gradient can only be fully used if full streamfunction is used
* Problem1: below ML base sign of w'b' changes as well >> no sensfull visual of streamfunc possible
* Problem2: when there is no front streamfunction is large, when w'b' is large where front is strong streamfunction is weak >> interpreatation?
![](https://pad.gwdg.de/uploads/34fe71ed-320f-436f-9a29-7e4cc62b6227.png)



## a glance at a single front:
using weekly time averages: (visualisation with instantaneous)
![](https://pad.gwdg.de/uploads/2c06df54-d965-4e6d-b3c0-b75100d3bf3d.png)
![](https://pad.gwdg.de/uploads/a879375b-6863-47c0-98e1-3df011b2f1bb.png)
![](https://pad.gwdg.de/uploads/2e5dc2ea-672a-4dff-803e-25b3f332cb01.png)
no time average used only alongfront average for wb_prime
![](https://pad.gwdg.de/uploads/d56ba35c-e954-4b13-84fa-c05469de467b.png)



![](https://pad.gwdg.de/uploads/0abf3888-ca87-40b3-bac0-b6afc93b7770.png)

using maximum/minumum values
![](https://pad.gwdg.de/uploads/e469d896-bc27-4e48-bcf2-7c49cd01d5b4.png)
using mean values
![](https://pad.gwdg.de/uploads/2efe4c89-6574-4db8-a784-c5af6b348a26.png)

![](https://pad.gwdg.de/uploads/738f4d2d-da8b-4318-b313-851920086f3b.png)
without alpha
![](https://pad.gwdg.de/uploads/7235a7cd-7136-4dfe-8dec-5c7604716980.png)
