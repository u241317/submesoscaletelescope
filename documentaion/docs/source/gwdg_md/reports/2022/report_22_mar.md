
# Report March/April 22

## Evaluation ssh zonal and meridional section
In the following section the spatial spectral estimates of the sea surface hight ssh of the ICON-SMT model is  evaluated using AVISO satellite ssh data. There is no larger data gaps in aviso data, therefore robust spectral estimates can be computed using all temporal realiztaions.  
#### Method:
+ First the spectral estimate of each temporal realization is computed and then all spectral estimates are averaged (90 temporal realizatins for satellite, approx 900 for smt)
+ then spatial averages over 10 sections are taken 

### zonal sections
+ there is no flags applyied for strange spectral estimates, see region F.
#### Regions
+ What happens above the gulf stream? Is there less sea level anomaly, which explains why the overal PSD is weaker? see F 
+  Why does region F shows this funny behaviour in the PSD?
+  here std corresponds to the spatial realisations not to the time averages 

![](https://pad.gwdg.de/uploads/b6ed8c81-009a-4be2-905d-0660bdb0500e.png)

#### example of a temporal average with std
>>>>>here shown to give an impression of the spread of the individual realisations
>>>>>>>![](https://pad.gwdg.de/uploads/15d26b28-536f-4d3b-9086-e63d102fdcd4.png)


#### corresponding regional averages
![](https://pad.gwdg.de/uploads/ca55c358-a481-461a-9a46-039f9f956efc.png)

#### total average
the following image shows an average over all regions and the average of each region (wheat color corresponds to observation, blue and grey to smt model run)

>>>>>![](https://pad.gwdg.de/uploads/7139445f-a2a4-4a1b-a800-467c041db720.png)

since F seems to be hampered >>here an average without region F and without std
![](https://pad.gwdg.de/uploads/2dd8efba-4177-4279-b7b0-eb1a4ffe6b32.png)

> varying latitude seems to have an impact on the overall PSD, see observation and model data (shifted) in above figure. Shift cannot be found in vertical sections with varying longitude
> however in averaged data this doesent matter... similar effect in observation data
> slope observation is not as steep as model

---
### meridional sections
+ no falgs necessary
#### regions
![](https://pad.gwdg.de/uploads/83f9d33c-684f-4b24-a92b-de5ae85cb5fd.png)
#### corresponding regional averages
![](https://pad.gwdg.de/uploads/41e9199d-eb3f-4e72-aa18-cc4d2d5451ee.png)
#### total average
![](https://pad.gwdg.de/uploads/697f584d-819c-4f6c-8ca5-3335db764390.png)

> slope of observation data is less steep then model
> good agreement in power spectra density

Questions:
+ How to deal with the error/std/sem? >> see sst evaluation (confidence intervals are introduced)
+ since there is a latitude dependence of the PSD, could one say one region, like A is sufficient? Or is this shift averaged out and its fine?
+ How to deal with confidence intervals if temporal and spatial averaging is used?
---

## Evaluation sst meridional and zonal section
In the following section the spatial spectral estimates of the sst are evaluated. Here data from the ICON SMT run will be compared with a coarser ICON R2B8 run and sst satellite data from Modis Aqua. Due to strong cloud cover over the north atlantic it is much more challenging to get a robust spectral estimate. Therefore averaging spatial spectras of different temporal realizations is not possible. To obtain more robust spectral estimates averages are taken of different spatial realizations.  The resolution of the two models and satellite data differ R2B8 (approx 10km), satellite (approx 4km in focus region) and SMT (approx 550 - 700 m in focus region). In order to have the same amount of realizations the satellite resolution is used to spatially average satellite and smt data. Consequently less realization can be used to investigate the spectra of the R2B8 Model (approx half), which in turn lead to weaker spectral estimates. Since the satellite data is stored on a rectangular grid only zonal and meridional sections are considered (a constant spacing is crucial for spectral analysis. !!! meridional sections do not have the exact same spacing - maybe one could say over a few degrees the impact is neglegible !!!). 

+ Only few days with small cloud cover could be found during the period of interest (one could investigate same season in different years...). 
* since there is no timeseries only spatialy averages are taken of one timesnapshot
* here 30 neighbouring sections in each region (spacing of satellite resolution, only 15 neighbours for r2b8 data (much coarser...))
* at least for the models more robust spectral estimates could be calculated (What is more important: same method, or the best spectral estimate of each dataset?)
* visualization needs to be adpted since estimates are less robust then ssh data (here more temporal realizations enable averaging of spectral estimates) and representation of errors/std/ci in loglog plots become ugly >> see example below
* r2b8 run uses same mixing scheme (TKE) like smt but started from a different spinup

* The Observation data determines the region, which can be analysed.
* Since the data gaps are different in every section (but less then approx 20 %) and gaps are filled with linear interpoaltion: Should these gaps be quantified?
* Is one timesnapshot of satellite data sufficient to make conclusions?

### zonal sections
#### regions
+ strange (several archs in spectra) spectral estimates are flagged: FLAGS at region C, n =0 and excluded from averaging
![](https://pad.gwdg.de/uploads/4395f8b3-a2c8-4f55-9567-c518ee0cb196.png)
![](https://pad.gwdg.de/uploads/e9ecae4d-931a-480b-843e-94bf5492e736.png)

#### corresponding regional averages
![](https://pad.gwdg.de/uploads/33d8d079-e63b-4417-a743-4709daca0c60.png)
* PSD of R2B8 run is one order weaker
* PSD of SMT run has the same order (here in all regions weaker) like PSD of statellite, although slope is steeper
* R2B8: Why does the slope becomes shallower towards higher resolutions? - Where is grid diffusion? >> due to Interpolation?


#### total average
![](https://pad.gwdg.de/uploads/094c1a91-6958-41b6-b9cc-28ff9f03a941.png)
* PSD and slope of SMT model are closer to satellite data then coarser R2B8 model - impact of increased hor. resolution/diff. spinup/or other?

* there might be a latitude dependence of the overall power density, colorbar viridis >> shift in PSD, larger spread of data points compared to meridional eval. it increases towards larger latitudes
* not as clearly visible as in ssh (latitude steps are much smaller here)

![](https://pad.gwdg.de/uploads/b0d7f761-9e07-4152-b8ed-27da0ad1f003.png)

### meridional sections
* same method used as for zonal sections
* spacing is not constant!!! - spacing in degree however is more difficult to interpret...
#### regions
+ strange (several archs in spectra) spectral estimates are flagged: FLAGS region B, n =0
![](https://pad.gwdg.de/uploads/ae17868b-def3-4670-b78f-f038f3119060.png)

#### corresponding regional averages
![](https://pad.gwdg.de/uploads/75fd8dc7-8c5d-4e8b-ae10-e3c2b6aa5e08.png)
* PSD of SMT run in average weaker then satellite, however in region e,f stronger, same is true for R2B8 model
#### total average
> visualization check of longitude dependence
> a to f corresponds to from dark to bright (west to east)
> no clear depndence visible
![](https://pad.gwdg.de/uploads/c74cd5f1-db30-4f5f-a86f-e55b3a25d8ab.png)

Longitude dependence
![](https://pad.gwdg.de/uploads/170c3d02-4060-42c9-b686-fe8b215d5d32.png)

## Mixed Layer Depth Sections
Here the Mixed lay depth of the ICON-SMT, ICON-R2B8 models and an argo float climatology of Holte et al. 2007 are evaluated. Three zonal sections through the north atlantic at 32, 35 and 38 degrees N are investigated. To compare the mixed layer depth only monthly means are evaluated.


R2B8 
---
* different run then used for sst evaluation (could be easily adapted)
* mlotst is the cmip6 Definition (H42) with rho_crit = 0.03 kg m-3 (as well Montegut 2004, using density treshold with respect to surface value)
* mld ist die alte MPIOM Definition mit rho_crit =  0.125 kg m-3
* pp vertical mixing scheme

SMT
---
#### two different temperature threshold methods Levitus/Montegut
for calculation of MLD first the temperature threshold method is used to reduce computational costs (averaging salinity and calc rho): 
+ Montegut Temperature threshold method is used dt = 0.2°C to near surface temperature (10m) >> supposedly equivalent to montegut density threshold method
+ Levitus threshold dt 0.5°C to surface temperature (here at 2.5 m depth)
+ depth of surface/near surface temperature is not having a large influence on calc MLD!!!
+ is the temperature from model run potential temperature?


differences to R2B8 which make comparison difficult
* TKE vertical mixing scheme
* different calculation for windstress
* different grid diffusion

ARGO FLOAT
---
+ Climatology data

there are two methods used to calculate the MLD:

* Threshold method by Montegut 2004 "mld_dt_mean". The criterion selected is a threshold value of temperature or density from a near-surface value at 10 m depth (DT =0.2°C or Dsq = 0.03 kg / m^3).

* Algorithm method by Holte 2007 "mld_da_mean" uses density gradients and tresholds but differentiates etc...

### a glance on MLD in zonal sections
* compute time smt 15 min for 3 monthly means to a depth of 500m, using dask cluster with 12 nodes
* 

### Comparison of Montegut and Levitus temperature threshold
![](https://pad.gwdg.de/uploads/5d6c7b44-105f-4be9-8334-677a3fa2d9e3.png)
* Levitus method estimates results in deeper MLD (approx 50m in jan and feb), in march up to 100m
* 
### Comparison of Montegut Density Threshold compared to Montegut temperature threshold method
* two methods result in similar MLD, however there seem to be region with large salinity impacts (see march up to 70 m difference)
![](https://pad.gwdg.de/uploads/b40cd40b-bf86-4b3a-bc42-c2a5596016bb.png)

### comparison of 3 sections 32°N, 35°N, 38°N
![](https://pad.gwdg.de/uploads/e05c6709-c337-41c2-abec-1348f61f44c4.png)


* strong averageing (convolution with 100 neighbours) is applied on the model mlds
* comparing SMT R2B8 MLD in different sections show large biases compared to argo float data. 
* Mostly R2B8 and SMT result in a similar MLD however in march at 32° and 35° the difference are above 100m. 
* The deviations to Argo data are strong between 60°-30°W and much weaker at 90-60°W and 30°-0°W. 
* model data is to noisy and MLD to random in time to see if one model is better
* argo float data maybe to coarse and not sensitive enough...?
#### comparison with mia
![](https://pad.gwdg.de/uploads/ed8565aa-d01b-48c6-820c-c8cbdd643502.png)

#### comparison with amk
![](https://pad.gwdg.de/uploads/4afac4cd-9ac7-465a-b028-efb14c3e787f.png)

* in january and february amk run is closer to argo float data
* in march mia run is slightly better

#### r2b8 comparisons

##### interanual variability of mld in r2b8 model mia
![](https://pad.gwdg.de/uploads/6bf7c837-e600-41d3-a154-e4760cc270df.png)
* output of model starts at 2010, 2010 seems to have the largest deviations from other years >> indicator of initializing/spinup effects
##### mia and amk in 2010
![](https://pad.gwdg.de/uploads/ffa73c7b-c150-4d14-9c9a-384d8b567c39.png)



### Questions MLD
* is it possible to compare Montegut temperature treshold with montegut density threshold? >> see above
* how can the mld of r2b8 and smt be so different in the first month since they have the same spinup? (it is propably a different spinup - compare to amk version) >> not compareable, models differ in to many points, a new model run could be investigated (I would not expect great results! - to random MLDs & threshold method is not elegant!)
* r2b8 and smt data are following different trends (maybe because there is different windstress applied)
* Does it make sense to compare r2b8 and smt run although they used different vertical mixing schemes?
* Helmut is suggesting Mia's r2b8 run, different spinup but same mixing scheme

