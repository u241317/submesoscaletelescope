# Report November 2


New Streamfunction:
* parameterizations used to calculate v'b' and w'b' 
* comparison of streamfunction
* Held Schneider: $$ \psi = - \overline{w'b'} * d_y b / (|d_x b|+|d_y b|)^2 $$
* Full: $$ \psi = (v'b'  N^2 - w'b'  d_y b) / |\nabla b|^2 $$


###  Diagonised vs. Parameterized: Full
![](https://pad.gwdg.de/uploads/f237b201-8c1c-4a82-b080-f4049bbd0a65.png)


###  Diagonised vs. Parameterized: Held
![](https://pad.gwdg.de/uploads/5c95c932-546a-4c95-9ae8-7b966949549f.png)

###  Profiles
![](https://pad.gwdg.de/uploads/ff10a407-2695-4d2d-8813-b8cc826904d2.png)

###  Filtered
* strong dependence of Held schneider formulation, cut out storng overtunring at weak lateral gradients..
no filter
![](https://pad.gwdg.de/uploads/fecab901-d56a-481a-ae25-3a07e07775ff.png)
1e-8
![](https://pad.gwdg.de/uploads/ef13bf2d-edcb-4097-bcc9-f595e40e5c3e.png)
3e-8
![](https://pad.gwdg.de/uploads/755b0bf8-c84b-432b-a8b7-ee326f94f7f1.png)
5e-8
![](https://pad.gwdg.de/uploads/aa2dc00a-963a-4a05-a9c1-23f3b6313aff.png)

###  Tuning Coefficents of Fox Kemper and Stone param. for 10 Fronts
* optimized for psi and vb (vb 2-3 orders larger then wb)
* full fromulation used, difference in optimum is small (eg. stone cs=0.25 & 0.33, for vb and wb respectively)
* LSQ: $$ min( \sum_{FRONTS} (\psi_{diag} -  \psi_{param.})^2 / N) $$
![](https://pad.gwdg.de/uploads/ef2d01ee-3c69-4bf3-9b1e-b8910e93d36b.png)
![](https://pad.gwdg.de/uploads/585add6f-716f-46bf-987f-a4cd297fd11a.png)

Stone:
psi, wb, vb
0.22222222222222224 0.32323232323232326 0.24242424242424243
Remaining Error Stone 3.3852192799969893

Fox:
psi, wb, vb
0.038181818181818185 0.05000000000000001 0.03727272727272728
Remaining Error Fox 3.587545177986796

##  Evaluation of Eddies
![](https://pad.gwdg.de/uploads/7bce356d-1587-4e01-9c64-fda782cf1a3f.png)
![](https://pad.gwdg.de/uploads/a6e5e0d8-bb38-4d5f-a580-0a4e5fe6b8cf.png)


![](https://pad.gwdg.de/uploads/5170bc26-1758-4cc9-a2f5-6b3e5fd93e3b.png)
![](https://pad.gwdg.de/uploads/0177dd30-5cb0-4fe6-aab7-9b259715da18.png)
![](https://pad.gwdg.de/uploads/238b37df-26ee-42dc-8acf-b5e6ebf96b72.png)
![](https://pad.gwdg.de/uploads/9592e440-11e7-42c5-aae9-31fe6b04480a.png)
![](https://pad.gwdg.de/uploads/454847db-e489-49c9-b32d-80728a9f7ab7.png)
![](https://pad.gwdg.de/uploads/91704df2-e5bd-4e1f-b873-3254bc8fcaa7.png)
![](https://pad.gwdg.de/uploads/9a92c90f-5e55-4d50-bb01-280dc438eaeb.png)
![](https://pad.gwdg.de/uploads/77e39cf0-9ac0-4f7f-bbe2-a32f75f02ea1.png)

##  Richardson wb correlation
comparison of Fox-Kemper, Stone param vs diag.
![](https://pad.gwdg.de/uploads/516c6053-749a-40ed-8713-c014158c1066.png)

filter Ri number larger 2e2, (2 points), Front 4 und 8
![](https://pad.gwdg.de/uploads/2379ee30-bca0-4e79-ab19-c167c74b1efb.png)

![](https://pad.gwdg.de/uploads/bc2c3a44-2b50-4762-95a7-053ee982ad5a.png)
