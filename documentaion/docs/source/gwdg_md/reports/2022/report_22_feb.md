# Report February 2022

## Introduction
topics worked on in February


![](https://pad.gwdg.de/uploads/2ab09a29-5b6b-48ee-8db2-7b18b6528ded.gif)


> GHRSST
> https://www.ghrsst.org/ghrsst-data-services/products/
![](https://pad.gwdg.de/uploads/61ee793c-970d-46b4-8a33-6b4395195e04.png)
> * sst data with clouds is sea surface skin temprature
> (Infra-red instruments measure SST skin at high spatial resolution but the surface is obscured by clouds.)


## Evaluation of SST
 The accuracy of the SST is given as 0.4 K;
 
#### spatial resolution of "interpolated steps" at 40°N
distance of one degree in meter at equator: 111194m
distance of one degree in meter at section: 84616m
in degrees: smt model 0.0065 r2b8 model 0.163 satellite 0.0417
in meter: smt model 550m r2b8 model 13819m satellite 3526m

### Quantitative evaluation of SST
model issues:
+ second meandering jet - does it in reality exists?
+ GS detaches to far north (seems to be a typical modelling problem)
+ therefore sensible heat fluxes are to strong (they make use of climatology data, if the GS detaches at the wrong place the temperature gradients between atmosphere and ocean are wrong) > maybe a reason for the shift in fft of sst
+ evaluate NorthWest Corner of GS
+ sst evaluation in ocean model with forcing "inapropriate" due to restoring (you directly compare model results with forcing)
+ evaluation of model sst at 2.5m depth vs. satellite at sstskin at 10 micrometer


>![](https://pad.gwdg.de/uploads/637b8f4a-a14b-4ffb-9a37-e4b17fce539e.png)
Sea surface temperature of Aviso satellite, ICON SMT-NATL and ICON R2B8. The sst data of satellite and smt model are at the same date whereas the r2b8 data is only available as a monthly average of march 2010. However maybe more data can be found in archive. Satellite data is almost only available at night in the aviso dataset. Besides that daily data shows large areas with cloud cover which makes evaluations difficult.

Questions:
* Does it make sense to quantify the bias of instantaneous images? Maybe better take the bias of monthly means? >> yes

### Evaluation of spatial variability
+ modis aqua satellite data only available during night
+ large blank areas due to cloud cover over focus region
+ satellite data already on rectangular grid >> section
+ smt and r2b8 data selected using nearest neighbour method (number of points selected with respect to resolution of data)
+ NaNs are filled using linear interpolation


>Selection of largest connected dataset in focus region.
![](https://pad.gwdg.de/uploads/dcfaa221-34c3-4558-8976-3c3a7df1115f.png)
> The lower figure shows the corresponding sst for satellite, smt and r2b8 models.
![](https://pad.gwdg.de/uploads/46c6353d-9715-450c-8d05-2fedd1c3c292.png)


+ cut and shifted periodogram shows fft
+  thick lines show convolved/smoothed spectral estimates with a kernel width of 20
+ slope of three series are alike however satellite data has larger magnitudes (shifted)
  +  green line is not robust - snapshot
  +  lower magnitude of smt (shift) could be explained with a higher variability at the surface then at 2.5m depth
+ the friction at grid scales should be visible (deviation towards smaller magnitudes) - not visible!

Questions:
* Why are we not seeing higher magnitudes towards larger wavenumbers?
* How to get a robust green line? (eg. get more spectral estimats by changing latitude?)
* How to introduce confidence bounds to satellite data?
* Is it helpful to detrend the data instead of taking of the mean?


![](https://pad.gwdg.de/uploads/563c99f7-ba9c-4561-8613-b90739717815.png)


### Evaluation of temporal variability
+ the temporal resolution of aviso sst data is twice per day
+ most of the values in focus region and time are NaNs, therefore a comparison of the smt data with temporal satellite data is not possible
>![](https://pad.gwdg.de/uploads/93de961c-2178-4585-be4e-bb3c8a15c9f4.png)
SST Time series of SMT data at lon = 64 lat =39.1



>FFT of sst time series (SMT-NATL data) at three different lon and 32°N lat.
![](https://pad.gwdg.de/uploads/cb364056-cb6c-4a2e-882e-617e7d677bde.png)

>Visualisation of spectral estimates over longitude. (SMT-NATL data)
![](https://pad.gwdg.de/uploads/c40788b7-0d9e-4749-b17d-e54bd759a52b.png)
* stripes could appear because of non interpolated forcing (a shift in forcing every hour)
* are harmonics at integer frequencies realistic?
--- 
## Evaluation of SSH
> [How does the resolution "change" with varying latitude??]
+ ssh data resolution of 0.25° which is approx 23km at 32°N
+  daily output, no data gaps >> (temporal evaluation possible)
+  Aviso ssh data from 2010


#### SLA
+ comment: The sea level anomaly is the sea surface height above mean sea surface; it is referenced to the [1993, 2012] period; see the product user manual for details
+ coordinates: longitude latitude
+ grid_mapping: crs
+ long_name: Sea level anomaly
+ scale_factor: 1.0E-4
+ standard_name: sea_surface_height_above_sea_level
+ units: m

#### ADT
+ comment: The absolute dynamic topography is the sea surface height above geoid; the adt is obtained as follows: adt=sla+mdt where mdt is the mean dynamic topography; see the product user manual for details
+ coordinates: longitude latitude
+ grid_mapping: crs
+ long_name: Absolute dynamic topography
+ scale_factor: 1.0E-4
+ standard_name: sea_surface_height_above_geoid
+ units: m

>![](https://pad.gwdg.de/uploads/b6f23591-4e3f-460c-b585-320febc06f19.png)
Up: absolute dynamic topography. Mid: Sea level anomaly. Down: SMT ssh data. For a more suitable evaluation the mean should be taken of (adt and sla will appear alike...)

### Evalutaion of spatial variability

>Up: Visualisation of ssh section. only in smt focus region!
![](https://pad.gwdg.de/uploads/c54b5f91-95a5-466e-ae5a-8c0f125b8895.png)
>Down: sla, adt and ssh over section
![](https://pad.gwdg.de/uploads/cfbca925-cd72-44ea-a850-f89d6f08122d.png)

![](https://pad.gwdg.de/uploads/a1b010b4-bb4e-4aa6-a0fa-6c812d6f2a14.png)



![](https://pad.gwdg.de/uploads/00ce5e68-1bc4-4a1c-84f3-a0ba6d8a29d7.png)


![](https://pad.gwdg.de/uploads/4234500c-5c3c-496b-b707-d375df557c7f.png)

Upper figure: SLA ssh data from 2010, SMT_NATL data from 2010 (first three winter month), smoothing after fft.\\
Lower figure: ADT Aviso ssh data from 2010, SMT data selected to suite dates of satellite data, smt data "interpolated" to a grid with approx 1000m grid spacing, smoothing after fft with kernelwidth of 20. In grey/wheat: smoothed spectral estimates of other timesteps. 

* adt data seems to agree well with model ssh data

Questions: 
* Would it make sense to quantify the bias? (like taking the deviation of monthly means)
* Why is sla data so different from adt data? - especially at larger wavenumbers
* 

### Evaluation of Temporal variability
* satellite ssh data is daily available
* smt output is 2h

![](https://pad.gwdg.de/uploads/fad09e05-39ce-4cbd-aa91-ec4cf2010054.png)

* the power spectrald density of the satellite data varys strongly with longitude however evaluation shows that there is no clear connection
* explanation for the peak at semidiurnal freq.?


## Mixed Layer depth
### Argo
Argo float Data to climatology by Holte et al 2017:
offers to mld calculations:
1. Density Algorithm by Holte 2010
2. Montegut 2004 Threshold method (near-surface value at 10 m depth (dT =0.2°C or dsigma_theta = 0.03 kg m^3).)
### R2B8
Icon R2B8 offers to mld calculations:
1. "mlotst" ist die cmip6 Definition (H42) mit rho_crit = 0.03 kg m-3
2. "mld" ist die alte MPIOM Definition mit rho_crit =  0.125 kg m-3

> here montegut argo and mlotst
![](https://pad.gwdg.de/uploads/9293e853-c702-4a11-90d9-f395f9568615.png)

Questions:
* efficiently quantify the bias by taking of means
* introduce smt figure of mean mld calculation by montegut


## Small scales
Ideas to evaluate spatial and temporal small scale signals/patterns:

* comparing glider section data of mesoscale eddy with smt section of a smiliar eddy 

![](https://pad.gwdg.de/uploads/db680f4c-05fc-485a-930c-79e098b94fed.png)


![](https://pad.gwdg.de/uploads/0021b454-ee5c-418f-9808-776b31655b98.png)

![](https://pad.gwdg.de/uploads/a0178853-c102-41a9-9f27-dae5c57e5e40.png)

