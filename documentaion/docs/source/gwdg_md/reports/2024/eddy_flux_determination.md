# Eddy flux determination
| section | ML aveer |
:--:|:--:
![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/comparison/wb_inst.png) | ![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/comparison/wb_comp_sec_inst.png)
![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/comparison/wb_ml_mean_inst.png) 



### temporal and spatial mean
![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/comparison/wb_comp.png)
![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/comparison/wb_comp_sec.png)
and ml averaged
![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/comparison/wb_comp_ml_ave.png)
