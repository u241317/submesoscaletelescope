# Eddy flux dependence on Ri number
For the Reynolds decompostion two different time windows are used: 2 days and 1 week. The critical Ri number is found at 8 and 20.  The evaluation is done for the study area and the North Atlantic.

## Evaluation of Study Area
time: 1week, Ri: 8, cs: 0.09, cf: 0.04
![](../../../../../../notebooks/images/eval_ri/regional/area/vb_wb/vb_wb_slope_multiple_ri8_1W_cs0.09_cf0.04.png)
time: 1week, Ri: 20, cs: 0.15, cf: 0.04
![](../../../../../../notebooks/images/eval_ri/regional/area/vb_wb/vb_wb_slope_multiple_ri20_1W_cs0.15_cf0.04.png)
time: 2days, Ri: 8, cs: 0.02, cf: 0.01
![](../../../../../../notebooks/images/eval_ri/regional/area/vb_wb/vb_wb_slope_multiple_ri8_2d_cs0.02_cf0.01.png)
time: 2days, Ri: 20, cs: 0.04, cf: 0.01
![](../../../../../../notebooks/images/eval_ri/regional/area/vb_wb/vb_wb_slope_multiple_ri20_2d_cs0.04_cf0.01.png)

## Evaluation of NA
time: 1week, Ri: 8, cs: 0.16, cf: 0.06
![](../../../../../../notebooks/images/eval_ri/regional/area/vb_wb/vb_wb_slope_na_ri8_1W_cs0.16_cf0.06.png)
time: 1week, Ri: 20, cs: 0.28, cf: 0.08
![](../../../../../../notebooks/images/eval_ri/regional/area/vb_wb/vb_wb_slope_na_ri20_1W_cs0.28_cf0.08.png)
time: 2days, Ri: 8, cs: 0.05, cf: 0.02
![](../../../../../../notebooks/images/eval_ri/regional/area/vb_wb/vb_wb_slope_na_ri8_2d_cs0.05_cf0.02.png)
time: 2days, Ri: 20, cs: 0.09, cf: 0.03
![](../../../../../../notebooks/images/eval_ri/regional/area/vb_wb/vb_wb_slope_na_ri20_2d_cs0.09_cf0.03.png)