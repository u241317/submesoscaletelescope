# Uchida Evaluation update

* additional evaluation of our study area
* coarsegraining has influence on result > doubling coarsgraing size doubles tuning coeff.

## their domain
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/b_filtered_to_30km.png)
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/w_filtered_to_30km.png)

| wb | MLD |
:--:|:--:
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/wb_prime2.png) | ![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/MLD.png)


Results
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/wb_diag_param_tseries_2h_stone.png)
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/Ce_Cs.png)
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/Ce_Cs_mean_median.png)



## our domain
* much larger
* need to adapt threshold method, otherwise dominance of negative fluxes last time steps
* comparison in question...

![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/b_filtered_to_30km.png)
![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/w_filtered_to_30km.png)
### additional evaluation of MLD Method
| 020 | 003 |
:--:|:--:
![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/MLD.png) | ![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/mld003/MLD_003.png) 

![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/mld003/mld003MLD_method.png)
![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/mld003/mld003MLD_method_wb.png)
* wb is not going to zero below ML

| 020 | 003 |
:--:|:--:
![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/wb_prime2.png) | ![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/mld003/mld003wb_prime2.png)

| 020 | 003 |
:--:|:--:
![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/wb_diag_param_tseries_2h.png) | ![](../../../../../../results/smt_natl/uchida_eval/our_domain/wb/mld003/mld003wb_diag_param_tseries_2h.png)