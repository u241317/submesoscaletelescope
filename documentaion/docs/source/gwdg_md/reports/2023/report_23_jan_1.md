# Report January 2023

## Mean depth profiles
* vertical profiles of eg. N2 are depth averaged and plugged into parametrizations, thus the vertical shape of psi only depends on structure function
* 

### Result Density Threshold MLD Method delta 0,2 kg / m^3 instead of 0,03 (Montegut)

* Flags: 15, 18 ,19, 41, 43, 10 , 14, 30 integer

#### Tuning and Error
![](https://pad.gwdg.de/uploads/0e7b7ac3-ba14-42d3-bbbb-8434880972fe.png)
![](https://pad.gwdg.de/uploads/f25d8738-6d22-49fd-bcbb-a851bf42fe54.png)
#### Comparison of variables
![](https://pad.gwdg.de/uploads/c4830b8f-a1ea-47ad-892e-e6eeb501358c.png)
![](https://pad.gwdg.de/uploads/fc0adb36-878b-41ca-bf8c-3b5a6035ccae.png)
![](https://pad.gwdg.de/uploads/6025ed4e-3c56-47af-a0cd-52fa88df41cf.png)


### Results Fox Kemper MLD Method
* with cm =3 and 4 flags (wrong MLD)
[3,4,10,15]) ## fox mld wrong with  
[14, 30]) #negative psi
#### Tuning and Error
![](https://pad.gwdg.de/uploads/d2745294-3940-481b-9ee0-817586415636.png)
![](https://pad.gwdg.de/uploads/4cb28f65-4906-4fd2-9da8-91e02c85add6.png)

#### Comparison of variables
![](https://pad.gwdg.de/uploads/6c7758e4-8705-4ede-8c30-e207180a8362.png)

![](https://pad.gwdg.de/uploads/2e4336c8-6065-4a70-951e-d450ccc06ec0.png)
![](https://pad.gwdg.de/uploads/d0297cd6-6aea-4ad9-b4d6-bd59724aa646.png)


#### Fox Kemper MLD Method

![](https://pad.gwdg.de/uploads/74c354d2-65b6-4b9b-844c-bc9c224eacb2.png)

* C1 implemented
* extended by MLD has to be below N2 minimum!!
* here cm = 3 turns out to be close to what an by eye selection would result in (only for 4 flags); in caomparison cm =4 results in more then 10 flags! and cm = 2 gives in most cases the minmimum of N^2

##### Visulaization
> still minimum of N2
![](https://pad.gwdg.de/uploads/7c8a6956-00e9-40dd-ac13-3d319059fff3.png)
> MLD gets deeper with larger cm
![](https://pad.gwdg.de/uploads/411ce0cc-c49b-4fa1-bb5b-bc07c6000c84.png)
![](https://pad.gwdg.de/uploads/81a0f67f-3ef3-457e-aac4-8b749a2eef05.png)
![](https://pad.gwdg.de/uploads/1b5e1787-c6ce-4cbd-a790-5a721c34fad5.png)
> problematic front:
> ![](https://pad.gwdg.de/uploads/b70a43f3-81e1-44b9-837c-529fc74e9833.png)

> double mld
> ![](https://pad.gwdg.de/uploads/278720ec-09a0-452a-ac94-97329ccb840e.png)



### Results MLD 30% wb max 
* Flags: 3, 10, 20 , 14, 30 integer!

#### Tuning
![](https://pad.gwdg.de/uploads/49738613-d756-4a3e-8e92-860b4e13ae4e.png)
![](https://pad.gwdg.de/uploads/b887ae91-7a35-420e-9fb6-3e241842d486.png)

#### Comparison of variables
![](https://pad.gwdg.de/uploads/c9961d15-9875-4302-bbc7-7c65d0f174d5.png)
![](https://pad.gwdg.de/uploads/0be0ed99-2e3d-4d6c-b9c7-8e162f15485e.png)
![](https://pad.gwdg.de/uploads/39379fc8-69b9-43fe-8a66-43e933c95dd1.png)


#### Method
* below wb max where wb matches 30% of wb max
* observation in profiles with two MLs wb always high in first layer!! however in second layer values larger 30 % can be obtained...? 
![](https://pad.gwdg.de/uploads/72239cb7-fe66-4284-92a5-ae0423f06214.png)

## Discussion
### comparison of MLD of Methods
![](https://pad.gwdg.de/uploads/c29361bf-261e-4c3e-9357-7e98b4131f22.png)

* fronts 20s and first 30s have two Mixed Layers
* here we can see that Fox Kemper MLD Method diagnoses the lower part (mainly due to implementation), this explains the difference of 150m difference in average between methods
* Fox Kemper seems to be lower on typically shaped N2 (depends on the choice of cm tuning coefficient)
* wb and threshold method predict similar mlds and thus lead to similar errors and results, however diagonised slope of wb method is much closer to Greens prediction +0.5 slope


### Evaluation of slopes Ri vs wb
* Applying the calculation almost exactly results in the theoretical slopes: in particular -1/2 for Stone Parameteriztaion and 0 for Fox-Kemper parameterization (here mentioned because depth profiles values plugged into parameterizations do not reproduce slopes as precisely)
* slope of diagonised values differs strongly depending on the MLD Diagnostic: Threshold: +0.7, Fox Kemper: 0.07, wb max: 0.4

### evaluation of parameterization mean error 
* Stone and Fox Kemper parameterizations have very similiar errors compared to diagonised evaluation
* differences are Threshold 2%, Fox 11%, wb 3%
* in absolute values the error is smallest for threshold and largest for Fox Kemper MLD method

### Tuning coefficients
#### Stone Param
Threshold: 0,35
Fox: 0,2
wb: 0,33

#### Fox Param Range Fox Kemper 0,05 - 0,08
Threshold: 0,0564
Fox: 0,0191
wb: 0,0572

* Fox Kemper MLD method leads to a tuning coefficient which lies out of expected range
* wb and threshold method lead to similar coefficients

## Conclusion
* larger error and "wrong" tuning coefficient of Fox Kemper MLD algorithm are indicators for  algorithm 
    * (implementation can be improved by making it more sensitive for stratificationw ith two MLs)
* diagonised slope of more robust MLD methods indicate positive slope (although high sensitivity to MLD)
    * not predicted by Fox Kemper zero slope or Stone -0.5 slope: explanation?


## coarse graining (here only shown for Threshold MLD)
![](https://pad.gwdg.de/uploads/cb02306f-b72b-4d8a-99c1-d5644ea4e846.png)
![](https://pad.gwdg.de/uploads/69228a37-0e38-4061-8732-cf88666f57e8.png)
![](https://pad.gwdg.de/uploads/142531f0-8c28-447f-860a-c98af48ea1bd.png)
![](https://pad.gwdg.de/uploads/0b8a2446-467a-481a-b866-32e56a2167e0.png)




## Vertical profiles

### Result Density Threshold MLD Method delta 0,2 kg / m^3 instead of 0,03 (Montegut)

* Flags: 15, 18 ,19, 41, 43, 10 , 14, 30 integer

#### Tuning and Error
![](https://pad.gwdg.de/uploads/bc9c06ed-984f-42ce-a16e-72be4dc1cc27.png)
![](https://pad.gwdg.de/uploads/8aa5249d-f7af-488b-bb89-45319f0ce7f8.png)

#### Comparison of variables

![](https://pad.gwdg.de/uploads/93f1431f-401c-499d-afdd-9a0f917c21c8.png)
![](https://pad.gwdg.de/uploads/ced359f0-c0b1-40ab-8728-015883d92c51.png)
filterd one extreme outlier
![](https://pad.gwdg.de/uploads/0241908a-709e-4bdf-b493-5056f752d258.png)



### Results Fox Kemper MLD Method
* with cm =3 and 4 flags (wrong MLD)
[3,4,10,15]) ## fox mld wrong with  
[14, 30]) #negative psi
#### Tuning and Error
![](https://pad.gwdg.de/uploads/2240dc99-eef7-43c1-b21b-e13e2fcc34f5.png)
![](https://pad.gwdg.de/uploads/20f15fea-fb1a-43f3-ab0f-0687f11bde0d.png)


#### Comparison of variables
![](https://pad.gwdg.de/uploads/fefd2142-854b-40e6-a0c6-514ac3460044.png)
![](https://pad.gwdg.de/uploads/4f85a1a4-b62a-4e84-93b9-dd57bb382ec3.png)
![](https://pad.gwdg.de/uploads/564ad719-6c5a-4d87-920c-b8dab313a07f.png)

### Results MLD 30% wb max 
* Flags: 3, 10, 20 , 14, 30 integer!

#### Tuning
![](https://pad.gwdg.de/uploads/980ecbfd-eeb5-467c-8147-3fafe228f213.png)
![](https://pad.gwdg.de/uploads/8c275524-6daa-4796-bb5a-eda3e67be912.png)


#### Comparison of variables
![](https://pad.gwdg.de/uploads/d3dfa598-7ee3-407e-8569-92424af39909.png)
![](https://pad.gwdg.de/uploads/53661484-cf36-467e-84ef-d0e5e2f783cb.png)
1e4 filter Ri two values
![](https://pad.gwdg.de/uploads/0d2ac811-83ae-42a4-bbe4-a56dab42e047.png)



### comparison ri_dmean without flags

![](https://pad.gwdg.de/uploads/063b096e-f24d-4fe3-b594-8d96c2ca1c4a.png)
* larger values of MLD (especially at double MLs ) of Fox Kemper MLD method lead to large Ri numbers and which then affects the slope eg. stone slope to -0.15 (normaly -0.5)
* it effects as well the slope of diagnostics
    * since threshold on wb mld method keep diagnostic slope; here an explanation is needed??!!! maybe a bug?

### Tuning coefficient and Error
 * tuning coefficient is slightly (except from Fox mld method) affected (not suprising since Ri explodes)
* Errors of parameterization increase however suprisingly threshold mld method results in similar (slightly smaller error) Why?

### Conclusion
* full vertical profiles of n2 result in large  single values in calculating ri (explode), which result in strong deviations!!
* large Ri numbers lead to failure of Stone parameterization (>> advantage of Fox kemper)
* therefore preference to mean depth values used for param!!!!



