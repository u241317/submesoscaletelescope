# Uchida evaluation of wb_prime
## based on gaussian spatial filtering

Gaussian 30km filter
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/w_filtered_to_30km.png)
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/b_filtered_to_30km.png)

compared to reference Uchida
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/refeference/gaussian.png)

### Diagnosed vs. parameterized
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/wb_prime_diag_param_2h.png)
<!-- ![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/wb_prime_diag_param_2h_stone.png) -->


compared to reference Uchida
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/refeference/mli_snap.png)

### Diagnosed wb_prime
computation in Uchida not precise: b_prime*w_prime unequal  $ <w_{prime}  b_{prime}> = <wb> - <w><b>$
```python
wb_dat     = ((w_sel - w_filtered_to_30km) * (b_sel - b_filtered_to_30km)) * dz_diff_ext * Mask_MLD
wb_dat_sum = wb_dat.sum(dim='depthi', skipna=True)
wb_prime   = wb_dat_sum / MLD
wb_prime   = wb_prime * mask_land_100
```

![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/wb_prime2.png)

> boundaries seem to be wrong - due to filtering? >> Fixed with gcm filtering on irregular grid and adapted mask

### Evaluation of spatial median over time - here one week
<!-- ![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/wb_diag_param_tseries.png) -->
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/wb_diag_param_tseries_2h.png)
Why does the median does not fit in mean? Should the red line not be correct in average?

including stone
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/wb_diag_param_tseries_2h_stone.png)

> difference of Stone parameterizstion for wb_prime is ```1/np.sqrt(1+Ri) ```, and appears in median not to be significant

compared to reference Uchida
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/refeference/mli_eval.png)

### MLD
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/MLD.png)

>different threshold used to compute MLD

compared to reference Uchida
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/refeference/mld.png)


## Other properties
scaling difference Stone Fox Kemper
| | |
|---|---|
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/diff_stone_fox-kemper.png) | ![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/N2_mean.png)
![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/Ri.png) | ![](../../../../../../results/smt_natl/uchida_eval/uchida_domain/wb/1oRi.png)


## Other
to obtain computation over three months 3d fields of b dbdx dbdy w are required
if Stone evaluation shall be included N2 is also required


### Differences
* MLD threshold
* reference density
* Gaussian filtering at boundaries
* time span