# Report Juni 2023


## Linear regression
> following "Statistical analysis in climate research" Hans von Storch

### example strong slope: wb vs ri
| all  |  bin
:-------------------------:|:-------------------------:
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/linreg_all.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/linreg_binned_no_weights.png)

### example no slope: psi vs ri
| all  |  bin
:-------------------------:|:-------------------------:
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/linreg_all.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/linreg_binned_no_weights.png)

How to implement the weights for the bins?
* weight on the errors?
* in x and y?




wb | psi
:--:|--:
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/multiple_binned_all_wb_ri.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/multiple_binned_all_psi_ri.png)

![](../../../../../../notebooks/images/eval_ri/regional/area/location_fronts.png)

![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/front_cut_no_map1.png)

## Reynolds Decompostion
### dependecy of time average:

### ub prime
| 1D | 2D | 4D
:---:|:---:|:---:
![](../../../../../../notebooks/images/eval_ri/regional/reynolds_decomposition/ub_prime_1D_mean.png) | ![](../../../../../../notebooks/images/eval_ri/regional/reynolds_decomposition/ub_prime_2D_mean.png) | ![](../../../../../../notebooks/images/eval_ri/regional/reynolds_decomposition/ub_prime_4D_mean.png)

### vb prime
| 1D | 2D | 4D
:---:|:---:|:---:
![](../../../../../../notebooks/images/eval_ri/regional/reynolds_decomposition/vb_prime_1D_mean.png) | ![](../../../../../../notebooks/images/eval_ri/regional/reynolds_decomposition/vb_prime_2D_mean.png) | ![](../../../../../../notebooks/images/eval_ri/regional/reynolds_decomposition/vb_prime_4D_mean.png)

## different time windows

### week
wb | psi
:--:|--:
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/multiple_binned_all_wb_ri_1W.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/multiple_binned_all_psi_ri_1W.png)
### 2 days
wb | psi
:--:|--: 
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/multiple_binned_all_wb_ri_2D.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/multiple_binned_all_psi_ri_2D.png)