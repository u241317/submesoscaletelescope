# Report January 2

Here compared with brueggeman 2014
## Spindown Dependency of horizontal and vertical eddy fluxes
* tuned for minimum mean error in psi

### over Ri
![](https://pad.gwdg.de/uploads/c891d3b0-8058-4634-b8bb-e0a4e199f2b4.png)
![](https://pad.gwdg.de/uploads/4a31285b-5daf-49b7-9140-70ed40a0b5bc.png)

slopes diag brueggemann: vb: 1 wb: 0.1 psi: -0.1 K: -1.2

qualitative agreement of diagonised slopes; exception is vb

### over alpha 
![](https://pad.gwdg.de/uploads/3263112b-05dd-4c95-9b39-8c3ec5f950fa.png)
![](https://pad.gwdg.de/uploads/33482795-45bf-44de-b889-b91b4f5c6041.png)

slopes diag brueggemann: vb: 3.3 wb: 2.3 psi: 1.3 K: -0.1

* qualitative agreement of diagonised slopes; 
* howvere all slopes in brueggemann are steeper
* slopes of Stone and Fox in K have opposite sign


slopes:
![](https://pad.gwdg.de/uploads/03339ad1-5e62-4736-a2f3-4ad2433151ca.png)

## Sensitivity !
![](https://pad.gwdg.de/uploads/305a0a19-d4ad-490a-ba46-21d2cbe0de92.png)
![](https://pad.gwdg.de/uploads/205acf54-c01b-41eb-97b6-2719bbf3081c.png)

slopes STD brueggemann: vb: 1 wb: 0.01
* vb, psi, K: qualitative agreement
* wb no agrrement


## Discussion

sssss