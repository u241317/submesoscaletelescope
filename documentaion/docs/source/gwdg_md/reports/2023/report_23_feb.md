# Report February 2023


https://swiftbrowser.dkrz.de/public/dkrz_60d6f2c9-4d53-41cc-94a6-8e4c9370d1e6/SMT/videos/?show_all


## Paper Figures

### Introduction SMT
![](https://pad.gwdg.de/uploads/100d3e8d-7068-45cc-bc5c-598d84f64d20.png)
![](https://pad.gwdg.de/uploads/8061d5d8-26a4-43d7-8db3-2c804f9875b8.png)



SSH             |  SST
:-------------------------:|:-------------------------:
![](https://pad.gwdg.de/uploads/160453e7-ee1d-49d5-a756-c75b746cf5c5.png) |  ![](https://pad.gwdg.de/uploads/34054231-1377-4778-b58b-db2fa78fd712.png)



### Location of submesoscale fronts
![](https://pad.gwdg.de/uploads/fbaf929e-e6fc-41b5-93eb-f8ef140d759b.png)

### Single Ocean front!
![](https://pad.gwdg.de/uploads/259f7243-67ea-4339-b608-9ef9d674963c.png)

### Profiles
![](https://pad.gwdg.de/uploads/d721d5a8-0fe2-46c7-94a2-7c7812eba90f.png)


### Held Schneider vs full streamfunction
![](https://pad.gwdg.de/uploads/84e9af06-3c26-45d0-b2c5-63acd3cd883d.png)




### Statistics
param vs diag              | Error
:-------------------------:|:-------------------------:
![](https://pad.gwdg.de/uploads/6380bee9-67b2-4281-93d1-c68c2d3ff04d.png) | ![](https://pad.gwdg.de/uploads/bc4e5e3b-551e-4ebe-9c8a-7a53c6cdc923.png)

#### alpha dependency (spindown eval.)
![](https://pad.gwdg.de/uploads/27c12c63-dff7-4bc9-ab31-93e570524a50.png)

#### Ri dependency (spindown eval.)
![](https://pad.gwdg.de/uploads/eb1906e9-8b49-4da7-b722-35ee1a7bf094.png)

#### Sensitivity
![](https://pad.gwdg.de/uploads/1a357410-18ee-4a22-af26-c004ba8f7435.png)

## Poster Critics
* cherry picking of fronts, find algortihm to asses all fronts (problem: I cannot even find correlation in ths handpicked ones)
* EOF analysis to find significant correlations in fronts
* add significance/ r value/ p value to linear regression
* possibility to use 2d spectra for model evaluation
* explanation of why is w'b' always positive: framework of ocean front
* show all frontal profiles (eg. normalization)
* search really unstable front, comma shape, buoyancy which gets out of front, those compartments go into inverse cascade, neglected in existing parameterizations
* histogram of frontal properties
* normalize fronts
* image recognition tequniches to identify all fronts
* 1km satellite dataset from viir https://www.earthdata.nasa.gov/learn/find-data/near-real-time/viirs
* inverse and forward cascade: fox kemper feeds into forward cascade
* coarse graining of front, filtering and evaluating all fronts
* map of horizontal ri number and w'b' 
* w'b' what time mean used to define an eddy?

### Histogram of param vs diag Data!
psi | wb | vb
:---------------------:|:-----------------:|:----------------:
![](https://pad.gwdg.de/uploads/f9ace80b-6935-4e16-b23d-c11dc3683c06.png) | ![](https://pad.gwdg.de/uploads/941228f0-b3d2-48ba-9fab-0a58cc7a9fab.png) | ![](https://pad.gwdg.de/uploads/9c2a5bd5-ba74-4b99-b7e8-6bc61277f73a.png)
 
* optimized for psi!
* all over "postivie correlation"
* all p values are small >> statistically significant
* spread is "equally" on positve and negative side
* paramterizations roughly capture behaviour

### Forcing situation
#### Ekman Pumping
##### large
instantan | mean
:------------:|:------------:
![](https://pad.gwdg.de/uploads/7911c01b-51c3-40e4-8658-aabc6205f18f.png) | ![](https://pad.gwdg.de/uploads/79d517d6-5424-4086-a706-ea4596c1f83a.png)

##### zoom
instantan | mean
:------------:|:------------:
![](https://pad.gwdg.de/uploads/f36405a1-ec64-4cd3-a815-6f957bf5f785.png) | ![](https://pad.gwdg.de/uploads/f8c450b5-4aa2-433e-a728-a28273e1c546.png)


#### Wind stress
##### t = 0
![](https://pad.gwdg.de/uploads/00801f92-4f79-462e-a0a2-c36a1bdf4228.png)
##### t = 168
![](https://pad.gwdg.de/uploads/0137aa4f-7526-4d55-94c8-ed0387bc7511.png)
##### mean
![](https://pad.gwdg.de/uploads/fcaf6fd6-d789-4956-942a-aad85fa5e31e.png)



#### Windstress Curl
![](https://pad.gwdg.de/uploads/d2f395a9-0f52-4ebd-9cc1-d5e1cef4e138.png)
![](https://pad.gwdg.de/uploads/fa0098e8-93f2-4058-88b0-11e77a3715e8.png)


### MLD
#### $\Delta \rho = 0.03$ Montegut
![](https://pad.gwdg.de/uploads/c5a11feb-056f-47f8-8c88-b3113af5deaf.png)
#### $\Delta \rho = 0.2$ adapted threshold in focus region
![](https://pad.gwdg.de/uploads/33760192-6d58-4a7a-aa3f-153a3cacd764.png)
#### MLD Algorithms
![](https://pad.gwdg.de/uploads/abb0193c-9df6-4da3-a4e5-12334eeaa130.png)



## Updated filtered domain evaluation

* data is averaged over mld
* negative wb prime is filtered out
* after taking logarithm, negative x values are filtered out (few points only)


filter | single | multiple | na
:-----------:|:-----------:|:-----------:|:-----------:
m2 > 0 | ![](https://pad.gwdg.de/uploads/07220738-0f4b-4970-98c5-c104faf3dd3b.png) | ![](https://pad.gwdg.de/uploads/321f8b6a-d9b0-46de-addc-cca0a9aeb0e9.png) | ![](https://pad.gwdg.de/uploads/dcb66497-c899-4bf0-8f66-1a79ad7a4f1b.png)
slope | ![](https://pad.gwdg.de/uploads/219bace0-d8d0-430c-93c6-b7b678367a2e.png)  | ![](https://pad.gwdg.de/uploads/5bf2f0ad-695f-483b-ac1d-d4b97cad00b4.png)  | ![](https://pad.gwdg.de/uploads/8d5ea4f7-9797-4dfb-842d-1df37c93bd4e.png)
m2 >1e-8 | ![](https://pad.gwdg.de/uploads/862f5271-72da-48b2-97e1-e20f96b32d7f.png) | ![](https://pad.gwdg.de/uploads/5131f79f-d480-4a31-be90-1081a5530b69.png) | ![](https://pad.gwdg.de/uploads/29be311a-6aa4-46cc-b589-54662bdbdcf0.png)
slope | ![](https://pad.gwdg.de/uploads/02b56741-93f7-4feb-9a56-a144ec642702.png) | ![](https://pad.gwdg.de/uploads/94330f09-cc54-4279-93c0-5c6b6a8ec879.png) | ![](https://pad.gwdg.de/uploads/16da2a41-43f2-47f9-a1e6-99298419169d.png)
m2 > 3e-8 | ![](https://pad.gwdg.de/uploads/a4d21779-dd78-44a9-a7d1-7e27f3ce8054.png) | ![](https://pad.gwdg.de/uploads/92b2b514-ed60-4478-815c-1f32e961de7d.png) | ![](https://pad.gwdg.de/uploads/55bd44c2-7fd2-442b-94fd-592a93e11dfc.png) 
slope | ![](https://pad.gwdg.de/uploads/752fa8be-7e15-4d7e-994f-c9ec876fc091.png)    | ![](https://pad.gwdg.de/uploads/510415fe-e375-49e1-b6d8-d6ef6f38e376.png) | ![](https://pad.gwdg.de/uploads/72608322-e28a-4061-b9c3-f6f3669a7cee.png)


> result of single front indicates same slope like when evaluating independent selected single fronts
> first filter m2 > 1e-8 already includes a lot of nans but does not have a significant impact on slope
> second filter m2 > 3e-8 filters single front quite well; here slopes get steeper only for mltiple and na front
> 






 








