# heat flux approximation
 -----------------------
#
The heat flux is approximated by the following formula:

$$ q = - \rho c_p  w \nabla T $$

where :math:`c_p` is the specific heat capacity, :math:`\rho` is the density, :math:`w` is the vertical velocity and :math:`T` is the temperature.

![](../../../../../../results/smt_natl/heat_flux/map_param_fronts_portrait_w_DT_98.0.png)
![](../../../../../../results/smt_natl/heat_flux/map_param_fronts_portrait_q_vert_98.0.png)

> division by surface area?

---
##  $$w^\prime T^\prime$$
> no vertical interpolation to the same depth level > error might be significant at greater depths


![](../../../../../../results/smt_natl/heat_flux/wT_prime/map_param_fronts_portrait_9.5.png)
![](../../../../../../results/smt_natl/heat_flux/wT_prime/map_param_fronts_portrait_51.5.png)
![](../../../../../../results/smt_natl/heat_flux/wT_prime/map_param_fronts_portrait_99.5.png)
![](../../../../../../results/smt_natl/heat_flux/wT_prime/map_param_fronts_portrait_202.75.png)
![](../../../../../../results/smt_natl/heat_flux/wT_prime/map_param_fronts_portrait_290.5.png)
![](../../../../../../results/smt_natl/heat_flux/wT_prime/map_param_fronts_portrait_496.5.png)

### Components
depthc levels: 9.5, 51.5, 99.5, 202.75, 290.5, 496.5
![](../../../../../../results/smt_natl/heat_flux/wT_prime/wT_components_9.5.png)
![](../../../../../../results/smt_natl/heat_flux/wT_prime/wT_components_51.5.png)
![](../../../../../../results/smt_natl/heat_flux/wT_prime/wT_components_99.5.png)
![](../../../../../../results/smt_natl/heat_flux/wT_prime/wT_components_202.75.png)
![](../../../../../../results/smt_natl/heat_flux/wT_prime/wT_components_290.5.png)
![](../../../../../../results/smt_natl/heat_flux/wT_prime/wT_components_496.5.png)