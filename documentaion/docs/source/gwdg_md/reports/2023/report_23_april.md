# Report April 2023
## Evaluation of Domain 

* filter negative wb

### Location of single fronts filtered by Richardsonnumber


<!-- <p><img src="../../../../../../notebooks/images/m188_offline/NA_002dgr/ri_vs_wb_regional/diag_param/location_fronts.png"  width="60%" ><BR CLEAR=LEFT> -->

![](../../../../../../notebooks/images/eval_ri/regional/area/location_fronts.png)


### M2 and Ri Filter


here only M2 Filter: 

large NA domain; regional domain; single front

|  M2 >0e-8 |  M2 >1e-8 |  M2 >3e-8 |  M2 >5e-8 
:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:
![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/na_front_m2_lim_var_filter_0_contf.png) | ![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/na_front_m2_lim_var_filter_1e-08_contf.png) |![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/na_front_m2_lim_var_filter_3e-08_contf.png) |![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/na_front_m2_lim_var_filter_5e-08_contf.png) |
![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/multiple_front_m2_lim_var_filter_0_contf.png) | ![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/multiple_front_m2_lim_var_filter_1e-08_contf.png) |![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/multiple_front_m2_lim_var_filter_3e-08_contf.png) |![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/multiple_front_m2_lim_var_filter_5e-08_contf.png) |
![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/single_front_m2_lim_var_filter_0_contf.png) | ![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/single_front_m2_lim_var_filter_1e-08_contf.png) |![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/single_front_m2_lim_var_filter_3e-08_contf.png) |![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/single_front_m2_lim_var_filter_5e-08_contf.png) |

### Evaluating correlation of wb and psi with M2 and Ri filter:
Ri vs WB:
| NAT | REG | SIN
|:-------------------------:|:-------------------------:|:-------------------------:
 ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/na_front_m2_lim_diag_param_structure_m2_filter.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/multiple_front_m2_lim_diag_param_structure_m2_filter.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/single_front_m2_lim_diag_param_structure_m2_filter.png)
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/na_front_m2_lim_diag_param_structure_ri_filter.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/multiple_front_m2_lim_diag_param_structure_ri_filter.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/single_front_m2_lim_diag_param_structure_ri_filter.png)

* single front gets slope similar to all evaluated single fronts 0.72, see sensitivity evaluation of february, new eval. consitent with old evaluation looking on slope of single front
* cut out single front does has a constant slope independently from filter!

Ri vs psi:
| NAT | REG | SIN
:-------------------------:|:-------------------------:|:-------------------------:
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/na_front_m2_lim_diag_param_structure_m2_filter.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/multiple_front_m2_lim_diag_param_structure_m2_filter.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/single_front_m2_lim_diag_param_structure_m2_filter.png)
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/na_front_m2_lim_diag_param_structure_ri_filter.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/multiple_front_m2_lim_diag_param_structure_ri_filter.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/single_front_m2_lim_diag_param_structure_ri_filter.png)


* all filtering methods fail to obtain a significant correlation >> clouds are to large (with the exception of the single cut out front)
* if there is a correlation Ri ~ wb the exponent is larger then +0.5; not even close to Stone -0.5 or Fox-Kemper 0.0 
* 

### a few notes on data processing:
Raw data:
* only filtered out negative w'b'

| NAT | comments
:-------------------------:|:-------------------------:
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/raw_data/na_front_m2_lim_diag_param_raw_structure.png) | negative Ri numbers are filtered out in the above evaluation in order to apply linear regression; integral effect of mu is 2/3
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/raw_data/na_front_m2_lim_diag_param_S_m2_filter.png) | applying the structure function with the vertical profile and then integrating over depth leads to very similar data cloud however parameterizations widen on y axis >> most likely a processing issue

Looking on the map shows similar filtering effect for Ri and M2 filter:
* look almost identical if map is evaluated / dependence on thresholds
* Ri filter more elegant since it is nondimensional and does not depend on absolute values >> easier to apply in different dynamical regimes

eg:

| M2 > 5e-8 | Ri < 10
:--:|:--:
![](../../../../../../notebooks/images/eval_ri/regional/area/filter_m2/multiple_front_m2_lim_var_filter_5e-08_contf.png) | ![](../../../../../../notebooks/images/eval_ri/regional/area/filter_ri/multiple_front_m2_lim_var_filter_10.0_contf.png)


* jumping from single fronts to domains leads increases difficulties with sign dependence of M2 and v'b' or u'b' >> these are relevant in the evaluation of full formulation of streamfunction
* approach here: projection of lateral fluctuation on gradient of m2

  ```python
   A = A.assign(Vb_cross_grad = ( m2x_mask * ub_mask + m2y_mask * vb_mask) / (np.sqrt(np.power(m2x_mask,2) + np.power(m2y_mask,2))) ) # projection of fluctuation on front gradient 

   A = A.assign(m2_cross_grad = np.sqrt(np.power(m2x_mask,2) + np.power(m2y_mask,2)))
   ```
* problem: horizontal fluctuation rarely in direction of frontal buoyancy gradient; sign indicates fluctuation in and against front
* M2 can only be evaluated as a magnitude? which sign should the vector have?
   ```python
    def calc_streamfunc_full(wb, m2, vb, n2, abs_grad_b):
    """calc streamfunction, see Brüggemann 2014
    m2 here is the crossfront gradient, vb the crossfront fluctuation"""
    return  (vb * n2 - wb * m2) / np.power(abs_grad_b, 2)
   ```
    * wb and n2 positive, vb can have a sign (negative sign assigned to absolute value to have consistency with sign of parameterization); 
    * which sign should m2 have? (here absolute values are taken)
     * left term is 2 to 3 orders larger then right term
     * in present config all parameters except vb are positive


## new Eror evaluation of single fronts
| mean__ error | relative error |
:------:|:------:
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/param_vs_diag/psi_diag_vs_pram_cfopt_cross_single_norm_nov22_mean_error.png) | ![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/param_vs_diag/psi_diag_vs_pram_cfopt_cross_single_norm_nov22_rel_error.png)

* minimizing relative error leads to untypical tuning coefficients >> the regimes are to different
* minimizing mean error shows that only a few fronts have values larger then two times the mean psi: negative! evaluation optimizes mainly fronts with strong overturning...


python code:
[link](../../../../../../notebooks/december22/slope_param_diag_domain.py)

## Domain: Diagnostic vs Parameterization -> visual

### wb
![](../../../../../../notebooks/images/eval_ri/regional/area/visual_eval/multiple_wb.png)
### vb
![](../../../../../../notebooks/images/eval_ri/regional/area/visual_eval/multiple_vb.png)

### psi
![](../../../../../../notebooks/images/eval_ri/regional/area/visual_eval/multiple_psi.png)
### psi (held schneider)
![](../../../../../../notebooks/images/eval_ri/regional/area/visual_eval/multiple_psi_held.png)
### K
![](../../../../../../notebooks/images/eval_ri/regional/area/visual_eval/multiple_K.png)

### background state
N2 | M2
:----:|:----:
![](../../../../../../notebooks/images/eval_ri/regional/area/visual_eval/multiple_N2.png) | ![](../../../../../../notebooks/images/eval_ri/regional/area/visual_eval/multiple_m2.png)

* Influence of N2 shows a strong meridional dependence
* same accounts for M2