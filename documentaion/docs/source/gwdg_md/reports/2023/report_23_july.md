# Report July 2023
## Error evaluation:
### optimization coefficients
minimizing the mean error of psi, wb or vb
| Fox-Kemper | Stone
:--:|:--:
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/param_vs_diag/tuning_fox_dec22.png) | ![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/param_vs_diag/tuning_stone_dec22.png)


### wb vb dependence on opt. tuning coeficcient: mind the legend
| opt for | error
:--:|:--:
|wb| ![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/param_vs_diag/wb_vb_error_wb.png)
|vb| ![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/param_vs_diag/wb_vb_error_vb.png)
|psi| ![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/param_vs_diag/wb_vb_error_psi.png)

### psi
| opt. for wb | opt. for vb | opt. for psi
:--:|:--:|:--:
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/param_vs_diag/psi_diag_vs_pram_error_wb.png) | ![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/param_vs_diag/psi_diag_vs_pram_error_vb.png) | ![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/param_vs_diag/psi_diag_vs_pram_error_psi.png)

Result:
Tuning coefficients are so close to each other that optimization does not depend on the choice of optimized variable.


## novel calculation of slope

* bins with less then 20 counts excluded (2 on the left, 4 on the right)
* evaluation of absolute values of vb
* are the units correct? 
* why does vb data needs binning while psi and wb didnt?
* why are confidence intervals of first slope so hugh?
* why does stone have a different intercept? why does Fox-kemper appears to fit so well?


| vb | wb
:--:|:--:
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_vb_regional/multiple_binned_vb_ri_1W.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/multiple_binned_wb_ri_1W.png) 
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_vb_regional/multiple_binned_1slope_vb_ri_1W.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/multiple_binned_1slope_wb_ri_1W.png) 
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_vb_regional/multiple_binned_2slope_vb_ri_1W.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_wb_regional/multiple_binned_2slope_wb_ri_1W.png) 

and psi:
raw | 1slope | 2slope
:--:|:--:|:--:
![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/multiple_binned_psi_ri_1W.png)  | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/multiple_binned_1slope_psi_ri_1W.png) | ![](../../../../../../notebooks/images/eval_ri/regional/ri_vs_psi_regional/multiple_binned_2slope_psi_ri_1W.png) 

## novel MLD climatology
### MLD of monthly mean with typical threshold
![](../../../../../../notebooks/images/first_paper/overview/MLD/monthly_03.png) 

| large | small
:--:|:--:
![](../../../../../../notebooks/images/first_paper/overview/MLD/MLD_intercomparison2.png) | ![](../../../../../../notebooks/images/first_paper/overview/MLD/MLD_intercomparison_small.png)  

| spatial ave | wb method 
:--:|:--:
![](../../../../../../notebooks/images/first_paper/overview/wb_coarse_fine.png)  | ![](../../../../../../notebooks/images/first_paper/overview/MLD/MLD_intercomparison.png)

* wb method does not work in regions without strong overturning in the mixed layer
* it is bound to the maximum value in the ML; in the absence of strong wb is noise; when will it go to 5%? thats why it also find deep ml in regions with shallow ML
* only works at fronts

## wind forcing
![](../../../../../../notebooks/images/forcing/windforcing_temp_168.png)  
![](../../../../../../notebooks/images/forcing/windforcing_mean_168.png) 

## maps of fronts
![](../../../../../../notebooks/images/first_paper/overview/map_param_fronts_portrait.png)

# non-frontal behaviour of parameterization (full alongfront average)
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld//slice/eddie_cut_1.png)

![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/eddie_cut_no_map2.png)

min alongfront average:
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/front_cut_no_map_eddie3_pyic.png)


* why are there strong eddy fluxes on the right hand side? wrong isopycnals?

## all eddies 2-8 slice with approx 10 points alongfront average
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/map_eddy_slice2.png)


* mind variying scales of eddies
* stratification is dominant
* eddies vary to strongly in size...
* roundness differs significantly
* some are not optimally cut
* only F2 and F6 have wb fluxes as in frontal regions - these are the largest edies
* nonetheless one might conclude that overturning at eddies is much weaker
* vb almost always with unexpected sign (to weak?)


| 2 | 3 |
:--:|:--:
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/front2_crosssection_eddie.png) | ![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/front3_crosssection_eddie.png)
| 4 | 5 |
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/front4_crosssection_eddie.png) | ![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/front5_crosssection_eddie.png)
| 6 | 7 |
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/front6_crosssection_eddie.png) | ![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/front7_crosssection_eddie.png)
| 8 | 9 |
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/front8_crosssection_eddie.png) | 

![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/eddie_cut_thin_2.png)
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/eddie_cut_thin_3.png)
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/eddie_cut_thin_4.png)
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/eddie_cut_thin_5.png)
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/eddie_cut_thin_6.png)
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/eddie_cut_thin_7.png)
![](../../../../../../notebooks/images/eval_ri/front/inclined_fronts/param_with_mean_profile/threshold_mld/slice/eddie_cut_thin_8.png)