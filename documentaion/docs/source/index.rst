.. mypackage documentation master file, created by
   sphinx-quickstart on Mon Jul 18 11:49:22 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Submesoscale Telescope documentation!
================================================

SMT Wave
=====================

.. toctree::
   :maxdepth: 2

   /gwdg_md/eddy/anti_cyclone.md
   /gwdg_md/eddy/eddy_features.md
   /gwdg_md/swot/swot_l3_eval.md
   /gwdg_md/swot/swot_product.md
   /gwdg_md/tides_notides/KE_bandpass.md
   /gwdg_md/tides_notides/KE_spectra_mooring.md
   /gwdg_md/tides_notides/sections.md
   /gwdg_md/tides_notides/KE_spectra.md
   /gwdg_md/tides_notides/ssh_spectra.md
   /gwdg_md/tides_notides/temperature_spectra.md
   /gwdg_md/tides_notides/bias_map.md
   /gwdg_md/tides_notides/maps.md


Overturning of mixed layer eddies in a submesoscale resolving simulation of the North Atlantic
==============================================================================================

.. toctree::
   :maxdepth: 2

   /gwdg_md/other/abstract
   /gwdg_md/non_frontal/eddy_overturning
   /gwdg_md/reports/2024/eddy_flux_determination
   /gwdg_md/reports/2024/update_uchida_eval
   /gwdg_md/reports/2024/update_ri_dependence
   /gwdg_md/reports/2023/report_24_mar
   /gwdg_md/reports/2023/update_24_mar
   /gwdg_md/reports/2023/report_23_july2
   /gwdg_md/reports/2023/report_23_july
   /gwdg_md/reports/2023/report_23_may
   /gwdg_md/reports/2023/report_23_april
   /gwdg_md/reports/2023/report_23_feb
   /gwdg_md/reports/2023/report_23_jan_2
   /gwdg_md/reports/2023/report_23_jan_1
   /gwdg_md/reports/2022/report_22_dec
   /gwdg_md/reports/2022/report_22_nov_2
   /gwdg_md/reports/2022/report_22_nov
   /gwdg_md/reports/2022/report_22_oct
   /gwdg_md/reports/2022/report_22_sep
   /gwdg_md/reports/2022/report_22_aug
   /gwdg_md/reports/2022/report_22_jul

Model Evaluation
================
.. toctree::
   :maxdepth: 2
   
   /gwdg_md/reports/2022/report_22_jun_2
   /gwdg_md/reports/2022/report_22_jun
   /gwdg_md/reports/2022/report_22_may
   /gwdg_md/reports/2022/report_22_apr
   /gwdg_md/reports/2022/report_22_mar
   /gwdg_md/reports/2022/report_22_feb

Usefull Links
=============
.. toctree::
   :maxdepth: 2

   /gwdg_md/other/SMT_NATL.md
   /gwdg_md/other/links
   /gwdg_md/other/smt_resolutions

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
