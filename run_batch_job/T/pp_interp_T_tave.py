import sys
import numpy as np
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
import xarray as xr
import pandas as pd
from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')

### configure paths
run      = 'ngSMT_tke'
savefig  = False
path_fig = '../pics/'
nnf      = 0

gname = 'smt'
lev   = 'L128'

path_data    = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/????-??/'

#####################################################################
### Interpolate temperature to cell surfaces over all layers
levs   = np.arange(depthc.size)
nz     = levs.size
depthc = depthc[levs]

cc        = int(59799625)
nzi       = int(nz+1)

fpatho = '/scratch/u/u241317/calc/pp_calc_T_interp.nc'
fo = Dataset(fpatho, 'w', format='NETCDF4')
#fo.createDimension('depthc', nz) # depth
fo.createDimension('depthi', nzi) 
fo.createDimension('cc', cc) # new dim cell center

nc_T   = fo.createVariable('T','f4',('depthi','cc'))
ncv    = fo.createVariable('depthi','f4','depthi')
ncv[:] = depthi[:nzi]

# --- prepare output netcdf file
ts      = pyic.timing(ts, 'prepare nc')
varfile = 'T_S'
layers  = (  ['sp_001-016']*16 + ['sp_017-032']*16 + ['sp_033-048']*16
          + ['sp_049-064']*16 + ['sp_065-080']*16 + ['sp_081-096']*16 + ['sp_097-112']*16)

# --- load times and flist # only to get time data - it is not the selection of data
time0      = np.datetime64('2010-03-09T01:00:00')
ts         = pyic.timing(ts, 'load times and flist')
search_str = f'{run}_T_S_sp_001-016_*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')

itd     = np.argmin((timesd-time0).astype(float)**2)
fpath   = flist_tsd[itd]
pdtime0 = pd.to_datetime(time0) 
tstr    = pdtime0.strftime('%Y%m%d')+'T010000Z'


#only january
path_data = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-01/'

interp_coef = 1/(depthc[1:]-depthc[:-1]) * (depthi[1:-1] - depthc[:-1])

for aa, itd in enumerate(range(1,13)):
    pdtimeitd = pd.to_datetime(timesd[itd])
    print(pdtimeitd); print(f'timestep:{itd}');
    tstr      = pdtimeitd.strftime('%Y%m%d')+'T010000Z'
    print(tstr)
    if itd == 1:
        ###  version 2 interpolated
        for kk, lev in enumerate(levs[:-1]):
            ts = pyic.timing(ts, 'loop step')
            if lev == 0:
                # --- load temperature 1 
                fname = f'{run}_{varfile}_{layers[lev]}_{tstr}.nc'
                fpath = f'{path_data}{fname}'
                var   = f'T{lev+1:03d}_sp'
                print(f'kk = {kk}/{nz}; fname = {fname}; var = {var}')
                fi   = Dataset(fpath, 'r')
                T1   = fi.variables[var][itsd[itd],:]
                fi.close()
                # --- load temperature 2 
                fname = f'{run}_{varfile}_{layers[lev+1]}_{tstr}.nc'
                fpath = f'{path_data}{fname}'
                var   = f'T{lev+2:03d}_sp'
                print(f'kk = {kk+1}/{nz}; fname = {fname}; var = {var}')
                fi   = Dataset(fpath, 'r')
                T2   = fi.variables[var][itsd[itd],:]
                fi.close()
                #interpolate
                nc_T[kk+1,:] = T1[:cc] + (T2[:cc]-T1[:cc])*interp_coef[kk]
                # swap fields
                T1 = T2
            else:
                # --- load temperature 2 
                fname = f'{run}_{varfile}_{layers[lev+1]}_{tstr}.nc'
                fpath = f'{path_data}{fname}'
                var   = f'T{lev+2:03d}_sp'
                print(f'kk = {kk+1}/{nz}; fname = {fname}; var = {var}')
                fi   = Dataset(fpath, 'r')
                T2   = fi.variables[var][itsd[itd],:]
                fi.close()
                #interpolate
                nc_T[kk+1,:] = T1[:cc] + (T2[:cc]-T1[:cc])*interp_coef[kk]
                # swap fields
                T1 = T2
            nc_T[:1,:]   = np.NaN
            nc_T[-1:,:]  = np.NaN
        else:
            if lev == 0:
                # --- load temperature 1 
                fname = f'{run}_{varfile}_{layers[lev]}_{tstr}.nc'
                fpath = f'{path_data}{fname}'
                var   = f'T{lev+1:03d}_sp'
                print(f'kk = {kk}/{nz}; fname = {fname}; var = {var}')
                fi   = Dataset(fpath, 'r')
                T1   = fi.variables[var][itsd[itd],:]
                fi.close()
                # --- load temperature 2 
                fname = f'{run}_{varfile}_{layers[lev+1]}_{tstr}.nc'
                fpath = f'{path_data}{fname}'
                var   = f'T{lev+2:03d}_sp'
                print(f'kk = {kk+1}/{nz}; fname = {fname}; var = {var}')
                fi   = Dataset(fpath, 'r')
                T2   = fi.variables[var][itsd[itd],:]
                fi.close()
                #interpolate
                T_temp = T1[:cc] + (T2[:cc]-T1[:cc])*interp_coef[kk]
                # average and save
                nc_T[kk+1,:] =   (nc_T[kk+1,:] * 1/(itd-1) + T_temp)/itd
                # swap fields
                T1 = T2
            else:
                # --- load temperature 2 
                fname = f'{run}_{varfile}_{layers[lev+1]}_{tstr}.nc'
                fpath = f'{path_data}{fname}'
                var   = f'T{lev+2:03d}_sp'
                print(f'kk = {kk+1}/{nz}; fname = {fname}; var = {var}')
                fi   = Dataset(fpath, 'r')
                T2   = fi.variables[var][itsd[itd],:]
                fi.close()
                #interpolate
                T_temp = T1[:cc] + (T2[:cc]-T1[:cc])*interp_coef[kk]
                # average and save
                nc_T[kk+1,:] =   (nc_T[kk+1,:] * 1/(itd-1) + T_temp)/itd
                # swap fields
                T1 = T2
            nc_T[:1,:]   = np.NaN
            nc_T[-1:,:]  = np.NaN
            
fo.close()
