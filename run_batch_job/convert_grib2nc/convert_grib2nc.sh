#!/bin/sh

for i in {01..31}
do
    FILES="/pool/data/ERA5/E5/sf/an/1H/151/E5sf00_1H_2010-03-${i}_151.grb"
    echo $FILES
    OUTFILES="/work/mh0033/m300878/observation/pressure/E5sf00_1H_2010-03-${i}.nc"
    cdo setgridtype,regular $FILES $OUTFILES
done
