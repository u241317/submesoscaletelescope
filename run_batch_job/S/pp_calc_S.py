import sys
import numpy as np
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
import xarray as xr
import pandas as pd
from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')

### configure paths
run      = 'ngSMT_tke'
savefig  = False
path_fig = '../pics/'
nnf      = 0

gname = 'smt'
lev   = 'L128'

path_data    = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'

#####################################################################
### Merge salinity data of multiple layers
levs   = np.arange(depthc.size)
nz     = levs.size
depthc = depthc[levs]

cc     = int(59799625)

#fpatho = '/scratch/u/u241317/calc/pp_calc_S.nc'
fpatho = '/work/mh0033/u241317/smt/results/pp_calc_S.nc'
fo = Dataset(fpatho, 'w', format='NETCDF4')
fo.createDimension('depthc', nz) # depth
fo.createDimension('cc', cc) # new dim cell center

ncv    = fo.createVariable('depthc','f4','depthc')
ncv[:] = depthc[:nz]
nc_S   = fo.createVariable('S','f4',('depthc','cc'))

# --- prepare output netcdf file
ts      = pyic.timing(ts, 'prepare nc')
varfile = 'T_S'
layers  = (  ['sp_001-016']*16 + ['sp_017-032']*16 + ['sp_033-048']*16
          + ['sp_049-064']*16 + ['sp_065-080']*16 + ['sp_081-096']*16 + ['sp_097-112']*16)

# --- load times and flist # only to get time data - it is not the selection of data
time0      = np.datetime64('2010-03-09T01:00:00')
ts         = pyic.timing(ts, 'load times and flist')
search_str = f'{run}_T_S_sp_001-016_*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')

itd     = np.argmin((timesd-time0).astype(float)**2)
fpath   = flist_tsd[itd]
pdtime0 = pd.to_datetime(time0) 
tstr    = pdtime0.strftime('%Y%m%d')+'T010000Z'

##### version 1 not interpolated
for kk, lev in enumerate(levs):
  ts = pyic.timing(ts, 'loop step')
  fname = f'{run}_{varfile}_{layers[lev]}_{tstr}.nc'
  fpath = f'{path_data}{fname}'
  var   = f'S{lev+1:03d}_sp'
  print(f'kk = {kk}/{nz}; fname = {fname}; var = {var}')
  fi    = Dataset(fpath, 'r')
  S     = fi.variables[var][itsd[itd],:]
  fi.close()
  nc_S[kk,:] = S[:cc]


fo.close()
