import numpy as np
from netCDF4 import Dataset
import sys, os
import matplotlib
if len(sys.argv)==2:
  matplotlib.use('Agg')
import pyicon as pyic

ts = pyic.timing([0], 'start')

rev           = f''
tgname        = f'smtwv_oce_2022'
# gname         = f'OceanOnly_WalvisRidge_submeso_srtm15_2018'
gname         = f'OceanOnly_WalvisRidge_submeso_GEBCO_2020'
path_tgrid    = f'/pool/data/ICON/oes/grids/OceanOnly/' 
fname_tgrid   = f'{gname}.nc'
path_ckdtree  = f'/work/mh0033/m300878/{tgname}/ckdtree/'
path_rgrid    = path_ckdtree + 'rectgrids/' 
path_sections = path_ckdtree + 'sections/' 

all_grids = [
#  'global_1.0',
  'global_0.3',
  'global_0.1',
  'global_0.02',
  #'global_0.3_nn5',
  #'global_0.3_nn20',
#  'global_0.3_nn500',
            ]

all_secs = [
  '00E_3000pts',
#   '00E_3800pts',
#   '10E_3800pts',
#   '10E_3000pts',
#   '10E_300pts',
#   '30W_200pts',
#   '170W_200pts',
#   '30W_300pts',
#   '170W_300pts',
            ]

#all_grids = []
# all_secs = []

load_cgrid = True,
load_vgrid = True
load_egrid = False

gnames = [gname]

if not os.path.exists(path_rgrid): 
  os.makedirs(path_rgrid)
if not os.path.exists(path_sections): 
  os.makedirs(path_sections)
fpath = f'{path_ckdtree}/../{tgname}_tgrid.nc'
if not os.path.exists(fpath):
  os.symlink(path_tgrid+fname_tgrid, fpath)

if True:
      sname = 'global_0.005'
      if sname in all_grids:
        pyic.ckdtree_hgrid(lon_reg=[-20.,20.], lat_reg=[-50.,-10.], res=0.005,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_rgrid,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_cgrid=load_cgrid,
                          load_egrid=load_egrid,
                          load_vgrid=load_vgrid,
                          )



if False:
    for gname in gnames:
      ts = pyic.timing(ts, gname)
      print(gname)

      # --- grids
      sname = 'global_1.0'
      if sname in all_grids:
        pyic.ckdtree_hgrid(lon_reg=[-180.,180.], lat_reg=[-90.,90.], res=1.0,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_rgrid,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_cgrid=load_cgrid,
                          load_egrid=load_egrid,
                          load_vgrid=load_vgrid,
                          )

      sname = 'global_0.3_nn500'
      if sname in all_grids:
        pyic.ckdtree_hgrid(lon_reg=[-180.,180.], lat_reg=[-90.,90.], res=0.3,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_rgrid,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_cgrid=load_cgrid,
                          load_egrid=load_egrid,
                          load_vgrid=load_vgrid,
                          n_nearest_neighbours=500,
                          )

      sname = 'global_0.3_nn20'
      if sname in all_grids:
        pyic.ckdtree_hgrid(lon_reg=[-180.,180.], lat_reg=[-90.,90.], res=0.3,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_rgrid,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_cgrid=load_cgrid,
                          load_egrid=load_egrid,
                          load_vgrid=load_vgrid,
                          n_nearest_neighbours=20,
                          )
    
      sname = 'global_0.3_nn5'
      if sname in all_grids:
        pyic.ckdtree_hgrid(lon_reg=[-180.,180.], lat_reg=[-90.,90.], res=0.3,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_rgrid,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_cgrid=load_cgrid,
                          load_egrid=load_egrid,
                          load_vgrid=load_vgrid,
                          n_nearest_neighbours=5,
                          )
    
      sname = 'global_0.3'
      if sname in all_grids:
        pyic.ckdtree_hgrid(lon_reg=[-180.,180.], lat_reg=[-90.,90.], res=0.3,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_rgrid,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_cgrid=load_cgrid,
                          load_egrid=load_egrid,
                          load_vgrid=load_vgrid,
                          )
    
      sname = 'global_0.1'
      if sname in all_grids:
        pyic.ckdtree_hgrid(lon_reg=[-180.,180.], lat_reg=[-90.,90.], res=0.1,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_rgrid,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_cgrid=load_cgrid,
                          load_egrid=load_egrid,
                          load_vgrid=load_vgrid,
                          )

      sname = 'global_0.02'
      if sname in all_grids:
        pyic.ckdtree_hgrid(lon_reg=[-180.,180.], lat_reg=[-90.,90.], res=0.02,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_rgrid,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_cgrid=load_cgrid,
                          load_egrid=load_egrid,
                          load_vgrid=load_vgrid,
                          )
    

if True:
  # --- sections
  sname = '30W_200pts'
  if sname in all_secs:
    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[-30,-80], p2=[-30,80], npoints=200,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname = sname,
                      gname = gname,
                      tgname = tgname,
                      )
    
  sname = '170W_200pts'
  if sname in all_secs:
    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[-170,-80], p2=[-170,80], npoints=200,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname = sname,
                      gname = gname,
                      tgname = tgname,
                      )

  sname = '30W_300pts'
  if sname in all_secs:
    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[-30,-80], p2=[-30,80], npoints=300,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname = sname,
                      gname = gname,
                      tgname = tgname,
                      )
    
  sname = '170W_300pts'
  if sname in all_secs:
    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[-170,-80], p2=[-170,80], npoints=300,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname = sname,
                      gname = gname,
                      tgname = tgname,
                      )

  sname = '10E_300pts'
  if sname in all_secs:
    print('start compute section')
    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[10,-60], p2=[10,-10], npoints=300,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname        = sname,
                      gname        = gname,
                      tgname       = tgname,
                      save_as_nc   = True,
                      load_egrid   = False,
                      load_vgrid   = False,
                      )
    
  sname = '10E_3000pts'
  if sname in all_secs:
    print('start compute section')
    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[10,-60], p2=[10,-10], npoints=3000,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname        = sname,
                      gname        = gname,
                      tgname       = tgname,
                      save_as_nc   = True,
                      load_egrid   = False,
                      load_vgrid   = False,
                      )

  sname = '10E_3800pts'
  if sname in all_secs:
    print('start compute section')
    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[10,-45], p2=[10,-20], npoints=3800,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname        = sname,
                      gname        = gname,
                      tgname       = tgname,
                      save_as_nc   = True,
                      load_egrid   = False,
                      load_vgrid   = False,
                      )
  sname = '00E_3000pts'
  if sname in all_secs:
    print('start compute section')
    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[0,-60], p2=[0,-10], npoints=3000,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname        = sname,
                      gname        = gname,
                      tgname       = tgname,
                      save_as_nc   = True,
                      load_egrid   = False,
                      load_vgrid   = False,
                      )

  sname = '00E_3800pts'
  if sname in all_secs:
    print('start compute section')
    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[0,-45], p2=[0,-20], npoints=3800,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname        = sname,
                      gname        = gname,
                      tgname       = tgname,
                      save_as_nc   = True,
                      load_egrid   = False,
                      load_vgrid   = False,
                      )

print('make_ckdtree.py: All done!')
ts = pyic.timing(ts, 'All done!')
