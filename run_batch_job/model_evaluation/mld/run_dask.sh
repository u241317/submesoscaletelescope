#! /bin/bash
#SBATCH --time=08:00:00
#SBATCH --output=log.o-%j.out
#SBATCH --error=log.o-%j.out
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH --partition=compute,compute2
#SBATCH --mem=100G
#SBATCH --account=mh0033
#SBATCH --qos=bench
set -x

# submit with
# sbatch --job-name=dpp0063 run_pp_dymwin_extract_points.sh

#module load python/2.7.12
module list

source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`
mpirun -np 8 python calc_mld.py --batch $SLURM_JOB_NAME
enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

