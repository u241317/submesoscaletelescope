# %%
import warnings
warnings.filterwarnings('ignore')  #suppress some warnings about future code changes

import sys
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
import scipy.signal as sg  #Package for signal analysis
import scipy.ndimage as si #Another package for signal analysis
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates
from scipy import stats    #Used for 2D binned statistics
from mpl_toolkits.axes_grid1 import make_axes_locatable #For plotting interior colobars
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import pyicon as pyic
import glob, os     
from os import path   

import scipy
# from scipy import fft as spfft
from scipy.fft import fft 
from pathlib import Path
import sys
from importlib import reload
# from eval_funcs import *
# import eval_funcs as eva

sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
import smt_modules.tools as tools

# %%
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
# number of section to be averaged in space
reload(eva)
n = 30 # exp1
#n = 60 # exp2
nr = int(n/2) #size needs to be adapted since resolution is much coarser # for r2b8 model
#choose horizonatl or vertical
savefig      = False
horizontal   = bool(True)
calc_spectra = bool(True)
exp          = int(1)
secs         = 6 # number of sectionsincluded
hemisphere   = 'SA'

if horizontal  == True:
    if hemisphere == 'NA':
        root    = '/work/mh0033/m300878/model_evaluation/smt_wave/sst/tides/'
        lon_reg = [-80, -40]
        lat_reg = [25, 40]
    elif hemisphere == 'SA': 
        root    = '/work/mh0033/m300878/model_evaluation/smt_wave/sst/tides_sa/'
        lon_reg = [-40, 30]
        lat_reg = [-55, -15]
    else: raise ValueError('hemisphere not defined')
    parent_imag  = f'{root}images/horizontal/'
    parent_dat   = f'{root}data/horizontal/'
    regions      = 'A', 'B', 'C', 'D', 'E', 'F'
    if path.isdir(parent_imag):
        print('parent_imag exits')
    else:
        print('create folder parent_imag')
        Path(f"{parent_imag}").mkdir(parents=True, exist_ok=True)
        for i in enumerate(regions):
            os.mkdir(parent_imag+regions[i[0]])
    if path.isdir(parent_dat):
        print('parent_dat exits')
    else:
        print('create folder parent_dat')
        Path(f"{parent_dat}").mkdir(parents=True, exist_ok=True)
        for i in enumerate(regions):
            os.mkdir(parent_dat+regions[i[0]])
else: 
    parent_imag = '/work/mh0033/m300878/model_evaluation/sst/exp1/images/vertical/'
    parent_dat  = '/work/mh0033/m300878/model_evaluation/sst/exp1/data/vertical/'
    regions     = 'a', 'b', 'c', 'd', 'e', 'f'



# %%
reload(eva)
if calc_spectra == True:
    print('Load data')
    # smt        = eva.load_smt_wave('to', 8, t1=1300, t2=1301, remove_bnds=True)
    r2b8       = eva.load_r2b8_sst_mep()
    ds         = eva.load_satellite_sst()
    if hemisphere == 'NA':
        r2b8       = r2b8.isel(depth=0).sel(time='2010-03-21T00:00:00', method='nearest')
        ds         = ds.sel(time='2010-03-21T00:00:00', method='nearest')
        smt        = eva.load_smt_wave_quick('to', 8, t1=1152, t2=1153, remove_bnds=True)
    elif hemisphere == 'SA':
        r2b8       = r2b8.isel(depth=0).sel(time='2010-03-12T12:00:00', method='nearest')
        smt        = eva.load_smt_wave_quick('to', 8, t1=947, t2=948, remove_bnds=True)
        ds         = ds.sel(time='2010-03-12T00:00:00', method='nearest')
    else: raise ValueError('hemisphere not defined')

    smt        = smt.isel(time=0, depth=0).to
    r2b8_inter = eva.interpolate_to_rect_grid_r2b8(r2b8, lon_reg, lat_reg)

    reload(eva)
    print('visualize regions')
    if horizontal == True:
        print('horizontal sections')
        if hemisphere == 'NA':
            eva.plot_regions_horizontal_wave_NA(smt, secs, n, regions, parent_imag, savefig)
            eva.plot_regions_horizontal_r2b8(r2b8, secs, nr, regions, parent_imag, savefig)
            eva.plot_regions_horizontal_satellite(ds, secs, n, regions, parent_imag, savefig)
        else:
            eva.plot_regions_horizontal_wave_SA(smt, secs, n, regions, parent_imag, savefig, lon_reg, lat_reg)
            eva.plot_regions_horizontal_satellite_SA(ds, secs, n, regions, parent_imag, savefig)
            eva.plot_regions_horizontal_r2b8_wave_SA(r2b8, secs, nr, regions, parent_imag, savefig, lon_reg, lat_reg)
    else:
        print('vertical sections')
        eva.plot_regions_vertical(smt, secs, n, regions, parent_imag, savefig)
        eva.plot_regions_vertical_satellite(ds, secs, n, regions, parent_imag, savefig)
        eva.plot_regions_vertical_r2b8(r2b8, secs, nr, regions, parent_imag, savefig)

# %%
reload(eva)
for k in range(secs):
    # create folder
    reg_name  = f'{regions[k]}'
    path_dat  = f'{parent_dat}{reg_name}/'
    path_imag = f'{parent_imag}{reg_name}/'

    print(f'Section {reg_name}')

    #------------calc spectra of Satellite, SMT, R2B8-------------#
    if calc_spectra == True:
        print('calc ffts and temporal averages')
        if hemisphere == 'NA':
            eva.calc_satellite_spectra(ds, n, k, horizontal, path_dat)
            eva.calc_smt_spectra(smt, n, k, horizontal, path_dat)
            # eva.calc_r2b8_spectra(r2b8_inter, nr, k, horizontal, path_dat)
        elif hemisphere == 'SA':
            eva.calc_satellite_spectra_SA(ds, n, k, horizontal, path_dat)
            eva.calc_smt_spectra_SA(smt, n, k, horizontal, path_dat)
            eva.calc_r2b8_spectra_SA(r2b8_inter, nr, k, horizontal, path_dat)
        else: raise ValueError('hemisphere not defined')



# %%
######################################################################
# Visualization     
reload(eva)

for K in range(secs):      
    print(f'Section {K}')
    reg_name  = f'{regions[K]}'
    path_dat  = f'{parent_dat}{reg_name}/'
    path_imag = f'{parent_imag}{reg_name}/'
    
    # print(f'plot temporal averaged in reg {reg_name}')
    # # plot each section
    search_str = f'data_sat*.npy' 
    flist      = np.array(glob.glob(path_dat+search_str))
    flist.sort()
    sat_sections = flist 
    search_str = f'data_smt*.npy' 
    flist      = np.array(glob.glob(path_dat+search_str))
    flist.sort()
    smt_sections = flist
    search_str = f'data_r2b8*.npy' 
    flist      = np.array(glob.glob(path_dat+search_str))
    flist.sort()
    r2b8_sections = flist 

    #kk = 2 
    #plot single realisations of spektra
    X = np.array([0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29]) #easy fix taking twice the r2b8 grid
    for ii in np.arange(n)[::15]:
        section = smt_sections[ii]
        [A_smt, f_smt, dm_smt, A_smt_mean, std_smt] = np.load(f'{section}',allow_pickle=True)
        section = sat_sections[ii]
        [A_sat, f_sat, dm_sat, A_sat_mean, std_sat] = np.load(f'{section}',allow_pickle=True)   
        section = r2b8_sections[X[ii]]
        [A_r2b8, f_r2b8, dm_r2b8, A_r2b8_mean, std_r2b8] = np.load(f'{section}',allow_pickle=True)  
        #plot
        path_save_name = f'{path_imag}sst_sec_{ii}' 
        figname = f'{reg_name}_{ii}' 
        #there are no means here...
        # if horizontal == True: eva.plot_spatial_spectra_sst_horizontal(A_smt_mean, std_smt, f_smt, A_sat_mean, std_sat, f_sat, figname=figname, path_save_name=path_save_name, horizontal=horizontal) # if activate region plot doesent work figure in figure
        #else: plot_spatial_spectra_sst(A_smt, A_smt_mean, std_smt, f_smt, A_sat, A_sat_mean, std_sat, f_sat, A_r2b8, A_r2b8_mean, std_r2b8, f_r2b8, kk, figname, path_save_name, horizontal)
        eva.plot_spatial_spectra_sst_section(A_smt_mean, f_smt, A_sat_mean, f_sat, A_r2b8_mean, f_r2b8, figname, path_save_name)
# %%
reload(eva)
#create arrays for overall means
AAA = np.zeros((secs*n,  A_smt_mean.size)) 
BBB = np.zeros((secs*n,  A_sat_mean.size)) 
CCC = np.zeros((secs*nr, A_r2b8_mean.size)) 

#create figure for 6 section spectra
fig, axs = plt.subplots(3,2, figsize=(20,25))

for K in range(secs):
    reg_name = f'{regions[K]}'
    path_dat  = f'{parent_dat}{reg_name}/'
    path_imag = f'{parent_imag}{reg_name}/'
    
    print('plot temporal & spatial averaged')
    # plot each section
    search_str = f'data_sat*.npy' 
    flist      = np.array(glob.glob(path_dat+search_str))
    flist.sort()
    sat_sections = flist 
    search_str = f'data_smt*.npy' 
    flist      = np.array(glob.glob(path_dat+search_str))
    flist.sort()
    smt_sections = flist
    search_str = f'data_r2b8*.npy' 
    flist      = np.array(glob.glob(path_dat+search_str))
    flist.sort()
    r2b8_sections = flist 

    # cheap way to get array sizes
    section = smt_sections[0]
    [A_smt, f_smt, dm_smt, A_smt_mean, std_smt] = np.load(f'{section}',allow_pickle=True)
    section = sat_sections[0]
    [A_sat, f_sat, dm_sat, A_sat_mean, std_sat] = np.load(f'{section}',allow_pickle=True)  
    section = r2b8_sections[0]
    [A_r2b8, f_r2b8, dm_r2b8, A_r2b8_mean, std_r2b8] = np.load(f'{section}',allow_pickle=True) 


    AA = np.zeros((n,A_smt_mean.size))
    BB = np.zeros((n,A_sat_mean.size))
    CC = np.zeros((nr,A_r2b8_mean.size))

    # calculate section means!
    for ii in np.arange(n-1):
        section = smt_sections[ii]
        [A_smt, f_smt, dm_smt, A_smt_mean, std_smt] = np.load(f'{section}',allow_pickle=True)
        AA[ii,:] = A_smt_mean
        #------------------FLAGS-------------#
        #print(f'load data K = {K}, n= {n}, h{horizontal}')
        # if ((exp == 1) & (horizontal == True) & (K == 2)) & (ii==0): AA[ii,:] = np.nan; print(f'flag in region {regions[K]} at sec n={ii}'); flag = horizontal, K, n #flag for horizontal sst
        # if ((exp == 1) & (horizontal == False) & (K == 1)) & (ii==0): AA[ii,:] = np.nan; print(f'flag in region {regions[K]} at sec n={ii}'); flag = horizontal, K, n #flag for vertical sst
        # if ((exp == 2) & (horizontal == False) & (K == 1)) & (ii==22 or ii==21 or ii == 20 or ii==19 or ii== 18): AA[ii,:] = np.nan; print(f'flag in region {regions[K]} at sec n={ii}'); flag = horizontal, K, n #flag for vertical sst

        #satelite
        section = sat_sections[ii]
        [A_sat, f_sat, dm_sat, A_sat_mean, std_sat] = np.load(f'{section}',allow_pickle=True)
        BB[ii,:] = A_sat_mean
    
    for ii in np.arange(nr):
        #r2b8
        section = r2b8_sections[ii]
        [A_r2b8, f_r2b8, dm_r2b8, A_r2b8_mean, std_r2b8] = np.load(f'{section}',allow_pickle=True)
        CC[ii,:] = A_r2b8_mean

    AM = np.nanmean(AA, axis=0)
    BM = np.mean(BB, axis=0)
    CM = np.mean(CC, axis=0)

    # AAA[K,:] = AM
    # BBB[K,:] = BM
    # CCC[K,:] = CM
    start = K*n; end = K*n + n;
    AAA[start:end,:] = AA
    BBB[start:end,:] = BB
    start = K*nr; end = K*nr + nr;
    CCC[start:end,:] = CC
    
    AM_std = np.nanstd(AA, axis=0, ddof=1)
    BM_std = np.std(BB, axis=0, ddof=1)
    CM_std = np.std(CC, axis=0, ddof=1)

    #plot time and space averages
    #plot_spatial_spectra6(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, reg_name, K, axs, horizontal, n)
    if ((exp==1) & (horizontal ==  False) & (K == 2)): n_minus_flag = n-1; 
    else: n_minus_flag = n
    eva.plot_spatial_spectra_ci(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name, K, axs, horizontal, n_minus_flag, nr)

path_save_name = f'{parent_imag}sst_sec_ave_reg' 
# if savefig==True: plt.savefig(f'{path_save_name}.pdf', bbox_inches='tight', dpi=200)
if savefig==True: plt.savefig(f'{path_save_name}.png', bbox_inches='tight', dpi=200)

# %%
reload(eva)
# average over all vertiacl regions
AAM = np.nanmean(AAA, axis=0) #nanmean due to flags
BBM = np.mean(BBB, axis=0)
CCM = np.mean(CCC, axis=0)
AAM_std = np.nanstd(AAA, axis=0, ddof=1) #nan due to flags
BBM_std = np.std(BBB, axis=0, ddof=1)
CCM_std = np.std(CCC, axis=0, ddof=1)

path_save_name = f'{parent_imag}sst_sec_ave_all'
if horizontal == True: reg_name='zonal'
else: reg_name='meridional'

eva.plot_spatial_spectra_sst_all(AAA, AAM, AAM_std, f_smt, BBB, BBM, BBM_std, f_sat, CCC, CCM, CCM_std, f_r2b8, reg_name, path_save_name, secs, horizontal, n, nr, K, savefig)

# path_save_name = f'{parent_imag}sst_sec_ave_lat_dep'
# eva.plot_spatial_spectra_all_lat_dep(AAA, AAM, AAM_std, f_smt, BBB, BBM, BBM_std, f_sat, reg_name, path_save_name, secs, horizontal, regions)





# %%
