import warnings
warnings.filterwarnings('ignore')  #suppress some warnings about future code changes

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
import scipy.signal as sg  #Package for signal analysis
import scipy.ndimage as si #Another package for signal analysis
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates
from scipy import stats    #Used for 2D binned statistics
from mpl_toolkits.axes_grid1 import make_axes_locatable #For plotting interior colobars
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import pyicon as pyic
import glob, os     
from os import path   

import scipy
from scipy import fft as spfft
from scipy.fft import fft 
from pathlib import Path
import sys
import eval_funcs as eva

##################################################################
# small functions
# convert to meter
r = 6371000
const_lat = 32 # latitude of section
latitude = np.linspace(0,90,90)
delta = 2*np.pi*r/360*np.cos(latitude/90* np.pi/2)
print('distance of one degree in meter at equator', delta[0])
print('distance of one degree in meter at section', delta[const_lat])

def nan_helper(y):
    """Helper to handle indices and logical indices of NaNs.

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        >>> # linear interpolation of NaNs
        >>> nans, x= nan_helper(y)
        >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
    """

    return np.isnan(y), lambda z: z.nonzero()[0]

def plot_spatial_spectra_horizontal(A_smt_mean, std_smt, f_smt, A_sat_mean, std_sat, f_sat, k, figname, path_save_name):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))
    #l=2.5

    # smt
    kernel_size = k
    kernel = np.ones(kernel_size) / kernel_size
    #smean_m_std = A_smt_mean - std_smt #np.abs(A_smt_mean - std_smt)
    smean_m_std = np.abs(A_smt_mean - std_smt)
    smean_m_std = np.convolve(smean_m_std, kernel, mode='same')
    #smean_p_std = A_smt_mean + std_smt#np.abs(A_smt_mean + std_smt)
    smean_p_std = np.abs(A_smt_mean + std_smt)
    smean_p_std = np.convolve(smean_p_std, kernel, mode='same')

    plt.loglog(f_smt, smean_p_std, color='royalblue', linestyle='--',alpha=0.5)
    plt.loglog(f_smt, A_smt_mean, label='smt_ssh_mean', color='royalblue', linewidth=2)
    plt.fill_between(f_smt, A_smt_mean, smean_p_std, color='royalblue', alpha=0.25)
    plt.loglog(f_smt, smean_m_std, linestyle='--',  color='royalblue', alpha=0.5)
    plt.fill_between(f_smt, A_smt_mean, smean_m_std, color='royalblue', alpha=0.25)

    # satellite
    kernel_size = k
    kernel = np.ones(kernel_size) / kernel_size
    mean_m_std = np.abs(A_sat_mean - std_sat)
    mean_m_std = np.convolve(mean_m_std, kernel, mode='same')
    mean_p_std = np.abs(A_sat_mean + std_sat)
    mean_p_std = np.convolve(mean_p_std, kernel, mode='same')

    plt.loglog(f_sat, mean_p_std, color='tomato', linestyle='--', alpha=0.5)
    plt.loglog(f_sat, A_sat_mean, label='sat_adt_mean', color='tomato', linewidth=2)
    plt.fill_between(f_sat, A_sat_mean, mean_p_std, color='wheat', alpha=0.5)
    plt.loglog(f_sat, mean_m_std, linestyle='--',  color='tomato', alpha=0.5)
    plt.fill_between(f_sat, A_sat_mean, mean_m_std, color='wheat', alpha=0.5)

    #####################
    fx = np.linspace(1e-4,5e-4,100)
    pp = 10000
    y2 = pp* 3e-9*np.power(fx, (-2))
    y3 = pp* 1e-13*np.power(fx, (-3))
    y5 = pp* 1e-7*np.power(fx, (-5/3))

    plt.loglog(fx,y3, label='slope -3')
    plt.loglog(fx,y5, label='slope -5/3')

    # slope satellite
    xdata = f_sat[8:]
    ydata = A_sat_mean[8:]
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="limegreen")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="limegreen", label=f'slope {index:3.3}') 


    # slope smt
    if horizontal == True: 
        xdata = f_smt[50::700]
        ydata = A_smt_mean[50::700]
    else:
        xdata = f_smt[32::600]
        ydata = A_smt_mean[32::600]

    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="gold")  
    plt.plot(fx, powerlaw(fx, 5*pp*amp, index), linestyle='dashed', color="gold", label=f'slope {index:3.3}') 

    # slope smt
    if horizontal == True:
        xdata = f_smt[8:48]
        ydata = A_smt_mean[8:48]
    else:
        xdata = f_smt[6:30]
        ydata = A_smt_mean[6:30]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index), linewidth=4, linestyle='--', color="aqua")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linewidth=2, linestyle='--', color="aqua", label=f'slope {index:3.3}') 

    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(8e-4, 5e4)
    #ax.autoscale(enable=True, tight=True)
    #ax.set_xlim(1e-6, 1e10)
    fig.tight_layout()

    plt.legend(fontsize=15, loc='lower left')
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    plt.title(f'Averaged Spectral Estimates of SSH {figname}',fontsize=15)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight')


def plot_spatial_spectra_all_vertical(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, k, reg_name, path_save_name):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))
    #l=2.5
    
    for ii in np.arange(S):
        plt.scatter(f_smt, AA[ii,:], color='lightgrey')
    
    for ii in np.arange(S):
        plt.scatter(f_sat, BB[ii,:], color='wheat')

    #plt.loglog(f_smt, AM+AM_std, color='royalblue', linestyle='--',alpha=0.5)
    plt.loglog(f_smt, AM, label='smt_ssh_mean', color='royalblue', linewidth=2)
    #plt.fill_between(f_smt, AM, AM+AM_std, color='royalblue', alpha=0.25)
    #plt.loglog(f_smt, AM-AM_std, linestyle='--',  color='royalblue', alpha=0.5)
    #plt.fill_between(f_smt, AM, AM-AM_std, color='royalblue', alpha=0.25)

    #plt.loglog(f_sat, BM+BM_std, color='tomato', linestyle='--', alpha=0.5)
    plt.loglog(f_sat, BM, label='sat_adt_mean', color='tomato', linewidth=2)
    #plt.fill_between(f_sat, BM, BM+BM_std, color='wheat', alpha=0.5)
    #plt.loglog(f_sat, BM-BM_std, linestyle='--',  color='tomato', alpha=0.5)
    #plt.fill_between(f_sat, BM, BM-BM_std, color='wheat', alpha=0.5)

    #####################
    fx = np.linspace(1e-4,5e-4,100)
    pp = 10000
    y2 = pp* 3e-9*np.power(fx, (-2))
    y3 = pp* 6e-13*np.power(fx, (-3))
    y5 = pp* 1e-6*np.power(fx, (-5/3))

    plt.loglog(fx,y3, label='slope -3',)
    plt.loglog(fx,y5, label='slope -5/3 = 1.67')

    # slope satellite
    xdata = f_sat[8:]
    ydata =  BM[8:]
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="red")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="red", label=f'slope {index:3.3}') 


    # slope smt
    if horizontal == True: 
        xdata = f_smt[50::700]
        ydata = AM[50::700]
    else:
        xdata = f_smt[32::550]
        ydata = AM[32::550]

    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="gold")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="gold", label=f'slope {index:3.3}') 

    # slope smt
    if horizontal == True:
        xdata = f_smt[8:48]
        ydata = AM[8:48]
    else:
        xdata = f_smt[6:30]
        ydata = AM[6:30]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index), linewidth=4, linestyle='--', color="aqua")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linewidth=2, linestyle='--', color="aqua", label=f'slope {index:3.3}') 

    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-2, 1e5)
    #ax.autoscale(enable=True, tight=True)
    #ax.set_xlim(1e-6, 1e10)
    fig.tight_layout()

    plt.legend(fontsize=15, loc='lower left')
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    plt.title(f'Averaged Spectral Estimates of SSH {reg_name}',fontsize=15)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight')

    
def plot_spatial_spectra6(AM, AM_std, f_smt, BM, BM_std, f_sat, k, reg_name,  K, axs):
    powerlaw = lambda x, amp, index: amp * (x**index)
    
    if K == 0: idx1, idx2 = 0,0
    if K == 1: idx1, idx2 = 0,1
    if K == 2: idx1, idx2 = 1,0
    if K == 3: idx1, idx2 = 1,1
    if K == 4: idx1, idx2 = 2,0
    if K == 5: idx1, idx2 = 2,1
 
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_smt, AA[ii,:], color='lightgrey')
    
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_sat, BB[ii,:], color='wheat')

    axs[idx1,idx2].loglog(f_smt, AM+AM_std, color='royalblue', linestyle='--',alpha=0.5)
    axs[idx1,idx2].loglog(f_smt, AM, label='smt_ssh_mean', color='royalblue', linewidth=2)
    axs[idx1,idx2].fill_between(f_smt, AM, AM+AM_std, color='royalblue', alpha=0.25)
    axs[idx1,idx2].loglog(f_smt, AM-AM_std, linestyle='--',  color='royalblue', alpha=0.5)
    axs[idx1,idx2].fill_between(f_smt, AM, AM-AM_std, color='royalblue', alpha=0.25)

    axs[idx1,idx2].loglog(f_sat, BM+BM_std, color='tomato', linestyle='--', alpha=0.5)
    axs[idx1,idx2].loglog(f_sat, BM, label='sat_adt_mean', color='tomato', linewidth=2)
    axs[idx1,idx2].fill_between(f_sat, BM, BM+BM_std, color='wheat', alpha=0.5)
    axs[idx1,idx2].loglog(f_sat, BM-BM_std, linestyle='--',  color='tomato', alpha=0.5)
    axs[idx1,idx2].fill_between(f_sat, BM, BM-BM_std, color='wheat', alpha=0.5)

    #####################
    fx = np.linspace(1e-4,5e-4,100)
    pp = 10000
    y2 = pp* 3e-9*np.power(fx, (-2))
    y3 = pp* 6e-13*np.power(fx, (-3))
    y5 = pp* 1e-6*np.power(fx, (-5/3))

    axs[idx1,idx2].loglog(fx,y3, label='slope -3',)
    axs[idx1,idx2].loglog(fx,y5, label='slope -5/3 = 1.67')

    # slope satellite
    xdata = f_sat[8:]
    ydata =  BM[8:]
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    axs[idx1,idx2].plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="red")  
    axs[idx1,idx2].plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="red", label=f'slope {index:3.3}') 


    # slope smt
    if horizontal == True: 
        xdata = f_smt[50::700]
        ydata = AM[50::700]
    else:
        xdata = f_smt[32::550]
        ydata = AM[32::550]

    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    axs[idx1,idx2].plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="gold")  
    axs[idx1,idx2].plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="gold", label=f'slope {index:3.3}') 

    # slope smt
    if horizontal == True:
        xdata = f_smt[8:48]
        ydata = AM[8:48]
    else:
        xdata = f_smt[6:30]
        ydata = AM[6:30]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    axs[idx1,idx2].plot(xdata, powerlaw(xdata, amp, index), linewidth=4, linestyle='--', color="aqua")  
    axs[idx1,idx2].plot(fx, powerlaw(fx, pp*amp, index), linewidth=2, linestyle='--', color="aqua", label=f'slope {index:3.3}') 


    axs[idx1,idx2].autoscale(enable=True, tight=True)
    axs[idx1,idx2].set_ylim(1e-2, 1e5)

    axs[idx1,idx2].legend(fontsize=15, loc='lower left')
    axs[idx1,idx2].tick_params(labelsize=15)
    if K >= 4: axs[idx1,idx2].set_xlabel('Wavenumber (1/m)', fontsize=15)
    if is_odd(K)== False: axs[idx1,idx2].set_ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    axs[idx1,idx2].set_title(f'{reg_name}',fontsize=15)

def is_odd(num):
    return num & 0x1

def get_lon_lat_horizontal(k):
    #n = 10
    #lon1 = -49, - 25
    lon1 = -70, - 48
    #if k > 2: lon1 = -75, -51; k=k-3
    lati = 25.8, 26.2
    lati = lati + np.ones(2)*3*k
    Lat = np.zeros((n, 2))
    Lon = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lati
        Lon[ii,:] = lon1
        lati += np.ones(2)*0.25 #0.5
    return(Lat, Lon)

def get_lon_lat_vertical(k):
    #n = 10
    lat1 = 25, 43
    loni = -68.2, -67.8
    loni = loni + np.ones(2)*3*k
    Lat = np.zeros((n, 2))
    Lon = np.zeros((n, 2))
    for ii in np.arange(n):
        Lon[ii,:] = loni
        Lat[ii,:] = lat1
        loni += np.ones(2)*0.25 #0.5
    return(Lat, Lon)


def plot_regions_horizontal(smt):
    fpath_tgrid = '/work/mh0033/from_Mistral/mh0033/m300602/icon/grids/smt/smt_tgrid.nc'
    gg = xr.open_dataset(fpath_tgrid)
    res = np.sqrt(gg.cell_area_p)
    fpath_ckdtree = '/work/mh0033/from_Mistral/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'

    ddnpz = np.load(fpath_ckdtree)
    lon_010deg = ddnpz['lon']
    lat_010deg = ddnpz['lat']
    res_010deg = pyic.apply_ckdtree(res, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_010deg.size, lon_010deg.size)
    res_010deg[res_010deg==0.] = np.ma.masked

    # plot region
    lon_reg = [-90, -20]
    lat_reg = [20, 60]
    #regions = 'A','B','C','D','E','F'
    #n = 10
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    lon, lat, data = pyic.interp_to_rectgrid(smt.isel(time=50).h_sp, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    pyic.shade(lon, lat, data, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False)
    levels = 500 + np.arange(11)*100
    CS = ax.contour(lon_010deg, lat_010deg, res_010deg,  levels=levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)

    for k in range(S):
        #print(k)
        Lat, Lon = get_lon_lat_horizontal(k)
        #print(Lat)
        for ii in np.arange(n):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=1)

        loc = Lon[ii,0]+(Lon[ii,1]-Lon[ii,0])/2, Lat[5,0]
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=20,         #arrowprops=dict(arrowstyle="->", facecolor='black', linewidth=2, mutation_scale=30),
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)

    ax.set_title(f'Visualization sections', fontsize=15)
    #ax.set_xlabel('lon')
    #ax.set_ylabel('lat')
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    plt.savefig(f'{parent_imag}vis_reg', bbox_inches='tight')      

def plot_regions_vertical(smt):
    fpath_tgrid = '/work/mh0033/from_Mistral/mh0033/m300602/icon/grids/smt/smt_tgrid.nc'
    gg = xr.open_dataset(fpath_tgrid)
    res = np.sqrt(gg.cell_area_p)
    fpath_ckdtree = '/work/mh0033/from_Mistral/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'

    ddnpz = np.load(fpath_ckdtree)
    lon_010deg = ddnpz['lon']
    lat_010deg = ddnpz['lat']
    res_010deg = pyic.apply_ckdtree(res, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_010deg.size, lon_010deg.size)
    res_010deg[res_010deg==0.] = np.ma.masked

    # plot region
    lon_reg = [-90, -20]
    lat_reg = [20, 60]
    #regions = 'A','B','C','D','E','F'
    #n = 10
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    lon, lat, data = pyic.interp_to_rectgrid(smt.isel(time=50).h_sp, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    pyic.shade(lon, lat, data, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False)
    levels = 500 + np.arange(11)*100
    CS = ax.contour(lon_010deg, lat_010deg, res_010deg,  levels=levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)

    for k in range(S):
        Lat, Lon = get_lon_lat_vertical(k)
        for ii in np.arange(n):
            ax.plot((Lon[ii,0],Lon[ii,0]), (Lat[ii,0], Lat[ii,1]), transform=ccrs_proj, color='darkviolet', linewidth=1)

        loc = Lon[5,0], (Lat[0,0] + (Lat[0,1]-Lat[0,0])/2)
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=20,        
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)

    ax.set_title(f'Visualization sections', fontsize=15)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    plt.savefig(f'{parent_imag}vis_reg2', bbox_inches='tight')      

def load_satellite():
    #satellite
    #TODO change to ICDC aviso ssh folder
    search_str = f'dt_global_twosat_phy_l4_*.nc' 
    month = '01'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    path_data = f'/work/mh0033/from_Mistral/mh0033/u241317/satellite/ssh/ssh_sat_temp/{month}/'
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month = '02'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    path_data = f'/work/mh0033/from_Mistral/mh0033/u241317/satellite/ssh/ssh_sat_temp/{month}/'
    flist2      = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month = '03'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    path_data = f'/work/mh0033/from_Mistral/mh0033/u241317/satellite/ssh/ssh_sat_temp/{month}/'
    flist3      = np.array(glob.glob(path_data+search_str))
    flist3.sort()

    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(ds)

def load_smt():
    #smt model data
    run      = 'ngSMT_tke'
    gname = 'smt'

    search_str = f'_h_sp_*.nc' 
    month = '01'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month = '02'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist2      = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month = '03'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist3      = np.array(glob.glob(path_data+search_str))
    flist3.sort()

    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    time0 = np.datetime64('2010-01-09T01:00:00')
    dti = pd.date_range(time0, periods=984, freq="2h")
    smt = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    smt = smt.assign_coords(time=dti)
    return(smt)

############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
# number of section to be smoothed in space
n = 10
#choose horizonatl or vertical
horizontal = bool(False)
calc_spectra = bool(False)

if horizontal == True: parent_imag = '/work/mh0033/m300878/model_evaluation/ssh/images/horizontal/'
else: parent_imag = '/work/mh0033/m300878/model_evaluation/ssh/images/vertical/'

if calc_spectra == True:
    print('Load data')
    smt = load_smt()
    ds = load_satellite()

parent_dat  = '/work/mh0033/m300878/model_evaluation/ssh/data/'

S = 6 # number of sectionsincluded

print('visualize regions')
if horizontal == True:
    print('horizontal sections')
    regions = 'A','B','C','D','E','F'
    if calc_spectra == True: plot_regions_horizontal(smt)
else:
    print('vertical sections')
    regions = 'a','b','c','d','e','f'
    if calc_spectra == True: plot_regions_vertical(smt)


for k in range(S):
    if horizontal == True:
        Lat, Lon = get_lon_lat_horizontal(k)
    else:
        Lat, Lon = get_lon_lat_vertical(k)
    
    # create folder
    reg_name = f'{regions[k]}'
    path_dat  = f'{parent_dat}{reg_name}/'
    path_imag = f'{parent_imag}{reg_name}/'

    if path.isdir(path_dat):
        print('path_dat exits')
    else:
        print('create folder path dat')
        os.mkdir(path_dat)

    if path.isdir(path_imag):
        print('path_imag exits')
    else:
        print('create folder path imag')
        os.mkdir(path_imag)

    # Do calculation? if False only plots are regenerated
    if calc_spectra == True:
        print('calc ffts and temporal averages')
        print('for satellite')
        #n = 10

        #Satellite
        for kk in np.arange(n):
            print(kk)
            lat = Lat[kk,:]
            lon = Lon[kk,:]
            ds_section = ds.where((ds.latitude > lat[0]) & (ds.latitude < lat[1]) & (ds.longitude > lon[0]) & (ds.longitude < lon[1]), drop=True)
            ds_lat = int(ds_section.latitude[0].data)
            if horizontal == True: 
                ds_section = ds_section.isel(latitude=0)
                dm_sat = (ds_section.longitude[1] - ds_section.longitude[0]) * delta[ds_lat]
                ds_size = int(ds_section.longitude.size /2) +1
            else: 
                ds_section = ds_section.isel(longitude=0)
                dm_sat = (ds_section.latitude[1] - ds_section.latitude[0]) * delta[ds_lat]
                ds_size = int(ds_section.latitude.size /2) +1
            
            dm_sat = dm_sat.data
            ###############################################################
            # satellite
            
            iii = 0
            A_sat = np.zeros((90,ds_size))
            for ii in np.arange(90)[::1]:
                #t_sat = ds_section.time[ii].data
                y = ds_section.isel(time=ii).adt
                cv = y.data.compute() 
                nans, x= nan_helper(cv) 
                cv[nans]= np.interp(x(nans), x(~nans), cv[~nans])
                f, S = sg.periodogram(cv-np.nanmean(cv), fs=1/dm_sat)
                S = S.squeeze()
                A_sat[iii,:] = S
                iii += 1
            f_sat = f
            A_sat_mean = np.mean(A_sat, axis=0)
            std_sat = np.std(A_sat, axis=0)
            np.save(f'{path_dat}data_sat_{Lat[kk,0]}_{Lon[kk,0]}', [A_sat, f_sat, dm_sat, A_sat_mean, std_sat])
        

        print('and for smt model')
        # SMT Model
        for kk in np.arange(n):
            print(kk)
            lat = Lat[kk,:]
            lon = Lon[kk,:]
            ##############################################################
            ######## calculate nearest neighbour of section
            if horizontal == True: npoints = 2800
            else: npoints = 2000
            
            sname         = 'A'
            tgname        = 'SMT'
            gname         = 'OceanOnly_SubmesoNA_2500m_srtm30'
            path_tgrid    = f'/pool/data/ICON/oes/grids/OceanOnly/'
            fname_tgrid   = f'{gname}.nc'
            path_scratch  = f'/scratch/m/m300878/slices/'
            path_ckdtree  = path_scratch # where grid is stored

            if False: #Path(sec_path ).is_file():
                print ("section File exist")
                data = xr.open_dataset(sec_path)
                ickdtree = data.ickdtree_c.data
                lon_sec = data.lon_sec.data
                lat_sec = data.lat_sec.data
            else:
                print ("section file does not exist")
                if horizontal == True:
                    latii = Lat[kk,:] + 0.2
                    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lon[1],latii[0]], p2=[lon[0],latii[0]], npoints=npoints,
                              fname_tgrid  = fname_tgrid,
                              path_tgrid   = path_tgrid,
                              path_ckdtree = path_ckdtree,
                              sname = sname,
                              gname = gname,
                              tgname = tgname,
                              load_egrid=False,
                              load_vgrid=False,
                              )
                else:
                    lonii = Lon[kk,:] + 0.2
                    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lonii[0],lat[1]], p2=[lonii[0],lat[0]], npoints=npoints,
                              fname_tgrid  = fname_tgrid,
                              path_tgrid   = path_tgrid,
                              path_ckdtree = path_ckdtree,
                              sname = sname,
                              gname = gname,
                              tgname = tgname,
                              load_egrid=False,
                              load_vgrid=False,
                              )
                # check grid
                sgrid = xr.open_dataset(path_tgrid + fname_tgrid)
                grid  = sgrid.cell_area_p.compute()
                Clon  =  grid.clon * 180/np.pi
                grid  = grid.assign_coords(clon=Clon)
                Clat  =  grid.clat * 180/np.pi
                grid  = grid.assign_coords(clat=Clat)
                grid  = np.sqrt(grid.isel(cell=ickdtree))
                grid  = grid.assign_coords({"lon_sec": ("cell", lon_sec)})
                grid_smt = grid.assign_coords({"lat_sec": ("cell", lat_sec)})
                # section distance
                if horizontal == True:
                    lat_min = int(grid_smt.lat_sec[0])
                    d_smt_sec = (grid_smt.lon_sec[0] - grid_smt.lon_sec[1]).data *delta[lat_min]
                else:
                    lat_min = int(grid_smt.lat_sec[0])
                    d_smt_sec = (grid_smt.lat_sec[0] - grid_smt.lat_sec[1]).data *delta[lat_min]
                d_smt_hi_res = grid_smt.min().data
                print('max',grid_smt.max().data, 'min', grid_smt.min().data, 'distance section', d_smt_sec)
            # select data
            smt_sec = smt.isel(ncells=ickdtree)
            if horizontal == True:
                smt_sec = smt_sec.assign_coords({"clon": ("ncells", lon_sec)})
            else:
                smt_sec = smt_sec.assign_coords({"clat": ("ncells", lat_sec)})
            
            smt_sec = smt_sec.h_sp
            dm_smt = d_smt_sec

            # smt
            N = 984
            l = 11
            ll = int(N/l) +1
            ds_size = int(npoints /2) +1
            iii = 0   
            A_smt = np.zeros((ll,ds_size))
            for ii in np.arange(N)[::l]:
                t_smt = smt_sec.time[ii].data
                y = smt_sec.isel(time=ii)
                cv = y.data.compute() 
                f, S = sg.periodogram(cv-np.mean(cv), fs=1/dm_smt)
                S = S.squeeze()
                A_smt[iii,:] = S
                iii += 1
            f_smt = f
            A_smt_mean = np.mean(A_smt, axis=0)
            std_smt = np.std(A_smt, axis=0)
            print(f'save averaged data in {path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}')
            np.save(f'{path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}', [A_smt, f_smt, dm_smt, A_smt_mean, std_smt])

######################################################################
# Visualization           
if horizontal == False: AAA = np.zeros((S,int(1001))); AAAA = np.zeros((S*n,int(1001))) #A_smt_mean.size
else: AAA = np.zeros((S,int(1401))); AAAA = np.zeros((S*n,int(1401)))
if horizontal == False: BBB = np.zeros((S,int(37))); BBBB = np.zeros((S*n,int(37))) 
else: BBB = np.zeros((S,int(45))); BBBB = np.zeros((S*n,int(45)))  

fig, axs = plt.subplots(3,2, figsize=(20,25))

for K in range(S):
    reg_name = f'{regions[K]}'
    path_dat  = f'{parent_dat}{reg_name}/'
    path_imag = f'{parent_imag}{reg_name}/'
    
    print('plot temporal averaged and temporal + spatial averaged spectra')
    # plot each section
    search_str = f'data_sat*.npy' 
    flist      = np.array(glob.glob(path_dat+search_str))
    flist.sort()
    sat_sections = flist 
    search_str = f'data_smt*.npy' 
    flist      = np.array(glob.glob(path_dat+search_str))
    flist.sort()
    smt_sections = flist

    #kk = 2 
    #plot time averages
    for ii in np.arange(n):
        section = smt_sections[ii]
        [A_smt, f_smt, dm_smt, A_smt_mean, std_smt] = np.load(f'{section}',allow_pickle=True)
        section = sat_sections[ii]
        [A_sat, f_sat, dm_sat, A_sat_mean, std_sat] = np.load(f'{section}',allow_pickle=True)   
        path_save_name = f'{path_imag}ssh_sec_{section[-15:-4:]}'  
        figname = f'{reg_name}{section[-15:-4:]}' 
        #plot_spatial_spectra_horizontal(A_smt_mean, std_smt, f_smt, A_sat_mean, std_sat, f_sat, kk, figname, path_save_name) # if activate region plot doesent work figure in figure

    # average over space n=10
    AA = np.zeros((n,A_smt_mean.size))
    BB = np.zeros((n,A_sat_mean.size))

    for ii in np.arange(n):
        section = smt_sections[ii]
        [A_smt, f_smt, dm_smt, A_smt_mean, std_smt] = np.load(f'{section}',allow_pickle=True)
        AA[ii,:] = A_smt_mean
        if ( (horizontal == True) & (K == 5)) & (ii==1): AA[ii,:] = np.nan; print(f'flag in region {regions[K]} at sec n={ii}'); flag = horizontal, K, n #flag for horizontal sst
        if ( (horizontal == True) & (K == 5)) & (ii==3): AA[ii,:] = np.nan; print(f'flag in region {regions[K]} at sec n={ii}'); flag = horizontal, K, n #flag for horizontal sst


        #satelite
        section = sat_sections[ii]
        [A_sat, f_sat, dm_sat, A_sat_mean, std_sat] = np.load(f'{section}',allow_pickle=True)
        BB[ii,:] = A_sat_mean

    AM = np.nanmean(AA, axis=0)
    BM = np.mean(BB, axis=0)
    
    AAA[K,:] = AM
    BBB[K,:] = BM

    start = K*n; end = K*n + n;
    AAAA[start:end,:] = AA
    BBBB[start:end,:] = BB
    
    #AM_sem = scipy.stats.sem(AA, axis=0, ddof=1)
    AM_std = np.nanstd(AA, axis=0, ddof=1)
    
    BM_sem = scipy.stats.sem(BB, axis=0, ddof=1)
    BM_std = np.std(BB, axis=0, ddof=1)

    #plot time and space averages
    #plot_spatial_spectra6(AM, AM_std, f_smt, BM, BM_std, f_sat, kk, reg_name, K, axs)
    if ((horizontal ==  True) & (K == 5)): n_minus_flag = n-2; 
    else: n_minus_flag = n
    eva.plot_spatial_spectra_ssh_ci(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, reg_name,  K, axs, horizontal, n, n_minus_flag)

path_save_name = f'{parent_imag}ssh_sec_ave_reg_new' 
plt.savefig(f'{path_save_name}.png', bbox_inches='tight', dpi=200)
plt.savefig(f'{path_save_name}.pdf', bbox_inches='tight', dpi=200)

# average over all vertiacl regions
AAM = np.nanmean(AAA, axis=0)
BBM = np.mean(BBB, axis=0)
BBM_std = np.std(BBB, axis=0, ddof=1)
#BBM_sem = scipy.stats.sem(BBB, axis=0, ddof=1)
AAM_std = np.nanstd(AAA, axis=0, ddof=1)
#AAM_sem = scipy.stats.sem(AAA, axis=0, ddof=1)

path_save_name = f'{parent_imag}ssh_sec_ave_all_new'
if horizontal==True: reg_name='zonal'
else: reg_name='meridional'
#plot_spatial_spectra_all_vertical(AAA, AAM, AAM_std, f_smt, BBB, BBM, BBM_std, f_sat, kk, reg_name, path_save_name)
secs=6
eva.plot_spatial_spectra_ssh_all(AAAA, AAM, AAM_std, f_smt, BBBB, BBM, BBM_std, f_sat, reg_name, path_save_name, secs, horizontal, n, K)



