import warnings
warnings.filterwarnings('ignore')  #suppress some warnings about future code changes

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import scipy.signal as sg  #Package for signal analysis
import scipy.ndimage as si #Another package for signal analysis
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates
from scipy import stats    #Used for 2D binned statistics
from mpl_toolkits.axes_grid1 import make_axes_locatable #For plotting interior colobars
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import pyicon as pyic
import glob, os     
from os import path   
import string
from matplotlib.ticker import FormatStrFormatter


import scipy
from scipy import fft as spfft
from scipy.fft import fft 
from scipy.stats import chi2
from scipy.special import digamma
from scipy.stats import t
from pathlib import Path
import sys
from matplotlib.legend_handler import HandlerLine2D, HandlerTuple 
from matplotlib.legend import Legend
# from icon_smt_levels import dzw, dzt, depthc, depthi
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi


print('Load my smt functions')
##################################################################
# small functions
# convert to meter
r = 6371000
const_lat = 32 # latitude of section
latitude = np.linspace(0,90,90)
delta = 2*np.pi*r/360*np.cos(latitude/90* np.pi/2)
#print('distance of one degree in meter at equator', delta[0])
#print('distance of one degree in meter at section', delta[const_lat])

def calc_satellite_spectra(ds, n, k, horizontal, path_dat):
    print('for satellite')
    for kk in np.arange(n):
        print(kk)
        if horizontal == True: Lat, Lon = get_lon_lat_horizontal_sst(k,n)
        else: Lat, Lon = get_lon_lat_vertical_sst(k, n)
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ds_section = ds.where((ds.lat > lat[0]) & (ds.lat < lat[1]) & (ds.lon > lon[0]) & (ds.lon < lon[1]), drop=True)
        ds_lat = int(ds_section.lat[0].data)
        if horizontal == True: 
            ds_section = ds_section.isel(lat=0)
            dm_sat = np.abs((ds_section.lon[1] - ds_section.lon[0])) * delta[ds_lat]
            ds_size = int(ds_section.lon.size /2) +1
        else: 
            ds_section = ds_section.isel(lon=0)
            dm_sat = np.abs((ds_section.lat[1] - ds_section.lat[0])) * delta[ds_lat]
            ds_size = int(ds_section.lat.size /2) +1
        
        dm_sat = dm_sat.data
        ###############################################################
        # satellite
        ds_size_sat = ds_size
        iii = 0
        t = 79
        A_sat = np.zeros((1,ds_size))
        for ii in np.arange(1)[::1]:
            #t_sat = ds_section.time[ii].data
            y = ds_section.isel(time=t).sst_night
            cv = y.data.compute() 
            nans, x= nan_helper(cv) 
            cv[nans]= np.interp(x(nans), x(~nans), cv[~nans])
            f, S = sg.periodogram(cv-np.nanmean(cv), fs=1/dm_sat)
            S = S.squeeze()
            A_sat[iii,:] = S
            iii += 1
        f_sat = f
        A_sat_mean = np.mean(A_sat, axis=0) # average of spectra from realisations of different timesteps
        std_sat = np.std(A_sat, axis=0)
        np.save(f'{path_dat}data_sat_{Lat[kk,0]}_{Lon[kk,0]}', [A_sat, f_sat, dm_sat, A_sat_mean, std_sat])

def calc_satellite_spectra_welch(ds, n, k, horizontal, path_dat):
    print('for satellite')
    for kk in np.arange(n):
        print(kk)
        if horizontal == True: Lat, Lon = get_lon_lat_horizontal_sst(k,n)
        else: Lat, Lon = get_lon_lat_vertical_sst(k, n)
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ds_section = ds.where((ds.lat > lat[0]) & (ds.lat < lat[1]) & (ds.lon > lon[0]) & (ds.lon < lon[1]), drop=True)
        ds_lat = int(ds_section.lat[0].data)
        if horizontal == True: 
            ds_section = ds_section.isel(lat=0)
            dm_sat = np.abs((ds_section.lon[1] - ds_section.lon[0])) * delta[ds_lat]
            ds_size = int(ds_section.lon.size /2) +1
        else: 
            ds_section = ds_section.isel(lon=0)
            dm_sat = np.abs((ds_section.lat[1] - ds_section.lat[0])) * delta[ds_lat]
            ds_size = int(ds_section.lat.size /2) +1
        
        dm_sat = dm_sat.data
        ###############################################################
        # satellite
        ds_size_sat = ds_size
        iii = 0
        t = 79
        P = 2
        A_sat = np.zeros((1,int(ds_size/P/2+1)))
        for ii in np.arange(1)[::1]:
            #t_sat = ds_section.time[ii].data
            y = ds_section.isel(time=t).sst_night
            cv = y.data.compute() 
            nans, x= nan_helper(cv) 
            cv[nans]= np.interp(x(nans), x(~nans), cv[~nans])
            #f, S = sg.periodogram(cv-np.nanmean(cv), fs=1/dm_sat)
            f, S = sg.welch(cv, fs=1/dm_sat, window='hanning', nperseg=int(ds_size/P), noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=- 1, average='mean')
            S = S.squeeze()
            A_sat[iii,:] = S
            iii += 1
        f_sat = f
        A_sat_mean = np.mean(A_sat, axis=0) # average of spectra from realisations of different timesteps
        std_sat = np.std(A_sat, axis=0)
        np.save(f'{path_dat}data_sat_{Lat[kk,0]}_{Lon[kk,0]}', [A_sat, f_sat, dm_sat, A_sat_mean, std_sat])

def calc_smt_spectra(smt, n, k, horizontal, path_dat):
    print('and for smt model')
    Lat, Lon = get_lon_lat_vertical_sst(k, n)
    for kk in np.arange(n):
        print(kk)
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ##############################################################
        ######## calculate nearest neighbour of section
        if horizontal == True: npoints = 2300
        else: npoints = 850 #exp1 = 1700 #2000ssh
        
        sname         = 'A'
        tgname        = 'SMT'
        gname         = 'OceanOnly_SubmesoNA_2500m_srtm30'
        path_tgrid    = f'/pool/data/ICON/oes/grids/OceanOnly/'
        fname_tgrid   = f'{gname}.nc'
        path_scratch  = f'/scratch/m/m300878/slices/'
        path_ckdtree  = path_scratch # where grid is stored
        if False: #Path(sec_path ).is_file():
            print ("section File exist")
            data = xr.open_dataset(sec_path)
            ickdtree = data.ickdtree_c.data
            lon_sec = data.lon_sec.data
            lat_sec = data.lat_sec.data
        else:
            print ("section file does not exist")
            if horizontal == True:
                latii = Lat[kk,:]
                dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lon[1],latii[0]], p2=[lon[0],latii[0]], npoints=npoints,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_ckdtree,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_egrid=False,
                          load_vgrid=False,
                          )
            else:
                lonii = Lon[kk,:]
                dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lonii[0],lat[1]], p2=[lonii[0],lat[0]], npoints=npoints,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_ckdtree,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_egrid=False,
                          load_vgrid=False,
                          )
            # check grid
            sgrid = xr.open_dataset(path_tgrid + fname_tgrid)
            grid  = sgrid.cell_area_p.compute()
            Clon  =  grid.clon * 180/np.pi
            grid  = grid.assign_coords(clon=Clon)
            Clat  =  grid.clat * 180/np.pi
            grid  = grid.assign_coords(clat=Clat)
            grid  = np.sqrt(grid.isel(cell=ickdtree))
            grid  = grid.assign_coords({"lon_sec": ("cell", lon_sec)})
            grid_smt = grid.assign_coords({"lat_sec": ("cell", lat_sec)})
            # section distance
            if horizontal == True:
                lat_min = int(grid_smt.lat_sec[0])
                d_smt_sec = (grid_smt.lon_sec[0] - grid_smt.lon_sec[1]).data *delta[lat_min]
            else:
                lat_min = int(grid_smt.lat_sec[0])
                d_smt_sec = (grid_smt.lat_sec[0] - grid_smt.lat_sec[1]).data *delta[lat_min]
            d_smt_hi_res = grid_smt.min().data
            print('max',grid_smt.max().data, 'min', grid_smt.min().data, 'distance section', d_smt_sec)
        # select data
        smt_sec = smt.isel(ncells=ickdtree)
        if horizontal == True:
            smt_sec = smt_sec.assign_coords({"clon": ("ncells", lon_sec)})
        else:
            smt_sec = smt_sec.assign_coords({"clat": ("ncells", lat_sec)})
        
        dm_smt = d_smt_sec
        # smt
        tsmt = 71*12
        #N = 984
        #l = 11
        #ll = int(N/l) +1
        ds_size = int(npoints /2) +1
        iii = 0   
        A_smt = np.zeros((1,ds_size))
        for ii in np.arange(1)[::1]:
            t_smt = smt_sec.time[tsmt].data
            y = smt_sec.isel(time=tsmt)
            cv = y.data.compute() 
            f, S = sg.periodogram(cv-np.mean(cv), fs=1/dm_smt)
            S = S.squeeze()
            A_smt[iii,:] = S
            iii += 1
        f_smt = f
        A_smt_mean = np.mean(A_smt, axis=0)
        std_smt = np.std(A_smt, axis=0)
        print(f'save averaged data in {path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}')
        np.save(f'{path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}', [A_smt, f_smt, dm_smt, A_smt_mean, std_smt])

def calc_smt_spectra_welch(smt, n, k, horizontal, path_dat):
    print('and for smt model')
    Lat, Lon = get_lon_lat_vertical_sst(k, n)
    for kk in np.arange(n):
        print(kk)
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ##############################################################
        ######## calculate nearest neighbour of section
        if horizontal == True: npoints = 2300
        else: npoints = 1700 #exp2 = 850 #exp1 = 1700 #2000ssh
        
        sname         = 'A'
        tgname        = 'SMT'
        gname         = 'OceanOnly_SubmesoNA_2500m_srtm30'
        path_tgrid    = f'/pool/data/ICON/oes/grids/OceanOnly/'
        fname_tgrid   = f'{gname}.nc'
        path_scratch  = f'/scratch/m/m300878/slices/'
        path_ckdtree  = path_scratch # where grid is stored
        if False: #Path(sec_path ).is_file():
            print ("section File exist")
            data = xr.open_dataset(sec_path)
            ickdtree = data.ickdtree_c.data
            lon_sec = data.lon_sec.data
            lat_sec = data.lat_sec.data
        else:
            print ("section file does not exist")
            if horizontal == True:
                latii = Lat[kk,:]
                dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lon[1],latii[0]], p2=[lon[0],latii[0]], npoints=npoints,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_ckdtree,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_egrid=False,
                          load_vgrid=False,
                          )
            else:
                lonii = Lon[kk,:]
                dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lonii[0],lat[1]], p2=[lonii[0],lat[0]], npoints=npoints,
                          fname_tgrid  = fname_tgrid,
                          path_tgrid   = path_tgrid,
                          path_ckdtree = path_ckdtree,
                          sname = sname,
                          gname = gname,
                          tgname = tgname,
                          load_egrid=False,
                          load_vgrid=False,
                          )
            # check grid
            sgrid = xr.open_dataset(path_tgrid + fname_tgrid)
            grid  = sgrid.cell_area_p.compute()
            Clon  =  grid.clon * 180/np.pi
            grid  = grid.assign_coords(clon=Clon)
            Clat  =  grid.clat * 180/np.pi
            grid  = grid.assign_coords(clat=Clat)
            grid  = np.sqrt(grid.isel(cell=ickdtree))
            grid  = grid.assign_coords({"lon_sec": ("cell", lon_sec)})
            grid_smt = grid.assign_coords({"lat_sec": ("cell", lat_sec)})
            # section distance
            if horizontal == True:
                lat_min = int(grid_smt.lat_sec[0])
                d_smt_sec = (grid_smt.lon_sec[0] - grid_smt.lon_sec[1]).data *delta[lat_min]
            else:
                lat_min = int(grid_smt.lat_sec[0])
                d_smt_sec = (grid_smt.lat_sec[0] - grid_smt.lat_sec[1]).data *delta[lat_min]
            d_smt_hi_res = grid_smt.min().data
            print('max',grid_smt.max().data, 'min', grid_smt.min().data, 'distance section', d_smt_sec)
        # select data
        smt_sec = smt.isel(ncells=ickdtree)
        if horizontal == True:
            smt_sec = smt_sec.assign_coords({"clon": ("ncells", lon_sec)})
        else:
            smt_sec = smt_sec.assign_coords({"clat": ("ncells", lat_sec)})
        
        dm_smt = d_smt_sec
        # smt
        tsmt = 71*12
        #N = 984
        #l = 11
        #ll = int(N/l) +1
        ds_size = int(npoints /2) +1
        iii = 0   
        P = 4
        A_smt = np.zeros((1,int(ds_size/P/2+1)))
        for ii in np.arange(1)[::1]:
            t_smt = smt_sec.time[tsmt].data
            y = smt_sec.isel(time=tsmt)
            cv = y.data.compute() 
            #f, S = sg.periodogram(cv-np.mean(cv), fs=1/dm_smt)
            f, S = sg.welch(cv, fs=1/dm_smt, window='hanning', nperseg=int(ds_size/P), noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=- 1, average='mean')
            S = S.squeeze()
            A_smt[iii,:] = S
            iii += 1
        f_smt = f
        A_smt_mean = np.mean(A_smt, axis=0)
        std_smt = np.std(A_smt, axis=0)
        print(f'save averaged data in {path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}')
        np.save(f'{path_dat}data_smt_{Lat[kk,0]}_{Lon[kk,0]}', [A_smt, f_smt, dm_smt, A_smt_mean, std_smt])


def calc_r2b8_spectra(r2b8, n, k, horizontal, path_dat):
    print('and for r2b8 model')
    if horizontal == True: Lat, Lon = get_lon_lat_horizontal_r2b8(k, n)
    else: Lat, Lon = get_lon_lat_vertical_sst_r2b8_exp2(k, n)
    for kk in np.arange(n):
        print(kk)
        data = r2b8
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ds_section = data.where((data.lat > lat[0]) & (data.lat < lat[1]) & (data.lon > lon[0]) & (data.lon < lon[1]), drop=True)
        ds_lat = int(ds_section.lat[0].data)
        if horizontal == True: 
            ds_section = ds_section.isel(lat=0)
            dm_r2b8 = np.abs((ds_section.lon[1] - ds_section.lon[0])) * delta[ds_lat]
            ds_size = int(ds_section.lon.size /2) +1
        else: 
            ds_section = ds_section.isel(lon=0)
            dm_r2b8 = np.abs((ds_section.lat[1] - ds_section.lat[0])) * delta[ds_lat]
            ds_size = int(ds_section.lat.size /2) +1
        
        dm_r2b8 = dm_r2b8.data
        ###############################################################
        # r2b8
        ds_size_r2b8 = ds_size
        iii = 0
        t = 79
        A_r2b8 = np.zeros((1,ds_size))
        for ii in np.arange(1)[::1]:
            y = ds_section.isel(time=t)
            cv = y.data
            f, S = sg.periodogram(cv-np.nanmean(cv), fs=1/dm_r2b8)
            S = S.squeeze()
            A_r2b8[iii,:] = S
            iii += 1
        f_r2b8 = f
        A_r2b8_mean = np.mean(A_r2b8, axis=0) # average of spectra from realisations of different timesteps
        std_r2b8 = np.std(A_r2b8, axis=0)
        np.save(f'{path_dat}data_r2b8_{Lat[kk,0]}_{Lon[kk,0]}', [A_r2b8, f_r2b8, dm_r2b8, A_r2b8_mean, std_r2b8])

def calc_r2b8_spectra_welch(r2b8, n, k, horizontal, path_dat):
    print('and for r2b8 model')
    if horizontal == True: Lat, Lon = get_lon_lat_horizontal_r2b8(k, n)
    else: Lat, Lon = get_lon_lat_vertical_sst_r2b8(k, n)
    for kk in np.arange(n):
        print(kk)
        data = r2b8
        lat = Lat[kk,:]
        lon = Lon[kk,:]
        ds_section = data.where((data.lat > lat[0]) & (data.lat < lat[1]) & (data.lon > lon[0]) & (data.lon < lon[1]), drop=True)
        ds_lat = int(ds_section.lat[0].data)
        if horizontal == True: 
            ds_section = ds_section.isel(lat=0)
            dm_r2b8 = np.abs((ds_section.lon[1] - ds_section.lon[0])) * delta[ds_lat]
            ds_size = int(ds_section.lon.size /2) +1
        else: 
            ds_section = ds_section.isel(lon=0)
            dm_r2b8 = np.abs((ds_section.lat[1] - ds_section.lat[0])) * delta[ds_lat]
            ds_size = int(ds_section.lat.size /2) +1
        
        dm_r2b8 = dm_r2b8.data
        ###############################################################
        # r2b8
        ds_size_r2b8 = ds_size
        iii = 0
        t = 79
        P = 2
        A_r2b8 = np.zeros((1,ds_size))
        A_r2b8 = np.zeros((1,int(ds_size/P/2+1)))
        for ii in np.arange(1)[::1]:
            y = ds_section.isel(time=t)
            cv = y.data
            #f, S = sg.periodogram(cv-np.nanmean(cv), fs=1/dm_r2b8)
            f, S = sg.welch(cv, fs=1/dm_r2b8, window='hanning', nperseg=int(ds_size/P), noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=- 1, average='mean')
            S = S.squeeze()
            A_r2b8[iii,:] = S
            iii += 1
        f_r2b8 = f
        A_r2b8_mean = np.mean(A_r2b8, axis=0) # average of spectra from realisations of different timesteps
        std_r2b8 = np.std(A_r2b8, axis=0)
        np.save(f'{path_dat}data_r2b8_{Lat[kk,0]}_{Lon[kk,0]}', [A_r2b8, f_r2b8, dm_r2b8, A_r2b8_mean, std_r2b8])

def calc_mld_montegut(rho_mean, depthc):
    """ 
    applies Montegut 2004 Threshold Method
    if salinity can be negelcted it is equivalent to dT = 0.2 
    """

    rho_surf_mean = rho_mean.isel(depthc=2) #should be 10m
    diff = np.sqrt((rho_surf_mean - rho_mean)**2)
    
    d = diff.where(diff<=0.03)
    dd = np.sum(~np.isnan(d), axis=2)
    
    return(depthc[dd-1])   

def calc_richardsonnumber(lat, N2, M2):
    """calculates Richardssonumber with respect to latitude"""

    f=2*2*np.pi/86400*np.sin(lat*np.pi/180.)
    f2 = f**2
    M4 = xr.ufunc.power(M2,2)
    
    return N2 * f2  / M4 

def nan_helper(y):
    """Helper to handle indices and logical indices of NaNs.

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        >>> # linear interpolation of NaNs
        >>> nans, x= nan_helper(y)
        >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
    """

    return np.isnan(y), lambda z: z.nonzero()[0]

#-------------------Plots----------------#

def plot_spatial_spectra_horizontal(A_smt_mean, std_smt, f_smt, A_sat_mean, std_sat, f_sat, k, figname, path_save_name, horizontal):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))
    #l=2.5

    # smt
    kernel_size = k
    kernel = np.ones(kernel_size) / kernel_size
    #smean_m_std = A_smt_mean - std_smt #np.abs(A_smt_mean - std_smt)
    smean_m_std = np.abs(A_smt_mean - std_smt)
    smean_m_std = np.convolve(smean_m_std, kernel, mode='same')
    #smean_p_std = A_smt_mean + std_smt#np.abs(A_smt_mean + std_smt)
    smean_p_std = np.abs(A_smt_mean + std_smt)
    smean_p_std = np.convolve(smean_p_std, kernel, mode='same')

    plt.loglog(f_smt, smean_p_std, color='royalblue', linestyle='--',alpha=0.5)
    plt.loglog(f_smt, A_smt_mean, label='smt_ssh_mean', color='royalblue', linewidth=2)
    plt.fill_between(f_smt, A_smt_mean, smean_p_std, color='royalblue', alpha=0.25)
    plt.loglog(f_smt, smean_m_std, linestyle='--',  color='royalblue', alpha=0.5)
    plt.fill_between(f_smt, A_smt_mean, smean_m_std, color='royalblue', alpha=0.25)

    # satellite
    kernel_size = k
    kernel = np.ones(kernel_size) / kernel_size
    mean_m_std = np.abs(A_sat_mean - std_sat)
    mean_m_std = np.convolve(mean_m_std, kernel, mode='same')
    mean_p_std = np.abs(A_sat_mean + std_sat)
    mean_p_std = np.convolve(mean_p_std, kernel, mode='same')

    plt.loglog(f_sat, mean_p_std, color='tomato', linestyle='--', alpha=0.5)
    plt.loglog(f_sat, A_sat_mean, label='sat_adt_mean', color='tomato', linewidth=2)
    plt.fill_between(f_sat, A_sat_mean, mean_p_std, color='wheat', alpha=0.5)
    plt.loglog(f_sat, mean_m_std, linestyle='--',  color='tomato', alpha=0.5)
    plt.fill_between(f_sat, A_sat_mean, mean_m_std, color='wheat', alpha=0.5)

    #####################
    fx = np.linspace(1e-4,5e-4,100)
    pp = 10000
    y2 = pp* 3e-9*np.power(fx, (-2))
    y3 = pp* 1e-13*np.power(fx, (-3))
    y5 = pp* 1e-7*np.power(fx, (-5/3))

    plt.loglog(fx,y3, label='slope -3')
    plt.loglog(fx,y5, label='slope -5/3')

    # slope satellite
    xdata = f_sat[8:]
    ydata = A_sat_mean[8:]
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="limegreen")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="limegreen", label=f'slope {index:3.3}') 


    # slope smt
    if horizontal == True: 
        xdata = f_smt[50::700]
        ydata = A_smt_mean[50::700]
    else:
        xdata = f_smt[32::600]
        ydata = A_smt_mean[32::600]

    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="gold")  
    plt.plot(fx, powerlaw(fx, 5*pp*amp, index), linestyle='dashed', color="gold", label=f'slope {index:3.3}') 

    # slope smt
    if horizontal == True:
        xdata = f_smt[8:48]
        ydata = A_smt_mean[8:48]
    else:
        xdata = f_smt[6:30]
        ydata = A_smt_mean[6:30]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index), linewidth=4, linestyle='--', color="aqua")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linewidth=2, linestyle='--', color="aqua", label=f'slope {index:3.3}') 

    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(8e-4, 5e4)
    #ax.autoscale(enable=True, tight=True)
    #ax.set_xlim(1e-6, 1e10)
    fig.tight_layout()

    plt.legend(fontsize=15, loc='lower left')
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    plt.title(f'Averaged Spectral Estimates of SSH {figname}',fontsize=15)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight')

# def plot_spatial_spectra_sst(A_smt_mean, std_smt, f_smt, A_sat_mean, std_sat, f_sat, k, figname, path_save_name, horizontal):
#     powerlaw = lambda x, amp, index: amp * (x**index)
#     fig, ax = plt.subplots(1, 1, figsize=(12, 6))

#     plt.loglog(f_smt, A_smt_mean, label='smt_sst_mean', color='royalblue', linewidth=2)
#     plt.loglog(f_sat, A_sat_mean, label='sat_sst_mean', color='tomato', linewidth=2)

#     add_slopes(f_sat, A_sat_mean, f_smt, A_smt_mean, horizontal)

#     x = f_smt
#     def forward(x):
#         return 1 / x / 1000
#     def inverse(x):
#         return 1 / x 
#     secax = ax.secondary_xaxis('top', functions=(forward, inverse))
#     secax.set_xlabel('wavelength in [km]', fontsize=15)

#     ax.autoscale(enable=True, tight=True)
#     ax.set_ylim(1e-2, 1e7)
#     #ax.autoscale(enable=True, tight=True)
#     #ax.set_xlim(1e-6, 1e10)
#     fig.tight_layout()

#     plt.legend(fontsize=15, loc='lower left')
#     ax.tick_params(labelsize=15)
#     secax.tick_params(labelsize=15)
#     plt.xlabel('Wavenumber (1/m)', fontsize=15)
#     plt.ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
#     plt.title(f'Averaged Spectral Estimates of SST {figname}',fontsize=15)
#     plt.savefig(f'{path_save_name}.png', bbox_inches='tight')


def add_slopes(f_sat, A_sat_mean, f_smt, A_smt_mean, horizontal):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fx = np.linspace(1e-4,5e-4,100)
    pp = 1000
    y2 = pp* 3e-5*np.power(fx, (-2))
    y3 = pp* 1e-10*np.power(fx, (-3))
    y5 = pp* 1e-3*np.power(fx, (-5/3))

    plt.loglog(fx,y3, label='slope -3')
    plt.loglog(fx,y5, label='slope -5/3')

    # slope satellite
    xdata = f_sat[15:]
    ydata = A_sat_mean[15:]
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="limegreen")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="limegreen", label=f'slope {index:3.3}') 


    # slope smt
    if horizontal == True: 
        xdata = f_smt[300:]
        ydata = A_smt_mean[300:]
    else:
        xdata = f_smt[100::600]
        ydata = A_smt_mean[100::600]

    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="gold")  
    plt.plot(fx, powerlaw(fx, 5*pp*amp, index), linestyle='dashed', color="gold", label=f'slope {index:3.3}') 

    # slope smt
    if horizontal == True:
        xdata = f_smt[20:300]
        ydata = A_smt_mean[20:300]
    else:
        xdata = f_smt[15:145]
        ydata = A_smt_mean[15:145]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index), linewidth=4, linestyle='--', color="aqua")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linewidth=2, linestyle='--', color="aqua", label=f'slope {index:3.3}') 




def plot_spatial_spectra_sst_horizontal(A_smt_mean, std_smt, f_smt, A_sat_mean, std_sat, f_sat, figname, path_save_name, horizontal):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))
    #l=2.5

    # # smt
    # kernel_size = k
    # kernel = np.ones(kernel_size) / kernel_size
    # #smean_m_std = A_smt_mean - std_smt #np.abs(A_smt_mean - std_smt)
    # smean_m_std = np.abs(A_smt_mean - std_smt)
    # smean_m_std = np.convolve(smean_m_std, kernel, mode='same')
    # #smean_p_std = A_smt_mean + std_smt#np.abs(A_smt_mean + std_smt)
    # smean_p_std = np.abs(A_smt_mean + std_smt)
    # smean_p_std = np.convolve(smean_p_std, kernel, mode='same')

    #plt.loglog(f_smt, smean_p_std, color='royalblue', linestyle='--',alpha=0.5)
    plt.loglog(f_smt, A_smt_mean, label='smt_sst_mean', color='royalblue', linewidth=2)
    #plt.fill_between(f_smt, A_smt_mean, smean_p_std, color='royalblue', alpha=0.25)
    #plt.loglog(f_smt, smean_m_std, linestyle='--',  color='royalblue', alpha=0.5)
    #plt.fill_between(f_smt, A_smt_mean, smean_m_std, color='royalblue', alpha=0.25)

    # # satellite
    # kernel_size = k
    # kernel = np.ones(kernel_size) / kernel_size
    # mean_m_std = np.abs(A_sat_mean - std_sat)
    # mean_m_std = np.convolve(mean_m_std, kernel, mode='same')
    # mean_p_std = np.abs(A_sat_mean + std_sat)
    # mean_p_std = np.convolve(mean_p_std, kernel, mode='same')

    #plt.loglog(f_sat, mean_p_std, color='tomato', linestyle='--', alpha=0.5)
    plt.loglog(f_sat, A_sat_mean, label='sat_sst_mean', color='tomato', linewidth=2)
    #plt.fill_between(f_sat, A_sat_mean, mean_p_std, color='wheat', alpha=0.5)
    #plt.loglog(f_sat, mean_m_std, linestyle='--',  color='tomato', alpha=0.5)
    #plt.fill_between(f_sat, A_sat_mean, mean_m_std, color='wheat', alpha=0.5)

    #####################
    fx = np.linspace(1e-4,5e-4,100)
    pp = 100000
    y2 = pp* 3e-8*np.power(fx, (-2))
    y3 = pp* 1e-12*np.power(fx, (-3))
    y5 = pp* 1e-6*np.power(fx, (-5/3))

    plt.loglog(fx,y3, label='slope -3')
    plt.loglog(fx,y5, label='slope -5/3')

    # slope satellite
    xdata = f_sat[20:]
    ydata = A_sat_mean[20:]
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="limegreen")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="limegreen", label=f'slope {index:3.3}') 


    # slope smt
    if horizontal == True: 
        xdata = f_smt[300:]
        ydata = A_smt_mean[300:]
    else:
        xdata = f_smt[32::600]
        ydata = A_smt_mean[32::600]

    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="gold")  
    plt.plot(fx, powerlaw(fx, 5*pp*amp, index), linestyle='dashed', color="gold", label=f'slope {index:3.3}') 

    # slope smt
    if horizontal == True:
        xdata = f_smt[20:300]
        ydata = A_smt_mean[20:300]
    else:
        xdata = f_smt[6:30]
        ydata = A_smt_mean[6:30]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    plt.plot(xdata, powerlaw(xdata, amp, index), linewidth=4, linestyle='--', color="aqua")  
    plt.plot(fx, powerlaw(fx, pp*amp, index), linewidth=2, linestyle='--', color="aqua", label=f'slope {index:3.3}') 

    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-2, 1e7)
    #ax.autoscale(enable=True, tight=True)
    #ax.set_xlim(1e-6, 1e10)
    fig.tight_layout()

    plt.legend(fontsize=15, loc='lower left')
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    plt.title(f'Averaged Spectral Estimates of SST {figname}',fontsize=15)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight')


def plot_spatial_spectra_sst_all(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name, path_save_name, secs, horizontal, n, nr, K):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))

    # f_smt = f_smt*1000
    # f_sat = f_sat*1000
    # f_r2b8 = f_r2b8*1000
    # plot_slopes(horizontal, f_sat, BM, f_r2b8, CM, f_smt, AM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)
    # plt.legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.3,0.0), frameon=False)

    # add -2 slope
    x = np.linspace(1e-6, 2e-3, 100)
    y = 1e-6*x**-2
    plt.loglog(x, y, color='black', linestyle='--', linewidth=1)
    #add -2 text at y=5e-5
    plt.text(1.3e-6, 4.5e5, r'${-2}$', fontsize=15)
    # and -3 slope
    y = 1e-10*x**-3
    plt.loglog(x, y, color='black', linestyle=':', linewidth=1)
    #add -3 text at y=5e-5
    plt.text(5.3e-6, 4.5e5, r'${-3}$', fontsize=15)


    s=0.02
    alpha = 0.8
    for ii in np.arange(secs*n):
        plt.scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(secs*n):
        plt.scatter(f_sat, BB[ii,:], color='wheat', s=s, alpha=alpha, rasterized=True)

    for ii in np.arange(secs*nr):
        plt.scatter(f_r2b8, CC[ii,:], color='mediumseagreen', s=s, alpha=alpha, rasterized=True)

    confidence = 0.95 
    n = K*n
    CI = calc_CI(AM, AM_std, n-1, confidence, n)
    l1, = ax.loglog(f_smt, AM, label='smt', color='royalblue', linewidth=1)
    f = ax.fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='royalblue', alpha=0.5, label=f'CI={confidence}', zorder=10)

    CI = calc_CI(BM, BM_std, n-1, confidence, n)
    l2, = ax.loglog(f_sat, BM, label='sat', color='tomato', linewidth=1)
    f2 = ax.fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tomato', alpha=0.5, label=f'CI={confidence}', zorder=10)

    nr = K*nr
    CI = calc_CI(CM, CM_std, nr-1, confidence, nr)
    l3, = ax.loglog(f_r2b8, CM, label='r2b8', color='tab:green', linewidth=1)
    f3 = ax.fill_between(f_r2b8,  CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:green', alpha=0.5, label=f'CI={confidence}', zorder=10)
        
    #---------------------------------------#
    x = f_smt
    # def forward(x):
    #     return 1 / x / 1000
    # def inverse(x):
    #     return 1 / x 
    # secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax = plt.gca().secondary_xaxis('top', functions=(lambda x: 1/x/1000, lambda x: 1/x/1000))
    secax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-1, 1e6)
    ax.set_xlim(7e-7, 8.5e-4)
    fig.tight_layout()

    leg = Legend(ax,
    [(l1, f, f), (l2, f2, f2),(l3, f3, f3)],
    [f"ICON-SMT ", f"Modis Aqua ", f"ICON-R2B8 "],
    fontsize=15,
    loc="lower left",
    frameon=False,
    #handlelength=3.0,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    ax.add_artist(leg) #add artificial legend 
    ax.text(-0.10, 1.02, '('+string.ascii_lowercase[0]+')', transform=ax.transAxes, size=15)
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.grid(ls=':')
    plt.xlabel('wavenumber [1/m]', fontsize=15)
    plt.ylabel(r'PSD $K^2/(1/m)$',fontsize=15)
    # plt.title(f'Spatially averaged spectral estimates of {reg_name} SST sections',fontsize=15)
    plt.savefig(f'{path_save_name}.pdf', bbox_inches='tight', dpi=200)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight', dpi=200)



def plot_spatial_spectra_sst_all_new(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name, path_save_name, secs, horizontal, n, nr, K):
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))

    x = np.linspace(1e-6, 2e-3, 100)
    y = 1e-6*x**-2
    plt.loglog(x, y, color='black', linestyle='--', linewidth=1)
    plt.text(1.3e-6, 4.5e5, r'${-2}$', fontsize=15)
    y = 1e-10*x**-3
    plt.loglog(x, y, color='black', linestyle=':', linewidth=1)
    plt.text(5.3e-6, 4.5e5, r'${-3}$', fontsize=15)


    s=0.02
    alpha = 0.8
    # for ii in np.arange(secs*n):
    #     plt.scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    # for ii in np.arange(secs*n):
    #     plt.scatter(f_sat, BB[ii,:], color='wheat', s=s, alpha=alpha, rasterized=True)

    # for ii in np.arange(secs*nr):
    #     plt.scatter(f_r2b8, CC[ii,:], color='mediumseagreen', s=s, alpha=alpha, rasterized=True)

    confidence = 0.95 
    n = K*n
    CI = calc_CI(AM, AM_std, n-1, confidence, n)
    l1, = ax.loglog(f_smt, AM, label='smt', color='royalblue', linewidth=2.5)
    f = ax.fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='royalblue', alpha=0.5, label=f'CI={confidence}', zorder=10)

    CI = calc_CI(BM, BM_std, n-1, confidence, n)
    l2, = ax.loglog(f_sat, BM, label='sat', color='tomato', linewidth=2.5)
    f2 = ax.fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tomato', alpha=0.5, label=f'CI={confidence}', zorder=10)

    nr = K*nr
    CI = calc_CI(CM, CM_std, nr-1, confidence, nr)
    l3, = ax.loglog(f_r2b8, CM, label='r2b8', color='tab:green', linewidth=2.5)
    f3 = ax.fill_between(f_r2b8,  CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:green', alpha=0.4, label=f'CI={confidence}', zorder=10)
        
    #---------------------------------------#
    x = f_smt
    secax = plt.gca().secondary_xaxis('top', functions=(lambda x: 1/x/1000, lambda x: 1/x/1000))
    secax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-1, 1e6)
    ax.set_xlim(7e-7, 8.5e-4)
    fig.tight_layout()

    leg = Legend(ax,
    [(l1, f, f), (l2, f2, f2),(l3, f3, f3)],
    [f"ICON-SMT ", f"Modis Aqua ", f"ICON-R2B8 "],
    fontsize=15,
    loc="lower left",
    frameon=False,
    #handlelength=3.0,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    ax.add_artist(leg) #add artificial legend 
    ax.text(-0.10, 1.02, '('+string.ascii_lowercase[0]+')', transform=ax.transAxes, size=15)
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.grid(ls=':')
    plt.xlabel('wavenumber [1/m]', fontsize=15)
    plt.ylabel(r'PSD $K^2/(1/m)$',fontsize=15)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight', dpi=300)


def plot_spatial_spectra_ssh_all(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name, path_save_name, secs, horizontal, n, K, savefig):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))

    # plot_slopes_ssh(horizontal, f_sat, BM, f_r2b8, CM, f_smt, AM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)
    # plot_slopes_ssh(horizontal, f_sat, BM, f_smt, AM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)

    # plot_slopes_ssh3(horizontal, f_sat, BM, f_smt, AM, f_r2b8, CM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)
    # plt.legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.3,0.0), frameon=False)

    f = f_smt[2:]
    ax.loglog(f, 1e-8*f**(-2), color='k', lw=1, linestyle='-')
    ax.text(1e-6, 2e3, r'${-2}$', fontsize=14, rotation=0, va='bottom', ha='left')
    # and -3 slope
    ax.loglog(f, 1e-13*f**(-3), color='k', lw=1, linestyle='--')
    ax.text(1e-6, 4e4, r'${-3}$', fontsize=14, rotation=0, va='bottom', ha='left')
    # and -11/3
    ax.loglog(f, 6e-17*f**(-11/3), color='k', lw=1, linestyle='-.')
    ax.text(2e-6, 4e4, r'${-11/3}$', fontsize=14, rotation=0, va='bottom', ha='left')
    ax.text(-0.10, 1.02, '('+string.ascii_lowercase[1]+')', transform=ax.transAxes, size=15)

    s=0.05
    alpha = 0.8
    for ii in np.arange(K*n):
        plt.scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(K*n):
        plt.scatter(f_sat, BB[ii,:], color='tab:orange', s=s, alpha=alpha, rasterized=True)

    for ii in np.arange(n):
        ax.scatter(f_r2b8, CC[ii,:], color='tab:green', s=s, alpha=alpha, rasterized=True)


    confidence = 0.95
    n_smt = K*n#*int(984/11)
    CI    = calc_CI(AM, AM_std, n_smt-1, confidence, n_smt)
    l1,   = ax.loglog(f_smt, AM, label='smt', color='royalblue', linewidth=1)
    f     = ax.fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='royalblue', alpha=0.5, label=f'CI={confidence}', zorder=10)

    n_sat = K*n#*90
    CI    = calc_CI(BM, BM_std, n_sat-1, confidence, n_sat)
    l2,   = ax.loglog(f_sat, BM, label='sat', color='tab:orange', linewidth=1)
    f2    = ax.fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:orange', alpha=0.5, label=f'CI={confidence}', zorder=10)

    n_r2b8 = K*n
    CI     = calc_CI(CM, CM_std, n_r2b8-1, confidence, n_r2b8)
    l3, = ax.loglog(f_r2b8, CM, label='r2b8', color='tab:green', linewidth=1)
    f3  = ax.fill_between(f_r2b8, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:green', alpha=0.5, label=f'CI={confidence}', zorder=10)

    #---------------------------------------#
    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)
    secax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))


    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-3, 1e5)
    ax.set_xlim(7e-7, 8.5e-4)
    fig.tight_layout()

    leg = Legend(ax,
    [(l1, f, f), (l2, f2, f2), (l3, f3, f3)],
    [f"ICON-SMT ", f"Aviso ", f"ICON-R2B8 "],
    fontsize=15,
    loc="lower left",
    frameon=False,
    #handlelength=3.0,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    ax.add_artist(leg) #add artificial legend 
    
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.grid(ls=':')
    plt.xlabel(r'wavenumber [1/m]', fontsize=15)
    plt.ylabel(r'PSD $m^2/(1/m)$',fontsize=15)
    # plt.title(f'Temporal and spatially averaged spectral estimates of {reg_name} SSH sections',fontsize=15)
    # if savefig == True: plt.savefig(f'{path_save_name}.pdf', bbox_inches='tight', dpi=200)
    if savefig == True: plt.savefig(f'{path_save_name}.png', bbox_inches='tight', dpi=200)



def plot_spatial_spectra_ssh_all_new(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name, path_save_name, secs, horizontal, n, K, savefig):
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))

    # plot_slopes_ssh(horizontal, f_sat, BM, f_r2b8, CM, f_smt, AM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)
    # plot_slopes_ssh(horizontal, f_sat, BM, f_smt, AM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)

    # plot_slopes_ssh3(horizontal, f_sat, BM, f_smt, AM, f_r2b8, CM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)
    # plt.legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.3,0.0), frameon=False)

    f = f_smt[:]
    ax.loglog(f, 1e-8*f**(-2), color='k', lw=1, linestyle='--')
    ax.text(1e-6, 2e3, r'${-2}$', fontsize=14, rotation=0, va='bottom', ha='left')
    # and -3 slope
    ax.loglog(f, 1e-13*f**(-3), color='k', lw=1, linestyle=':')
    ax.text(1e-6, 2e4, r'${-3}$', fontsize=14, rotation=0, va='bottom', ha='left')
    # and -11/3
    ax.loglog(f, 6e-17*f**(-11/3), color='k', lw=1, linestyle='-.')
    ax.text(2e-6, 4e4, r'${-11/3}$', fontsize=14, rotation=0, va='bottom', ha='left')
    ax.text(-0.10, 1.02, '('+string.ascii_lowercase[1]+')', transform=ax.transAxes, size=15)

    s=0.05
    alpha = 0.8
    # for ii in np.arange(K*n):
    #     plt.scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    # for ii in np.arange(K*n):
    #     plt.scatter(f_sat, BB[ii,:], color='tab:orange', s=s, alpha=alpha, rasterized=True)

    # for ii in np.arange(n):
    #     ax.scatter(f_r2b8, CC[ii,:], color='tab:green', s=s, alpha=alpha, rasterized=True)


    confidence = 0.95
    n_smt = K*n#*int(984/11)
    CI    = calc_CI(AM, AM_std, n_smt-1, confidence, n_smt)
    l1,   = ax.loglog(f_smt, AM, label='smt', color='royalblue', linewidth=2.5)
    f     = ax.fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='royalblue', alpha=0.5, label=f'CI={confidence}', zorder=10)

    n_sat = K*n#*90
    CI    = calc_CI(BM, BM_std, n_sat-1, confidence, n_sat)
    l2,   = ax.loglog(f_sat, BM, label='sat', color='tab:orange', linewidth=2.5)
    f2    = ax.fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:orange', alpha=0.4, label=f'CI={confidence}', zorder=10)

    n_r2b8 = K*n
    CI     = calc_CI(CM, CM_std, n_r2b8-1, confidence, n_r2b8)
    l3, = ax.loglog(f_r2b8, CM, label='r2b8', color='tab:green', linewidth=2.5)
    f3  = ax.fill_between(f_r2b8, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:green', alpha=0.5, label=f'CI={confidence}', zorder=10)

    #---------------------------------------#
    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)
    secax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))


    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-3, 1e5)
    ax.set_xlim(7e-7, 8.5e-4)
    fig.tight_layout()

    leg = Legend(ax,
    [(l1, f, f), (l2, f2, f2), (l3, f3, f3)],
    [f"ICON-SMT ", f"Aviso-C3S", f"ICON-R2B8 "],
    fontsize=15,
    loc="lower left",
    frameon=False,
    #handlelength=3.0,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    ax.add_artist(leg) #add artificial legend 
    
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.grid(ls=':')
    plt.xlabel(r'wavenumber [1/m]', fontsize=15)
    plt.ylabel(r'PSD $m^2/(1/m)$',fontsize=15)
    # plt.title(f'Temporal and spatially averaged spectral estimates of {reg_name} SSH sections',fontsize=15)
    # if savefig == True: plt.savefig(f'{path_save_name}.pdf', bbox_inches='tight', dpi=200)
    if savefig == True: plt.savefig(f'{path_save_name}.png', bbox_inches='tight', dpi=300)


def plot_spatial_spectra_sst_section(A_smt, f_smt, A_sat, f_sat, A_r2b8, f_r2b8, figname, path_save_name):
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))

    plt.loglog(f_smt, A_smt, label='smt_sst', color='royalblue', linewidth=1)

    plt.loglog(f_sat, A_sat, label='sat_sst', color='tomato', linewidth=1)

    plt.loglog(f_r2b8, A_r2b8, label='r2b8_sst', color='tab:green', linewidth=1)

    #---------------------------------------#
    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-1, 1e6)

    fig.tight_layout()

    plt.legend(fontsize=15, loc='lower left')
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density K^2/(1/m)',fontsize=15)
    plt.title(f'Averaged Spectral Estimates of SST {figname}',fontsize=15)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight')

def plot_spatial_spectra_all_lat_dep(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, reg_name, path_save_name, secs, horizontal, regions):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))
    color = iter(cm.viridis(np.linspace(0, 1, secs)))

    for ii in np.arange(secs):
        c = next(color)
        plt.scatter(f_smt, AA[ii,:], c=c, label=f'sst_smt_{regions[ii]}', alpha =0.5, s=6)
    
    color = iter(cm.viridis(np.linspace(0, 1, secs)))
    for ii in np.arange(secs):
        c = next(color)
        plt.scatter(f_sat, BB[ii,:], c=c, label=f'sst_sat_{regions[ii]}', alpha=1, s=10, marker='s')

    plt.loglog(f_smt, AM, label='smt_sst_mean', color='royalblue', linewidth=3)
    plt.loglog(f_sat, BM, label='sat_sst_mean', color='tomato', linewidth=3)

    
    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-1, 1e6)
    fig.tight_layout()

    plt.legend(fontsize=15, loc='lower left')
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.xlabel('Wavenumber (1/m)', fontsize=15)
    plt.ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    plt.title(f'Latitude dependence of SST Spectra {reg_name}',fontsize=15)
    plt.savefig(f'{path_save_name}.png', bbox_inches='tight')

def calc_CI(m, s, dof, confidence, realisations):
    t_crit = np.abs( t.ppf((1-confidence)/2, dof))
    A = (m-s*t_crit/np.sqrt(realisations), m+s*t_crit/np.sqrt(realisations))
    return A

def plot_spatial_spectra_ci(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name,  K, axs, horizontal, n, nr):
    powerlaw = lambda x, amp, index: amp * (x**index)
    
    if K == 0: idx1, idx2 = 0,0
    elif K == 1: idx1, idx2 = 0,1
    elif K == 2: idx1, idx2 = 1,0
    elif K == 3: idx1, idx2 = 1,1
    elif K == 4: idx1, idx2 = 2,0
    elif K == 5: idx1, idx2 = 2,1
    else:
        raise ValueError('K must be between 0 and 5')

    plot_slopes(horizontal, f_sat, BM, f_r2b8, CM, f_smt, AM, powerlaw, axs, idx1, idx2, REG = True)
    axs[idx1,idx2].legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.5,0.0), frameon=False)

    s=0.05
    alpha = 0.8
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_sat, BB[ii,:], color='wheat', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(nr):
        axs[idx1,idx2].scatter(f_r2b8, CC[ii,:], color='mediumseagreen', s=s, alpha=alpha, rasterized=True)

    confidence = 0.95 
    
    CI = calc_CI(AM, AM_std, n-1, confidence, n)

    l1, = axs[idx1,idx2].loglog(f_smt, AM, label='smt', color='royalblue', linewidth=1)
    f1 = axs[idx1,idx2].fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0,  color='royalblue', alpha=0.5, label=f'CI={confidence}')

    CI = calc_CI(BM, BM_std, n-1, confidence, n)

    l2, = axs[idx1,idx2].loglog(f_sat, BM, label='sat', color='tomato', linewidth=1)
    f2 = axs[idx1,idx2].fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tomato', alpha=0.5, label=f'CI={confidence}')

    CI = calc_CI(CM, CM_std, nr-1, confidence, nr)

    l3, = axs[idx1,idx2].loglog(f_r2b8, CM, label='r2b8', color='tab:green', linewidth=1)
    f3 = axs[idx1,idx2].fill_between(f_r2b8,  CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:green', alpha=0.5, label=f'CI={confidence}')
    
    axs[idx1,idx2].autoscale(enable=True, tight=True)
    axs[idx1,idx2].set_ylim(1e-1, 1e6)

    leg = Legend(axs[idx1,idx2],
    [(l1, f1, f1), (l2, f2, f2),(l3, f3, f3)],
    [f"ICON-SMT ", f"Aviso ", f"ICON-R2B8 "],
    fontsize=15,
    loc="lower left",
    frameon=False,
    #handlelength=3.0,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    axs[idx1,idx2].add_artist(leg) #add artificial legend 

    #axs[idx1,idx2].legend(fontsize=15, loc='lower left')
    axs[idx1,idx2].tick_params(labelsize=15)
    if K >= 4: axs[idx1,idx2].set_xlabel('Wavenumber (1/m)', fontsize=15)
    if is_odd(K)== False: axs[idx1,idx2].set_ylabel(r'Power Spectral Density $K^2/(1/m)$',fontsize=15)
    axs[idx1,idx2].set_title(f'{reg_name}',fontsize=15)

def plot_spatial_spectra_ssh_ci(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, reg_name,  K, axs, horizontal, n, n_minus_flag):
    powerlaw = lambda x, amp, index: amp * (x**index)
    
    if K == 0: idx1, idx2 = 0,0
    elif K == 1: idx1, idx2 = 0,1
    elif K == 2: idx1, idx2 = 1,0
    elif K == 3: idx1, idx2 = 1,1
    elif K == 4: idx1, idx2 = 2,0
    elif K == 5: idx1, idx2 = 2,1
    else:
        raise ValueError('K must be between 0 and 5')

    plot_slopes_ssh(horizontal, f_sat, BM, f_smt, AM, powerlaw, axs, idx1, idx2, REG=True)
    axs[idx1,idx2].legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.5,0.0), frameon=False)

    s     = 0.05
    alpha = 0.8
    for ii in np.arange(n_minus_flag):
        axs[idx1,idx2].scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_sat, BB[ii,:], color='wheat', s=s, alpha=alpha, rasterized=True)

    confidence = 0.95
    n_smt      = n_minus_flag#*int(984/11)
    CI         = calc_CI(AM, AM_std, n_smt-1, confidence, n_smt)

    l1, = axs[idx1,idx2].loglog(f_smt, AM, label='smt', color='royalblue', linewidth=1)
    f1  = axs[idx1,idx2].fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0,  color='royalblue', alpha=0.5, label=f'CI={confidence}')
    n_sat = n#*90
    CI    = calc_CI(BM, BM_std, n_sat-1, confidence, n_sat)

    l2, = axs[idx1,idx2].loglog(f_sat, BM, label='sat', color='tomato', linewidth=1)
    f2  = axs[idx1,idx2].fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tomato', alpha=0.5, label=f'CI={confidence}')

    axs[idx1,idx2].autoscale(enable=True, tight=True)
    axs[idx1,idx2].set_ylim(1e-2, 1e5)

    leg = Legend(axs[idx1,idx2],
    [(l1, f1, f1), (l2, f2, f2)], # type: ignore
    [f"ICON-SMT ", f"Aviso "],
    fontsize=15,
    loc="lower left",
    frameon=False,
    #handlelength=3.0,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    axs[idx1,idx2].add_artist(leg) #add artificial legend 

    #axs[idx1,idx2].legend(fontsize=15, loc='lower left')
    axs[idx1,idx2].tick_params(labelsize=15)
    if K >= 4: axs[idx1,idx2].set_xlabel('Wavenumber (1/m)', fontsize=15)
    if is_odd(K)== False: axs[idx1,idx2].set_ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    axs[idx1,idx2].set_title(f'{reg_name}',fontsize=15)

def plot_spatial_spectra_ssh_ci3(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name,  K, axs, horizontal, n, n_minus_flag):
    powerlaw = lambda x, amp, index: amp * (x**index)
    
    if K == 0: idx1, idx2 = 0,0
    elif K == 1: idx1, idx2 = 0,1
    elif K == 2: idx1, idx2 = 1,0
    elif K == 3: idx1, idx2 = 1,1
    elif K == 4: idx1, idx2 = 2,0
    elif K == 5: idx1, idx2 = 2,1
    else:
        raise ValueError('K must be between 0 and 5')

    plot_slopes_ssh3(horizontal, f_sat, BM, f_smt, AM, f_r2b8, CM, powerlaw, axs, idx1, idx2, REG=True)
    axs[idx1,idx2].legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.5,0.0), frameon=False)

    s     = 0.05
    alpha = 0.8
    for ii in np.arange(n_minus_flag):
        axs[idx1,idx2].scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_sat, BB[ii,:], color='wheat', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_r2b8, CC[ii,:], color='tab:green', s=s, alpha=alpha, rasterized=True)

    confidence = 0.95

    n_smt      = n_minus_flag#*int(984/11)
    CI         = calc_CI(AM, AM_std, n_smt-1, confidence, n_smt)
    l1, = axs[idx1,idx2].loglog(f_smt, AM, label='smt', color='royalblue', linewidth=1)
    f1  = axs[idx1,idx2].fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0,  color='royalblue', alpha=0.5, label=f'CI={confidence}')
    
    n_sat = n
    CI    = calc_CI(BM, BM_std, n_sat-1, confidence, n_sat)
    l2, = axs[idx1,idx2].loglog(f_sat, BM, label='sat', color='tomato', linewidth=1)
    f2  = axs[idx1,idx2].fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tomato', alpha=0.5, label=f'CI={confidence}')

    n_r2b8 = n
    CI     = calc_CI(CM, CM_std, n_r2b8-1, confidence, n_r2b8)
    l3, = axs[idx1,idx2].loglog(f_r2b8, CM, label='r2b8', color='tab:green', linewidth=1)
    f3  = axs[idx1,idx2].fill_between(f_r2b8, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:green', alpha=0.5, label=f'CI={confidence}')


    axs[idx1,idx2].autoscale(enable=True, tight=True)
    axs[idx1,idx2].set_ylim(1e-2, 1e5)

    leg = Legend(axs[idx1,idx2],
    [(l1, f1, f1), (l2, f2, f2), (l3, f3, f3)], # type: ignore
    [f"ICON-SMT ", f"Aviso ", f"ICON-R2B8 "],
    fontsize=15,
    loc="lower left",
    frameon=False,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    axs[idx1,idx2].add_artist(leg) #add artificial legend 

    #axs[idx1,idx2].legend(fontsize=15, loc='lower left')
    axs[idx1,idx2].tick_params(labelsize=15)
    if K >= 4: axs[idx1,idx2].set_xlabel('Wavenumber (1/m)', fontsize=15)
    if is_odd(K)== False: axs[idx1,idx2].set_ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    axs[idx1,idx2].set_title(f'{reg_name}',fontsize=15)


def plot_slopes(horizontal, f_sat, BM, f_r2b8, CM, f_smt, AM, powerlaw, axs, idx1, idx2, REG):
    # shift regressions
    shift = 80

    # slope smt
    if horizontal == True:
       start = 20
       end   = -int(f_smt.size/2)
       xdata = f_smt[start:end]
       ydata = AM[start:end]
    else:
       #xdata = f_smt[15:145]
       #ydata = AM[15:145]
       #welch
       #xdata = f_smt[3:35]
       #ydata = AM[3:35]
       start = 20
       end   = -int(f_smt.size/2)
       xdata = f_smt[start:end]
       ydata = AM[start:end]
    
    logx  = np.log10(xdata)
    logy  = np.log10(ydata)
    res   = stats.linregress(logx, logy)
    index = res.slope
    amp   = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  

    # slope satellite
    if horizontal == True:
        start = 20
        end   = -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata = BM[start:end]
    else:
        #xdata = f_sat[15:]
        #ydata =  BM[15:]
        #welch
        #xdata = f_sat[6:]
        #ydata =  BM[6:]
        start = 20
        end   = -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata = BM[start:end]
    
    logx  = np.log10(xdata)
    logy  = np.log10(ydata)
    res   = stats.linregress(logx, logy)
    index = res.slope
    amp   = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tomato", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tomato", label=f'slope {index:3.3}')  

    # if CM==None: return
    # else:
    # slope r2b8
    if horizontal == True:
        start = 20
        end   = -int(f_r2b8.size/4)
        xdata = f_r2b8[start:end]
        ydata = CM[start:end]
    else:
        #xdata = f_r2b8[15:]
        #ydata = CM[15:]
        #welch
        #xdata = f_r2b8[4:]
        #ydata = CM[4:]
        start = 20
        end   = -int(f_r2b8.size/4)
        xdata = f_r2b8[start:end]
        ydata = CM[start:end]

    logx  = np.log10(xdata)
    logy  = np.log10(ydata)
    res   = stats.linregress(logx, logy)
    index = res.slope
    amp   = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tab:green", label=f'slope {index:3.3}') 
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tab:green", label=f'slope {index:3.3}') 

def plot_slopes_ssh3(horizontal, f_sat, BM, f_smt, AM, f_r2b8, CM, powerlaw, axs, idx1, idx2, REG):
    # shift regressions
    shift = 80
    start = 10

    # slope smt
    if horizontal == True:
        end   = -int(39*f_smt.size/40)
        xdata = f_smt[start:end]
        ydata = AM[start:end]
    else:
        end   = -int(39*f_smt.size/40)
        xdata = f_smt[start-2:end]
        ydata = AM[start-2:end]
    
    logx  = np.log10(xdata)
    logy  = np.log10(ydata)
    res   = stats.linregress(logx, logy)
    index = res.slope
    amp   = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  

    # slope satellite
    if horizontal == True:
        end   = -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata = BM[start:end]
    else:
        end   = -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata = BM[start:end]
    
    logx  = np.log10(xdata)
    logy  = np.log10(ydata)
    res   = stats.linregress(logx, logy)
    index = res.slope
    amp   = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tab:orange", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tab:orange", label=f'slope {index:3.3}')  

    # slope r2b8
    if horizontal == True:
        end   = -int(75)
        xdata = f_r2b8[start:end]
        ydata = CM[start:end]
    else:
        end   = -int(60)
        xdata = f_r2b8[start:end]
        ydata = CM[start:end]
    
    logx  = np.log10(xdata)
    logy  = np.log10(ydata)
    res   = stats.linregress(logx, logy)
    index = res.slope
    amp   = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="tab:green", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="tab:green", label=f'slope {index:3.3}')  


def plot_slopes_ssh(horizontal, f_sat, BM, f_smt, AM, powerlaw, axs, idx1, idx2, REG):
    # shift regressions
    shift = 80
    start = 10
    # slope smt
    if horizontal == True:
        end   = -int(39*f_smt.size/40)
        xdata = f_smt[start:end]
        ydata = AM[start:end]
    else:
        end   = -int(39*f_smt.size/40)
        xdata = f_smt[start-2:end]
        ydata = AM[start-2:end]
    
    logx  = np.log10(xdata)
    logy  = np.log10(ydata)
    res   = stats.linregress(logx, logy)
    index = res.slope
    amp   = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  

    # slope satellite
    if horizontal == True:
        end   = -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata = BM[start:end]
    else:
        end   = -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata = BM[start:end]
    
    logx  = np.log10(xdata)
    logy  = np.log10(ydata)
    res   = stats.linregress(logx, logy)
    index = res.slope
    amp   = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tomato", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tomato", label=f'slope {index:3.3}')  


def plot_spatial_spectra6(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, reg_name,  K, axs, horizontal, n):
    powerlaw = lambda x, amp, index: amp * (x**index)
    
    if K == 0: idx1, idx2 = 0,0
    elif K == 1: idx1, idx2 = 0,1
    elif K == 2: idx1, idx2 = 1,0
    elif K == 3: idx1, idx2 = 1,1
    elif K == 4: idx1, idx2 = 2,0
    elif K == 5: idx1, idx2 = 2,1
    else:
        raise ValueError('K must be between 0 and 5')
 
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_smt, AA[ii,:], color='lightgrey')
    
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_sat, BB[ii,:], color='wheat')

    #axs[idx1,idx2].loglog(f_smt, AM+AM_std, color='royalblue', linestyle='--',alpha=0.5)
    axs[idx1,idx2].loglog(f_smt, AM, label='smt_ssh_mean', color='royalblue', linewidth=2)
    #axs[idx1,idx2].fill_between(f_smt, AM, AM+AM_std, color='royalblue', alpha=0.25)
    #axs[idx1,idx2].loglog(f_smt, AM-AM_std, linestyle='--',  color='royalblue', alpha=0.5)
    #axs[idx1,idx2].fill_between(f_smt, AM, AM-AM_std, color='royalblue', alpha=0.25)

    #axs[idx1,idx2].loglog(f_sat, BM+BM_std, color='tomato', linestyle='--', alpha=0.5)
    axs[idx1,idx2].loglog(f_sat, BM, label='sat_adt_mean', color='tomato', linewidth=2)
    #axs[idx1,idx2].fill_between(f_sat, BM, BM+BM_std, color='wheat', alpha=0.5)
    #axs[idx1,idx2].loglog(f_sat, BM-BM_std, linestyle='--',  color='tomato', alpha=0.5)
    #axs[idx1,idx2].fill_between(f_sat, BM, BM-BM_std, color='wheat', alpha=0.5)

    #####################
    fx = np.linspace(1e-4,5e-4,100)
    if horizontal == True: 
        pp = 10000
        y2 = pp* 3e-9*np.power(fx, (-2))
        y3 = pp* 6e-13*np.power(fx, (-3))
        y5 = pp* 1e-6*np.power(fx, (-5/3))
    else:
        pp = 1000
        y2 = pp* 3e-5*np.power(fx, (-2))
        y3 = pp* 1e-10*np.power(fx, (-3))
        y5 = pp* 1e-3*np.power(fx, (-5/3))

    axs[idx1,idx2].loglog(fx,y3, label='slope -3',)
    axs[idx1,idx2].loglog(fx,y5, label='slope -5/3 = 1.67')

    # slope satellite
    if horizontal == True: 
        xdata = f_sat[20:]
        ydata =  BM[20:]
    else:
        xdata = f_sat[15:]
        ydata =  BM[15:]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    axs[idx1,idx2].plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="red")  
    axs[idx1,idx2].plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="red", label=f'slope {index:3.3}') 


    # slope smt
    if horizontal == True: 
        xdata = f_smt[300:]
        ydata = AM[300:]
    else:
        xdata = f_smt[100::600]
        ydata = AM[100::600]

    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    axs[idx1,idx2].plot(xdata, powerlaw(xdata, amp, index),linewidth=3, linestyle='dashed', color="gold")  
    axs[idx1,idx2].plot(fx, powerlaw(fx, pp*amp, index), linestyle='dashed', color="gold", label=f'slope {index:3.3}') 

    # slope smt
    if horizontal == True:
        xdata = f_smt[20:300]
        ydata = AM[20:300]
    else:
        xdata = f_smt[15:145]
        ydata = AM[15:145]
    
    logx = np.log10(xdata)
    logy = np.log10(ydata)
    res = stats.linregress(logx, logy)
    index = res.slope
    amp = 10**res.intercept
    axs[idx1,idx2].plot(xdata, powerlaw(xdata, amp, index), linewidth=4, linestyle='--', color="aqua")  
    axs[idx1,idx2].plot(fx, powerlaw(fx, pp*amp, index), linewidth=2, linestyle='--', color="aqua", label=f'slope {index:3.3}') 


    axs[idx1,idx2].autoscale(enable=True, tight=True)
    axs[idx1,idx2].set_ylim(1e-1, 1e6)

    axs[idx1,idx2].legend(fontsize=15, loc='lower left')
    axs[idx1,idx2].tick_params(labelsize=15)
    if K >= 4: axs[idx1,idx2].set_xlabel('Wavenumber (1/m)', fontsize=15)
    if is_odd(K)== False: axs[idx1,idx2].set_ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    axs[idx1,idx2].set_title(f'{reg_name}',fontsize=15)

def is_odd(num):
    return num & 0x1

#horizontal for ssh OLD
def get_lon_lat_horizontal(k,n):
    #n = 10
    #lon1 = -49, - 25
    lon1 = -70, - 48
    #if k > 2: lon1 = -75, -51; k=k-3
    lati = 25.8, 26.2
    lati = lati + np.ones(2)*3*k
    Lat = np.zeros((n, 2))
    Lon = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lati
        Lon[ii,:] = lon1
        lati += np.ones(2)*0.25 #0.5
    return(Lat, Lon)

#horizontal for sst
def get_lon_lat_horizontal_sst(k, n):
    lon1 = -72.5, - 56
    dlat = 0.04166412
    y1 = 28.98
    lati = y1, y1+dlat
    #lati = 25.8, 26.2
    lati = lati + np.ones(2)*1.7*k
    Lat = np.zeros((n, 2))
    Lon = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lati
        Lon[ii,:] = lon1
        lati += np.ones(2)*0.05 # slightly larger then grid spacing
    return(Lat, Lon)

def get_lon_lat_horizontal_r2b8(k, n):
    lon1 = -72.5, - 56
    dlat = 0.1
    y1 = 28.98
    lati = y1, y1+dlat
    lati = lati + np.ones(2)*1.7*k
    Lat = np.zeros((n, 2))
    Lon = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lati
        Lon[ii,:] = lon1
        lati += np.ones(2)*0.1 # slightly larger then grid spacing
    return(Lat, Lon)

#old!
def get_lon_lat_vertical(k, n):
    #n = 10
    lat1 = 25, 43
    loni = -68.2, -67.8
    loni = loni + np.ones(2)*3*k
    Lat = np.zeros((n, 2))
    Lon = np.zeros((n, 2))
    for ii in np.arange(n):
        Lon[ii,:] = loni
        Lat[ii,:] = lat1
        loni += np.ones(2)*0.25 #0.5
    return(Lat, Lon)

#used for smt and satellite
def get_lon_lat_vertical_sst(k,n):
    lat1 = 28, 40
    dlon = 0.04166412
    y1   = -68
    loni = y1, y1+dlon
    loni = loni + np.ones(2)*1.7*k
    Lat  = np.zeros((n, 2))
    Lon  = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lat1
        Lon[ii,:] = loni
        loni += np.ones(2)*0.05 # slightly larger then grid spacing
    return(Lat, Lon)

def get_lon_lat_vertical_sst_r2b8(k,n):
    lat1 = 28, 40
    dlon = 0.1
    y1   = -68
    loni = y1, y1+dlon
    loni = loni + np.ones(2)*1.7*k
    Lat  = np.zeros((n, 2))
    Lon  = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lat1
        Lon[ii,:] = loni
        loni += np.ones(2)*0.1 # slightly larger then grid spacing
    return(Lat, Lon)

def get_lon_lat_vertical_sst_r2b8_exp2(k,n):
    lat1 = 31, 37
    dlon = 0.1
    y1   = -70
    loni = y1, y1+dlon
    loni = loni + np.ones(2)*3.3*k
    Lat  = np.zeros((n, 2))
    Lon  = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lat1
        Lon[ii,:] = loni
        loni += np.ones(2)*0.1 # slightly larger then grid spacing
    return(Lat, Lon)

def get_lon_lat_vertical_sst_exp2(k,n):
    lat1 = 31, 37
    dlon = 0.04166412
    y1   = -70
    loni = y1, y1+dlon
    loni = loni + np.ones(2)*3.3*k
    Lat  = np.zeros((n, 2))
    Lon  = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lat1
        Lon[ii,:] = loni
        loni += np.ones(2)*0.05 # slightly larger then grid spacing
    return(Lat, Lon)   


def plot_regions_horizontal(smt, S, n, regions, parent_imag, savefig):
    fpath_tgrid = '/work/mh0033/from_Mistral/mh0033/m300602/icon/grids/smt/smt_tgrid.nc'
    gg = xr.open_dataset(fpath_tgrid)
    res = np.sqrt(gg.cell_area_p)
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'

    ddnpz = np.load(fpath_ckdtree)
    lon_010deg = ddnpz['lon']
    lat_010deg = ddnpz['lat']
    res_010deg = pyic.apply_ckdtree(res, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_010deg.size, lon_010deg.size)
    res_010deg[res_010deg==0.] = np.ma.masked

    # plot region
    lon_reg = [-90, -20]
    lat_reg = [20, 60]
    #regions = 'A','B','C','D','E','F'
    #n = 10
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    lon, lat, data = pyic.interp_to_rectgrid(smt.isel(time=50), fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    pyic.shade(lon, lat, data, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False)
    levels = 500 + np.arange(11)*100
    CS = ax.contour(lon_010deg, lat_010deg, res_010deg,  levels=levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)

    for k in range(S):
        #print(k)
        Lat, Lon = get_lon_lat_horizontal_sst(k, n) #TODO wrong name for regions
        #print(Lat)
        for ii in np.arange(n):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=0.3)

        loc = Lon[ii,0]+(Lon[ii,1]-Lon[ii,0])/2, Lat[5,0]
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         #arrowprops=dict(arrowstyle="->", facecolor='black', linewidth=2, mutation_scale=30),
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)

    ax.set_title(f'Visualization sections', fontsize=15)
    #ax.set_xlabel('lon')
    #ax.set_ylabel('lat')
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig == True: plt.savefig(f'{parent_imag}vis_reg', bbox_inches='tight')   

def plot_regions_horizontal_satellite(sata, S, n, regions, parent_imag, savefig):
    lon_reg = [-90, -10]
    lat_reg = [15, 60]
    clim = 0, 24

    sst_sat = sata.where((sata.lat > lat_reg[0]) & (sata.lat < lat_reg[1]) & (sata.lon > lon_reg[0]) & (sata.lon < lon_reg[1]), drop=True)

    t=79
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=2, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(sst_sat.lon, sst_sat.lat, sst_sat.isel(time=t).sst_night, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False)
    # add colorbar units 
    ax.set_title('SST [°C]')

    for k in range(S):
        Lat, Lon = get_lon_lat_horizontal_sst(k, n) # TODO:wrong name for regions
        for ii in np.arange(n):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=0.1)

        # loc = Lon[ii,0]+(Lon[ii,1]-Lon[ii,0])/2, Lat[5,0]
        # ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
        #     xytext=(loc), textcoords='data',
        #     fontsize=8,         
        #     ha='center', va='center', 
        #     bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
        #     )

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))


    if savefig == True: plt.savefig(f'{parent_imag}vis_reg_sat_2', bbox_inches='tight') 

def plot_regions_vertical_satellite(sata, S, n, regions, parent_imag, savefig):
    lon_reg = [-90, -20]
    lat_reg = [20, 60]
    clim = 0, 24

    sst_sat = sata.where((sata.lat > lat_reg[0]) & (sata.lat < lat_reg[1]) & (sata.lon > lon_reg[0]) & (sata.lon < lon_reg[1]), drop=True)

    t=79
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(sst_sat.lon, sst_sat.lat, sst_sat.isel(time=t).sst_night, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False)

    for k in range(S):
        Lat, Lon = get_lon_lat_vertical_sst(k, n)
        for ii in np.arange(n):
            ax.plot((Lon[ii,0],Lon[ii,0]), (Lat[ii,1], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=0.3)

        loc = Lon[5,0], (Lat[0,0] + (Lat[0,1]-Lat[0,0])/2)
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig == True: plt.savefig(f'{parent_imag}vis_reg_sat', bbox_inches='tight') 

def plot_regions_horizontal_r2b8(data, S, n, regions, parent_imag, savefig):
    data          = data.isel(time=79)
    lon_reg       = [-90, -20]
    lat_reg       = [20, 60]
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b8_oce_r0004/ckdtree/rectgrids/r2b8_oce_r0004_res0.10_180W-180E_90S-90N.nc'

    r2b8_inter = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=5, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(r2b8_inter.lon, r2b8_inter.lat, r2b8_inter, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False)

    for k in range(S):
        Lat, Lon = get_lon_lat_horizontal_r2b8(k, n)
        for ii in np.arange(n):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=1)

        loc = Lon[ii,0]+(Lon[ii,1]-Lon[ii,0])/2, Lat[5,0]
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig==True: plt.savefig(f'{parent_imag}vis_reg_r2b8_n3w.png', bbox_inches='tight', format='png') 


def plot_regions_vertical_r2b8(data, S, n, regions, parent_imag, savefig):
    data = data.isel(time=79)
    lon_reg = [-90, -20]
    lat_reg = [20, 60]
    clim = 0, 24
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b8_oce_r0004/ckdtree/rectgrids/r2b8_oce_r0004_res0.10_180W-180E_90S-90N.nc'

    r2b8_inter = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=5, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(r2b8_inter.lon, r2b8_inter.lat, r2b8_inter, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False)

    for k in range(S):
        Lat, Lon = get_lon_lat_vertical_sst_r2b8(k, n)
        for ii in np.arange(n):
            ax.plot((Lon[ii,0],Lon[ii,0]), (Lat[ii,1], Lat[ii,0]), transform=ccrs_proj, color='darkviolet', linewidth=1)

        loc = Lon[5,0], (Lat[0,0] + (Lat[0,1]-Lat[0,0])/2)
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=15,         
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig==True: plt.savefig(f'{parent_imag}vis_reg_r2b8_n3w.png', bbox_inches='tight', format='png') 

def plot_regions_vertical(smt, S, n, regions, parent_imag, savefig):
    fpath_tgrid = '/work/mh0033/from_Mistral/mh0033/m300602/icon/grids/smt/smt_tgrid.nc'
    gg = xr.open_dataset(fpath_tgrid)
    res = np.sqrt(gg.cell_area_p)
    fpath_ckdtree = '/work/mh0033/from_Mistral/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'

    ddnpz = np.load(fpath_ckdtree)
    lon_010deg = ddnpz['lon']
    lat_010deg = ddnpz['lat']
    res_010deg = pyic.apply_ckdtree(res, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_010deg.size, lon_010deg.size)
    res_010deg[res_010deg==0.] = np.ma.masked

    # plot region
    lon_reg = [-90, -20]
    lat_reg = [20, 60]

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    lon, lat, data = pyic.interp_to_rectgrid(smt.isel(time=50), fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    pyic.shade(lon, lat, data, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False)
    levels = 500 + np.arange(11)*100
    CS = ax.contour(lon_010deg, lat_010deg, res_010deg,  levels=levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)

    for k in range(S):
        Lat, Lon = get_lon_lat_vertical_sst(k, n)
        for ii in np.arange(n):
            ax.plot((Lon[ii,0],Lon[ii,0]), (Lat[ii,0], Lat[ii,1]), transform=ccrs_proj, color='darkviolet', linewidth=0.3)

        loc = Lon[5,0], (Lat[0,0] + (Lat[0,1]-Lat[0,0])/2)
        ax.annotate(f'{regions[k]}', xy=loc,  xycoords='data',
            xytext=(loc), textcoords='data',
            fontsize=20,        
            ha='center', va='center', 
            bbox=dict(ec='none', fc='w', alpha=0.8, boxstyle='square,pad=0.')
            )

    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)

    ax.set_title(f'Visualization sections', fontsize=15)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig == True: plt.savefig(f'{parent_imag}vis_reg2', bbox_inches='tight')      

def load_satellite_sst():
    #satellite
    #TODO change to ICDC aviso ssh folder
    #path_data  = '/pool/data/ICDC/ocean/modis_aqua_sst/DATA/daily/2010/'
    path_data  = '/work/mh0033/m300878/observation/satellite/sst/2010/'
    search_str = f'MODIS-AQUA__C6__SST_v2019.0__4km__2010*.nc' 

    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(ds)


def load_satellite_monthly_sst():
    path_data  = '/pool/data/ICDC/ocean/modis_aqua_sst/DATA/monthly/2010/'
    search_str = f'MODIS-AQUA__C6__SST_v2019.0__4km__2010*.nc' 

    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(ds)

def load_r2b8_sst():
    # data of R2B8 run of Mia, TKE mixing scheme  like smt model, diffrent spinup
    path_data  = '/work/mh0033/m211054/projects/mia/icon-oes-1.3.01_mia/experiments/exp.ocean_era51h_r2b8_msp22063-ERA/outdata/'
    search_str = f'exp.ocean_era51h_r2b8_msp22063-ERA_3du_P1D_2010*' 
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()
    r2b8 = xr.open_mfdataset(flist[:89], combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    r2b8 = r2b8.rename(ncells_2 = 'ncells')
    return(r2b8.isel(depth=0).to)

def load_r2b8_sst_mep():
    # data of R2B8 run of Mia, TKE mixing scheme  like smt model, diffrent spinup
    path_data  = '/work/mh0033/m300878/run_icon/icon-oes-1.3.01_mia/experiments/exp.ocean_era51h_r2b8_mep23214-ERA/outdata/'
    search_str = f'exp.ocean_era51h_r2b8_mep23214-ERA_PT1H_to*' 
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()
    r2b8 = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(r2b8.to)

def interpolate_to_rect_grid_r2b8(r2b8):
    print('interpolate to rectangular grid')
    lon_reg = [-80, -40]
    lat_reg = [25, 40]
    #fpath_ckdtree = '/work/mh0033/from_Mistral/mh0033/m300602/icon/grids/r2b8_oce_r0004/ckdtree/rectgrids/r2b8_oce_r0004_res0.10_180W-180E_90S-90N.nc'
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b8_oce_r0004/ckdtree/rectgrids/r2b8_oce_r0004_res0.10_180W-180E_90S-90N.nc'
    r2b8_ = pyic.interp_to_rectgrid_xr(r2b8, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) 
    return(r2b8_)


### Load smt Data
def load_smt_w():
    search_str = f'pp_calc_w_*.nc' 
    path_data = '/work/mh0033/from_Mistral/mh0033/u241317/smt/w/'
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort() 
    path_dat2 = '/work/mh0033/m300878/smt/pp_calc_w2_2010-03-22T21:00:00.nc' #TODO: last timestep is hampered and was recalculated
    flist2 = np.array(glob.glob(path_dat2))
    flist = [*flist1[:-1], *flist2]
    
    data = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depthi=1))
    times = data.time.size
    time0 = np.datetime64('2010-03-15T21:00:00')
    dti  = pd.date_range(time0, periods=times, freq="2h")
    data = data.assign_coords(time=dti)
    data = data.rename({'cc': 'ncells'}) 
    return(data)

def load_smt_b():
    search_str = f'pp_calc_b_*.nc' 
    path_data = '/work/mh0033/from_Mistral/mh0033/u241317/smt/db/'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    data = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depthi=1))
    times = data.time.size
    time0 = np.datetime64('2010-03-15T21:00:00')
    dti = pd.date_range(time0, periods=times, freq="2h")
    data = data.assign_coords(time=dti)
    data = data.rename({'cc': 'ncells'}) 
    return(data)

def load_smt_N2():
    search_str = f'pp_calc_N2_*.nc' 
    path_data = '/work/mh0033/from_Mistral/mh0033/u241317/smt/N2/'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    data = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1, depthi=1))
    times = data.time.size
    time0 = np.datetime64('2010-03-15T21:00:00')
    dti = pd.date_range(time0, periods=times, freq="2h")
    data = data.assign_coords(time=dti)
    data = data.rename({'cc': 'ncells'}) 
    return(data)

def load_smt_sst():
    #smt model data
    run      = 'ngSMT_tke'
    gname = 'smt'

    #search_str = f'_h_sp_*.nc' 
    search_str = f'_T_S_sp_001-016_*.nc' 
    month = '01'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month = '02'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist2      = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month = '03'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist3      = np.array(glob.glob(path_data+search_str))
    flist3.sort()

    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    time0 = np.datetime64('2010-01-09T01:00:00')
    dti = pd.date_range(time0, periods=984, freq="2h")
    smt = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    smt = smt.assign_coords(time=dti)
    smt = smt.T001_sp
    return(smt)

def load_smt_T():
    layers  =   'sp_001-016' , 'sp_017-032',  'sp_033-048',  'sp_049-064',  'sp_065-080',  'sp_081-096',  'sp_097-112'
    tt = 'a','b','c','d','e','f','g'
    run      = 'ngSMT_tke'
    gname = 'smt'

    ii = 0
    for ll in enumerate(layers):
        ii += 1
        month = '01'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        search_str = f'_T_S_{ll[1]}_2010*.nc' 
        flist1      = np.array(glob.glob(path_data+search_str))
        flist1.sort()
        month = '02'
        #path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        flist2      = np.array(glob.glob(path_data+search_str))
        flist2.sort()
        month = '03'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        flist3      = np.array(glob.glob(path_data+search_str))
        flist3.sort()

        flist = [*flist1, *flist2, *flist3]
        flist.sort()

        globals()[f"{tt[ii-1]}"] = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))

    to = xr.merge([a,b,c,d,e,f,g])

    #select only temperature 
    Tlist = np.empty(depthc.size, dtype='object')
    for dd in range(depthc.size):
            level = "{0:03}".format(dd+1) 
            Tlist[dd]= f'T{level}_sp'

    time0 = np.datetime64('2010-01-09T01:00:00')
    dti = pd.date_range(time0, periods=984, freq="2h")
    to = to.assign_coords(time=dti)

    ls = [   to[Tlist[0]],  to[Tlist[1]],  to[Tlist[2]],  to[Tlist[3]],  to[Tlist[4]],  to[Tlist[5]],  to[Tlist[6]],  to[Tlist[7]],  to[Tlist[8]],  to[Tlist[9]],
            to[Tlist[10]], to[Tlist[11]], to[Tlist[12]], to[Tlist[13]], to[Tlist[14]], to[Tlist[15]], to[Tlist[16]], to[Tlist[17]], to[Tlist[18]], to[Tlist[19]],
            to[Tlist[20]], to[Tlist[21]], to[Tlist[22]], to[Tlist[23]], to[Tlist[24]], to[Tlist[25]], to[Tlist[26]], to[Tlist[27]], to[Tlist[28]], to[Tlist[29]],
            to[Tlist[30]], to[Tlist[31]], to[Tlist[32]], to[Tlist[33]], to[Tlist[34]], to[Tlist[35]], to[Tlist[36]], to[Tlist[37]], to[Tlist[38]], to[Tlist[39]],
            to[Tlist[40]], to[Tlist[41]], to[Tlist[42]], to[Tlist[43]], to[Tlist[44]], to[Tlist[45]], to[Tlist[46]], to[Tlist[47]], to[Tlist[48]], to[Tlist[49]],
            to[Tlist[50]], to[Tlist[51]], to[Tlist[52]], to[Tlist[53]], to[Tlist[54]], to[Tlist[55]], to[Tlist[56]], to[Tlist[57]], to[Tlist[58]], to[Tlist[59]],
            to[Tlist[60]], to[Tlist[61]], to[Tlist[62]], to[Tlist[63]], to[Tlist[64]], to[Tlist[65]], to[Tlist[66]], to[Tlist[67]], to[Tlist[68]], to[Tlist[69]],
            to[Tlist[70]], to[Tlist[71]], to[Tlist[72]], to[Tlist[73]], to[Tlist[74]], to[Tlist[75]], to[Tlist[76]], to[Tlist[77]], to[Tlist[78]], to[Tlist[79]],
            to[Tlist[80]], to[Tlist[81]], to[Tlist[82]], to[Tlist[83]], to[Tlist[84]], to[Tlist[85]], to[Tlist[86]], to[Tlist[87]], to[Tlist[88]], to[Tlist[89]],
            to[Tlist[90]], to[Tlist[91]], to[Tlist[92]], to[Tlist[93]], to[Tlist[94]], to[Tlist[95]], to[Tlist[96]], to[Tlist[97]], to[Tlist[98]], to[Tlist[99]],
            to[Tlist[100]], to[Tlist[101]], to[Tlist[102]], to[Tlist[103]], to[Tlist[104]], to[Tlist[105]], to[Tlist[106]], to[Tlist[107]], to[Tlist[108]], to[Tlist[109]],
            to[Tlist[110]], to[Tlist[111]], 
     ]
    TO = xr.concat( ls, dim="depthc")
    TO = TO.assign_coords(depthc=depthc)

    ####### String of smt t_s data names
    # Slist = np.empty(depthc.size, dtype='object')
    # for d in range(depthc.size):
    #     level = "{0:03}".format(d+1) 
    #     Slist[d]= f'S{level}_sp'
    # # list on how data is stored
    # # list built for mixed layer depth analysis
    # lis = np.empty(2*depthc.size, dtype='object')#
    # k=0
    # for ii in range(112):
    #     lis[ii+k]   = Tlist[ii]
    #     lis[ii+1+k] = Slist[ii]
    #     k += 1
    # lis

    return TO  #to[tList]

def load_smt_T_calc(): #function not necessary! same as stored temperature - however data is stored in different shape TODO!
    search_str = f'pp_calc_T_period*.nc' 
    path_data = '/work/mh0033/from_Mistral/mh0033/u241317/smt/T/'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()

    data = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    times = data.time.size
    time0 = np.datetime64('2010-03-15T21:00:00')
    dti = pd.date_range(time0, periods=times, freq="2h")
    data = data.assign_coords(time=dti)
    data = data.rename({'temperature': 'ncells'}) 
    return(data)

def load_smt_S():
    layers  =   'sp_001-016' , 'sp_017-032',  'sp_033-048',  'sp_049-064',  'sp_065-080',  'sp_081-096',  'sp_097-112'
    tt = 'a','b','c','d','e','f','g'
    run      = 'ngSMT_tke'

    ii = 0
    for ll in enumerate(layers):
        ii += 1
        month = '01'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        search_str = f'_T_S_{ll[1]}_2010*.nc' 
        flist1      = np.array(glob.glob(path_data+search_str))
        flist1.sort()
        month = '02'
        #path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        flist2      = np.array(glob.glob(path_data+search_str))
        flist2.sort()
        month = '03'
        path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
        flist3      = np.array(glob.glob(path_data+search_str))
        flist3.sort()

        flist = [*flist1, *flist2, *flist3]
        flist.sort()

        globals()[f"{tt[ii-1]}"] = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))

    so = xr.merge([a,b,c,d,e,f,g])

    #select only salinity
    Slist = np.empty(depthc.size, dtype='object')
    for dd in range(depthc.size):
        level = "{0:03}".format(dd+1) 
        Slist[dd]= f'S{level}_sp'

    time0 = np.datetime64('2010-01-09T01:00:00')
    dti = pd.date_range(time0, periods=984, freq="2h")
    so = so.assign_coords(time=dti)

    ls = [   so[Slist[0]],  so[Slist[1]],  so[Slist[2]],  so[Slist[3]],  so[Slist[4]],  so[Slist[5]],  so[Slist[6]],  so[Slist[7]],  so[Slist[8]],  so[Slist[9]],
            so[Slist[10]], so[Slist[11]], so[Slist[12]], so[Slist[13]], so[Slist[14]], so[Slist[15]], so[Slist[16]], so[Slist[17]], so[Slist[18]], so[Slist[19]],
            so[Slist[20]], so[Slist[21]], so[Slist[22]], so[Slist[23]], so[Slist[24]], so[Slist[25]], so[Slist[26]], so[Slist[27]], so[Slist[28]], so[Slist[29]],
            so[Slist[30]], so[Slist[31]], so[Slist[32]], so[Slist[33]], so[Slist[34]], so[Slist[35]], so[Slist[36]], so[Slist[37]], so[Slist[38]], so[Slist[39]],
            so[Slist[40]], so[Slist[41]], so[Slist[42]], so[Slist[43]], so[Slist[44]], so[Slist[45]], so[Slist[46]], so[Slist[47]], so[Slist[48]], so[Slist[49]],
            so[Slist[50]], so[Slist[51]], so[Slist[52]], so[Slist[53]], so[Slist[54]], so[Slist[55]], so[Slist[56]], so[Slist[57]], so[Slist[58]], so[Slist[59]],
            so[Slist[60]], so[Slist[61]], so[Slist[62]], so[Slist[63]], so[Slist[64]], so[Slist[65]], so[Slist[66]], so[Slist[67]], so[Slist[68]], so[Slist[69]],
            so[Slist[70]], so[Slist[71]], so[Slist[72]], so[Slist[73]], so[Slist[74]], so[Slist[75]], so[Slist[76]], so[Slist[77]], so[Slist[78]], so[Slist[79]],
            so[Slist[80]], so[Slist[81]], so[Slist[82]], so[Slist[83]], so[Slist[84]], so[Slist[85]], so[Slist[86]], so[Slist[87]], so[Slist[88]], so[Slist[89]],
            so[Slist[90]], so[Slist[91]], so[Slist[92]], so[Slist[93]], so[Slist[94]], so[Slist[95]], so[Slist[96]], so[Slist[97]], so[Slist[98]], so[Slist[99]],
            so[Slist[100]], so[Slist[101]], so[Slist[102]], so[Slist[103]], so[Slist[104]], so[Slist[105]], so[Slist[106]], so[Slist[107]], so[Slist[108]], so[Slist[109]],
            so[Slist[110]], so[Slist[111]], 
     ]
    SO = xr.concat( ls, dim="depthc")
    SO = SO.assign_coords(depthc=depthc)

    return SO #so[Slist]


def load_satellite(): #old
    #satellite
    #TODO change to ICDC aviso ssh folder ---- see below
    search_str = f'dt_global_twosat_phy_l4_*.nc' 
    month = '01'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    path_data = f'/work/mh0033/from_Mistral/mh0033/u241317/satellite/ssh/ssh_sat_temp/{month}/'
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month = '02'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    path_data = f'/work/mh0033/from_Mistral/mh0033/u241317/satellite/ssh/ssh_sat_temp/{month}/'
    flist2      = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month = '03'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    path_data = f'/work/mh0033/from_Mistral/mh0033/u241317/satellite/ssh/ssh_sat_temp/{month}/'
    flist3      = np.array(glob.glob(path_data+search_str))
    flist3.sort()

    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(ds)

def load_aviso_aqua(): #updated
    path_data = '/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/01/'
    month = '01'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    search_str = f'dt_global_twosat_phy_l4_*.nc' 
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month = '02'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    search_str = f'dt_global_twosat_phy_l4_*.nc' 
    flist2      = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month = '03'
    path_data = f'/pool/data/ICDC/ocean/aviso_ssh/DATA/2010/{month}/'
    search_str = f'dt_global_twosat_phy_l4_*.nc' 
    flist3      = np.array(glob.glob(path_data+search_str))
    flist3.sort()
    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    return(ds)

def load_smt():
    #smt model data
    run      = 'ngSMT_tke'
    gname = 'smt'

    search_str = f'_h_sp_*.nc' 
    month = '01'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist1      = np.array(glob.glob(path_data+search_str))
    flist1.sort()
    month = '02'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist2      = np.array(glob.glob(path_data+search_str))
    flist2.sort()
    month = '03'
    path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    path_data = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/2010-{month}/{run}'
    flist3      = np.array(glob.glob(path_data+search_str))
    flist3.sort()

    flist = [*flist1, *flist2, *flist3]
    flist.sort()

    time0 = np.datetime64('2010-01-09T01:00:00')
    dti = pd.date_range(time0, periods=984, freq="2h")
    smt = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    smt = smt.assign_coords(time=dti)
    return(smt)


def mconf(K,gamma,str):
    """
    Compute symmetric confidence intervals for multitaper spectral estimation.
    
    Args: 
        K: Number of tapers used in the spectral estimate, normally 2*P-1
        gamma: confidence level, e.g., 0.95
        str: 'lin' to return confidence intervals for linear axis 
             'log' to return confidence intervals for log10 axis 
    
    Returns:
        ra,rb:  Ratio factors for confidence interval
        
    If S0 is the true value of the spectrum and S is the spectral estimate,
    then the confindence interval is defined such that 
    
        Probability that ra < S/S0 < ra = gamma     (linear case)
        Probability that ra < log10(S)/log10(S0) < ra = gamma (log10 case)

    The confidence interval will be S*ra to S*rb for the spectral values
    in linear space, or S*10^ra to S*10^rb in log10 space.  If log10(S) 
    is plotted rather than S with a logarithmic axis, the latter would 
    become log10(S)+ra to log10(S)*rb.    
    """

    dx=0.0001
    #Compute pdf symmetrically about unity

    if str=='lin':
    
        x1=np.arange(1-dx/2,-1,step=-dx)*2*K  #from one to minus one
        x2=np.arange(1+dx/2,3,step=dx)*2*K #from 1 to three 
        fx=(chi2.pdf(x1,2*K)+chi2.pdf(x2,2*K))*2*K
        sumfx=np.cumsum(fx)*dx

        ii=np.where(sumfx>=gamma)[0][0]
        ra=x1[ii]/2/K
        rb=x1[ii]/2/K
    
    elif str=='log':
        
        xo=np.log(2)+np.log(1/2/K)+digamma(K) #see pav15-arxiv
        c=np.log(10)
        xo=xo/c  #change of base rule
    
        x1=np.power(10,np.arange(xo-dx/2,xo-2,step=-dx))   
        x2=np.power(10,np.arange(xo+dx/2,xo+2,step=dx))  
    
        fx1=c*2*K*np.multiply(x1,chi2.pdf(2*K*x1,2*K))
        fx2=c*2*K*np.multiply(x2,chi2.pdf(2*K*x2,2*K))

        sumfx=np.cumsum(fx1+fx2)*dx
    
        ii=np.where(sumfx>=gamma)[0][0]
        ra=np.log10(x1[ii])
        rb=np.log10(x2[ii])
    
    return ra,rb    