#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j
#SBATCH --nodes=1
#SBATCH --partition=compute
#SBATCH --mem=256Gb
#SBATCH --time=03:30:00 
#SBATCH --exclusive 
#SBATCH --account=mh0033
#SBATCH --mail-type=FAIL 

module list

source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`
python -u sst_wave.py --slurm
enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

