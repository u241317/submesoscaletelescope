# %%
import sys
sys.path.insert(0, "../../")
import numpy as np
from netCDF4 import Dataset, num2date # type: ignore
import pyicon as pyic
import glob
# import pickle
# import datetime
import xarray as xr
import pandas as pd
import seawater as sw
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.all_funcs as eva
# %%

ts = pyic.timing([0], 'start')

print('Run calculation of buoyancy')

### configure paths
run      = 'ngSMT_tke'
savefig  = False
path_fig = '../pics/'
nnf      = 0
gname    = 'smt'
lev      = 'L128'

#####################################################################
### precalculation of coefficients
levs   = np.arange(depthc.size)
nz     = levs.size
cc     = int(59799625)
nzi    = int(nz+1)
g      = 9.80665
rho0   = 1025.022

def get_grid_variables():
    grid_sphere_radius = 6.371229e6
    # fpath_tgrid           = '/home/mpim/m300602/work/icon/grids/smt/smt_tgrid.nc'
    fpath_tgrid           = '/work/mh0033/m300602/icon/grids/smt/smt_tgrid.nc'
    f                     = Dataset(fpath_tgrid, 'r')
    clon                  = f.variables['clon'][:] * 180./np.pi # center longitude
    clat                  = f.variables['clat'][:] * 180./np.pi # center latitude
    # vlon                  = f.variables['vlon'][:] * 180./np.pi # vertex longitude
    # vlat                  = f.variables['vlat'][:] * 180./np.pi # vertex latitude
    # vertex_of_cell        = f.variables['vertex_of_cell'][:].transpose()-1 # vertices of each cellcells ad
    edge_of_cell          = f.variables['edge_of_cell'][:].transpose()-1 # edges of each cellvertices
    # edges_of_vertex       = f.variables['edges_of_vertex'][:].transpose()-1 # edges around vertex
    dual_edge_length      = f.variables['dual_edge_length'][:] # lengths of dual edges (distances between triangul 
    # edge_orientation      = f.variables['edge_orientation'][:].transpose()
    edge_length           = f.variables['edge_length'][:] # lengths of edges of triangular cells
    # cell_area_p           = f.variables['cell_area_p'][:] # area of grid cell
    # dual_area             = f.variables['dual_area'][:] # areas of dual hexagonal/pentagonal cells
    #edge_length          = f.variables['edge_length'][:]
    adjacent_cell_of_edge = f.variables['adjacent_cell_of_edge'][:].transpose()-1 # cells adjacent to each edge
    orientation_of_normal = f.variables['orientation_of_normal'][:].transpose() # orientations of normals to triangular cell edges
    # edge_vertices         = f.variables['edge_vertices'][:].transpose()-1 #vertices at the end of of each edge
    f.close()

    dtype = 'float32'
    f                  = Dataset(fpath_tgrid, 'r')
    elon               = f.variables['elon'][:] * 180./np.pi #edge midpoint longitude
    # elat               = f.variables['elat'][:] * 180./np.pi #edge midpoint latitude
    cell_cart_vec      = np.ma.zeros((clon.size,3), dtype=dtype)
    cell_cart_vec[:,0] = f.variables['cell_circumcenter_cartesian_x'][:] # cartesian position of the prime cell circumcenter
    cell_cart_vec[:,1] = f.variables['cell_circumcenter_cartesian_y'][:]
    cell_cart_vec[:,2] = f.variables['cell_circumcenter_cartesian_z'][:]
    edge_cart_vec      = np.ma.zeros((elon.size,3), dtype=dtype)
    edge_cart_vec[:,0] = f.variables['edge_middle_cartesian_x'][:] # prime edge center cartesian coordinate x on unit
    edge_cart_vec[:,1] = f.variables['edge_middle_cartesian_y'][:]
    edge_cart_vec[:,2] = f.variables['edge_middle_cartesian_z'][:]
    f.close()

    dist_vector        = edge_cart_vec[edge_of_cell,:] - cell_cart_vec[:,np.newaxis,:]
    norm               = np.sqrt(pyic.scalar_product(dist_vector,dist_vector,dim=2))
    prime_edge_length  = edge_length/grid_sphere_radius
    fixed_vol_norm     = (0.5 * norm * (prime_edge_length[edge_of_cell]))
    fixed_vol_norm     = fixed_vol_norm.sum(axis=1)
    edge2cell_coeff_cc = (dist_vector * (edge_length[edge_of_cell,np.newaxis] / grid_sphere_radius) * orientation_of_normal[:,:,np.newaxis] )

    sinLon = np.sin(clon*np.pi/180.)
    cosLon = np.cos(clon*np.pi/180.)
    sinLat = np.sin(clat*np.pi/180.)
    cosLat = np.cos(clat*np.pi/180.)

    grad_coeff = (1./dual_edge_length)

    return adjacent_cell_of_edge, grad_coeff, edge2cell_coeff_cc, edge_of_cell, fixed_vol_norm, sinLon, cosLon, sinLat, cosLat


adjacent_cell_of_edge, grad_coeff, edge2cell_coeff_cc, edge_of_cell, fixed_vol_norm, sinLon, cosLon, sinLat, cosLat = get_grid_variables()

# load temperature and salinity data
ds_T = eva.load_smt_T()
ds_S = eva.load_smt_S()
# %%
# --- load times and flist # only to get time data - it is not the selection of data
path_data  = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/????-??/'
# time0      = np.datetime64('2010-03-23T01:00:00') #compute config
# timeEnd    = np.datetime64('2010-03-24T23:00:00')

time0      = np.datetime64('2010-03-31T15:00:00') #gpu config

memory          = 512
memory_per_core = 50
ncores          = np.floor(memory/memory_per_core)#in parallel
gpu_time        = 12*60 #h
compute_time    = 80 #min
loops_per_node  = np.floor(gpu_time / compute_time)
nsteps          = int(np.floor(loops_per_node * ncores))
nsteps          = 5 #end of march
timeEnd         = time0 + np.timedelta64(nsteps,'2h')

ts         = pyic.timing(ts, 'load times and flist')
search_str = f'{run}_T_S_sp_001-016_*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')

#time0 #timeEnd
itd0     = np.argmin((timesd-time0).astype(float)**2)
itdEnd   = np.argmin((timesd-timeEnd).astype(float)**2)
# %%
# %%
# %%
##################################################################################
steps = np.arange(itd0, itdEnd+1)
print('time0:   ', timesd[itd0], 'timeEnd: ', timesd[itdEnd], 'steps: ', steps.size)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro] # type: ignore
steps = list_all_pros[rank]

##################################################################################

# for itd in range(itd0,itdEnd+1):
for nn, step in enumerate(steps): # type: ignore
    ### load file for oine timestep
    print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps))) # type: ignore
    print('Load files for timestep', {timesd[step]})

    to = ds_T.sel(time=timesd[step])
    so = ds_S.sel(time=timesd[step])

    ### write data to
    fpatho = f'/work/mh0033/m300878/smt/b/b_depthi/pp_calc_b_period_{timesd[step]}.nc'
    fo     = Dataset(fpatho, 'w', format='NETCDF4')
    fo.createDimension('depthi', nzi) 
    fo.createDimension('ncells', cc) # new dim cell center
    nc_b     = fo.createVariable('b'     ,'f4',('depthi','ncells'))
    nc_dbdx  = fo.createVariable('dbdx'  ,'f4',('depthi','ncells'))
    nc_dbdy  = fo.createVariable('dbdy'  ,'f4',('depthi','ncells'))
    ncv      = fo.createVariable('depthi','f4', 'depthi')
    ncv[:]   = depthi[:nzi]

    pdtime   = pd.to_datetime(timesd[step])
    print('start calculate b for timestep', pdtime)
    
    for kk, lev in enumerate(levs[:]):
            rhop    = sw.dens(so[lev,:], to[lev,:], depthi[lev,np.newaxis]) # TODO: change to depthc since there is no vertical interpolation!! choice of depthi interpolates rho to desired depth
            
            b       = - g * (rhop - rho0)/rho0
            gradh_b = (b[adjacent_cell_of_edge[:,1]]-b[adjacent_cell_of_edge[:,0]])*grad_coeff

            p_vn_c = ( edge2cell_coeff_cc[:,:,:]
                    * gradh_b[edge_of_cell,np.newaxis]
                    * dzw[lev]
                    ).sum(axis=1)
            p_vn_c *= 1./(fixed_vol_norm[:,np.newaxis]*dzw[lev])

            u1 = p_vn_c[:,0]
            u2 = p_vn_c[:,1]
            u3 = p_vn_c[:,2]

            dbdx =   u2*cosLon - u1*sinLon
            dbdy = -(u1*cosLon + u2*sinLon)*sinLat + u3*cosLat

            ### save
            nc_b[kk+1,:]    = b[:cc]
            nc_dbdx[kk+1,:] = dbdx[:cc]
            nc_dbdy[kk+1,:] = dbdy[:cc]

    nc_b[0,:]     = np.NaN
    nc_dbdx[0,:]  = np.NaN
    nc_dbdy[0,:]  = np.NaN
    nc_b[-1,:]    = np.NaN
    nc_dbdx[-1,:] = np.NaN
    nc_dbdy[-1,:] = np.NaN

    fo.close()

    print('finish calculate at')