#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --time=08:00:00
#SBATCH --output=log.o-%j.out
#SBATCH --error=log.o-%j.out
#SBATCH --ntasks=1
#SBATCH --partition=gpu
#SBATCH --mem=60G
#####SBATCH --account=bm0371
#####SBATCH --account=mh0033
#SBATCH --account=mh0033

#module load python/2.7.12
module list

#source /home/mpim/m300602/bin/myactcondenv.sh
#source /home/mpim/m300602/pyicon/tools/conda_act_mistral_pyicon_env.sh
source /work/mh0033/u241317/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`
#python -u pp_calc_div.py --slurm
python -u pp_calc_db.py --slurm
#python -u pp_calc_v_h.py --slurm
#python -u pp_interp_S.py --slurm
#python -u pp_interp_T.py --slurm
#python -u pp_calc_w.py --slurm
#python -u pp_interp_T_tave.py --slurm
#python -u pp_calc_S_period.py --slurm
enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

# to create a loop
#####sbatch ./run_batch_job
#create a file here to stop loop 
