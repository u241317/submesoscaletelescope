#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=5 #10
#SBATCH --partition=gpu
#SBATCH --time=02:00:00 
#SBATCH --exclusive 
#SBATCH --account=mh0033
#SBATCH --mail-type=FAIL 

####SBATCH --mem=256Gb

module list

source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`
mpirun -np 5 python -u pp_calc_db_period_mpi.py --slurm

enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

# to create a loop
#####sbatch ./run_batch_job
#create a file here to stop loop 
