#!/usr/bin/env python
import os
import numpy as np
import math

nsteps = 85 #total number of steps; one more!
npar   = 12  #number of jobs run on one node
njobs  = math.ceil(nsteps/npar) #3

for kk in range(njobs): #0,1,2
  k1 = kk*npar #0,4,8
  k2 = (kk+1)*npar #4,8,12
  os.system(f"sbatch ./run_pp_calc_db_parallel.sh {kk+1} {k1} {k2}") # 1,0,4, 2,4,8, 3,8,12
  