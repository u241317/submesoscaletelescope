import sys
import numpy as np
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
import xarray as xr
import pandas as pd
import seawater as sw
from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')
print('start calculation of b and spatial derivatives')

### configure paths
run      = 'ngSMT_tke'
savefig  = False
path_fig = '../pics/'
nnf      = 0
gname = 'smt'
lev   = 'L128'

# load interpolated temperature and salinity
path_data    = '/work/mh0033/u241317/smt/T/pp_calc_T.nc'
dt = xr.open_dataset(path_data)
to = dt.T
path_data    = '/work/mh0033/u241317/smt/S/pp_calc_S.nc'
ds = xr.open_dataset(path_data)
so = ds.S

path_data    = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/????-??/'
#####################################################################
### Calculate dudz over all layers for one timestep
levs   = np.arange(depthc.size)
nz     = levs.size
depthc = depthc[levs]

cc        = int(59799625)
nzi       = int(nz+1)

fpatho = '/work/mh0033/u241317/smt/db/pp_calc_dbc.nc'
fo = Dataset(fpatho, 'w', format='NETCDF4')
fo.createDimension('depthc', nz) 
fo.createDimension('cc', cc) # new dim cell center

nc_dbdx   = fo.createVariable('dbdx','f4',('depthc','cc'))
nc_dbdy   = fo.createVariable('dbdy','f4',('depthc','cc'))
ncv       = fo.createVariable('depthc','f4','depthc')
ncv[:]    = depthc[:nz]

## --- load times and flist # only to get time data - it is not the selection of data
#time0      = np.datetime64('2010-03-09T01:00:00')
#ts         = pyic.timing(ts, 'load times and flist')
#search_str = f'{run}_vn_sp_001-016_*.nc' 
#flist      = np.array(glob.glob(path_data+search_str))
#flist.sort()
#timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')
#
#itd     = np.argmin((timesd-time0).astype(float)**2)
#fpath   = flist_tsd[itd]
#pdtime0 = pd.to_datetime(time0) 
#tstr    = pdtime0.strftime('%Y%m%d')+'T010000Z'

#calc coeff
grid_sphere_radius = 6.371229e6

fpath_tgrid  = '/home/mpim/m300602/work/icon/grids/smt/smt_tgrid.nc'
f = Dataset(fpath_tgrid, 'r')
clon = f.variables['clon'][:] * 180./np.pi # center longitude
clat = f.variables['clat'][:] * 180./np.pi # center latitude
vlon = f.variables['vlon'][:] * 180./np.pi # vertex longitude
vlat = f.variables['vlat'][:] * 180./np.pi # vertex latitude
vertex_of_cell = f.variables['vertex_of_cell'][:].transpose()-1 # vertices of each cellcells ad
edge_of_cell = f.variables['edge_of_cell'][:].transpose()-1 # edges of each cellvertices
edges_of_vertex = f.variables['edges_of_vertex'][:].transpose()-1 # edges around vertex
dual_edge_length = f.variables['dual_edge_length'][:] # lengths of dual edges (distances between triangul 
edge_orientation = f.variables['edge_orientation'][:].transpose()
edge_length = f.variables['edge_length'][:] # lengths of edges of triangular cells
cell_area_p = f.variables['cell_area_p'][:] # area of grid cell 
dual_area = f.variables['dual_area'][:] # areas of dual hexagonal/pentagonal cells
#edge_length = f.variables['edge_length'][:]
adjacent_cell_of_edge = f.variables['adjacent_cell_of_edge'][:].transpose()-1 # cells adjacent to each edge 
orientation_of_normal = f.variables['orientation_of_normal'][:].transpose() # orientations of normals to triangular cell edges
edge_vertices = f.variables['edge_vertices'][:].transpose()-1 #vertices at the end of of each edge
f.close()

dtype = 'float32'
f = Dataset(fpath_tgrid, 'r')
elon = f.variables['elon'][:] * 180./np.pi #edge midpoint longitude
elat = f.variables['elat'][:] * 180./np.pi #edge midpoint latitude
cell_cart_vec = np.ma.zeros((clon.size,3), dtype=dtype)
cell_cart_vec[:,0] = f.variables['cell_circumcenter_cartesian_x'][:] # cartesian position of the prime cell circumcenter
cell_cart_vec[:,1] = f.variables['cell_circumcenter_cartesian_y'][:]
cell_cart_vec[:,2] = f.variables['cell_circumcenter_cartesian_z'][:]
edge_cart_vec = np.ma.zeros((elon.size,3), dtype=dtype)
edge_cart_vec[:,0] = f.variables['edge_middle_cartesian_x'][:] # prime edge center cartesian coordinate x on unit
edge_cart_vec[:,1] = f.variables['edge_middle_cartesian_y'][:]
edge_cart_vec[:,2] = f.variables['edge_middle_cartesian_z'][:]
f.close()

dist_vector        = edge_cart_vec[edge_of_cell,:] - cell_cart_vec[:,np.newaxis,:]
norm               = np.sqrt(pyic.scalar_product(dist_vector,dist_vector,dim=2))
prime_edge_length  = edge_length/grid_sphere_radius
fixed_vol_norm     = (0.5 * norm * (prime_edge_length[edge_of_cell]))
fixed_vol_norm     = fixed_vol_norm.sum(axis=1)
edge2cell_coeff_cc = (dist_vector * (edge_length[edge_of_cell,np.newaxis] / grid_sphere_radius) * orientation_of_normal[:,:,np.newaxis] )

sinLon = np.sin(clon*np.pi/180.)
cosLon = np.cos(clon*np.pi/180.)
sinLat = np.sin(clat*np.pi/180.)
cosLat = np.cos(clat*np.pi/180.)

grad_coeff = (1./dual_edge_length)

#now = datetime.now().time() # time object
print('start calculate at')

g     = 9.80665
rho0  = 1025.022

for kk, lev in enumerate(levs):
    print('lev', lev)
    #rhop    = sw.dens(so[lev,:], to[lev,:], depthi[lev,np.newaxis])
    rhop    = sw.dens(so[lev,:], to[lev,:], depthc[lev,np.newaxis])
    b       = - g * (rhop - rho0)/rho0
    gradh_b = (b[adjacent_cell_of_edge[:,1]]-b[adjacent_cell_of_edge[:,0]])*grad_coeff

    #b = b[np.newaxis,:]

    p_vn_c = ( edge2cell_coeff_cc[:,:,:]
            * gradh_b[edge_of_cell,np.newaxis]
            * dzw[lev]
            ).sum(axis=1)
    p_vn_c *= 1./(fixed_vol_norm[:,np.newaxis]*dzw[lev])

    u1 = p_vn_c[:,0]
    u2 = p_vn_c[:,1]
    u3 = p_vn_c[:,2]

    dbdx =   u2*cosLon - u1*sinLon
    dbdy = -(u1*cosLon + u2*sinLon)*sinLat + u3*cosLat

    #save
    nc_dbdx[kk,:] = dbdx[:cc]
    nc_dbdy[kk,:] = dbdy[:cc]

fo.close()

#now = datetime.now().time() # time object
print('finish calculate at')