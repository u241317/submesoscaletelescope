#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --partition=compute
#SBATCH --mem=256Gb
#SBATCH --time=08:00:00 
#SBATCH --exclusive 
#SBATCH --account=mh0033
#SBATCH --mail-type=FAIL 


module list

source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`
# mpirun -np 4 python -u pp_calc_db_period_mpi_wave.py --slurm $1 $2 $3
# mpirun -np 4 python -u pp_calc_db_notides.py --slurm $1 $2 $3
mpirun -np 4 python -u pp_calc_db_period_mpi_depthc.py --slurm $1 $2 $3

enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

# to create a loop
#####sbatch ./run_batch_job
#create a file here to stop loop 
