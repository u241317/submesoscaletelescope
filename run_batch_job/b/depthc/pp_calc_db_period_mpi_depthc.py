# %%
import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "../../../")
import numpy as np
from netCDF4 import Dataset, num2date # type: ignore
import pyicon as pyic
import glob
import xarray as xr
import pandas as pd
import seawater as sw
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi

# %%
num = int(sys.argv[2])
t1  = int(sys.argv[3])
t2  = int(sys.argv[4])

ts = pyic.timing([0], 'start')

print('Run calculation of buoyancy')

def get_grid_variables():
    print('get grid variables')
    grid_sphere_radius = 6.371229e6
    fpath_tgrid           = '/work/mh0033/m300602/icon/grids/smt/smt_tgrid.nc'
    # fpath_tgrid           = '/home/m/m300602/work/icon/grids/smtwv_oce_2022/smtwv_oce_2022_tgrid.nc'
    f                     = Dataset(fpath_tgrid, 'r')
    clon                  = f.variables['clon'][:] * 180./np.pi # center longitude
    clat                  = f.variables['clat'][:] * 180./np.pi # center latitude
    edge_of_cell          = f.variables['edge_of_cell'][:].transpose()-1 # edges of each cellvertices
    dual_edge_length      = f.variables['dual_edge_length'][:] # lengths of dual edges (distances between triangul 
    edge_length           = f.variables['edge_length'][:] # lengths of edges of triangular cells
    adjacent_cell_of_edge = f.variables['adjacent_cell_of_edge'][:].transpose()-1 # cells adjacent to each edge
    orientation_of_normal = f.variables['orientation_of_normal'][:].transpose() # orientations of normals to triangular cell edges
    f.close()

    dtype = 'float32'
    f                  = Dataset(fpath_tgrid, 'r')
    elon               = f.variables['elon'][:] * 180./np.pi #edge midpoint longitude
    cell_cart_vec      = np.ma.zeros((clon.size,3), dtype=dtype)
    cell_cart_vec[:,0] = f.variables['cell_circumcenter_cartesian_x'][:] # cartesian position of the prime cell circumcenter
    cell_cart_vec[:,1] = f.variables['cell_circumcenter_cartesian_y'][:]
    cell_cart_vec[:,2] = f.variables['cell_circumcenter_cartesian_z'][:]
    edge_cart_vec      = np.ma.zeros((elon.size,3), dtype=dtype)
    edge_cart_vec[:,0] = f.variables['edge_middle_cartesian_x'][:] # prime edge center cartesian coordinate x on unit
    edge_cart_vec[:,1] = f.variables['edge_middle_cartesian_y'][:]
    edge_cart_vec[:,2] = f.variables['edge_middle_cartesian_z'][:]
    f.close()

    dist_vector        = edge_cart_vec[edge_of_cell,:] - cell_cart_vec[:,np.newaxis,:]
    norm               = np.sqrt(pyic.scalar_product(dist_vector,dist_vector,dim=2))
    prime_edge_length  = edge_length/grid_sphere_radius
    fixed_vol_norm     = (0.5 * norm * (prime_edge_length[edge_of_cell]))
    fixed_vol_norm     = fixed_vol_norm.sum(axis=1)
    edge2cell_coeff_cc = (dist_vector * (edge_length[edge_of_cell,np.newaxis] / grid_sphere_radius) * orientation_of_normal[:,:,np.newaxis] )

    sinLon = np.sin(clon*np.pi/180.)
    cosLon = np.cos(clon*np.pi/180.)
    sinLat = np.sin(clat*np.pi/180.)
    cosLat = np.cos(clat*np.pi/180.)

    grad_coeff = (1./dual_edge_length)

    return adjacent_cell_of_edge, grad_coeff, edge2cell_coeff_cc, edge_of_cell, fixed_vol_norm, sinLon, cosLon, sinLat, cosLat

# %%
#####################################################################
### precalculation of coefficients
g     = 9.80665
rho0  = 1025.022

beginn  = '2010-03-15-T21:00:00'
# beginn  = '2010-03-22-T21:00:00'
end     = '2010-03-22-T21:00:00'
tstring = f'{beginn}', f'{end}'

adjacent_cell_of_edge, grad_coeff, edge2cell_coeff_cc, edge_of_cell, fixed_vol_norm, sinLon, cosLon, sinLat, cosLat = get_grid_variables()
# %%
# load temperature and salinity data
print('load temperature and salinity data')
ds_T = eva.load_smt_T()
ds_T = ds_T.sel(time=slice(f'{beginn}', f'{end}'))
ds_S = eva.load_smt_S()
ds_S = ds_S.sel(time=slice(f'{beginn}', f'{end}'))


timesd = ds_T.time.data
steps  = np.arange(0, timesd.size)
ncells = ds_T.ncells.size
# nzc    = ds_T.depthc.size + 1
levs   = np.arange(ds_T.depthc.size)


# %%
##################################################################################
steps_global = np.arange(timesd.size)
steps        = steps_global[t1:t2]
print('time0:   ', timesd[steps[0]], 'timeEnd: ', timesd[steps[-1]], 'steps: ', steps.size)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro] # type: ignore
steps = list_all_pros[rank]

##################################################################################

# for itd in range(itd0,itdEnd+1):
for nn, step in enumerate(steps): # type: ignore
    ### load file for oine timestep
    print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps))) # type: ignore
    pdtime   = pd.to_datetime(timesd[step])
    print('start calculate b for timestep', pdtime)

    to = ds_T.sel(time=timesd[step])
    so = ds_S.sel(time=timesd[step])

    ### write data to
    fpatho = f'/work/mh0033/m300878/smt/b/b_depthc/pp_calc_b_{timesd[step]}.nc'
    fo1     = Dataset(fpatho, 'w', format='NETCDF4')
    fo1.createDimension('depthc', len(levs)) 
    fo1.createDimension('ncells', ncells) 
    nc_b     = fo1.createVariable('b'     ,'f4',('depthc','ncells'))

    fpatho = f'/work/mh0033/m300878/smt/b/b_depthc/pp_calc_dbdx_{timesd[step]}.nc'
    fo2     = Dataset(fpatho, 'w', format='NETCDF4')
    fo2.createDimension('depthc', len(levs)) 
    fo2.createDimension('ncells', ncells) 
    nc_dbdx  = fo2.createVariable('dbdx'  ,'f4',('depthc','ncells'))

    fpatho = f'/work/mh0033/m300878/smt/b/b_depthc/pp_calc_dbdy_{timesd[step]}.nc'
    fo3     = Dataset(fpatho, 'w', format='NETCDF4')
    fo3.createDimension('depthc', len(levs)) 
    fo3.createDimension('ncells', ncells)
    nc_dbdy  = fo3.createVariable('dbdy'  ,'f4',('depthc','ncells'))

    for kk, lev in enumerate(levs[:]):
            rhop    = sw.dens(so[lev,:], to[lev,:], depthc[lev,np.newaxis]) # TODO: change to depthc since there is no vertical interpolation!! choice of depth interpolates rho to desired depth
            
            b       = - g * (rhop - rho0)/rho0
            gradh_b = (b[adjacent_cell_of_edge[:,1]]-b[adjacent_cell_of_edge[:,0]])*grad_coeff

            p_vn_c = ( edge2cell_coeff_cc[:,:,:]
                    * gradh_b[edge_of_cell,np.newaxis]
                    * dzw[lev]
                    ).sum(axis=1)
            p_vn_c *= 1./(fixed_vol_norm[:,np.newaxis]*dzw[lev])

            u1 = p_vn_c[:,0]
            u2 = p_vn_c[:,1]
            u3 = p_vn_c[:,2]

            dbdx =   u2*cosLon - u1*sinLon
            dbdy = -(u1*cosLon + u2*sinLon)*sinLat + u3*cosLat

            ### save
            nc_b[kk,:]    = b[:]
            nc_dbdx[kk,:] = dbdx[:]
            nc_dbdy[kk,:] = dbdy[:]

    fo1.close()
    fo2.close()
    fo3.close()

    print('finish calculate at')