#! /bin/bash
#SBATCH --job-name=cdo_slev
#SBATCH --time=02:00:00
#SBATCH --nodes=1
#SBATCH --output=log.o-%j.out
#SBATCH --error=log.o-%j.out
#SBATCH --account=mh0033
#SBATCH --partition=compute
### SBATCH --mem=256G
#SBATCH --exclusive 

exp=9
variable=v
path_to_input_files=/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv000${exp}/outdata_${variable}/
infilename=smtwv000${exp}_oce_3d_${variable}_PT1H_*.nc
path_to_output_files=/work/bm1239/m300878/smtwv_levs/2/exp${exp}/outdata_${variable}/

echo "Processing in parallel..."
module load parallel
# find ${path_to_input_files} -name ${infilename} | parallel --jobs 50 --eta cdo -sellevidx,1,18,27,77,96,108,116 -selvar,${variable} {} ${path_to_output_files}{/}
find ${path_to_input_files} -name ${infilename} | parallel --jobs 100 --eta cdo -sellevidx,17,19,26,28,42,43 -selvar,${variable} {} ${path_to_output_files}{/}
echo "read/write finished..."

echo "Renaming files..."
find ${path_to_output_files} -name ${infilename} | xargs rename 3d lev
echo "Job finished..."

infilename=smtwv000${exp}_oce_lev_${variable}_PT1H_*.nc
echo "Drop Coordinate Variables..."
module load nco
find ${path_to_output_files} -name ${infilename} | parallel --jobs 50 --eta ncks -C -O -x -v clat_bnds,clon_bnds,clat,clon {} ${path_to_output_files}{/}
echo "Drop Coordinate Variables finished..."