#%%
import xarray as xr
import numpy as np
import glob
# %%
path = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_u/smtwv0007_oce_3d_u_PT1H_20190801T171500Z.nc'

ds = xr.open_dataset(path)

# %%
path_to_output_files='/work/bm1239/m300878/smtwv_levs/exp7/outdata_u/'
flist      = np.array(glob.glob(path_to_output_files+'*.nc'))
flist.sort()
ds = xr.open_mfdataset(flist, concat_dim='time', combine='nested', chunks={'time': 1, 'depth':1}, decode_cf=False)
ds = xr.decode_cf(ds) #restores timestamp
# = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))

# %%
#select every 100 time tep
ds.u.isel(time=slice(0, None, 100)).isel(depth=2).plot()


# %%
ds = xr.open_dataset(flist[1])

# symmetric instability in the gulf stream thomas
