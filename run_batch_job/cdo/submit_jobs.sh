#! /bin/bash


ntimes_global=685
ntimes_per_node=240
nodes=3
nlist=$(seq 0 $((nodes-1)))

for node in ${nlist};
do
t0=$((1 + node*ntimes_per_node))
t1=$(((node+1)*ntimes_per_node))
echo "Processing $t0 to $t1..."
sbatch select_lev_output.sh ${t0} ${t1}
done