#! /bin/bash
#SBATCH --job-name=cdo_slev
#SBATCH --time=08:00:00
#SBATCH --output=log.o-%j.out
#SBATCH --error=log.o-%j.out
#SBATCH --account=mh0033
#SBATCH --partition=compute
#SBATCH --exclusive 

t0=$1
t1=$2

variable=u
path_to_input_files=/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_${variable}/smtwv0007_oce_3d_u_PT1H_*.nc
path_to_output_files=/work/bm1239/m300878/smtwv_levs/exp7/

# create file list
file_list=$(ls ${path_to_input_files} | sed -n "${t0},${t1}p" )

for file in ${file_list};
do
    filename=$(basename -- "$file")
    echo "Processing $filename file..."
    date=${filename: -19}
    outname=smtwv0007_oce_lev_u_PT1H_${date}
    cdo -sellevidx,1,18,27,77,96,108,116 -selvar,{${variable}} ${file} ${path_to_output_files}${outname}
    echo "Done with $outname file..."
done


# write same code but use find and run in parallel
# module load parallel
# find /work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_u/ -name "smtwv0007_oce_3d_u_PT1H_*.nc" | parallel --jobs 100 --eta cdo -sellevidx,1,18,27,77,96,108,116 -selvar,u {} /work/bm1239/m300878/smtwv_levs/exp7/{/}

# variable=u
# path_to_input_files=/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_${variable}/
# infilename=smtwv0007_oce_3d_u_PT1H_20190706T1*.nc
# infilename=smtwv0007_oce_lev_u_PT1H_20190706T131500Z.nc

# find ${path_to_input_files} -name ${infilename}  -print | cdo -sellevidx,1,18,27,77,96,108,116 -selvar,${variable}  /work/bm1239/m300878/smtwv_levs/exp7/{/}

# find ${path_to_input_files} -name ${infilename} -print0 | xargs -0 -n 100 -P 4 cdo -sellevidx,1,18,27,77,96,108,116 -selvar,${variable} ${} /work/bm1239/m300878/smtwv_levs/exp7/${/}

#write above code with find and -exec
# find /work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_u/ -name "smtwv0007_oce_3d_u_PT1H_*.nc" -exec cdo -sellevidx,1,18,27,77,96,108,116 -selvar,u {} /work/bm1239/m300878/smtwv_levs/exp7/1.nc \;

