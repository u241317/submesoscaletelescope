#!/bin/bash

## ~~~~~~~~~~~~~~~~~~~~~~~~~ start user input ~~~~~~~~~~~~~~~~~~~~~~~~~
# HINT:
#   * You can change the values right of the "=" as you wish.
#   * The "%j" in the log file names means that the job id will be inserted

#SBATCH --job-name=test_slk_retr_job   # Specify job name
#SBATCH --output=test_job.o%j    # name for standard output log file
#SBATCH --error=test_job.e%j     # name for standard error output log file
#SBATCH --partition=shared      # Specify partition name
#SBATCH --ntasks=1               # Specify max. number of tasks to be invoked
#SBATCH --mem=6GB                # allocated memory for the script
#SBATCH --time=08:00:00          # Set a limit on the total run time
#SBATCH --account=mh0033         # Charge resources on this project account
## ~~~~~~~~~~~~~~~~~~~~~~~~~ end user input ~~~~~~~~~~~~~~~~~~~~~~~~~

source_resource=/arch/mh0287/m211054/icon/era5/experiments/exp.ocean_era51h_zstar_r2b9_22255-ERA/exp.ocean_era51h_zstar_r2b9_22255-ERA_restart_004.tar
target_folder=/home/m/m300878/work/run_icon/icon-oes-zstar-r2b9/restart

# create folder to retrieve into (target folder)
mkdir -p ${target_folder}
module load slk
# set striping for target folder
# see https://docs.dkrz.de/doc/hsm/striping.html
# ON LEVANTE
lfs setstripe -E 1G -c 1 -S 1M -E 4G -c 4 -S 1M -E -1 -c 8 -S 1M ${target_folder}
# ON MISTRAL
#lfs setstripe -S 4M -c 8 ${target_folder}

# do the retrieval
echo "doing 'slk retrieve ${source_resource} ${target_folder}'"
slk retrieve ${source_resource} ${target_folder}
# '$?' captures the exit code of the previous command
if [ $? -ne 0 ]; then
  >&2 echo "an error occurred in slk retrieve call"
else
  echo "retrieval successful"
fi