#!/bin/bash
#SBATCH --job-name=slk_retr_r2b8        # Specify job name
#SBATCH --output=slk_retr_r2b8.o%j      # name for standard output log file
#SBATCH --error=slk_retr_r2b8.e%j       # name for standard error output log
#SBATCH --partition=slk                 # partition name
#SBATCH --nodes=1                       # Specify number of nodes
#SBATCH --ntasks=1                      # Specify number of (MPI) tasks on each node
#SBATCH --time=08:00:00                 # Set a limit on the total run time
#SBATCH --mail-type=FAIL                # Notify user by email in case of job failure
#SBATCH --account=mh0033                # Charge resources on this project account
#SBATCH --mem=8GB

echo "doing 'slk retrieve'"
module load slk
startdate=`date +%Y-%m-%d\ %H:%M:%S`

target_folder=/scratch/m/m300878/retrieve
# create folder to retrieve into (target folder)
mkdir -p ${target_folder}

#slk retrieve /arch/mh0287/m211054/icon/era5/experiments/ocean_era51h_r2b8_19074-AMK/outdata/ocean_era51h_r2b8_19074-AMK_dm_20100301T000000Z.nc.gz ${target_folder}
slk retrieve /arch/mh0287/m211054/icon/era5/experiments/ocean_era51h_r2b8_19074-AMK/outdata/ocean_era51h_r2b8_19074-AMK_2010.tar ${target_folder}
if [ $? -ne 0 ]; then
    >&2 echo "an error occurred in slk retrieve call"
else
    echo "retrieval successful"
fi

enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

