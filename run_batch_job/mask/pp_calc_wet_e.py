import sys
import numpy as np
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
import xarray as xr
import pandas as pd
import pyicon as pyic
from termcolor import colored
from icon_smt_levels import dzw, dzt, depthc, depthi

print(colored('start calc wet_e', 'red'))
ts = pyic.timing([0], 'start')

### configure paths
run      = 'ngSMT_tke'
savefig  = False
path_fig = '../pics/'
nnf      = 0

gname = 'smt'
lev   = 'L128'

path_data    = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'
path_grid    = f'/mnt/lustre01/work/mh0033/m300602/icon/grids/{gname}/'
fpath_tgrid  = '/home/mpim/m300602/work/icon/grids/smt/smt_tgrid.nc'

#####################################################################
### Calculate dudz over all layers for one timestep
levs   = np.arange(depthc.size)
nz     = levs.size
depthc = depthc[levs]

cc        = int(59799625)
ce        = int(89813639)

fpatho = '/work/mh0033/u241317/smt/vh/wet_e.nc'
fo = Dataset(fpatho, 'w', format='NETCDF4')
fo.createDimension('depthc', nz) 
fo.createDimension('ce', ce) # new dim cell center

nc_wet_e  = fo.createVariable('mask','f4',('depthc','ce'))
ncv    = fo.createVariable('depthc','f4','depthc')
ncv[:] = depthc[:nz]

# --- prepare output netcdf file
ts      = pyic.timing(ts, 'prepare nc')
varfile = 'vn'
layers  = (  ['sp_001-016']*16 + ['sp_017-032']*16 + ['sp_033-048']*16
          + ['sp_049-064']*16 + ['sp_065-080']*16 + ['sp_081-096']*16 + ['sp_097-112']*16)

# --- load times and flist # only to get time data - it is not the selection of data
time0      = np.datetime64('2010-03-09T01:00:00')
ts         = pyic.timing(ts, 'load times and flist')
search_str = f'{run}_vn_sp_001-016_*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')

itd     = np.argmin((timesd-time0).astype(float)**2)
fpath   = flist_tsd[itd]
pdtime0 = pd.to_datetime(time0) 
tstr    = pdtime0.strftime('%Y%m%d')+'T010000Z'

#only march
path_data = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'

#now = datetime.now().time() # time object
print('start calculate at')

####  
for kk, lev in enumerate(levs):
  print('lev', lev)
  #ts = pyic.timing(ts, 'loop step')
  # --- load horizontal velocity 1
  fname = f'{run}_{varfile}_{layers[lev]}_{tstr}.nc'
  fpath = f'{path_data}{fname}'
  var   = f'vn{lev+1:03d}_sp'
  #print(f'kk = {kk}/{nz}; fname = {fname}; var = {var}')
  fi   = Dataset(fpath, 'r')
  ve   = fi.variables[var][itsd[itd],:]
  fi.close()
  #ve = xr.open_dataset(fpath)
  vex = xr.DataArray(data=ve, dims=(["edge"]))
  wet_e = vex.where(vex==0., 1)
  nc_wet_e[kk,:] = wet_e[:ce]
  
fo.close()
print('finish calculate at')