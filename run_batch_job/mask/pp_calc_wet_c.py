import sys
import numpy as np
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
import xarray as xr
from icon_smt_levels import dzw, dzt, depthc, depthi

print('start calc wet_c')

#####################################################################
### Calculate dudz over all layers for one timestep
levs   = np.arange(depthc.size)
nz     = levs.size
depthc = depthc[levs]

cc        = int(59799625)

fpatho = '/work/mh0033/u241317/smt/vh/wet_c.nc'
fo = Dataset(fpatho, 'w', format='NETCDF4')
fo.createDimension('depthc', nz) 
fo.createDimension('cc', cc) # new dim cell center

nc_wet_c  = fo.createVariable('mask','f4',('depthc','cc'))
ncv    = fo.createVariable('depthc','f4','depthc')
ncv[:] = depthc[:nz]

path_data    = '/work/mh0033/u241317/smt/T/pp_calc_T.nc'
ds = xr.open_dataset(path_data)
tt = ds.T

wet_c = tt.where(tt.T==0., 1)
wet_c = wet_c.where(tt.T!=0., 0)
nc_wet_c[:,:] = wet_c[:,:]

# ####  
# for kk, lev in enumerate(levs):
#   print('lev', lev)
#   tlev = tt.isel(depthc=lev)
#   wet_c = tlev.where(tlev.T==0., 0)
#   nc_wet_c[kk,:] = wet_c[:cc]
  
fo.close()
print('finish calculate at')