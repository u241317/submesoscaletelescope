import sys
import numpy as np
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import cartopy.crs as ccrs
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
import xarray as xr
import pandas as pd
from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')

print('start calculation of vertical velocity field')

### configure paths
run      = 'ngSMT_tke'
savefig  = False
path_fig = '../pics/'
nnf      = 0

gname = 'smt'
lev   = 'L128'

#path_data    = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/????-??/'
#path_data    = f'/scratch/u/u241317/calc/pp_calc_m_div.nc'
fpath_tgrid  = '/home/mpim/m300602/work/icon/grids/smt/smt_tgrid.nc'
fpath_Tri    = '/mnt/lustre01/work/mh0033/m300602/tmp/Tri.pkl'

path_grid     = f'/mnt/lustre01/work/mh0033/m300602/icon/grids/{gname}/'
path_ckdtree  = f'{path_grid}ckdtree/'
fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'

### read out grid data
#  plus first calculations
f = Dataset(fpath_tgrid, 'r')
clon = f.variables['clon'][:] * 180./np.pi # center longitude
clat = f.variables['clat'][:] * 180./np.pi # center latitude
vlon = f.variables['vlon'][:] * 180./np.pi # vertex longitude
vlat = f.variables['vlat'][:] * 180./np.pi # vertex latitude
vertex_of_cell = f.variables['vertex_of_cell'][:].transpose()-1 # vertices of each cellcells ad
edge_of_cell = f.variables['edge_of_cell'][:].transpose()-1 # edges of each cellvertices
edges_of_vertex = f.variables['edges_of_vertex'][:].transpose()-1 # edges around vertex
dual_edge_length = f.variables['dual_edge_length'][:] # lengths of dual edges (distances between triangul 
edge_orientation = f.variables['edge_orientation'][:].transpose()
edge_length = f.variables['edge_length'][:] # lengths of edges of triangular cells
cell_area_p = f.variables['cell_area_p'][:] # area of grid cell 
dual_area = f.variables['dual_area'][:] # areas of dual hexagonal/pentagonal cells
#edge_length = f.variables['edge_length'][:]
adjacent_cell_of_edge = f.variables['adjacent_cell_of_edge'][:].transpose()-1 # cells adjacent to each edge 
orientation_of_normal = f.variables['orientation_of_normal'][:].transpose() # orientations of normals to triangular cell edges
edge_vertices = f.variables['edge_vertices'][:].transpose()-1 #vertices at the end of of each edge
f.close()

dtype = 'float32'
f = Dataset(fpath_tgrid, 'r')
elon = f.variables['elon'][:] * 180./np.pi #edge midpoint longitude
elat = f.variables['elat'][:] * 180./np.pi #edge midpoint latitude
cell_cart_vec = np.ma.zeros((clon.size,3), dtype=dtype)
cell_cart_vec[:,0] = f.variables['cell_circumcenter_cartesian_x'][:] # cartesian position of the prime cell circumcenter
cell_cart_vec[:,1] = f.variables['cell_circumcenter_cartesian_y'][:]
cell_cart_vec[:,2] = f.variables['cell_circumcenter_cartesian_z'][:]
edge_cart_vec = np.ma.zeros((elon.size,3), dtype=dtype)
edge_cart_vec[:,0] = f.variables['edge_middle_cartesian_x'][:] # prime edge center cartesian coordinate x on unit
edge_cart_vec[:,1] = f.variables['edge_middle_cartesian_y'][:]
edge_cart_vec[:,2] = f.variables['edge_middle_cartesian_z'][:]
f.close()

# calc mapping coefficents
grid_sphere_radius = 6.371229e6
dist_vector        = edge_cart_vec[edge_of_cell,:] - cell_cart_vec[:,np.newaxis,:]
norm               = np.sqrt(pyic.scalar_product(dist_vector,dist_vector,dim=2))
prime_edge_length  = edge_length/grid_sphere_radius
fixed_vol_norm     = (0.5 * norm * (prime_edge_length[edge_of_cell]))
fixed_vol_norm     = fixed_vol_norm.sum(axis=1)
edge2cell_coeff_cc = (dist_vector * (edge_length[edge_of_cell,np.newaxis] / grid_sphere_radius) * orientation_of_normal[:,:,np.newaxis] )

### Calculate DIvergence of multiple layers
levsi = np.arange(depthi.size)
nzi = levsi.size
#depthc = depthi[levs]
levsc = np.arange(depthc.size)
nzc = levsc.size

#calc divergence coefficients
div_coeff = (  edge_length[edge_of_cell]*orientation_of_normal ) / cell_area_p[:,np.newaxis]
cc        = int(div_coeff.size/3)

w = np.zeros([depthi.size, cc], dtype='f4')
div_v = np.zeros([depthc.size, cc], dtype='f4')

# --- prepare output netcdf file
varfile = 'vn'
layers  = (  ['sp_001-016']*16 + ['sp_017-032']*16 + ['sp_033-048']*16
          + ['sp_049-064']*16 + ['sp_065-080']*16 + ['sp_081-096']*16 + ['sp_097-112']*16)

# --- load times and flist # only to get time data - it is not the selection of data
path_data  = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/????-??/'
time0      = np.datetime64('2010-03-20T19:00:00')
timeEnd    = np.datetime64('2010-04-01T01:00:00')
ts         = pyic.timing(ts, 'load times and flist')
search_str = f'{run}_vn_sp_001-016_*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')

#time0 #timeEnd
itd0     = np.argmin((timesd-time0).astype(float)**2)
itdEnd   = np.argmin((timesd-timeEnd).astype(float)**2)

path_dat = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/'
month_dat = '2010-03/'

for itd in range(itd0,itdEnd+1):
    ts      = pyic.timing(ts, 'calculation w for one timestep')
    fpath    = flist_tsd[itd]
    pdtime   = pd.to_datetime(timesd[itd]) 
    tstr     = pdtime.strftime('%Y%m%d')+'T010000Z'
    #fpatho   = f'/scratch/u/u241317/calc/timesteps_w/pp_calc_w_period_{timesd[itd]}.nc'
    fpatho   = f'/work/mh0033/u241317/smt_w/pp_calc_w_period_{timesd[itd]}.nc'

    print("Start Calculate vertical velocity for timestep", pdtime)

    fo = Dataset(fpatho, 'w', format='NETCDF4')
    fo.createDimension('depthi', nzi) # depth
    fo.createDimension('vert_velocity', cc) # new dim

    ncv = fo.createVariable('depthi','f4','depthi')
    ncv[:] = depthi[:nzi]

    nc_w = fo.createVariable('w','f4',('depthi','vert_velocity'))

    print("Calculate Divergence")
    for kk, lev in enumerate(levsc):
        #ts = pyic.timing(ts, 'loop step')
        # --- load velocity
        fname = f'{run}_{varfile}_{layers[lev]}_{tstr}.nc'
        fpath = f'{path_dat}{month_dat}{fname}'
        var = f'vn{lev+1:03d}_sp'
        #print(f'kk = {kk}/{nzc}; fname = {fname}; var = {var}')
        fi = Dataset(fpath, 'r')
        ve = fi.variables[var][itsd[itd],:]
        #print(fi.variables[var].shape)
        fi.close()
        
        #edge2cell
        p_vn_3d = pyic.edges2cell(IcD, ve)
        #cell2edge
        ptp_vn_e = pyic.cell2edges(IcD, p_vn_3d*IcD.wet_c[:,:,np.newaxis])*IcD.wet_e
        #xarry div_v

        #div_v1  = (ve[edge_of_cell]    * div_coeff).sum(axis=1)  
        #div_v[kk,:] = div_v1[:cc]
        div_v[kk,:] = (ve[edge_of_cell]    * div_coeff).sum(axis=1)

    print("Calculate vertical Velocity")
    for z in reversed(range(nzc)):
        #print(z)
        w[z,:]    = - w[z+1,:] - div_v[z,:] * dzw[z]
        nc_w[z,:] = w[z,:cc]

    fo.close()

    print('finish calculate')