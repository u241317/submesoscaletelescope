#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --time=02:30:00
#SBATCH --output=log.o-%j.out
#SBATCH --error=log.o-%j.out
#SBATCH --ntasks=1
#SBATCH --partition=gpu
#SBATCH --mem=60G
####SBATCH --exclusive 
#SBATCH --account=mh0033

#module load python/2.7.12
module list

#source /home/mpim/m300602/bin/myactcondenv.sh
#source /home/mpim/m300602/pyicon/tools/conda_act_mistral_pyicon_env.sh
source /work/mh0033/u241317/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`
mpirun -np 1 python -u pp_calc_w_period_new_dask.py --slurm
enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"
