import sys
import numpy as np
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import cartopy.crs as ccrs
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
import xarray as xr
import pandas as pd
from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')

### configure paths
run      = 'ngSMT_tke'
savefig  = False
path_fig = '../pics/'
nnf      = 0

gname = 'smt'
lev   = 'L128'

#path_data    = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/????-??/'
path_data    = f'/scratch/u/u241317/calc/pp_calc_m_div_t.nc'
fpath_tgrid  = '/home/mpim/m300602/work/icon/grids/smt/smt_tgrid.nc'
fpath_Tri    = '/mnt/lustre01/work/mh0033/m300602/tmp/Tri.pkl'

path_grid     = f'/mnt/lustre01/work/mh0033/m300602/icon/grids/{gname}/'
path_ckdtree  = f'{path_grid}ckdtree/'
fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'

### read out grid data
#  plus first calculations
# f = Dataset(path_data, 'r')
# div_v = f.variables['div_v'][:]
# f.close()

ds = xr.open_dataset(path_data)
div_v = ds.div_v
a,b = div_v.shape
w = np.zeros([depthi.size, b], dtype='f4')

# ds = ds.assign_coords(depthi=("depthi", depthi))
# #maybe reduce storage by makeing use of a masked array
# w = np.zeros([depthi.size, 59799625], dtype='f4')
# ds = ds.assign({'w': (('depthi','cc'), w)})

# cc = int(59799625)

#####################################################################
### Calculate DIvergence of multiple layers
levs = np.arange(depthi.size)
nz = levs.size
#depthc = depthi[levs]

fpatho = '/scratch/u/u241317/calc/pp_calc_w_p.nc'
#path_data = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'
fo = Dataset(fpatho, 'w', format='NETCDF4')
fo.createDimension('depthi', nz) # depth
fo.createDimension('vert_velocity', b) # new dim

ncv = fo.createVariable('depthi','f4','depthi')
ncv[:] = depthi[:nz]

nc_w = fo.createVariable('w','f4',('depthi','vert_velocity'))

# # only march data
# path_data = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'
# levs = np.array([1,2,3,4,5]) # only for testing

# for kk, lev in enumerate(levs):
#   ts = pyic.timing(ts, 'loop step')

#   ds.w.isel(lev-1) = ds.w.isel(lev) - ds.div_v.isel(depthc=(lev-1)) * dzw(lev-1)

#   #nc_w[kk,:] = ds.w[:cc]

# fo.close()

for z in reversed(range(a)):
    print(z)
    w[z,:]    = w[z+1,:] - div_v[z,:] * dzw[z]
    nc_w[z,:] = w[z,:b]

fo.close()