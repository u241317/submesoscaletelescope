import sys
import numpy as np
from netCDF4 import Dataset, num2date # type: ignore
from ipdb import set_trace as mybreak
import pyicon as pyic
import cartopy.crs as ccrs
import glob
import pickle
#import maps_icon_smt_temp as smt
import datetime
import dask.array as da
import xarray as xr
import pandas as pd
import time
from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')
run         = 'ngSMT_tke'

print('start calculation of vertical velocity field new dask')
def get_grid_variables():
    ### configure paths
    fpath_tgrid = '/work/mh0033/m300602/icon/grids/smt/smt_tgrid.nc'
    ds_tg       = xr.open_dataset(fpath_tgrid, chunks=dict())
    ds_IcD      = pyic.convert_tgrid_data(ds_tg)
    path_data   = '/work/mh0033/from_Mistral/mh0033/u241317/smt/vh/wet_e.nc'
    dse         = xr.open_dataset(path_data)
    wet_e       = dse.mask
    wet_e       = wet_e.rename({'ce': 'edge'})
    path_data   = '/work/mh0033/from_Mistral/mh0033/u241317/smt/vh/wet_c.nc'
    dsc         = xr.open_dataset(path_data)
    wet_c       = dsc.mask
    wet_c       = wet_c.rename({'cc': 'cell'})


    ########### compute coefficients #############
    ds_IcD               = ds_IcD.compute()
    fixed_vol_norm       = pyic.xr_calc_fixed_volume_norm(ds_IcD)
    div_coeff            = pyic.xr_calc_div_coeff(ds_IcD)
    div_coeff            = div_coeff.compute()
    edge2cell_coeff_cc   = pyic.xr_calc_edge2cell_coeff_cc(ds_IcD)
    edge2cell_coeff_cc   = edge2cell_coeff_cc.compute()
    edge2cell_coeff_cc_t = pyic.xr_calc_edge2cell_coeff_cc_t(ds_IcD).compute()

    return ds_IcD, fixed_vol_norm, div_coeff, edge2cell_coeff_cc, edge2cell_coeff_cc_t, wet_e, wet_c

ds_IcD, fixed_vol_norm, div_coeff, edge2cell_coeff_cc, edge2cell_coeff_cc_t, wet_e, wet_c = get_grid_variables()
#####################################################################
### Calculate DIvergence of multiple layers
levsi = np.arange(depthi.size)
nzi   = levsi.size
levsc = np.arange(depthc.size)
nzc   = levsc.size

#calc divergence coefficients
cc = int(59799625)
ce = int(89813639)

# --- load times and flist # only to get time data - it is not the selection of data
path_data  = f'/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/????-??/'

time0           = np.datetime64('2010-03-31T01:00:00') #15T21:00:00
# memory          = 256
# memory_per_core = 60
# ncores          = np.floor(memory/memory_per_core)#in parallel
# gpu_time        = 8*60 #h
# compute_time    = 120 #min
# loops_per_node  = np.floor(gpu_time / compute_time)
# nsteps          = int(np.floor(loops_per_node * ncores))
# timeEnd         = time0 + np.timedelta64((nsteps-1),'2h')

timeEnd         = np.datetime64('2010-03-31T23:00:00') #22T21:00:00

ts         = pyic.timing(ts, 'load times and flist')
search_str = f'{run}_vn_sp_001-016_*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')

#time0 #timeEnd
itd0     = np.argmin((timesd-time0).astype(float)**2)
itdEnd   = np.argmin((timesd-timeEnd).astype(float)**2)

path_dat  = '/work/mh0287/from_Mistral/mh0287/users/leonidas/icon/ngSMT/results/'
month_dat = '2010-03/'

#### prep. dask cluster
search_str = f'{run}_vn_sp_*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()

VE = xr.open_mfdataset(flist,  chunks=dict(time=1, depthc=1, depthi=1))  # type: ignore

##################################################################################
steps = np.arange(itd0, itdEnd+1)
print('time0:   ', timesd[itd0], 'timeEnd: ', timesd[itdEnd], 'steps: ', steps.size)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro] # type: ignore
steps = list_all_pros[rank]

for nn, step in enumerate(steps): # type: ignore
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps))) # type: ignore
  ts = pyic.timing(ts, 'loop time step')
  t1 = timesd[step]
  t2 = timesd[step+1]
  print(t1, t2)
  if t1==t2:
    continue

  ts       = pyic.timing(ts, 'calculation w for one timestep')
  fpath    = flist_tsd[step]
  pdtime   = pd.to_datetime(timesd[step]) 
  tstr     = pdtime.strftime('%Y%m%d')+'T010000Z'
  fpatho   = f'/work/mh0033/m300878/smt/w/pp_calc_w_{timesd[step]}.nc'  

  print("Start Calculate vertical velocity for timestep", pdtime)  
  fo = Dataset(fpatho, 'w', format='NETCDF4')
  fo.createDimension('depthi', nzi) 
  fo.createDimension('cc', cc)   
  ncv    = fo.createVariable('depthi','f4','depthi')
  ncv[:] = depthi[:nzi]
  nc_w   = fo.createVariable('w','f4',('depthi','cc'))

  w0 = np.zeros([1, cc], dtype='f4')
  nc_w[depthi.size-1,:] = w0

  vet = VE.isel(time=step)

  print("Calculate Divergence/Calculate vertical Velocity")
  for lev in reversed(levsc):
      ts = pyic.timing(ts, 'loop level step')
      # --- load velocity on edges#

      ve = vet[f'vn{lev+1:03d}_sp']
      ve = ve.rename({'ncells': 'edge'})
      ve = ve.expand_dims(dim='depth')
      ve = ve.compute()

      #interpolated to cell center and back
      dze         = xr.DataArray(dzw[lev]*np.ones((1,ce)), dims=['depth', 'edge'])
      p_vn_c      = pyic.xr_edges2cell(ds_IcD, ve, dze, dzw[lev], edge2cell_coeff_cc=edge2cell_coeff_cc, fixed_vol_norm=fixed_vol_norm)
      p_vn_c      = p_vn_c.where(wet_c.isel(depthc=lev)!=0.)
      vei         = pyic.xr_cell2edges(ds_IcD, p_vn_c, edge2cell_coeff_cc_t=edge2cell_coeff_cc_t)
      vei         = vei.where(wet_e.isel(depthc=lev)!=0.)  #*dzw[lev]
      div_h       = pyic.xr_calc_div(ds_IcD, vei, div_coeff)
      w1          = w0 - div_h * dzw[lev]
      nc_w[lev,:] = w1
      w0          = w1


  fo.close()

print('finish calculate')