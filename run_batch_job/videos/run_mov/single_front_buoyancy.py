# %%
import sys
import matplotlib 
if len(sys.argv)>=2:
  matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import pyicon as pyic
import cartopy.crs as ccrs 
import pyicon.quickplots as pyicqp
import datetime
sys.path.append('/home/mpim/m300602/python/pytbx/mypy')
from matplotlib.dates import DateFormatter
import os
print(sys.argv)
import xarray as xr
import pandas as pd
sys.path.insert(0, "../../../")
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp


ts = pyic.timing([0], 'start')

run      = 'single'
path_fig = '../images/%s/%s/' % (__file__.split('/')[-1][:-3], run)
nnf      = 0
gname    = 'front1'
savefig  = True

if not os.path.exists(path_fig):
  try:
    os.makedirs(path_fig)
  except:
    pass

# %%

print('--- '+run)
##################################################################################
#### Load Data

path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
time_averaged = ''

fig_path = '/home/m/m300878/submesoscaletelescope/notebooks/images/first_paper/overview/'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'
fname = 'na'

lon_reg_2 = np.array([-66, -58])
lat_reg_2 = np.array([31, 37])
lon_reg_3 = np.array([-63.9, -61.5])
lat_reg_3 = np.array([34, 35.8])

asp = (lat_reg_2[1]-lat_reg_2[0])/(lon_reg_2[1]-lon_reg_2[0])
print(asp)
asp3 = (lat_reg_3[1]-lat_reg_3[0])/(lon_reg_3[1]-lon_reg_3[0])
print(asp3)

# %% instantan
ds  = eva.load_smt_b()
idepth = 50
ds  = ds.isel(depthi=idepth)

itd0   = 0
itdEnd = ds.time.shape
timesd = ds.time

m2 = np.abs(ds.dbdx) + np.abs(ds.dbdy)

# %%
clim_vort = 1
clim_sst  = 2, 26# close 14.5,21.7
clim_m2   = 0, 2e-7


# %%
def add_front_lines(ax):
    points = eva.get_points_of_inclined_fronts()
    # for ii in np.arange(int(points.shape[0]/2)):
    for ii in np.arange(12):
        if ii == 5 or ii == 10: continue
        P = points[2*ii:2*ii+2,:]
        y, x, m, b = eva.calc_line(P)
        delta=0.8#eva.convert_degree_to_meter(int(y[0]))*0.1
        ax.plot(x,y,lw=3, ls=':', color='k')
        # ax.plot(x,y+delta,lw=1, color='r')
        # ax.plot(x,y-delta,lw=1, color='r')
        t = ax.text(P[0,0], P[0,1], f'F{ii+1}', color='black', fontsize=20)
        t.set_bbox(dict(facecolor='white', alpha=0.6, edgecolor='white'))

def add_rectangles(ax, lon_reg, lat_reg, txt):
    rect, right, top = eva.draw_rect(lat_reg, lon_reg,  color='black')
    ax.add_patch(rect)
    t = ax.text(lon_reg[1], lat_reg[1], f'{txt}', color='black', fontsize=20)
    # t.set_bbox(dict(facecolor='white', alpha=0.6, edgecolor='white'))

# %%
##################################################################################

#steps = np.arange(IcD.times.size-1)
steps = np.arange(itd0, itdEnd[0]+1)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))

  t1 = timesd[step]
  t2 = timesd[step+1]
  print(t1, t2)
  if t1==t2:
    continue

  # <<< start time loop here
  #####################################################################
  ### load file for oine timestep
  print(f'Load files for timestep {timesd[step]}')

  m2_sel = m2.isel(time=step)
  # data_coarse = pyic.interp_to_rectgrid_xr(m2_sel, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
  # Tri, data_reg = eva.calc_triangulation_obj(m2_sel, lon_reg_3, lat_reg_3)
  Tri, data_reg = eva.calc_triangulation_obj(m2_sel, lon_reg_2, lat_reg_2)


  # %%
  # ================================================================================   
  # Here start plotting
  # ================================================================================   
  if nn==0:
    plt.close('all')
    ccrs_proj = ccrs.PlateCarree()
    cmap = 'RdPu_r'
    
    # ---
    hca, hcb = pyic.arrange_axes(2,1, plot_cb='right', asp=asp, fig_size_fac=2,
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, axlab_kw=None,
                                 )
    ii=-1
    
    # ---
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # data =  data_coarse
    clim = clim_m2


    # hm1 = pyic.shade(data.lon, data.lat, data.data, ax=ax, cax=cax, clim=clim, cmap=cmap,
    #                 transform=ccrs_proj, rasterized=False)
    hm1 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    add_front_lines(ax)
    add_rectangles(ax, lon_reg_3, lat_reg_3, 'a')

    ax.set_title(rf'|dbdx| + |dbdy| at {depthi[idepth]}m', fontsize = 15)

    pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2)

    
    # ---
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # clim = 17,18.7
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    

    points = eva.get_points_of_inclined_fronts()
    P = points[0:2,:]
    y, x, m, b = eva.calc_line(P)
    delta=0.8*0.1 # 0.8 view 0.1 actual front
    color = 'k'
    lw = 3
    ls = ':'
    # ax.plot(x,y, lw=lw, color=color, ls=ls)
    ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
    ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
    ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
    ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
    t = ax.text(P[0,0]-0.05, P[0,1], f'F{1}', color='black', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


    #----
    ht = ax.set_title(str(f'{timesd[step].data:.13}h').replace('T',' '), loc='right', fontsize=10)
    
    pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3, do_xyticks=False)


    if not savefig:
      plt.show()
      sys.exit()
  else:
    ## --- update figures
    hm1[0].set_array(data_reg)
    hm2[0].set_array(data_reg)

    ht.set_text(str(f'{timesd[step].data:.13}h').replace('T',' '))

  if savefig:
    fpath = '%s%s_%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], run, step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=300)
  else:
    plt.draw()
    input()

ts = pyic.timing(ts, 'all done')
# %%
