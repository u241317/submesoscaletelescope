#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --time=01:00:00
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j
#SBATCH --ntasks=85
#SBATCH --partition=compute
#SBATCH --account=mh0033

module list
source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`

run="randompath1"
path_data="randompath2"
# mpirun -np 8 python -u single_front.py ${path_data} ${run}
# mpirun -np 10 python -u single_front_temperature.py ${path_data} ${run}
# mpirun -np 32 python -u single_front_buoyancy.py ${path_data} ${run}
mpirun -np 85 python -u single_front_temperature_layers.py ${path_data} ${run}

enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

