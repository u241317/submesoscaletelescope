import sys
import numpy as np
import matplotlib
if len(sys.argv)==2:
  matplotlib.use('Agg')
import matplotlib.pyplot as plt
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import cartopy.crs as ccrs
import glob
#import pickle
#import maps_icon_smt_vort as smt
import os

run = 'ngSMT_tke'
savefig = True
path_fig = '../movies/%s_%s/' % (__file__.split('/')[-1][:-3], run)
nnf=0

if not os.path.exists(path_fig):
  os.makedirs(path_fig)

path_data = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-02/'
gname = 'smt'
path_grid = f'/mnt/lustre01/work/mh0033/m300602/icon/grids/{gname}/'
path_ckdtree  = f'{path_grid}ckdtree/'
fpath_tgrid = '/home/mpim/m300602/work/icon/grids/smt/smt_tgrid.nc'

lon_reg_1 = [-88, 0]
lat_reg_1 = [ 5, 68] 

step = 20

ts = pyic.timing([0], 'start')

# --- load times and flist
#search_str = 'submeso_NA_vort_f_cells_50m*.nc'
search_str = f'{run}_vort_f_50m*.nc'
flist = np.array(glob.glob(path_data+search_str))
flist.sort()
times, flist_ts, its = pyic.get_timesteps(flist, time_mode='float2date')

#40 and 60W or 35-65W, and either 0-20N or 5-15N.

# --- crop grid
fpath_ckdtree = f'{path_grid}ckdtree/rectgrids/{gname}_res0.02_180W-180E_90S-90N.npz'
ddnpz = np.load(fpath_ckdtree)
lon = ddnpz['lon'] 
lat = ddnpz['lat'] 
lon_reg = [-65, -35]
lat_reg = [0, 20]
Lon, Lat = np.meshgrid(lon, lat)
Lon, Lat, lon, lat, ind_reg, indx, indy = pyic.crop_regular_grid(lon_reg, lat_reg, Lon, Lat)

# --- load data
ts = pyic.timing(ts, 'load data')
f = Dataset(flist_ts[step], 'r')
ro = f.variables['vort_f_cells_50m'][its[step],:]
f.close()

# --- interpolate to rectgrid
ts = pyic.timing(ts, 'apply ckdtree')
lon, lat, roi = pyic.interp_to_rectgrid(ro, fpath_ckdtree, mask_reg=ind_reg, indx=indx, indy=indy, coordinates='clat clon')
roi[roi==0.] = np.ma.masked

# -------------------------------------------------------------------------------- 
# Here start plotting
# -------------------------------------------------------------------------------- 
plt.close('all')
clim = 2.
ccrs_proj = ccrs.PlateCarree()

ts = pyic.timing(ts, 'start plottin')

# ---
hca, hcb = pyic.arrange_axes(1,1, plot_cb=True, fig_size_fac=2, asp=0.667, 
                             axlab_kw=None, projection=ccrs_proj,
                             dfigl=-0.2, dfigr=-1,
                             )
ii=-1

cax = hcb[0]
cax.set_position((0.13, 0.15, 0.02, 0.3)) 

ts = pyic.timing(ts, 'plot 1')
ii+=1; ax=hca[ii]; cax=hcb[ii]
hm1 = pyic.shade(lon, lat, roi, ax=ax, cax=cax, clim=2, transform=ccrs_proj)
ax.set_title('rel. vort. / plan. vort.', fontsize=10)
ax.set_title('ICON-O SMT', loc='left', fontsize=10)
ht = ax.set_title(str(times[step]).replace('T',' '), loc='right', fontsize=10)
for ax in hca:
  pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg, xticks=np.arange(-65,35,10))

def update_fig(step):
  # --- load data
  f = Dataset(flist_ts[step], 'r')
  ro = f.variables['vort_f_cells_50m'][its[step],:]
  f.close()
  
  # --- interpolate to rectgrid
  lon, lat, roi = pyic.interp_to_rectgrid(ro, fpath_ckdtree, mask_reg=ind_reg, indx=indx, indy=indy, coordinates='clat clon')
  roi[roi==0.] = np.ma.masked

  # --- update hm1
  hm1[0].set_array(roi[1:,1:].flatten())
  ht.set_text(str(times[step]).replace('T',' '))
  return #ro_reg, ro_002deg_r1, ro_002deg_r2, tstr

steps = np.arange(flist_ts.size)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

# --- start loop
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
  ts = pyic.timing(ts, 'loop step')

  update_fig(step)

  nnf+=1
  if savefig:
    fpath = '%s%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=300)

ts = pyic.timing(ts, 'all done')

#plt.show()
