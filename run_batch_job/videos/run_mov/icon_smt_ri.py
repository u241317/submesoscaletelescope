import sys
import matplotlib 
if len(sys.argv)>=2:
  matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset, num2date
import pyicon as pyic
import cartopy.crs as ccrs 
import pyicon.quickplots as pyicqp
import datetime
sys.path.append('/home/mpim/m300602/python/pytbx/mypy')
import my_toolbox as my
from matplotlib.dates import DateFormatter
import os
print(sys.argv)
import xarray as xr
import pandas as pd
import glob
import pickle
from timezonefinder import TimezoneFinder
import re
#import matplotlib.gridspec as gridspec
from icon_smt_levels import dzw, dzt, depthc, depthi

# if len(sys.argv)==1:
#   run       = 'hel20134-STR'
#   path_data = '/work/mh0033/m211054/projects/icon/ruby/icon-oes_fluxadjust/experiments/'+run+'/'
#   savefig   = False
# else:
#   path_data = sys.argv[1]
#   run       = sys.argv[2]
#   savefig   = True

ts = pyic.timing([0], 'start')

run      = 'ngSMT_tke'
path_fig = '../images/%s/%s/' % (__file__.split('/')[-1][:-3], run)
nnf      = 0
gname    = 'smt'
lev      = 'L128'
savefig  = True

if not os.path.exists(path_fig):
  try:
    os.makedirs(path_fig)
  except:
    pass


fpath_tgrid  = '/home/mpim/m300602/work/icon/grids/smt/smt_tgrid.nc'
#fpath_Tri    = '/mnt/lustre01/work/mh0033/m300602/tmp/Tri.pkl'
path_grid     = f'/mnt/lustre01/work/mh0033/m300602/icon/grids/{gname}/'
path_ckdtree  = f'{path_grid}ckdtree/'
fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'


print('--- '+run)
##################################################################################
#### Load Data
## depth
iz = 3
print('depth =', depthi[iz])
## coriolis
f=2*2*np.pi/86400*np.sin(40*np.pi/180.)
f2 = f**2

#lon1 = [-60, -55]
#lon1 = [-65, -55]
#lat1 = [36.5, 41.5]
#lon1 = [-70, -60]
#lat1 = [32, 37]
lon_reg = [-75, -55]
lat_reg = [33, 43]

tf = TimezoneFinder()
latitude, longitude = lon1[0]-(lon1[0]-lon1[1])/2, lat1[0]-(lat1[0]-lat1[1])/2
timezone = tf.timezone_at(lng=longitude, lat=latitude)
TZ = int(re.search(r'\d+', timezone).group())
print('latitude', latitude, 'longitudte', longitude, 'timezone', timezone, 'dTZ', TZ)


# --- load times and flist
path_data  = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/????-??/'
time0      = np.datetime64('2010-03-15T23:00:00')
timeEnd    = np.datetime64('2010-03-22T21:00:00')
ts         = pyic.timing(ts, 'load times and flist')
search_str = f'{run}_T_S_sp_001-016_*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')

#local time
timesd_local = timesd - np.timedelta64(TZ, 'h')

#time0 #timeEnd
itd0     = np.argmin((timesd-time0).astype(float)**2)
itdEnd   = np.argmin((timesd-timeEnd).astype(float)**2)

# read out vorticity
varfile   = 'vort_f_50m'
path_dat  = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/'
month_dat = '2010-03/'

# path_data = '/work/mh0033/u241317/smt/forcing/windstress.nc'
# windstress = xr.open_dataset(path_data)
# lonw = windstress.lon
# latw = windstress.lat

path_data = '/work/mh0033/u241317/smt/forcing/ustressSelL.nc'
ustress   = xr.open_dataset(path_data)
lonw      = ustress.lon #.data
latw      = ustress.lat #.data

path_data  = '/work/mh0033/u241317/smt/forcing/vstressSelL.nc'
vstress    = xr.open_dataset(path_data)
windstress = np.sqrt(ustress**2 + vstress**2)


##################################################################################

#steps = np.arange(IcD.times.size-1)
steps = np.arange(itd0, itdEnd+1)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))

  t1 = timesd[step]
  t2 = timesd[step+1]
  print(t1, t2)
  if t1==t2:
    continue

  # <<< start time loop here
  #####################################################################
  ### load file for oine timestep
  print('Load files for timestep', {timesd[step]})
  path_data = f'/work/mh0033/u241317/smt/w/pp_calc_w_{timesd[step]}.nc'
  dw        = xr.open_dataset(path_data)
  w         = dw.w.isel(depthi=iz)
  # w         = w.rename({'vert_velocity': 'cc'}) # only necessary for corrupted dims

  path_data = f'/work/mh0033/u241317/smt/db/pp_calc_b_period_{timesd[step]}.nc'
  db        = xr.open_dataset(path_data)
  b         = db.b.isel(depthi=iz)
  dbdx      = db.dbdx.isel(depthi=iz)
  dbdy      = db.dbdy.isel(depthi=iz)
  M4        = (dbdx**2 + dbdy**2)

  # calc b*w
  wb = b*w

  ### calc balanced RIchardson number
  path_data  = f'/work/mh0033/u241317/smt/N2/pp_calc_N2_period_{timesd[step]}.nc'
  dN2        = xr.open_dataset(path_data)
  N2         = dN2.N2.isel(depthi=iz)
  Ri_b       = N2 * f2  / M4 

  ### vorticity
  pdtime    = pd.to_datetime(timesd[step]) 
  tstr      = pdtime.strftime('%Y%m%d')+'T010000Z'
  fname     = f'{run}_{varfile}_{tstr}.nc'
  fpath     = f'{path_dat}{month_dat}{fname}'
  ds_vort   = xr.open_dataset(fpath)
  vort      = ds_vort.vort_f_cells_50m.isel(time=itsd[step])

  ### load windstress
  #windstress.sel(time=timesd[step])

  lon, lat, toi1 = pyic.interp_to_rectgrid(vort, fpath_ckdtree, lon_reg=lon1, lat_reg=lat1)
  lon, lat, toi2 = pyic.interp_to_rectgrid(wb, fpath_ckdtree, lon_reg=lon1, lat_reg=lat1)
  #lon, lat, toi2 = pyic.interp_to_rectgrid(b, fpath_ckdtree, lon_reg=lon1, lat_reg=lat1)
  lon, lat, toi3 = pyic.interp_to_rectgrid(Ri_b, fpath_ckdtree, lon_reg=lon1, lat_reg=lat1)
  #lon, lat, toi4 = pyic.interp_to_rectgrid(w, fpath_ckdtree, lon_reg=lon1, lat_reg=lat1)
  # toi4 = windstress.var180.sel(time=timesd[step])
  # toi4 = toi4.data
  w4 = windstress.var180.sel(time=timesd[step]).data
  u4 = ustress.var180.sel(time=timesd[step]).data
  v4 = vstress.var180.sel(time=timesd[step]).data
  
  # ================================================================================   
  # Here start plotting
  # ================================================================================   
  if nn==0:
    plt.close('all')
    ccrs_proj = ccrs.PlateCarree()
    
    # ---
    hca, hcb = pyic.arrange_axes(2,2, plot_cb=True, asp=0.5, fig_size_fac=1.5,
                                 sharex=True, sharey=True, xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, axlab_kw=None,
                                 )
    ii=-1
    
    # ---
    clim = 1 #0.5
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    #lon, lat, toi1 = pyic.interp_to_rectgrid(vort, fpath_ckdtree, lon_reg=lon1, lat_reg=lat1)
    hm1 = pyic.shade(lon, lat, toi1, ax=ax, cax=cax, transform=ccrs_proj, rasterized=False, clim=clim)
    ax.set_title('Vorticity at 50m')
    ax.set_xlim(lon1)
    ax.set_ylim(lat1)
    
    # ---
    clim = 0.000008
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    #lon, lat, toi2 = pyic.interp_to_rectgrid(wb, fpath_ckdtree, lon_reg=lon1, lat_reg=lat1)
    hm2 = pyic.shade(lon, lat, toi2, ax=ax, cax=cax, transform=ccrs_proj, rasterized=False, clim=clim, cmap='RdYlBu_r')
    ax.set_title(f'wb at %.1fm,TZ{timezone}'%(depthi[iz]))
    ax.set_xlim(lon1)
    ax.set_ylim(lat1)
    
    #clim = clim= (-0.013, -0.019) #good for North
    # clim = clim= (-0.009, -0.015)
    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # #lon, lat, toi2 = pyic.interp_to_rectgrid(wb, fpath_ckdtree, lon_reg=lon1, lat_reg=lat1)
    # hm2 = pyic.shade(lon, lat, toi2, ax=ax, cax=cax, transform=ccrs_proj, rasterized=False, clim=clim, cmap='RdYlBu_r')
    # ax.set_title(f'b at %.1fm, at loc TZ: {timezone}'%(depthi[iz]))
    # ax.set_xlim(lon1)
    # ax.set_ylim(lat1)

    ht = ax.set_title(str(timesd_local[step]).replace('T',' '), loc='right', fontsize=10)
    
    # ---
    clim = np.array([-1, 9])
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    #lon, lat, toi3 = pyic.interp_to_rectgrid(Ri_b, fpath_ckdtree, lon_reg=lon1, lat_reg=lat1)
    hm3 = pyic.shade(lon, lat, toi3, ax=ax, cax=cax, transform=ccrs_proj, rasterized=False, clim=clim, cmap='tab10')
    #hm3 = pyic.shade(lon, lat, toi3, ax=ax, cax=cax, transform=ccrs_proj, rasterized=False,     clevs=(0,1,10,100,1000), clim=(0, 1000), cmap='RdYlBu')
    ax.set_title('balanced Ri at %.1fm'%(depthi[iz]))
    ax.set_xlim(lon1)
    ax.set_ylim(lat1)
    
    # ---
    # clim = np.array([0, 1])
    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # #lon, lat, toi4 = pyic.interp_to_rectgrid(w, fpath_ckdtree, lon_reg=lon1, lat_reg=lat1)
    # hm4 = pyic.shade(lonw, latw, toi4, ax=ax, cax=cax, transform=ccrs_proj, rasterized=False, clim=clim, cmap='RdYlBu_r')
    # ax.set_title('mag. windstress in N/m^2')
    # ax.set_xlim(lon1)
    # ax.set_ylim(lat1)

    clim = np.array([0, 0.6])
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm4 = pyic.shade(lonw, latw, w4, ax=ax, cax=cax, transform=ccrs_proj, rasterized=False, clim=clim, cmap='RdYlBu_r')
    #ax.pcolor(lonw, latw, w4, cmap='RdYlBu_r') #c=
    #fig.colorbar(c, ax=ax)
    hm5 = ax.quiver(lonw, latw, u4, v4)
    ax.set_title('mag. windstress in N/m^2')
    ax.set_xlim(lon1)
    ax.set_ylim(lat1)


    #----
    

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon1, ylim=lat1)

    if not savefig:
      plt.show()
      sys.exit()
  else:
    ## --- update figures
    hm1[0].set_array(toi1.flatten())
    hm2[0].set_array(toi2.flatten())
    hm3[0].set_array(toi3.flatten())
    #hm3[0].set_array(toi4.flatten())

    hm4[0].set_array(w4.flatten())
    # #hm5[0].set_array(u4.flatten())
    # #hm5[0].set_array(v4.flatten())
    hm5.set_UVC(u4.flatten(), v4.flatten())

    ht.set_text(str(timesd[step]).replace('T',' '))

  if savefig:
    fpath = '%s%s_%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], run, step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=300)
  else:
    plt.draw()
    input()

ts = pyic.timing(ts, 'all done')