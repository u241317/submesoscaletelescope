import sys
import matplotlib 
if len(sys.argv)>=2:
  matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset, num2date
import pyicon as pyic
import cartopy.crs as ccrs 
import pyicon.quickplots as pyicqp
import datetime
sys.path.append('/home/mpim/m300602/python/pytbx/mypy')
import my_toolbox as my
from matplotlib.dates import DateFormatter
import os
print(sys.argv)
import xarray as xr
import pandas as pd
import glob
import pickle
from timezonefinder import TimezoneFinder
import re
#import matplotlib.gridspec as gridspec
from icon_smt_levels import dzw, dzt, depthc, depthi


ts = pyic.timing([0], 'start')

run      = 'satellite'
path_fig = '../images/%s/%s/' % (__file__.split('/')[-1][:-3], run)
nnf      = 0
gname    = 'sat'
savefig  = True

if not os.path.exists(path_fig):
  try:
    os.makedirs(path_fig)
  except:
    pass



print('--- '+run)
##################################################################################
#### Load Data
#lon_reg = [-80, -30]
#lat_reg = [25, 50]
lon_reg = [-80, -20]
lat_reg = [10, 60]
clim = 0, 24

# --- load times and flist
ts         = pyic.timing(ts, 'load times and flist')

path_data  = '/pool/data/ICDC/ocean/modis_aqua_sst/DATA/daily/2010/'
search_str = f'MODIS-AQUA__C6__SST_v2019.0__4km__2010*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()

time0      = np.datetime64('2010-01-01T00:00:00')
timesd     = pd.date_range(time0, periods=90, freq="1D")

timeEnd    = np.datetime64('2010-03-31T00:00:00')

#time0 #timeEnd
itd0     = 0 #timesd[0]
itdEnd   = 89 #timesd[89]

sat = xr.open_mfdataset(flist[:90], combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
sat = sat.where((sat.lat > lat_reg[0]) & (sat.lat < lat_reg[1]) & (sat.lon > lon_reg[0]) & (sat.lon < lon_reg[1]), drop=True)

lon = sat.lon.data
lat = sat.lat.data

##################################################################################

#steps = np.arange(IcD.times.size-1)
steps = np.arange(itd0, itdEnd+1)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))

  t1 = timesd[step]
  t2 = timesd[step+1]
  print(t1, t2)
  if t1==t2:
    continue

  # <<< start time loop here
  #####################################################################
  ### load file for oine timestep
  print('Load files for timestep', {timesd[step]})
  sst_day   = sat.isel(time=step).sst_day.data
  sst_night = sat.isel(time=step).sst_night.data

  
  # ================================================================================   
  # Here start plotting
  # ================================================================================   
  if nn==0:
    plt.close('all')
    ccrs_proj = ccrs.PlateCarree()
    
    # ---
    hca, hcb = pyic.arrange_axes(2,1, plot_cb=True, asp=0.84, fig_size_fac=2,
                                 sharex=True, sharey=True, xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, axlab_kw=None,
                                 )
    ii=-1
    
    # ---
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(lon, lat, sst_day, ax=ax, cax=cax, clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title('sst day')
    ax.set_ylabel('lat')
    
    # ---
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    #lon, lat, toi2 = pyic.interp_to_rectgrid(wb, fpath_ckdtree, lon_reg=lon1, lat_reg=lat1)
    hm2 = pyic.shade(lon, lat, sst_night, ax=ax, cax=cax, clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title('sst night')
    ax.set_ylabel('lat')
    #----
    ht = ax.set_title(str(timesd[step]).replace('T',' '), loc='right', fontsize=10)

    
    for ax in hca:
      pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if not savefig:
      plt.show()
      sys.exit()
  else:
    ## --- update figures
    hm1[0].set_array(sst_day.flatten())
    hm2[0].set_array(sst_night.flatten())

    ht.set_text(str(timesd[step]).replace('T',' '))

  if savefig:
    fpath = '%s%s_%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], run, step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=300)
  else:
    plt.draw()
    input()

ts = pyic.timing(ts, 'all done')