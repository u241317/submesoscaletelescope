#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --time=01:00:00
#SBATCH --output=log.o-%j.out
#SBATCH --error=log.o-%j.out
#SBATCH --ntasks=4
#SBATCH --partition=compute,compute2
#####SBATCH --account=bm0371
#SBATCH --account=mh0033

#module load python/2.7.12
module list
source /work/mh0033/u241317/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`

run="randompath1"
path_data="randompath2"
mpirun -np 4 python -u satellite_sst.py ${path_data} ${run}

enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

