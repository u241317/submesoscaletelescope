#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --time=08:00:00
#SBATCH --output=log.o-%j.out
#SBATCH --error=log.o-%j.out
#SBATCH --ntasks=24
#SBATCH --partition=compute,compute2
#####SBATCH --account=bm0371
#SBATCH --account=mh0033

#module load python/2.7.12
module list

#source /home/mpim/m300602/bin/myactcondenv.sh
source /home/mpim/m300602/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`

#run="hel20134-STR"
#path_data="/work/mh0033/m211054/projects/icon/ruby/icon-oes_fluxadjust/experiments/${run}/"
#run="dap7027-r0"
run="dap7023-r0"
path_data="/work/mh0033/m300466/icon-ruby/icon-ruby2b/experiments/${run}/"
mpirun -np 24 python -u icon_ruby_na_circ_mov.py ${path_data} ${run}

enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

