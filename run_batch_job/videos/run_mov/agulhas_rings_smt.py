# %%
import sys
import matplotlib 
if len(sys.argv)>=2:
  matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import pyicon as pyic
import cartopy.crs as ccrs 
ccrs_proj = ccrs.PlateCarree()

import matplotlib.patches as patches

import os
import xarray as xr
import pandas as pd
import glob
from icon_smt_levels import dzw, dzt, depthc, depthi

sys.path.insert(0, "../../../")
import smt_modules.all_funcs as eva

# %%

def add_subplot_axes(ax,rect,axisbg='w'):
    fig             = plt.gcf()
    box             = ax.get_position()
    width           = box.width
    height          = box.height
    inax_position   = ax.transAxes.transform(rect[0:2])
    transFigure     = fig.transFigure.inverted()
    infig_position  = transFigure.transform(inax_position)
    x               = infig_position[0]
    y               = infig_position[1]
    width          *= rect[2]
    height         *= rect[3]  # < = Typo was here
    #subax          = fig.add_axes([x,y,width,height],facecolor=facecolor)  # matplotlib 2.0+
    subax           = fig.add_axes([x,y,width,height])
    x_labelsize     = subax.get_xticklabels()[0].get_size()
    y_labelsize     = subax.get_yticklabels()[0].get_size()
    x_labelsize    *= rect[2]**0.5
    y_labelsize    *= rect[3]**0.5
    subax.xaxis.set_tick_params(labelsize=x_labelsize)
    subax.yaxis.set_tick_params(labelsize=y_labelsize)
    return subax
# %%


ts = pyic.timing([0], 'start')

run      = 'agulhas_rings_smt'
path_fig = '../images/%s/%s/' % (__file__.split('/')[-1][:-3], run)
nnf      = 0
savefig  = True

if not os.path.exists(path_fig):
  try:
    os.makedirs(path_fig)
  except:
    pass


# %%

print('--- '+run)
##################################################################################
#### Load Data
lon_reg = [-25, 35]
lat_reg = [-50, -5]
asp     = (lat_reg[1]-lat_reg[0]) / (lon_reg[1]-lon_reg[0])

lon_reg_zoom = [-5, 12.34]
lat_reg_zoom = [-38, -25]
asp_z        = (lat_reg_zoom[1]-lat_reg_zoom[0]) / (lon_reg_zoom[1]-lon_reg_zoom[0])
rectangle = patches.Rectangle((lon_reg_zoom[0], lat_reg_zoom[0]), lon_reg_zoom[1]-lon_reg_zoom[0], lat_reg_zoom[1]-lat_reg_zoom[0], linewidth=1, edgecolor='r', facecolor='none')

ds_1, ds_2 = eva.load_mooring_data()
pos = [(ds_1.longitude.data, ds_1.latitude.data), (ds_2.longitude.data, ds_2.latitude.data)]

# %%
# --- load times and flist
ts             = pyic.timing(ts, 'load times and flist')

# %%
path = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/'
data = xr.open_dataset(path+'smtwv0008_ssh_02.nc')
# data = xr.open_dataset(path+'exp7_KE_100m.nc')
# data = data.rename({'__xarray_dataarray_variable__': 'KE'})

print('data loaded')

# %%
itd0   = 0
itdEnd = data.time.shape
timesd = data.time.data

data       = data.where((data.lat > lat_reg[0]) & (data.lat < lat_reg[1]) & (data.lon > lon_reg[0]) & (data.lon < lon_reg[1]), drop=True)
data_zoom  = data.where((data.lat > lat_reg_zoom[0]) & (data.lat < lat_reg_zoom[1]) & (data.lon > lon_reg_zoom[0]) & (data.lon < lon_reg_zoom[1]), drop=True)

# data_zoom_mean = data_zoom.mean(dim='time').mean(dim='lat').mean(dim='lon')
# data       = data - data_zoom_mean
# data_zoom = data_zoom - data_zoom_mean

lon   = data.lon.data
lat   = data.lat.data
lon_z = data_zoom.lon.data
lat_z = data_zoom.lat.data


##################################################################################
steps = np.arange(itd0, itdEnd[0])
print('delegate steps')
# %%
# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

print('selected steps',steps)
# %%
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
# %%
  t1 = timesd[step]
  t2 = timesd[step+1]
  print(t1, t2)
  if t1==t2:
    continue

  ### load file for oine timestep
  # %%
  print(f'my timestep: {step}')
  if True:
    zos      = data.isel(time=step).zos
    print(zos.time.data)
    zos_zoom = data_zoom.isel(time=step).zos
    zos      = zos.data
    zos_zoom = zos_zoom.data

    clim    = -0.5, 0.5
    cticks  = 6
    cbticks = np.linspace(clim[0],clim[1],cticks)
    cmap    = 'PuOr'  # 'RdYlGn'
  else:
    KE = data.isel(time=step).KE
    KE = KE.data
    KE_zoom = data_zoom.isel(time=step).KE
    KE_zoom = KE_zoom.data

    clim    = 0, 0.25
    cticks  = 6
    cbticks = np.linspace(clim[0],clim[1],cticks)
    cmap    = 'plasma'

  # %
  # ================================================================================   
  # Here start plotting
  # ================================================================================   
  # if nn==0:
  #   plt.close('all')
  # %%
  hca, hcb = pyic.arrange_axes(2,1, plot_cb='right', asp=asp, fig_size_fac=2,
                               sharex=False, sharey=False, xlabel="", ylabel="",
                               projection=ccrs_proj,
                               daxr=-1, dcbl=1.3, axlab_kw=None,
                               )
  ii=-1
  
  # ---
  if True:
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(lon, lat, zos, ax=ax, cax=cax, clim=clim, cmap= cmap, transform=ccrs_proj, rasterized=False, nclev=25, contfs=True, cbticks=cbticks)
    ax.set_title('SMT EXP7 zos [m]')
    rectangle = patches.Rectangle((lon_reg_zoom[0], lat_reg_zoom[0]), lon_reg_zoom[1]-lon_reg_zoom[0], lat_reg_zoom[1]-lat_reg_zoom[0], linewidth=1, edgecolor='r', facecolor='none')
    ax.add_patch(rectangle)
    ht = ax.set_title(f'{timesd[step]:.10}'.replace('T',' '), loc='right', fontsize=10)
    ax.plot(pos[0][0], pos[0][1],  'r*', markersize=20, transform=ccrs_proj, label='Mooring Position')
    ax.plot(pos[1][0], pos[1][1],  'b*', markersize=20, transform=ccrs_proj)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(lon_z, lat_z, zos_zoom, ax=ax, cax=cax, clim=clim, cmap= cmap, transform=ccrs_proj, rasterized=False, nclev=25, contfs=True, cbticks=cbticks)
    ax.plot(pos[0][0], pos[0][1],  'r*', markersize=20, transform=ccrs_proj, label='Mooring Position')
    ax.plot(pos[1][0], pos[1][1],  'b*', markersize=20, transform=ccrs_proj)
  else:
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(lon, lat, KE, ax=ax, cax=cax, clim=clim, cmap= cmap, transform=ccrs_proj, rasterized=False, nclev=25, contfs=True, cbticks=cbticks)
    ax.set_title('SMT KE')
    rectangle = patches.Rectangle((lon_reg_zoom[0], lat_reg_zoom[0]), lon_reg_zoom[1]-lon_reg_zoom[0], lat_reg_zoom[1]-lat_reg_zoom[0], linewidth=1, edgecolor='r', facecolor='none')
    ax.add_patch(rectangle)
    ht = ax.set_title(f'{timesd[step]:.10}'.replace('T',' '), loc='right', fontsize=10)
    ax.plot(pos[0][0], pos[0][1],  'r*', markersize=20, transform=ccrs_proj, label='Mooring Position')
    ax.plot(pos[1][0], pos[1][1],  'b*', markersize=20, transform=ccrs_proj)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(lon_z, lat_z, KE_zoom, ax=ax, cax=cax, clim=clim, cmap= cmap, transform=ccrs_proj, rasterized=False, nclev=25, contfs=True, cbticks=cbticks)
    ax.plot(pos[0][0], pos[0][1],  'r*', markersize=20, transform=ccrs_proj, label='Mooring Position')
    ax.plot(pos[1][0], pos[1][1],  'b*', markersize=20, transform=ccrs_proj)
    # pyic.plot_settings(ax, xlim=lon_reg_zoom, ylim=lat_reg_zoom)


  if savefig:
    fpath = '%s%s_%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], run, step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, bbox_inches='tight', dpi=250)
  else:
    plt.draw()
    input()

ts = pyic.timing(ts, 'all done')

# %%
