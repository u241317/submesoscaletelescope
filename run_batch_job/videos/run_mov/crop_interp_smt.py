""" 1: script to crop and interpolate ssh data for eddy tracker
    2: script to compute kinetic energy from u and v for eddy tracker"""

# %%
import sys
import matplotlib 
# if len(sys.argv)>=2:
#   matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import pyicon as pyic
import cartopy.crs as ccrs 
ccrs_proj = ccrs.PlateCarree()

import matplotlib.patches as patches

import os
import xarray as xr
import pandas as pd
import glob
from icon_smt_levels import dzw, dzt, depthc, depthi

sys.path.insert(0, "../../../")
import smt_modules.all_funcs as eva

import smt_modules.init_slurm_cluster as scluster 
from importlib import reload

# %% get cluster

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00')
cluster
client


# %%
##################################################################################
##################################### SSH ########################################
if True:
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'
    path          = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/03deg/'
else:
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
    path          = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/'

# %%
##################################################################################
#### Load Data
# lon_reg = [-25, 35]
# lat_reg = [-50, -5]
lon_reg = [-15, 20]
lat_reg = [-36, -20]
asp     = (lat_reg[1]-lat_reg[0]) / (lon_reg[1]-lon_reg[0])
# %%
mask_land = eva.load_smt_wave_land_mask()
mask_interp = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree, lon_reg, lat_reg)

# %%
ds_tides_tmean = eva.load_smt_wave_2d_exp7_07(it=1)
# begin          = '2019-07-09T00:00:00'
begin          = '2019-07-06T07:15:00'
end            = '2019-07-18T23:15:00'
p1             = ds_tides_tmean.zos.sel(time=slice(begin, end))
p1             = p1.drop_duplicates('time')
p1             = p1.isel(time=slice(0,239)) #  smtwv0007_oce_2d_PT1H_20190718T231500Z.nc defect
p1             = p1.drop('clon').drop('clat')
# %%
reload(eva)
ds_tides_tmean_2 = eva.load_smt_wave_2d_exp7_07_part2(it=1)
begin            = '2019-07-19T00:00:00'
end              = '2019-08-31T23:15:00'
p2               = ds_tides_tmean_2.zos.sel(time=slice(begin, end))
p2               = p2.drop('clon').drop('clat')
# %%
reload(eva)
ds_tides_tmean_3 = eva.load_smt_wave_2d_exp7_07_part3(it=1)
p3               = ds_tides_tmean_3.zos
# %%
exp7 = xr.concat([p1, p2, p3], dim='time')
exp7 = exp7.drop_duplicates('time')

if True: # only for 3d
    begin = pd.to_datetime('2019-07-06T07:15:00')
    end   = pd.to_datetime('2019-10-17T23:15:00')
    exp7 = exp7.sel(time=slice(begin, end))
# %%
# select every 24th value
data_tsel           = exp7.isel(time=slice(0,exp7.time.shape[0],1))
data                = pyic.interp_to_rectgrid_xr(data_tsel, fpath_ckdtree, lon_reg, lat_reg)
data                = data * mask_interp
data.name           = 'zos'
data.attrs['units'] = 'm'
data.to_netcdf(path+'smtwv0007_ssh_old.nc')

# %%
exp9 = eva.load_smt_wave_9('2d', it=1)
exp9 = exp9.zos
# %%
data_tsel           = exp9.isel(time=slice(0,exp9.time.shape[0],1))
data                = pyic.interp_to_rectgrid_xr(data_tsel, fpath_ckdtree, lon_reg, lat_reg)
data                = data * mask_interp
data.name           = 'zos'
data.attrs['units'] = 'm'
data.to_netcdf(path+'smtwv0009_ssh_fine.nc')
# %%
ds_tides_22 = eva.load_smt_wave_all('2d', exp=8, it=1)
exp8         = ds_tides_22.drop('clon').drop('clat').zos
exp8         = exp8.drop_duplicates('time')
# %%
data_tsel    = exp8.isel(time=slice(0,exp8.time.shape[0],24))
data         = pyic.interp_to_rectgrid_xr(data_tsel, fpath_ckdtree, lon_reg, lat_reg)
data.to_netcdf(path+'smtwv0008_ssh_02.nc')





# %%  ######################################Kinetic ENERGY##################################################################################
##################################################################################
# idepth     = 17
# idepth_lev = 1
# idepth     = 76
# idepth_lev = 3

idepth     = 26
idepth_lev = 2
# %% EXP7

begin = '2019-07-06T01:15:00'
end   = '2019-10-17T23:15:00'

ds_u1     = eva.load_smt_wave_all('u', exp=7, remove_bnds=True)
ds_u1     = ds_u1.isel(depth=idepth).u
ds_u1     = ds_u1.drop('clon').drop('clat')
ds_levels = eva.load_smt_wave_levels(7, it=2)
ds_levels = ds_levels.rename({'ncells_2': 'ncells'})
ds_u2     = ds_levels.isel(depth=idepth_lev).u
ds_u      = xr.concat([ds_u1, ds_u2], dim='time').sel(time=slice(begin, end)) 

ds_v1 = eva.load_smt_wave_all('v', exp=7, remove_bnds=True)
ds_v1 = ds_v1.isel(depth=idepth).v
ds_v1 = ds_v1.drop('clon').drop('clat')

time_missing = '2019-07-08T01:15:00' #20190708T0115
infilled_data_set = ds_v1.resample(time="2H").asfreq()
infilled_data_set = infilled_data_set.assign_coords(time=infilled_data_set.time.data + (ds_v1.time.data[0] - infilled_data_set.time.data[0]))

ds_v2 = ds_levels.isel(depth=idepth_lev).v
ds_v  = xr.concat([infilled_data_set, ds_v2], dim='time').sel(time=slice(begin, end)) 

# %%
data        = ds_u**2 /2 + ds_v**2 /2
data_tsel   = data.isel(time=slice(0,data.time.shape[0],1))
data_interp = pyic.interp_to_rectgrid_xr(data_tsel, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data        = data_interp * mask_interp
data.name   = 'KE'
data.to_netcdf(f'{path}/exp7_KE_100m_fine.nc')
# %%

fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
path          = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/'
mask_land     = eva.load_smt_wave_land_mask()
mask_interp   = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree, lon_reg, lat_reg)

# %% exp9
begin = '2019-07-01T03:15:00'
end   = '2019-10-19T23:15:00'

reload(eva)
ds_levels = eva.load_smt_wave_levels(9, it=2)
ds_levels = ds_levels.rename({'ncells_2': 'ncells'})
# %%

ds_u1 = eva.load_smt_wave_9('u', it=1)
ds_u1 = ds_u1.isel(depth=idepth).u
ds_u2 = ds_levels.isel(depth=idepth_lev).u
ds_u  = xr.concat([ds_u1, ds_u2], dim='time').sel(time=slice(begin, end))

ds_v1 = eva.load_smt_wave_9('v', it=1)
ds_v1 = ds_v1.isel(depth=idepth).v
ds_v2 = ds_levels.isel(depth=idepth_lev).v
ds_v  = xr.concat([ds_v1, ds_v2], dim='time').sel(time=slice(begin, end))

# %%
data        = ds_u**2 /2 + ds_v**2 /2
data_tsel   = data.isel(time=slice(0,data.time.shape[0],1))
data_interp = pyic.interp_to_rectgrid_xr(data_tsel, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data        = data_interp * mask_interp
data.name   = 'KE'
data.to_netcdf(f'{path}/exp9_KE_100m_fine.nc')
# %%
