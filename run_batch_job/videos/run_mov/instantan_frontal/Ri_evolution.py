# %%
import sys
import matplotlib 
if len(sys.argv)>=2:
  matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import pyicon as pyic
import cartopy.crs as ccrs 
import pyicon.quickplots as pyicqp
import datetime
sys.path.append('/home/mpim/m300602/python/pytbx/mypy')
from matplotlib.dates import DateFormatter
import os
print(sys.argv)
import xarray as xr
import pandas as pd
sys.path.insert(0, "../../../../")
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
from importlib import reload
import glob

# %%
# import smt_modules.init_slurm_cluster as scluster 
# reload(scluster)

# client, cluster = scluster.init_dask_slurm_cluster(walltime='02:00:00', wait=False, scale=2)
# cluster
# client

# %%

ts = pyic.timing([0], 'start')

run      = 'single'
path_fig = '/home/m/m300878/submesoscaletelescope/run_batch_job/videos/images/%s/%s/' % (__file__.split('/')[-1][:-3], run)
nnf      = 0
gname    = 'front1'
savefig  = True

if not os.path.exists(path_fig):
  try:
    os.makedirs(path_fig)
  except:
    pass

# %%

print('--- '+run)
##################################################################################
#### Load Data

path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
time_averaged = ''

fig_path       = '/home/m/m300878/submesoscaletelescope/notebooks/images/first_paper/overview/'
fpath_ckdtree  = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
# fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'
fname          = 'na'
# lon_reg_2 = [-75, -55]
# lat_reg_2 = [33, 43]
lon_reg_2 = np.array([-66, -58])
lat_reg_2 = np.array([31, 37])
lon_reg_3 = np.array([-63.9, -61.5])
lat_reg_3 = np.array([34, 35.8])

asp = (lat_reg_2[1]-lat_reg_2[0])/(lon_reg_2[1]-lon_reg_2[0])
print(asp)
asp3 = (lat_reg_3[1]-lat_reg_3[0])/(lon_reg_3[1]-lon_reg_3[0])
print(asp3)

# %%
ds_b  = eva.load_smt_b_fast()
ds_n2 = eva.load_smt_N2()
f     = eva.calc_coriolis_parameter(np.rad2deg(ds_n2.clat))

M4 = ds_b.dbdx**2 + ds_b.dbdy**2
Ri = ds_n2.N2 * f**2 / M4
Ri = Ri.isel(depthi=17)

# %%
itd0   = 0
itdEnd = Ri.time.shape
timesd = Ri.time


# %%
range1 = np.array([-10,0])
range2 = np.array([0,10])
color1 = 'Reds'
color2 = 'Blues_r'
cmap1 = plt.get_cmap(color1)
cmap2 = plt.get_cmap(color2)
# Combine the colormaps
colors_combined = np.vstack((cmap1(np.linspace(0, 1, 128)), cmap2(np.linspace(0, 1, 128))))
cmap_combined = matplotlib.colors.ListedColormap(colors_combined)

def add_front_lines(ax):
    points = eva.get_points_of_inclined_fronts()
    # for ii in np.arange(int(points.shape[0]/2)):
    for ii in np.arange(2):
        # if ii == 5 or ii == 10: continue
        P = points[2*ii:2*ii+2,:]
        y, x, m, b = eva.calc_line(P)
        delta=0.8#eva.convert_degree_to_meter(int(y[0]))*0.1
        ax.plot(x,y,lw=3, ls=':', color='k')
        # ax.plot(x,y+delta,lw=1, color='r')
        # ax.plot(x,y-delta,lw=1, color='r')
        t = ax.text(P[0,0], P[0,1], f'F{ii+1}', color='black', fontsize=20)
        t.set_bbox(dict(facecolor='white', alpha=0.6, edgecolor='white'))

def add_rectangles(ax, lon_reg, lat_reg, txt):
    rect, right, top = eva.draw_rect(lat_reg, lon_reg,  color='black')
    ax.add_patch(rect)
    t = ax.text(lon_reg[1], lat_reg[1], f'{txt}', color='black', fontsize=20)
    # t.set_bbox(dict(facecolor='white', alpha=0.6, edgecolor='white'))

# %%
##################################################################################

#steps = np.arange(IcD.times.size-1)
steps = np.arange(itd0, itdEnd[0])

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))

  t1 = timesd[step]
  t2 = timesd[step+1]
  print(t1, t2)
  if t1==t2:
    continue

  # <<< start time loop here
  #####################################################################
  ### load file for oine timestep
  print(f'Load files for timestep {timesd[step]}')
  # %%
  data = Ri.drop('clon').drop('clat')
  data_sel = data.isel(time=step)
  # data_data_coarse = pyic.interp_to_rectgrid_xr(data_sel, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
  Tri_data, data_reg_data = eva.calc_triangulation_obj(data_sel, lon_reg_2, lat_reg_2)
  # Tri_data, data_reg_data = eva.calc_triangulation_obj(data_sel, lon_reg_3, lat_reg_3)


  # %%
  # ================================================================================   
  # Here start plotting
  # ================================================================================   
  if nn==0:
    plt.close('all')
    ccrs_proj = ccrs.PlateCarree()
    # cmap = 'RdYlBu_r'
    
    # %%
    # ---
    hca, hcb = pyic.arrange_axes(2,1, plot_cb='right', asp=asp, fig_size_fac=2,
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, axlab_kw=None,
                                 )
    ii=-1
    
    # ---
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # clim = np.array([-1, 9])
    # cmap='tab10'
    cmap = cmap_combined
    clim= -10,10
    ax.set_title(fr'Ri at {data.depthi.values}m', fontsize = 20)

    hm1 = pyic.shade(Tri_data, data_reg_data, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    # hm1 = pyic.shade(data.lon, data.lat, data.data, ax=ax, cax=cax, clim=clim, cmap=cmap,
    #                 transform=ccrs_proj, rasterized=False)
    add_front_lines(ax)
    add_rectangles(ax, lon_reg_3, lat_reg_3, 'RF1')


    pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2)

    #
    # ---
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # clim = 0,2
    hm2 = pyic.shade(Tri_data, data_reg_data, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    

    points = eva.get_points_of_inclined_fronts()
    P = points[0:2,:]
    y, x, m, b = eva.calc_line(P)
    delta=0.8*0.1 # 0.8 view 0.1 actual front
    color = 'purple'
    lw = 3
    ls = ':'
    # ax.plot(x,y, lw=lw, color=color, ls=ls)
    ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
    ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
    ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
    ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
    t = ax.text(P[0,0]-0.05, P[0,1], f'F{1}', color='black', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


    #----
    ht = ax.set_title(str(f'{timesd[step].data:.13}h').replace('T',' '), loc='right', fontsize=10)
    
    pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3, do_xyticks=False)
  #%%

    if not savefig:
      plt.show()
      sys.exit()
  else:
    ## --- update figures
    # hm1[0].set_array(data_data_coarse.data.flatten())
    hm1[0].set_array(data_reg_data)
    hm2[0].set_array(data_reg_data)

    ht.set_text(str(f'{timesd[step].data:.13}h').replace('T',' '))

  if savefig:
    fpath = '%s%s_%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], run, step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=300)
  else:
    plt.draw()
    input()

ts = pyic.timing(ts, 'all done')
# %%

exit()
# with log colorbar
data.plot(cmap ='Blues_r', norm=matplotlib.colors.LogNorm(vmin=1, vmax=1e14))
# %%
clim=0,50
cmap='Blues_r'
data.plot(cmap=cmap, vmin=0, vmax=50)

# %%
# define new colormap
range1 = np.array([-100,0])
range2 = np.array([0,1])
range3 = np.array([1,10])
range4 = np.array([10,1000])
cmap = plt.cm.Blues_r
colors1 = cmap(np.linspace(-100, 0, 256))
colors2 = cmap(np.linspace(0, 1, 256))
colors3 = cmap(np.linspace(1, 10, 256))
colors4 = cmap(np.linspace(10, 1000, 256))
index1 = np.round((range1 - range1.min())/(range1.max()-range1.min())*255).astype(int)
index2 = np.round((range2 - range2.min())/(range2.max()-range2.min())*255).astype(int)
index3 = np.round((range3 - range3.min())/(range3.max()-range3.min())*255).astype(int)
index4 = np.round((range4 - range4.min())/(range4.max()-range4.min())*255).astype(int)
colors1 = colors1[index1]
colors2 = colors2[index2]
colors3 = colors3[index3]
colors4 = colors4[index4]
colors = np.vstack((colors1, colors2, colors3, colors4))
cmap = matplotlib.colors.ListedColormap(colors)
norm = matplotlib.colors.BoundaryNorm([0,1,10,1000], cmap.N)

plt.figure()
data.plot(cmap=cmap, norm=norm)

# %%
range1 = np.array([-100,0])
range2 = np.array([0,1])
range3 = np.array([1,10])
range4 = np.array([10,1000])

color1 = 'tab:red'
color2 = 'tab:orange'
color3 = 'tab:blue'
color4 = 'blue'

colors = [color1, color2, color3, color4]


# %%
cmap = matplotlib.colors.ListedColormap(colors)
norm = matplotlib.colors.BoundaryNorm([-100,0,1,10,1000], cmap.N)

plt.figure()
data.plot(cmap=cmap, norm=norm)

# %%

range1 = np.array([-50,0])
range2 = np.array([0,50])

color1 = 'Reds'
# very light red


color2 = 'Blues_r'

cmap1 = plt.get_cmap(color1)
cmap2 = plt.get_cmap(color2)

# Combine the colormaps
colors_combined = np.vstack((cmap1(np.linspace(0, 1, 128)), cmap2(np.linspace(0, 1, 128))))
cmap_combined = matplotlib.colors.ListedColormap(colors_combined)

# Create a norm for the new ranges
norm = matplotlib.colors.BoundaryNorm([-100, 0, 100], cmap_combined.N)

plt.figure()
data.plot(cmap=cmap_combined, vmin=-50, vmax=50)
# %%
