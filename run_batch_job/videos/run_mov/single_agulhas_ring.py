# %%
import sys
import matplotlib 
if len(sys.argv)>=2:
  matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import pyicon as pyic
import cartopy.crs as ccrs 
ccrs_proj = ccrs.PlateCarree()

import matplotlib.patches as patches

import os
import xarray as xr
import pandas as pd
import glob
# from icon_smt_levels import dzw, dzt, depthc, depthi

sys.path.insert(0, "../../../")
import smt_modules.all_funcs as eva
# %%
# root_dir = '/scratch/m/m300878/agulhas_eddies/exp9/'
root_dir = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp9/'
varname     = 'zos'
wavelength  = 700  #choice of spatial cutoff for high pass filter in km
shape_error = 25 #choice of shape error in km
eddy_dir    = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
eddy_type   = 'anticyclonic'
dlon       = 1.8
dlat       = 1.8
res        = 0.02
npts       = int(dlon/res) #number of points from centre

# rgn='AR'
mindays=20 #min number of days for tracked eddy

tracker_dir=eddy_dir+'tracks/'+'composite_exp9_KE_100m/'
ds = xr.open_dataset(tracker_dir+eddy_type+'_'+str(dlon)+'x'+str(dlat)+'deg_trackID_0.nc')

# %%
ts = pyic.timing([0], 'start')

run      = 'single_agulhas_ring'
path_fig = '../images/%s/%s/' % (__file__.split('/')[-1][:-3], run)
nnf      = 0
savefig  = True

if not os.path.exists(path_fig):
  try:
    os.makedirs(path_fig)
  except:
    pass


# %%

print('--- '+run)

itd0   = 0
itdEnd = ds.time.shape
timesd = ds.time.data

##################################################################################
steps = np.arange(itd0, itdEnd[0])
print('delegate steps')
# %%
# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

print('selected steps',steps)
# %%
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
# %%
  t1 = timesd[step]
  t2 = timesd[step+1]
  print(t1, t2)
  if t1==t2:
    continue

  ### load file for oine timestep
  # %%
  print(f'my timestep: {step}')

  data = ds.isel(time=step).KE

  clim    = 0, 2
  cmap    = 'viridis'

  asp = data.x.shape[0]/data.y.shape[0]

  # %
  # ================================================================================   
  # Here start plotting
  # ================================================================================   
  # if nn==0:
  #   plt.close('all')
  # %%
  if nn==0:
    plt.close('all')
    hca, hcb = pyic.arrange_axes(1,1, plot_cb='right', asp=asp, fig_size_fac=2,
                                 sharex=False, sharey=False, xlabel="", ylabel="",
                                 daxr=-1, dcbl=1.3, axlab_kw=None,
                                 )
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(data.x, data.y, data, ax=ax, cax=cax, clim=clim, cmap= cmap, rasterized=False)
    ht = ax.set_title((f'EXP9 KE {data.time.data:.10}').replace('T',' '))

    pyic.plot_settings(ax)
  else:
        ## --- update figures
    hm1[0].set_array(data.data.flatten())
    ht.set_text(str(f'EXP9 KE {data.time.data:.10}').replace('T',' '))

  if savefig:
    fpath = '%s%s_%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], run, step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, bbox_inches='tight', dpi=250)
  else:
    plt.draw()
    input()

ts = pyic.timing(ts, 'all done')

# %%
