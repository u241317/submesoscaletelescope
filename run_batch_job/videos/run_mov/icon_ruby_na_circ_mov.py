import sys
import matplotlib 
if len(sys.argv)>=2:
  matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset
import pyicon as pyic
import cartopy.crs as ccrs 
import pyicon.quickplots as pyicqp
sys.path.append('/home/mpim/m300602/python/pytbx/mypy')
import my_toolbox as my
from matplotlib.dates import DateFormatter
import os
print(sys.argv)

if len(sys.argv)==1:
  run       = 'hel20134-STR'
  path_data = '/work/mh0033/m211054/projects/icon/ruby/icon-oes_fluxadjust/experiments/'+run+'/'
  savefig   = False
else:
  path_data = sys.argv[1]
  run       = sys.argv[2]
  savefig   = True

path_fig = '../movies/%s/%s/' % (__file__.split('/')[-1][:-3], run)
nnf=0

if not os.path.exists(path_fig):
  try:
    os.makedirs(path_fig)
  except:
    pass

runname   = ''
gname     = 'r2b6'
lev       = 'L64'
path_grid     = f'/mnt/lustre01/work/mh0033/m300602/icon/grids/{gname}/'
path_ckdtree  = f'{path_grid}ckdtree/'
fpath_ckdtree = f'{path_grid}ckdtree/rectgrids/{gname}_res0.30_180W-180E_90S-90N.npz'
fpath_fx      = '/mnt/lustre01/work/mh0033/m211054/projects/icon/ruby/icon-oes_fluxadjust/experiments/exp.ocean_omip_long_tke_r2b6_20133-CIQ/exp.ocean_omip_long_tke_r2b6_20133-CIQ_fx.nc'
fpath_tgrid   = '/work/mh0033/m211054/projects/icon/topo/OceanOnly_Global_IcosSymmetric_0039km_rotatedZ37d_BlackSea_Greenland_modified_srtm30_1min/OceanOnly_Global_IcosSymmetric_0039km_rotatedZ37d_BlackSea_Greenland_modified_sills_srtm30_1min.nc'
fpath_initial_state = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_08/icon-oes/experiments/nib0004/initial_state.nc'
omit_last_file = True

print('--- '+run)
fname_def = run+'_oce_def_????????????????.nc'
fname_moc = run+'_oce_moc_????????????????.nc'
fname_dbg = run+'_oce_dbg_????????????????.nc'
fname_ice = run+'_oce_ice_????????????????.nc'
fname_mon = run+'_oce_mon_????????????????.nc'

print('def')
IcD = pyic.IconData(
               fname        = fname_def,
               path_data    = path_data,
               path_grid    = path_grid,
               gname        = gname,
               lev          = lev,
               fpath_fx     = fpath_fx,
               fpath_tgrid  = fpath_tgrid,
               do_triangulation = True,
               omit_last_file   = omit_last_file,
              )
print('moc')
IcD_moc = pyic.IconData(
               fname        = fname_moc,
               path_data    = path_data,
               path_grid    = path_grid,
               gname        = gname,
               lev          = lev,
               fpath_fx     = fpath_fx,
               fpath_tgrid  = fpath_tgrid,
               do_triangulation   = False,
               omit_last_file     = omit_last_file,
               load_vertical_grid = False,
               load_triangular_grid = False,
               calc_coeff         = False,

              )

print('dbg')
IcD_dbg = pyic.IconData(
               fname        = fname_dbg,
               path_data    = path_data,
               path_grid    = path_grid,
               gname        = gname,
               lev          = lev,
               fpath_fx     = fpath_fx,
               fpath_tgrid  = fpath_tgrid,
               do_triangulation   = False,
               omit_last_file     = omit_last_file,
               load_vertical_grid = False,
               load_triangular_grid = False,
               calc_coeff         = False,
              )

print('ice')
IcD_ice = pyic.IconData(
               fname        = fname_ice,
               path_data    = path_data,
               path_grid    = path_grid,
               gname        = gname,
               lev          = lev,
               fpath_fx     = fpath_fx,
               fpath_tgrid  = fpath_tgrid,
               do_triangulation   = False,
               omit_last_file     = omit_last_file,
               load_vertical_grid = False,
               load_triangular_grid = False,
               calc_coeff         = False,
              )
print('mon')
IcD_mon = pyic.IconData(
               fname        = fname_mon,
               path_data    = path_data,
               path_grid    = path_grid,
               gname        = gname,
               lev          = lev,
               fpath_fx     = fpath_fx,
               fpath_tgrid  = fpath_tgrid,
               do_triangulation   = False,
               omit_last_file     = omit_last_file,
               load_vertical_grid = False,
               load_triangular_grid = False,
               calc_coeff         = False,
              )

IcD.edge2cell_coeff_cc_t = pyic.calc_edge2cell_coeff_cc_t(IcD) # to calculate wind stress curl
print('Done!')

topo_c = IcD.dzw.sum(axis=0)
lon, lat, topoi = pyic.interp_to_rectgrid(topo_c, fpath_ckdtree, coordinates='clat clon')

f = Dataset(fpath_initial_state, 'r')
temp_ref = f.variables['T'][0,:,:]
salt_ref = f.variables['S'][0,:,:]
f.close()
temp_ref[temp_ref==0.]=np.ma.masked
salt_ref[salt_ref==0.]=np.ma.masked

steps = np.arange(IcD.times.size-1)

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))

  t1 = IcD.times[step]
  t2 = IcD.times[step+1]
  print(t1, t2)
  if t1==t2:
    continue

  # <<< start time loop here
  
  it_ave = np.where( (IcD_dbg.times>=t1) & (IcD_dbg.times<=t2) )[0]
  it_ave_mar = it_ave[2::12]
  it_ave_sep = it_ave[8::12]
  
  # --- load data    
  #uo, it_ave   = pyic.time_average(IcD, 'u', t1=t1, t2=t2, iz='all')
  #vo, it_ave   = pyic.time_average(IcD, 'v', t1=t1, t2=t2, iz='all')
  #wo, it_ave   = pyic.time_average(IcD, 'w', t1=t1, t2=t2, iz='all')
  to, it_ave   = pyic.time_average(IcD, 'to', t1=t1, t2=t2, iz='all')
  so, it_ave   = pyic.time_average(IcD, 'so', t1=t1, t2=t2, iz='all')
  zo, it_ave   = pyic.time_average(IcD, 'zos', t1=t1, t2=t2, iz='all')
  #hi, it_ave   = pyic.time_average(IcD_ice, 'hi', t1=t1, t2=t2, iz='all')
  #hs, it_ave   = pyic.time_average(IcD_ice, 'hs', t1=t1, t2=t2, iz='all')
  #conc, it_ave   = pyic.time_average(IcD_ice, 'conc', t1=t1, t2=t2, iz='all')
  #ice_u, it_ave   = pyic.time_average(IcD_ice, 'ice_u', t1=t1, t2=t2, iz='all')
  #ice_v, it_ave   = pyic.time_average(IcD_ice, 'ice_v', t1=t1, t2=t2, iz='all')
  #zUnderIce, it_ave = pyic.time_average(IcD_dbg, 'zUnderIce', t1=t1, t2=t2, iz='all')
  
  mld_mar, it_ave = pyic.time_average(IcD_dbg, 'mld', it_ave=it_ave_mar, iz='all')
  
  #HeatFlux_Total, it_ave = pyic.time_average(IcD_dbg, 'HeatFlux_Total', t1=t1, t2=t2, iz='all')
  #FrshFlux_TotalOcean, it_ave = pyic.time_average(IcD_dbg, 'FrshFlux_TotalOcean', t1=t1, t2=t2, iz='all')
  #FrshFlux_TotalOcean, it_ave = pyic.time_average(IcD_dbg, 'FrshFlux_Runoff', t1=t1, t2=t2, iz='all')
  
  #conc_mar, it_ave = pyic.time_average(IcD_dbg, 'conc', it_ave=it_ave_mar, iz='all')
  #hi_mar, it_ave   = pyic.time_average(IcD_dbg, 'hi', it_ave=it_ave_mar, iz='all')
  #hs_mar, it_ave   = pyic.time_average(IcD_dbg, 'hs', it_ave=it_ave_mar, iz='all')
  #hiconc_mar = (hi_mar*conc_mar)[0,:]
  #hsconc_mar = (hs_mar*conc_mar)[0,:]
  sst_mar, it_ave   = pyic.time_average(IcD_dbg, 'to', it_ave=it_ave_mar, iz=0)
  sst_sep, it_ave   = pyic.time_average(IcD_dbg, 'to', it_ave=it_ave_sep, iz=0)
  #ssh_mar, it_ave   = pyic.time_average(IcD_dbg, 'zos', it_ave=it_ave_mar, iz=0)
  
  times, amoc26n = IcD_mon.load_timeseries('amoc26n', ave_freq=12)

  #tauu, it_ave   = pyic.time_average(IcD_atm2d, 'tauu', t1=t1, t2=t2, iz='all')
  #tauv, it_ave   = pyic.time_average(IcD_atm2d, 'tauv', t1=t1, t2=t2, iz='all')
  #tauu[IcD_atm2d.cell_sea_land_mask>0] = np.ma.masked
  #tauv[IcD_atm2d.cell_sea_land_mask>0] = np.ma.masked
  #p_tau = pyic.calc_3d_from_2dlocal(IcD_atm2d, tauu[np.newaxis,:], tauv[np.newaxis,:])
  #ptp_tau = pyic.cell2edges(IcD_atm2d, p_tau)
  #curl_tau = pyic.calc_curl(IcD_atm2d, ptp_tau)
  tauu, it_ave   = pyic.time_average(IcD_dbg, 'atmos_fluxes_stress_x', t1=t1, t2=t2, iz='all')
  tauv, it_ave   = pyic.time_average(IcD_dbg, 'atmos_fluxes_stress_y', t1=t1, t2=t2, iz='all')
  tauu[IcD.wet_c[0,:]==0] = np.ma.masked
  tauv[IcD.wet_c[0,:]==0] = np.ma.masked
  p_tau = pyic.calc_3d_from_2dlocal(IcD, tauu[np.newaxis,:], tauv[np.newaxis,:])
  ptp_tau = pyic.cell2edges(IcD, p_tau)
  #curl_tau = pyic.calc_curl(IcD, ptp_tau)
  curl_tau = (ptp_tau[:,IcD.edges_of_vertex] * IcD.rot_coeff[0,:,:]).sum(axis=2)
  curl_tau *= 1./1e-7
  
  print('Done!')
  
  tbias = to-temp_ref
  sbias = so-salt_ref
  
  # --- calculate barotropic streamfunction
  #mass_flux, it_ave   = pyic.time_average(IcD, 'mass_flux', t1=t1, t2=t2, iz='all')
  #bstr = pyic.calc_bstr_vgrid(IcD, mass_flux.sum(axis=0), lon_start=0., lat_start=90.)
  
  #lon, lat, bstri = pyic.interp_to_rectgrid(bstr, fpath_ckdtree, coordinates='vlat vlon')
  lon, lat, sshi = pyic.interp_to_rectgrid(zo, fpath_ckdtree, coordinates='clat clon')
  #lon, lat, ssti = pyic.interp_to_rectgrid(to[0,:], fpath_ckdtree, coordinates='clat clon')
  lon, lat, sst_mari = pyic.interp_to_rectgrid(sst_mar, fpath_ckdtree, coordinates='clat clon')
  lon, lat, sst_sepi = pyic.interp_to_rectgrid(sst_sep, fpath_ckdtree, coordinates='clat clon')
  lon, lat, mld_mari = pyic.interp_to_rectgrid(mld_mar, fpath_ckdtree, coordinates='clat clon')
  #lon, lat, hiconc_mari = pyic.interp_to_rectgrid(hiconc_mar, fpath_ckdtree, coordinates='clat clon')
  #lon, lat, ui = pyic.interp_to_rectgrid(uo[0,:], fpath_ckdtree, coordinates='clat clon')
  #lon, lat, vi = pyic.interp_to_rectgrid(vo[0,:], fpath_ckdtree, coordinates='clat clon')
  #lon, lat, ssh_mari = pyic.interp_to_rectgrid(ssh_mar, fpath_ckdtree, coordinates='clat clon')
  lon, lat, sst_biasi = pyic.interp_to_rectgrid(tbias[0,:], fpath_ckdtree, coordinates='clat clon')
  lon, lat, sss_biasi = pyic.interp_to_rectgrid(sbias[0,:], fpath_ckdtree, coordinates='clat clon')
  lon, lat, curl_taui = pyic.interp_to_rectgrid(curl_tau[0,:], fpath_ckdtree, coordinates='vlat vlon')
  
  # ================================================================================   
  # Here start plotting
  # ================================================================================   
  if nn==0:
    plt.close('all')
    ccrs_proj = ccrs.PlateCarree()
    
    # ---
    hca, hcb = pyic.arrange_axes(2,2, plot_cb=True, asp=0.5, fig_size_fac=1.5,
                                 sharex=True, sharey=True, xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0,
                                 )
    ii=-1
    
    # ---
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    #hm1 = pyic.shade(lon, lat, bstri, ax=ax, cax=cax, transform=ccrs_proj,
    #                 clim=40, cincr=5., 
    #                )
    hm1 = pyic.shade(lon, lat, sshi, ax=ax, cax=cax, transform=ccrs_proj,
                     clim=[-1.1,-0.6], cincr=0.05, 
                    )
    hc1 = [0,0]
    hc1[0] = ax.contour(lon, lat, sst_mari, [-1.79], colors='r', linestyles='solid')
    hc1[1] = ax.contour(lon, lat, sst_sepi, [-1.79], colors='g', linestyles='solid')
    pyic.shade(lon, lat, topoi, ax=ax, cax=cax, transform=ccrs_proj, use_pcol_or_contf=False,
               conts=[1000, 3000], contcolor='0.2')
    #ax.set_title('barotropic streamfunction [Sv]')
    ax.set_title('SSH [m]')
    
    # ---
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(lon, lat, mld_mari, ax=ax, cax=cax, transform=ccrs_proj,
                     clim=[0,3000], cincr=200, 
                    )
    ax.set_title('March mixed layer depth [m]')
    
    # ---
    #ii+=1; ax=hca[ii]; cax=hcb[ii]
    #hm3 = pyic.shade(lon, lat, sst_biasi, ax=ax, cax=cax, transform=ccrs_proj,
    #                 clim=6, cincr=1., 
    #                )
    #ax.set_title('SST bias [$^o$C]')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm3 = pyic.shade(lon, lat, curl_taui, ax=ax, cax=cax, transform=ccrs_proj,
                     clim=4, cincr=0.25,
                    )
    ax.set_title('wind stress curl [1e-7 N m-3]')
    
    # ---
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm4 = pyic.shade(lon, lat, sss_biasi, ax=ax, cax=cax, transform=ccrs_proj,
                     clim=5, cincr=1.
                    )
    ax.set_title('SSS bias [g/kg]')
    
    # --- inlay with amoc26n
    ht1 = ax.text(0.5, 0.98, f"{run}\n{t1.astype('datetime64[D]')} - {t2.astype('datetime64[D]')}",
                  ha='center', va='top', transform=plt.gcf().transFigure)
    axi = my.make_inlay_axes(xy=(0.58,0.1), width=(0.4), height=(0.4), transform=ax.transAxes, ax=ax)
    axi.plot(times, amoc26n)
    ind = np.argmin((times-t1).astype(float)**2) 
    hl1 = axi.plot(times[ind], amoc26n[ind], marker='o', color='r')
    axi.grid(True)
    axi.set_xticks(np.array(['1500', '1700', '1900', '2100', '2300'], dtype='datetime64'))
    axi.xaxis.set_major_formatter(DateFormatter('%Y'))
    #axi.set_xlim(times.min(), times.max())
    axi.text(0.5, 0.98, 'AMOC 26$^o$N [Sv]', transform=axi.transAxes, ha='center', va='top')
    
    for ax in hca:
        #ax.contour(lon, lat, sst_mari, [-1.79], colors='k', linestyles='solid')
        #ax.contour(lon, lat, sst_sepi, [-1.79], colors='r', linestyles='solid')
    #     xlim = [-80,0]
    #     ylim = [30,70]
        xlim=[-70,-6]
        ylim=[38,70]
        pyic.plot_settings(ax, xlim, ylim, projection=ccrs_proj)

    if not savefig:
      plt.show()
      sys.exit()
  
  ## --- update figures
  hm1[0].set_array(sshi[1:,1:].flatten())
  hm2[0].set_array(mld_mari[1:,1:].flatten())
  #hm3[0].set_array(sst_biasi[1:,1:].flatten())
  hm3[0].set_array(curl_taui[1:,1:].flatten())
  hm4[0].set_array(sss_biasi[1:,1:].flatten())

  ht1.set_text(f"{run}\n{t1.astype('datetime64[D]')} - {t2.astype('datetime64[D]')}")

  ind = np.argmin((times-t1).astype(float)**2) 
  hl1[0].set_xdata(times[ind])
  hl1[0].set_ydata(amoc26n[ind])

  for mm in range(2):
    for tp in hc1[mm].collections:
       tp.remove()
  hc1[0] = hca[0].contour(lon, lat, sst_mari, [-1.79], colors='r', linestyles='solid')
  hc1[1] = hca[0].contour(lon, lat, sst_sepi, [-1.79], colors='g', linestyles='solid')

  if savefig:
    fpath = '%s%s_%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], run, step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=300)
  else:
    plt.draw()
    input()
