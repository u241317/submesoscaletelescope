# %%
import sys
import matplotlib 
if len(sys.argv)>=2:
  matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import pyicon as pyic
import cartopy.crs as ccrs 
ccrs_proj = ccrs.PlateCarree()

import matplotlib.patches as patches

import os
import xarray as xr
import pandas as pd
import glob
from icon_smt_levels import dzw, dzt, depthc, depthi

sys.path.insert(0, "../../../")
import smt_modules.all_funcs as eva

def add_subplot_axes(ax,rect,axisbg='w'):
    fig = plt.gcf()
    box = ax.get_position()
    width = box.width
    height = box.height
    inax_position  = ax.transAxes.transform(rect[0:2])
    transFigure = fig.transFigure.inverted()
    infig_position = transFigure.transform(inax_position)    
    x = infig_position[0]
    y = infig_position[1]
    width *= rect[2]
    height *= rect[3]  # <= Typo was here
    #subax = fig.add_axes([x,y,width,height],facecolor=facecolor)  # matplotlib 2.0+
    subax = fig.add_axes([x,y,width,height])
    x_labelsize = subax.get_xticklabels()[0].get_size()
    y_labelsize = subax.get_yticklabels()[0].get_size()
    x_labelsize *= rect[2]**0.5
    y_labelsize *= rect[3]**0.5
    subax.xaxis.set_tick_params(labelsize=x_labelsize)
    subax.yaxis.set_tick_params(labelsize=y_labelsize)
    return subax
# %%


ts = pyic.timing([0], 'start')

run      = 'agulhas_rings'
path_fig = '../images/%s/%s/' % (__file__.split('/')[-1][:-3], run)
nnf      = 0
# gname    = 'sat'
savefig  = True

if not os.path.exists(path_fig):
  try:
    os.makedirs(path_fig)
  except:
    pass


# %%

print('--- '+run)
##################################################################################
#### Load Data
lon_reg = [-25, 35]
lat_reg = [-50, -5]
asp     = (lat_reg[1]-lat_reg[0]) / (lon_reg[1]-lon_reg[0])

lon_reg_zoom = [-5, 12.34]
lat_reg_zoom = [-38, -25]
asp_z        = (lat_reg_zoom[1]-lat_reg_zoom[0]) / (lon_reg_zoom[1]-lon_reg_zoom[0])


rectangle = patches.Rectangle((lon_reg_zoom[0], lat_reg_zoom[0]), lon_reg_zoom[1]-lon_reg_zoom[0], lat_reg_zoom[1]-lat_reg_zoom[0], linewidth=1, edgecolor='r', facecolor='none')

ds_1, ds_2 = eva.load_mooring_data()
pos = [(ds_1.longitude.data, ds_1.latitude.data), (ds_2.longitude.data, ds_2.latitude.data)]

# %%

# --- load times and flist
ts             = pyic.timing(ts, 'load times and flist')
path_data_root = '/pool/data/ICDC/ocean/aviso_ssh/DATA/'
search_str     = f'dt_global_twosat_phy_l4*.nc'

path_data = path_data_root+'2021/**/'
flist1    = np.array(glob.glob(path_data+search_str))
flist1.sort()
path_data = path_data_root+'2022/**/'
flist2    = np.array(glob.glob(path_data+search_str))
flist2.sort()
flist = np.insert(flist1, 0, flist2)
flist.sort()

sat = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))

# %%
itd0   = 0
itdEnd = sat.time.shape
timesd = sat.time.data

sat['KE'] = sat.ugos**2 / 2 + sat.vgos**2 / 2
sat       = sat.where((sat.latitude > lat_reg[0]) & (sat.latitude < lat_reg[1]) & (sat.longitude > lon_reg[0]) & (sat.longitude < lon_reg[1]), drop=True)
sat_zoom  = sat.where((sat.latitude > lat_reg_zoom[0]) & (sat.latitude < lat_reg_zoom[1]) & (sat.longitude > lon_reg_zoom[0]) & (sat.longitude < lon_reg_zoom[1]), drop=True)

lon   = sat.longitude.data
lat   = sat.latitude.data
lon_z = sat_zoom.longitude.data
lat_z = sat_zoom.latitude.data


##################################################################################
steps = np.arange(itd0, itdEnd[0])
# %%
# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

print('selected steps',steps)
# %%
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
# %%
  t1 = timesd[step]
  t2 = timesd[step+1]
  print(t1, t2)
  if t1==t2:
    continue

  # <<< start time loop here
  #####################################################################
  ### load file for oine timestep
  # %%
  print(f'my timestep: {step}')
  if True:
    sla      = sat.isel(time=step).sla
    print(sla.time.data)
    sla_zoom = sat_zoom.isel(time=step).sla
    sla      = sla.data
    sla_zoom = sla_zoom.data

    clim    = -0.5, 0.5
    cticks  = 6
    cbticks = np.linspace(clim[0],clim[1],cticks)
    cmap    = 'PuOr'  # 'RdYlGn'
  else:
    KE = sat.isel(time=step).KE
    KE = KE.data
    KE_zoom = sat_zoom.isel(time=step).KE
    KE_zoom = KE_zoom.data

    clim    = 0, 0.25
    cticks  = 6
    cbticks = np.linspace(clim[0],clim[1],cticks)
    cmap    = 'plasma'

  # %
  # ================================================================================   
  # Here start plotting
  # ================================================================================   
  # if nn==0:
  #   plt.close('all')
  # %%
  hca, hcb = pyic.arrange_axes(2,1, plot_cb='right', asp=asp, fig_size_fac=2,
                               sharex=False, sharey=False, xlabel="", ylabel="",
                               projection=ccrs_proj,
                               daxr=-1, dcbl=1.3, axlab_kw=None,
                               )
  ii=-1
  
  # ---
  if True:
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(lon, lat, sla, ax=ax, cax=cax, clim=clim, cmap= cmap, transform=ccrs_proj, rasterized=False, nclev=25, contfs=True, cbticks=cbticks)
    ax.set_title('Aviso SLA [m]')
    rectangle = patches.Rectangle((lon_reg_zoom[0], lat_reg_zoom[0]), lon_reg_zoom[1]-lon_reg_zoom[0], lat_reg_zoom[1]-lat_reg_zoom[0], linewidth=1, edgecolor='r', facecolor='none')
    ax.add_patch(rectangle)
    ht = ax.set_title(f'{timesd[step]:.10}'.replace('T',' '), loc='right', fontsize=10)
    ax.plot(pos[0][0], pos[0][1],  'r*', markersize=20, transform=ccrs_proj, label='Mooring Position')
    ax.plot(pos[1][0], pos[1][1],  'b*', markersize=20, transform=ccrs_proj)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(lon_z, lat_z, sla_zoom, ax=ax, cax=cax, clim=clim, cmap= cmap, transform=ccrs_proj, rasterized=False, nclev=25, contfs=True, cbticks=cbticks)
    ax.plot(pos[0][0], pos[0][1],  'r*', markersize=20, transform=ccrs_proj, label='Mooring Position')
    ax.plot(pos[1][0], pos[1][1],  'b*', markersize=20, transform=ccrs_proj)
  else:
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(lon, lat, KE, ax=ax, cax=cax, clim=clim, cmap= cmap, transform=ccrs_proj, rasterized=False, nclev=25, contfs=True, cbticks=cbticks)
    ax.set_title('Aviso geostrophic KE')
    rectangle = patches.Rectangle((lon_reg_zoom[0], lat_reg_zoom[0]), lon_reg_zoom[1]-lon_reg_zoom[0], lat_reg_zoom[1]-lat_reg_zoom[0], linewidth=1, edgecolor='r', facecolor='none')
    ax.add_patch(rectangle)
    ht = ax.set_title(f'{timesd[step]:.10}'.replace('T',' '), loc='right', fontsize=10)
    ax.plot(pos[0][0], pos[0][1],  'r*', markersize=20, transform=ccrs_proj, label='Mooring Position')
    ax.plot(pos[1][0], pos[1][1],  'b*', markersize=20, transform=ccrs_proj)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(lon_z, lat_z, KE_zoom, ax=ax, cax=cax, clim=clim, cmap= cmap, transform=ccrs_proj, rasterized=False, nclev=25, contfs=True, cbticks=cbticks)
    ax.plot(pos[0][0], pos[0][1],  'r*', markersize=20, transform=ccrs_proj, label='Mooring Position')
    ax.plot(pos[1][0], pos[1][1],  'b*', markersize=20, transform=ccrs_proj)
    # pyic.plot_settings(ax, xlim=lon_reg_zoom, ylim=lat_reg_zoom)

  fig = plt.gcf()
  rect = [-1.02,1.2,2,0.7]
  ax = add_subplot_axes(ax,rect, axisbg='w')

  ax.set_title('TSeries: KE Moorings 2D running mean')

  window = 2*24
  ax.plot(ds_1.time, ds_1.KE.isel(depth=3).rolling(time=window, center=True).mean(), 'tab:red', alpha=.7, label=f'M1 {ds_1.KE.isel(depth=3).depth.data:.0f}m')
  ax.plot(ds_1.time, ds_1.KE.isel(depth=0).rolling(time=window, center=True).mean(), 'tab:red', ls=':', alpha=.7, label=f'M1 {ds_1.KE.isel(depth=0).depth.data:.0f}m')
  ax.plot(ds_1.time, ds_1.KE.isel(depth=13).rolling(time=window, center=True).mean(), 'tab:red', ls='--', alpha=.7, label=f'M1 {ds_1.KE.isel(depth=13).depth.data:.0f}m')
  ax.plot(ds_2.time, ds_2.KE.isel(depth=8).rolling(time=window, center=True).mean(), 'tab:blue', alpha=.7, label=f'M2 {ds_2.KE.isel(depth=8).depth.data:.0f}m')
  ax.plot(ds_2.time, ds_2.KE.isel(depth=3).rolling(time=window, center=True).mean(), 'tab:blue', ls=':', alpha=.7, label=f'M2 {ds_2.KE.isel(depth=3).depth.data:.0f}m')
  ax.plot(ds_2.time, ds_2.KE.isel(depth=27).rolling(time=window, center=True).mean(), 'tab:blue', ls='--', alpha=.7, label=f'M2 {ds_2.KE.isel(depth=27).depth.data:.0f}m')

  ax.vlines(timesd[step], 1e-5, 3, colors='k', linestyles='-')
  plt.legend(loc='upper left')
  # ax.set_ylim(1e-3,3e-1)
  ax.set_ylim(0,0.2)
  ax.set_xlim(timesd[step]-np.timedelta64(60,'D'), timesd[step]+np.timedelta64(60,'D'))
  ax.xaxis.set_major_locator(plt.MaxNLocator(5))

  if savefig:
    fpath = '%s%s_%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], run, step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, bbox_inches='tight', dpi=250)
  else:
    plt.draw()
    input()

ts = pyic.timing(ts, 'all done')

# %%
