#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --time=00:10:00
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j
#SBATCH --ntasks=16
#SBATCH --partition=compute
#SBATCH --account=mh0033

module list
source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`

run="randompath1"
path_data="randompath2"
# mpirun -np 32 python -u agulhas_rings.py ${path_data} ${run}
# mpirun -np 16 python -u agulhas_rings_smt.py ${path_data} ${run}
mpirun -np 16 python -u single_agulhas_ring.py ${path_data} ${run}


enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

