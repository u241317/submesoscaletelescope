
import sys

sys.path.insert(0, "../../")
import netCDF4 as nc
# from dask.diagnostics import ProgressBar
import numpy as np
import pandas as pd
# import glob, os
import pyicon as pyic
import xarray as xr
from netCDF4 import Dataset  # type: ignore

import smt_modules.all_funcs as eva
import smt_modules.tools as tools
from smt_modules.icon_smt_levels import depthc, depthi, dzt, dzw

# import datetime          #https://docs.python.org/3/library/datetime.html

# import matplotlib.pyplot as plt
# from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates
# import cartopy.crs as ccrs
# ccrs_proj = ccrs.PlateCarree()

# print('Hello Mo')
# print("This is the name of the script: ", sys.argv[0])
# print("Number of arguments: ", len(sys.argv))
# print("The arguments are: " , str(sys.argv))

run=str(sys.argv[2])
print(f'run: {run}')
# if run == '1': print('lets go')
# if run == '2': print('lets go')
# exit()


ts = pyic.timing([0], 'start')

print('start calculation coefficients')
########### compute coefficients #############
def xr_edges2cell(edge_of_cell, ve, dze, dzc, edge2cell_coeff_cc=None, fixed_vol_norm=None):
    # if fixed_vol_norm is None:
    #     fixed_vol_norm = xr_calc_fixed_vol_norm(ds_IcD)
    # if edge2cell_coeff_cc is None:
    #     edge2cell_coeff_cc = xr_calc_edge2cell_coeff_cc(ds_IcD)
    if ve.dims != dze.dims:
      raise ValueError('::: Dims of ve and dze have to be the same!:::')
    p_vn_c = (
        edge2cell_coeff_cc 
        * ve.isel(edge=edge_of_cell) 
        #* ds_fx.prism_thick_e.isel(edge=ds_IcD.edge_of_cell)
        * dze.isel(edge=edge_of_cell)
    ).sum(dim='nv')
    if 'depth' in p_vn_c.dims:
        p_vn_c = p_vn_c.transpose('depth', 'cell', 'cart')
    
    p_vn_c = p_vn_c / (
        fixed_vol_norm 
        #* ds_fx.prism_thick_c
        * dzc
    )
    return p_vn_c

def get_variables():
  tgrid  = eva.load_smt_grid()
  ds_IcD = pyic.convert_tgrid_data(tgrid)
  ds_IcD = ds_IcD.compute()

  fixed_vol_norm = pyic.xr_calc_fixed_volume_norm(ds_IcD)
  fixed_vol_norm = fixed_vol_norm.compute()

  edge2cell_coeff_cc = pyic.xr_calc_edge2cell_coeff_cc(ds_IcD)
  edge2cell_coeff_cc = edge2cell_coeff_cc.compute()

  edge_of_cell = ds_IcD.edge_of_cell.compute()

  sinLon = np.sin(ds_IcD.clon*np.pi/180.)
  cosLon = np.cos(ds_IcD.clon*np.pi/180.)
  sinLat = np.sin(ds_IcD.clat*np.pi/180.)
  cosLat = np.cos(ds_IcD.clat*np.pi/180.)

  ce     = ds_IcD.edge.size
  ncells = ds_IcD.cell.size
  nz     = depthc.size
  return fixed_vol_norm, edge_of_cell, edge2cell_coeff_cc, sinLon, cosLon, sinLat, cosLat, ce, ncells, nz

fixed_vol_norm, edge_of_cell, edge2cell_coeff_cc, sinLon, cosLon, sinLat, cosLat, ce, ncells, nz = get_variables()

######### load data ###########
ds = eva.load_smt_vn()

#start time
pdtime0     = np.datetime64('2010-03-09T09:00:00')
pdtimeEnd   = np.datetime64('2010-03-22T19:00:00')
# 48 steps per run with 6 cpus on one compute node
# if run == '0':
#   pdtime0     = np.datetime64('2010-03-13T21:00:00')
#   pdtimeEnd   = np.datetime64('2010-03-17T19:00:00')
#   print(f'start: {pdtime0}')
# if run == '1':
#   pdtime0     = np.datetime64('2010-03-17T21:00:00')
#   pdtimeEnd   = np.datetime64('2010-03-21T19:00:00')
#   print(f'start: {pdtime0}')
# if run == '2':
#   pdtime0     = np.datetime64('2010-03-21T21:00:00')
#   pdtimeEnd   = np.datetime64('2010-03-22T19:00:00')
#   print(f'start: {pdtime0}')

# if run == '0':
#   pdtime0     = np.datetime64('2010-03-13T15:00:00')
#   pdtimeEnd   = np.datetime64('2010-03-13T15:00:00')
# if run == '1':
#   pdtime0     = np.datetime64('2010-03-22T21:00:00')
#   pdtimeEnd   = np.datetime64('2010-03-22T21:00:00')

itd0        = int(np.where(ds.time.values==pdtime0)[0])
itdEnd      = int(np.where(ds.time.values==pdtimeEnd)[0])

#number of total time steps
steps = np.arange(itd0, itdEnd+1)
print('steps', steps)

ts      = pyic.timing(ts, 'finished calc coefficients')
print('finished calc coefficients')
##################################################################################

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
# print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]  # type: ignore
steps = list_all_pros[rank]

###################################################################################

for nn, step in enumerate(steps):  # type: ignore
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))  # type: ignore
  ts = pyic.timing(ts, f'loop time step: {step}')
  t1 = ds.time[step]
  t2 = ds.time[step+1]
  #print(t1, t2)
  if t1==t2:
    continue

  pdtime = ds.time.isel(time=step)
  ######### open file ###########
  fpatho = f'/work/mh0033/m300878/smt/v/pp_calc_v_h{pdtime.data:.13}.nc'
  fo     = Dataset(fpatho, 'w', format='NETCDF4')
  fo.createDimension('depthc', nz) 
  fo.createDimension('ncells', ncells) # new dim cell center

  nc_u   = fo.createVariable('u','f4',('depthc','ncells'))
  nc_v   = fo.createVariable('v','f4',('depthc','ncells'))
  ncv    = fo.createVariable('depthc','f4','depthc')
  ncv[:] = depthc[:nz]

  vet = ds.isel(time=step)
  
  # ts       = pyic.timing(ts, 'calc hor. v for one timestep')
  print(f"start {step} timestep: {pdtime.data:.13}")  
  for lev in np.arange(depthc.size):
      ve     = vet.isel(depthc=lev)
      ve     = ve.expand_dims(dim='depthc')
      ve     = ve.rename({'ncells': 'edge'})
      ve     = ve.compute() #important!
      dze    = xr.DataArray(dzw[lev]*np.ones((1,ce)), dims=['depthc', 'edge'])
      p_vn_c = xr_edges2cell(edge_of_cell, ve, dze, dzw[lev], edge2cell_coeff_cc=edge2cell_coeff_cc, fixed_vol_norm=fixed_vol_norm)
      u1     = p_vn_c[:,0]
      u2     = p_vn_c[:,1]
      u3     = p_vn_c[:,2]
      uo     = u2 * cosLon - u1 * sinLon
      vo     = -(u1 * cosLon + u2 * sinLon) * sinLat + u3 * cosLat

      nc_u[lev,:] = uo[:ncells]
      nc_v[lev,:] = vo[:ncells]

  print(f'finish calculate step {step}, timestep: {pdtime.data:.13}')

  fo.close()

print('all done')