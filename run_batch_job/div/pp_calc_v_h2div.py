import sys
import numpy as np
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
import xarray as xr
import dask.array as da
import pandas as pd
from termcolor import colored
from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')

print(colored('start calc div', 'red'))

### configure paths
run      = 'ngSMT_tke'
#savefig  = False
#path_fig = '../pics/'
#nnf      = 0

#gname = 'smt'
#lev   = 'L128'

#path_data    = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'
#path_grid    = f'/mnt/lustre01/work/mh0033/m300602/icon/grids/{gname}/'
fpath_tgrid  = '/home/mpim/m300602/work/icon/grids/smt/smt_tgrid.nc'

ds_tg = xr.open_dataset(fpath_tgrid, chunks=dict())
ds_IcD = pyic.convert_tgrid_data(ds_tg)

path_data    = '/work/mh0033/u241317/smt/vh/wet_e.nc'
dse = xr.open_dataset(path_data)
wet_e = dse.mask
wet_e = wet_e.rename({'ce': 'edge'})

path_data    = '/work/mh0033/u241317/smt/vh/wet_c.nc'
dsc = xr.open_dataset(path_data)
wet_c = dsc.mask
wet_c = wet_c.rename({'cc': 'cell'})

path_data    = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'


#####################################################################
### Calculate dudz over all layers for one timestep
levs   = np.arange(depthc.size)
nz     = levs.size
depthc = depthc[levs]

cc        = int(59799625)
ce        = int(89813639)
#nzi       = int(nz+1)

fpatho = '/work/mh0033/u241317/smt/vh/pp_calc_div_h_nodze.nc'
fo = Dataset(fpatho, 'w', format='NETCDF4')
#fo.createDimension('depthc', nz) # depth
fo.createDimension('depthc', nz) 
fo.createDimension('cc', cc) # new dim cell center

nc_div_h   = fo.createVariable('div_h','f4',('depthc','cc'))
#nc_v   = fo.createVariable('v','f4',('depthc','cc'))
ncv    = fo.createVariable('depthc','f4','depthc')
ncv[:] = depthc[:nz]

# --- prepare output netcdf file
ts      = pyic.timing(ts, 'prepare nc')
varfile = 'vn'
layers  = (  ['sp_001-016']*16 + ['sp_017-032']*16 + ['sp_033-048']*16
          + ['sp_049-064']*16 + ['sp_065-080']*16 + ['sp_081-096']*16 + ['sp_097-112']*16)

# --- load times and flist # only to get time data - it is not the selection of data
time0      = np.datetime64('2010-03-19T01:00:00')
ts         = pyic.timing(ts, 'load times and flist')
search_str = f'{run}_vn_sp_001-016_*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')

itd     = np.argmin((timesd-time0).astype(float)**2)
fpath   = flist_tsd[itd]
pdtime0 = pd.to_datetime(time0) 
tstr    = pdtime0.strftime('%Y%m%d')+'T010000Z'

ds_IcD = ds_IcD.compute()
fixed_vol_norm = pyic.xr_calc_fixed_volume_norm(ds_IcD)

#div_coeff = (  edge_length[edge_of_cell]*orientation_of_normal ) / cell_area_p[:,np.newaxis]
div_coeff = pyic.xr_calc_div_coeff(ds_IcD)
div_coeff = div_coeff.compute()

edge2cell_coeff_cc = pyic.xr_calc_edge2cell_coeff_cc(ds_IcD)
edge2cell_coeff_cc = edge2cell_coeff_cc.compute()
#edge2cell_coeff_cc_da = da.from_array(edge2cell_coeff_cc, chunks=(59799625, 1, 1))
edge2cell_coeff_cc_t = pyic.xr_calc_edge2cell_coeff_cc_t(ds_IcD).compute()

#only march
path_data = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'

#now = datetime.now().time() # time object
print('start calculate at')

####  
for kk, lev in enumerate(levs):
  print('lev', lev)
  ts = pyic.timing(ts, 'loop step')
  # --- load horizontal velocity 1
  fname = f'{run}_{varfile}_{layers[lev]}_{tstr}.nc'
  fpath = f'{path_data}{fname}'
  var   = f'vn{lev+1:03d}_sp'
  #print(f'kk = {kk}/{nz}; fname = {fname}; var = {var}')
  fi   = Dataset(fpath, 'r')
  ve   = fi.variables[var][itsd[itd],:]
  fi.close()
  #ve = xr.open_dataset(fpath)
  ve = xr.DataArray(ve[np.newaxis,:], dims=['depth', 'edge'])
  #ve = ve.squeeze()
  #ve_da = ve.chunk({"edge": 1_000_000})
  dze = xr.DataArray(dzw[lev]*np.ones((1,ve.size)), dims=['depth', 'edge'])
  #dze = dze.squeeze()
  #dze_da = dze.chunk({"edge": 1_000_000})
  #ve_da = da.from_array(ve, chunks=(1_000_000,))
  
  #dzc = xr.DataArray(dzw[lev]*np.ones((1,cc)), dims=['depth', 'cell'])
  #edge2cell
  # p_vn_c = ( edge2cell_coeff_cc[:,:,:]
  #          * ve[edge_of_cell,np.newaxis]
  #          * dzw[lev]
  #          ).sum(axis=1)
  # p_vn_c *= 1./(fixed_vol_norm[:,np.newaxis]*dzw[lev])
  
  print('start calc edges2cell')
  p_vn_c = pyic.xr_edges2cell(ds_IcD, ve, dze, dzw[lev], edge2cell_coeff_cc=edge2cell_coeff_cc, fixed_vol_norm=fixed_vol_norm)
  #p_vn_c = edge2cell_coeff_cc * ve.isel(edge=ds_IcD.edge_of_cell) 
  #p_vn_c *= dze.isel(edge=ds_IcD.edge_of_cell)
  #p_vn_c = p_vn_c.sum(dim='nv')
  #p_vn_c = p_vn_c.transpose('depth', 'cell', 'cart')
  #p_vn_c *= 1./(fixed_vol_norm[:]*dzw[lev])

  p_vn_c = p_vn_c.where(wet_c.isel(depthc=lev)!=0.)
  #dze, dzc,
  #cell2edeges
  #p_vo_3d = pyic.calc_3d_from_2dlocal(ds_IcD, uo, vo)
  #ptp_vo_e = pyic.cell2edges(ds_IcD, p_vn_c*ds_IcD.wet_c[:,:,np.newaxis])*ds_IcD.wet_e * ds_IcD.dze
  print('start calc cell2edges')
  vei = pyic.xr_cell2edges(ds_IcD, p_vn_c, edge2cell_coeff_cc_t=edge2cell_coeff_cc_t)
  vei = vei.where(wet_e.isel(depthc=lev)!=0.)   #*dzw[lev] #out of reconstructions ptp_vn_e = pyic.cell2edges(IcD, p_vn_3d*IcD.wet_c[:,:,np.newaxis])*IcD.wet_e


  #calc div
  div_h = pyic.xr_calc_div(ds_IcD, vei, div_coeff)
  #div_h  = (vei[edge_of_cell]    * div_coeff).sum(axis=1)  
  
  #wvel_from_uv = pyic.calc_wvel(ds_IcD, ptp_vo_e)  
  #save
  nc_div_h[kk,:] = div_h[:cc]
  


fo.close()

#now = datetime.now().time() # time object
print('finish calculate at')