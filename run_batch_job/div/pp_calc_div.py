import sys
import numpy as np
#import matplotlib
#import matplotlib.pyplot as plt
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import cartopy.crs as ccrs
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
#from matplotlib.patches import Rectangle
import xarray as xr

from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')

#print('Are you expirienced?')
#exit

### configure paths
run      = 'ngSMT_tke'
savefig  = False
path_fig = '../pics/'
nnf      = 0

gname = 'smt'
lev   = 'L128'

path_data    = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/????-??/'
#fpath_tgrid = '/mnt/lustre01/work/mh0287/users/leonidas/icon/submeso/grid/cell_grid-OceanOnly_SubmesoNA_2500m_srtm30-icon.nc'
fpath_tgrid  = '/home/mpim/m300602/work/icon/grids/smt/smt_tgrid.nc'
fpath_Tri    = '/mnt/lustre01/work/mh0033/m300602/tmp/Tri.pkl'

path_grid     = f'/mnt/lustre01/work/mh0033/m300602/icon/grids/{gname}/'
path_ckdtree  = f'{path_grid}ckdtree/'
# fpath_ckdtree = f'{path_grid}ckdtree/rectgrids/{gname}_res0.30_180W-180E_90S-90N.npz'
fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'

# mfdset_kwargs = dict(combine='nested', concat_dim='time', 
#                      data_vars='minimal', coords='minimal', compat='override', join='override',
#                      parallel=True,
#                     )

### read out grid data
#  plus first calculations
f = Dataset(fpath_tgrid, 'r')
clon = f.variables['clon'][:] * 180./np.pi # center longitude
clat = f.variables['clat'][:] * 180./np.pi # center latitude
vlon = f.variables['vlon'][:] * 180./np.pi # vertex longitude
vlat = f.variables['vlat'][:] * 180./np.pi # vertex latitude
vertex_of_cell = f.variables['vertex_of_cell'][:].transpose()-1 # vertices of each cellcells ad
edge_of_cell = f.variables['edge_of_cell'][:].transpose()-1 # edges of each cellvertices
edges_of_vertex = f.variables['edges_of_vertex'][:].transpose()-1 # edges around vertex
dual_edge_length = f.variables['dual_edge_length'][:] # lengths of dual edges (distances between triangul 
edge_orientation = f.variables['edge_orientation'][:].transpose()
edge_length = f.variables['edge_length'][:] # lengths of edges of triangular cells
cell_area_p = f.variables['cell_area_p'][:] # area of grid cell 
dual_area = f.variables['dual_area'][:] # areas of dual hexagonal/pentagonal cells
#edge_length = f.variables['edge_length'][:]
adjacent_cell_of_edge = f.variables['adjacent_cell_of_edge'][:].transpose()-1 # cells adjacent to each edge 
orientation_of_normal = f.variables['orientation_of_normal'][:].transpose() # orientations of normals to triangular cell edges
edge_vertices = f.variables['edge_vertices'][:].transpose()-1 #vertices at the end of of each edge
f.close()

dtype = 'float32'
f = Dataset(fpath_tgrid, 'r')
elon = f.variables['elon'][:] * 180./np.pi #edge midpoint longitude
elat = f.variables['elat'][:] * 180./np.pi #edge midpoint latitude
cell_cart_vec = np.ma.zeros((clon.size,3), dtype=dtype)
cell_cart_vec[:,0] = f.variables['cell_circumcenter_cartesian_x'][:] # cartesian position of the prime cell circumcenter
cell_cart_vec[:,1] = f.variables['cell_circumcenter_cartesian_y'][:]
cell_cart_vec[:,2] = f.variables['cell_circumcenter_cartesian_z'][:]
edge_cart_vec = np.ma.zeros((elon.size,3), dtype=dtype)
edge_cart_vec[:,0] = f.variables['edge_middle_cartesian_x'][:] # prime edge center cartesian coordinate x on unit
edge_cart_vec[:,1] = f.variables['edge_middle_cartesian_y'][:]
edge_cart_vec[:,2] = f.variables['edge_middle_cartesian_z'][:]
f.close()

# calc mapping coefficents
grid_sphere_radius = 6.371229e6
dist_vector        = edge_cart_vec[edge_of_cell,:] - cell_cart_vec[:,np.newaxis,:]
norm               = np.sqrt(pyic.scalar_product(dist_vector,dist_vector,dim=2))
prime_edge_length  = edge_length/grid_sphere_radius
fixed_vol_norm     = (0.5 * norm * (prime_edge_length[edge_of_cell]))
fixed_vol_norm     = fixed_vol_norm.sum(axis=1)
edge2cell_coeff_cc = (dist_vector * (edge_length[edge_of_cell,np.newaxis] / grid_sphere_radius) * orientation_of_normal[:,:,np.newaxis] )

############################################################
### calculate divergence of one level
# load velocity normal to triangle edges
# --- load times and flist
time0 = np.datetime64('2010-03-09T01:00:00')
# search_str = f'{run}_T_S_sp_001-016_*.nc'
search_str = f'{run}_vn_sp_001-016_*.nc' # velocity files
# search_str = f'{run}_vn_sp_081-096_*.nc'
flist = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')
###
itd = np.argmin((timesd-time0).astype(float)**2)
fpath = flist_tsd[itd]
f = Dataset(fpath, 'r')
# var = 'vn001_sp'
# var = 'vn090_sp'
depth0 = 52.  # needs to suite dataset
iz = np.argmin((depthc-depth0)**2)
#choose real hight
# Choose data variable
var = f'vn{iz:03d}_sp'
# var = 'vn016_sp'
ve = f.variables[var][itsd[itd],:] # VELOCITies on edges
f.close()
#print(timesd[itd])

# choose depth
depth0 = 100.
iz = (depthc<depth0).sum()
iz, depthc[iz]

# velocity normal in cell center - one vector
p_vn_c = ( edge2cell_coeff_cc[:,:,:]
         * ve[edge_of_cell,np.newaxis]
#          * prism_thick_e[iz,IcD.edge_of_cell,np.newaxis]
         * dzw[iz]
       ).sum(axis=1)
p_vn_c *= 1./(fixed_vol_norm[:,np.newaxis]*dzw[iz])

# calculate divergence
div_coeff = (  edge_length[edge_of_cell]*orientation_of_normal ) / cell_area_p[:,np.newaxis]
div_v     = (ve[edge_of_cell]    * div_coeff).sum(axis=1)


# #############################################################
# # load Temperature Data for test
# search_str = f'{run}_T_S_sp_*.nc'
# flist = np.array(glob.glob(path_data+search_str))
# flist.sort()

# flist = flist[:10]
# ds = xr.open_mfdataset(flist, **mfdset_kwargs, chunks=dict(time=1, depth=1, depth_2=1)) #what does happen here?
# #error in opening this dataset????

# depth0 = 50.
# iz = np.argmin((depthc-depth0)**2)
# iz

# # Choose data variable
# var = f'T{iz:03d}_sp'
# t0 = ds[var]

# timesv, flist_tsv, itsv = pyic.get_timesteps(flist, time_mode='float2date')
# ds['time'] = timesv
# it = ((ds.time - np.datetime64('2010-01-09')).astype(float)**2).argmin()
# ds.time[it]
# t00 = t0.isel(time=it)

##############################################################
### Save Data one layer
levs = np.arange(depthc.size)
nz = levs.size
depthc = depthc[levs]

nc = p_vn_c.size #clon_reg.size

fpatho = '/scratch/u/u241317/calc/pp_calc_div.nc'
#path_data = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'
fo = Dataset(fpatho, 'w')
fo.createDimension('jc', nc)
fo.createDimension('depthc', nz) # depth
fo.createDimension('div_v', div_v.size) # new dim

ncv = fo.createVariable('depthc','f4',('depthc',))
ncv[:] = depthc[:nz]

# nc_div = fo.createVariable('div_v','f4',('depthc','jc',))
# nc_div[:] = div_v[:] #p_vn_c[:nc]

nc_div = fo.createVariable('div_v','f4',('div_v',))
nc_div[:] = div_v[:] #p_vn_c[:nc]

# fo.createDimension('T', t00.size)
# nc_T = fo.createVariable('T','f4',('T',))
# nc_T[:] = t00[:t00.size]

fo.close()

# alternative works for 'small' arrays
# fpatho = '/scratch/u/u241317/calc/pp_calc_div.nc'
# print ('saving to ', fpatho)
# xdiv_v = xr.DataArray(div_v)
# xdiv_v.to_netcdf(path=fpatho)
# print ('finished saving')
