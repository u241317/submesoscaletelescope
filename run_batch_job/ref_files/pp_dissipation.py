import sys
import numpy as np
import matplotlib 
if len(sys.argv)==2:
  matplotlib.use('Agg')
import matplotlib
import matplotlib.pyplot as plt
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import cartopy.crs as ccrs
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
import pandas as pd

from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')

run = 'ngSMT_tke'
savefig = False
path_fig = '../pics/'
nnf=0

# --- define simulation
gname = 'smt'
lev = 'L128'

path_data = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/????-??/'
#fpath_tgrid = '/mnt/lustre01/work/mh0287/users/leonidas/icon/submeso/grid/cell_grid-OceanOnly_SubmesoNA_2500m_srtm30-icon.nc'
fpath_tgrid = '/home/mpim/m300602/work/icon/grids/smt/smt_tgrid.nc'
fpath_Tri = '/mnt/lustre01/work/mh0033/m300602/tmp/Tri.pkl'

path_grid     = f'/mnt/lustre01/work/mh0033/m300602/icon/grids/{gname}/'
path_ckdtree  = f'{path_grid}ckdtree/'
fpath_ckdtree = f'{path_grid}ckdtree/rectgrids/{gname}_res0.30_180W-180E_90S-90N.npz'

# --- define time step
#time0 = np.datetime64('2010-03-01T01:00:00')
time0 = np.datetime64('2010-03-09T01:00:00')

# --- load times and flist
ts = pyic.timing(ts, 'load times and flist')
search_str = f'{run}_vn_sp_001-016_*.nc'
flist = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')

itd = np.argmin((timesd-time0).astype(float)**2)
fpath = flist_tsd[itd]
pdtime0 = pd.to_datetime(time0) 
tstr = pdtime0.strftime('%Y%m%d')+'T010000Z'

ts = pyic.timing(ts, 'load tgrid')

# --- load grid variables
f = Dataset(fpath_tgrid, 'r')
clon = f.variables['clon'][:] * 180./np.pi
clat = f.variables['clat'][:] * 180./np.pi
vlon = f.variables['vlon'][:] * 180./np.pi
vlat = f.variables['vlat'][:] * 180./np.pi
elon = f.variables['elon'][:] * 180./np.pi
elat = f.variables['elat'][:] * 180./np.pi
vertex_of_cell = f.variables['vertex_of_cell'][:].transpose()-1
edge_of_cell = f.variables['edge_of_cell'][:].transpose()-1
edges_of_vertex = f.variables['edges_of_vertex'][:].transpose()-1
dual_edge_length = f.variables['dual_edge_length'][:]
edge_orientation = f.variables['edge_orientation'][:].transpose()
edge_length = f.variables['edge_length'][:]
cell_area_p = f.variables['cell_area_p'][:]
dual_area = f.variables['dual_area'][:]
edge_length = f.variables['edge_length'][:]
adjacent_cell_of_edge = f.variables['adjacent_cell_of_edge'][:].transpose()-1
orientation_of_normal = f.variables['orientation_of_normal'][:].transpose()
edge_vertices = f.variables['edge_vertices'][:].transpose()-1

dtype = 'float32'
cell_cart_vec = np.ma.zeros((clon.size,3), dtype=dtype)
cell_cart_vec[:,0] = f.variables['cell_circumcenter_cartesian_x'][:]
cell_cart_vec[:,1] = f.variables['cell_circumcenter_cartesian_y'][:]
cell_cart_vec[:,2] = f.variables['cell_circumcenter_cartesian_z'][:]
# vert_cart_vec = np.ma.zeros((vlon.size,3), dtype=dtype)
# vert_cart_vec[:,0] = f.variables['cartesian_x_vertices'][:]
# vert_cart_vec[:,1] = f.variables['cartesian_y_vertices'][:]
# vert_cart_vec[:,2] = f.variables['cartesian_z_vertices'][:]
edge_cart_vec = np.ma.zeros((elon.size,3), dtype=dtype)
edge_cart_vec[:,0] = f.variables['edge_middle_cartesian_x'][:]
edge_cart_vec[:,1] = f.variables['edge_middle_cartesian_y'][:]
edge_cart_vec[:,2] = f.variables['edge_middle_cartesian_z'][:]
# edge_prim_norm = np.ma.zeros((elon.size,3), dtype=dtype)
# edge_prim_norm[:,0] = f.variables['edge_primal_normal_cartesian_x'][:]
# edge_prim_norm[:,1] = f.variables['edge_primal_normal_cartesian_y'][:]
# edge_prim_norm[:,2] = f.variables['edge_primal_normal_cartesian_z'][:]
f.close()

grid_sphere_radius = 6.371229e6
earth_angular_velocity = 7.29212e-05
fv = 2.* earth_angular_velocity * np.sin(vlat*np.pi/180.)

# --- derive velocity conversion coeffs
ts = pyic.timing(ts, 'derive velocity coeffs')
sinLon = np.sin(clon*np.pi/180.)
cosLon = np.cos(clon*np.pi/180.)
sinLat = np.sin(clat*np.pi/180.)
cosLat = np.cos(clat*np.pi/180.)

dist_vector = edge_cart_vec[edge_of_cell,:] - cell_cart_vec[:,np.newaxis,:]
norm = np.sqrt(pyic.scalar_product(dist_vector,dist_vector,dim=2))
prime_edge_length = edge_length/grid_sphere_radius
fixed_vol_norm = (  0.5 * norm
                * (prime_edge_length[edge_of_cell]))
fixed_vol_norm = fixed_vol_norm.sum(axis=1)
edge2cell_coeff_cc = (  dist_vector
                    * (edge_length[edge_of_cell,np.newaxis] / grid_sphere_radius)
                    * orientation_of_normal[:,:,np.newaxis] )

# --- derive dissipation coefficients
ts = pyic.timing(ts, 'derive dissip coeffs')
rot_coeff = (  dual_edge_length[edges_of_vertex]*edge_orientation ) / dual_area[:,np.newaxis]
div_coeff = (  edge_length[edge_of_cell]*orientation_of_normal ) / cell_area_p[:,np.newaxis]
grad_coeff = (1./dual_edge_length)
rottr_coeff = (1./edge_length)
  
# --- limit domain
ts = pyic.timing(ts, 'limit domain')
lon_reg_2 = [-75, -55]
lat_reg_2 = [33, 43]
lon_reg = lon_reg_2
lat_reg = lat_reg_2
clon_reg, clat_reg, vertex_of_cell_reg, edge_of_cell_reg, ind_reg = pyic.crop_tripolar_grid(lon_reg, lat_reg,
                       clon, clat, vertex_of_cell, edge_of_cell)
#Tri = matplotlib.tri.Triangulation(vlon, vlat, triangles=vertex_of_cell_reg)
nc = clon_reg.size
  
# --- prepare output netcdf file
ts = pyic.timing(ts, 'prepare nc')
varfile = 'vn'
#layers = ['sp_001-016']*16 + ['sp_017-032']*16 + ['sp_033-048']*16 +['sp_049-064']*16
#nz = len(layers)
layers = (  ['sp_001-016']*16 + ['sp_017-032']*16 + ['sp_033-048']*16
          + ['sp_049-064']*16 + ['sp_065-080']*16 + ['sp_081-096']*16 + ['sp_097-112']*16)
#levs = np.arange(64)
#levs = np.concatenate((np.arange(0,48,2),np.arange(48,48+40,1))).astype(int)
levs = np.arange(depthc.size)
nz = levs.size
depthc = depthc[levs]

#fpatho = '/scratch/m/m300602/tmp/pp_dissipation_all.nc'
fpatho = '/scratch/u/u241317/calc/pp_dissipation_all.nc'
path_data = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'
fo = Dataset(fpatho, 'w')
fo.createDimension('jc', nc)
fo.createDimension('depthc', nz)
ncv = fo.createVariable('depthc','f4',('depthc',))
ncv[:] = depthc[:nz]
nc_diss = fo.createVariable('diss_i','f4',('depthc','jc',))
nc_vort = fo.createVariable('vort_i','f4',('depthc','jc',))
nc_div = fo.createVariable('div_v','f4',('depthc','jc',))
nc_uo = fo.createVariable('uo','f4',('depthc','jc',))
nc_vo = fo.createVariable('vo','f4',('depthc','jc',))

A4 = 0.04 * 500**3
#A4 = 1.
for kk, lev in enumerate(levs):
#for kk in range(nz):
  ts = pyic.timing(ts, 'loop step')

  # --- load velocity
  fname = f'{run}_{varfile}_{layers[lev]}_{tstr}.nc'
  fpath = f'{path_data}{fname}'
  # var = 'vn001_sp'
  # var = 'vn090_sp'
  # var = 'vn016_sp'
  var = f'vn{lev+1:03d}_sp'
  print(f'kk = {kk}/{nz}; fname = {fname}; var = {var}')
  fi = Dataset(fpath, 'r')
  ve = fi.variables[var][itsd[itd],:]
  #print(fi.variables[var].shape)
  fi.close()
  #print(timesd[itd])
  
  # --- derive dissipation
  curl_v = (ve[edges_of_vertex] * rot_coeff).sum(axis=1)
  div_v  = (ve[edge_of_cell]    * div_coeff).sum(axis=1)
  
  grad_div_v = (div_v[adjacent_cell_of_edge[:,1]]-div_v[adjacent_cell_of_edge[:,0]])*grad_coeff
  curlt_curl_v = (curl_v[edge_vertices[:,1]]-curl_v[edge_vertices[:,0]])*rottr_coeff
  
  diss = -grad_div_v**2 - curlt_curl_v**2
  diss *= A4

  ro = curl_v/fv
  
  # --- interpolate to cell center
  diss_i = diss[edge_of_cell].sum(axis=1)/3.
  ro_i = ro[vertex_of_cell].sum(axis=1)/3.

  # --- velocity
  p_vn_c = ( edge2cell_coeff_cc[:,:,:]
           * ve[edge_of_cell,np.newaxis]
  #          * prism_thick_e[iz,IcD.edge_of_cell,np.newaxis]
           * dzw[lev]
         ).sum(axis=1)
  p_vn_c *= 1./(fixed_vol_norm[:,np.newaxis]*dzw[lev])

  u1 = p_vn_c[:,0]
  u2 = p_vn_c[:,1]
  u3 = p_vn_c[:,2]
  
  uo =   u2*cosLon - u1*sinLon
  vo = -(u1*cosLon + u2*sinLon)*sinLat + u3*cosLat

  if False:
    # --- interpolate to rectgrid
    fpath_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'
    ddnpz = np.load(fpath_ckdtree)
    lon_002deg = ddnpz['lon']
    lat_002deg = ddnpz['lat']

    # --- cut domain 2
    i1r2 = (lon_002deg<lon_reg_2[0]).sum()
    i2r2 = (lon_002deg<lon_reg_2[1]).sum()
    j1r2 = (lat_002deg<lat_reg_2[0]).sum()
    j2r2 = (lat_002deg<lat_reg_2[1]).sum()

    lon_002deg_r2 = lon_002deg[i1r2:i2r2]
    lat_002deg_r2 = lat_002deg[j1r2:j2r2]

    diss_002deg = pyic.apply_ckdtree(diss_i, fpath_ckdtree, coordinates='clat clon', radius_of_influence=1.).reshape(lat_002deg.size, lon_002deg.size)
    diss_002deg[diss_002deg==0.] = np.ma.masked
    diss_002deg_r2= diss_002deg[j1r2:j2r2,i1r2:i2r2]

    plt.close('all')
    ccrs_proj = None
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=0.5, projection=ccrs_proj, fig_size_fac=3)
    ii=-1
    #clim = [-5e8,0]
    clim = 2e-7
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(lon_002deg_r2, lat_002deg_r2, diss_002deg_r2, ax=ax, cax=cax, clim=clim,
                    transform=ccrs_proj, rasterized=False)
    
    
    for ax in hca:
        ax.set_xlim(lon_reg)
        ax.set_ylim(lat_reg)
    plt.show()
    sys.exit()
  
  # --- save to netcdf file
  nc_diss[kk,:] = diss_i[ind_reg]
  nc_vort[kk,:] = ro_i[ind_reg]
  nc_div[kk,:] = div_v[ind_reg]
  nc_uo[kk,:] = uo[ind_reg]
  nc_vo[kk,:] = vo[ind_reg]

fo.close()
