## Script to interpolate dataset from cell interface to cell centers
# if reused drop time scale otherwise concat_dim doesent work when reopened and merged with xarray
# %%
import sys

sys.path.insert(0, "../../")
import netCDF4 as nc
import numpy as np
import pandas as pd
import pyicon as pyic
import xarray as xr

import smt_modules.all_funcs as eva
import smt_modules.tools as tools
from smt_modules.icon_smt_levels import depthc, depthi, dzt, dzw
# %%

var = 'dbdy'
# %%
path_save_data = '/work/mh0033/m300878/smt/b/b_depthi/'
ts             = pyic.timing([0], 'start')
# %%
ds_b = eva.load_smt_b_depthc_no_interp(var)
ds_b = ds_b[var]

# %%
beginn = '2010-03-19T19:00:00.000000000'
end    = '2010-03-22T21:00:00.000000000'

ds_b = ds_b.sel(time=slice(beginn, end))

# %%
ds_b_interp = ds_b.interp(depthc=depthi, method='linear', kwargs={'fill_value': 'extrapolate'})
ds_b_interp = ds_b_interp.rename(depthc='depthi')
# %%
for i in np.arange(ds_b.time.size):
    data = ds_b_interp.isel(time=i)
    name = f'{data.time.data:.13}'
    ts   = pyic.timing(ts, f'start calc: {name}')
    data.to_netcdf(f'{path_save_data}{var}_depthi_{name}.nc')

print('all done')

# %%
