#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j
#SBATCH --nodes=1
#SBATCH --partition=compute
####SBATCH --mem=256Gb
#SBATCH --time=08:00:00 
#SBATCH --exclusive 
#SBATCH --account=mh0033
#SBATCH --mail-type=FAIL 


module list

#source /work/mh0033/u241317/pyicon/tools/conda_act_mistral_pyicon_env.sh
source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

# run=$1
startdate=`date +%Y-%m-%d\ %H:%M:%S`
# python -u interpolate_depthi2depthc.py --slurm #${run} 
python -u interpolate_depthc2depthi.py --slurm #${run} 

enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

