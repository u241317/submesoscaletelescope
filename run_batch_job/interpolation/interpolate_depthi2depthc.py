## Script to interpolate dataset from cell interface to cell centers
# if reused drop time scale otherwise concat_dim doesent work when reopened and merged with xarray
# %%
import sys

sys.path.insert(0, "../../")
import netCDF4 as nc
import numpy as np
import pandas as pd
import pyicon as pyic
import xarray as xr

import smt_modules.all_funcs as eva
import smt_modules.tools as tools
from smt_modules.icon_smt_levels import depthc, depthi, dzt, dzw

# %%
path_save_data = '/work/mh0033/m300878/smt/b/'
ts = pyic.timing([0], 'start')
# %%
ds_b = eva.load_smt_b()
#TODO: remove time coords and clon, cat
ds_b        = ds_b.b
ds_b_interp = ds_b.interp(depthi=depthc, method='linear')
ds_b_interp = ds_b_interp.rename(depthi='depthc')
# %%
for i in np.arange(ds_b.time.size):
    data = ds_b_interp.isel(time=i)
    name = f'{data.time.data:.13}'
    ts   = pyic.timing(ts, f'start calc: {name}')
    data.to_netcdf(f'{path_save_data}b_deptc_{name}.nc')

print('all done')

