import sys
import numpy as np
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
#import xarray as xr
import pandas as pd
import seawater as sw
from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')

### configure paths
run      = 'ngSMT_tke'
savefig  = False
path_fig = '../pics/'
nnf      = 0
gname = 'smt'
lev   = 'L128'

path_data    = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/????-??/'

#####################################################################
### Calculate dudz over all layers for one timestep
levs   = np.arange(depthc.size)
nz     = levs.size
depthc = depthc[levs]

cc        = int(59799625)
nzi       = int(nz+1)

fpatho = '/work/mh0033/u241317/smt/N2/pp_calc_N2i.nc'
fo = Dataset(fpatho, 'w', format='NETCDF4')
#fo.createDimension('depthc', nz) # depth
fo.createDimension('depthi', nzi) 
fo.createDimension('cc', cc) # new dim cell center

nc_N2     = fo.createVariable('N2','f4',('depthi','cc'))
ncv       = fo.createVariable('depthi','f4','depthi')
ncv[:]    = depthi[:nzi]

# --- prepare output netcdf file
ts      = pyic.timing(ts, 'prepare nc')
varfile = 'T_S'
layers  = (  ['sp_001-016']*16 + ['sp_017-032']*16 + ['sp_033-048']*16
          + ['sp_049-064']*16 + ['sp_065-080']*16 + ['sp_081-096']*16 + ['sp_097-112']*16)

# --- load times and flist # only to get time data - it is not the selection of data
time0      = np.datetime64('2010-03-09T01:00:00')
ts         = pyic.timing(ts, 'load times and flist')
search_str = f'{run}_vn_sp_001-016_*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')

itd     = np.argmin((timesd-time0).astype(float)**2)
fpath   = flist_tsd[itd]
pdtime0 = pd.to_datetime(time0) 
tstr    = pdtime0.strftime('%Y%m%d')+'T010000Z'

g     = 9.80665
rho0  = 1025.022

#only march
path_data = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'

print('start calculate at')
####  version 2 interpolated
for kk, lev in enumerate(levs[:-1]):
  ts = pyic.timing(ts, 'loop step')
  
  if lev == 0:
    fname  = f'{run}_{varfile}_{layers[lev]}_{tstr}.nc'
    fpath  = f'{path_data}{fname}'
    varS1  = f'S{lev+1:03d}_sp'
    varT1  = f'S{lev+1:03d}_sp'
    fname  = f'{run}_{varfile}_{layers[lev+1]}_{tstr}.nc'
    fpath  = f'{path_data}{fname}'
    varS2  = f'S{lev+2:03d}_sp'
    varT2  = f'S{lev+2:03d}_sp'
    print(f'kk = {kk}/{nz}; fname = {fname}; var = {varS1}, var = {varS2}')
    fi   = Dataset(fpath, 'r')
    S1   = fi.variables[varS1][itsd[itd],:]
    T1   = fi.variables[varT1][itsd[itd],:]
    S2   = fi.variables[varS2][itsd[itd],:]
    T2   = fi.variables[varT2][itsd[itd],:]
    fi.close()

    rhop1 = sw.dens(S2, T2, depthi[lev+1])
    rhom1 = sw.dens(S1, T1, depthi[lev+1])
    #rhop1 = sw.dens(S2, T2, depthc[lev+1])
    #rhop2 = sw.dens(S1, T1, depthc[lev])
    N2    = - g/rho0 * (rhom1 - rhop1) / (depthc[lev+1]-depthc[lev]) # dbdz

    #save
    nc_N2[kk+1,:] = N2
    # swap fields
    T1 = T2
    S1 = S2
  else:
    fname  = f'{run}_{varfile}_{layers[lev+1]}_{tstr}.nc'
    fpath  = f'{path_data}{fname}'
    varS2  = f'S{lev+2:03d}_sp'
    varT2  = f'S{lev+2:03d}_sp'
    print(f'kk = {kk}/{nz}; fname = {fname}; var = {varS2}')
    fi   = Dataset(fpath, 'r')
    S2   = fi.variables[varS2][itsd[itd],:]
    T2   = fi.variables[varT2][itsd[itd],:]
    fi.close()

    rhop1 = sw.dens(S2, T2, depthi[lev+1])
    rhom1 = sw.dens(S1, T1, depthi[lev+1])
    #rhop1 = sw.dens(S2, T2, depthc[lev+1])
    #rhop2 = sw.dens(S1, T1, depthc[lev])
    N2    = - g/rho0 * (rhom1 - rhop1) / (depthc[lev+1]-depthc[lev]) # dbdz

    #save
    nc_N2[kk+1,:] = N2
    # swap fields
    T1 = T2
    S1 = S2

fo.close()

print('finish calculate at')