import sys
import numpy as np
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
import xarray as xr
import pandas as pd
import gsw
from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')

print('Calculation of N2')

### configure paths
run      = 'ngSMT_tke'
savefig  = False
path_fig = '../pics/'
nnf      = 0
gname    = 'smt'
lev      = 'L128'

#####################################################################
### Calculate dudz over all layers for one timestep
levs   = np.arange(depthc.size)
nz     = levs.size
depthc = depthc[levs]
cc     = int(59799625)
nzi    = int(nz+1)
g      = 9.80665
rho0   = 1025.022

# --- load times and flist # only to get time data - it is not the selection of data
path_data  = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/????-??/'
time0      = np.datetime64('2010-03-22T05:00:00')
timeEnd    = np.datetime64('2010-03-22T21:00:00')
ts         = pyic.timing(ts, 'load times and flist')
search_str = f'{run}_T_S_sp_001-016_*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
timesd, flist_tsd, itsd = pyic.get_timesteps(flist, time_mode='float2date')

#time0 #timeEnd
itd0     = np.argmin((timesd-time0).astype(float)**2)
itdEnd   = np.argmin((timesd-timeEnd).astype(float)**2)

for itd in range(itd0,itdEnd+1):
    #####################################################################
    ### load file for oine timestep
    print('Load files for timestep', {timesd[itd]})
    path_data    = f'/work/mh0033/u241317/smt/T/pp_calc_T_period_{timesd[itd]}.nc'
    dt = xr.open_dataset(path_data)
    to = dt.T
    to = to.rename({'temperature': 'cc'}) # only necessary for corrupted dims
    path_data    = f'/work/mh0033/u241317/smt/S/pp_calc_S_period_{timesd[itd]}.nc'
    ds = xr.open_dataset(path_data)
    so = ds.S
    so = so.rename({'salinity': 'cc'}) # only necessary for corrupted dims

    ### write data to
    fpatho   = f'/work/mh0033/u241317/smt/N2/pp_calc_N2_period_{timesd[itd]}.nc'
    fo = Dataset(fpatho, 'w', format='NETCDF4')
    #fo.createDimension('depthc', nz) # depth
    fo.createDimension('depthi', nzi) 
    fo.createDimension('cc', cc) # new dim cell center
    nc_N2     = fo.createVariable('N2','f4',('depthi','cc'))
    ncv       = fo.createVariable('depthi','f4','depthi')
    ncv[:]    = depthi[:nzi]

    pdtime   = pd.to_datetime(timesd[itd])
    print('start calculate N2 for timestep', pdtime)
    
    for kk, lev in enumerate(levs[:-1]):
      #ts = pyic.timing(ts, 'loop step')

      if lev == 0:
        S2    = so.isel(depthc=lev+1)
        S1    = so.isel(depthc=lev)
        T2    = to.isel(depthc=lev+1)
        CT2   = gsw.CT_from_pt(S2,T2)
        T1    = to.isel(depthc=lev)
        CT1   = gsw.CT_from_pt(S1,T1)
        rhop1 = gsw.rho(S2, CT2, depthi[lev+1])
        rhom1 = gsw.rho(S1, CT1, depthi[lev+1])
        N2    = - g/rho0 * (rhom1 - rhop1) / dzt[lev+1] # dbdz

        nc_N2[kk+1,:] = N2 
        # swap fields
        CT1 = CT2
        S1 = S2
      else:
        S2    = so.isel(depthc=lev+1)
        T2    = to.isel(depthc=lev+1)
        CT2   = gsw.CT_from_pt(S2,T2)
        rhop1 = gsw.rho(S2, CT2, depthi[lev+1])
        rhom1 = gsw.rho(S1, CT1, depthi[lev+1])
        N2    = - g/rho0 * (rhom1 - rhop1) / dzt[lev+1] # dbdz

        nc_N2[kk+1,:] = N2
        # swap fields
        CT1 = CT2
        S1 = S2

    fo.close()
    
    print('finish calculate')