import sys
import numpy as np
from netCDF4 import Dataset, num2date
from ipdb import set_trace as mybreak
import pyicon as pyic
import glob
import pickle
import maps_icon_smt_temp as smt
import datetime
import xarray as xr
import pandas as pd
import gsw
from icon_smt_levels import dzw, dzt, depthc, depthi

ts = pyic.timing([0], 'start')

### configure paths
run      = 'ngSMT_tke'
savefig  = False
path_fig = '../pics/'
nnf      = 0
gname = 'smt'
lev   = 'L128'

path_data    = f'/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/????-??/'

#####################################################################
### Calculate dudz over all layers for one timestep
levs   = np.arange(depthc.size)
nz     = levs.size
depthc = depthc[levs]

cc        = int(59799625)
nzi       = int(nz+1)

fpatho = '/work/mh0033/u241317/smt/N2/pp_calc_N2t.nc'
fo = Dataset(fpatho, 'w', format='NETCDF4')
#fo.createDimension('depthc', nz) # depth
fo.createDimension('depthi', nzi) 
fo.createDimension('cc', cc) # new dim cell center

nc_N2     = fo.createVariable('N2','f4',('depthi','cc'))
ncv       = fo.createVariable('depthi','f4','depthi')
ncv[:]    = depthi[:nzi]


#g     = 9.80665
#rho0  = 1025.022

path_data    = '/work/mh0033/u241317/smt/T/pp_calc_T.nc'
dt = xr.open_dataset(path_data)
to = dt.T
path_data    = '/work/mh0033/u241317/smt/S/pp_calc_S.nc'
ds = xr.open_dataset(path_data)
so = ds.S

g     = 9.80665
rho0  = 1025.022

#only march
path_data = '/mnt/lustre01/work/mh0287/users/leonidas/icon/ngSMT/results/2010-03/'

print('start calculate neu')
####  version 2 interpolated
for kk, lev in enumerate(levs[:-1]):
  ts = pyic.timing(ts, 'loop step')
  
  if lev == 0:
    S2 = ds.S.isel(depthc=lev+1)
    S1 = ds.S.isel(depthc=lev)
    T2 = dt.T.isel(depthc=lev+1)
    CT2 = gsw.CT_from_pt(S2,T2)
    T1 = dt.T.isel(depthc=lev)
    CT1 = gsw.CT_from_pt(S1,T1)
    rhop1 = gsw.rho(S2, CT2, depthi[lev+1])
    rhom1 = gsw.rho(S1, CT1, depthi[lev+1])
    N2    = - g/rho0 * (rhom1 - rhop1) / dzt[lev+1] # dbdz

    #save
    nc_N2[kk+1,:] = N2
    # swap fields
    CT1 = CT2
    S1 = S2
  else:
    S2 = ds.S.isel(depthc=lev+1)
    T2 = dt.T.isel(depthc=lev+1)
    CT2 = gsw.CT_from_pt(S2,T2)
    rhop1 = gsw.rho(S2, CT2, depthi[lev+1])
    rhom1 = gsw.rho(S1, CT1, depthi[lev+1])
    N2    = - g/rho0 * (rhom1 - rhop1) / dzt[lev+1] # dbdz

    #save
    nc_N2[kk+1,:] = N2
    # swap fields
    CT1 = CT2
    S1 = S2

fo.close()
#
print('finish calculate')