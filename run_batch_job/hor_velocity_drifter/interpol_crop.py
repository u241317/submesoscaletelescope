# %%
import sys
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import dask as da

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import time as pytime

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload
from netCDF4 import Dataset  # type: ignore


# %%
ts            = pyic.timing([0], 'start')
ds            = eva.load_smt_vn()
int_max_depth = 16 # for multiple depths
ds            = ds.isel(depthc=slice(0,int_max_depth))

fpath_ckdtree  = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
path_save_data = '/work/mh0033/m300878/crop_interpolate/hor_velocities/'
lon_reg        = np.array([-90,0])
lat_reg        = np.array([0,90])

# %%
num = int(sys.argv[1])
t1  = int(sys.argv[2])
t2  = int(sys.argv[3])

# time = pd.date_range('2020-01-01', periods=240, freq='H')

steps_global = np.arange(ds.time.size)
steps_global = np.arange(ds.time[:].size) #+ 960
steps        = steps_global[t1:t2]
# %%
def xr_edges2cell(edge_of_cell, ve, dze, dzc, edge2cell_coeff_cc=None, fixed_vol_norm=None):
    if ve.dims != dze.dims:
      raise ValueError('::: Dims of ve and dze have to be the same!:::')
    p_vn_c = (
        edge2cell_coeff_cc 
        * ve.isel(edge=edge_of_cell) 
        #* ds_fx.prism_thick_e.isel(edge=ds_IcD.edge_of_cell)
        * dze.isel(edge=edge_of_cell)
    ).sum(dim='nv')
    if 'depth' in p_vn_c.dims:
        p_vn_c = p_vn_c.transpose('depth', 'cell', 'cart')
    
    p_vn_c = p_vn_c / (
        fixed_vol_norm 
        #* ds_fx.prism_thick_c
        * dzc
    )
    return p_vn_c

def get_variables():
  tgrid  = eva.load_smt_grid()
  ds_IcD = pyic.convert_tgrid_data(tgrid)
  ds_IcD = ds_IcD.compute()

  fixed_vol_norm = pyic.xr_calc_fixed_volume_norm(ds_IcD)
  fixed_vol_norm = fixed_vol_norm.compute()

  edge2cell_coeff_cc = pyic.xr_calc_edge2cell_coeff_cc(ds_IcD)
  edge2cell_coeff_cc = edge2cell_coeff_cc.compute()

  edge_of_cell = ds_IcD.edge_of_cell.compute()

  sinLon = np.sin(ds_IcD.clon*np.pi/180.)
  cosLon = np.cos(ds_IcD.clon*np.pi/180.)
  sinLat = np.sin(ds_IcD.clat*np.pi/180.)
  cosLat = np.cos(ds_IcD.clat*np.pi/180.)

  ce     = ds_IcD.edge.size
  ncells = ds_IcD.cell.size
  nz     = depthc.size
  return fixed_vol_norm, edge_of_cell, edge2cell_coeff_cc, sinLon, cosLon, sinLat, cosLat, ce, ncells, nz

fixed_vol_norm, edge_of_cell, edge2cell_coeff_cc, sinLon, cosLon, sinLat, cosLat, ce, ncells, nz = get_variables()
ts      = pyic.timing(ts, 'finished calc coefficients')

# %%


##################################################################################

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
# print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]  # type: ignore
steps = list_all_pros[rank]

pytime.sleep(0.1*rank)
###################################################################################

# %%
for nn, step in enumerate(steps):  # type: ignore

    print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))  # type: ignore
    ts = pyic.timing(ts, f'loop time step: {step}')
    t1 = ds.time[step]
    t2 = ds.time[step+1]
    #print(t1, t2)
    if t1==t2:
      continue

    idepth    = 4
    pdtime = ds.time.isel(time=step)
    nlat=4499
    nlon=4500
    ######### open file ###########
    fpatho = f'{path_save_data}depthc_{idepth}/v_h_crop_interp{pdtime.data:.13}.nc'
    fo     = Dataset(fpatho, 'w', format='NETCDF4')
    fo.createDimension('lon', nlon)
    fo.createDimension('lat', nlat)
    fo.createVariable('lon','f4',('lon'))
    fo.createVariable('lat','f4',('lat'))

    # fo.createDimension('depthc', 1) 
    # ncv  = fo.createVariable('depthc','f4','depthc')
    # ncv  = depthc[idepth]
    nc_u = fo.createVariable('u','f4',('lon','lat'))
    nc_v = fo.createVariable('v','f4',('lon','lat'))

    vet = ds.isel(time=step)
    
    ts       = pyic.timing(ts, 'calc hor. v for one timestep')
    print(f"start {step} timestep: {pdtime.data:.13}")  
    # for lev in np.arange(1):
    print('int depth level', idepth, 'depth', depthc[idepth])
    ve     = vet.isel(depthc=idepth)
    ve     = ve.expand_dims(dim='depthc')
    ve     = ve.rename({'ncells': 'edge'})
    ve     = ve.compute() #important!
    dze    = xr.DataArray(dzw[idepth]*np.ones((1,ce)), dims=['depthc', 'edge'])
    p_vn_c = xr_edges2cell(edge_of_cell, ve, dze, dzw[idepth], edge2cell_coeff_cc=edge2cell_coeff_cc, fixed_vol_norm=fixed_vol_norm)
    u1     = p_vn_c[:,0]
    u2     = p_vn_c[:,1]
    u3     = p_vn_c[:,2]
    uo     = u2 * cosLon - u1 * sinLon
    vo     = -(u1 * cosLon + u2 * sinLon) * sinLat + u3 * cosLat

    uo     = uo.drop(['clon', 'clat'])
    vo     = vo.drop(['clon', 'clat'])

    uo_inter = pyic.interp_to_rectgrid_xr(uo, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    vo_inter = pyic.interp_to_rectgrid_xr(vo, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    uo_inter = uo_inter.squeeze()
    vo_inter = vo_inter.squeeze()
    # swap order of dimensions only necessary for single depth layer
    uo_inter = uo_inter.transpose('lon', 'lat')
    vo_inter = vo_inter.transpose('lon', 'lat')
    nc_u[:,:] = uo_inter.data
    nc_v[:,:] = vo_inter.data
    # print(f'finish calculate level {idepth}, timestep: {pdtime.data:.13}')


    # save to netcdf
    print(f'finish calculate step {step}, timestep: {pdtime.data:.13}')
    #add atribute to nc file
    fo.description = f'horizontal velocity at depth {depthc[idepth]}m'

    fo.variables['lon'][:] = uo_inter.lon.data
    fo.variables['lat'][:] = uo_inter.lat.data
    fo.close()

print('all done')

# %%
# quick plot
if False:
  reload(eva)
  da = eva.load_smt_v_crop_interp()

  hca, hcb = pyic.arrange_axes(1,1, plot_cb=True, asp=1, fig_size_fac=5, projection=ccrs_proj)
  ii=-1
  ii+=1; ax=hca[ii]; cax=hcb[ii]
  pyic.shade(da.lon, da.lat, da.isel(time=644).u.data.transpose(), ax=ax, cax=cax, clim=0.5, transform=ccrs_proj, rasterized=False, cmap='RdYlBu_r')
  pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)   # type: ignore
# %%
if False:
  # test
  da1 = eva.load_smt_v_crop_interp()
  # %%
  da2 = eva.load_smt_v()
  da_new = da1.isel(time=808).u.transpose()
  da_new = da_new.compute()
  da_old = da2.isel(time=100, depthc=3).u
  da_old = pyic.interp_to_rectgrid_xr(da_old, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) 

  print(da_new.mean(), da_old.mean())
  print(da_new.std(), da_old.std())


