#!/bin/bash
#SBATCH --job-name=postproc_smt
#SBATCH --time=08:00:00
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --account=mh0033
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j

source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

mpirun -np 4 python -u interpol_crop.py $1 $2 $3
# mpirun -np 4 python -u python_mpi4py.py $1 $2 $3
