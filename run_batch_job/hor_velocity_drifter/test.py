# %%
import numpy as np
import xarray as xr
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import pyicon as pyic
import smt_modules.all_funcs as eva

ccrs_proj = pyic.ccrs.PlateCarree()

# %%
lev=2

for lev in np.arange(1):
        print('int depth level', lev, 'depth', depthc[lev])
# %%

path2 = '/work/mh0033/m300878/crop_interpolate/hor_velocities/depthc_2/v_h_crop_interp2010-01-24T07.nc'
path3 = '/work/mh0033/m300878/crop_interpolate/hor_velocities/depthc_3/v_h_crop_interp2010-01-24T07.nc'

# %%
# %%
ds2 = xr.open_dataset(path2)
ds3 = xr.open_dataset(path3)
# %%
ds3.u.plot()
# %%
ds2.u.plot()
# %%
# bias
bias = ds2.u - ds3.u
bias.plot()
# %%

if True:
#   reload(eva)
  da = eva.load_smt_v_crop_interp()

  hca, hcb = pyic.arrange_axes(1,1, plot_cb=True, asp=1, fig_size_fac=5, projection=ccrs_proj)
  ii=-1
  ii+=1; ax=hca[ii]; cax=hcb[ii]
  pyic.shade(da.lon, da.lat, da.isel(time=644).u.data.transpose(), ax=ax, cax=cax, clim=0.5, transform=ccrs_proj, rasterized=False, cmap='RdYlBu_r')
  pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)   # type: ignore
# %%
# test
from importlib import reload
reload(eva)
fpath_ckdtree  = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
path_save_data = '/work/mh0033/m300878/crop_interpolate/hor_velocities/'
lon_reg        = np.array([-90,0])
lat_reg        = np.array([0,90])
da1 = eva.load_smt_v_crop_interp()
# %%
import pandas as pd
start_date = pd.to_datetime('2010-01-09T05')
da_new = da1.isel(time=808).u.transpose()
da_new = da_new.compute()
da2 = eva.load_smt_v()
da_old = da2.isel(time=100, depthc=3).u
da_old = pyic.interp_to_rectgrid_xr(da_old, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) 

print(da_new.mean().values, da_old.mean().values)
print(da_new.std().values,  da_old.std().values)
# %%
path0 = '/work/mh0033/m300878/crop_interpolate/hor_velocities/depthc_4/v_h_crop_interp2010-03-24T07.nc'
ds0 = xr.open_dataset(path0)

# %%
ds0.v.plot()
# %%
