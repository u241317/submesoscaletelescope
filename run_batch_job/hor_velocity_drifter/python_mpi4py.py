import sys
import numpy as np
import mpi4py
import time as pytime
import pandas as pd

num = int(sys.argv[1])
t1  = int(sys.argv[2])
t2  = int(sys.argv[3])


# time = np.linspace(1,24, 240)
time = pd.date_range('2020-01-01', periods=240, freq='H')


steps_global = np.arange(time.size)
steps = steps_global[t1:t2]

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1


list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

pytime.sleep(0.1*rank)


# use mpi4py here
for kk, step in enumerate(steps):
  print(f'node: {num}: kk = {kk+1}/{steps.size}, step = {step}')
  print(f'{time[step]}')

