import numpy as np
from netCDF4 import Dataset
import sys, os
from importlib import reload

import pyicon as pyic
reload(pyic)

ts = pyic.timing([0], 'start')

# with open('path_data.txt') as f:
# path_data  = f.read()[:-1]
path_data  = '/work/mh0033/m300878/grids/'
tgname     = 'smtwv_oce_2018'
gname      = 'smtwv_oce_2022_tgrid'
path_grid  = f'{path_data}/{tgname}/'
path_tgrid = f'/home/m/m300602/work/icon/grids/smtwv_oce_2022/'


fname_tgrid   = f'{gname}.nc'
#path_ckdtree  = f'{path_data}/{tgname}/ckdtree/'
path_rgrid    = f'{path_data}/{tgname}/ckdtree/rectgrids/' 
# path_sections = f'{path_data}/{tgname}/ckdtree/sections/' 

# fpath_tgrid_source = f'{path_tgrid}/{fname_tgrid}'
# fpath_tgrid_target = f'{path_grid}/{tgname}_tgrid.nc'
# fpath_fx_source = f'{path_tgrid}/{gname}_fx.nc'
# fpath_fx_target = f'{path_grid}/{tgname}_L40_fx.nc'

#print(fpath_tgrid_source)
#print(fpath_tgrid_target)
#print(fpath_fx_source)
#print(fpath_fx_target)

# all_grids = [
#   'global_1.0',
#   'global_0.3',
#             ]

# all_secs = [
#   '30W_300pts',
#   '170W_300pts',
#             ]

# #all_grids = []
# #all_secs = []

# gnames = [gname]

# # --- create some paths
if not os.path.exists(path_rgrid): 
  os.makedirs(path_rgrid)
# if not os.path.exists(path_sections): 
#   os.makedirs(path_sections)
# # --- links grid file and fx file
# if not os.path.exists(fpath_tgrid_target):
#   os.symlink(fpath_tgrid_source, fpath_tgrid_target)
# if not os.path.exists(fpath_fx_target):
#   os.symlink(fpath_fx_source, fpath_fx_target)

# for gname in gnames:
ts = pyic.timing(ts)


  # --- grids
sname = 'reg_0.01'
#   if sname in all_grids:
pyic.ckdtree_hgrid(lon_reg=[-20,20], lat_reg=[-50,-10], res=0.01,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_rgrid,
                      sname  = sname,
                      gname  = gname,
                      tgname = tgname,
                      )

# sname = 'reg_0.005'
# #   if sname in all_grids:
# pyic.ckdtree_hgrid(lon_reg=[-20,20], lat_reg=[-50,-10], res=0.005,
#                       fname_tgrid  = fname_tgrid,
#                       path_tgrid   = path_tgrid,
#                       path_ckdtree = path_rgrid,
#                       sname  = sname,
#                       gname  = gname,
#                       tgname = tgname,
#                       )
  
#   sname = 'global_0.3'
#   if sname in all_grids:
#     pyic.ckdtree_hgrid(lon_reg=[-180.,180.], lat_reg=[-90.,90.], res=0.3,
#                       fname_tgrid  = fname_tgrid,
#                       path_tgrid   = path_tgrid,
#                       path_ckdtree = path_rgrid,
#                       sname = sname,
#                       gname = gname,
#                       tgname = tgname,
#                       )
  
#   # --- sections
#   sname = '30W_300pts'
#   if sname in all_secs:
#     dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[-30,-80], p2=[-30,80], npoints=300,
#                       fname_tgrid  = fname_tgrid,
#                       path_tgrid   = path_tgrid,
#                       path_ckdtree = path_sections,
#                       sname = sname,
#                       gname = gname,
#                       tgname = tgname,
#                       )
    
#   sname = '170W_300pts'
#   if sname in all_secs:
#     dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[-170,-80], p2=[-170,80], npoints=300,
#                       fname_tgrid  = fname_tgrid,
#                       path_tgrid   = path_tgrid,
#                       path_ckdtree = path_sections,
#                       sname = sname,
#                       gname = gname,
#                       tgname = tgname,
#                       )

print('make_ckdtree.py: All done!')
ts = pyic.timing(ts, 'All done!')



# test
if False:
  # %%
  fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
  data          = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_to/smtwv0007_oce_3d_to_PT1H_20190812T151500Z.nc'
  # %%
  ds = xr.open_dataset(data)
  sel = ds.to.isel(depth=17).isel(time=0)
  sel = sel.compute()
  # %%
  ds_inter = pyic.interp_to_rectgrid_xr(sel, fpath_ckdtree=fpath_ckdtree, lon_reg=[-5,0], lat_reg=[-30,-25])
  
  # %%
  ds_inter.plot(cmap='inferno')
  # %%
