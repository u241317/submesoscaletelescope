# %%
import sys
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import numpy as np
import xarray as xr    
import dask as da
import pandas as pd
import netCDF4 as nc

import smt_modules.all_funcs as eva
from   smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 

from importlib import reload

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00')

# client, cluster = scluster.init_my_cluster()
cluster
client


###############################################################################
# %% start evaluation

def calc_time_mean(data, interval):
    # interval: '1D', '2D', '1W'
    return data.resample(time=interval).mean()

def save_data(data, path_save_data, name):
    data.to_netcdf(f'{path_save_data}{name}.nc')


# %%
def compute_means(var, tstring, var_name, chunks, path_save_data):
    """compute dayly averages for 4 days then 2day averages and a 4 day average"""
    ################# 1D mean #################

    time_window = '1D'
    data                        = calc_time_mean(var, time_window)
    data.attrs["time averaged"] = tstring
    data.attrs["time steps"]    = "12 on each day"
    print(data)
    save_data(data, path_save_data, f'{var_name}_{time_window}_mean_quan')

    ################# 2D mean #################

    path_data   = f'{path_save_data}{var_name}_{time_window}_mean.nc'
    time_window = '2D'
    var         = xr.open_dataset(path_data, chunks=chunks)
    data        = calc_time_mean(var, time_window)
    print(data)
    save_data(data, path_save_data, f'{var_name}_{time_window}_mean')

    ################# 4D mean #################
    path_data   = f'{path_save_data}{var_name}_{time_window}_mean.nc'
    time_window = '4D'
    var         = xr.open_dataset(path_data, chunks=chunks)
    data        = calc_time_mean(var, time_window)
    print(data)
    save_data(data, path_save_data, f'{var_name}_{time_window}_mean')
# %%
################# load and prepare data #################
# load and lazy compute data
path_save_data = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
beginn  = '2010-03-16-T01'
end     = '2010-03-19-T23'
tstring = f'{beginn}', f'{end}'


var_name = 'u'
if var_name == 'ub':
    ds_v     = eva.load_smt_v()
    ds_v_sel = ds_v.sel(time=slice(f'{beginn}', f'{end}'))
    ds_b     = eva.load_smt_b_depthc()
    ds_b     = ds_b.b.sel(time=slice(f'{beginn}', f'{end}'))
    var      = ds_b * ds_v_sel.u
elif var_name == 'vb':
    ds_v     = eva.load_smt_v()
    ds_v_sel = ds_v.sel(time=slice(f'{beginn}', f'{end}'))
    ds_b     = eva.load_smt_b_depthc()
    ds_b     = ds_b.b.sel(time=slice(f'{beginn}', f'{end}'))
    var      = ds_b * ds_v_sel.v
elif var_name == 'u':
    ds_v     = eva.load_smt_v()
    var = ds_v.sel(time=slice(f'{beginn}', f'{end}')).u
elif var_name == 'v':
    ds_v     = eva.load_smt_v()
    var      = ds_v.sel(time=slice(f'{beginn}', f'{end}')).v
elif var_name == 'b':
    def load():
        ds_b     = eva.load_smt_b_depthc()
        var      = ds_b.sel(time=slice(f'{beginn}', f'{end}')).b
        return var
    var = load()
else:
    raise ValueError('var_name not recognized')

var      = var.drop(['clon', 'clat'])
var.name = f'{var_name}_mean'
print(var)

chunks_c = {'depthc': 1, 'time': 1}
chunks_i = {'depthi': 1, 'time': 1}

if   var.depthc.size == 112: chunks = chunks_c
elif var.depthi.size == 113: chunks = chunks_i
else: raise ValueError('depth dimension not recognized')

# %%

compute_means(var, tstring, var_name, chunks, path_save_data)

# %%
cluster.close()
client.close()
