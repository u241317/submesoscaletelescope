# %%
import sys
sys.path.insert(0, "../../")
import pyicon as pyic
import numpy as np
import xarray as xr    
import dask as da
import pandas as pd
import netCDF4 as nc

import smt_modules.all_funcs as eva
from   smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 

from importlib import reload
# %%
def save_data(data, path_save_data, name):
    data.to_netcdf(f'{path_save_data}{name}.nc')
# %% load data
path_save_data = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
# beginn  = '2010-03-15-T01'
# end     = '2010-03-19-T23'

chunks_c = {'depthc': 1, 'time': 1}
chunks_i = {'depthi': 1, 'time': 1}

# string list with time windows
time_windows = '1D', '2D', '4D'

var = 'ub_prime'

for i in enumerate(time_windows):
    time_window = i[1]
    print(time_window)
    var_name = 'b'
    ds_b  = xr.open_dataset(f'{path_save_data}{var_name}_{time_window}_mean.nc', chunks=chunks_c)
    
    if var == 'vb_prime':
        var_name      = 'vb'
        ds_vb         = xr.open_dataset(f'{path_save_data}{var_name}_{time_window}_mean.nc', chunks=chunks_c)
        var_name      = 'v'
        ds_v          = xr.open_dataset(f'{path_save_data}{var_name}_{time_window}_mean.nc', chunks=chunks_c)
        var_name      = 'vb_prime'
        vb_prime      = ds_vb.vb_mean - ds_v.v_mean * ds_b.b_mean
        vb_prime.name = f'{var_name}_mean'
        print(vb_prime)
        save_data(vb_prime, path_save_data, f'{var_name}_{time_window}_mean')
    elif var == 'ub_prime':
        var_name      = 'u'
        ds_u          = xr.open_dataset(f'{path_save_data}{var_name}_{time_window}_mean.nc', chunks=chunks_c)
        # ds_u          = ds_u.sel(time=slice(f'{beginn}', f'{end}'))
        var_name      = 'ub'
        ds_ub         = xr.open_dataset(f'{path_save_data}{var_name}_{time_window}_mean.nc', chunks=chunks_c)
        var_name      = 'ub_prime'
        ub_prime      = ds_ub.ub_mean - ds_u.u_mean * ds_b.b_mean
        ub_prime.name = f'{var_name}_mean'
        print(ub_prime)
        save_data(ub_prime, path_save_data, f'{var_name}_{time_window}_mean')



# %%
