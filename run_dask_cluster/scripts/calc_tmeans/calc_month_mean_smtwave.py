# %%
import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "../../../")
import glob, os
import pyicon as pyic
import numpy as np
import xarray as xr    
import dask as da
import pandas as pd
import netCDF4 as nc

import smt_modules.all_funcs as eva
from   smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 

from importlib import reload

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00')

# client, cluster = scluster.init_my_cluster()
cluster
client

ts            = pyic.timing([0], 'start')

###############################################################################
# %% start evaluation

def calc_time_mean(data, interval):
    # interval: '1D', '2D', '1W'
    return data.resample(time=interval).mean()

def save_data(data, path_save_data, name):
    data.to_netcdf(f'{path_save_data}{name}.nc')


# %%
def compute_means(var, tstring, var_name, chunks, path_save_data):
    """compute dayly averages for 4 days then 2day averages and a 4 day average"""
    ################# 1D mean #################

    time_window = '1D'
    data                        = calc_time_mean(var, time_window)
    data.attrs["time averaged"] = tstring
    data.attrs["time steps"]    = "12 on each day"
    print(data)
    save_data(data, path_save_data, f'{var_name}_{time_window}_mean_quan')

    ################# 2D mean #################

    path_data   = f'{path_save_data}{var_name}_{time_window}_mean.nc'
    time_window = '2D'
    var         = xr.open_dataset(path_data, chunks=chunks)
    data        = calc_time_mean(var, time_window)
    print(data)
    save_data(data, path_save_data, f'{var_name}_{time_window}_mean')

    ################# 4D mean #################
    path_data   = f'{path_save_data}{var_name}_{time_window}_mean.nc'
    time_window = '4D'
    var         = xr.open_dataset(path_data, chunks=chunks)
    data        = calc_time_mean(var, time_window)
    print(data)
    save_data(data, path_save_data, f'{var_name}_{time_window}_mean')

def compute_means_month(var, var_name, chunks, path_save_data, ts):
    """compute dayly averages for 4 days then 2day averages and a 4 day average"""
    ################# 1W mean #################
    print('calc weekly means')
    time_window = '7D'
    data                        = calc_time_mean(var, time_window)
    data.attrs["time averaged"] = f'start: {var.time[0].values}, end {var.time[-1].values}'
    data.attrs["time steps"]    = "12 on each day"
    for i in range(data.time.size):
        ts      = pyic.timing(ts, f'start week {i}')
        print(data.isel(time=i))
        save_data(data.isel(time=i), path_save_data, f'{var_name}_{time_window}_mean_{i}')


    ################# 1M mean #################
    print('calc monthly means')
    path_data   = f'{path_save_data}{var_name}_{time_window}_mean_*.nc'
    flist       = np.array(glob.glob(path_data))
    flist.sort() 
    time_window = '1M'
    var         = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True,  chunks=chunks)
    data        = calc_time_mean(var, time_window)
    ts      = pyic.timing(ts, f'start month mean ')
    print(data)
    save_data(data, path_save_data, f'{var_name}_{time_window}_mean')

def compute_mean_7D(var, var_name, path_save_data):
    """compute 7D mean"""
    ################# 1W mean #################
    print('calc weekly means')
    time_window = '7D'
    data                        = calc_time_mean(var, time_window)
    data.attrs["time averaged"] = f'start: {var.time[0].values}, end {var.time[-1].values}'
    data.attrs["time steps"]    = "12 on each day"
    print(data)
    save_data(data, path_save_data, f'{var_name}_{time_window}_mean')

# %%
var_name = 'b'
################# load and prepare data #################
# load and lazy compute data
# path_save_data = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
# path_save_data = '/work/mh0033/m300878/parameterization/time_averages/month_mean/'
# path_save_data = '/work/mh0033/m300878/parameterization/time_averages/month_mean/S/'
# path_save_data = '/work/mh0033/m300878/parameterization/time_averages_smtwave/jul19_mean/T/'
# path_save_data = '/work/mh0033/m300878/parameterization/time_averages_smtwave/jul19_mean/S/'
# path_save_data = f'/work/bm1239/m300878/smt_data/tave/jul19_mean/{var_name}/'

path_save_data = f'/work/bm1239/m300878/smt_data/tave/jul19_mean/2019_07_mean_T05-T12/{var_name}/'

# check if folder exists
if not os.path.exists(path_save_data):
    os.makedirs(path_save_data)
    print(f'created folder {path_save_data}')

# beginn  = '2019-08-01-T00:00:00'
# end     = '2019-08-07-T23:15:00'
beginn  = '2019-07-05-T00:00:00'
end     = '2019-07-11-T23:15:00'
tstring = f'{beginn}', f'{end}'


if var_name == 'T':
    ds_T     = eva.load_smt_wave_to_exp7_08(it=2)
    var      = ds_T.to
elif var_name == 'S':
    ds_S     = eva.load_smt_wave_so_exp7_08(it=2)
    var      = ds_S.so
elif var_name == 'w':
    ds_w     = eva.load_smt_wave_all('w', 7, remove_bnds=True, it=2)
    var      = ds_w.w.sel(time=slice(f'{beginn}', f'{end}'))
elif var_name == 'wb':
    ds_w         = eva.load_smt_wave_all('w', 7, remove_bnds=True, it=2)
    ds_w         = ds_w.drop(['clon', 'clat'])
    var1         = ds_w.w.sel(time=slice(f'{beginn}', f'{end}'))
    ds_b         = eva.load_smt_wave_b()
    var2         = ds_b.b
    var2['time'] = var1.time
    var2['depth'] = var1.depth #whats wrong here?
    var          = var1 * var2
elif var_name == 'b':
    ds_w         = eva.load_smt_wave_all('w', 7, remove_bnds=True, it=2)
    ds_w         = ds_w.drop(['clon', 'clat'])
    var1         = ds_w.w.sel(time=slice(f'{beginn}', f'{end}'))
    ds_b         = eva.load_smt_wave_b()
    var2         = ds_b.b
    var2['time'] = var1.time
    var2['depth'] = var1.depth #whats wrong here?
    var          = var2
elif var_name == 'dbdx':
    ds_w         = eva.load_smt_wave_all('w', 7, remove_bnds=True, it=2)
    ds_w         = ds_w.drop(['clon', 'clat'])
    var1         = ds_w.w.sel(time=slice(f'{beginn}', f'{end}'))
    ds_b         = eva.load_smt_wave_b()
    var2         = ds_b.dbdx
    var2['time'] = var1.time
    var2['depth'] = var1.depth #whats wrong here?
    var          = var2
elif var_name == 'dbdy':
    ds_w         = eva.load_smt_wave_all('w', 7, remove_bnds=True, it=2)
    ds_w         = ds_w.drop(['clon', 'clat'])
    var1         = ds_w.w.sel(time=slice(f'{beginn}', f'{end}'))
    ds_b         = eva.load_smt_wave_b()
    var2         = ds_b.dbdy
    var2['time'] = var1.time
    var2['depth'] = var1.depth #whats wrong here?
    var          = var2
elif var_name == 'tke':
    ds_tke        = eva.load_smt_wave_tke_exp7_08(it=2)
    var           = ds_tke.tke
    var           = var.sel(time=slice(f'{beginn}', f'{end}'))
else:
    raise ValueError('var_name not recognized')

# slect first 10 timesteps
#  var = var.isel( depth=slice(0,80))

chunks = {'depth': 1, 'time': 1}

# %%
ts      = pyic.timing(ts, 'start calc means')

# compute_means(var, tstring, var_name, chunks, path_save_data)
# compute_means_month(var, var_name, chunks, path_save_data, ts)
compute_mean_7D(var, var_name, path_save_data)

ts     = pyic.timing(ts, 'end calc means')  
# %%
print('job finished')
cluster.close()
client.close()

# %%
# # test
# x = ds_T.isel(time=0, depth=0)
# x.to_netcdf(f'{path_save_data}{var_name}_mean_0.nc')
# # %%
