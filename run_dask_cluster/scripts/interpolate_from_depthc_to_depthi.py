# Is not working yet, however interpolation for b on depthi is done in run batch script of  compute of b and its gradients
# does not work! in batch script each time step is interpolated to a depthc and then stored - maybe 4d dataset is to large to handle for dask
import sys
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import numpy as np
import xarray as xr    
import dask as da
import pandas as pd
import netCDF4 as nc

import smt_modules.all_funcs as eva
from   smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 

from importlib import reload

def interp(var, depthc):
    var      = var.interp(depthi=depthc, method='linear')
    var      = var.rename({'depthi': 'depthc'})
    # rechunk to depthi
    # var      = var.chunk({'depthc': 1, 'time': 1})
    return var

def save_data(data, path_save_data, name):
    data.to_netcdf(f'{path_save_data}{name}.nc')

def load():
    beginn      = '2010-03-16'
    end         = '2010-03-19'
    var_name    = 'b'
    time_window = 'd'
    chunks_i    = {'depthi': 1, 'time': 1}
    ds_b        = xr.open_dataset(f'{path_save_data}{var_name}_{time_window}_mean.nc', chunks=chunks_i)
    var         = ds_b.sel(time=slice(f'{beginn}', f'{end}')).b
    return var

# %%

path_save_data = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
var = load()
var = interp(var, depthc)
save_data(var, path_save_data, 'b_1D_mean_depthc')