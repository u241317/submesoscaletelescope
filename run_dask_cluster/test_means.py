# %%
import sys
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import dask as da
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()

import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

from importlib import reload
# %%
var_name    = 'b'
chunks_c = {'depthc': 1, 'time': 1}
chunks_i = {'depthi': 1, 'time': 1}
chunks   = {'depth': 1, 'time': 1}
path_save_data = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
path_save_data = '/work/mh0033/m300878/parameterization/time_averages/month_mean/'
path_save_data = f'/work/bm1239/m300878/smt_data/tave/jul19_mean/{var_name}/'


time_window = '1M'
time_window = '7D'

path_data = f'{path_save_data}{var_name}_{time_window}_mean.nc'
# path_data = f'{path_save_data}{var_name}_{time_window}_mean.nc'

# path_data = '/work/mh0033/m300878/smt/b/b_depthi/pp_calc_b_period_2010-03-31T23:00:00.nc'
# path_data = '/work/mh0033/m300878/smt/w/pp_calc_w_2010-03-25T17:00:00.nc'
var       = xr.open_dataset(path_data, chunks=chunks)
# var       = var.rename({'cc': 'ncells'})
# %%
da = eva.load_smt_T().isel(depthc=10)
# %%
da = eva.load_smt_wave_so_exp7_08(it=2)

# %% 
da = eva.load_smt_wave_b()
# %%
path = '/work/bm1239/m300878/smt_data/tave/jul19_mean/wb/wb_7D_mean.nc'
var = xr.open_dataset(path, chunks=chunks)
var = var.rename({'__xarray_dataarray_variable__': 'wb_mean'})

#%%
beginn  = '2010-03-06-T01'
da1 = da.sel(time=beginn)
eva.plot_quick(da1, clim = np.array([13,22]))

# %%
lon_reg = [-40, 30]
lat_reg = [-55, -15]
reload(eva)
eva.plot_quick(var.b.isel( depthi=10), clim= 2e-2) #b
# eva.plot_quick_wave(var.to.isel(depth=12), clim = np.array([13,22])) #T
# eva.plot_quick_wave(da.to.isel(depth=12, time=24), clim =  np.array([13,22])) #S

# %%
clim=  np.array([34,38])
eva.plot_quick_wave(var.so.isel(depth=12), clim = clim) #T
eva.plot_quick_wave(da.so.isel(depth=12, time=24), clim = clim) #S
# %%
clim = np.array([-1e-4, 1e-4])
eva.plot_quick_wave(var.w.isel(depth=12, time=0), clim = clim) #T

# %% 
clim=np.array([-1e-2, -1e-3])
eva.plot_quick_wave(var.b.isel(time=0, depth=12), clim = clim) #T
eva.plot_quick_wave(da.b.isel(time=0, depth=12), clim = clim) #T

# %%
clim=np.array([-2e-7, 2e-7])
eva.plot_quick_wave(var.wb_mean.isel(time=0, depth=12), clim=clim) #T
# %%
path = '/work/bm1239/m300878/smt_data/tave/jul19_mean/wb/wb_7D_mean.nc'
var = xr.open_dataset(path, chunks=chunks)
wb_mean = var.rename({'__xarray_dataarray_variable__': 'wb_mean'})

path = '/work/bm1239/m300878/smt_data/tave/jul19_mean/b/b_7D_mean.nc'
b_mean = xr.open_dataset(path, chunks=chunks)

path = '/work/bm1239/m300878/smt_data/tave/jul19_mean/w/w_7D_mean.nc'
w_mean = xr.open_dataset(path, chunks=chunks)
# %%
wb_prime = wb_mean.wb_mean.isel(time=0) - w_mean.w.isel(time=0) * b_mean.b.isel(time=0)


# %%
eva.plot_quick_wave(wb_prime.isel( depth=12), clim=clim, cmap='RdBu_r') #T

# %%
clim = np.array([-1e-4, 1e-4])
eva.plot_quick_wave(w_mean.w.isel(time=0, depth=12), clim=clim, cmap='RdBu_r') #T
# %%
