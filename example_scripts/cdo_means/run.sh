#!/bin/bash
# reads template and exchanges YYYYMMDD with 20230831
cat sbatch_mean_template.sh | sed -s s/YYYYMMDD/20230831/g > run_sbatch_mean_template_20230831.sh
# now submit the job
sbatch run_sbatch_mean_template_20230831.sh