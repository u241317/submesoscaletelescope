#!/bin/bash

#SBATCH --job-name=YYYYMM_preproc
#SBATCH --partition=compute
#SBATCH --exclusive
#SBATCH --nodes=1
#SBATCH --mem=1000G
#SBATCH --time=08:00:00
#SBATCH --mail-type=FAIL
#SBATCH --account=mh0033
#SBATCH --output=output.o%j
#SBATCH --error=error.e%j

set -xe

INPUT=sst_YYYYMM
OUTPUT=sst_YYYYMM.nc

cdo -P 32 timavg [ -cat  ${INPUT}* ] ${OUTPUT} # behind cat would select every 1-10levels -sellevidx,1/10


# all files concatenated need to fit the memory of the node