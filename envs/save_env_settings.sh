#!/bin/bash

conda list > conda_list.txt
pip list > pip_list.txt
conda env export --name pyicon_py39 > environment.yml
