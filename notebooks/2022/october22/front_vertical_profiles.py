# %%
# Evaluation of single ocean fronts: here various averaged parameters are evaluated over the depth
# %%
import sys

#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import pandas as pd
import netCDF4 as nc
import xarray as xr    
from dask.diagnostics import ProgressBar
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload


# %%
#Reload modules:
reload(eva)
reload(tools)
# %%
lon_reg_R0    = [-80.5, -55]
lat_reg_R0    = [25, 40]
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/12_fronts_modified/vertical_profiles/'
path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
lon_reg       = lon_reg_R0
lat_reg       = lat_reg_R0
time_averaged = ''
# %%
#calculate MLD
path_data     = f'{path_root_dat}t{time_averaged}.nc'
t_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
path_data     = f'{path_root_dat}s{time_averaged}.nc'
s_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
data_t_mean   = pyic.interp_to_rectgrid_xr(t_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_s_mean   = pyic.interp_to_rectgrid_xr(s_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_rho_mean = gsw.rho(data_s_mean.S001_sp, data_t_mean.T001_sp, depthc[2])

mld_monte, mask_monte, mldx_monte = eva.calc_mld_montegut_xr(data_rho_mean, depthc)
mld, mask, mldx = eva.calc_mld_xr(data_rho_mean, depthc)

#calculate Ri
path_data      = f'{path_root_dat}bx{time_averaged}.nc'
dbdx_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}by{time_averaged}.nc'
dbdy_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}n2{time_averaged}.nc'
n_mean         = xr.open_dataset(path_data, chunks=dict(depthi=1))
data_dbdx_mean = pyic.interp_to_rectgrid_xr(dbdx_mean.dbdx, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_dbdy_mean = pyic.interp_to_rectgrid_xr(dbdy_mean.dbdy, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_n2_mean   = pyic.interp_to_rectgrid_xr(n_mean.N2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_M2_mean   = np.sqrt(np.power(data_dbdx_mean,2) + np.power(data_dbdy_mean,2))

ri_mean = eva.calc_richardsonnumber(data_n2_mean.lat, data_n2_mean, data_dbdy_mean)

path_data          = f'{path_root_dat}wb{time_averaged}_prime.nc'
wb_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
wb_prime_mean      = wb_prime_mean.rename(__xarray_dataarray_variable__='wb_prime_mean')
data_wb_prime_mean = pyic.interp_to_rectgrid_xr(wb_prime_mean.wb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week


path_data          = f'{path_root_dat}vb{time_averaged}_prime.nc'
vb_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthc=1))
vb_prime_mean      = vb_prime_mean.rename(__xarray_dataarray_variable__='vb_prime_mean')
data_vb_prime_mean = pyic.interp_to_rectgrid_xr(vb_prime_mean.vb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

data_vb_prime_mean_depthi = data_vb_prime_mean.interp(depthc=depthi)
data_vb_prime_mean_depthi = data_vb_prime_mean_depthi.rename(depthc='depthi')

# %%
path_data          = f'{path_root_dat}ub{time_averaged}_prime.nc'
ub_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthc=1))
ub_prime_mean      = ub_prime_mean.rename(__xarray_dataarray_variable__='ub_prime_mean')
data_ub_prime_mean = pyic.interp_to_rectgrid_xr(ub_prime_mean.ub_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

data_ub_prime_mean_depthi = data_ub_prime_mean.interp(depthc=depthi)
data_ub_prime_mean_depthi = data_ub_prime_mean_depthi.rename(depthc='depthi')

# %%
argo = eva.load_argo_climatology()

# %%
def plot_front_profiles(rho, n2, m2x, m2y, ri, ub, vb, wb, mldx, mldx_monte, argo_mld, fig_path, front, dim):
    print('MLD', mldx.mean().data)
    # m2y_masked = m2y.mean(dim=dim).data
    psi_held        = eva.calc_streamfunc_held_schneider(wb.mean(dim=dim).data, m2x.mean(dim=dim).data, m2y.mean(dim=dim).data)
    grad_b          = eva.calc_abs_grad_b(m2x=m2x.mean(dim=dim).data, m2y= m2y.mean(dim=dim).data, n2=n2.mean(dim=dim).data)
    psi_full        = eva.calc_streamfunc_full(wb.mean(dim=dim).data, m2y.mean(dim=dim).data, vb.mean(dim=dim).data, n2.mean(dim=dim).data, grad_b)
    psi_traditional = eva.calc_streamfunc(vb.mean(dim=dim).data,n2.mean(dim=dim).data)

    fig, ax = plt.subplots(1,6, figsize=(20,7), sharey=True, squeeze=True) #16,7 for 5
    fig.suptitle(rf'{front} averaged variables', fontsize=25)
    ylim = np.array([1200, 0])
    lw   = 2
    fs   = 30
    fl   = 15


    i=0
    ax[i].plot(rho.squeeze(), depthc, color='lightgrey', lw=lw)
    ax[i].plot(rho.mean(dim=dim).data, depthc, 'black', lw=lw)
    ax[i].axhline(mldx.mean(), color='r', ls=':', lw=lw, label=f'MLD rho 0.2 (week) {int(mldx.mean().data)}m')
    ax[i].axhline(mldx_monte.mean(), color='purple', ls=':', lw=lw, label=f'MLD rho 0.03 (week) {int(mldx_monte.mean().data)}m')
    ax[i].axhline(argo_mld, color='tab:green', ls=':', lw=lw, label=f'MLD Argo (month) {int(argo_mld)}m')
    ax[i].set_ylim(ylim)  # type: ignore
    R = rho.mean(dim=dim).isel(depthc=slice(0,98))
    ax[i].set_xlim(R.min()-0.1,R.max())
    ax[i].set_xlabel(fr'$\rho$ (ref {depthc[2]}m)', fontsize=fs)
    ax[i].set_ylabel('depth [m]', fontsize=fs)
    ax[i].legend(loc='lower left', fontsize=11)
    ax[i].grid()
    ax[i].set_xticks(ax[i].get_xticks()[::2])
    ax[i].tick_params(labelsize=fl)


    i=1
    ax[i].plot(n2.squeeze(), depthi, color='lightgrey', lw=lw)
    ax[i].plot(n2.mean(dim=dim).data, depthi, 'black', lw=lw)
    ax[i].axhline(mldx.mean(), color='r', ls=':', lw=lw)
    ax[i].axhline(mldx_monte.mean(), color='purple', ls=':', lw=lw)
    ax[i].axhline(argo_mld, color='tab:green', ls=':', lw=lw)
    ax[i].set_ylim(ylim) # type: ignore
    # ax[i].set_xlim(-1e-5,4e-5)
    ax[i].set_xlim(0,2e-5)
    ax[i].set_xlabel('$N^2$ ', fontsize=fs)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)


    i=2
    ax[i].plot(m2y.squeeze(), depthi, color='lightgrey', lw=lw)
    ax[i].plot(m2x.mean(dim=dim).data, depthi, ls='dashed', color='black', lw=lw, label='$d_x b$')
    ax[i].plot(m2y.mean(dim=dim).data, depthi, 'black', lw=lw, label='$d_y b$')
    ax[i].axhline(mldx.mean(), color='r', ls=':', lw=lw)
    ax[i].axhline(mldx_monte.mean(), color='purple', ls=':', lw=lw)
    ax[i].axhline(argo_mld, color='tab:green', ls=':', lw=lw)
    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(-7e-8,3e-8)
    ax[i].set_xlabel('$M^2$ ', fontsize=fs)
    ax[i].legend(loc='lower left', fontsize=15)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)


    i=3
    ax[i].plot(vb.squeeze(), depthi, color='lightgrey',lw=lw)
    ax[i].plot(ub.mean(dim=dim).data, depthi, color='black', lw=lw, ls='dashed', label = r"$\overline{u'b'}$")
    ax[i].plot(vb.mean(dim=dim).data, depthi, color='black', lw=lw, label = r"$\overline{v'b'}$")
    ax[i].axhline(mldx.mean(), color='r', ls=':', lw=lw)
    ax[i].axhline(mldx_monte.mean(), color='purple', ls=':', lw=lw)
    ax[i].axhline(argo_mld, color='tab:green', ls=':', lw=lw)
    ax[i].set_ylim(ylim) # type: ignore
    #ax[i].set_xlim(-0.5e-7,2e-7)
    # ax[i].set_xlabel(r"$\overline{v'b'} & \overline{u'b'} $ ", fontsize=fs)
    ax[i].legend(loc='lower right', fontsize=15)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)


    i=4
    ax[i].plot(wb.squeeze(), depthi, color='lightgrey',lw=lw)
    ax[i].plot(wb.mean(dim=dim).data, depthi, 'black', lw=lw)
    ax[i].axhline(mldx.mean(), color='r', ls=':', lw=lw)
    ax[i].axhline(mldx_monte.mean(), color='purple', ls=':', lw=lw)
    ax[i].axhline(argo_mld, color='tab:green', ls=':', lw=lw)
    ax[i].set_ylim(ylim) # type: ignore
    # ax[i].set_xlim(-0.5e-7,2e-7)
    ax[i].set_xlabel(r"$\overline{w'b'}$ ", fontsize=fs)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)


    i=5
    # ax[i].plot(psi.squeeze(), depthc, color='lightgrey')
    ax[i].plot(psi_held, depthi, 'black', lw=lw, ls='dashed', label=r"$ \psi = - \frac{\overline{w'b'} * d_y b }{ (|d_x b|+|d_y b|)^2 }$")
    ax[i].plot(psi_full, depthi, 'black', lw=lw,  label=r"$ \psi = \frac{\overline{v'b'}  N^2 - \overline{w'b'}  d_y b}{ |\nabla b|^2 }$")
    ax[i].plot(psi_traditional, depthi, 'black', ls=':', lw=lw, label=r"$ \psi = v'b' * N^2 / |N^2|^2 $")
    ax[i].legend(loc='lower right', fontsize=12)
    ax[i].axhline(mldx.mean(), color='r', ls=':', lw=lw)
    ax[i].axhline(mldx_monte.mean(), color='purple', ls=':', lw=lw)
    ax[i].axhline(argo_mld, color='tab:green', ls=':', lw=lw)
    ax[i].set_ylim(ylim) # type: ignore
    # ax[i].set_xlim(-1,5)
    ax[i].set_xlabel(r"$ \psi $", fontsize=fs)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)

    plt.savefig(f'{fig_path}front_{front}_averaged_sign_sensitive_oct22.png', dpi=150, format='png', bbox_inches='tight')

# %%
def eval_front(lon_front, lat_front, front):
    lat_mean = eva.mean_help(lat_front)
    lon_mean = eva.mean_help(lon_front)
    argo_mld = argo.mld_da_mean.isel(iLAT=int(lat_mean), iLON=int(lon_mean), iMONTH=3)

    # data_dbdy_masked = data_dbdy_mean.where((data_dbdy_mean < -1e-9), np.nan) #to have clean overturning for fox kemper streamfunction

    # m2_filter = 0
    # data_dbdy_masked = data_dbdy_mean.where(np.abs(data_dbdy_mean) > m2_filter, np.nan) #alternative threshold


    wb_fselect, ub_fselect, vb_fselect, ri_fselect, mldx_fselect, mldx_monte_fselect, m2x_fselect, m2y_fselect, alpha_fselect, lhs_fselect, rho_fselect, n2_fselect = eva.calc_front_varaibles_2(
        data_wb_prime_mean, data_ub_prime_mean_depthi, data_vb_prime_mean_depthi, ri_mean, mldx, mldx_monte, data_dbdx_mean, data_dbdy_mean, 
        data_rho_mean, data_n2_mean, lon_front, lat_front, dim='lon', ave=True)

    plot_front_profiles(rho_fselect, n2_fselect, m2x_fselect, m2y_fselect, ri_fselect, ub_fselect, vb_fselect,
                        wb_fselect, mldx_fselect, mldx_monte_fselect, argo_mld, fig_path, front=f'Front_{front}', dim='lat')


# %%
reload(eva)
lon_reg_all, lat_reg_all = eva.get_new_fronts()
for ii in np.arange(12):
    # ii=11
    front =             f'R{ii+1}f'
    lon_front = lon_reg_all[ii,:]
    lat_front = lat_reg_all[ii,:]

    eval_front(lon_front , lat_front, front)

print('all done front vertical profiles')

# %%
