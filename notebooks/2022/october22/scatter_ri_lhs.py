# evaluate section of submesoscale ocean front
# method: masking MLD, averaging over MLD, 
# removing zeros and infs, taking log10 of absolute values,
#  scatterplot, linear regression to calculate slope
# %%
import sys

from fsspec import register_implementation

#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import pandas as pd
import netCDF4 as nc
import xarray as xr    
from dask.diagnostics import ProgressBar
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload
from scipy import stats    #Used for 2D binned statistics

from matplotlib.ticker import FormatStrFormatter
# %%
def calc_log10(x_data, y_data):
    mask   = ~np.isnan(x_data) & ~np.isnan(y_data) #remove nans from original data
    x_data = x_data[mask]
    y_data = y_data[mask]

    # mask   = np.ma.masked_where((x_data >= 0), x_data) # remove zeros and negative numbers from xdata
    # x_data = x_data[mask.mask]
    # y_data = y_data[mask.mask]

    logx = np.log10(np.abs(x_data)) #not nessecary
    logy = np.log10(np.abs(y_data))
    # mask = ~np.isnan(logx) & ~np.isnan(logy) & ~np.isinf(logy) #remove inf numbers and nans again after taking log
    # logx = logx[mask]
    # logy = logy[mask]

    return(logx,logy)

# %%
def plot_slope(logx, logy, figname):
    fig, ax = plt.subplots(figsize = (5, 5))
    plt.plot(logx, logy , 'ro', markersize=0.5)

    res = stats.linregress(logx, logy)
    plt.plot(logx, res.intercept + res.slope*logx, 'b', label=f'slope={res.slope:.2f}')  # type: ignore

    ax.set_xlabel(r'$log_{10} \overline{Ri}$', fontsize=30)
    ax.set_ylabel(r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $', fontsize=30)
    ax.set_title(f'{figname}')
    plt.grid()
    plt.legend(title=f'intercept = {res.intercept:.1}')  # type: ignore
    # plt.autoscale(enable=False, axis='y', tight=True)
    ax.axis('equal')
    print(res)

# %%
def plot_slopes(logx_vb, logy_vb, logx_wb, logy_wb, logx_psi, logy_psi, logx_K, logy_K, m2_lim, figname, savefig):
    hca, hcb = pyic.arrange_axes(4, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    fs = 10
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_vb
    logy = logy_vb
    ax.scatter(logx, logy , color='lightgrey', s=0.5)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'black', label=f'slope={res.slope:.2f}')  # type: ignore
    ax.set_xlabel(r'$log_{10} \overline{Ri}$', fontsize=fs)
    ax.set_title(r'$log_{10} |\overline{v^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^3) $', fontsize=fs)
    ax.legend(title=f'intercept = {res.intercept:.1}')  # type: ignore

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_wb
    logy = logy_wb
    ax.scatter(logx, logy , color='lightgrey', s=0.5)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'black', label=f'slope={res.slope:.2f}')  # type: ignore
    ax.set_xlabel(r'$log_{10} \overline{Ri}$', fontsize=fs)
    ax.set_title(r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $', fontsize=fs)
    ax.legend(title=f'intercept = {res.intercept:.1}')  # type: ignore

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_psi
    logy = logy_psi
    ax.scatter(logx, logy , color='lightgrey', s=0.5)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'black', label=f'slope={res.slope:.2f}')  # type: ignore
    ax.set_xlabel(r'$log_{10} \overline{Ri}$', fontsize=fs)
    ax.set_title(r'$log_{10} |\psi|/(H^2 f \alpha) $', fontsize=fs)
    ax.legend(title=f'intercept = {res.intercept:.1}')  # type: ignore

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_K
    logy = logy_K
    ax.scatter(logx, logy , color='lightgrey', s=0.5)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'black', label=f'slope={res.slope:.2f}')  # type: ignore
    ax.set_xlabel(r'$log_{10} \overline{Ri}$', fontsize=fs)
    ax.set_title(r'$log_{10} |K_{dia}|/(H^2 f) $', fontsize=fs)
    ax.legend(title=f'intercept = {res.intercept:.1}')  # type: ignore
    # ax.text(0.95, 0.01, f'|M2| > {m2_lim}',
    ax.text(0.95, 0.01, f'Ri < {m2_lim}',
        verticalalignment='bottom', horizontalalignment='right',
        transform=ax.transAxes,
        color='black', fontsize=fs)

    for ax in hca:
        pyic.plot_settings(ax)   # type: ignore
        ax.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
        ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
        ax.axis('equal')
        ax.grid()
        # ax.tick_params(labelsize=15)

    # ax.set_title(f'{figname}')
    # plt.autoscale(enable=False, axis='y', tight=True)
    if savefig == True: plt.savefig(f'{fig_path}{figname}_scatter_ri_{m2_lim}.png', dpi=150, format='png', bbox_inches='tight')


# %%
#Reload modules:
reload(eva)
reload(tools)
# %%
lon_reg_R0    = [-80.5, -55]
lat_reg_R0    = [25, 40]
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/12_fronts_modified/scatter/2nd/'
path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
lon_reg       = lon_reg_R0
lat_reg       = lat_reg_R0
# %%
time_averaged = ''

#calculate MLD
path_data            = f'{path_root_dat}t{time_averaged}.nc'
t_mean               = xr.open_dataset(path_data, chunks=dict(depthc=1))
path_data            = f'{path_root_dat}s{time_averaged}.nc'
s_mean               = xr.open_dataset(path_data, chunks=dict(depthc=1))
data_t_mean          = pyic.interp_to_rectgrid_xr(t_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_s_mean          = pyic.interp_to_rectgrid_xr(s_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_rho_mean        = gsw.rho(data_s_mean.S001_sp, data_t_mean.T001_sp, depthc[2])
data_rho_mean_depthi = data_rho_mean.interp(depthc=depthi)
# data_rho_mean_depthi = data_rho_mean_depthi.rename(depthc='depthi')

mld_monte, mask_monte, mldx_monte = eva.calc_mld_montegut_xr(data_rho_mean_depthi, depthi)
mask_monte = mask_monte.rename(depthc='depthi')

mld, mask, mldx = eva.calc_mld_xr(data_rho_mean_depthi, depthi)
mask      = mask.rename(depthc='depthi')
a         = data_t_mean.T001_sp.isel(depthc=0).where(~(data_t_mean.T001_sp.isel(depthc=0)==0), np.nan)
mask_land = ~np.isnan(a)
mask_land = mask_land.where(mask_land==True, np.nan)
mldx      = mldx * mask_land


# %% load and interpolate data
path_data      = f'{path_root_dat}bx{time_averaged}.nc'
dbdx_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}by{time_averaged}.nc'
dbdy_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}n2{time_averaged}.nc'
n_mean         = xr.open_dataset(path_data, chunks=dict(depthi=1))
data_dbdx_mean = pyic.interp_to_rectgrid_xr(dbdx_mean.dbdx, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_dbdy_mean = pyic.interp_to_rectgrid_xr(dbdy_mean.dbdy, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_n2_mean   = pyic.interp_to_rectgrid_xr(n_mean.N2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# data_M2_mean   = np.sqrt(np.power(data_dbdx_mean,2) + np.power(data_dbdy_mean,2))

# ri_mean        = eva.calc_richardsonnumber(data_n2_mean.lat, data_n2_mean, data_dbdy_mean)
# ri_mean        = eva.calc_richardsonnumber(data_n2_mean.lat, data_n2_mean, data_M2_mean)


path_data          = f'{path_root_dat}wb{time_averaged}_prime.nc'
wb_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
wb_prime_mean      = wb_prime_mean.rename(__xarray_dataarray_variable__='wb_prime_mean')
data_wb_prime_mean = pyic.interp_to_rectgrid_xr(wb_prime_mean.wb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week


path_data          = f'{path_root_dat}vb{time_averaged}_prime.nc'
vb_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthc=1))
vb_prime_mean      = vb_prime_mean.rename(__xarray_dataarray_variable__='vb_prime_mean')
data_vb_prime_mean = pyic.interp_to_rectgrid_xr(vb_prime_mean.vb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

data_vb_prime_mean_depthi = data_vb_prime_mean.interp(depthc=depthi)
data_vb_prime_mean_depthi = data_vb_prime_mean_depthi.rename(depthc='depthi')

data_M2_mean   = np.sqrt(np.power(data_dbdx_mean,2) + np.power(data_dbdy_mean,2))

# %%
mask_used = mask
mldx_used = mldx
n2_sel    = data_n2_mean

m2_sel  = data_M2_mean
m2x_sel = data_dbdx_mean
m2y_sel = data_dbdy_mean

# %%
m2_min_lim = 0#2e-8#3e-8
# m2
# m2 = m2.where((np.abs(m2) > 2e-8) & (np.abs(m2) < 5e-8), np.nan) #threshold
m2      = m2_sel.where(np.abs(m2_sel) > m2_min_lim, np.nan)
m2      = m2.where(~np.isinf(m2),np.nan) #infs to nans
m2_mask = m2 * mask_used
m2_mask = m2_mask.where(~(m2_mask == 0), np.nan) # remove zeros

m2x      = m2x_sel.where(np.abs(m2x_sel) > m2_min_lim, np.nan)
m2x      = m2x.where(~np.isinf(m2x),np.nan) #infs to nans
m2x_mask = m2x * mask_used
m2x_mask = m2x_mask.where(~(m2x_mask == 0), np.nan) # remove zeros

m2y      = m2y_sel.where(np.abs(m2y_sel) > m2_min_lim, np.nan)
m2y      = m2y.where(~np.isinf(m2y),np.nan) #infs to nans
m2y_mask = m2y * mask_used
m2y_mask = m2y_mask.where(~(m2y_mask == 0), np.nan) # remove zeros

# n2
n2      = n2_sel.where(~np.isinf(n2_sel),np.nan) #infs to nans
n2_mask = n2 * mask_used #application of mask leads to new zero values
n2_mask = n2_mask.where(~(n2_mask == 0), np.nan) # remove zeros
# n2_mask = n2_mask.where(np.abs(n2) < 1e-5, np.nan)

wb_mask = data_wb_prime_mean * mask_used
wb_mask = wb_mask.where(~(m2_mask == 0), np.nan) # remove zeros
vb_mask = data_vb_prime_mean_depthi * mask_used
vb_mask = vb_mask.where(~(m2_mask == 0), np.nan) # remove zeros

# %%
############################################################
# no more filters from here

ri       = eva.calc_richardsonnumber(n2_mask.lat, n2_mask, m2_mask )

# ri       = ri.where(ri > 1000, np.nan)
ri_dmean = ri.mean(dim='depthi', skipna=True)

f     = eva.calc_coriolis_parameter(m2_mask.lat)
alpha = eva.calc_alpha(m2_mask, f)

lhs_wb       = eva.calc_lhs_wb(wb_mask, mldx_used, f, alpha)
lhs_wb_dmean = lhs_wb.mean(dim='depthi', skipna=True)
lhs_vb       = eva.calc_lhs_vb(vb_mask, mldx_used, f, alpha)
lhs_vb_dmean = lhs_vb.mean(dim='depthi', skipna=True)

grad_b        = eva.calc_abs_grad_b(m2x_mask, m2y_mask, n2_mask)
psi_held      = eva.calc_streamfunc_full(wb_mask, m2y_mask, vb_mask, n2_mask, grad_b)
lhs_psi       = eva.calc_lhs_psi(psi_held, mldx_used, f, alpha)
lhs_psi_dmean = lhs_psi.mean(dim='depthi', skipna=True)

K           = eva.calc_diapycnal_diffusivity(vb_mask, m2_mask, wb_mask, n2_mask, grad_b)
lhs_K       = eva.calc_lhs_diapycnal_diffusivity(K, mldx_used, f)
lhs_K_dmean = lhs_K.mean(dim='depthi', skipna=True)
 

# %%
for i in np.arange(1):
    lon_reg_all, lat_reg_all = eva.get_new_fronts()
    i = 0
    R       = f'F{i+1}'
    figname = f'{R}'
    lon_reg = lon_reg_all[i,:]
    lat_reg = lat_reg_all[i,:]

    # lon_reg = lon_reg_R0
    # lat_reg = lat_reg_R0
    # lon_reg       =-71,-56
    # lat_reg       =25,38

    ri_dat     = ri_dmean.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))
    lhs_wb_dat = lhs_wb_dmean.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))

    lhs_vb_dat  = lhs_vb_dmean.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))
    lhs_psi_dat = lhs_psi_dmean.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))
    lhs_K_dat   = lhs_K_dmean.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))


    laty, lonx   = ri_dat.shape
    # logx, logy   = calc_log10(np.reshape(ri_dat.data, laty*lonx), np.reshape(lhs_wb_dat.data, laty*lonx))
    # plot_slope(logx, logy, figname)
    lim = 1e10 # no effect    

    logx_vb, logy_vb   = calc_log10(np.reshape(ri_dat.where(ri_dmean < lim).data, laty*lonx),  np.reshape(lhs_vb_dat.where(ri_dmean < lim).data, laty*lonx))
    logx_wb, logy_wb   = calc_log10(np.reshape(ri_dat.where(ri_dmean < lim).data, laty*lonx),  np.reshape(lhs_wb_dat.where(ri_dmean < lim).data, laty*lonx))
    logx_psi, logy_psi = calc_log10(np.reshape(ri_dat.where(ri_dmean < lim).data, laty*lonx), np.reshape(lhs_psi_dat.where(ri_dmean < lim).data, laty*lonx))
    logx_K, logy_K     = calc_log10(np.reshape(ri_dat.where(ri_dmean < lim).data, laty*lonx),   np.reshape(lhs_K_dat.where(ri_dmean < lim).data, laty*lonx))

    plot_slopes(logx_vb, logy_vb, logx_wb, logy_wb, logx_psi, logy_psi, logx_K, logy_K, m2_min_lim, figname, savefig=True)

# %%
for i in np.arange(10):
    lon_reg_all, lat_reg_all = eva.get_new_fronts()
    # i = 0
    R       = f'F{i+1}'
    figname = f'{R}'
    lon_reg = lon_reg_all[i,:]
    lat_reg = lat_reg_all[i,:]

    # lon_reg = lon_reg_R0
    # lat_reg = lat_reg_R0
    # lon_reg       =-71,-56
    # lat_reg       =25,38

    # ri_dat      = ri.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))
    n2_dat = n2_mask.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))
    n2_dat = n2_dat.mean(dim='lon').mean(dim='lat')    
    m2_dat = m2y_mask.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))
    m2_dat = m2_dat.mean(dim='lon').mean(dim='lat') 
    ri_dat = eva.calc_richardsonnumber(n2_mask.lat.mean(), n2_dat, m2_dat)  #calc. of Ri should be done as late as possbile, otherwise one obtains extreme large values 

    lhs_wb_dat  = lhs_wb.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))
    lhs_vb_dat  = lhs_vb.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))
    lhs_psi_dat = lhs_psi.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))
    lhs_K_dat   = lhs_K.sel(lat=slice(lat_reg[0],lat_reg[1])).sel(lon=slice(lon_reg[0],lon_reg[1]))

    # ri_dat      = ri_dat.mean(dim='lon').mean(dim='lat')     
    lhs_wb_dat  = lhs_wb_dat.mean(dim='lon').mean(dim='lat')
    lhs_vb_dat  = lhs_vb_dat.mean(dim='lon').mean(dim='lat')
    lhs_psi_dat = lhs_psi_dat.mean(dim='lon').mean(dim='lat')
    lhs_K_dat   = lhs_K_dat.mean(dim='lon').mean(dim='lat')

    lim = 2e2  
    x = ri_dat.where(ri_dat < lim).data
    logx_vb, logy_vb   = calc_log10(x, lhs_vb_dat.where(ri_dat < lim).data)
    logx_wb, logy_wb   = calc_log10(x, lhs_wb_dat.where(ri_dat < lim).data)
    logx_psi, logy_psi = calc_log10(x, lhs_psi_dat.where(ri_dat < lim).data)
    logx_K, logy_K     = calc_log10(x, lhs_K_dat.where(ri_dat < lim).data)

    plot_slopes(logx_vb, logy_vb, logx_wb, logy_wb, logx_psi, logy_psi, logx_K, logy_K, lim, figname, savefig=True)


# %%
print('all done front stream fluctuation')

# %%
