# compare streamfunction traditional, fox kemper and full held schneider formulation
# %% 
import sys

#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import pandas as pd
import netCDF4 as nc
import xarray as xr    
from dask.diagnostics import ProgressBar
import numpy as np

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload

# %%
grain = True
#Reload modules:
reload(eva)
reload(tools)
# %%
lon_reg_R0    = np.array([-80.5, -55])
lat_reg_R0    = np.array([25, 40])
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
# fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/12_fronts_modified/streamfunction/res001_ref/'
# fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/eddies/streamfunc/'
fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/12_fronts_modified/coarse_grained/streamfunction/'

path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
lon_reg       = lon_reg_R0
lat_reg       = lat_reg_R0

# %%
fpath_ckdtree_r01 = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.10_180W-180E_90S-90N.nc'
grid01            = xr.open_dataset(fpath_ckdtree_r01)
grid01_sel        = grid01.sel(lon=slice(lon_reg_R0[0], lon_reg_R0[1]), lat=slice(lat_reg_R0[0],lat_reg_R0[1]))

def coarse_graining(data):
    data_interp = data.interp_like(grid01_sel, method='linear', assume_sorted=False, kwargs=None)
    return data_interp
# %%
time_averaged = ''

#calculate MLD
path_data     = f'{path_root_dat}t{time_averaged}.nc'
t_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
path_data     = f'{path_root_dat}s{time_averaged}.nc'
s_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
data_t_mean   = pyic.interp_to_rectgrid_xr(t_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_s_mean   = pyic.interp_to_rectgrid_xr(s_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_rho_mean = gsw.rho(data_s_mean.S001_sp, data_t_mean.T001_sp, depthc[2])

if grain == True: data_rho_mean = coarse_graining(data_rho_mean)


mld_monte, mask_monte, mldx_monte = eva.calc_mld_montegut_xr(data_rho_mean, depthc)
mld, mask, mldx = eva.calc_mld_xr(data_rho_mean, depthc)

#calculate Ri
path_data      = f'{path_root_dat}bx{time_averaged}.nc'
dbdx_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}by{time_averaged}.nc'
dbdy_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}n2{time_averaged}.nc'
n_mean         = xr.open_dataset(path_data, chunks=dict(depthi=1))
data_dbdx_mean = pyic.interp_to_rectgrid_xr(dbdx_mean.dbdx, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_dbdy_mean = pyic.interp_to_rectgrid_xr(dbdy_mean.dbdy, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_n2_mean   = pyic.interp_to_rectgrid_xr(n_mean.N2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# data_M2_mean   = np.sqrt(np.power(data_dbdx_mean,2) + np.power(data_dbdy_mean,2))

ri_mean = eva.calc_richardsonnumber(data_n2_mean.lat, data_n2_mean, data_dbdy_mean)

path_data          = f'{path_root_dat}wb{time_averaged}_prime.nc'
wb_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
wb_prime_mean      = wb_prime_mean.rename(__xarray_dataarray_variable__='wb_prime_mean')
data_wb_prime_mean = pyic.interp_to_rectgrid_xr(wb_prime_mean.wb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week


path_data          = f'{path_root_dat}vb{time_averaged}_prime.nc'
vb_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthc=1))
vb_prime_mean      = vb_prime_mean.rename(__xarray_dataarray_variable__='vb_prime_mean')
data_vb_prime_mean = pyic.interp_to_rectgrid_xr(vb_prime_mean.vb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

data_vb_prime_mean_depthi = data_vb_prime_mean.interp(depthc=depthi)
data_vb_prime_mean_depthi = data_vb_prime_mean_depthi.rename(depthc='depthi')

path_data          = f'{path_root_dat}ub{time_averaged}_prime.nc'
ub_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthc=1))
ub_prime_mean      = ub_prime_mean.rename(__xarray_dataarray_variable__='ub_prime_mean')
data_ub_prime_mean = pyic.interp_to_rectgrid_xr(ub_prime_mean.ub_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

data_ub_prime_mean_depthi = data_ub_prime_mean.interp(depthc=depthi)
data_ub_prime_mean_depthi = data_ub_prime_mean_depthi.rename(depthc='depthi')


# %% Coarse graining >> interpolate to coarser grid: horizontal smoothing
if grain == True: 
    data_wb_prime_mean        = coarse_graining(data_wb_prime_mean)
    data_ub_prime_mean_depthi = coarse_graining(data_ub_prime_mean_depthi)
    data_vb_prime_mean_depthi = coarse_graining(data_vb_prime_mean_depthi)
    data_n2_mean              = coarse_graining(data_n2_mean)
    data_dbdx_mean            = coarse_graining(data_dbdx_mean)
    data_dbdy_mean            = coarse_graining(data_dbdy_mean)

# %%
argo = eva.load_argo_climatology()
# %%
def plot_slice_front(x, y, lhs, alpha, wb, vb, ri, mldx, mldx_monte, argo_mld, rho, m2x, m2y, n2, dl, ylim=[5000,0], title=None, fig_path=None, savefig=False):
    """plot vertical parameters of front"""
    fig, ax = plt.subplots(6, 1, figsize=(20,31), sharey=True)
    lw      = 4
    color   = 'r'
    ls      = 'dashed'
    fs      = 25
    depth_s = 83

    # psi             = eva.calc_streamfunc_held_schneider(wb, -np.abs(m2y_masked))
    psi             = eva.calc_streamfunc_held_schneider(wb, m2x, m2y)
    psi             = psi.isel(depthi=slice(0,depth_s))
    grad_b          = eva.calc_abs_grad_b(m2x, m2y, n2)
    psi_full        = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)
    psi_traditional = eva.calc_streamfunc(vb.data, n2)

    i=0
    scale  = 0.7
    data_p = alpha.isel(depthi=slice(0,depth_s))
    data   = data_p.where((data_p.lat > data_p.lat.min() + dl) & (data_p.lat < data_p.lat.max() - dl)).data
    cmax   = np.nanmax(data)
    cmin   = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), data_p.data, levels, cmap='PuOr_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls)
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw)
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r'$\alpha = db/dy ~ /f^2$',fontsize=fs)

    i = 1
    scale   = 0.7
    psi_lim = psi.where((psi.lat > psi.lat.min() + dl) & (psi.lat < psi.lat.max() - dl))
    data    = psi_lim.data
    cmax    = np.nanmax(data)
    cmin    = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), psi.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \psi = - \overline{w'b'} * d_y b / (|d_x b|+|d_y b|)^2 $",fontsize=fs)


    i = 2
    scale = 0.7
    data  = psi_full.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, psi_full.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \psi = (v'b'  N^2 - w'b'  d_y b) / |\nabla b|^2 $",fontsize=fs)


    i = 3
    scale = 0.7
    data  = psi_traditional.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, psi_traditional.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \psi = v'b' * N^2 / |N^2|^2 $", fontsize=fs)
 

    i = 4
    scale = 0.7
    data  = vb.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, vb.data, levels,  cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title("$ v'b' $",fontsize=fs)
 

    i = 5
    scale = 0.7
    data  = wb.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, wb.data, levels,  cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title("$  w'b' $",fontsize=fs)
    ax[i].set_xlabel(f'{title}', fontsize=fs)
   

    if savefig == True: plt.savefig(f'{fig_path}', dpi=150, format='png', bbox_inches='tight')

# %%
def plot_slice_front_psi_held(x, y, lhs, alpha, wb, vb, ri, mldx, mldx_monte, argo_mld, rho, m2x, m2y, n2, dl, ylim=[5000,0], title=None, fig_path=None, savefig=False):
    """plot vertical parameters of front"""
    fig, ax = plt.subplots(3, 1, figsize=(20,15), sharex=True)
    lw      = 4
    color   = 'r'
    ls      = 'dashed'
    fs      = 25
    fl      = 15
    depth_s = 83

    # psi             = eva.calc_streamfunc_held_schneider(wb, -np.abs(m2y_masked))
    psi_held_diag = eva.calc_streamfunc_held_schneider(wb, m2x, m2y)
    psi_held_diag = psi_held_diag.isel(depthi=slice(0,depth_s))
    grad_b        = eva.calc_abs_grad_b(m2x, m2y, n2)
    psi_full_diag = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)
    # psi_traditional = eva.calc_streamfunc(vb.data, n2)

    cf                 = 0.06
    cs                 = 0.53

    lat_mean           = n2.lat.mean()
    ri_diag            = eva.calc_richardsonnumber(lat_mean, n2, m2y)
    wb_fox_param       = eva.calc_wb_fox_param(lat_mean, mldx, m2y, -m2y.depthi, cf, structure=True )
    vb_fox_param       = eva.calc_vb_fox_param(lat_mean, mldx, m2y, ri_diag, -m2y.depthi, cf, structure=True )
    psi_fox_full_param = eva.calc_streamfunc_full(wb_fox_param, m2y, vb_fox_param, n2, grad_b)
    psi_fox_held_param = eva.calc_streamfunc_held_schneider(wb_fox_param, m2x, m2y)

    wb_stone_param = eva.calc_wb_stone_param(lat_mean, mldx, m2y, ri_diag, cs, -m2y.depthi)
    vb_stone_param = eva.calc_vb_stone_param(lat_mean, mldx, m2y, ri_diag, cs)

    psi_stone_held_param = eva.calc_streamfunc_held_schneider(wb_stone_param, m2x,  m2y)
    psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param, m2y, vb_stone_param, n2, grad_b)

    i = 0
    scale   = 0.7
    psi_lim = psi_held_diag.where((psi_held_diag.lat > psi_held_diag.lat.min() + dl) & (psi_held_diag.lat < psi_held_diag.lat.max() - dl))
    data    = psi_lim.data
    cmax    = np.nanmax(data)
    cmin    = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), psi_held_diag.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \psi = - \overline{w'b'} * d_y b / (|d_x b|+|d_y b|)^2 $ diagonised",fontsize=fs)
    ax[i].tick_params(labelsize=fl)

    i = 1
    scale = 0.7
    data  = psi_fox_held_param.isel(depthi=slice(0,depth_s)).data#psi_held.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, psi_fox_held_param.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \psi $ Fox-Kemper Param.",fontsize=fs)
    ax[i].tick_params(labelsize=fl)

    i = 2
    scale = 0.7
    data  = psi_stone_held_param.isel(depthi=slice(0,depth_s)).data#psi_held.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, psi_stone_held_param.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \psi $ Stone Param.",fontsize=fs)
    ax[i].set_xlabel(f'{title}', fontsize=fs)
    ax[i].tick_params(labelsize=fl)

    if savefig == True: plt.savefig(f'{fig_path}', dpi=150, format='png', bbox_inches='tight')


# %%
def plot_slice_front_psi_full(x, y, lhs, alpha, wb, vb, ri, mldx, mldx_monte, argo_mld, rho, m2x, m2y, n2, dl, ylim=[5000,0], title=None, fig_path=None, savefig=False):
    """plot vertical parameters of front"""
    fig, ax = plt.subplots(3, 1, figsize=(20,15), sharex=True)
    lw      = 4
    color   = 'r'
    ls      = 'dashed'
    fs      = 25
    fl      = 15
    # depth_s = 83
    depth_s  = (np.abs(depthi - ylim[0])).argmin()


    # psi             = eva.calc_streamfunc_held_schneider(wb, -np.abs(m2y_masked))
    psi_held_diag = eva.calc_streamfunc_held_schneider(wb, m2x, m2y)
    psi_held_diag = psi_held_diag.isel(depthi=slice(0,depth_s))
    grad_b        = eva.calc_abs_grad_b(m2x, m2y, n2)
    psi_full_diag = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)
    # psi_traditional = eva.calc_streamfunc(vb.data, n2)

    # cf                 = 0.06
    # cs                 = 0.53
    cf                 = 0.038181818181818185
    cs                 = 0.22222222222222224
    if grain == True:
        cf = 0.04272727272727273
        cs = 0.2828282828282829

    lat_mean           = n2.lat.mean()
    ri_diag            = eva.calc_richardsonnumber(lat_mean, n2, m2y)
    wb_fox_param       = eva.calc_wb_fox_param(lat_mean, mldx, m2y, -m2y.depthi, cf, structure=True )
    vb_fox_param       = eva.calc_vb_fox_param(lat_mean, mldx, m2y, ri_diag, -m2y.depthi, cf, structure=True )
    psi_fox_full_param = eva.calc_streamfunc_full(wb_fox_param, m2y, vb_fox_param, n2, grad_b)
    # psi_fox_held_param = eva.calc_streamfunc_held_schneider(wb_fox_param, m2x, m2y)

    wb_stone_param       = eva.calc_wb_stone_param(lat_mean, mldx, m2y, ri_diag, cs, -m2y.depthi)
    vb_stone_param       = eva.calc_vb_stone_param(lat_mean, mldx, m2y, ri_diag, cs)
    psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param, m2y, vb_stone_param, n2, grad_b)
    # psi_stone_held_param = eva.calc_streamfunc_held_schneider(wb_stone_param, m2x,  m2y)

    if np.isnan(argo_mld.data): argo_mld = 0

    scale = 1
    data  = psi_fox_full_param.isel(depthi=slice(0,depth_s)).where((psi_fox_full_param.lat > psi_fox_full_param.lat.min() + dl) & (psi_fox_full_param.lat < psi_fox_full_param.lat.max() - dl))
    data  = data.where(psi_fox_full_param.depthi < mldx, drop=True)
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)

    i = 0
    # scale   = 0.7
    psi_lim = psi_full_diag.where((psi_full_diag.lat > psi_full_diag.lat.min() + dl) & (psi_full_diag.lat < psi_full_diag.lat.max() - dl))
    data    = psi_lim.data
    # cmax    = np.nanmax(data)
    # cmin    = np.nanmin(data)
    # if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    # else: ulimit = -scale*cmin; llimit = scale*cmin
    # levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, psi_full_diag.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    levels_iso = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels_iso, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \psi = (v'b'  N^2 - w'b'  d_y b) / |\nabla b|^2 $ diagonised",fontsize=fs)
    ax[i].tick_params(labelsize=fl)

    i = 1
    # scale = 0.7
    data  = psi_fox_full_param.isel(depthi=slice(0,depth_s)).data
    # cmax  = np.nanmax(data)
    # cmin  = np.nanmin(data)
    # if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    # else: ulimit = -scale*cmin; llimit = scale*cmin
    # levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, psi_fox_full_param.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    # levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels_iso, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \psi $ Fox-Kemper Param.",fontsize=fs)
    ax[i].tick_params(labelsize=fl)

    i = 2
    # scale = 0.7
    data  = psi_stone_full_param.isel(depthi=slice(0,depth_s)).data#psi_full.data
    # cmax  = np.nanmax(data)
    # cmin  = np.nanmin(data)
    # if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    # else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, psi_stone_full_param.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    # levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels_iso, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \psi $ Stone Param.",fontsize=fs)
    ax[i].set_xlabel(f'{title}', fontsize=fs)
    ax[i].tick_params(labelsize=fl)

    if savefig == True: plt.savefig(f'{fig_path}', dpi=150, format='png', bbox_inches='tight')

# %%
def eval_front(lon_front, lat_front, front, dl):
    ylim =350,0

    lat_mean = eva.mean_help(lat_front)
    lon_mean = eva.mean_help(lon_front)
    argo_mld = argo.mld_da_mean.isel(iLAT=int(lat_mean), iLON=int(lon_mean), iMONTH=3)

    # M2y_masked       = M2y_dselect.where(M2y_dselect < -5e-10, np.nan)
    # wb_fselect,       vb_fselect, ri_fselect, mldx_fselect, mldx_monte_fselect, m2x_fselect, m2y_fselect, alpha_fselect, lhs_fselect, rho_fselect, n2_fselect = eva.calc_front_varaibles_2(wb_dselect, vb_dselect, ri_dselect, mldx, mldx_monte, M2x_dselect, M2y_masked, data_rho_mean, n2_dselect, lon_front, lat_front, dim='lon', ave=True)
    # data_dbdy_masked = data_dbdy_mean.where(data_dbdy_mean < -5e-8, np.nan)
    m2_filter        = 0#5e-8
    data_dbdy_masked = data_dbdy_mean.where(np.abs(data_dbdy_mean) > m2_filter, np.nan) #alternative threshold
    # data_rho_masked  = data_rho_mean.where(np.abs(data_dbdy_mean) > m2_filter, np.nan)

    wb_fselect, ub_fselect, vb_fselect, ri_fselect, mldx_fselect, mldx_monte_fselect, m2x_fselect, m2y_fselect, alpha_fselect, lhs_fselect, rho_fselect, n2_fselect = eva.calc_front_varaibles_2(
        data_wb_prime_mean, data_ub_prime_mean_depthi, data_vb_prime_mean_depthi, ri_mean, mldx, mldx_monte, data_dbdx_mean, data_dbdy_masked,
        data_rho_mean, data_n2_mean, lon_front, lat_front, dim='lon', ave=True)

    path = f'{fig_path}{front}_stream_func_filter_oct22.png'
    title = rf"{front}"

    # plot_slice_front(x=alpha_fselect.lat, y=alpha_fselect.depthi, lhs=lhs_fselect, alpha=alpha_fselect,
    #                  wb=wb_fselect, vb=vb_fselect, ri=ri_fselect, mldx=mldx_fselect, mldx_monte=mldx_monte_fselect,
    #                  argo_mld=argo_mld, rho=rho_fselect, m2x=m2x_fselect, m2y=m2y_fselect, n2=n2_fselect, dl=dl,
    #                  ylim=ylim, title=title, fig_path=path, savefig=False)

    path = f'{fig_path}{front}_stream_func_held_schneider_oct22.png'


    # plot_slice_front_psi_held(x=alpha_fselect.lat, y=alpha_fselect.depthi, lhs=lhs_fselect, alpha=alpha_fselect,
    #                  wb=wb_fselect, vb=vb_fselect, ri=ri_fselect, mldx=mldx_fselect, mldx_monte=mldx_monte_fselect,
    #                  argo_mld=argo_mld, rho=rho_fselect, m2x=m2x_fselect, m2y=m2y_fselect, n2=n2_fselect, dl=dl,
    #                  ylim=ylim, title=title, fig_path=path, savefig=True)

    path = f'{fig_path}{front}_stream_func_full_oct22.png'

    plot_slice_front_psi_full(x=alpha_fselect.lat, y=alpha_fselect.depthi, lhs=lhs_fselect, alpha=alpha_fselect,
                     wb=wb_fselect, vb=vb_fselect, ri=ri_fselect, mldx=mldx_fselect, mldx_monte=mldx_monte_fselect,
                     argo_mld=argo_mld, rho=rho_fselect, m2x=m2x_fselect, m2y=m2y_fselect, n2=n2_fselect, dl=dl,
                     ylim=ylim, title=title, fig_path=path, savefig=True)

# %%
reload(eva)
lon_reg_all, lat_reg_all = eva.get_new_fronts()
# lon_reg_all, lat_reg_all = eva.get_eddies()
nf = lon_reg_all.shape
# for ii in np.arange(1):
for ii in np.arange(nf[0]):
    # ii=0
    front =             f'R{ii+1}f'
    lon_front = lon_reg_all[ii,:]
    lat_front = lat_reg_all[ii,:]

    dl=1
    lat_front[1] = lat_front[1] + dl
    lat_front[0] = lat_front[0] - dl

    eval_front(lon_front , lat_front, front, dl)
# %%
print('all done compare stream func')
# %%
