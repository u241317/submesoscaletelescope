# evaluate section of submesoscale ocean front
# %%
import sys

#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import pandas as pd
import netCDF4 as nc
import xarray as xr    
from dask.diagnostics import ProgressBar
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload


# %%
#Reload modules:
reload(eva)
reload(tools)
# %%
lon_reg_R0    = [-80.5, -55]
lat_reg_R0    = [25, 40]
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/12_fronts_modified/stream_func_fluctuation/'
path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
lon_reg       = lon_reg_R0
lat_reg       = lat_reg_R0
# %%
time_averaged = ''

#calculate MLD
path_data     = f'{path_root_dat}t{time_averaged}.nc'
t_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
path_data     = f'{path_root_dat}s{time_averaged}.nc'
s_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
data_t_mean   = pyic.interp_to_rectgrid_xr(t_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_s_mean   = pyic.interp_to_rectgrid_xr(s_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_rho_mean = gsw.rho(data_s_mean.S001_sp, data_t_mean.T001_sp, depthc[2])

mld_monte, mask_monte, mldx_monte = eva.calc_mld_montegut_xr(data_rho_mean, depthc)
mld, mask, mldx = eva.calc_mld_xr(data_rho_mean, depthc)

#calculate Ri
path_data      = f'{path_root_dat}bx{time_averaged}.nc'
dbdx_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}by{time_averaged}.nc'
dbdy_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}n2{time_averaged}.nc'
n_mean         = xr.open_dataset(path_data, chunks=dict(depthi=1))
data_dbdx_mean = pyic.interp_to_rectgrid_xr(dbdx_mean.dbdx, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_dbdy_mean = pyic.interp_to_rectgrid_xr(dbdy_mean.dbdy, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_n2_mean   = pyic.interp_to_rectgrid_xr(n_mean.N2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_M2_mean   = np.sqrt(np.power(data_dbdx_mean,2) + np.power(data_dbdy_mean,2))

ri_mean = eva.calc_richardsonnumber(data_n2_mean.lat, data_n2_mean, data_dbdy_mean)

path_data          = f'{path_root_dat}wb{time_averaged}_prime.nc'
wb_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
wb_prime_mean      = wb_prime_mean.rename(__xarray_dataarray_variable__='wb_prime_mean')
data_wb_prime_mean = pyic.interp_to_rectgrid_xr(wb_prime_mean.wb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week


path_data          = f'{path_root_dat}vb{time_averaged}_prime.nc'
vb_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthc=1))
vb_prime_mean      = vb_prime_mean.rename(__xarray_dataarray_variable__='vb_prime_mean')
data_vb_prime_mean = pyic.interp_to_rectgrid_xr(vb_prime_mean.vb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

data_vb_prime_mean_depthi = data_vb_prime_mean.interp(depthc=depthi)
data_vb_prime_mean_depthi = data_vb_prime_mean_depthi.rename(depthc='depthi')

path_data          = f'{path_root_dat}ub{time_averaged}_prime.nc'
ub_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthc=1))
ub_prime_mean      = ub_prime_mean.rename(__xarray_dataarray_variable__='ub_prime_mean')
data_ub_prime_mean = pyic.interp_to_rectgrid_xr(ub_prime_mean.ub_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

data_ub_prime_mean_depthi = data_ub_prime_mean.interp(depthc=depthi)
data_ub_prime_mean_depthi = data_ub_prime_mean_depthi.rename(depthc='depthi')

# %%
argo = eva.load_argo_climatology()

# %%
def plot_slice_front_wide(x, y, alpha, vb, wb, mldx, mldx_monte, argo_mld, rho, m2x, m2y, n2, M2y_large, dl, ylim=[5000,0], title=None, fig_path=None, savefig=False):
    """plot vertical parameters of front"""
    fig, ax = plt.subplots(5, 1, figsize=(15,22))
    lw      = 4
    color   = 'r'
    ls      = 'dashed'
    fs      = 20
    depth_s = 83

    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_held = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)
    
    i=0
    idepth = 20
    scale  = 0.3
    data_p = M2y_large.isel(depthi=idepth)
    data   = data_p.where((data_p.lat > data_p.lat.min() + dl) & (data_p.lat < data_p.lat.max() - dl) & (data_p.lon > data_p.lon.min() + dl) & (data_p.lon < data_p.lon.max() - dl)).data
    cmax   = np.nanmax(data)
    cmin   = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(M2y_large.lon, M2y_large.lat, data_p.data, levels, cmap='PuOr_r', extend='both')

    # cf = ax[i].pcolormesh(M2y_large.lon, M2y_large.lat, M2y_large.isel(depthi=idepth).data, clim=clim, cmap='PuOr_r')
    cb = fig.colorbar(cf, ax=ax[i])
    rect, right, top = eva.draw_rect([M2y_large.lat[0]+dl, M2y_large.lat[-1]-dl], [M2y_large.lon[0]+dl, M2y_large.lon[-1]-dl],  color='black')
    ax[i].add_patch(rect)
    ax[i].set_title(rf'$db/dy$ at {depthi[idepth]}m', fontsize=fs)

    i=1
    scale  = 0.7
    data_p = alpha.isel(depthi=slice(0,depth_s))
    data   = data_p.where((data_p.lat > data_p.lat.min() + dl) & (data_p.lat < data_p.lat.max() - dl)).data
    cmax   = np.nanmax(data)
    cmin   = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), data_p.data, levels, cmap='PuOr_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls)
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw)
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r'$\alpha = db/dy ~ /f^2$',fontsize=fs)

    i=2
    scale  = 1
    data_p = wb.isel(depthi=slice(0,depth_s))
    data   = data_p.where((data_p.lat > data_p.lat.min() + dl) & (data_p.lat < data_p.lat.max() - dl)).data
    cmax   = np.nanmax(data)
    cmin   = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), data_p, levels, cmap='RdBu_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls)
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw)
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r'$ \overline{w^{\prime} b^{\prime}}$',fontsize=fs)

    i = 3
    scale = 0.7
    data  = vb.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(x, y, vb.data, levels,  cmap='RdBu_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    # levels = np.linspace(1025,1027,120)
    # ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \overline{v'b'} $",fontsize=fs)

    i=4
    scale = 0.7
    data  = psi_held.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, psi_held.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i])
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthc, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
    ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
    ax[i].axvline(wb.lat[0]+dl, color='black')
    ax[i].axvline(wb.lat[-1]-dl, color='black')
    ax[i].legend(loc='lower right')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \psi = (\overline{v'b'}  N^2 - \overline{w'b'}   d_y b) / |\nabla b|^2 $",fontsize=fs)
    ax[i].set_xlabel(f"{title}")

    if savefig == True: plt.savefig(fig_path, dpi=150, format='png')

# %%

def eval_front(lon_front, lat_front, front, dl):
    ylim = 500,0

    lat_mean = eva.mean_help(lat_front)
    lon_mean = eva.mean_help(lon_front)
    argo_mld = argo.mld_da_mean.isel(iLAT=int(lat_mean), iLON=int(lon_mean), iMONTH=3)

    m2_filter = 1e-8
    data_dbdy_masked = data_dbdy_mean.where(np.abs(data_dbdy_mean) > m2_filter, np.nan) #alternative threshold

    wb_fselect, ub_fselect, vb_fselect, ri_fselect, mldx_fselect, mldx_monte_fselect, m2x_fselect, m2y_fselect, alpha_fselect, lhs_fselect, rho_fselect, n2_fselect = eva.calc_front_varaibles_2(
        data_wb_prime_mean, data_ub_prime_mean_depthi, data_vb_prime_mean_depthi, ri_mean, mldx, mldx_monte, data_dbdx_mean, data_dbdy_masked, 
        data_rho_mean, data_n2_mean, lon_front, lat_front, dim='lon', ave=True)


    path = f'{fig_path}{front}_wide_slice_oct22.png'
    title = rf"{front} with $|d_y b| > {m2_filter}$"

    M2y_large = data_dbdy_mean.sel(lat=slice( lat_front[0], lat_front[1]), lon=slice(lon_front[0] -dl, lon_front[1]+dl))

    plot_slice_front_wide(x=alpha_fselect.lat, y=alpha_fselect.depthi, alpha=alpha_fselect,
                          vb=vb_fselect, wb=wb_fselect,  mldx=mldx_fselect, mldx_monte=mldx_monte_fselect,
                          argo_mld=argo_mld, rho=rho_fselect, m2x=m2x_fselect, m2y=m2y_fselect,
                          n2=n2_fselect, M2y_large=M2y_large, dl=dl, ylim=ylim,
                          title=title, fig_path=path, savefig=False)


# %%
reload(eva)
lon_reg_all, lat_reg_all = eva.get_new_fronts()
for ii in np.arange(1):
    ii=0
    front =             f'R{ii+1}f'
    lon_front = lon_reg_all[ii,:]
    lat_front = lat_reg_all[ii,:]

    dl=1
    lat_front[1] = lat_front[1] + dl
    lat_front[0] = lat_front[0] - dl

    eval_front(lon_front , lat_front, front, dl)

print('all done front stream fluctuation')
# %%
