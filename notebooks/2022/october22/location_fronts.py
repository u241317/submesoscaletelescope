# %%
import sys

#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import pandas as pd
import netCDF4 as nc
import xarray as xr    
from dask.diagnostics import ProgressBar
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload

# %%
lon_reg_R0    = [-80.5, -55]
lat_reg_R0    = [25, 40]
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/12_fronts_modified/location/'
path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
lon_reg       = lon_reg_R0
lat_reg       = lat_reg_R0
time_averaged = ''

# %%
path_data   = f'{path_root_dat}vort_mean{time_averaged}.nc'
vort_mean_w = xr.open_dataset(path_data, chunks=dict(depthc=1))
vorticity   = pyic.interp_to_rectgrid_xr(vort_mean_w.vort_f_cells_50m, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

#calculate MLD
path_data     = f'{path_root_dat}t{time_averaged}.nc'
t_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
path_data     = f'{path_root_dat}s{time_averaged}.nc'
s_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
data_t_mean   = pyic.interp_to_rectgrid_xr(t_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_s_mean   = pyic.interp_to_rectgrid_xr(s_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_rho_mean = gsw.rho(data_s_mean.S001_sp, data_t_mean.T001_sp, depthc[2])

mld_monte, mask_monte, mldx_monte = eva.calc_mld_montegut_xr(data_rho_mean, depthc)
mld, mask, mldx = eva.calc_mld_xr(data_rho_mean, depthc)

path_data      = f'{path_root_dat}bx{time_averaged}.nc'
dbdx_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}by{time_averaged}.nc'
dbdy_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}n2{time_averaged}.nc'
n_mean         = xr.open_dataset(path_data, chunks=dict(depthi=1))
data_dbdx_mean = pyic.interp_to_rectgrid_xr(dbdx_mean.dbdx, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_dbdy_mean = pyic.interp_to_rectgrid_xr(dbdy_mean.dbdy, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_n2_mean   = pyic.interp_to_rectgrid_xr(n_mean.N2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_M2_mean   = np.sqrt(np.power(data_dbdx_mean,2) + np.power(data_dbdy_mean,2))


# %%
reload(eva)
# lon_reg_all, lat_reg_all = eva.get_new_fronts()
lon_reg_all, lat_reg_all = eva.get_eddies()
nf = lon_reg_all[:,0].shape
nf = nf[0]

def add_rectangles():
    fs = 22
    for ii in np.arange(nf):
        # if ii == 3: continue
        front =             f'R{ii+1}f'
        lon_front = lon_reg_all[ii,:]
        lat_front = lat_reg_all[ii,:]

        rect, right, top = eva.draw_rect(lat_front, lon_front,  color='black')
        ax.add_patch(rect)
        ax.text(right, top, f'{front}', fontsize=fs)


# %% large!
idepth = 16
lon = data_M2_mean.lon
lat = data_M2_mean.lat
lon_reg = lon_reg_R0
lat_reg = lat_reg_R0
fs=22
ls = 15


asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
hca, hcb = pyic.arrange_axes(2, 2, plot_cb=False, asp=asp, fig_size_fac=4, projection=ccrs_proj, sharex=True, axlab_kw=None,  sharey=True, dcbr=2)
# fig = plt.gcf()
# fig.suptitle('7 Day mean')
ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 15.5,21
pyic.shade(lon, lat, data_t_mean.isel(depthc=0).T001_sp, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdYlBu_r')
ax.set_title(f'SST', fontsize=fs)
add_rectangles()

ii+=1; ax=hca[ii]; cax=hcb[ii]
clim=0,1e-7
pyic.shade(lon, lat, data_M2_mean.isel(depthi=idepth), ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdPu_r')
ax.set_title(f'M2 at {depthi[idepth]}m', fontsize=fs)
add_rectangles()

ii+=1; ax=hca[ii]; cax=hcb[ii]
clim = 1
pyic.shade(lon, lat, vorticity, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False)
ax.set_xlabel(f'Vorticity at 50m over f', fontsize=fs)
add_rectangles()

ii+=1; ax=hca[ii]; cax=hcb[ii]
contfs = np.arange(50.,600.,50.)
clim = [contfs.min(), contfs.max()]
pyic.shade(lon, lat, mldx, ax=ax, cax=cax, clim=clim, contfs=contfs,  transform=ccrs_proj, rasterized=False)
ax.set_xlabel(f'MLD', fontsize=22)
add_rectangles()

for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)   # type: ignore
    ax.tick_params(labelsize=15)

# plt.savefig(f'{fig_path}select_front_overviews_oct22.png', dpi=250, format='png', bbox_inches='tight')

plt.show()

# %%  close 1
idepth = 16
lon = data_M2_mean.lon
lat = data_M2_mean.lat
lon_reg = -71,-56
lat_reg = 25,38
fs=30
ls = 15


asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
hca, hcb = pyic.arrange_axes(2, 2, plot_cb=False, asp=asp, fig_size_fac=4, projection=ccrs_proj, sharex=False, axlab_kw=None,  sharey=True, daxr=3)
# fig = plt.gcf()
# fig.suptitle('7 Day mean')
ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 16.5,22
pyic.shade(lon, lat, data_t_mean.isel(depthc=0).T001_sp, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdYlBu_r')
ax.set_title(f'SST', fontsize=fs)
add_rectangles()

ii+=1; ax=hca[ii]; cax=hcb[ii]
clim=0,1e-7
pyic.shade(lon, lat, data_M2_mean.isel(depthi=idepth), ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdPu_r')
ax.set_title(f'M2 at {depthi[idepth]}m', fontsize=fs)
add_rectangles()

ii+=1; ax=hca[ii]; cax=hcb[ii]
clim = 1
pyic.shade(lon, lat, vorticity, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False)
ax.set_xlabel(f'Vorticity at 50m over f', fontsize=fs)
add_rectangles()

ii+=1; ax=hca[ii]; cax=hcb[ii]
contfs = np.arange(50.,600.,50.)
clim = [contfs.min(), contfs.max()]
pyic.shade(lon, lat, mldx, ax=ax, cax=cax, clim=clim, contfs=contfs,  transform=ccrs_proj, rasterized=False)
ax.set_xlabel(f'MLD', fontsize=fs)
add_rectangles()

for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)  # type: ignore
    ax.tick_params(labelsize=22)
    # cax.tick_params(labelsize=15)

# plt.savefig(f'{fig_path}select_front_zoom_oct22.png', dpi=250, format='png', bbox_inches='tight')


# %%  close 2
idepth = 50
lon = data_M2_mean.lon
lat = data_M2_mean.lat
lon_reg = -71,-56
lat_reg = 25,38
fs=30
ls = 15

asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
hca, hcb = pyic.arrange_axes(2, 1, plot_cb=False, asp=asp, fig_size_fac=4, projection=ccrs_proj, sharex=False, axlab_kw=None,  sharey=True, daxr=3)
ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 5e-8
pyic.shade(lon, lat, data_dbdx_mean.isel(depthi=idepth), ax=ax, cax=cax, clim=clim, transform=ccrs_proj, rasterized=False, cmap='PuOr_r')
ax.set_title(f'dbdx', fontsize=fs)
add_rectangles()

ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 5e-8
pyic.shade(lon, lat, data_dbdy_mean.isel(depthi=idepth), ax=ax, cax=cax, clim=clim, transform=ccrs_proj, rasterized=False, cmap='PuOr_r')
ax.set_title(f'dbdy {depthi[idepth]}m', fontsize=fs)
add_rectangles()

for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)  # type: ignore
    ax.tick_params(labelsize=22)

# plt.savefig(f'{fig_path}select_front_lateral_oct22.png', dpi=250, format='png', bbox_inches='tight')


# %% Select only strong fronts in region
mask = data_dbdy_mean.where(np.abs(data_dbdy_mean) > 2e-8, np.nan)

# %%
idepth = 55
lon = data_M2_mean.lon
lat = data_M2_mean.lat
lon_reg = -71,-56
lat_reg = 25,38
fs=30
ls = 15

asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=asp, fig_size_fac=4, projection=ccrs_proj, sharex=False, axlab_kw=None,  sharey=True, daxr=3)
ii=-1

ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 5e-8
pyic.shade(lon, lat, mask.isel(depthi=idepth), ax=ax, cax=cax, clim=clim, transform=ccrs_proj, rasterized=False, cmap='PuOr_r')
ax.set_title(f'dbdy {depthi[idepth]}m', fontsize=fs)
add_rectangles()

for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)  # type: ignore
    ax.tick_params(labelsize=22)

plt.savefig(f'{fig_path}select_front_filter_oct22.png', dpi=250, format='png', bbox_inches='tight')
# %%
from mpl_point_clicker import clicker
%matplotlib widget
# %%
idepth = 55
lon = data_M2_mean.lon
lat = data_M2_mean.lat
lon_reg = -73,-68
lat_reg = 33,37
fs=30
ls = 15

asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=asp, fig_size_fac=4, projection=ccrs_proj, sharex=False, axlab_kw=None,  sharey=True, daxr=3)
ii=-1

ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 5e-8
pyic.shade(lon, lat, data_dbdy_mean.isel(depthi=idepth), ax=ax, cax=cax, clim=clim, transform=ccrs_proj, rasterized=False, cmap='PuOr_r')
ax.set_title(f'dbdy {depthi[idepth]}m', fontsize=fs)
# add_rectangles()

for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)  # type: ignore
    ax.tick_params(labelsize=22)

plt.savefig(f'{fig_path}select_front_filter_oct22.png', dpi=250, format='png', bbox_inches='tight')

# klicker = clicker(ax, ["event"], markers=["x"])

plt.show()

# print(klicker.get_positions())
# %%

# %%
reload(eva)
idepth = 55
lon = data_M2_mean.lon
lat = data_M2_mean.lat
lon_reg = -74,-60
lat_reg = 25,38.5
fs=30
ls = 15

asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=4, projection=ccrs_proj, sharex=False, axlab_kw=None,  sharey=True, daxr=3)
ii=-1

ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 5e-8
# pyic.shade(lon, lat, data_dbdy_mean.isel(depthi=idepth), ax=ax, cax=cax, clim=clim, transform=ccrs_proj, rasterized=False, cmap='PuOr_r')
clim = 11,21
pyic.shade(lon, lat, data_t_mean.T001_sp.isel(depthc=idepth), ax=ax, cax=cax, clim=clim, transform=ccrs_proj, rasterized=False, cmap='gist_ncar')
# ax.set_title(f'dbdy {depthi[idepth]}m', fontsize=fs)
ax.set_title(f'T {depthc[idepth]}m', fontsize=fs)

add_rectangles()

for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)  # type: ignore
    ax.tick_params(labelsize=22)


plt.savefig(f'{fig_path}select_eddiesT_oct22.png', dpi=250, format='png', bbox_inches='tight')

# %%
