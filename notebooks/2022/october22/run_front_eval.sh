#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j
#SBATCH --nodes=1
#####SBATCH --ntasks-per-node=1
#SBATCH --partition=compute
####SBATCH --mem=256Gb
#SBATCH --time=00:30:00 
#SBATCH --exclusive
#SBATCH --account=mh0033
#SBATCH --mail-type=FAIL 


module list

#source /work/mh0033/u241317/pyicon/tools/conda_act_mistral_pyicon_env.sh
source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

run=$1
startdate=`date +%Y-%m-%d\ %H:%M:%S`
# mpirun -np 1 python -u compare_stream_func.py --slurm ${run}  
# mpirun -np 1 python -u front_stream_fluctuation.py --slurm ${run}
# mpirun -np 1 python -u front_vertical_profiles.py --slurm ${run}
python -u inst_front_stream_fluctuation.py --slurm ${run}
enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

