#%%
import intake
import pandas as pd
import xarray as xr

# %%
eerie_cat=intake.open_catalog("https://raw.githubusercontent.com/eerie-project/intake_catalogues/main/eerie.yaml")
eerie_cat
# %%
eerie_dkrz=eerie_cat["dkrz"]
eerie_dkrz_disk=eerie_dkrz["disk"]
cat=eerie_dkrz_disk["model-output"]

# %%
dsid = "icon-esm-er.eerie-control-1950.v20231106.ocean.gr025"#.native.5lev_daily_mean"
cat[dsid]
#%%
ds = cat[dsid].to_dask()
ds
# %%
dsdict={}
for ds in list(dsid):
    print(ds)
    dsdict[ds]=dsid[ds].to_dask()
# %%
list(cat[dsid])
# %% ################# NEXT GEMS
import intake
cat = intake.open_catalog("https://data.nextgems-h2020.eu/catalog.yaml")
# ds = cat.ICON.ngc4008.to_dask()  # this does NOT use dask, see note below
ds = cat.FESOM.IFS_28-FESOM_25-cycle3.to_dask()  # this does NOT use dask, see note below
# %%
list(cat.FESOM)
# %%

cat2 = cat.FESOM
# %%
da = cat2["IFS_4.4-FESOM_5-cycle3"]
da = da.to_dask()
# %%
import xarray as xr
import smt_modules.all_funcs as eva

path = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_lev/smtwv0007_oce_2d_lev_PT1H_20191002T001500Z.nc'
path = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv0007/outdata_lev_2/smtwv0007_oce_2d_lev_2_PT1H_20200325T171500Z.nc'
ds = xr.open_dataset(path)
ds
# %%
