# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors

# %%
def plot_wT_prime_components(wT_prime_inter, wT_mean_inter, w_mean_T_mean_inter, idepth, lon_reg, lat_reg, savefig=False, fig_path=None):
    asp = (lat_reg[1] - lat_reg[0]) /(lon_reg[1] - lon_reg[0] )

    hca, hcb = pyic.arrange_axes(3, 1, plot_cb = True, asp=asp, fig_size_fac=2, projection=ccrs_proj, sharey=True)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(wT_prime_inter.lon, wT_prime_inter.lat, wT_prime_inter, ax=ax, cax=cax, clim=[-5e-4, 5e-4], transform=ccrs_proj, cmap='RdBu_r', extend='both')
    hm1[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm1[1].formatter.set_useMathText(True)
    ax.set_title(r'$\overline{w^{\prime}T^{\prime}}$', fontsize=16)
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(wT_mean_inter.lon, wT_mean_inter.lat, wT_mean_inter, ax=ax, cax=cax, clim=[-1e-2,1e-2],  transform=ccrs_proj, cmap='RdBu_r', extend='both')
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)
    ax.set_title(r'$\overline{wT} $', fontsize=16)
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm3 = pyic.shade(w_mean_T_mean_inter.lon, w_mean_T_mean_inter.lat, w_mean_T_mean_inter, ax=ax, cax=cax, clim=[-1e-2,1e-2],  transform=ccrs_proj, cmap='RdBu_r', extend='both')
    hm3[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm3[1].formatter.set_useMathText(True)
    ax.set_title(r'$\overline{w} ~\overline{T}$', fontsize=16)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    if savefig==True: plt.savefig(f'{fig_path}/wT_components_{depthc[idepth]}.png', dpi=150, bbox_inches='tight')

def plot_wt_prime(wT_prime, lon_reg, lat_reg,  savefig=False, fig_path=None):
    asp = (lat_reg[1] - lat_reg[0]) /(lon_reg[1] - lon_reg[0] )
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = True, asp=asp, fig_size_fac=2, projection=ccrs_proj)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = [-5e-4, 5e-4]
    hm1 = pyic.shade(wT_prime.lon, wT_prime.lat, wT_prime, ax=ax, cax=cax, clim=clim, transform=ccrs_proj, cmap='Spectral', extend='both')
    hm1[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm1[1].formatter.set_useMathText(True)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    ax.set_title(r'$\overline{w^{\prime}T^{\prime}}$', fontsize=16)
    ax.legend(loc='lower left')
    if savefig:
        plt.savefig(f'{fig_path}/wT_prime.png', dpi=150, bbox_inches='tight')


def add_front_lines_box(ax):
    points = eva.get_points_of_inclined_fronts()
    for ii in np.arange(int(points.shape[0]/2)):
        P = points[2*ii:2*ii+2,:]
        y, x, m, b = eva.calc_line(P)
        delta=0.8*0.1 # 0.8 view 0.1 actual front
        color = 'r'
        lw = 1
        ls = ':'
        # ax.plot(x,y, lw=lw, color=color, ls=ls)
        ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
        ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
        ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
        ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
        # t = ax.text(P[0,0]-0.05, P[0,1], f'F{1}', color='black', fontsize=20)
        # t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

def add_rectangles(ax, lon_reg, lat_reg, txt, color='black'):
    rect, right, top = eva.draw_rect(lat_reg, lon_reg,  color=color)
    ax.add_patch(rect)
    ax.text(lon_reg[1], lat_reg[1], f'{txt}', color=color, fontsize=20)

def plot_single_front_margins(ax):
    points = eva.get_points_of_inclined_fronts()
    P = points[0:2,:]
    y, x, m, b = eva.calc_line(P)
    delta = 0.8*0.1 # 0.8 view 0.1 actual front
    color = 'r'
    lw    = 3
    ls    = ':'
    ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
    ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
    ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
    ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
    t = ax.text(P[0,0]-0.05, P[0,1]+0.2, f'F{1}', color='black', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


def plot_region_zoom_vertical(data_coarse, data_reg, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2):
    hca, hcb = pyic.arrange_axes(2, 1,  asp=asp2, plot_cb='right', fig_size_fac=2, daxl=0.9,  # type: ignore
                                 projection=ccrs_proj,
                                 daxt=1, f_dcbt=1.5)
    ii=-1

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = -5e-4, 5e-4
    cmap = 'RdBu_r'
    ax.set_title(r' $ \overline{w^{\prime}T^{\prime}} $'+f' at {depthc[idepth]}m')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = np.array([-63.9, -61.5])
    lat_RF1 = np.array([34, 35.875])

    i=0
    for ax in hca:
        i += 1
        if i%2 == 1:
            add_front_lines_box(ax)
            add_rectangles(ax, lon_reg_3, lat_reg_3, '', color='red')
            t = ax.text(lon_R[1]-0.9, lat_R[1]-1, 'R', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
            pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) # type: ignore
        else:
            pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3) # type: ignore
            plot_single_front_margins(ax)
            t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore


    fname = 'map_param_fronts_portrait'
    if savefig == True: plt.savefig(f'{fig_path}{fname}_{depthc[idepth]}.png', dpi=250, format='png', bbox_inches='tight')


def plot_region_w_DT(data_coarse, data_reg, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2):
    hca, hcb = pyic.arrange_axes(2, 1,  asp=asp2, plot_cb='right', fig_size_fac=2, daxl=0.9,  # type: ignore
                                 projection=ccrs_proj,
                                 daxt=1, f_dcbt=1.5)
    ii=-1

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = -1e-5, 1e-5
    cmap = 'RdBu_r'
    ax.set_title(r' $ w \partial_z T$'+f' at {depthc[idepth]}m')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = np.array([-63.9, -61.5])
    lat_RF1 = np.array([34, 35.875])

    i=0
    for ax in hca:
        i += 1
        if i%2 == 1:
            add_front_lines_box(ax)
            add_rectangles(ax, lon_reg_3, lat_reg_3, '', color='red')
            t = ax.text(lon_R[1]-0.9, lat_R[1]-1, 'R', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
            pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) # type: ignore
        else:
            pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3) # type: ignore
            plot_single_front_margins(ax)
            t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore


    fname = 'map_param_fronts_portrait_w_DT'
    if savefig == True: plt.savefig(f'{fig_path}{fname}_{depthi[idepth]}.png', dpi=250, format='png', bbox_inches='tight')



def plot_region_q_vert(data_coarse, data_reg, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2):
    hca, hcb = pyic.arrange_axes(2, 1,  asp=asp2, plot_cb='right', fig_size_fac=2, daxl=0.9,  # type: ignore
                                 projection=ccrs_proj,
                                 daxt=1, f_dcbt=1.5)
    ii=-1

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 100
    cmap = 'RdBu_r'
    ax.set_title(r' $ - \rho c_p w \partial_z T$'+f' at {depthc[idepth]}m')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = np.array([-63.9, -61.5])
    lat_RF1 = np.array([34, 35.875])

    i=0
    for ax in hca:
        i += 1
        if i%2 == 1:
            add_front_lines_box(ax)
            add_rectangles(ax, lon_reg_3, lat_reg_3, '', color='red')
            t = ax.text(lon_R[1]-0.9, lat_R[1]-1, 'R', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
            pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) # type: ignore
        else:
            pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3) # type: ignore
            plot_single_front_margins(ax)
            t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore


    fname = 'map_param_fronts_portrait_q_vert'
    if savefig == True: plt.savefig(f'{fig_path}{fname}_{depthi[idepth]}.png', dpi=250, format='png', bbox_inches='tight')

