# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import string
from matplotlib.ticker import FormatStrFormatter
import funcs as fu

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %%
fig_path      = '/home/m/m300878/submesoscaletelescope/results/smt_natl/heat_flux/'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
savefig       = True

lon_reg_3 = np.array([-63.9, -61.5])
lat_reg_3 = np.array([34, 35.875])
lon_reg_2 = np.array([-70, -54])
lat_reg_2 = np.array([23.5, 36])

asp3 = (lat_reg_3[1]-lat_reg_3[0])/(lon_reg_3[1]-lon_reg_3[0])
asp2 = (lat_reg_2[1]-lat_reg_2[0])/(lon_reg_2[1]-lon_reg_2[0])
print('asp2', asp2, 'asp3',asp3)
idepth        = 32

time0 = pd.to_datetime('2010-03-15T21:00:00.000000000')
time1 = pd.to_datetime('2010-03-22T19:00:00.000000000')
# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00')
cluster
client

# %% load mean field of temperature and vertical velocity
path      = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
name      = 't.nc'
ds_t_mean = xr.open_dataset(path+name)
name      = 'w.nc'
ds_w_mean = xr.open_dataset(path+name)
# %%
T_mean = ds_t_mean.T001_sp.isel(depthc=idepth)
w_mean = ds_w_mean.w.isel(depthi=idepth)
w_mean = w_mean.rename({'depthi':'depthc'})

# %% load instantan temperature and vertical velocity
ds_T = eva.load_smt_T()
ds_w = eva.load_smt_w()
ds_w = ds_w.drop('clon').drop('clat')
# %%
ds_S = eva.load_smt_S()
ds_S_sel = ds_S.sel(time=slice(time0, time1))
# %%
ds_T_sel = ds_T.sel(time=slice(time0, time1))
ds_w_sel = ds_w.sel(time=slice(time0, time1))
to       = ds_T_sel.isel(depthc=idepth)
wo       = ds_w_sel.w.isel(depthi=idepth)
wo       = wo.rename({'depthi':'depthc'})

# %% compute wT_prime
wT_mean  = (to * wo).mean('time')
wT_mean  = wT_mean.compute()
wT_prime = wT_mean - w_mean * T_mean
# %% interpolate to rectgrid
wT_mean_inter       = pyic.interp_to_rectgrid_xr(wT_mean, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
w_mean_T_mean_inter = pyic.interp_to_rectgrid_xr(w_mean * T_mean, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
w_mean_inter        = pyic.interp_to_rectgrid_xr(w_mean, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
T_mean_inter        = pyic.interp_to_rectgrid_xr(T_mean, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
wT_prime_inter      = pyic.interp_to_rectgrid_xr(wT_prime, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)

# %% interpolate to triangular grid
data_coarse     = wT_prime_inter
Tri, data_reg   = eva.calc_triangulation_obj(wT_prime, lon_reg_3, lat_reg_3)

# %% plot components
reload(fu)
plt.figure()
w_mean_inter.plot(vmin=-5e-4, vmax=5e-4, cmap='RdBu_r')
plt.figure()
T_mean_inter.plot(vmin=16,vmax=25, cmap='plasma')

fu.plot_wT_prime_components(wT_prime_inter, wT_mean_inter, w_mean_T_mean_inter, idepth, lon_reg_2, lat_reg_2, savefig=savefig, fig_path=fig_path)
# %% coarse
fu.plot_wt_prime(wT_prime_inter, lon_reg_3, lat_reg_3, savefig=False, fig_path=None)

# %% large and zoomed and fronts
fu.plot_region_zoom_vertical(data_coarse, data_reg, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path=fig_path, savefig=savefig, asp2=asp2)


# %%
# comppute vertical component of advective heat flux
# %% compute density
itime     = 42
rho_upper = gsw.rho(ds_S_sel.isel(depthc=idepth-1).isel(time=itime), ds_T_sel.isel(depthc=idepth-1).isel(time=itime), depthc[idepth-1])
rho_lower = gsw.rho(ds_S_sel.isel(depthc=idepth).isel(time=itime), ds_T_sel.isel(depthc=idepth).isel(time=itime), depthc[idepth])
# interpolate to depthi inbetween
rho = (rho_upper + rho_lower)/2

cp_lower = gsw.cp_t_exact(ds_S_sel.isel(depthc=idepth).isel(time=itime), ds_T_sel.isel(depthc=idepth).isel(time=itime), depthc[idepth])
cp_upper = gsw.cp_t_exact(ds_S_sel.isel(depthc=idepth-1).isel(time=itime), ds_T_sel.isel(depthc=idepth-1).isel(time=itime), depthc[idepth-1])

cp = (cp_upper + cp_lower)/2
# add coordinate depthi
cp  = cp.assign_coords(depthi=depthi[idepth])
rho = rho.assign_coords(depthi=depthi[idepth])
# compute
rho    = rho.compute()
cp     = cp.compute()



# %%
w_DT = wo * ( ds_T_sel.isel(depthc=idepth-1)- ds_T_sel.isel(depthc=idepth))/dzw[idepth]
w_DT = w_DT.isel(time=itime).compute()

w_DT_inter = pyic.interp_to_rectgrid_xr(w_DT, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
Tri, data_reg = eva.calc_triangulation_obj(w_DT, lon_reg_3, lat_reg_3)
# %%
reload(fu)
fu.plot_region_w_DT(w_DT_inter, data_reg, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path=fig_path, savefig=savefig, asp2=asp2)

# %%
q_vert = - rho * cp * w_DT
q_vert = q_vert.compute()
# %%
q_vert_inter = pyic.interp_to_rectgrid_xr(q_vert, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
Tri, data_reg = eva.calc_triangulation_obj(q_vert, lon_reg_3, lat_reg_3)
# %%
reload(fu)
fu.plot_region_q_vert(q_vert_inter, data_reg, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path=fig_path, savefig=savefig, asp2=asp2)
# %%
# add area
