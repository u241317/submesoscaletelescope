# %%
from collections import OrderedDict
from pathlib import Path
import xarray as xr
import pyicon as pyic
import numpy as np

from importlib import reload
from dask_jobqueue import SLURMCluster
from dask.distributed import Client
import dask.config

from tempfile import NamedTemporaryFile, TemporaryDirectory # Creating temporary Files/Dirs
from getpass import getuser # Libaray to copy things


# %%

def init_dask_slurm_cluster(scale = 2, processes = 16, walltime="04:00:00", memory="256GiB", wait=True):
    dask.config.set(
        {
            "distributed.worker.data-directory": "/scratch/m/m300878/dask_temp",
            "distributed.worker.memory.target": 0.75,
            "distributed.worker.memory.spill": 0.85,
            "distributed.worker.memory.pause": 0.95,
            "distributed.worker.memory.terminate": 0.98,
        }
    )

    scluster = SLURMCluster(
        queue             = "compute",
        walltime          = walltime,
        memory            = memory,
        cores             = processes,
        processes         = processes,
        account           = "mh0033",
        name              = "m300878-dask-cluster",
        interface         = "ib0",
        asynchronous      = False,
        scheduler_options = {'dashboard_address': ':8989'},
        log_directory     = "/scratch/m/m300878/dask_logs/",
        local_directory   = "/scratch/m/m300878/dask_temp/",
    )
    
    client = Client(scluster)
    scluster.scale(jobs=scale)
    print(scluster.job_script())
    if wait ==True: 
        nworkers = scale * processes
        client.wait_for_workers(nworkers)              # waits for all workers to be ready, can be submitted now

    return client, scluster



def load_smt_wave_all(variable, exp, remove_bnds=False, it=1):
    '''
    strings are u,v,w,to,so,tke,2d,forcing
    run  7-9

    usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
    '''
    if exp == 7: print('exp: Jul-Aug 2019')
    elif exp==8: print('exp: Feb-Mar 2022')
    elif exp==9: print('exp: July 2019; no tides!')

    # if exp ==10: folder    = f'smtwv000{exp}'
    folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce*.nc'
    # if exp == 10: path_data = f'{path}/smtwv000{exp}_oce_3d_{variable}*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()
    # print('remove first 6 files, saved in different fashion')
    # if exp==7 or exp ==8: 
    #     flist = flist[6:]
    #     print('flist size', flist.size)

    if variable == '2d' or variable == 'forcing':
        chunks = dict(time=1)
    else:
        chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    # time0 = f'{flist[0]}'[106:117]
    flist = flist[::it]
    # size  = flist.size
    # pd.to_datetime(time0)
    # print('time0', time0)
    # timestring = pd.date_range(time0, periods=size, freq=f"{it}h")
    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp
    # print('override timestamp')
    # ds['time'] = timestring

    if remove_bnds==True:
        if 'clon_bnds' in ds:
            ds = ds.drop(['clon_bnds', 'clat_bnds'])
        else:
            print('no bnds to remove')

    return(ds)


def load_smt_wave_land_mask():
    # ds        = load_smt_wave_to_exp7_08(it=2)
    # sst       = ds.isel(time=0, depth=0).to
    # a         = sst.where(~(sst==0), np.nan)
    # mask_land = ~np.isnan(a)
    # mask_land = mask_land.where(mask_land==True, np.nan)
    # mask_land = mask_land.drop('time')

    path = '/work/mh0033/m300878/smtwv_oce_2022/masks/land_mask.nc'
    ds   = xr.open_dataarray(path)
    ds   = ds.drop('depth')
    return ds