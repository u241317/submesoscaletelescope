# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors

# %% get cluster
import smt_modules.init_slurm_cluster as scluster # replace this by your own environment
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:15:00')
cluster
client

# %%
ds_velocity = eva.load_smt_wave_all('u', 9, it=1, remove_bnds=True)
# %%
mask_land = eva.load_smt_wave_land_mask()
# %%
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'

# %%
ds_sel = ds_velocity.isel(time=0, depth=20)

# %%
ds_interp = pyic.interp_to_rectgrid_xr(ds_sel, fpath_ckdtree)
ds_interp.u.plot()
# %%
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
lat_reg       = [-40,-30]
lon_reg       = [-35, -25]
mask_interp   = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
ds_interp     = pyic.interp_to_rectgrid_xr(ds_sel, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

data = ds_interp * mask_interp

data.u.plot()
# %%
