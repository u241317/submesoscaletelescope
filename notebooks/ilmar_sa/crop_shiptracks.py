# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import numpy as np
from scipy.spatial import cKDTree
import xarray as xr
import pandas as pd

# %% get cluster
import smt_modules.init_slurm_cluster as scluster # replace this by your own environment
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:15:00')
cluster
client
# %%

def spherical_to_cartesian(lon, lat):
  earth_radius = 6371e3
  x = earth_radius * np.cos(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  y = earth_radius * np.sin(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  z = earth_radius * np.sin(lat*np.pi/180.)
  return x, y, z

def interp_to_reg_grid(lon_i, lat_i, lon_o, lat_o, da):
    xi, yi, zi = spherical_to_cartesian(lon_i, lat_i)
    xo, yo, zo = spherical_to_cartesian(lon_o, lat_o)

    lzip_i = np.concatenate((xi[:,np.newaxis],yi[:,np.newaxis],zi[:,np.newaxis]), axis=1)
    lzip_o = np.concatenate((xo[:,np.newaxis],yo[:,np.newaxis],zo[:,np.newaxis]), axis=1) 
    tree   = cKDTree(lzip_i)
    dckdtree, ickdtree = tree.query(lzip_o , k=1, workers=-1)
    # dckdtree, ickdtree = tree.query(lzip_o , k=n_nearest_neighbours)

    Dind_dist = dict()
    Dind_dist['dckdtree_c'] = dckdtree
    Dind_dist['ickdtree_c'] = ickdtree

    ds_grid = xr.Dataset(coords=dict(lon=da.lon, lat=da.lat))
    for var in Dind_dist.keys(): 
        ds_grid[var] = xr.DataArray(Dind_dist[var].reshape(da.crosstrack.size, da.alongtrack.size), dims=['crosstrack', 'alongtrack'])

    return ds_grid


def apply_grid(ds, ds_grid):
    arr_interp = ds.isel(ncells=ds_grid.ickdtree_c.compute().data.flatten())
    arr_interp = arr_interp.assign_coords(ncells=pd.MultiIndex.from_product([ds_grid.crosstrack.data, ds_grid.alongtrack.data], names=("crosstrack", "alongtrack"))).unstack()
    arr_interp = arr_interp.drop('crosstrack').drop('alongtrack')
    arr_interp = arr_interp.assign_coords(lat=ds_grid.lat, lon=ds_grid.lon)
    return arr_interp


def create_shiptrack_mesh(ds_tracks,i):
    lon0 = ds_tracks.isel(shiptrack=i).lon[0+i*2].values
    lat0 = ds_tracks.isel(shiptrack=i).lat[0+i*2].values
    lon1 = ds_tracks.isel(shiptrack=i).lon[1+i*2].values
    lat1 = ds_tracks.isel(shiptrack=i).lat[1+i*2].values

    # create a line between the two points with 0.01° spacing
    m       = (lat1 - lat0) / (lon1 - lon0)
    b       = lat0 - m * lon0
    dist    = np.sqrt((lat1 - lat0)**2 + (lon1 - lon0)**2)
    dx      = dist/0.008
    npoints = int(dx)
    lon_o   = np.linspace(lon0, lon1, npoints)
    lat_o   = m * lon_o + b

    P0 = np.array([lon_o, lat_o])
    m_perpendicular = np.array([-m, 1])/np.sqrt(1 + m**2)
    m_perpendicular_reshaped = m_perpendicular[:, np.newaxis] 

    # create array with 15 parrelel lines to both sides of the shiptrack
    P_all = []
    for i in range(-15, 16):
        P = P0 + i * 0.01 * m_perpendicular_reshaped
        P_all.append(P)
    P_all = np.array(P_all)

    LON = P_all[:,0,:]
    LAT = P_all[:,1,:]

    # create datarray with dimension alongtrack and crosstrack; using LON and LAT as coords
    da = xr.DataArray(
        np.zeros((P_all.shape[0], P_all.shape[2])), 
        dims=['crosstrack', 'alongtrack'], 
        coords={
            'lat': (['crosstrack', 'alongtrack'], LAT), 
            'lon': (['crosstrack', 'alongtrack'], LON)})
    
    return da
# %%
path_to_data = '/work/mh0033/m300878/crop_interpolate/smtwv/shiptracks/'
vis = False

# create xarray dataset with shiptrack coordinates
# %% A
def load_shiptracks():
# lon lat is swapped... 
# A.) 21-Mar-2023 06:07:00 to 21-Mar-2023 21:57:00
#      Start lat, long: (7.302266458322036, -32.74449971946948)
#      End lat, long: (4.80271748093342, -32.21613895152733)
 
# B.) 28-Mar-202310:45:00 to 29-Mar-202303:24:00 
#      Start lat, long: (4.54042189701239, -33.45569307629649)
#      End lat, long: (4.114471409993365, -32.10328685800346)
 
# C.) 03-Apr-202321:42:00 to 05-Apr-202307:00:00
#      Start lat, long: (3.5984812119345775, -31.65245517030427)
#      End lat, long: (6.246898709890696, -32.52339933203654)
 
# D.) 08-Apr-202309:30:00 to 09-Apr-202302:09:00
#       Start lat, long: (5.877121946088332, -32.4159390824827)
#       End lat, long: (8.027409217601079, -30.093683137253233)
    time0 = datetime.datetime(2023, 3, 21, 6, 7, 0)
    time1 = datetime.datetime(2023, 3, 21, 21, 57, 0)
    lat0  = 7.302266458322036
    lon0  = -32.74449971946948
    lat1  = 4.80271748093342
    lon1  = -32.21613895152733
    ds_a  = xr.Dataset( {'time': ('time', [time0, time1]), 'lat': ('time', [lat0, lat1]), 'lon': ('time', [lon0, lon1])})
    # % B
    time0 = datetime.datetime(2023, 3, 28, 10, 45, 0)
    time1 = datetime.datetime(2023, 3, 29, 3, 24, 0)
    lat0  = 4.54042189701239
    lon0  = -33.45569307629649
    lat1  = 4.114471409993365
    lon1  = -32.10328685800346
    ds_b  = xr.Dataset( {'time': ('time', [time0, time1]), 'lat': ('time', [lat0, lat1]), 'lon': ('time', [lon0, lon1])})
    # % C
    time0 = datetime.datetime(2023, 4, 3, 21, 42, 0)
    time1 = datetime.datetime(2023, 4, 5, 7, 0, 0)
    lat0  = 3.5984812119345775
    lon0  = -31.65245517030427
    lat1  = 6.246898709890696
    lon1  = -32.52339933203654
    ds_c  = xr.Dataset( {'time': ('time', [time0, time1]), 'lat': ('time', [lat0, lat1]), 'lon': ('time', [lon0, lon1])})
    # % D
    time0 = datetime.datetime(2023, 4, 8, 9, 30, 0)
    time1 = datetime.datetime(2023, 4, 9, 2, 9, 0)
    lat0  = 5.877121946088332
    lon0  = -32.4159390824827
    lat1  = 8.027409217601079
    lon1  = -30.093683137253233
    ds_d  = xr.Dataset( {'time': ('time', [time0, time1]), 'lat': ('time', [lat0, lat1]), 'lon': ('time', [lon0, lon1])})
    # % All concat all with abcd as different variables
    ds_tracks = xr.concat([ds_a, ds_b, ds_c, ds_d], 'shiptrack')

    # rename lat to lon and lon to lat
    ds_tracks = ds_tracks.rename_vars({'lat': 'lon', 'lon': 'lat'})
    return ds_tracks

# %% Load ICON DATA
if False:
    ds       = eva.load_smt_wave_levels(7, it=1)
    lev      = 1
else:
    ds       = eva.load_smt_wave_levels_2(7, it=1)
    lev      = 2
ds       = ds.drop_duplicates(dim='time')
ds_tgrid = eva.load_smt_wave_grid()
clon     = np.rad2deg(ds_tgrid.clon.values)
clat     = np.rad2deg(ds_tgrid.clat.values)

# %%
ds_tracks     = load_shiptracks()
lon_reg       = [ds_tracks.lon.min().values, ds_tracks.lon.max().values]
lat_reg       = [ds_tracks.lat.min().values, ds_tracks.lat.max().values]
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
ds_interp     = pyic.interp_to_rectgrid_xr(ds.isel(time=1000).isel(depth=0).to.rename(ncells_2='ncells'), lon_reg=lon_reg, lat_reg=lat_reg, fpath_ckdtree=fpath_ckdtree)

for s in range(len(ds_tracks.shiptrack)):
    time0     = ds_tracks.isel(shiptrack=s).time[2*s]
    time1     = ds_tracks.isel(shiptrack=s).time[2*s+1]
    time_mean = time0 + (time1 - time0) / 2
    print(f'Processing shiptrack {s} at time {time_mean.values}')
    time_mean = time_mean.values - pd.DateOffset(years=3)
    time_mean = np.datetime64(time_mean).astype(ds.time.dtype)

    da = create_shiptrack_mesh(ds_tracks, s)
    
    if vis == True:
        fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        ds_interp.plot(ax=ax, cmap='viridis', add_colorbar=False)
        for i in range(len(ds_tracks.shiptrack)):
            ax.plot(ds_tracks.isel(shiptrack=i).lon, ds_tracks.isel(shiptrack=i).lat, color='red')
            ax.text(ds_tracks.isel(shiptrack=i).lon[0+2*i], ds_tracks.isel(shiptrack=i).lat[0+2*i], string.ascii_uppercase[i], fontsize=20, color='black')
            ax.text(ds_tracks.isel(shiptrack=i).lon[1+2*i], ds_tracks.isel(shiptrack=i).lat[1+2*i], string.ascii_uppercase[i], fontsize=20, color='black')
        for i in range(len(da.crosstrack)):
            ax.plot(da.lon[i], da.lat[i], color='red', alpha=0.3)
        plt.savefig(path_to_data + f'shiptrack_{s}_level_2.png')

    # %   Get indicies from ICON grid
    print('Interpolating to ICON grid')
    lon_o   = da.lon.values.flatten()
    lat_o   = da.lat.values.flatten()
    ds_grid = interp_to_reg_grid(clon, clat, lon_o, lat_o, da)

    idx = ds_grid.ickdtree_c.compute().data.flatten()
    print(f'Number of unique indices: {len(np.unique(idx))}')
    print(f'Number of indices: {len(idx)}')
    # % apply indices
    ds_icon           = ds.sel(time=time_mean, method='nearest').rename(ncells_2='ncells')
    ds_icon           = ds_icon.compute()
    ds_icon_shiptrack = apply_grid(ds_icon, ds_grid)
    ds_icon_shiptrack = ds_icon_shiptrack.compute()

    # keep variables to, so, u, v, w
    ds_icon_shiptrack = ds_icon_shiptrack[['to', 'so', 'u', 'v', 'w']]

    # scatter plot
    if vis == True:
        fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        lon = ds_icon_shiptrack.lon.values.flatten()
        lat = ds_icon_shiptrack.lat.values.flatten()
        u   = ds_icon_shiptrack.isel(depth=5).to.values.flatten()
        sc = ax.scatter(lon, lat, c=u, cmap='viridis', s=10)
        # % above code with plate caree projection on projection parallel lines!!!
        fig, ax = plt.subplots(1, 1, figsize=(10, 10), subplot_kw={'projection': ccrs.PlateCarree()})
        lon = ds_icon_shiptrack.lon.values.flatten()
        lat = ds_icon_shiptrack.lat.values.flatten()
        u   = ds_icon_shiptrack.isel(depth=5).to.values.flatten()
        sc = ax.scatter(lon, lat, c=u, cmap='viridis', s=10, transform=ccrs.PlateCarree())

    # % save data
    print(f' saving shiptrack_{s}_level_{lev}')
    ds_icon_shiptrack.to_netcdf(path_to_data + f'shiptrack_{s}_level_{lev}' + '.nc')

# %%