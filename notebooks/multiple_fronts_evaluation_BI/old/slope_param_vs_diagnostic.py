# %%
# Evaluate slope of over MLD depth and front averaged profiles
# manually selected MLD, visualization of vertical structure function
# %%
import sys

#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import pandas as pd
import netCDF4 as nc
import xarray as xr    
from dask.diagnostics import ProgressBar
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload
from matplotlib.ticker import FormatStrFormatter
from scipy import stats    #Used for 2D binned statistics


# %%
grain = True
#Reload modules:
reload(eva)
reload(tools)
# %%
lon_reg_R0    = [-80.5, -55]
lat_reg_R0    = [25, 40]
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
# fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/12_fronts_modified/'
fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/12_fronts_modified/coarse_grained/'

path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
lon_reg       = lon_reg_R0
lat_reg       = lat_reg_R0
time_averaged = ''

# %%
fpath_ckdtree_r01 = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.10_180W-180E_90S-90N.nc'
grid01            = xr.open_dataset(fpath_ckdtree_r01)
grid01_sel        = grid01.sel(lon=slice(lon_reg_R0[0], lon_reg_R0[1]), lat=slice(lat_reg_R0[0],lat_reg_R0[1]))

def coarse_graining(data):
    data_interp = data.interp_like(grid01_sel, method='quadratic', assume_sorted=False, kwargs=None)
    return data_interp

# %%
#calculate MLD
path_data     = f'{path_root_dat}t{time_averaged}.nc'
t_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
path_data     = f'{path_root_dat}s{time_averaged}.nc'
s_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
data_t_mean   = pyic.interp_to_rectgrid_xr(t_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_s_mean   = pyic.interp_to_rectgrid_xr(s_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_rho_mean = gsw.rho(data_s_mean.S001_sp, data_t_mean.T001_sp, depthc[2])
data_rho_mean_depthi_ = data_rho_mean.interp(depthc=depthi)

if grain == True: data_rho_mean_depthi = coarse_graining(data_rho_mean_depthi_) 
else: data_rho_mean_depthi = data_rho_mean_depthi_


mld, mask, mldx = eva.calc_mld_xr(data_rho_mean_depthi, depthi)
mask      = mask.rename(depthc='depthi')

data_t_mean_depthi = data_t_mean.T001_sp.interp(depthc=depthi)
data_t_mean_depthi_ = data_t_mean_depthi.rename(depthc='depthi')
if grain == True: data_t_mean_depthi = coarse_graining(data_t_mean_depthi_)
else: data_t_mean_depthi = data_t_mean_depthi_
a         = data_t_mean_depthi.isel(depthi=1).where(~(data_t_mean_depthi.isel(depthi=1)==0), np.nan)
mask_land = ~np.isnan(a)
mask_land = mask_land.where(mask_land==True, np.nan)
mldx      = mldx * mask_land
mldx      = mldx.rename('mld')


#calculate Ri
path_data       = f'{path_root_dat}bx{time_averaged}.nc'
dbdx_mean       = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data       = f'{path_root_dat}by{time_averaged}.nc'
dbdy_mean       = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data       = f'{path_root_dat}n2{time_averaged}.nc'
n_mean          = xr.open_dataset(path_data, chunks=dict(depthi=1))
data_dbdx_mean_ = pyic.interp_to_rectgrid_xr(dbdx_mean.dbdx, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_dbdy_mean_ = pyic.interp_to_rectgrid_xr(dbdy_mean.dbdy, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_n2_mean_   = pyic.interp_to_rectgrid_xr(n_mean.N2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_M2_mean_   = np.sqrt(np.power(data_dbdx_mean_,2) + np.power(data_dbdy_mean_,2))

ri_mean = eva.calc_richardsonnumber(data_n2_mean_.lat, data_n2_mean_, data_dbdy_mean_)

path_data           = f'{path_root_dat}wb{time_averaged}_prime.nc'
wb_prime_mean       = xr.open_dataset(path_data, chunks=dict(depthi=1))
wb_prime_mean       = wb_prime_mean.rename(__xarray_dataarray_variable__='wb_prime_mean')
data_wb_prime_mean_ = pyic.interp_to_rectgrid_xr(wb_prime_mean.wb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week


path_data          = f'{path_root_dat}vb{time_averaged}_prime.nc'
vb_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthc=1))
vb_prime_mean      = vb_prime_mean.rename(__xarray_dataarray_variable__='vb_prime_mean')
data_vb_prime_mean = pyic.interp_to_rectgrid_xr(vb_prime_mean.vb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

data_vb_prime_mean_depthi = data_vb_prime_mean.interp(depthc=depthi)
data_vb_prime_mean_depthi_ = data_vb_prime_mean_depthi.rename(depthc='depthi')

path_data          = f'{path_root_dat}ub{time_averaged}_prime.nc'
ub_prime_mean      = xr.open_dataset(path_data, chunks=dict(depthc=1))
ub_prime_mean      = ub_prime_mean.rename(__xarray_dataarray_variable__='ub_prime_mean')
data_ub_prime_mean = pyic.interp_to_rectgrid_xr(ub_prime_mean.ub_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

data_ub_prime_mean_depthi  = data_ub_prime_mean.interp(depthc=depthi)
data_ub_prime_mean_depthi_ = data_ub_prime_mean_depthi.rename(depthc='depthi')

# %% Coarse graining >> interpolate to coarser grid: horizontal smoothing
if grain == True: 
    data_wb_prime_mean        = coarse_graining(data_wb_prime_mean_)
    data_ub_prime_mean_depthi = coarse_graining(data_ub_prime_mean_depthi_)
    data_vb_prime_mean_depthi = coarse_graining(data_vb_prime_mean_depthi_)
    data_n2_mean              = coarse_graining(data_n2_mean_)
    data_dbdx_mean            = coarse_graining(data_dbdx_mean_)
    data_dbdy_mean            = coarse_graining(data_dbdy_mean_)
    data_M2_mean              = coarse_graining(data_M2_mean_)
else:
    data_wb_prime_mean        = data_wb_prime_mean_
    data_ub_prime_mean_depthi = data_ub_prime_mean_depthi_
    data_vb_prime_mean_depthi = data_vb_prime_mean_depthi_
    data_n2_mean              = data_n2_mean_
    data_dbdx_mean            = data_dbdx_mean_
    data_dbdy_mean            = data_dbdy_mean_
    data_M2_mean              = data_M2_mean_

# %%
mask_used = mask_land # * mask
mldx_used = mldx
n2_sel    = data_n2_mean

m2_sel  = data_M2_mean
m2x_sel = data_dbdx_mean
m2y_sel = data_dbdy_mean

#### Filter
m2_min_lim = 0#1e-8

m2         = m2_sel.where(np.abs(m2_sel) > m2_min_lim, np.nan)
m2         = m2.where(~np.isinf(m2),np.nan) #infs to nans
m2_mask    = m2 * mask_used
m2_mask    = m2_mask.where(~(m2_mask == 0), np.nan) # remove zeros
m2_mask    = m2_mask.rename('m2')

m2x      = m2x_sel.where(np.abs(m2x_sel) > m2_min_lim, np.nan)
m2x      = m2x.where(~np.isinf(m2x),np.nan) #infs to nans
m2x_mask = m2x * mask_used 
m2x_mask = m2x_mask.where(~(m2x_mask == 0), np.nan) # remove zeros
m2x_mask = m2x_mask.rename('m2x')

m2y      = m2y_sel.where(np.abs(m2y_sel) > m2_min_lim, np.nan)
m2y      = m2y.where(~np.isinf(m2y),np.nan) #infs to nans
m2y_mask = m2y * mask_used 
m2y_mask = m2y_mask.where(~(m2y_mask == 0), np.nan) # remove zeros
m2y_mask = m2y_mask.rename('m2y')

# n2
n2      = n2_sel.where(~np.isinf(n2_sel),np.nan) #infs to nans
n2_mask = n2 * mask_used  #application of mask leads to new zero values
n2_mask = n2_mask.where(~(n2_mask == 0), np.nan) # remove zeros
n2_mask = n2_mask.rename('n2')

wb_mask = data_wb_prime_mean * mask_used 
wb_mask = wb_mask.where(~(wb_mask == 0), np.nan) # remove zeros
wb_mask = wb_mask.rename('wb')

vb_mask = data_vb_prime_mean_depthi * mask_used
vb_mask = vb_mask.where(~(m2_mask == 0), np.nan) # remove zeros
vb_mask = vb_mask.rename('vb')

ub_mask = data_ub_prime_mean_depthi * mask_used
ub_mask = ub_mask.where(~(m2_mask == 0), np.nan) # remove zeros
ub_mask = ub_mask.rename('ub')

# %%
reload(eva)
lon_reg_all, lat_reg_all = eva.get_new_fronts()
shape = lon_reg_all.shape

fronts = xr.IndexVariable('fronts', np.array(np.arange(shape[0])+1))
M   = []
Mx  = []
My  = []
N   = []
WB  = []
UB  = []
VB  = []
MLD = []
LAT = []
for ii in np.arange(shape[0]):
    # ii=11
    front =             f'R{ii+1}f'
    lon_front = lon_reg_all[ii,:]
    lat_front = lat_reg_all[ii,:]

    #select fronts values and average over mld - save diagnostic values m2 n2 wb and mld
    m2_diag   = m2_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
    m2x_diag  = m2x_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
    m2y_diag  = m2y_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
    n2_diag   = n2_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
    wb_diag   = wb_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
    vb_diag   = vb_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
    ub_diag   = ub_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
    mldx_diag = mldx.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))

    #average 
    m2_diag_ave  = m2_diag.mean(dim=['lat', 'lon'])
    m2x_diag_ave = m2x_diag.mean(dim=['lat', 'lon'])
    m2y_diag_ave = m2y_diag.mean(dim=['lat', 'lon'])
    n2_diag_ave  = n2_diag.mean(dim=['lat', 'lon'])
    wb_diag_ave  = wb_diag.mean(dim=['lat', 'lon'])
    vb_diag_ave  = vb_diag.mean(dim=['lat', 'lon'])
    ub_diag_ave  = ub_diag.mean(dim=['lat', 'lon'])
    mldx_ave     = mldx_diag.mean(dim=['lat', 'lon'])

    LAT.append( m2_diag.lat.mean() )
    M.append(m2_diag_ave)
    Mx.append(m2x_diag_ave)
    My.append(m2y_diag_ave)
    N.append(n2_diag_ave)
    WB.append(wb_diag_ave)
    VB.append(vb_diag_ave)
    UB.append(ub_diag_ave)
    MLD.append(mldx_ave)

    # save background state values of each front
ds_M  = xr.concat(M, dim=fronts)
ds_Mx = xr.concat(Mx, dim=fronts)
ds_My = xr.concat(My, dim=fronts)
ds_N  = xr.concat(N, dim=fronts)
ds_WB = xr.concat(WB, dim=fronts)
ds_VB = xr.concat(VB, dim=fronts)
ds_UB = xr.concat(UB, dim=fronts)
ds_mld= xr.concat(MLD, dim=fronts)
ds_lat= xr.concat(LAT, dim=fronts)

ds_F = xr.merge([ds_M, ds_Mx, ds_My, ds_N, ds_WB, ds_VB, ds_UB, ds_mld, ds_lat])

# %%
# calculate fox kemper and stone param from diagnosed backround values
reload(eva)
mld_manu = np.array([450, 400, 180, 500, 300, 400, 300, 180,  250, 480 ])
aa = ds_F
AA = []
for ii in np.arange(shape[0]):
    # AA.append(aa.isel(fronts=ii).sel(depthi=slice(0, aa.isel(fronts=ii).mld)))
    AA.append(aa.isel(fronts=ii).sel(depthi=slice(0, mld_manu[ii])))

ds_AA = xr.concat(AA, dim = fronts)

A             = ds_AA
ri_diag       = eva.calc_richardsonnumber(A.lat, A.n2, A.m2y)
ri_dmean      = ri_diag.mean(dim='depthi')
psi_diag_held = eva.calc_streamfunc_held_schneider(A.wb, A.m2x, A.m2y)
grad_b        = eva.calc_abs_grad_b(A.m2x, A.m2y, A.n2)
fcoriolis     = eva.calc_coriolis_parameter(A.lat)

psi_diag_full        = eva.calc_streamfunc_full(A.wb, A.m2y, A.vb, A.n2, grad_b)
psi_diag_traditional = eva.calc_streamfunc(A.vb, A.n2)

x       = A.mld
x[:]    = mld_manu
mld_sel = x # A.mld

# %%
def calc_sum_lsq(data_diag, data_param):
    N = data_diag.shape
    lsq = np.sum(np.power((data_diag.data - data_param.data),2)) / N
    return lsq 

def calc_psi_stone(cs):
    # compare vb and wb with parameterizations
    wb_stone_param = eva.calc_wb_stone_param(A.lat, mld_sel, A.m2y, ri_diag, cs, -A.m2y.depthi)
    vb_stone_param = eva.calc_vb_stone_param(A.lat, mld_sel, A.m2y, ri_diag, cs)
    psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param, A.m2y, vb_stone_param, A.n2, grad_b)

    return wb_stone_param, vb_stone_param, psi_stone_full_param

def calc_psi_fox(cf):
    # compare vb and wb with parameterizations
    wb_fox_param = eva.calc_wb_fox_param(A.lat, mld_sel, A.m2y, -A.m2y.depthi, cf, structure=True )
    vb_fox_param = eva.calc_vb_fox_param(A.lat, mld_sel, A.m2y, ri_diag, -A.m2y.depthi, cf, structure=True )
    psi_fox_full_param   = eva.calc_streamfunc_full(wb_fox_param, A.m2y, vb_fox_param, A.n2, grad_b)

    return wb_fox_param, vb_fox_param, psi_fox_full_param

# %%
lsq_psi_wb_vb = np.zeros([3,100])
cs_test = np.linspace(0,2,100)
for i in np.arange(100):
    cs = cs_test[i]
    wb_stone_param, vb_stone_param, psi_stone_full_param = calc_psi_stone(cs)

    lsq_stone_wb = calc_sum_lsq(A.wb.mean(dim='depthi'), wb_stone_param.mean(dim='depthi'))
    lsq_stone_vb = calc_sum_lsq(A.vb.mean(dim='depthi'), vb_stone_param.mean(dim='depthi'))
    lsq_stone_psi= calc_sum_lsq(psi_diag_full.mean(dim='depthi'), psi_stone_full_param.mean(dim='depthi'))
    # print('Stone LSQ: wb, vb, psi', lsq_stone_wb, lsq_stone_vb, lsq_stone_psi)

    lsq_psi_wb_vb[0,i] = lsq_stone_psi
    lsq_psi_wb_vb[1,i] = lsq_stone_wb
    lsq_psi_wb_vb[2,i] = lsq_stone_vb

idx0 = lsq_psi_wb_vb[0,:].argmin()
idx1 = lsq_psi_wb_vb[1,:].argmin()
idx2 = lsq_psi_wb_vb[2,:].argmin()
cs_opt0 = cs_test[idx0]
cs_opt1 = cs_test[idx1]
cs_opt2 = cs_test[idx2]
print(cs_opt0, cs_opt1, cs_opt2)
print('Remaining Error Stone', lsq_psi_wb_vb[0,idx0])
plt.plot(cs_test, lsq_psi_wb_vb[0,:])
# %%
lsq_psi_wb_vb = np.zeros([3,100])
cf_test = np.linspace(0.01,0.1,100)
for i in np.arange(100):
    cf = cf_test[i]
    wb_fox_param, vb_fox_param, psi_fox_full_param = calc_psi_fox(cf)

    lsq_fox_wb = calc_sum_lsq(A.wb.mean(dim='depthi'), wb_fox_param.mean(dim='depthi'))
    lsq_fox_vb = calc_sum_lsq(A.vb.mean(dim='depthi'), vb_fox_param.mean(dim='depthi'))
    lsq_fox_psi= calc_sum_lsq(psi_diag_full.mean(dim='depthi'), psi_fox_full_param.mean(dim='depthi'))
    # print('fox LSQ: wb, vb, psi', lsq_fox_wb, lsq_fox_vb, lsq_fox_psi)

    lsq_psi_wb_vb[0,i] = lsq_fox_psi
    lsq_psi_wb_vb[1,i] = lsq_fox_wb
    lsq_psi_wb_vb[2,i] = lsq_fox_vb

idx0 = lsq_psi_wb_vb[0,:].argmin()
idx1 = lsq_psi_wb_vb[1,:].argmin()
idx2 = lsq_psi_wb_vb[2,:].argmin()
cf_opt0 = cf_test[idx0]
cf_opt1 = cf_test[idx1]
cf_opt2 = cf_test[idx2]
print(cf_opt0, cf_opt1, cf_opt2)
print('Remaining Error Fox', lsq_psi_wb_vb[0,idx0])
plt.plot(cf_test, lsq_psi_wb_vb[0,:])

# %%
# calculate parameterization
cs      = cs_opt0
cf      = cf_opt0


# compare vb and wb with parameterizations
wb_fox_param = eva.calc_wb_fox_param(A.lat, mld_sel, A.m2y, -A.m2y.depthi, cf, structure=True )
vb_fox_param = eva.calc_vb_fox_param(A.lat, mld_sel, A.m2y, ri_diag, -A.m2y.depthi, cf, structure=True )

wb_stone_param = eva.calc_wb_stone_param(A.lat, mld_sel, A.m2y, ri_diag, cs, -A.m2y.depthi)
vb_stone_param = eva.calc_vb_stone_param(A.lat, mld_sel, A.m2y, ri_diag, cs)

psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param, A.m2y, vb_stone_param, A.n2, grad_b)
psi_fox_full_param   = eva.calc_streamfunc_full(wb_fox_param, A.m2y, vb_fox_param, A.n2, grad_b)

psi_stone_held_param = eva.calc_streamfunc_held_schneider(wb_stone_param, A.m2x,  A.m2y)
psi_fox_held_param   = eva.calc_streamfunc_held_schneider(wb_fox_param, A.m2x, A.m2y)

alpha = eva.calc_alpha(A.m2y, fcoriolis)
# %%
lhs_fox         = wb_fox_param / np.power(mld_manu,2) / np.power(fcoriolis,3) / np.power(alpha,2)
lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
lhs_stone       = wb_stone_param.transpose() / np.power(mld_manu,2) / np.power(fcoriolis,3) / np.power(alpha,2)
lhs_stone_dmean = lhs_stone.mean(dim='depthi')
lhs_diag        = A.wb.transpose() / np.power(mld_manu,2) / np.power(fcoriolis,3) / np.power(alpha,2)
lhs_diag_dmean  = lhs_diag.mean(dim='depthi')

# %%
fig = plt.figure(figsize=(5,5))
x = np.linspace(1e1,1e4,10)
# y = np.linspace(1e-7,1e-11,10)
y = np.power(x,-0.5) 
# y = np.log10(np.power(x,-0.5))

for i in ri_diag.fronts:
    plt.loglog(ri_diag.isel(fronts=i-1).transpose(), lhs_diag.isel(fronts=i-1), label=f'front:{i.data}')
    plt.loglog(ri_diag.isel(fronts=i-1).mean(dim='depthi').transpose(), lhs_diag.isel(fronts=i-1).mean(dim='depthi'), marker='^', color='black')

plt.loglog(x,y, color='black', label='slope: -0.5')
plt.legend()
plt.grid()
plt.xlabel('log(Ri)')
plt.ylabel('log (wb)')

# %%
fig = plt.figure(figsize=(5,5))
x = np.linspace(1e1,1e6,10)
# y = np.linspace(1e-7,1e-11,10)
y = 1e-7*np.power(x,-0.5) 
# y = np.log10(np.power(x,-0.5))
for i in ri_diag.fronts:
    plt.loglog(ri_diag.isel(fronts=i-1).transpose(), A.wb.isel(fronts=i-1), label=f'front:{i.data}')
    plt.loglog(ri_diag.isel(fronts=i-1).mean(dim='depthi').transpose(), A.wb.isel(fronts=i-1).mean(dim='depthi'), marker='^', color='black')

# for i in ri_diag.fronts:
#     plt.loglog(ri_diag.isel(fronts=i-1).transpose(), wb_fox_param.isel(fronts=i-1), label=f'front:{i.data}')
#     plt.loglog(ri_diag.isel(fronts=i-1).mean(dim='depthi').transpose(), wb_fox_param.isel(fronts=i-1).mean(dim='depthi'), marker='^', color='black')


plt.loglog(x,y, color='black', label='slope: -0.5')
plt.legend()
plt.grid()
plt.xlabel('log(Ri)')
plt.ylabel('log (wb)')

# %%
# %%
def calc_log10(x_data, y_data):
    mask   = ~np.isnan(x_data) & ~np.isnan(y_data) #remove nans from original data
    x_data = x_data[mask]
    y_data = y_data[mask]

    logx = np.log10(np.abs(x_data)) #not nessecary
    logy = np.log10(np.abs(y_data))
    return(logx,logy)

def plot_slope(logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone ):
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    fs = 10
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_diag
    logy = logy_diag
    ax.scatter(logx, logy , color='black', marker='x')
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'black', label=f'diag s={res.slope:.2f} intercept={res.intercept:.2}')  # type: ignore
    # ax.set_xlabel(r'$log_{10} \overline{Ri}$', fontsize=fs)
    # ax.set_ylabel(r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $', fontsize=fs)
    # ax.legend(title=f'intercept = {res.intercept:.1}')  # type: ignore

    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_fox
    logy = logy_fox
    ax.scatter(logx, logy , color='blue', marker='o')
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'blue', label=f'fox slope={res.slope:.2f} intercept={res.intercept:.2}')  # type: ignore
    # ax.set_xlabel(r'$log_{10} \overline{Ri}$', fontsize=fs)
    # ax.set_ylabel(r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $', fontsize=fs)
    # ax.legend(title=f'intercept = {res.intercept:.1}')  # type: ignore

    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_stone
    logy = logy_stone
    ax.scatter(logx, logy , color='red', marker='^')
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'red', label=f'stone slope={res.slope:.2f} intercept={res.intercept:.2}')  # type: ignore
    ax.set_xlabel(r'$log_{10} \overline{Ri}$', fontsize=fs)
    ax.set_ylabel(r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $', fontsize=fs)
    # ax.legend(title=f'intercept = {res.intercept:.2}')  # type: ignore

    # for ax in hca:
    pyic.plot_settings(ax)   # type: ignore
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    # ax.axis('equal')
    ax.grid()
    ax.legend(loc='lower left', bbox_to_anchor=(0,1.02,1, 0.2))
    plt.savefig(f'{fig_path}/param_vs_diag/correlation_1e2_manu_mld_nov22.png', dpi=150, format='png', bbox_inches='tight')

# %%
lim = 1e2
logx_diag , logy_diag  = calc_log10(ri_dmean.where(ri_dmean < lim).data,  lhs_diag_dmean.where(ri_dmean < lim).data)
logx_fox  , logy_fox   = calc_log10(ri_dmean.where(ri_dmean < lim).data,   lhs_fox_dmean.where(ri_dmean < lim).data)
logx_stone, logy_stone = calc_log10(ri_dmean.where(ri_dmean < lim).data, lhs_stone_dmean.where(ri_dmean < lim).data)
plot_slope(logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone )

# %%
fig, ax = plt.subplots(1,2, figsize=(10,5), sharey=False, squeeze=True) #16,7 for 5
# fig.suptitle(r'$ \psi $', fontsize=25)
x = np.linspace(0,10,10)
y = x
i = 0
# ax.set_title('Held Schneider')
ax[i].plot(x,y, color='grey')
lim= A.wb.max()
ax[i].set_ylim(0,lim)
ax[i].set_xlim(0,lim)
ax[i].plot(A.wb.mean(dim='depthi'), wb_fox_param.mean(dim='depthi'), 'o', color='black', label=r'$wb_{fox} $'+f' Cf={cf:.4f}')
ax[i].plot(A.wb.mean(dim='depthi'), wb_stone_param.mean(dim='depthi'), 'x', color='black', label=r'$wb_{stone} $'+f' Cs={cs:.2f}')
ax[i].set_xlabel(r'$wb_{diag}$', fontsize=25)
ax[i].set_ylabel(r'$wb_{fox}, wb_{stone}$', fontsize=25)
ax[i].legend()
ax[i].grid()


i=1
ax[i].plot(x,y, color='grey')
lim= A.vb.max()
ax[i].set_ylim(0,lim)
ax[i].set_xlim(0,lim)
ax[i].plot(A.vb.mean(dim='depthi'), vb_fox_param.mean(dim='depthi'), 'o', color='black', label=r'$vb_{fox} $'+f' Cf={cf:.4f}')
ax[i].plot(A.vb.mean(dim='depthi'), vb_stone_param.mean(dim='depthi'), 'x', color='black', label=r'$vb_{stone} $'+f' Cs={cs:.2f}')
ax[i].set_xlabel(r'$vb_{diag}$', fontsize=25)
ax[i].set_ylabel(r'$vb_{fox}, vb_{stone}$', fontsize=25)
ax[i].legend()
ax[i].grid()

plt.savefig(f'{fig_path}/param_vs_diag/wb_diag_vs_pram_cfopt_nov22.png', dpi=250, format='png', bbox_inches='tight')

# %%
fig, ax = plt.subplots(1,2, figsize=(10,5), sharey=False, squeeze=True) #16,7 for 5
# fig.suptitle(r'$ \psi $', fontsize=25)
x = np.linspace(0,10,10)
y = x
i = 0
ax[i].set_title('Held Schneider')
ax[i].plot(x,y, color='grey')
ax[i].set_ylim(0,3)
ax[i].set_xlim(0,3)
ax[i].plot(psi_diag_held.mean(dim='depthi'), psi_fox_held_param.mean(dim='depthi'), 'o', color='black', label=r'$\psi_{fox} $'+f' Cf={cf:.4f}')
ax[i].plot(psi_diag_held.mean(dim='depthi'), psi_stone_held_param.mean(dim='depthi'), 'x', color='black', label=r'$\psi_{stone}$'+f'Cs={cs:.2f}')
ax[i].set_xlabel(r'$\psi_{diag}$', fontsize=25)
ax[i].set_ylabel(r'$\psi_{fox}, \psi_{stone}$', fontsize=25)
ax[i].legend()
ax[i].grid()

i = 1
ax[i].set_title('full')
ax[i].plot(x,y, color='grey')
ax[i].set_ylim(0,9)
ax[i].set_xlim(0,9)
ax[i].plot(psi_diag_full.mean(dim='depthi'), psi_fox_full_param.mean(dim='depthi'), 'o', color='black', label=r'$\psi_{fox} $'+f' Cf={cf:.4f}')
ax[i].plot(psi_diag_full.mean(dim='depthi'), psi_stone_full_param.mean(dim='depthi'), 'x', color='black', label=r'$\psi_{stone}$'+f'Cs={cs:.2f}')
ax[i].set_xlabel(r'$\psi_{diag}$', fontsize=25)
# ax[i].set_ylabel(r'$\psi_{fox}, \psi_{stone}$', fontsize=25)
ax[i].legend()
ax[i].grid()
plt.savefig(f'{fig_path}/param_vs_diag/psi_diag_vs_pram_cfopt_manu_nov22.png', dpi=250, format='png', bbox_inches='tight')
# %%
# %%
fig, ax = plt.subplots(1,1, figsize=(10,5), sharey=False, squeeze=True) #16,7 for 5
fig.suptitle(r'$ \psi $', fontsize=25)
x = np.linspace(0,10,10)
y = x
# ax.plot(x,y, color='grey')
ax.set_ylim(0,9)
# ax.set_xlim(0,9)
ax.plot(psi_diag_held.mean(dim='depthi'), 'o', color='black', label=r'$\psi ~ diag ~held$')
ax.plot(psi_diag_full.mean(dim='depthi'), '^', color='black', label=r'$\psi  ~diag ~full$')
ax.plot(psi_stone_full_param.mean(dim='depthi'), 's', color='red', label=r'$\psi_{stone} ~param.,$'+f'Cs={cs:.2f}')
ax.plot(psi_fox_full_param.mean(dim='depthi'), 'x', color='red', label=r'$\psi_{fox} ~param., C_f=0.06 $')
# ax.set_xlabel(r'$\psi_{diag}$', fontsize=25)
ax.set_ylabel(r'$\psi$', fontsize=25)
ax.legend()
#plt.legend(fontsize=12, loc='upper right')
plt.grid()
plt.savefig(f'{fig_path}/param_vs_diag/psi_align_opt_nov22.png', dpi=250, format='png', bbox_inches='tight')

# %%

# m2y_masked = m2y.mean(dim=dim).data
for ii in np.arange(shape[0]): 
# for ii in np.arange(1): # shape[0]
#     ii  = 0
    B   = ds_F.isel(fronts=ii)
    n2  = B.n2
    wb  = B.wb
    vb  = B.vb
    ub  = B.ub
    m2  = B.m2
    m2x = B.m2x
    m2y = B.m2y
    Mld = B.mld

    ri_diag_f = eva.calc_richardsonnumber(B.lat, B.n2, B.m2y)

    fig, ax = plt.subplots(1,5, figsize=(25,12), sharey=True, squeeze=True) #16,7 for 5
    fig.suptitle(rf'Front {ii+1}', fontsize=25)
    ylim = np.array([600, 0])
    lw   = 3
    fs   = 30
    fl   = 15
    i=0
    ax[i].semilogx(n2.squeeze(), depthi, color='black', lw=lw, label=r'$N^2$')
    ax[i].axhline(Mld, color='r', ls=':', lw=lw)

    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(1e-6,5e-5)
    # ax[i].set_xlim(0,1.5e-5)
    ax[i].set_xlabel('$N^2$ ', fontsize=fs)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)
    ax[i].legend(loc='lower left', fontsize=15)
    ax2 = ax[i].twiny()
    ax2.semilogx(ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])), ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])).depthi, 'black', lw=lw, ls='dashed', label=r'$Ri=\frac{N^2 f^2}{M^4}$')
    ax2.tick_params(labelsize=fl)
    ax2.set_xlabel(r'Ri', fontsize=fs)
    plt.legend(loc='upper right', fontsize=15)

    i=1
    ax[i].plot(m2x, depthi, ls='dashed', color='black', lw=lw, label='$d_x b$')
    ax[i].plot(m2y, depthi, 'black', lw=lw, label='$d_y b$')
    ax[i].axhline(Mld, color='r', ls=':', lw=lw)
    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(-7e-8,3e-8)
    ax[i].set_xlabel('$M^2$ ', fontsize=fs)
    ax[i].legend(loc='lower left', fontsize=15)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)

    i=2
    ax[i].plot(wb, depthi, 'black', lw=lw , label = r"$\overline{w'b'}$")
    ax[i].plot(wb_fox_param.isel(fronts=ii), wb_fox_param.depthi, ls=':', color='red', lw=lw, label = r"$\overline{w'b'} (Fox)$")
    ax[i].plot(wb_stone_param.isel(fronts=ii), wb_stone_param.depthi, ls=':', color='blue', lw=lw, label = r"$\overline{w'b'} (Stone)$")
    ax[i].axhline(Mld, color='r', ls=':', lw=lw)
    ax[i].legend(loc='lower right', fontsize=15)   
    ax[i].set_ylim(ylim) # type: ignore
    # ax[i].set_xlim(-0.5e-7,2e-7)
    ax[i].set_xlabel(r"$\overline{w'b'}$ ", fontsize=fs)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)

    i = 3
    ax[i].plot(ub, depthi, color='grey', lw=lw, ls='dashed', label = r"$\overline{u'b'}$")
    ax[i].plot(vb, depthi, color='black', lw=lw, label = r"$\overline{v'b'}$")
    ax[i].plot(vb_fox_param.isel(fronts=ii), vb_fox_param.depthi, ls=':', color='red', lw=lw, label = r"$\overline{v'b'} (Fox)$")
    ax[i].plot(vb_stone_param.isel(fronts=ii), vb_stone_param.depthi, ls=':', color='blue', lw=lw, label = r"$\overline{v'b'} (Stone)$")
    ax[i].axhline(Mld, color='r', ls=':', lw=lw) 
    ax[i].set_ylim(ylim) # type: ignore
    #ax[i].set_xlim(-0.5e-7,2e-7)
    # ax[i].set_xlabel(r"$\overline{v'b'} & \overline{u'b'} $ ", fontsize=fs)
    ax[i].legend(loc='lower right', fontsize=15)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)

    i=4
    ax[i].plot(psi_diag_held.isel(fronts=ii), psi_diag_held.depthi, 'black', lw=lw, ls='dashed', label=r"$ \psi_{held} = - \frac{\overline{w'b'} * d_y b }{ (|d_x b|+|d_y b|)^2 }$")
    ax[i].plot(psi_diag_full.isel(fronts=ii), psi_diag_full.depthi, 'black', lw=lw,  label=r"$ \psi_{full} = \frac{\overline{v'b'}  N^2 - \overline{w'b'}  d_y b}{ |\nabla b|^2 }$")
    # ax[i].plot(psi_diag_traditional.isel(fronts=ii), psi_diag_traditional.depthi, 'black', ls=':', lw=lw, label=r"$ \psi = v'b' * N^2 / |N^2|^2 $")
    # ax[i].plot(psi_fox_brg.isel(fronts=ii), psi_fox_brg.depthi, 'red', lw=lw, ls='dashed', label=r'$- C_f ~ \mu_f ~ \frac{H^2 M^2}{f}, C_f=0.06$')
    # ax[i].plot(psi_stone_brg.isel(fronts=ii), psi_stone_brg.depthi, 'red', lw=lw, ls='-.', label=r'$-C_s ~\mu_s ~ \frac{H^2 M^2}{\sqrt{1+Ri}}$'+f', C_s={cs:.2f}')
    ax[i].plot(psi_fox_held_param.isel(fronts=ii), psi_fox_held_param.depthi, 'red', lw=lw, ls='dashed', label=r'$\psi_{held} (Fox)$'+f', C_f={cf:.4f}')
    ax[i].plot(psi_fox_full_param.isel(fronts=ii), psi_fox_full_param.depthi, 'red', lw=lw, label=r'$\psi_{full} (Fox)$'+f', C_f={cf:.4f}')
    ax[i].plot(psi_stone_held_param.isel(fronts=ii), psi_stone_held_param.depthi, 'blue', lw=lw, ls='dashed', label=r'$\psi_{held} (Stone)$'+f', C_s={cs:.2f}')
    ax[i].plot(psi_stone_full_param.isel(fronts=ii), psi_stone_full_param.depthi, 'blue', lw=lw,  label=r'$\psi_{full} (Stone)$'+f', C_s={cs:.2f}')
    ax[i].axhline(Mld, color='r', ls=':', lw=lw)
    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(0,8)
    # ax[i].set_xlim(-1,5)
    ax[i].set_xlabel(r"$ \psi $", fontsize=fs)
    ax[i].legend(loc='lower right', fontsize=15)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    plt.savefig(f'{fig_path}/vertical_profiles/front{ii+1}structure_func_manu_mld_nov22.png', dpi=150, format='png', bbox_inches='tight')
    # %%
