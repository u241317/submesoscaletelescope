# evaluate section of submesoscale ocean front
# %%
import sys

#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import pandas as pd
import netCDF4 as nc
import xarray as xr    
from dask.diagnostics import ProgressBar
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload


# %%
#Reload modules:
reload(eva)
reload(tools)
# %%
lon_reg_R0    = [-80.5, -55]
lat_reg_R0    = [25, 40]
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/12_fronts_modified/stream_func_fluctuation/'
path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
lon_reg       = lon_reg_R0
lat_reg       = lat_reg_R0
# %%
#load data
time_averaged = ''
path_data     = f'{path_root_dat}t{time_averaged}.nc'
t_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
# path_data     = f'{path_root_dat}s{time_averaged}.nc'
# s_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1))
data_t_mean   = pyic.interp_to_rectgrid_xr(t_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# data_s_mean   = pyic.interp_to_rectgrid_xr(s_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# data_rho_mean = gsw.rho(data_s_mean.S001_sp, data_t_mean.T001_sp, depthc[2])
# data_rho_mean_depthi = data_rho_mean.interp(depthc=depthi)

# mld, mask, mldx = eva.calc_mld_xr(data_rho_mean_depthi, depthi)
# mask      = mask.rename(depthc='depthi')

data_t_mean_depthi = data_t_mean.T001_sp.interp(depthc=depthi)
data_t_mean_depthi = data_t_mean_depthi.rename(depthc='depthi')
a         = data_t_mean_depthi.isel(depthi=1).where(~(data_t_mean_depthi.isel(depthi=1)==0), np.nan)
mask_land = ~np.isnan(a)
mask_land = mask_land.where(mask_land==True, np.nan)
# mldx      = mldx * mask_land
# mldx      = mldx.rename('mld')


#calculate Ri
path_data      = f'{path_root_dat}bx{time_averaged}.nc'
dbdx_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}by{time_averaged}.nc'
dbdy_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}n2{time_averaged}.nc'
n_mean         = xr.open_dataset(path_data, chunks=dict(depthi=1))
data_dbdx_mean = pyic.interp_to_rectgrid_xr(dbdx_mean.dbdx, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_dbdy_mean = pyic.interp_to_rectgrid_xr(dbdy_mean.dbdy, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_n2_mean   = pyic.interp_to_rectgrid_xr(n_mean.N2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_M2_mean   = np.sqrt(np.power(data_dbdx_mean,2) + np.power(data_dbdy_mean,2))
# %%
data = data_M2_mean.isel(depthi=10)
data = data * mask_land

# %%
# making a more sophisticated plot using pyicon (here projection is used):
hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, fig_size_fac=3, asp=0.5, projection=ccrs_proj, axlab_kw=None)
ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
clim=0,1e-7
pyic.shade(data.lon, data.lat, data, ax=ax, clim=clim, cax=cax, transform=ccrs_proj, rasterized=False, cmap='BuPu')

for ax in hca:
    pyic.plot_settings(ax)
# %%
