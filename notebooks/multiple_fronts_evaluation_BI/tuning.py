import numpy as np
import matplotlib.pyplot as plt
import slope_param_vs_diagnostic_inclined_funcs as hal
import xarray as xr

def calc_sum_lsq(data_diag, data_param):
    N   = data_diag.shape
    lsq = np.sum(np.power((data_diag.data - data_param.data),2)) / N
    return lsq 

def calc_sum_rel_mean_error(data_diag, data_param):
    N   = data_diag.shape
    # lsq = np.sum(np.abs((data_diag.data - data_param.data)) / np.abs(data_diag.data)) / N
    lsq = (np.abs((data_diag.data - data_param.data)) / np.abs(data_diag.data)).mean()
    return lsq 

def min_mean_error_stone(A, depthI, mask_nan, use_profile_for_param, fig_path, savefig):
    """finds tuning coefficient for minimum error for psi wb and vb"""
    lsq_psi_wb_vb = np.zeros([3,100])
    cs_test       = np.linspace(0.02,1.5,100)
    for i in np.arange(100):
        cs = cs_test[i]

        if use_profile_for_param == False: #use mean
            wb_stone_param, vb_stone_param, psi_stone_full_param = hal.calc_psi_stone_dmean(A, cs, depthI, mask_nan, A.grad_b)
            lsq_stone_psi = calc_sum_lsq(A.psi_diag_full_dmean, psi_stone_full_param.mean(dim='depthi'))
            lsq_stone_wb = calc_sum_lsq(A.wb_dmean, wb_stone_param.mean(dim='depthi'))
            lsq_stone_vb = calc_sum_lsq(A.Vb_cross_grad_dmean, vb_stone_param)
        else:
            wb_stone_param, vb_stone_param, psi_stone_full_param = hal.calc_psi_stone(A, cs, depthI, mask_nan, A.grad_b)
            lsq_stone_psi= calc_sum_lsq(A.psi_diag_full.mean(dim='depthi'), psi_stone_full_param.mean(dim='depthi'))
            lsq_stone_wb = calc_sum_lsq(A.wb.mean(dim='depthi'), wb_stone_param.mean(dim='depthi'))
            lsq_stone_vb = calc_sum_lsq(A.Vb_cross_grad.mean(dim='depthi'), vb_stone_param.mean(dim='depthi'))

        lsq_psi_wb_vb[0,i] = lsq_stone_psi
        lsq_psi_wb_vb[1,i] = lsq_stone_wb
        lsq_psi_wb_vb[2,i] = lsq_stone_vb

    idx0 = lsq_psi_wb_vb[0,:].argmin()
    idx1 = lsq_psi_wb_vb[1,:].argmin()
    idx2 = lsq_psi_wb_vb[2,:].argmin()

    cs_opt0 = cs_test[idx0]
    cs_opt1 = cs_test[idx1]
    cs_opt2 = cs_test[idx2]

    cs = np.array([cs_opt0, cs_opt1, cs_opt2])
    es = np.array([lsq_psi_wb_vb[0,idx0], lsq_psi_wb_vb[1,idx1], lsq_psi_wb_vb[2,idx2]])

    print(cs_opt0, cs_opt1, cs_opt2)
    print('Remaining Error Stone psi', lsq_psi_wb_vb[0,idx0])
    print('Remaining Error Stone wb' , lsq_psi_wb_vb[1,idx1])
    print('Remaining Error Stone vb' , lsq_psi_wb_vb[2,idx2])

    fig, ax = plt.subplots()
    ax.plot(cs_test, lsq_psi_wb_vb[0,:])
    ax.set_xlabel('tuning coefficient Stone')
    ax.set_ylabel(r'$\sum{(A_{diag}-A_{param})^2 / N}$')
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    textstr = '\n'.join((
        f'Remaining Error Stone psi {lsq_psi_wb_vb[0,idx0]:.2f}',
        f'Remaining Error Stone wb  {lsq_psi_wb_vb[1,idx1]:.2E}',
        f'Remaining Error Stone vb  {lsq_psi_wb_vb[2,idx2]:.2E}',
        f'Optimal coeff. cs for:',
        f'psi {cs_test[idx0]:.2f}',
        f'wb {cs_test[idx1]:.2f}',
        f'vb {cs_test[idx2]:.2f}'))
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=10,
            verticalalignment='top', bbox=props)
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/tuning_stone_dec22.png', dpi=100, format='png', bbox_inches='tight')

    # E_stone = lsq_psi_wb_vb[0,idx0]
    return cs, es #psi wb vb

def min_mean_error_fox(A, depthI, mask_nan, use_profile_for_param, fig_path, savefig):
    """finds tuning coefficient for minimum error for psi wb and vb"""
    lsq_psi_wb_vb = np.zeros([3,100])
    cf_test = np.linspace(0.01,0.1,100)
    for i in np.arange(100):
        cf = cf_test[i]

        if use_profile_for_param == False: #use mean
            wb_fox_param, vb_fox_param, psi_fox_full_param = hal.calc_psi_fox_dmean(A, cf, depthI, mask_nan, A.grad_b)
            lsq_fox_psi= calc_sum_lsq(A.psi_diag_full_dmean, psi_fox_full_param.mean(dim='depthi'))
            lsq_fox_wb = calc_sum_lsq(A.wb_dmean, wb_fox_param.mean(dim='depthi'))
            lsq_fox_vb = calc_sum_lsq(A.Vb_cross_grad_dmean, vb_fox_param.mean(dim='depthi'))
        else:
            wb_fox_param, vb_fox_param, psi_fox_full_param = hal.calc_psi_fox(A, cf, depthI, mask_nan, A.grad_b)
            lsq_fox_psi= calc_sum_lsq(A.psi_diag_full.mean(dim='depthi'), psi_fox_full_param.mean(dim='depthi'))
            lsq_fox_wb = calc_sum_lsq(A.wb.mean(dim='depthi'), wb_fox_param.mean(dim='depthi'))
            lsq_fox_vb = calc_sum_lsq(A.Vb_cross_grad.mean(dim='depthi'), vb_fox_param.mean(dim='depthi'))

        lsq_psi_wb_vb[0,i] = lsq_fox_psi
        lsq_psi_wb_vb[1,i] = lsq_fox_wb
        lsq_psi_wb_vb[2,i] = lsq_fox_vb

    idx0 = lsq_psi_wb_vb[0,:].argmin()
    idx1 = lsq_psi_wb_vb[1,:].argmin()
    idx2 = lsq_psi_wb_vb[2,:].argmin()

    cf_opt0 = cf_test[idx0]
    cf_opt1 = cf_test[idx1]
    cf_opt2 = cf_test[idx2]

    cf = np.array([cf_opt0, cf_opt1, cf_opt2])
    ef = np.array([lsq_psi_wb_vb[0,idx0], lsq_psi_wb_vb[1,idx1], lsq_psi_wb_vb[2,idx2]])

    print(cf_opt0, cf_opt1, cf_opt2)
    print('Remaining Error Fox psi', lsq_psi_wb_vb[0,idx0])
    print('Remaining Error Fox wb' , lsq_psi_wb_vb[1,idx1])
    print('Remaining Error Fox vb' , lsq_psi_wb_vb[2,idx2])
    
    fig, ax = plt.subplots()
    ax.plot(cf_test, lsq_psi_wb_vb[0,:])
    ax.set_xlabel('tuning coefficient Fox Kemper')
    ax.set_ylabel(r'$\sum{(A_{diag}-A_{param})^2 / N}$')
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    textstr = '\n'.join((
        f'Remaining Error Fox psi {lsq_psi_wb_vb[0,idx0]:.2f}',
        f'Remaining Error Fox wb  {lsq_psi_wb_vb[1,idx1]:.2E}',
        f'Remaining Error Fox vb  {lsq_psi_wb_vb[2,idx2]:.2E}',
        f'Optimal coeff. cf for:',
        f'psi {cf_test[idx0]:.4f}',
        f'wb {cf_test[idx1]:.4f}',
        f'vb {cf_test[idx2]:.4f}'))
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=10,
            verticalalignment='top', bbox=props)

    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/tuning_fox_dec22.png', dpi=100, format='png', bbox_inches='tight')
    # E_fox = lsq_psi_wb_vb[0,idx0]
    return cf, ef


def min_mean_rel_error_stone(A, depthI, mask_nan, use_profile_for_param, fig_path, savefig):
    """finds tuning coefficient for minimum error for psi wb and vb"""
    lsq_psi_wb_vb = np.zeros([3,100])
    cs_test       = np.linspace(0.02,1.5,100)
    for i in np.arange(100):
        cs = cs_test[i]

        wb_stone_param, vb_stone_param, psi_stone_full_param = hal.calc_psi_stone(A, cs, depthI, mask_nan, A.grad_b)
        if use_profile_for_param == False: #use mean
            lsq_stone_psi= calc_sum_rel_mean_error(A.psi_diag_full, psi_stone_full_param.mean(dim='depthi'))
            lsq_stone_wb = calc_sum_rel_mean_error(A.wb, wb_stone_param.mean(dim='depthi'))
            lsq_stone_vb = calc_sum_rel_mean_error(A.Vb_cross_grad, vb_stone_param)
        else:
            lsq_stone_psi= calc_sum_rel_mean_error(A.psi_diag_full.mean(dim='depthi'), psi_stone_full_param.mean(dim='depthi'))
            lsq_stone_wb = calc_sum_rel_mean_error(A.wb.mean(dim='depthi'), wb_stone_param.mean(dim='depthi'))
            lsq_stone_vb = calc_sum_rel_mean_error(A.Vb_cross_grad.mean(dim='depthi'), vb_stone_param.mean(dim='depthi'))

        lsq_psi_wb_vb[0,i] = lsq_stone_psi
        lsq_psi_wb_vb[1,i] = lsq_stone_wb
        lsq_psi_wb_vb[2,i] = lsq_stone_vb

    idx0 = lsq_psi_wb_vb[0,:].argmin()
    idx1 = lsq_psi_wb_vb[1,:].argmin()
    idx2 = lsq_psi_wb_vb[2,:].argmin()
    cs_opt0 = cs_test[idx0]
    cs_opt1 = cs_test[idx1]
    cs_opt2 = cs_test[idx2]

    cs = np.array([cs_opt0, cs_opt1, cs_opt2])
    es = np.array([lsq_psi_wb_vb[0,idx0], lsq_psi_wb_vb[1,idx1], lsq_psi_wb_vb[2,idx2]])

    print(cs_opt0, cs_opt1, cs_opt2)
    print('Remaining Error Stone psi', lsq_psi_wb_vb[0,idx0])
    print('Remaining Error Stone wb' , lsq_psi_wb_vb[1,idx1])
    print('Remaining Error Stone vb' , lsq_psi_wb_vb[2,idx2])
    fig, ax = plt.subplots()
    ax.plot(cs_test, lsq_psi_wb_vb[0,:])
    ax.set_xlabel('tuning coefficient Stone')
    ax.set_ylabel(r'$\sum{(A_{diag}-A_{param})^2 / N}$')
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    textstr = '\n'.join((
        f'Remaining Error Stone psi {lsq_psi_wb_vb[0,idx0]:.2f}',
        f'Remaining Error Stone wb  {lsq_psi_wb_vb[1,idx1]:.2E}',
        f'Remaining Error Stone vb  {lsq_psi_wb_vb[2,idx2]:.2E}',
        f'Optimal coeff. cs for:',
        f'psi {cs_test[idx0]:.2f}',
        f'wb {cs_test[idx1]:.2f}',
        f'vb {cs_test[idx2]:.2f}'))
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=10,
            verticalalignment='top', bbox=props)
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/tuning_stone_dec22.png', dpi=100, format='png', bbox_inches='tight')

    return cs, es #psi wb vb


def min_mean_rel_error_fox(A, depthI, mask_nan, use_profile_for_param, fig_path, savefig):
    """finds tuning coefficient for minimum error for psi wb and vb"""
    lsq_psi_wb_vb = np.zeros([3,100])
    cf_test = np.linspace(0.01,0.1,100)
    for i in np.arange(100):
        cf = cf_test[i]
        wb_fox_param, vb_fox_param, psi_fox_full_param = hal.calc_psi_fox(A, cf, depthI, mask_nan, A.grad_b)
        if use_profile_for_param == False: #use mean
            lsq_fox_psi= calc_sum_rel_mean_error(A.psi_diag_full, psi_fox_full_param.mean(dim='depthi'))
            lsq_fox_wb = calc_sum_rel_mean_error(A.wb, wb_fox_param.mean(dim='depthi'))
            lsq_fox_vb = calc_sum_rel_mean_error(A.Vb_cross_grad, vb_fox_param.mean(dim='depthi'))
        else:
            lsq_fox_psi= calc_sum_rel_mean_error(A.psi_diag_full.mean(dim='depthi'), psi_fox_full_param.mean(dim='depthi'))
            lsq_fox_wb = calc_sum_rel_mean_error(A.wb.mean(dim='depthi'), wb_fox_param.mean(dim='depthi'))
            lsq_fox_vb = calc_sum_rel_mean_error(A.Vb_cross_grad.mean(dim='depthi'), vb_fox_param.mean(dim='depthi'))

        lsq_psi_wb_vb[0,i] = lsq_fox_psi
        lsq_psi_wb_vb[1,i] = lsq_fox_wb
        lsq_psi_wb_vb[2,i] = lsq_fox_vb

    idx0 = lsq_psi_wb_vb[0,:].argmin()
    idx1 = lsq_psi_wb_vb[1,:].argmin()
    idx2 = lsq_psi_wb_vb[2,:].argmin()
    cf_opt0 = cf_test[idx0]
    cf_opt1 = cf_test[idx1]
    cf_opt2 = cf_test[idx2]

    cf = np.array([cf_opt0, cf_opt1, cf_opt2])
    ef = np.array([lsq_psi_wb_vb[0,idx0], lsq_psi_wb_vb[1,idx1], lsq_psi_wb_vb[2,idx2]])

    print(cf_opt0, cf_opt1, cf_opt2)
    print('Remaining Error Fox psi', lsq_psi_wb_vb[0,idx0])
    print('Remaining Error Fox wb' , lsq_psi_wb_vb[1,idx1])
    print('Remaining Error Fox vb' , lsq_psi_wb_vb[2,idx2])
    fig, ax = plt.subplots()
    ax.plot(cf_test, lsq_psi_wb_vb[0,:])
    ax.set_xlabel('tuning coefficient Fox Kemper')
    ax.set_ylabel(r'$\sum{(A_{diag}-A_{param})^2 / N}$')
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    textstr = '\n'.join((
        f'Remaining Error Fox psi {lsq_psi_wb_vb[0,idx0]:.2f}',
        f'Remaining Error Fox wb  {lsq_psi_wb_vb[1,idx1]:.2E}',
        f'Remaining Error Fox vb  {lsq_psi_wb_vb[2,idx2]:.2E}',
        f'Optimal coeff. cf for:',
        f'psi {cf_test[idx0]:.4f}',
        f'wb {cf_test[idx1]:.4f}',
        f'vb {cf_test[idx2]:.4f}'))
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=10,
            verticalalignment='top', bbox=props)

    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/tuning_fox_dec22.png', dpi=100, format='png', bbox_inches='tight')
    return cf, ef

def calc_tuning_each_front(A, savefig, fig_path):

    def calc_tuning_coeff_dmean(A):
        cs_wb = A.wb_dmean / A.mld**2 / A.fcoriolis**3 / A.alpha_dmean**2 * np.sqrt(1+A.ri_dmean_from_mean)
        cf_wb = A.wb_dmean / A.mld**2 / A.fcoriolis**3 / A.alpha_dmean**2 
        cs_vb = A.Vb_cross_grad_dmean / A.mld**2 / A.fcoriolis**3 / A.alpha_dmean**3 / np.sqrt(1+A.ri_dmean_from_mean) * (-5/8)
        cf_vb = A.Vb_cross_grad_dmean / A.mld**2 / A.fcoriolis**3 / A.alpha_dmean**3 / A.ri_dmean_from_mean * (-1/2)

        C = xr.Dataset()
        C['cs_wb'] = cs_wb
        C['cf_wb'] = cf_wb
        C['cs_vb'] = cs_vb
        C['cf_vb'] = cf_vb
    
        return C

    # idx_front_mld_fail = np.array([7, 11, 15, 17, 19, 21, 22, 27, 31, 40, 41, 42, 43])-1

    C = calc_tuning_coeff_dmean(A)
    # #print mean and medianof tuning coefficients
    # print('mean and median of tuning coefficients')
    # print(C.mean())
    # print(C.median())


    plt.figure()
    # l´plot in semilogy with mean and median
    plt.plot(C.fronts, C.cs_wb, color='tab:orange', ls='-', label='cs_wb')
    plt.plot(C.fronts, C.cf_wb, color='tab:blue',   ls='-', label='cf_wb')
    plt.plot(C.fronts, C.cs_vb, color='tab:orange', ls='-.', label='cs_vb')
    plt.plot(C.fronts, C.cf_vb, color='tab:blue',   ls='-.', label='cf_vb')

    #plot hline with median values
    plt.hlines(C.median().cs_wb, C.fronts.min(), C.fronts.max(), color='tab:orange', ls='--')
    plt.hlines(C.median().cf_wb, C.fronts.min(), C.fronts.max(), color='tab:blue',   ls='--')
    plt.hlines(C.median().cs_vb, C.fronts.min(), C.fronts.max(), color='tab:orange', ls=':')
    plt.hlines(C.median().cf_vb, C.fronts.min(), C.fronts.max(), color='tab:blue',   ls=':')
    plt.legend()
    plt.yscale('log')
    # plt.ylim(1e-3, 10)
    plt.grid()
    plt.xlabel('fronts')
    plt.ylabel('tuning coefficients')
    #create to boxes above figure with mean and median values
    plt.text(0.1, 1.4, f'mean cs_wb: {C.mean().cs_wb.values:.4f}', transform=plt.gca().transAxes)
    plt.text(0.1, 1.3, f'mean cf_wb: {C.mean().cf_wb.values:.4f}', transform=plt.gca().transAxes)
    plt.text(0.1, 1.2, f'mean cs_vb: {C.mean().cs_vb.values:.4f}', transform=plt.gca().transAxes)
    plt.text(0.1, 1.1, f'mean cf_vb: {C.mean().cf_vb.values:.4f}', transform=plt.gca().transAxes)

    plt.text(0.5, 1.4, f'median cs_wb: {C.median().cs_wb.values:.4f}', transform=plt.gca().transAxes)
    plt.text(0.5, 1.3, f'median cf_wb: {C.median().cf_wb.values:.4f}', transform=plt.gca().transAxes)
    plt.text(0.5, 1.2, f'median cs_vb: {C.median().cs_vb.values:.4f}', transform=plt.gca().transAxes)
    plt.text(0.5, 1.1, f'median cf_vb: {C.median().cf_vb.values:.4f}', transform=plt.gca().transAxes)
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/tuning_coefficients_indu.png', dpi=150, format='png', bbox_inches='tight')
    return C