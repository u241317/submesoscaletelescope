# %%
# Evaluate slope of over MLD depth and front averaged profiles
# manually selected MLD, visualization of vertical structure function
# %%
import sys
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import dask as da

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload
from matplotlib.ticker import FormatStrFormatter
from scipy import stats    #Used for 2D binned statistics # type: ignore
from scipy.ndimage.interpolation import rotate

import slope_param_vs_diagnostic_inclined_funcs as hal
import string
import matplotlib.gridspec as gridspec

import tuning
# %%
grain        = False
time_averaged = ''

if True:
    # remote
    fpath_ckdtree       = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
    path_root_dat       = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
    path_to_interp_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/'
else:
    # local
    fpath_ckdtree       = '/Users/mepke/smt_local_data/smt_res0.02_180W-180E_90S-90N.nc'
    path_root_dat       = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
    path_to_interp_data = '/Users/mepke/smt_local_data/NA_002dgr/'
# % load cropped and interpolated data
# %%
reload(hal)
m2_mask, m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx, mask_land =  hal.open_cropped_interp_data(path_to_interp_data)


# %%
if False:
    reload(eva)
    mld03, mask_mld03, mldx03 = eva.calc_mld_xr(rho_mask, depthi, 0.03)
    mldx03 = mldx03 * mask_land

    mld2, mask_mld2, mldx2 = eva.calc_mld_xr(rho_mask, depthi, 0.2)
    mldx2 = mldx2 * mask_land
    # %
    if False: #wb algorithm on domain
        reload(eva)
        lon_reg_R0 = [-80.5, -55]
        lat_reg_R0 = [23, 38]
        fpath_ckdtree_r01 = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.10_180W-180E_90S-90N.nc'
        grid01            = xr.open_dataset(fpath_ckdtree_r01)
        grid01_sel        = grid01.sel(lon=slice(lon_reg_R0[0], lon_reg_R0[1]), lat=slice(lat_reg_R0[0],lat_reg_R0[1]))

        data = data_wb_prime_mean * mask_land
        data_wb_prime_mean_grain = hal.coarse_graining(data, grid01_sel) 


    # %
    reload(eva)
    data = wb_mask # * mask_land
    mld_wb = eva.calc_mld_via_wb(data, depthi, 0.3)
    mld_wb = mld_wb * mask_land

    mld_wb05 = eva.calc_mld_via_wb(data, depthi, 0.05)
    mld_wb05 = mld_wb05 * mask_land
    mld_wb4 = eva.calc_mld_via_wb(data, depthi, 0.4)
    mld_wb4 = mld_wb4 * mask_land


    # % maps of wb mld
    if False:
        savefig = False
        path_mld_deBoyer = '/work/mh0033/m300878/observation/MLD/deBoyer_22/mld_dr003_ref10m.nc'
        mld_deBoyer = xr.open_dataset(path_mld_deBoyer)

        # %
        lon_reg = [-80.5, -55]
        lat_reg = [23, 38]
        fig_path = '/home/m/m300878/submesoscaletelescope/notebooks/images/first_paper/overview/'
        sys.path.insert(0, "../february23/")
        import mld_eval_funcs as mldeva
        fname  = 'MLD_intercomparison_small'
        mldeva.all_mld_wb(mldx, mld_wb, mldx03, mld_deBoyer, lon_reg, lat_reg, fig_path, fname, savefig)
        # %
        reload(mldeva)
        fname = 'wb_intercomparison'
        mldeva.wb_mld_3(mld_wb4, mld_wb, mld_wb05, lon_reg, lat_reg, fig_path, fname, savefig)


# %% Rotatete and cut out single fronts and store into dataset
reload(eva)
reload(hal)
# mldx = mldx03

# concat fronts is extremly memory intense >> use compute node if  code not adapted
ds_F =  hal.cut_rotate_merge(m2_mask, m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx, slicing=False)


# %%
#################################################################################################################
###########################################start evaluation######################################################
reload(hal)
mld_method            = 'threshold_mld'
optimize_var          = 'wb'
tuning_variable       = 'min_mean_error' # 'min_mean_rel_error'

use_profile_for_param = True #False uses depth mean values whereas True uses profiles
savefig               = False
fig_path_root         = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/inclined_fronts/'
# fig_path_root         = '/Users/mepke/Documents/PhD/git_smt_project/submesoscaletelescope/results/images/'
if use_profile_for_param == False: profile_for_path = 'param_with_mean_profile/'
else: profile_for_path = 'param_with_profile/'
fig_path              = fig_path_root+profile_for_path+f'{mld_method}/'
if grain == True: 
    fgrain = 'grained/'
    fig_path = fig_path + f'{fgrain}'


dl     = 0.1   # percentage of data cut out: +-10 % in cross direction of front
fronts = ds_F.fronts

Profiles_full, Profiles_over_mld = hal.apply_mld_method(ds_F, mld_method, dl, fronts)

# %%
#calc overturning streamfunction and richardson 
grad_b            = eva.calc_abs_grad_b(Profiles_full.m2_along_grad, Profiles_full.m2_cross_grad, Profiles_full.n2)
Profiles_full     = Profiles_full.assign(grad_b         = grad_b)
psi_diag_full     = eva.calc_streamfunc_full(Profiles_full.wb, Profiles_full.m2_cross_grad, Profiles_full.Vb_cross_grad, Profiles_full.n2, Profiles_full.grad_b)
psi_diag_full[:,0] = 0
ri_diag           = eva.calc_richardsonnumber(Profiles_full.lat, Profiles_full.n2, np.abs(Profiles_full.m2_cross_grad)) #recalculate with mean values
# add variables
Profiles_full = Profiles_full.assign(ri_diag        = ri_diag)
Profiles_full = Profiles_full.assign(psi_diag_full  = psi_diag_full)

# %%
if False:
    i=13
    plt.figure()
    Profiles_full.isel(fronts=i).Vb_cross_grad.plot(y='depthi', label='Vb_cross_grad')
    Profiles_full.isel(fronts=i).Vb_along_grad.plot(y='depthi', label='Vb_along_grad')
    # (np.abs(Profiles_full.isel(fronts=i).Vb_cross_grad) + np.abs(Profiles_full.isel(fronts=i).Vb_along_grad)).plot(y='depthi', label='Vb abs2')
    # Profiles_full.isel(fronts=i).VB_abs.plot(y='depthi', label='VB abs')
    (-1*Profiles_full.isel(fronts=i)).vb_proj.plot(y='depthi', label='vb_proj')
    plt.ylim(1500,0)
    plt.grid()  
    plt.legend()

# %% evaluation of above mld methods
reload(hal)
if False:
    mld_method = 'wb_mld'
    A_wb30, Profiles_over_mld = hal.apply_mld_method(ds_F, mld_method, dl, fronts, prozent=0.30)
    A_wb15, Profiles_over_mld = hal.apply_mld_method(ds_F, mld_method, dl, fronts, prozent=0.15)
    A_wb05, Profiles_over_mld = hal.apply_mld_method(ds_F, mld_method, dl, fronts, prozent=0.05)
    A_wb45, Profiles_over_mld = hal.apply_mld_method(ds_F, mld_method, dl, fronts, prozent=0.45)

    #%
    mld03, mask_mld03, mldx03 = eva.calc_mld_xr(rho_mask, depthi, 0.03)
    mldx03 = mldx03 * mask_land
    mld_method = 'threshold_mld'
    ds_F2 =  hal.cut_rotate_merge(m2_mask, m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx, slicing=False)
    A_threshold2, Profiles_over_mld = hal.apply_mld_method(ds_F2, mld_method, dl, fronts)
    ds_F03 =  hal.cut_rotate_merge(m2_mask, m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx03, slicing=False)
    A_threshold03, Profiles_over_mld = hal.apply_mld_method(ds_F03, mld_method, dl, fronts)

    #%
    mld_method = 'fox_mld'
    A_fox, Profiles_over_mld = hal.apply_mld_method(ds_F, mld_method, dl, fronts)

    # %
    hal.eval_mld_methods(ds_F, A_threshold03, A_fox, A_wb05, dl, savefig)
    #%
    reload(hal)
    hal.wb_mld_sensitivity(A_wb30, A_wb15, A_wb45, A_threshold03, A_threshold2, A_fox, savefig)
    # %
    reload(hal)
    hal.plot_mld_n2_wb_all(ds_F, ds_F2, ds_F03, A_threshold2, A_threshold03, A_fox, A_wb15, A_wb45, A_wb30, dl, savefig)

# %% Calculate psi and ri
def calc_weighted_mean_in_ml(ds, mask_fronts, dzt):
    start       = 0
    end         = ds.depthi.size
    dzt_i       = xr.DataArray(dzt, dims='depthi')
    dzt_i       = dzt_i.isel(depthi=slice(start, end))
    ds_weighted = ds * dzt_i.isel(depthi=slice(start, end))
    ds_mean     = ds_weighted.isel(depthi=slice(start, end)).sum(dim='depthi', skipna=True) / (dzt_i*mask_fronts).sum(dim='depthi', skipna=True)
    return ds_mean

A      = Profiles_over_mld
A_prof = Profiles_over_mld

mask_fronts = xr.DataArray(np.ones_like(A.m2), dims=['fronts', 'depthi']) * A.m2.where(np.isnan(A.m2), 1)

#replacing absolute values
A['m2']            = np.abs(A.m2_cross_grad) + np.abs(A.m2_along_grad) # difference lies in averaging!
A['VB_abs']        = np.abs(A.Vb_cross_grad) + np.abs(A.Vb_along_grad)
A['Vb_cross_grad'] = np.abs(A.Vb_cross_grad) #correction bug from rotation


fcoriolis         = eva.calc_coriolis_parameter(A.lat)
A                 = A.assign(fcoriolis      = fcoriolis)
grad_b            = eva.calc_abs_grad_b(A.m2_along_grad, A.m2_cross_grad, A.n2)
psi_diag_held     = eva.calc_streamfunc_held_schneider(A.wb, A.m2_along_grad, A.m2_cross_grad)
psi_diag_McIntyre = eva.calc_streamfunc(A.Vb_cross_grad, A.n2)
psi_diag_full     = eva.calc_streamfunc_full(A.wb, A.m2_cross_grad, A.Vb_cross_grad, A.n2, grad_b)
ri_diag           = eva.calc_richardsonnumber(A.lat, A.n2, np.abs(A.m2_cross_grad)) #recalculate with mean values
K                 = eva.calc_diapycnal_diffusivity(A.Vb_cross_grad, A.m2_cross_grad, A.wb, A.n2, grad_b)

A = A.assign(ri_diag        = ri_diag)
A = A.assign(psi_diag_held  = psi_diag_held)
A = A.assign(psi_diag_full  = psi_diag_full)
A = A.assign(psi_diag_full_dmean = calc_weighted_mean_in_ml(psi_diag_full, mask_fronts, dzt))
A = A.assign(psi_dig_n2     = psi_diag_McIntyre)
A = A.assign(grad_b         = grad_b)
A = A.assign(K              = K)

mask_nan = A.m2_cross_grad
mask_nan = mask_nan.where(np.isnan(mask_nan),1.)
depthI   = A.m2.depthi

A = A.assign(m2_cross_grad_dmean = calc_weighted_mean_in_ml(A.m2_cross_grad, mask_fronts, dzt))
A = A.assign(ri_dmean            = calc_weighted_mean_in_ml(ri_diag, mask_fronts, dzt))
A = A.assign(n2_dmean            = calc_weighted_mean_in_ml(A.n2, mask_fronts, dzt))
A = A.assign(grad_b_dmean        = calc_weighted_mean_in_ml(grad_b, mask_fronts, dzt))
A = A.assign(K_dmean             = calc_weighted_mean_in_ml(K, mask_fronts, dzt))
A = A.assign(wb_dmean            = calc_weighted_mean_in_ml(A.wb, mask_fronts, dzt))
A = A.assign(Vb_cross_grad_dmean = np.abs(calc_weighted_mean_in_ml(A.Vb_cross_grad, mask_fronts, dzt)))
# A = A.assign(vb_proj_dmean       = calc_weighted_mean_in_ml(-1*A.vb_proj, mask_fronts, dzt))
alpha                             = eva.calc_alpha( A.m2_cross_grad, A.fcoriolis)
A = A.assign(alpha                = alpha                )
A = A.assign(alpha_dmean          = calc_weighted_mean_in_ml(alpha, mask_fronts, dzt))
A = A.assign(ri_dmean_from_mean   = eva.calc_richardsonnumber(A.lat, A.n2_dmean, np.abs(A.m2_cross_grad_dmean)))

if False:
    plt.figure()
    A.ri_dmean.plot()
    A.ri_dmean_from_mean.plot()
    plt.yscale('log')




# %% remove outliers

if True:
    use_profile_for_param = False
    if False:
        print('remove outliers')
        if mld_method == 'threshold_mld':
            print('use MLD threshold_mld method') #mld already defined
            drop_fronts = np.array([16,19,20,41,43]) -1 # wrong mld threshold
            drop_fronts = np.append(drop_fronts, [10, 14, 30]) #negative psi
        elif mld_method == 'fox_mld': 
            print('use Fox Kemper MLD Algorithm')
            drop_fronts = np.array([3,4,10,15]) # fox mld wrong with cm=3
            drop_fronts = np.append(drop_fronts, [14, 30]) #negative psi
        elif mld_method == 'wb_mld':
            drop_fronts = np.array([3,10, 20]) # wb mld wrong with cm 30% to shallow #20 outliere psi param
            drop_fronts = np.append(drop_fronts, [14, 30]) #negative psi
        else:
            print('no mld method recognized')
            drop_fronts = np.nan

        A      = A.drop_isel(     fronts=drop_fronts)
        A_prof = A_prof.drop_isel(fronts=drop_fronts)

    # tuning minimize mean error
    reload(tuning)
    reload(hal)
    if tuning_variable == 'min_mean_error':
        cs, error_s = tuning.min_mean_error_stone(A, depthI, mask_nan, use_profile_for_param, fig_path, savefig)
        cf, error_f = tuning.min_mean_error_fox(  A, depthI, mask_nan, use_profile_for_param, fig_path, savefig)
    elif tuning_variable == 'min_mean_rel_error':
        cs, error_s = tuning.min_mean_rel_error_stone(A, depthI, mask_nan, use_profile_for_param, fig_path, savefig)
        cf, error_f = tuning.min_mean_rel_error_fox(  A, depthI, mask_nan, use_profile_for_param, fig_path, savefig)
    else:
        raise ValueError('tuning_variable not recognized')
else:
    print('use old parameters')
    cs = np.array([0.33, 0.33, 0.33])
    cf = np.array([0.06, 0.06, 0.06])

#optimize for:
if optimize_var == 'psi':
    cs      = cs[0] #optimize for psi
    cf      = cf[0]
elif optimize_var == 'wb':
    cs      = cs[1] #optimize for wb
    cf      = cf[1]
elif optimize_var == 'vb':
    cs      = cs[2] #optimize for vb
    cf      = cf[2]
else:
    raise ValueError('optimize_var not recognized')

A = A.assign(cs = cs)
A = A.assign(cf = cf)
#%% alternative
if True:
    reload(eva)
    reload(tuning)
    C = tuning.calc_tuning_each_front(A, savefig, fig_path)

    # cs = (C.cs_wb.median(dim='fronts').values + C.cs_vb.median(dim='fronts').values) /2
    # cf = (C.cf_wb.median(dim='fronts').values + C.cf_vb.median(dim='fronts').values) /2

    A = A.assign(cs_ind = C.cs_wb)
    A = A.assign(cf_ind = C.cf_wb)

    # wb_fox_param         = mask_nan * eva.calc_wb_fox_param(A.lat, A.mld,  A.m2_cross_grad_dmean, -depthI, C.cf_vb, structure=True )
    # vb_fox_param         = mask_nan * eva.calc_vb_fox_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, -depthI, C.cf_vb, structure=True )
    # wb_stone_param       = mask_nan * eva.calc_wb_stone_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, C.cs_vb, -depthI, structure=True)
    # vb_stone_param_dmean = eva.calc_vb_stone_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, C.cs_vb)
#########################################################################
#########################################################################
# %% 
# calculate parameterization

wb_fox_param         = mask_nan * eva.calc_wb_fox_param(A.lat, A.mld,  A.m2_cross_grad_dmean, -depthI, A.cf, structure=True )
vb_fox_param         = mask_nan * eva.calc_vb_fox_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, -depthI, A.cf, structure=True )
wb_stone_param       = mask_nan * eva.calc_wb_stone_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, A.cs, -depthI, structure=True)
# vb_stone_param_dmean = eva.calc_vb_stone_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, cs)

#%
vb_stone_param_dmean = eva.calc_vb_stone_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, A.cs)
psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param,  A.m2_cross_grad, vb_stone_param_dmean, A.n2, A.grad_b)
psi_fox_full_param   = eva.calc_streamfunc_full(wb_fox_param,  A.m2_cross_grad, vb_fox_param, A.n2, A.grad_b)
psi_stone_held_param = eva.calc_streamfunc_held_schneider(wb_stone_param, A.m2_along_grad,   A.m2_cross_grad)
psi_fox_held_param   = eva.calc_streamfunc_held_schneider(wb_fox_param, A.m2_along_grad,  A.m2_cross_grad)
K_fox                = eva.calc_diapycnal_diffusivity(vb_fox_param, A.m2_cross_grad, wb_fox_param, A.n2, grad_b)
K_stone              = eva.calc_diapycnal_diffusivity(vb_stone_param_dmean, A.m2_cross_grad, wb_stone_param, A.n2, grad_b)

A = A.assign(wb_fox_param         = wb_fox_param         )
A = A.assign(vb_fox_param         = vb_fox_param         )
A = A.assign(wb_stone_param       = wb_stone_param       )
A = A.assign(vb_stone_param_dmean = vb_stone_param_dmean )
A = A.assign(psi_stone_full_param = psi_stone_full_param )
A = A.assign(psi_fox_full_param   = psi_fox_full_param   )
A = A.assign(psi_stone_held_param = psi_stone_held_param )
A = A.assign(psi_fox_held_param   = psi_fox_held_param   )
A = A.assign(K_fox                = K_fox                )  
A = A.assign(K_stone              = K_stone              )

A = A.assign(wb_fox_param_dmean   = eva.calc_wb_fox_param(A.lat, A.mld,  A.m2_cross_grad_dmean, -depthI, cf, structure=False ))
A = A.assign(vb_fox_param_dmean   = eva.calc_vb_fox_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, -depthI, A.cf, structure=False))
A = A.assign(wb_stone_param_dmean = eva.calc_wb_stone_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, A.cs, -depthI, structure=False))
# for front in A.fronts:
#     A['vb_stone_param']       = A['vb_stone_param_dmean'].isel(fronts=front).expand_dims(dim={'depthi': mask_nan.isel(fronts=front) * A.wb_stone_param.depthi})

# %% Slopes  wb Ri old  
if use_profile_for_param==False:
    reload(hal)
    # hal.eval_single_fronts_slope_ri(A, A.ri_dmean, optimize_var, use_profile_for_param, fig_path, savefig)
    # % Evaluation like Spindown Scenario
    # hal.eval_single_fronts_slope_ri_spin(A, A.ri_dmean, optimize_var, use_profile_for_param, fig_path, savefig)
    # % alpha evaluation 
    # hal.eval_single_front_slope_alpha(A, optimize_var, use_profile_for_param, fig_path, savefig)
# %%
reload(hal)
# hal.eval_single_fronts_slope_ri_paper_n(A, optimize_var, use_profile_for_param, fig_path, savefig)
# hal.eval_single_front_slope_alpha_paper_n(A, optimize_var, use_profile_for_param, fig_path, savefig)
hal.eval_single_fronts_combined(A, optimize_var, use_profile_for_param, fig_path, savefig)

# %%
reload(hal)
A_drop = A#.drop_isel(fronts=(np.array([12,21, 31, 35,38])-1)) #drop large Ri number

if False:
    hal.plot_param_vs_diag_v_single_error(A, A_prof, Profiles_over_mld, cf ,cs, fig_path, savefig, use_profile_for_param, error_s[0], error_f[0])
    hal.plot_param_vs_diag_v_single(A, A_prof, Profiles_over_mld, cf ,cs, fig_path, savefig, use_profile_for_param, error_s[0], error_f[0])
    hal.plot_param_vs_diag_v_single_error_ri(A, A_prof, Profiles_over_mld, cf ,cs, fig_path, savefig, use_profile_for_param, error_s[0], error_f[0])
    hal.plot_param_vs_diag_v_single_log(A, A_prof, Profiles_over_mld, cf ,cs, fig_path, savefig, use_profile_for_param, error_s[0], error_f[0], optimize_var)
    hal.plot_param_vs_diag_v(A, A_prof, Profiles_over_mld, cf ,cs, fig_path, savefig, use_profile_for_param)
    hal.plot_param_vs_diag_v_logarithmic2(A, A_prof, Profiles_over_mld, cf ,cs, fig_path, savefig, use_profile_for_param)
hal.plot_param_vs_diag_v_logarithmic(A_drop, fig_path, savefig, optimize_var)
# %%
reload(hal)
data = A.mean(dim='depthi', skipna=True)
hal.plot_histograms(data, fig_path, savefig)

# %%
reload(hal)
hal.plot_1d_hist(A, fig_path, savefig)
# %%
if False:
    hist, xedges, yedges = np.histogram2d(A.m2, A.n2, bins=20)
    plt.pcolormesh(xedges, yedges, hist.T, cmap='Spectral_r')
    plt.colorbar()

    # %%
    # plot 2d histograms
    data = ds_F
    # data = data.isel(fronts=0)
    data = data.mean(dim='crossfront', skipna=True)
    data = data.mean(dim='alongfront', skipna=True)
    data = data.mean(dim='depthi', skipna=True)

    # %%
    m2_data = data.m2.data.flatten() 
    n2_data = data.n2.data.flatten()
    mask = ~np.isnan(m2_data) & ~np.isnan(n2_data) & (m2_data > 0) & (n2_data > 1e-8)

    #%%
    print(len(m2_data[mask]))
    # apply 2d histogram
    hist, xedges, yedges = np.histogram2d(m2_data[mask], n2_data[mask], bins=5000)
    # drop bins with less 10
    hist[hist < 2] = np.nan
    #
    # plt.hist2d(m2_data[mask], n2_data[mask], bins=1000, cmap='Greys')
    plt.pcolormesh(xedges, yedges, hist.T, cmap='Spectral_r', norm=colors.LogNorm())
    plt.colorbar()
    plt.xlabel('$M^2$')
    plt.ylabel('$N^2$')
    plt.xlim(min(m2_data[mask]), 1e-7)  # Adjust limits to avoid log(0) issues
    # plt.ylim(min(n2_data[mask]), max(n2_data[mask])) 
    plt.xscale('log')
    plt.yscale('log')


    #%%
    plt.hist2d(A.m2, A.n2, bins=20, cmap='viridis')
    plt.colorbar()
    plt.xlabel('$M^2$')
    plt.ylabel('$Ri$')
    # %%
    plt.hist2d( A.ri_dmean, A.wb, bins=20, cmap='viridis')
    plt.colorbar()
    plt.ylabel('$wb$')
    plt.xlabel('$Ri$')

    # %%
    plt.hist2d(A.alpha, A.ri_dmean, bins=15, cmap='viridis')
    plt.colorbar()
    plt.xlabel('$alpha$')
    plt.ylabel('$Ri$')

    # %%
    cb = plt.scatter(A.lat, A.mld, c=A.n2, cmap='Spectral_r', s=50)
    plt.xlabel('$latitude$')
    plt.ylabel('$mld$')
    plt.colorbar(cb, label='N2')
    if savefig == True: plt.savefig(fig_path+'simple_dependences'+'lat_mld_wb.png')

# %%
reload(hal)
# MLD is a horizpontal average, which can be in between interface, which explains the gap between mld line and data points
# for ii in np.arange(shape): 
for ii in np.arange(1): # shape
    ii  = 3
    # hal.plot_profiles(Profiles_full, ii, mask_nan, use_profile_for_param, A, wb_fox_param, wb_stone_param, vb_fox_param, psi_fox_held_param, psi_fox_full_param, psi_stone_held_param, psi_stone_full_param, cf, cs,  fig_path, savefig)
    # hal.plot_profiles_poster(Profiles_full, ii, mask_nan, use_profile_for_param, A, wb_fox_param, wb_stone_param, vb_fox_param, psi_fox_held_param, psi_fox_full_param, psi_stone_held_param, psi_stone_full_param, cf, cs,  fig_path, savefig)
    # hal.plot_profiles_poster_osm(Profiles_full, ii, mask_nan, use_profile_for_param, A, wb_fox_param, wb_stone_param, vb_fox_param, psi_fox_held_param, psi_fox_full_param, psi_stone_held_param, psi_stone_full_param, cf, cs,  fig_path, savefig)

    hal.plot_profiles_poster_clean(Profiles_full, ii, A, fig_path, savefig)

# %%
# A = A.assign(alpha_mean        = A.alpha.mean(dim='depthi', skipna=True))
# Profiles_full = Profiles_full.assign( alpha_mean = A.alpha.mean(dim='depthi', skipna=True))

#sort Profiles by lat
# Profiles_full_sorted = Profiles_full.sortby('lat', ascending=False)
# A_sorted = A.sortby('lat', ascending=False)


reload(hal)
fig = plt.figure(figsize=(25*3, 12*8))
outer_grid = gridspec.GridSpec(8, 3, wspace=0.15, hspace=0.4)

part = 'part1'

for i in range(24):
    inner_grid = gridspec.GridSpecFromSubplotSpec(1, 5, subplot_spec=outer_grid[i], wspace=0.2)
    if part == 'part1':
        f = i
    elif part == 'part2':
        f = i+24
        if f > 44: continue

    fig = hal.plot_subprofiles_clean(fig, inner_grid, f, Profiles_full, A)
    # if i == 3: break
if savefig==True: fig.savefig(fig_path+f'/vertical_profiles/all/new_profiles_{part}_vert_c_vb_ind.png', dpi=150, format='png', bbox_inches='tight')


   # %% eval streamfunction
reload(hal)
i     = 8
C     = ds_F.isel(fronts=i).mean(dim=["alongfront"], skipna=True)
C     = C.dropna(dim='crossfront', how='all')
s     = C.m2.crossfront.shape[0]
# %%
idl   = int(s/2 -s*dl), int(s/2+s*dl)
ylim  = 500,                0
front = f'F{i+1}'
title = rf"{front}"
path  = f'{fig_path}streamfunction/{front}_stream_func_full__oct22.png'
hal.plot_slice_front_psi_full(x=np.arange(C.m2.shape[1]), y=C.m2.depthi, wb=C.wb, vb= C.Vb_cross_grad, mldx=C.mld, 
    mldx_monte = C.mld, rho      = C.rho, m2x     = C.m2_along_grad, m2y = C.m2_cross_grad, n2 = C.n2, lat_mean = C.lat.data, dl = idl, cf_opt0=cf, cs_opt0=cs, grain=grain, ylim = ylim,
    title      = title, fig_path = path,  savefig = savefig)

path  = f'{fig_path}streamfunction/{front}_stream_func_full_held_oct22.png'
hal.plot_slice_front_psi_full_held(x=np.arange(C.m2.shape[1]), y=C.m2.depthi, wb=C.wb, vb= C.Vb_cross_grad, mldx=C.mld, 
    mldx_monte = C.mld, rho      = C.rho, m2x     = C.m2_along_grad, m2y = C.m2_cross_grad, n2 = C.n2, lat_mean = C.lat.data, dl = idl, cf_opt0=cf, cs_opt0=cs, grain=grain, ylim = ylim,
    title      = title, fig_path = path,  savefig = savefig)

# %%
grad_b = eva.calc_abs_grad_b(C.m2_along_grad, C.m2_cross_grad, C.n2)
C      = C.assign(grad_b = grad_b)
dist   = C.crossfront*0.02*eva.convert_degree_to_meter(int(C.lat.values))/1000
C      = C.assign(dist = dist)

psi_full = eva.calc_streamfunc_full(C.wb, C.m2_cross_grad, C.Vb_cross_grad, C.n2, C.grad_b)
psi_held = eva.calc_streamfunc_held_schneider(C.wb, C.m2_along_grad, C.m2_cross_grad)
psi_n2   = eva.calc_streamfunc(C.Vb_cross_grad, C.n2)
x = C.dist
y = psi_full.depthi
levels = np.linspace(-10,10, 15)

clim=6

hca, hcb = pyic.arrange_axes(3, 1, plot_cb='right', asp=0.7, fig_size_fac=1, sharey=True)
ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
pyic.shade(x, y, psi_full, ax, cax, clim=clim, cmap='PiYG_r', extend='both', contfs=True)
ax.set_ylabel('depth [m]')
ax.set_title(r'$\frac{\overline{\hat{v}^\prime b^\prime} N^2 -  \overline{w^\prime b^\prime} M^2 }{|\nabla \mathbf{\overline{b}}|^2}$')

ii+=1; ax=hca[ii]; cax=hcb[ii]
pyic.shade(x, y, psi_held, ax, cax, clim=clim, cmap='PiYG_r', extend='both', contfs=True)
ax.set_title(r'$\frac{\overline{w^\prime b^\prime}}{ \overline{M^2}}$')

ii+=1; ax=hca[ii]; cax=hcb[ii]
pyic.shade(x, y, psi_n2, ax, cax, clim=clim, cmap='PiYG_r', extend='both', contfs=True)
cax.set_ylabel('$\psi$ [m$^2$/s]')
ax.set_title(r'$\frac{\overline{\hat{v}^\prime b^\prime}}{\overline{N^2}}$')

levels_iso = np.linspace(1025,1027,120)
for ax in hca:
    pyic.plot_settings(ax)   # type: ignore
    ax.set_xlabel('$\hat{x}$ [km]')
    ax.set_ylim(400, 0)
    ax.set_xlim(40, 160)
    ax.contour(x, y, C.rho, levels_iso, colors='gray', label='rho contour', linewidths=0.5)
    ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
    ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
    ax.plot(x, C.mld, color='tab:green', linewidth=2, linestyle='--', label=r'MLD $\rho=0.2$')

if savefig == True: plt.savefig(fig_path+f'streamfunction/psi_comparison_f{i}.png', dpi=300, bbox_inches='tight')

# %%
if False:
    lon_reg_3 = np.array([-63.9, -61.5])
    lat_reg_3 = np.array([34, 35.875])

    lon_reg_2 = np.array([-70, -54])
    lat_reg_2 = np.array([23.5, 36])

    asp3 = (lat_reg_3[1]-lat_reg_3[0])/(lon_reg_3[1]-lon_reg_3[0])
    asp2 = (lat_reg_2[1]-lat_reg_2[0])/(lon_reg_2[1]-lon_reg_2[0])
    print('asp2', asp2, 'asp3',asp3)
    path_data   = f'{path_root_dat}bx{time_averaged}.nc'
    dbdx_mean   = xr.open_dataset(path_data, chunks=dict(depthi=1))
    path_data   = f'{path_root_dat}by{time_averaged}.nc'
    dbdy_mean   = xr.open_dataset(path_data, chunks=dict(depthi=1))
    idepth      = 17
    m2_mean     = np.abs(dbdx_mean.dbdx.isel(depthi=idepth)) + np.abs(dbdy_mean.dbdy.isel(depthi=17))
    data_coarse = pyic.interp_to_rectgrid_xr(m2_mean, fpath_ckdtree, lon_reg=lon_reg_3, lat_reg=lat_reg_3)
    data_coarse = m2_mask.isel(depthi=17)
    m2_min_lim = 3e-8
    if m2_min_lim != 0: data_coarse = data_coarse.where(np.abs(data_coarse) > m2_min_lim, np.nan)

# %% select slice
ii       = 17
ds_slice = ds_F.isel(fronts=ii).dropna(dim='crossfront', how='all').dropna(dim='alongfront', how='all')
ds_slice = ds_slice.mean(dim='alongfront', skipna=True)
# ds_slice = ds_slice.isel(alongfront=5)
# lat      = ds_slice.lat
# mld      = ds_slice.mld
# rho     = ds_slice.rho
# ds_slice = ds_slice.where(ds_slice.wb >=0, 0)
# ds_slice['lat'] = lat
# ds_slice['mld'] = mld
# ds_slice['rho'] = rho
# %
reload(hal)

ri    = ds_slice.n2 * np.power(eva.calc_coriolis_parameter(ds_slice.lat),2) / np.power(ds_slice.m2_cross_grad,2)
dl    = 0.055
ylim  = np.array([500,0])
title = 'test'
path  = f'{fig_path}/slice/front{ii+1}_crosssection.png'

dist = ds_slice.crossfront*0.02*eva.convert_degree_to_meter(int(ds_slice.lat.values))/1000
# %%
reload(hal)
hal.plot_slice_front_wide_pyic(x=dist, y=ds_slice.depthi, 
            vb=ds_slice.Vb_cross_grad, wb=ds_slice.wb,  mldx=ds_slice.mld, 
            rho=ds_slice.rho, m2x=ds_slice.m2_along_grad, m2y=ds_slice.m2_cross_grad,
            n2=ds_slice.n2, lat =ds_slice.lat, data_coarse=None, dl=dl, ylim=ylim,
            fig_path=path, savefig=savefig)

#%%
reload(hal)
fig_path = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/inclined_fronts/'
path  = f'{fig_path}/diagnostic/front{ii+1}_crosssection_narrow.png'

ylim= np.array([400,0])
hal.plot_slice_front_wide_pyic_2(x=dist, y=ds_slice.depthi, 
            vb=ds_slice.Vb_cross_grad, wb=ds_slice.wb,  mldx=ds_slice.mld, 
            rho=ds_slice.rho, m2x=ds_slice.m2_along_grad, m2y=ds_slice.m2_cross_grad,
            n2=ds_slice.n2, lat =ds_slice.lat, data_coarse=None, dl=dl, ylim=ylim,
            fig_path=path, savefig=savefig)

print(eva.calc_coriolis_parameter(ds_slice.lat).data)
# %%
reload(hal)
path  = f'{fig_path}/slice/front{ii+1}_crosssection_eddie.png'
hal.plot_slice_front_wide_pyic_2_hor(x=dist, y=ds_slice.depthi, 
            vb=ds_slice.Vb_cross_grad, wb=ds_slice.wb,  mldx=ds_slice.mld, 
            rho=ds_slice.rho, m2x=ds_slice.m2_along_grad, m2y=ds_slice.m2_cross_grad,
            n2=ds_slice.n2, lat =ds_slice.lat, data_coarse=None, dl=dl, ylim=ylim,
            fig_path=path, savefig=savefig)


# %% plot overturning
reload(hal)
path  = f'{fig_path}/slice/overturning_single.png'

hal.plot_overturning(x=ds_slice.crossfront, y=ds_slice.depthi, 
            vb=ds_slice.Vb_cross_grad, wb=ds_slice.wb,  mldx=ds_slice.mld, 
            rho=ds_slice.rho, m2x=ds_slice.m2_along_grad, m2y=ds_slice.m2_cross_grad,
            n2=ds_slice.n2, lat =ds_slice.lat, data_coarse=None, dl=dl, ylim=ylim,
            fig_path=path, savefig=savefig)

# %% plot overturning
reload(hal)
path  = f'{fig_path}/slice/overturning_comp.png'

hal.plot_overturning_comp(x=ds_slice.crossfront, y=ds_slice.depthi, 
            vb=ds_slice.Vb_cross_grad.data, wb=ds_slice.wb,  mldx=ds_slice.mld, 
            rho=ds_slice.rho, m2x=ds_slice.m2_along_grad, m2y=ds_slice.m2_cross_grad,
            n2=ds_slice.n2, lat =ds_slice.lat, data_coarse=None, dl=dl, ylim=ylim,
            fig_path=path, savefig=savefig)

# %%
reload(hal)

path  = f'{fig_path}/slice/overturning_contribution.png'

hal.plot_overturning_contribution(x=ds_slice.crossfront, y=ds_slice.depthi, 
            vb=ds_slice.Vb_cross_grad.data, wb=ds_slice.wb,  mldx=ds_slice.mld, 
            rho=ds_slice.rho, m2x=ds_slice.m2_along_grad, m2y=ds_slice.m2_cross_grad,
            n2=ds_slice.n2, lat =ds_slice.lat, data_coarse=None, dl=dl, ylim=ylim,
            fig_path=path, savefig=savefig)


# %%
path = f'{fig_path}/slice/front_cut_map{ii+1}.png'
hal.plot_slice_front_wide_map(x=ds_slice.crossfront, y=ds_slice.depthi, alpha=ds_slice.m2_cross_grad,
            vb=ds_slice.Vb_cross_grad, wb=ds_slice.wb,  mldx=ds_slice.mld, 
            rho=ds_slice.rho, m2x=ds_slice.m2_along_grad, m2y=ds_slice.m2_cross_grad,
            n2=ds_slice.n2, data_coarse=None, dl=dl, ylim=ylim,
            fig_path=path, savefig=False)

# %%
reload(hal)
reload(eva)
ylim = np.array([300,0])
path = f'{fig_path}/slice/eddie_{ii+1}_fs.png'
dist = ds_slice.crossfront*0.02*eva.convert_degree_to_meter(int(ds_slice.lat.values))/1000
hal.plot_slice_eddie(x=dist, y=ds_slice.depthi, 
            vb=ds_slice.Vb_cross_grad, wb=ds_slice.wb,  mldx=ds_slice.mld,
            rho=ds_slice.rho, m2x=ds_slice.m2_along_grad, m2y=ds_slice.m2_cross_grad,
            n2=ds_slice.n2, lat =ds_slice.lat, ylim=ylim,
            fig_path=path, savefig=savefig)


    # %%
