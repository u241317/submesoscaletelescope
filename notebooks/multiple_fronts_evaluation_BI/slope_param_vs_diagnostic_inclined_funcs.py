import sys
import string
#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import dask as da

import pandas as pd
import netCDF4 as nc
import xarray as xr    
#from dask.diagnostics import ProgressBar
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload
from matplotlib.ticker import FormatStrFormatter
from scipy import stats    #Used for 2D binned statistics
from scipy.ndimage.interpolation import rotate

def open_cropped_interp_data(path_to_interp_data):
    data_wb_prime_mean        = xr.open_dataarray(f'{path_to_interp_data}/data_wb_prime_mean.nc')
    data_ub_prime_mean_depthi = xr.open_dataarray(f'{path_to_interp_data}/data_ub_prime_mean_depthi.nc')
    data_vb_prime_mean_depthi = xr.open_dataarray(f'{path_to_interp_data}/data_vb_prime_mean_depthi.nc')
    data_n2_mean              = xr.open_dataarray(f'{path_to_interp_data}/data_n2_mean.nc')
    data_dbdx_mean            = xr.open_dataarray(f'{path_to_interp_data}/data_dbdx_mean.nc')
    data_dbdy_mean            = xr.open_dataarray(f'{path_to_interp_data}/data_dbdy_mean.nc')
    data_M2_mean              = xr.open_dataarray(f'{path_to_interp_data}/data_M2_mean.nc')
    data_rho_mean_depthi      = xr.open_dataarray(f'{path_to_interp_data}/data_rho_mean_depthi.nc')
    mask_land                 = xr.open_dataarray(f'{path_to_interp_data}/mask_land.nc')
    mldx                      = xr.open_dataarray(f'{path_to_interp_data}/mldx.nc')

    n2_sel    = data_n2_mean
    m2        = data_M2_mean
    m2x       = data_dbdx_mean
    m2y       = data_dbdy_mean
    rho       = data_rho_mean_depthi

    #### Filter
    # m2_min_lim = 0

    # m2      = m2_sel.where(np.abs(m2_sel) > m2_min_lim, np.nan)
    m2      = m2.where(~np.isinf(m2),np.nan) #infs to nans
    m2_mask = m2 * mask_land
    # m2_mask = m2_mask.where(~(m2_mask == 0), np.nan) # remove zeros
    m2_mask = m2_mask.rename('m2')

    # m2x      = m2x_sel.where(np.abs(m2x_sel) > m2_min_lim, np.nan)
    m2x      = m2x.where(~np.isinf(m2x),np.nan) #infs to nans
    m2x_mask = m2x * mask_land 
    # m2x_mask = m2x_mask.where(~(m2x_mask == 0), np.nan) # remove zeros
    m2x_mask = m2x_mask.rename('m2x')

    # m2y      = m2y_sel.where(np.abs(m2y_sel) > m2_min_lim, np.nan)
    m2y      = m2y.where(~np.isinf(m2y),np.nan) #infs to nans
    m2y_mask = m2y * mask_land 
    # m2y_mask = m2y_mask.where(~(m2y_mask == 0), np.nan) # remove zeros
    m2y_mask = m2y_mask.rename('m2y')

    n2      = n2_sel.where(~np.isinf(n2_sel),np.nan) #infs to nans
    n2_mask = n2 * mask_land  #application of mask leads to new zero values
    # n2_mask = n2_mask.where(~(n2_mask == 0), np.nan) # remove zeros
    n2_mask = n2_mask.rename('n2')

    wb_mask = data_wb_prime_mean * mask_land 
    # wb_mask = wb_mask.where(~(wb_mask == 0), np.nan) # remove zeros
    wb_mask = wb_mask.rename('wb')

    vb_mask = data_vb_prime_mean_depthi * mask_land
    # vb_mask = vb_mask.where(~(m2_mask == 0), np.nan) # remove zeros
    vb_mask = vb_mask.rename('vb')

    ub_mask = data_ub_prime_mean_depthi * mask_land
    # ub_mask = ub_mask.where(~(m2_mask == 0), np.nan) # remove zeros
    ub_mask = ub_mask.rename('ub')

    rho_mask = rho * mask_land
    # rho_mask = rho_mask.where(~(m2_mask == 0), np.nan) # remove zeros
    rho_mask = rho_mask.rename('rho')

    #extrapolate to surface
    m2_mask  = extrapol_surf_layer(m2_mask, depthi) * mask_land
    m2x_mask = extrapol_surf_layer(m2x_mask, depthi) * mask_land
    m2y_mask = extrapol_surf_layer(m2y_mask, depthi) * mask_land
    n2_mask  = extrapol_surf_layer(n2_mask, depthi) * mask_land
    wb_mask  = extrapol_surf_layer(wb_mask, depthi) * mask_land
    rho_mask = extrapol_surf_layer(rho_mask, depthi) * mask_land
    vb_mask  = extrapol_2surf_layers(vb_mask, depthi) * mask_land
    ub_mask  = extrapol_2surf_layers(ub_mask, depthi) * mask_land


    return m2_mask, m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx, mask_land

def extrapol_surf_layer(ds, depthi):
    ds[0,:,:] = ds[1,:,:] - (ds[2,:,:] - ds[1,:,:])/(depthi[2]-depthi[1]) * (depthi[1]-depthi[0])
    return ds

def extrapol_2surf_layers(ds, depthi):
    ds[1,:,:] = ds[2,:,:] - (ds[3,:,:] - ds[2,:,:])/(depthi[3]-depthi[2]) * (depthi[2]-depthi[1])
    ds[0,:,:] = ds[1,:,:] - (ds[2,:,:] - ds[1,:,:])/(depthi[2]-depthi[1]) * (depthi[1]-depthi[0])
    return ds


def apply_mld_method(ds_F, mld_method, dl, fronts, prozent=0.3):
    # calculate fox kemper and stone param from diagnosed backround values
    profiles      = []
    profiles_full = []
    for ii in np.arange(ds_F.fronts.shape[0]):
        Data = ds_F.isel(fronts=ii).dropna(dim='crossfront', how='all').dropna(dim='alongfront', how='all')

        s    = Data.m2.crossfront.shape[0]
        dat  = Data.isel(crossfront=slice(int(s/2)-int(s*dl), int(s/2)+int(s*dl))) #cut out ocean front

        if False: # calculate tuning variables from Ri filtered data bevor averaging
            data        = dat
            mld_restore = data.mean(dim=["crossfront", "alongfront"], skipna=True).mld
            lat_front   = data.lat
            ri_diag     = eva.calc_richardsonnumber(data.lat.data, data.n2, np.abs(data.m2_cross_grad)) #recalculate with mean values
            data        = data.where(ri_diag < 100)
            data        = data.mean(dim=["crossfront", "alongfront"], skipna=True)
            data['mld'] = mld_restore
            data['lat'] = lat_front
            dat         = data

        data = dat.mean(dim=["alongfront", "crossfront"], skipna=True)

        if True: #manually correct MLD
            idx     = np.array([7, 11, 15, 17, 19, 21, 22, 27, 31, 40, 41, 42, 43])-1
            new_mld = np.array([350, 300, 70, 180,  50, 70, 50, 80, 110, 100, 70, 70, 70])
            if ii in idx:
                print(f'Front {ii+1} manually corrected')
                idx_of_idx = np.where(idx == ii)[0][0]
                data['mld'] = new_mld[idx_of_idx]

        print('get closest integer to mean MLD')
        idx_mld = np.argmin(np.abs(data.depthi.data - data.mld.data))
        data['mld'] = data.depthi[idx_mld]

        profiles_full.append(data)

        if mld_method == 'threshold_mld':
            if ii == 0: print('use MLD threshold_mld method') #mld already defined
        elif mld_method == 'fox_mld': 
            if ii == 0: print('use Fox Kemper MLD Algorithm')
            idx, mld = calc_mld_fox2(data.n2.isel(depthi=slice(0,80)), depthi[0:80], cm=3)
            if np.isnan(mld.data): print(f'Front {ii+1} no MLD can be calculated')
            data['mld'] = mld #mld replaced
        elif mld_method == 'wb_mld':
            # prozent    = 0.05
            if ii == 0: print(f'use {prozent} of wb_max to diagonise mld')
            wbb        = data.wb
            wbb        = wbb.where(wbb.depthi < depthi[80]) # cut off values close to bottom
            idx_wb_max = wbb.argmax()
            wb_max     = wbb[idx_wb_max]
            wbb        = wbb.where(wbb.depthi > depthi[idx_wb_max] ) # dont use values above wb_max
            idx_wb_min = wbb.argmin()
            # wb_min     = wbb[idx_wb_min]
            wbb        = wbb.where(wbb.depthi < depthi[idx_wb_min]) # dont use values below wb_min
            idx        = np.abs(wbb-prozent*wb_max).argmin() #select mld at 10 % of max wb
            mld        = depthi[idx]
            data['mld']= mld #mld replaced
        else:
            print(' mld method does not exit!!!')
            break

        data = data.sel(depthi=slice(0, data.mld))
        profiles.append(data)

    Profiles_over_mld = xr.concat(profiles,      dim = fronts)
    Profiles_full     = xr.concat(profiles_full, dim = fronts)

    return Profiles_full, Profiles_over_mld


def cut_rotate_merge(m2_mask, m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx, slicing=False):
    if slicing == True:
        print('single eddy') 
        points = eva.get_eddies()
    else:
        points = eva.get_points_of_inclined_fronts()
        # print('subselect fewer fronts')
        # points = points[:4]

    shape = int(points.shape[0]/2)

    fronts   = xr.IndexVariable('fronts', np.array(np.arange(shape)+1))
    M2_cross = []
    M2_along = []
    M        = []
    N        = []
    WB       = []
    VB       = []
    Vb_cross = []
    Vb_along = []
    VB_proj  = []
    RHO      = []
    MLD      = []
    LAT      = []
    for ii in np.arange(shape):
        front =             f'R{ii+1}f'
        print(front)

        P = points[2*ii:2*ii+2,:]
        y, x, m, b = calc_line(P)

        if slicing == True:
            y        = np.linspace(P[0,1]-0.5, P[1,1]+0.5, x.shape[0])
            data     = select_data_eddy(m2y_mask, x, y, delta = 0.1)
        else:
            data = select_data(m2y_mask, x, y, delta = 0.8)

        lon_front = data.lon.min(), data.lon.max()
        lat_front = data.lat.min(), data.lat.max()

        #select fronts values and average over mld - save diagnostic values m2 n2 wb and mld
        m2_diag   = m2_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
        m2x_diag  = m2x_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
        m2y_diag  = m2y_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
        n2_diag   = n2_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
        wb_diag   = wb_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
        ub_diag   = ub_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
        vb_diag   = vb_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
        rho_diag  = rho_mask.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))
        mldx_diag = mldx.sel(lat=slice(lat_front[0],lat_front[1])).sel(lon=slice(lon_front[0],lon_front[1]))

        VB_diag   = np.sqrt(np.power(ub_diag,2) + np.power(vb_diag,2))
        # VB_diag   = np.abs(ub_diag) + np.abs(vb_diag)
        vb_diag_cross =  - np.abs( ( m2x_diag * ub_diag + m2y_diag * vb_diag) / (np.sqrt(np.power(m2x_diag,2) + np.power(m2y_diag,2))) ) # projection of fluctuation on front gradient

        #rotate todo
        if slicing == True: #pseudo rotation
            theta = np.rad2deg(np.arctan(+0.0000001))
            print(theta)
        else:
            theta = np.rad2deg(np.arctan(m))
            c, s = np.cos(theta), np.sin(theta)
            R = np.array(((c, -s), (s, c)))
            print(theta, R)

        rot_m2_diag   = rotate(m2_diag  , theta, axes=(1,2), cval = np.nan, order=1)
        rot_m2x_diag  = rotate(m2x_diag , theta, axes=(1,2), cval = np.nan, order=1)
        rot_m2y_diag  = rotate(m2y_diag , theta, axes=(1,2), cval = np.nan, order=1)
        rot_n2_diag   = rotate(n2_diag  , theta, axes=(1,2), cval = np.nan, order=1)
        rot_wb_diag   = rotate(wb_diag  , theta, axes=(1,2), cval = np.nan, order=1)
        rot_vb_diag   = rotate(vb_diag  , theta, axes=(1,2), cval = np.nan, order=1)
        rot_ub_diag   = rotate(ub_diag  , theta, axes=(1,2), cval = np.nan, order=1)
        rot_VB_diag   = rotate(VB_diag  , theta, axes=(1,2), cval = np.nan, order=1) #abs
        rot_vb_proj   = rotate(vb_diag_cross, theta, axes=(1,2), cval = np.nan, order=1)
        rot_rho_diag  = rotate(rho_diag , theta, axes=(1,2), cval = np.nan, order=1)
        rot_mldx_diag = rotate(mldx_diag, theta, axes=(0,1), cval = np.nan, order=1)

        s = rot_m2x_diag.shape
        m2_diag_ave  = empty_xr('m2' , theta)
        m2x_diag_ave = empty_xr('m2x', theta)
        m2y_diag_ave = empty_xr('m2y', theta)
        n2_diag_ave  = empty_xr('n2' , theta)
        wb_diag_ave  = empty_xr('wb' , theta)
        vb_diag_ave  = empty_xr('vb' , theta)
        ub_diag_ave  = empty_xr('ub' , theta)
        VB_diag_ave  = empty_xr('VB_abs' , theta)
        vb_proj      = empty_xr('vb_proj', theta)
        rho_diag_ave = empty_xr('rho', theta)
        mldx_ave     = empty_mldx_xr('mld')

        m2_diag_ave [:,0:s[1],0:s[2]] = rot_m2_diag
        m2x_diag_ave[:,0:s[1],0:s[2]] = rot_m2x_diag
        m2y_diag_ave[:,0:s[1],0:s[2]] = rot_m2y_diag
        n2_diag_ave [:,0:s[1],0:s[2]] = rot_n2_diag
        wb_diag_ave [:,0:s[1],0:s[2]] = rot_wb_diag
        vb_diag_ave [:,0:s[1],0:s[2]] = rot_vb_diag
        VB_diag_ave [:,0:s[1],0:s[2]] = rot_VB_diag
        vb_proj     [:,0:s[1],0:s[2]] = rot_vb_proj
        ub_diag_ave [:,0:s[1],0:s[2]] = rot_ub_diag
        rho_diag_ave[:,0:s[1],0:s[2]] = rot_rho_diag

        if np.sign(theta) == 1.0: #theta positive
            m2_cross_grad = xr.DataArray(name='m2_cross_grad', data=(   m2x_diag_ave*np.sin(np.deg2rad(theta)) + m2y_diag_ave*np.cos(np.deg2rad(theta))))
            m2_along_grad = xr.DataArray(name='m2_along_grad', data=(   m2x_diag_ave*np.cos(np.deg2rad(theta)) - m2y_diag_ave*np.sin(np.deg2rad(theta))))
            Vb_cross_grad = xr.DataArray(name='Vb_cross_grad', data=(   ub_diag_ave*np.sin(np.deg2rad( theta))  + vb_diag_ave*np.cos(np.deg2rad(theta))))
            Vb_along_grad = xr.DataArray(name='Vb_along_grad', data=(   ub_diag_ave*np.cos(np.deg2rad( theta))  - vb_diag_ave*np.sin(np.deg2rad(theta))))
        elif np.sign(theta) == -1.0: #theta negative
            print('theta negative for front', ii)
            m2_cross_grad = xr.DataArray(name='m2_cross_grad', data=(   m2x_diag_ave*np.sin(np.deg2rad(np.abs(theta))) + m2y_diag_ave*np.cos(np.deg2rad(np.abs(theta)))))
            m2_along_grad = xr.DataArray(name='m2_along_grad', data=(   m2x_diag_ave*np.cos(np.deg2rad(np.abs(theta))) - m2y_diag_ave*np.sin(np.deg2rad(np.abs(theta)))))
            Vb_cross_grad = xr.DataArray(name='Vb_cross_grad', data=(   ub_diag_ave*np.sin(np.deg2rad( np.abs(theta)))  + vb_diag_ave*np.cos(np.deg2rad(np.abs(theta)))))
            Vb_along_grad = xr.DataArray(name='Vb_along_grad', data=(   ub_diag_ave*np.cos(np.deg2rad( np.abs(theta)))  - vb_diag_ave*np.sin(np.deg2rad(np.abs(theta)))))
        else:
            print('no theta calculated')
            break


        mldx_ave    [0:s[1],0:s[2]] = rot_mldx_diag

        LAT.append( m2x_diag.lat.mean() )
        M.append(m2_diag_ave)
        M2_cross.append(m2_cross_grad)
        M2_along.append(m2_along_grad)
        N.append(n2_diag_ave)
        WB.append(wb_diag_ave)
        VB.append(VB_diag_ave)
        Vb_cross.append(Vb_cross_grad)
        Vb_along.append(Vb_along_grad)
        VB_proj.append(vb_proj)
        RHO.append(rho_diag_ave)
        MLD.append(mldx_ave)

    print('concatenate fronts: # concat fronts is extremly memory intense >> use compute node if  code not adapted')
        # save background state values of each front
    ds_M        = xr.concat(M  , dim=fronts)       # type: ignore
    ds_M2_cross = xr.concat(M2_cross , dim=fronts)  # type: ignore
    ds_M2_along = xr.concat(M2_along , dim=fronts)  # type: ignore
    ds_N        = xr.concat(N  , dim=fronts)  # type: ignore
    ds_WB       = xr.concat(WB , dim=fronts)  # type: ignore
    ds_VB       = xr.concat(VB,  dim=fronts)  # type: ignore
    ds_VB_cross = xr.concat(Vb_cross , dim=fronts)  # type: ignore
    ds_VB_along = xr.concat(Vb_along , dim=fronts)  # type: ignore
    ds_VB_proj  = xr.concat(VB_proj, dim=fronts)  # type: ignore
    ds_RHO      = xr.concat(RHO, dim=fronts)  # type: ignore
    ds_mld      = xr.concat(MLD, dim=fronts)  # type: ignore
    ds_lat      = xr.concat(LAT, dim=fronts)  # type: ignore

    print('merge fronts')
    ds_F = xr.merge([ds_M, ds_M2_cross, ds_M2_along, ds_N, ds_WB, ds_VB, ds_VB_cross, ds_VB_along, ds_VB_proj, ds_RHO, ds_mld, ds_lat]) # type: ignore

    return ds_F


# %% Functions
def coarse_graining(data, grid01_sel):
    data_interp = data.interp_like(grid01_sel, method='linear', assume_sorted=False, kwargs=None)
    return data_interp

def get_points_of_inclined_fronts():
    points = np.array([
       [-63.52200227,  35.00133776],
       [-61.74780872,  35.27190053],
       [-63.51034652,  32.74462865],
       [-61.91357233,  31.47839489],
       [-62.18978201,  32.85642603],
       [-60.69583039,  33.02282214],
       [-64.39704952,  31.83672991],
       [-63.40410597,  31.5228771 ],
       [-63.82749307,  34.095278  ],
       [-63.48475113,  33.52168493],
       [-60.4349622 ,  30.20891437],
       [-59.64463962,  29.85718277],
       [-58.35431704,  29.75436891],
       [-57.45915575,  29.78142519],
       [-59.9599433 ,  32.77168493],
       [-58.78050781,  32.06822173],
       [-62.43776588,  28.67861134],
       [-61.18574975,  28.60826502],
       [-60.02612903,  28.10220272],
       [-59.28419355,  27.65577415],
       [-66.28419355,  34.50804687],
       [-65.00193548,  34.012917  ],
       [-60.95354839,  35.80674817],
       [-59.92935484,  35.43337155],
       [-59.76806452,  35.5551248 ],
       [-58.95354839,  35.68499493],
       [-58.61483871,  35.36843649],
       [-58.25193548,  34.65415077],
       [-68.15004455,  26.73052907],
       [-67.85770584,  27.87771522],
       [-68.26093165,  26.29762864],
       [-66.86980262,  26.48161132],
       [-67.09157681,  28.64611348],
       [-66.67827036,  27.97511781],
       [-66.44641552,  27.73702258],
       [-65.62988326,  28.0616979 ],
       [-64.82343165,  28.91667626],
       [-64.53109294,  28.00758535],
       [-64.55125423,  27.98594032],
       [-63.66415745,  27.49892734],
       [-64.58149616,  26.30845115],
       [-63.54318971,  25.72403556],
       [-62.77966636,  27.81278015], 
       [-61.70305346,  27.20671955],
       [-59.11434378,  26.21104855],
       [-58.0740212 ,  26.11364595],
       [-63.97724701,  26.00542084],
       [-63.16676314,  25.42100526],
       [-63.98934378,  25.24784509],
       [-63.09418249,  25.02057236],
       [-56.92532258,  28.28445211],
       [-56.24991935,  27.65674648],
       [-56.24991935,  27.65674648],
       [-55.58459677,  27.60263393],
       [-55.58459677,  27.60263393],
       [-55.13096774,  26.99657332],
       [-57.87290323,  28.91215774],
       [-57.09669355,  28.5766599 ],
       [-58.10475806,  27.06150839],
       [-57.60072581,  26.73683306],
       [-58.9616129 ,  28.52254735],
       [-58.18540323,  28.63077246],
       [-59.5866129 ,  26.96410579],
       [-59.02209677,  26.81259064],
       [-58.39709677,  27.93813176],
       [-57.88298387,  27.29960363],
       [-57.96735082,  33.95886109],
       [-56.91896373,  33.62065763],
       [-56.49557663,  32.26784378],
       [-55.66896373,  31.38851477],
       [-55.90081857,  30.98267062],
       [-55.06412502,  30.71210785],
       [-57.01977018,  33.75593902],
       [-56.67702824,  32.99836326],
       [-58.52178631,  33.40420741],
       [-58.08831857,  32.60604724],
       [-57.36251211,  29.48104724],
       [-56.09235082,  29.68396932],
       [-62.10975163,  25.44360187],
       [-61.28717098,  24.75018057],
       [-61.14200969,  24.40927148],
       [-59.98071937,  24.60407668],
       [-59.81942905,  25.13167408],
       [-59.11781614,  24.41738837],
       [-60.86781614,  23.90602473],
       [-59.11781614,  24.04401174],
       [-63.73900083,  30.76779017],
       [-62.41238793,  29.88034429],
       [-65.06561373,  29.71800662],
       [-64.09182341,  29.71700662]])
    return points


def calc_line(point):
    #m = (point[0,1] - point[1,1])/(point[0,0]-point[1,0])
    m = (point[1,1] - point[0,1] )/(point[1,0]-point[0,0])
    b = point[1,1] - m * point[1,0] 
    x = np.linspace(point[0,0],point[1,0],100)
    y = m * x + b
    return y, x, m, b

def select_data(data, x, y, delta):
    dat   = data.where((data.lon > x.min()) & (data.lon < x.max()) , drop=True)
    dat   = dat.where((dat.lat > y.min() - delta) & (dat.lat < y.max() + delta) , drop=True)
    return dat

def select_data_eddy(data, x, y, delta):
    dat   = data.where((data.lon > x.min() - delta) & (data.lon < x.max() + delta) , drop=True)
    dat   = dat.where((dat.lat > y.min() ) & (dat.lat < y.max() ) , drop=True)
    return dat


def create_xr(data, depthi, name, theta):
    da = xr.DataArray(
        name=name,
        data=data,
        dims=["depthi", "crossfront", "alongfront"],
        coords=dict(
        depthi=(["depthi"], depthi),
        ),
        attrs=dict(
             description="rotated front", 
             theta = theta,
         )
        )
    return da

def create_xr_mldx(data, name, theta):
    da = xr.DataArray(
        name=name,
        data=data,
        dims=["crossfront", f"alongfront"],
        attrs=dict(
             description="rotated front", 
             theta = theta,
         )
        )
    return da

def empty_xr(name, theta):
    data = np.zeros([113,250,250])
    data[:,:,:] = np.nan
    DA = xr.DataArray(
                name   = name,
                data   = data,
                dims   = ["depthi","crossfront","alongfront"],
                coords = dict(
                depthi = (["depthi"], depthi),
                ),
                attrs  = dict(
                    description = "rotated front", 
                    theta = theta,
                )
                )
    return DA

def empty_mldx_xr(name):
    data = np.zeros([250,250])
    data[:,:] = np.nan
    DA = xr.DataArray(
                name   = name,
                data   = data,
                dims   = ["crossfront","alongfront"],
                )
    return DA

def calc_mld_fox(N2, depthi):
    """ 
    Fox Kemper MLD algorithm for single fronts
    """
    cm           = 3
    sum          = N2.cumsum(dim='depthi') * cm / depthi
    diff         = N2 - sum
    N2_min_depth = N2.argmin(dim='depthi')
    arg = np.abs(diff.where(diff.depthi > depthi[N2_min_depth] ))
    if arg.isnull().all() == True:
        idx  = np.nan
        mldx = np.nan
    else: 
        idx  = arg.argmin()
        mldx = depthi[idx]
    
    #idx          = np.abs(diff.where(diff.depthi >depthi[N2_min_depth] )).argmin(dim='depthi')
    #mldx         = depthi[mld_idx]
    #idx          = np.abs(diff).argmin(dim='depthi')
    #mldx         = depthi[idx]
    mldx         = xr.DataArray(name='mld', data = mldx)

    return(idx, mldx)    #integer, depth


def calc_mld_fox2(N2, depthI, cm):
    """
    Averages N2 from top to certain depth; 
    cal. difference between N2 and averaged N2;
    find minimum of difference, below minima of N2 and above bottom influence
    """
    sum  = N2.cumsum(dim='depthi') * cm / (depthI )
    diff = N2 - sum

    N2_min_depth = N2.argmin()
    arg          = np.abs(diff.where(diff.depthi > depthI[N2_min_depth] ))

    mld_idx = arg.argmin()
    mldx    = depthI[mld_idx]
    return (mld_idx, mldx)

def calc_sum_lsq(data_diag, data_param):
    N = data_diag.shape
    lsq = np.sum(np.power((data_diag.data - data_param.data),2)) / N
    return lsq 

def calc_psi_stone(A, cs, depthI, mask_nan, grad_b):
    # compare vb and wb with parameterizations
    wb_stone_param       = mask_nan * eva.calc_wb_stone_param(A.lat, A.mld,  A.m2_cross_grad, A.ri_diag, cs, -depthI, structure=True)
    vb_stone_param       = eva.calc_vb_stone_param(A.lat, A.mld,  A.m2_cross_grad, A.ri_diag, cs)
    psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param,  A.m2_cross_grad, vb_stone_param, A.n2, grad_b)

    return wb_stone_param, vb_stone_param, psi_stone_full_param

def calc_psi_stone_dmean(A, cs, depthI, mask_nan, grad_b):
    # compare vb and wb with parameterizations
    wb_stone_param       = mask_nan * eva.calc_wb_stone_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, cs, -depthI, structure=False)
    vb_stone_param       = eva.calc_vb_stone_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, cs)
    psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param,  A.m2_cross_grad_dmean, vb_stone_param, A.n2_dmean, grad_b)

    return wb_stone_param, vb_stone_param, psi_stone_full_param

def calc_psi_fox(A, cf, depthI, mask_nan, grad_b):
    # compare vb and wb with parameterizations
    wb_fox_param       = mask_nan * eva.calc_wb_fox_param(A.lat, A.mld,  A.m2_cross_grad, - depthI, cf, structure=True )
    vb_fox_param       = mask_nan * eva.calc_vb_fox_param(A.lat, A.mld,  A.m2_cross_grad, A.ri_diag, -depthI, cf, structure=True )
    psi_fox_full_param = eva.calc_streamfunc_full(wb_fox_param,  A.m2_cross_grad, vb_fox_param, A.n2, grad_b)

    return wb_fox_param, vb_fox_param, psi_fox_full_param


def calc_psi_fox_dmean(A, cf, depthI, mask_nan, grad_b):
    # compare vb and wb with parameterizations
    wb_fox_param       = mask_nan * eva.calc_wb_fox_param(A.lat, A.mld,  A.m2_cross_grad_dmean, - depthI, cf, structure=False)
    vb_fox_param       = mask_nan * eva.calc_vb_fox_param(A.lat, A.mld,  A.m2_cross_grad_dmean, A.ri_dmean_from_mean, -depthI, cf, structure=True )
    psi_fox_full_param = eva.calc_streamfunc_full(wb_fox_param,  A.m2_cross_grad_dmean, vb_fox_param, A.n2_dmean, grad_b)

    return wb_fox_param, vb_fox_param, psi_fox_full_param


def calc_log10(x_data, y_data):
    mask   = ~np.isnan(x_data) & ~np.isnan(y_data) #remove nans from original data
    x_data = x_data[mask]
    y_data = y_data[mask]

    logx = np.log10(np.abs(x_data)) #not nessecary
    logy = np.log10(np.abs(y_data))
    return(logx,logy)

def calc_log10_2(x_data, y_data):
    mask   = ~np.isnan(x_data) & ~np.isnan(y_data) #remove nans from original data
    x_data = x_data[mask]
    y_data = y_data[mask]

    # maskx = x_data > 0
    # masky = y_data > 0
    # mask = maskx * masky
    # x_data = x_data[mask]
    # y_data = y_data[mask]


    logx = np.log10(x_data) 
    logy = np.log10(y_data)

    print('Cut of negative Ri values')
    mask = logx > 0 #negative x values are outliers >> especially in large regional domains it has a strong impact on slopes
    logx = logx[mask]
    logy = logy[mask]
    # mask = logy > 0
    # logx = logx[mask]
    # logy = logy[mask]

    mask   = ~np.isnan(logx) & ~np.isnan(logy)
    logx = logx[mask]
    logy = logy[mask]

    return(logx,logy)

def plot_slope(logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name, savefig, fig_path):
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    fs = 10
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_diag
    logy = logy_diag
    ax.scatter(logx, logy , color='red', marker='^')
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'red', label=f'diag s={res.slope:.2f} intercept={res.intercept:.2}')  # type: ignore
    # ax.set_xlabel(r'$log_{10} Ri$', fontsize=fs)
    # ax.set_ylabel(r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $', fontsize=fs)
    # ax.legend(title=f'intercept = {res.intercept:.1}')  # type: ignore

    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_fox
    logy = logy_fox
    ax.scatter(logx, logy , color='black', marker='x')
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'black', label=f'fox slope={res.slope:.2f} intercept={res.intercept:.2}')  # type: ignore
    # ax.set_xlabel(r'$log_{10} Ri$', fontsize=fs)
    # ax.set_ylabel(r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $', fontsize=fs)
    # ax.legend(title=f'intercept = {res.intercept:.1}')  # type: ignore

    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_stone
    logy = logy_stone
    ax.scatter(logx, logy , color='blue', marker='o')
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'blue', label=f'stone slope={res.slope:.2f} intercept={res.intercept:.2}')  # type: ignore
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)
    # ax.legend(title=f'intercept = {res.intercept:.2}')  # type: ignore

    # for ax in hca:
    pyic.plot_settings(ax)   # type: ignore
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    # ax.axis('equal')
    ax.grid()
    ax.legend(loc='lower left', bbox_to_anchor=(0,1.02,1, 0.2))
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/correlation_{name}_{lim}_nov22.png', dpi=150, format='png', bbox_inches='tight')


def plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name, string='Ri'):
    fs = 10
    # ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_diag
    logy = logy_diag
    ax.scatter(logx, logy , color='k', marker='^', facecolors='none')
    res = stats.linregress(logx, logy)
    # string = f's={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.2} se:{res.stderr:.2}'
    intercept = rf'{10**res.intercept:.2f} ' # type: ignore
    slope     = rf'{res.slope:.2f} ' # type: ignore
    string    = rf'$ {intercept} ~Ri^{{{slope}}}$ ' # type: ignore
    ax.plot(logx, res.intercept + res.slope*logx, 'k', label=f'diag.: {string}')  # type: ignore
    
    logx = logx_fox
    logy = logy_fox
    ax.scatter(logx, logy , color='tab:blue', marker='o', facecolors='none')
    res = stats.linregress(logx, logy)
    intercept = rf'{10**res.intercept:.2f} ' # type: ignore
    slope     = rf'{res.slope:.2f} ' # type: ignore
    string    = rf'$ {intercept} ~Ri^{{{slope}}}$ ' # type: ignore
    ax.plot(logx, res.intercept + res.slope*logx, 'tab:blue', label=f'PER: {string}')  # type: ignore
    
    logx = logx_stone
    logy = logy_stone
    ax.scatter(logx, logy , color='tab:orange', marker='x')
    res = stats.linregress(logx, logy)
    intercept = rf'{10**res.intercept:.2f} ' # type: ignore
    slope     = rf'{res.slope:.2f} ' # type: ignore
    string    = rf'$ {intercept} ~Ri^{{{slope}}}$ ' # type: ignore
    ax.plot(logx, res.intercept + res.slope*logx, 'tab:orange', label=f'ALS: {string}')  # type: ignore
    
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)
    # for ax in hca:
    pyic.plot_settings(ax)   # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
    ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore

    ax.grid()
    ax.legend()


def plot_slope_4_a(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name, string='Ri'):
    fs = 10
    # ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_diag
    logy = logy_diag
    ax.scatter(logx, logy , color='k', marker='^', facecolors='none')
    res = stats.linregress(logx, logy)
    # string = f's={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.2} se:{res.stderr:.2}'
    intercept = rf'{10**res.intercept:.2f} ' # type: ignore
    slope     = rf'{res.slope:.2f} ' # type: ignore
    string    = rf'$ {intercept} ~\alpha^{{{slope}}}$ ' # type: ignore
    ax.plot(logx, res.intercept + res.slope*logx, 'k', label=f'diag.: {string}')  # type: ignore
    
    logx = logx_fox
    logy = logy_fox
    ax.scatter(logx, logy , color='tab:blue', marker='o', facecolors='none')
    res = stats.linregress(logx, logy)
    intercept = rf'{10**res.intercept:.2f} ' # type: ignore
    slope     = rf'{res.slope:.2f} ' # type: ignore
    string    = rf'$ {intercept} ~\alpha^{{{slope}}}$ ' # type: ignore
    ax.plot(logx, res.intercept + res.slope*logx, 'tab:blue', label=f'PER: {string}')  # type: ignore
    
    logx = logx_stone
    logy = logy_stone
    ax.scatter(logx, logy , color='tab:orange', marker='x')
    res = stats.linregress(logx, logy)
    intercept = rf'{10**res.intercept:.2f} ' # type: ignore
    slope     = rf'{res.slope:.2f} ' # type: ignore
    string    = rf'$ {intercept} ~\alpha^{{{slope}}}$ ' # type: ignore
    ax.plot(logx, res.intercept + res.slope*logx, 'tab:orange', label=f'ALS: {string}')  # type: ignore
    
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)
    # for ax in hca:
    pyic.plot_settings(ax)   # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
    ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore

    ax.grid()
    ax.legend()



def plot_param_vs_diag_v(A, A_prof, Profiles_over_mld, cf , cs, fig_path, savefig, use_profile_for_param):
    fig, ax = plt.subplots(1,2, figsize=(10,5), sharey=False, squeeze=True) #16,7 for 5
    x = np.linspace(0,10,10)
    y = x
    i = 0
    ax[i].plot(x,y, color='grey')
    lim= Profiles_over_mld.wb.max()
    ax[i].set_ylim(0,lim)
    ax[i].set_xlim(0,lim)
    ax[i].plot(A_prof.wb.mean(dim='depthi'), A.wb_fox_param.mean(dim='depthi'), 'o', color='black', label=r'$wb_{fox} $'+f' Cf={cf:.4f}')
    ax[i].plot(A_prof.wb.mean(dim='depthi'), A.wb_stone_param.mean(dim='depthi'), 'x', color='black', label=r'$wb_{stone} $'+f' Cs={cs:.2f}')
    ax[i].set_xlabel(r'$wb_{diag}$', fontsize=25)
    ax[i].set_ylabel(r'$wb_{fox}, wb_{stone}$', fontsize=25)
    ax[i].legend()
    ax[i].grid()


    if use_profile_for_param == True: vb_stone_param = A.vb_stone_param.mean(dim = 'depthi')
    else: vb_stone_param = A.vb_stone_param

    i=1
    ax[i].plot(x,y, color='grey')
    lim= A.Vb_cross_grad.max()
    ax[i].set_ylim(0,lim)
    ax[i].set_xlim(0,lim)
    ax[i].plot(A_prof.Vb_cross_grad.mean(dim='depthi'), A.vb_fox_param.mean(dim='depthi'), 'o', color='black', label=r'$vb_{fox} $'+f' Cf={cf:.4f}')
    ax[i].plot(A_prof.Vb_cross_grad.mean(dim='depthi'), vb_stone_param, 'x', color='black', label=r'$vb_{stone} $'+f' Cs={cs:.2f}')
    ax[i].set_xlabel(r'$vb_{diag}$', fontsize=25)
    ax[i].set_ylabel(r'$vb_{fox}, vb_{stone}$', fontsize=25)
    ax[i].legend()
    ax[i].grid()

    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/wb_diag_vs_pram_cfopt_cross_nov22.png', dpi=100, format='png', bbox_inches='tight')

    if use_profile_for_param == False:
        psi_diag_held_dmean = A.psi_diag_held
        psi_diag_full_dmean = A.psi_diag_full
    else:
        psi_diag_held_dmean = A.psi_diag_held.mean(dim='depthi')
        psi_diag_full_dmean = A.psi_diag_full.mean(dim='depthi')

    fig, ax = plt.subplots(1,2, figsize=(10,5), sharey=False, squeeze=True) 
    x = np.linspace(0,10,10)
    y = x
    i = 0
    ax[i].set_title('Held Schneider')
    ax[i].plot(x,y, color='grey')
    lim = psi_diag_held_dmean.max()
    ax[i].set_ylim(0,lim)
    ax[i].set_xlim(0,lim)
    ax[i].plot(psi_diag_held_dmean, A.psi_fox_held_param.mean(dim='depthi'), 'o', color='black', label=r'$\psi_{fox} $'+f' Cf={cf:.4f}')
    ax[i].plot(psi_diag_held_dmean, A.psi_stone_held_param.mean(dim='depthi'), 'x', color='black', label=r'$\psi_{stone}$'+f'Cs={cs:.2f}')
    ax[i].set_xlabel(r'$\psi_{diag}$', fontsize=25)
    ax[i].set_ylabel(r'$\psi_{fox}, \psi_{stone}$', fontsize=25)
    ax[i].legend()
    ax[i].grid()

    i = 1
    ax[i].set_title('full')
    ax[i].plot(x,y, color='grey')
    lim = psi_diag_full_dmean.max()
    ax[i].set_ylim(0,lim)
    ax[i].set_xlim(0,lim)
    ax[i].plot(psi_diag_full_dmean, A.psi_fox_full_param.mean(dim='depthi'), 'o', color='black', label=r'$\psi_{fox} $'+f' Cf={cf:.4f}')
    ax[i].plot(psi_diag_full_dmean, A.psi_stone_full_param.mean(dim='depthi'), 'x', color='black', label=r'$\psi_{stone}$'+f'Cs={cs:.2f}')
    ax[i].set_xlabel(r'$\psi_{diag}$', fontsize=25)
    ax[i].legend()
    ax[i].grid()
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/psi_diag_vs_pram_cfopt_cross_nov22.png', dpi=100, format='png', bbox_inches='tight')

def plot_param_vs_diag_v_single_error(A, A_prof, Profiles_over_mld, cf , cs, fig_path, savefig, use_profile_for_param, E_stone, E_fox):
    if use_profile_for_param == False:
        psi_diag_held_dmean = A.psi_diag_held
        psi_diag_full_dmean = A.psi_diag_full
    else:
        psi_diag_held_dmean = A.psi_diag_held.mean(dim='depthi')
        psi_diag_full_dmean = A.psi_diag_full.mean(dim='depthi')

    fig, ax = plt.subplots(1,1, figsize=(5,5), sharey=False, squeeze=True) 
    # x = np.linspace(0,10,10)
    # y = x

    fronts = A.psi_diag_full.fronts
    # ax.plot(fronts, psi_diag_full_dmean - A.psi_fox_full_param.mean(dim='depthi'), 'o', color='black', label=r'$\psi_{fox} $'+f' Cf={cf:.4f}' )
    # ax.plot(fronts, psi_diag_full_dmean - A.psi_stone_full_param.mean(dim='depthi') , 'x', color='black', label=r'$\psi_{stone}$'+f'Cs={cs:.2f}')
            
    ax.semilogy(fronts, np.power(psi_diag_full_dmean - A.psi_fox_full_param.mean(dim='depthi')  ,2) , 'o', color='black', mfc='none', label=r'$\psi_{fox} $'+f' Cf={cf:.4f}' )
    ax.semilogy(fronts, np.power(psi_diag_full_dmean - A.psi_stone_full_param.mean(dim='depthi'),2) , 'x', color='black', label=r'$\psi_{stone}$'+f'Cs={cs:.2f}')
    ax.set_xlabel('Front', fontsize=15)
    ax.set_ylabel(r'$(\psi_{diag} - \psi_{param})^2$', fontsize=15)
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)
    textstr = '\n'.join((
    r'$E_{mean} = \sum{(\psi_{diag}-\psi_{param})^2 / N}$',
    r'$E_{stone} = $'+f' {E_stone:.2f}',
    r'$E_{fox} = $'+f' {E_fox:.2f}',
    ))
    ax.text(0.05, 0.2, textstr, transform=ax.transAxes, fontsize=10,
        verticalalignment='top', bbox=props)
    ax.legend(loc='lower right')
    ax.grid(axis='y')
    # ax.grid(True, which='both', axis='y')
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/psi_diag_vs_pram_cfopt_cross_single_semi_nov22.png', dpi=150, format='png', bbox_inches='tight')

def plot_param_vs_diag_v_single_error_ri(A, A_prof, Profiles_over_mld, cf , cs, fig_path, savefig, use_profile_for_param, E_stone, E_fox):
    if use_profile_for_param == False:
        psi_diag_held_dmean = A.psi_diag_held
        psi_diag_full_dmean = A.psi_diag_full
    else:
        psi_diag_held_dmean = A.psi_diag_held.mean(dim='depthi')
        psi_diag_full_dmean = A.psi_diag_full.mean(dim='depthi')

    E_fox   = (1/np.abs(psi_diag_full_dmean)  * np.sqrt(np.power(A.psi_diag_full - A.psi_fox_full_param.mean(dim='depthi')  ,2))).mean()
    E_stone = (1/np.abs(psi_diag_full_dmean)  * np.sqrt(np.power(A.psi_diag_full - A.psi_stone_full_param.mean(dim='depthi')  ,2))).mean()

    fig, ax = plt.subplots(1,1, figsize=(5,5), sharey=False, squeeze=True) 

    N = A.ri_diag.size
    ax.semilogy(A.ri_diag, 1/np.abs(psi_diag_full_dmean)  * np.abs(psi_diag_full_dmean - A.psi_fox_full_param.mean(dim='depthi')  ) , 'o', color='black', mfc='none', label=r'$\psi_{fox} $'+f' Cf={cf:.4f}' )
    ax.semilogy(A.ri_diag, 1/np.abs(psi_diag_full_dmean)  * np.abs(psi_diag_full_dmean - A.psi_stone_full_param.mean(dim='depthi')) , 'x', color='black', label=r'$\psi_{stone}$'+f'Cs={cs:.2f}')
    ax.set_xlabel('Ri', fontsize=15)
    ax.set_ylabel(r'$ \frac{|\psi_{diag} - \psi_{param}|}{|\psi_{diag}|}$', fontsize=15)
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)
    textstr = '\n'.join((
    r'$E_{mean} = \frac{1}{N}~ \sum \frac{|\psi_{diag} - \psi_{param}|}{|\psi_{diag}|}$',
    r'$E_{stone} = $'+f' {E_stone.data:.2f}',
    r'$E_{fox} = $'+f' {E_fox.data:.2f}',
    ))
    ax.text(0.05, 0.25, textstr, transform=ax.transAxes, fontsize=10,
        verticalalignment='top', bbox=props)
    ax.legend(loc='lower right')
    ax.grid(axis='y')
    # ax.grid(True, which='both', axis='y')
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/psi_diag_vs_pram_cfopt_cross_single_semi_nov22.png', dpi=150, format='png', bbox_inches='tight')


def plot_param_vs_diag_v_single(A, A_prof, Profiles_over_mld, cf , cs, fig_path, savefig, use_profile_for_param, E_stone, E_fox):
    if use_profile_for_param == False:
        psi_diag_held_dmean = A.psi_diag_held
        psi_diag_full_dmean = A.psi_diag_full
    else:
        psi_diag_held_dmean = A.psi_diag_held.mean(dim='depthi')
        psi_diag_full_dmean = A.psi_diag_full.mean(dim='depthi')

    fig, ax = plt.subplots(1,1, figsize=(5,5), sharey=False, squeeze=True) 
    x = np.linspace(0,10,10)
    y = x

    lim = psi_diag_full_dmean.max()
    ax.set_ylim(0,lim)
    ax.set_xlim(0,lim)
    ax.plot(psi_diag_full_dmean, A.psi_fox_full_param.mean(dim='depthi'), 'o', color='black', mfc='none', label=r'$\psi_{fox} $'+f' Cf={cf:.4f}' )
    ax.plot(psi_diag_full_dmean, A.psi_stone_full_param.mean(dim='depthi') , 'x', color='black', label=r'$\psi_{stone}$'+f'Cs={cs:.2f}')
    ax.plot(x,y, 'grey')
    # ax.plot(x,y*0.5, 'r')
    # ax.plot(x,y*1.5, 'r')
    ax.set_xlabel(r'$\psi_{diag}$', fontsize=15)
    ax.set_ylabel(r'$\psi_{fox}, \psi_{stone}$', fontsize=15)
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)
    textstr = '\n'.join((
    r'$E_{mean} = \sum{(\psi_{diag}-\psi_{param})^2 / N}$',
    r'$E_{stone} = $'+f' {E_stone:.2f}',
    r'$E_{fox} = $'+f' {E_fox:.2f}',
    ))
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=10,
        verticalalignment='top', bbox=props)
    ax.legend(loc='lower right')
    ax.grid()
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/psi_diag_vs_pram_cfopt_cross_single_norm_nov22.png', dpi=150, format='png', bbox_inches='tight')

def plot_param_vs_diag_v_single_log(A, A_prof, Profiles_over_mld, cf , cs, fig_path, savefig, use_profile_for_param, E_stone, E_fox, optimize_var):
    if use_profile_for_param == False:
        psi_diag_held_dmean = A.psi_diag_held
        psi_diag_full_dmean = A.psi_diag_full
    else:
        psi_diag_held_dmean = A.psi_diag_held.mean(dim='depthi')
        psi_diag_full_dmean = A.psi_diag_full.mean(dim='depthi')

    fig, ax = plt.subplots(1,1, figsize=(5,5), sharey=False, squeeze=True) 
    x = np.linspace(1e-2,10,100)
    y = x
    
    xvals = [np.nan, np.nan, 3e-2, 4e-2, 4e-2, 2e-2 ]
    lim_max = 10
    lim_min = 1e-2
    ax.set_ylim(lim_min,lim_max)
    ax.set_xlim(lim_min,lim_max)
    ax.grid()
    ax.loglog(psi_diag_full_dmean, A.psi_fox_full_param.mean(dim='depthi'), 'o', color='tab:blue', mfc='none', label=r'$\psi_{Fox-Kemper} $'+f' Cf={cf:.3f}' , zorder=20)
    ax.loglog(psi_diag_full_dmean, A.psi_stone_full_param.mean(dim='depthi') , 'x', color='tab:orange', label=r'$\psi_{Stone}$'+f'Cs={cs:.2f}', zorder=21)
    ax.legend(loc='lower center')
    ax.plot(x,y, 'grey', label = r'$\psi_{diag}  = \psi_{param}$')

    ax.loglog(x, y * 10, 'r', ls='-', label=r'$ 10 ~\psi_{diag}$ ')
    ax.loglog(x, y * 0.1, 'r', ls='-')
    ax.loglog(x, y * 5, 'r', ls=':', label=r'$ 5 ~\psi_{diag}$ ')
    ax.loglog(x, y * 0.2, 'r', ls=':')

    tools.labelLines(plt.gca().get_lines(), align = True, xvals=xvals, zorder=2)
    ax.set_xlabel(r'$\psi_{diag} ~[m^2/s]$', fontsize=15)
    ax.set_ylabel(r'$\psi_{Fox-Kemper}, \psi_{Stone} ~[m^2/s]$', fontsize=15)
    ax.text(1.2e-2, 7, r'Overestimated $\psi$', color='grey')
    ax.text(7, 1.3e-2, r'Underestimated $\psi$', color='grey', rotation=90)

    if savefig == True: plt.savefig(f'{fig_path}param_vs_diag/psi_diag_vs_pram_error_{optimize_var}.png', dpi=200, format='png', bbox_inches='tight')


# only optimized for one of the four figures!!!!
def plot_param_vs_diag_v_logarithmic(A, fig_path, savefig, optimize_var):
    hca, hcb = pyic.arrange_axes(2, 1, plot_cb=False, asp=1, fig_size_fac=2.1, daxl=1.4)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    x = np.linspace(4e-7,1e-4,100)
    y = x
    
    xvals   = [np.nan, np.nan, 9e-7, 7e-6, np.nan, 8e-6 ]
    lim_max = x.max()
    lim_min = x.min()

    ax.set_ylim(lim_min,lim_max)
    ax.set_xlim(lim_min,lim_max)
    ax.grid()


    ax.loglog(A.Vb_cross_grad_dmean, A.vb_fox_param_dmean, 'o', color='tab:blue', mfc='none', label=r'$\overline{\hat{v}^\prime b^\prime}_{PER} $', zorder=20)
    ax.loglog(A.Vb_cross_grad_dmean, A.vb_stone_param_dmean, 'x', color='tab:orange', label=r'$\overline{\hat{v}^\prime b^\prime}_{ALS} $', zorder=21)
    ax.legend(loc='lower center')
    ax.plot(x,y, 'grey', label = r'$\overline{\hat{v}^\prime b^\prime}_{diag}  = \overline{\hat{v}^\prime b^\prime}_{param}$')

    ax.loglog(x, y * 10, 'r', ls='-', label=r'$ 10 ~\overline{\hat{v}^\prime b^\prime}_{diag}$ ')
    ax.loglog(x, y * 0.1, 'r', ls='-')
    ax.loglog(x, y * 5, 'r', ls=':', label=r'$ 5 ~\overline{\hat{v}^\prime b^\prime}_{diag}$ ')
    ax.loglog(x, y * 0.2, 'r', ls=':')

    tools.labelLines(ax.get_lines(), align = True, xvals=xvals, zorder=2)

    ax.set_xlabel(r'$\overline{\hat{v}^\prime b^\prime}_{diag}~[m^2/s^3]$')
    ax.set_ylabel(r'$\overline{\hat{v}^\prime b^\prime}_{param}~[m^2/s^3]$')
    # ax.text(4.3e-7, 7e-5, r'Overestimated $\overline{\hat{v}^\prime b^\prime}$', color='grey')
    # ax.text(7e-5, 4.9e-7, r'Underestimated $\overline{\hat{v}^\prime b^\prime}$', color='grey', rotation=90)
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    x = np.linspace(3e-10,5e-7,100)
    y = x

    xvals   = [np.nan, np.nan, 1e-9, 1e-9, np.nan, 1e-9 ]
    lim_max = x.max()
    lim_min = x.min()

    ax.set_ylim(lim_min,lim_max)
    ax.set_xlim(lim_min,lim_max)
    ax.grid()

    ax.loglog(A.wb_dmean, A.wb_fox_param_dmean, 'o', color='tab:blue', mfc='none', label=r'$\overline{w^\prime b^\prime}_{PER} $', zorder=20)
    ax.loglog(A.wb_dmean, A.wb_stone_param_dmean, 'x', color='tab:orange', label=r'$\overline{w^\prime b^\prime}_{ALS} $', zorder=21)
    ax.legend(loc='upper right')
    ax.plot(x,y, 'grey', label = r'$\overline{w^\prime b^\prime}_{diag}  = \overline{w^\prime b^\prime}_{param}$')

    ax.loglog(x, y * 10, 'r', ls='-', label=r'$ 10 ~\overline{w^\prime b^\prime}_{diag}$ ')
    ax.loglog(x, y * 0.1, 'r', ls='-')
    ax.loglog(x, y * 5, 'r', ls=':', label=r'$ 5 ~\overline{w^\prime b^\prime}_{diag}$ ')
    ax.loglog(x, y * 0.2, 'r', ls=':')

    tools.labelLines(ax.get_lines(), align = True, xvals=xvals, zorder=2)

    ax.set_xlabel(r'$\overline{w^\prime b^\prime}_{diag} ~[m^2/s^3]$')
    ax.set_ylabel(r'$\overline{w^\prime b^\prime}_{param}~[m^2/s^3]$')
    # ax.text(3.5e-10, 3.3e-7, r'Overestimated $\overline{w^\prime b^\prime}$', color='grey')
    # ax.text(3.3e-7, 4e-10, r'Underestimated $\overline{w^\prime b^\prime}$', color='grey', rotation=90)

    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/wb_vb_error_{optimize_var}_02.png', dpi=200, format='png', bbox_inches='tight')


def plot_param_vs_diag_v_logarithmic2(A, A_prof, Profiles_over_mld, cf , cs, fig_path, savefig, use_profile_for_param):


    if use_profile_for_param == False:
        psi_diag_held_dmean = A.psi_diag_held
        psi_diag_full_dmean = A.psi_diag_full
    else:
        psi_diag_held_dmean = A.psi_diag_held.mean(dim='depthi')
        psi_diag_full_dmean = A.psi_diag_full.mean(dim='depthi')

    fig, ax = plt.subplots(1,2, figsize=(10,5), sharey=False, squeeze=True) 
    i = 0
    ax[i].set_title('Held Schneider')
    ax[i].loglog(psi_diag_held_dmean, A.psi_fox_held_param.mean(dim='depthi'), 'o', color='black', label=r'$\psi_{fox} $'+f' Cf={cf:.4f}')
    ax[i].loglog(psi_diag_held_dmean, A.psi_stone_held_param.mean(dim='depthi'), 'x', color='black', label=r'$\psi_{stone}$'+f'Cs={cs:.2f}')
    ax[i].set_xlabel(r'$\psi_{diag}$', fontsize=25)
    ax[i].set_ylabel(r'$\psi_{fox}, \psi_{stone}$', fontsize=25)
    ax[i].legend()
    ax[i].grid()
    lim_x = ax[i].get_xlim()
    lim_y = ax[i].get_ylim()
    lim_min = np.min([lim_x[0], lim_y[0]])
    lim_max = np.max([lim_x[1], lim_y[1]])
    x = np.linspace(lim_min, lim_max,2)
    y = x
    ax[i].loglog(x,y, color='grey')

    i = 1
    ax[i].set_title('full')
    ax[i].loglog(psi_diag_full_dmean, A.psi_fox_full_param.mean(dim='depthi'), 'o', color='black', label=r'$\psi_{fox} $'+f' Cf={cf:.4f}')
    ax[i].loglog(psi_diag_full_dmean, A.psi_stone_full_param.mean(dim='depthi'), 'x', color='black', label=r'$\psi_{stone}$'+f'Cs={cs:.2f}')
    ax[i].set_xlabel(r'$\psi_{diag}$', fontsize=25)
    ax[i].legend()
    ax[i].grid()
    lim_x = ax[i].get_xlim()
    lim_y = ax[i].get_ylim()
    lim_min = np.min([lim_x[0], lim_y[0]])
    lim_max = np.max([lim_x[1], lim_y[1]])
    x = np.linspace(lim_min, lim_max,2)
    y = x
    ax[i].loglog(x,y, color='grey')
    #ax[i].autoscale(False)
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/psi_diag_vs_pram_cfopt_cross_log_dec22.png', dpi=100, format='png', bbox_inches='tight')


def plot_profiles(Profiles_full, ii, mask_nan, use_profile_for_param, A, wb_fox_param, wb_stone_param, vb_fox_param, psi_fox_held_param, psi_fox_full_param, psi_stone_held_param, psi_stone_full_param, cf, cs,  fig_path, savefig):
    B   = Profiles_full.isel(fronts=ii)
    n2  = B.n2
    wb  = B.wb
    ub  = B.Vb_along_grad
    vb  = B.Vb_cross_grad
    m2x = B.m2_along_grad
    m2y = B.m2_cross_grad
    Mld = B.mld
    #replacing absolute values
    # m2 = np.sqrt(np.power(B.m2_cross_grad,2) + np.power(B.m2_along_grad,2)) # difference lies in averaging!
    # VB = np.sqrt(np.power(B.Vb_cross_grad,2) + np.power(B.Vb_along_grad,2))
    m2 = np.abs(m2x) + np.abs(m2y) # difference lies in averaging!
    VB = np.abs(ub) + np.abs(vb)
    m2_sign = np.sign(m2y)

    ri_diag_f     = eva.calc_richardsonnumber(B.lat, n2, np.abs(m2y))
    psi_diag_held = eva.calc_streamfunc_held_schneider(B.wb, B.m2_along_grad, B.m2_cross_grad)
    grad_b        = eva.calc_abs_grad_b(B.m2_along_grad, B.m2_cross_grad, B.n2)
    fcoriolis     = eva.calc_coriolis_parameter(B.lat)
    psi_diag_full = eva.calc_streamfunc_full(B.wb, B.m2_cross_grad, B.Vb_cross_grad, B.n2, grad_b)
    if use_profile_for_param== False:  vb_stone_param = A.vb_stone_param.expand_dims(dim={'depthi': mask_nan.isel(fronts=ii) * A.wb_stone_param.depthi}) # type: ignore
    else: vb_stone_param = A.vb_stone_param
    
    fig, ax = plt.subplots(1,5, figsize=(25,12), sharey=True, squeeze=True) #16,7 for 5
    fig.suptitle(rf'Front {ii+1}', fontsize=25)
    ylim = np.array([600, 0])
    lw   = 3
    fs   = 30
    fl   = 15
    i=0
    ax[i].semilogx(n2.squeeze(), depthi, color='black', lw=lw, label=r'$dbdz$')
    ax[i].axhline(Mld, color='r', ls=':', lw=lw)

    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(1e-6,5e-5)
    # ax[i].set_xlim(0,1.5e-5)
    ax[i].set_xlabel('$dbdz$ ', fontsize=fs)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)
    ax[i].legend(loc='lower left', fontsize=15)
    ax2 = ax[i].twiny()
    ax2.semilogx(ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])), ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])).depthi, 'black', lw=lw, ls='dashed', label=r'$Ri=\frac{N^2 f^2}{M^4}$')
    ax2.tick_params(labelsize=fl)
    ax2.set_xlabel(r'Ri', fontsize=fs)
    plt.legend(loc='upper right', fontsize=15)

    i=1
    ax[i].plot(m2x, depthi, ls='dashed', color='black', lw=lw, label='$along b$')
    ax[i].plot(m2y, depthi, ls=':', color='black', lw=lw, label='$cross b$')
    ax[i].plot(m2*m2_sign, depthi, 'black', lw=lw, label='$M2 abs$')    
    ax[i].axhline(Mld, color='r', ls=':', lw=lw)
    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(-7e-8,3e-8)
    ax[i].set_xlabel(r'$dbd\hat{y}$ ', fontsize=fs)
    ax[i].legend(loc='lower left', fontsize=15)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)

    i=2
    ax[i].plot(wb, depthi, 'black', lw=lw , label = r"$\overline{w'b'}$")
    ax[i].plot(wb_fox_param.isel(fronts=ii), wb_fox_param.depthi, ls=':', color='red', lw=lw, label = r"$\overline{w'b'} (Fox)$")
    ax[i].plot(wb_stone_param.isel(fronts=ii), wb_stone_param.depthi, ls=':', color='blue', lw=lw, label = r"$\overline{w'b'} (Stone)$")
    ax[i].axhline(Mld, color='r', ls=':', lw=lw)
    ax[i].legend(loc='lower right', fontsize=15)   
    ax[i].set_ylim(ylim) # type: ignore
    # ax[i].set_xlim(-0.5e-7,2e-7)
    ax[i].set_xlabel(r"$\overline{w'b'}$ ", fontsize=fs)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)

    i = 3
    ax[i].plot(ub, depthi, color='grey', lw=lw, ls='dashed', label = r"$\overline{V'b'} $along")
    ax[i].plot(vb, depthi, color='black', lw=lw, ls=':', label = r"$\overline{\hat{v}'b'}$")
    ax[i].plot(VB, depthi, color='black', lw=lw, label = r"$\overline{VB}$")
    ax[i].plot(vb_fox_param.isel(fronts=ii), vb_fox_param.depthi, ls=':', color='red', lw=lw, label = r"$\overline{v'b'} (Fox)$")
    ax[i].plot(vb_stone_param.isel(fronts=ii), vb_stone_param.depthi, ls=':', color='blue', lw=lw, label = r"$\overline{v'b'} (Stone)$")
    ax[i].axhline(Mld, color='r', ls=':', lw=lw) 
    ax[i].set_xlabel(r'$dbd\hat{y}$ ', fontsize=fs)
    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(-0.5e-5,5e-5)
    # ax[i].set_xlabel(r"$\overline{v'b'} & \overline{u'b'} $ ", fontsize=fs)
    ax[i].legend(loc='lower right', fontsize=15)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    ax[i].xaxis.get_offset_text().set_fontsize(fl)

    i=4
    ax[i].plot(psi_diag_held, psi_diag_held.depthi, 'black', lw=lw, ls='dashed', label=r"$ \psi_{held} = - \frac{\overline{w'b'} * d_y b }{ (|d_x b|+|d_y b|)^2 }$")
    ax[i].plot(psi_diag_full, psi_diag_full.depthi, 'black', lw=lw,  label=r"$ \psi_{full} = \frac{\overline{v'b'}  N^2 - \overline{w'b'}  d_y b}{ |\nabla b|^2 }$")
    # ax[i].plot(psi_diag_traditional.isel(fronts=ii), psi_diag_traditional.depthi, 'black', ls=':', lw=lw, label=r"$ \psi = v'b' * N^2 / |N^2|^2 $")
    # ax[i].plot(psi_fox_brg.isel(fronts=ii), psi_fox_brg.depthi, 'red', lw=lw, ls='dashed', label=r'$- C_f ~ \mu_f ~ \frac{H^2 M^2}{f}, C_f=0.06$')
    # ax[i].plot(psi_stone_brg.isel(fronts=ii), psi_stone_brg.depthi, 'red', lw=lw, ls='-.', label=r'$-C_s ~\mu_s ~ \frac{H^2 M^2}{\sqrt{1+Ri}}$'+f', C_s={cs:.2f}')
    ax[i].plot(psi_fox_held_param.isel(fronts=ii), psi_fox_held_param.depthi, 'red', lw=lw, ls='dashed', label=r'$\psi_{held} (Fox)$'+f', C_f={cf:.4f}')
    ax[i].plot(psi_fox_full_param.isel(fronts=ii), psi_fox_full_param.depthi, 'red', lw=lw, label=r'$\psi_{full} (Fox)$'+f', C_f={cf:.4f}')
    ax[i].plot(psi_stone_held_param.isel(fronts=ii), psi_stone_held_param.depthi, 'blue', lw=lw, ls='dashed', label=r'$\psi_{held} (Stone)$'+f', C_s={cs:.2f}')
    ax[i].plot(psi_stone_full_param.isel(fronts=ii), psi_stone_full_param.depthi, 'blue', lw=lw,  label=r'$\psi_{full} (Stone)$'+f', C_s={cs:.2f}')
    ax[i].axhline(Mld, color='r', ls=':', lw=lw)
    ax[i].set_ylim(ylim) # type: ignore
    #ax[i].set_xlim(0,8)
    # ax[i].set_xlim(-1,5)
    ax[i].set_xlabel(r"$ \psi $", fontsize=fs)
    ax[i].legend(loc='lower right', fontsize=15)
    ax[i].grid()
    ax[i].tick_params(labelsize=fl)
    if savefig == True: plt.savefig(f'{fig_path}/vertical_profiles/front{ii+1}structure_func_cross_mld_nov22.png', dpi=100, format='png', bbox_inches='tight')

def plot_profiles_poster(Profiles_full, ii, mask_nan, use_profile_for_param, A, wb_fox_param, wb_stone_param, vb_fox_param, psi_fox_held_param, psi_fox_full_param, psi_stone_held_param, psi_stone_full_param, cf, cs,  fig_path, savefig):
    B   = Profiles_full.isel(fronts=ii)
    n2  = B.n2
    wb  = B.wb
    ub  = B.Vb_along_grad
    vb  = B.Vb_cross_grad
    m2x = B.m2_along_grad
    m2y = B.m2_cross_grad
    Mld = B.mld
    #replacing absolute values
    # m2 = np.sqrt(np.power(B.m2_cross_grad,2) + np.power(B.m2_along_grad,2)) # difference lies in averaging!
    # VB = np.sqrt(np.power(B.Vb_cross_grad,2) + np.power(B.Vb_along_grad,2))
    m2 = np.abs(m2x) + np.abs(m2y) # difference lies in averaging!
    VB = np.abs(ub) + np.abs(vb)
    m2_sign = np.sign(m2y)

    ri_diag_f     = eva.calc_richardsonnumber(B.lat, n2, np.abs(m2y))
    psi_diag_held = eva.calc_streamfunc_held_schneider(B.wb, B.m2_along_grad, B.m2_cross_grad)
    grad_b        = eva.calc_abs_grad_b(B.m2_along_grad, B.m2_cross_grad, B.n2)
    fcoriolis     = eva.calc_coriolis_parameter(B.lat)
    psi_diag_full = eva.calc_streamfunc_full(B.wb, B.m2_cross_grad, B.Vb_cross_grad, B.n2, grad_b)
    if use_profile_for_param== False:  vb_stone_param = A.vb_stone_param.expand_dims(dim={'depthi': mask_nan.isel(fronts=ii) * A.wb_stone_param.depthi}) # type: ignore
    else: vb_stone_param = A.vb_stone_param
    
    fig, ax = plt.subplots(1,5, figsize=(25,12), sharey=True, squeeze=True) #16,7 for 5
    fig.suptitle(rf'Front {ii+1}', fontsize=30)
    ylim = np.array([600, 0])
    lw   = 4
    fs   = 30
    fl   = 25
    ls   = 20
    i=0
    ax[i].semilogx(n2.squeeze(), depthi, color='black', lw=lw, label=r'$N^2$')
    ax[i].legend(loc='lower left', fontsize=ls)
    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(1e-6,5e-5)
    # ax[i].set_xlim(0,1.5e-5)
    ax[i].set_xlabel(r'$N^2 ~ [1/s^2]$ ', fontsize=fs, labelpad=20)
    ax[i].set_ylabel(r'$z ~ [m]$', fontsize=fs)
    ax2 = ax[i].twiny()
    ax2.semilogx(ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])), ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])).depthi, 'black', lw=lw, ls='dashed', label=r'$Ri=\frac{N^2 f^2}{M^4}$')
    ax2.tick_params(labelsize=fl)
    ax2.set_xlabel(r'Ri', fontsize=fs, labelpad=20)
    plt.legend(loc='upper right', fontsize=ls)

    i=1
    ax[i].plot(m2x, depthi, ls='dashed', color='black', lw=lw, label=r'$\overline{dbd\hat{x}}$')
    ax[i].plot(m2y, depthi, color='black', lw=lw, label=r'$\overline{dbd\hat{y}}$')
    # ax[i].plot(m2*m2_sign, depthi, 'black', ls=':', lw=lw, label=r'$- (|\overline{dbd\hat{x}}|+|\overline{dbd\hat{y}}|)$')    
    ax[i].plot(m2*m2_sign, depthi, 'black', ls=':', lw=lw, label=r'$- |\Delta_h \overline{b}|$')    

    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(-7e-8,3e-8)
    ax[i].set_xlabel(r'$M^2 ~ [1/s^2]$ ', fontsize=fs, labelpad=20)
    ax[i].legend(loc='lower left', fontsize=ls)
    ax2 = ax[i].twiny()
    alpha = m2y / (fcoriolis**2)
    # ax2.plot(alpha, depthi, 'red', lw=lw, ls='dashed', label=r'$\alpha = \frac{M^2}{f^2}$')
    low = (-7e-8/(fcoriolis**2)).data
    up = (3e-8/(fcoriolis**2)).data
    ax2.set_xlim(low,up)
    ax2.tick_params(labelsize=fl)
    ax2.set_xlabel(r'$\alpha=M^2/f^2$', fontsize=fs, labelpad=20)
    # plt.legend(loc='upper right', fontsize=ls)

    i=2
    ax[i].plot(wb_fox_param.isel(fronts=ii), wb_fox_param.depthi, color='tab:blue', lw=lw, label = r"$\overline{w'b'}_{Fox-Kemper}$")
    ax[i].plot(wb_stone_param.isel(fronts=ii), wb_stone_param.depthi, color='tab:orange', lw=lw, label = r"$\overline{w'b'}_{Stone}$")
    ax[i].plot(wb, depthi, 'black', lw=lw , label = r"$\overline{w'b'}$")
    ax[i].set_ylim(ylim) # type: ignore
    # ax[i].set_xlim(-0.5e-7,2e-7)
    ax[i].set_xlabel(r"$\overline{w'b'} ~ [m^2/s^3]$ ", fontsize=fs, labelpad=20)
    # ax[i].legend(loc='lower right', fontsize=ls)   

    i = 3
    # ax[i].plot(VB, depthi, color='black', lw=lw, label = r"$\overline{VB}$")
    ax[i].plot(vb_fox_param.isel(fronts=ii), vb_fox_param.depthi, color='tab:blue', lw=lw)
    ax[i].plot(vb_stone_param.isel(fronts=ii), vb_stone_param.depthi, color='tab:orange', lw=lw)
    ax[i].plot(ub, depthi, color='black', lw=lw, ls='dashed', label = r"$\overline{\hat{u}'b'}$")
    ax[i].plot(vb, depthi, color='black', lw=lw, label = r"$\overline{\hat{v}'b'}$")
    ax[i].set_xlabel(r'$\overline{\hat{v}^\prime b^\prime}  ~ [m^2/s^3]$ ', fontsize=fs, labelpad=20)
    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(-0.5e-5,5e-5)
    # ax[i].set_xlabel(r"$\overline{v'b'} & \overline{u'b'} $ ", fontsize=fs)
    ax[i].legend(loc='lower right', fontsize=ls)


    i=4
    # ax[i].plot(psi_diag_held, psi_diag_held.depthi, 'black', lw=lw, ls='dashed', label=r"$ \psi_{held} = - \frac{\overline{w'b'} * d_y b }{ (|d_x b|+|d_y b|)^2 }$")
    # ax[i].plot(psi_diag_traditional.isel(fronts=ii), psi_diag_traditional.depthi, 'black', ls=':', lw=lw, label=r"$ \psi = v'b' * N^2 / |N^2|^2 $")
    # ax[i].plot(psi_fox_brg.isel(fronts=ii), psi_fox_brg.depthi, 'red', lw=lw, ls='dashed', label=r'$- C_f ~ \mu_f ~ \frac{H^2 M^2}{f}, C_f=0.06$')
    # ax[i].plot(psi_stone_brg.isel(fronts=ii), psi_stone_brg.depthi, 'red', lw=lw, ls='-.', label=r'$-C_s ~\mu_s ~ \frac{H^2 M^2}{\sqrt{1+Ri}}$'+f', C_s={cs:.2f}')
    # ax[i].plot(psi_fox_held_param.isel(fronts=ii), psi_fox_held_param.depthi, 'red', lw=lw, ls='dashed', label=r'$\psi_{held} (Fox)$'+f', C_f={cf:.4f}')
    ax[i].plot(psi_diag_full, psi_diag_full.depthi, 'black', lw=lw,  label=r"$ \psi = \frac{\overline{\hat{v'}b'}  ~\overline{d_z b} - \overline{w'b'} ~ \overline{d_y b}}{ |\nabla \overline{b}|^2 }$")
    ax[i].plot(psi_fox_full_param.isel(fronts=ii), psi_fox_full_param.depthi, 'tab:blue', lw=lw, label=r'$\psi_{Fox-Kemper}$')  # r'$\psi_{Fox-Kemper}$'+f', $C_f={cf:.2f}$'
    # ax[i].plot(psi_stone_held_param.isel(fronts=ii), psi_stone_held_param.depthi, 'blue', lw=lw, ls='dashed', label=r'$\psi_{held} (Stone)$'+f', C_s={cs:.2f}')
    ax[i].plot(psi_stone_full_param.isel(fronts=ii), psi_stone_full_param.depthi, 'tab:orange', lw=lw,  label=r'$\psi_{Stone}$') # r'$\psi_{Stone}$'+f', $C_s={cs:.2f}$'
    ax[i].set_ylim(ylim) # type: ignore
    #ax[i].set_xlim(0,8)
    # ax[i].set_xlim(-1,5)
    ax[i].set_xlabel(r"$ \psi ~ [m^2/s] $", fontsize=fs, labelpad=20)
    # ax[i].legend(loc='lower right', fontsize=ls, markerscale=1.5)

    for i in np.arange(5):
        ax[i].text(-0.1, 1.02, '('+string.ascii_lowercase[i]+')', transform=ax[i].transAxes, size=20)
        ax[i].grid()
        ax[i].tick_params(labelsize=fl)
        ax[i].xaxis.get_offset_text().set_fontsize(fl)
        ax[i].axhline(Mld, color='green', ls='--', lw=lw-1, label='MLD')

    handles, labels = ax[i].get_legend_handles_labels()
    labels = ['diagnostic', 'PER', 'ALS', 'MLD']
    ax[i].legend(handles, labels, loc='lower center', fontsize=ls)

    if savefig == True: plt.savefig(f'{fig_path}/vertical_profiles/all/name/front{ii+1:02d}_vert.png', dpi=200, format='png', bbox_inches='tight')


def plot_profiles_poster_clean(Profiles_full, ii, A, fig_path, savefig):
    A   = A.isel(fronts=ii)
    B   = Profiles_full.isel(fronts=ii)
    n2  = B.n2
    wb  = B.wb
    ub  = B.Vb_along_grad
    vb  = B.Vb_cross_grad
    m2x = B.m2_along_grad
    m2y = B.m2_cross_grad
    Mld = B.mld
    m2 = np.abs(m2x) + np.abs(m2y) # difference lies in averaging!

    # ri_diag_f     = eva.calc_richardsonnumber(B.lat, n2, np.abs(m2y))

    fig, ax = plt.subplots(1,5, figsize=(25,12), sharey=True, squeeze=True) #16,7 for 5
    fig.suptitle(rf'Front {ii+1}', fontsize=30)
    # ylim = np.array([600, 0])
    ylim = np.array([1000, 0])
    lw   = 4
    fs   = 30
    fl   = 25
    ls   = 20
    i=0
    ax[i].semilogx(n2.squeeze(), depthi, color='black', lw=lw, label=r'$N^2$')
    ax[i].legend(loc='lower left', fontsize=ls)
    ax[i].set_ylim(ylim) # type: ignore
    # ax[i].set_xlim(1e-6,5e-5)
    ax[i].set_xlim(n2.isel(depthi=slice(0,80)).min(), n2.isel(depthi=slice(0,100)).max())
    ax[i].set_xlabel(r'$N^2 ~ [1/s^2]$ ', fontsize=fs, labelpad=20)
    ax[i].set_ylabel(r'$z ~ [m]$', fontsize=fs)
    ax2 = ax[i].twiny()
    # ax2.semilogx(ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])), ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])).depthi, 'black', lw=lw, ls='dashed', label=r'$Ri=\frac{N^2 f^2}{M^4}$')
    ax2.semilogx(B.ri_diag.sel(depthi=slice(ylim[1], ylim[0])), B.ri_diag.sel(depthi=slice(ylim[1], ylim[0])).depthi, 'black', lw=lw, ls='dashed', label=r'$Ri=\frac{N^2 f^2}{M^4}$')

    ax2.tick_params(labelsize=fl)
    ax2.set_xlabel(r'Ri', fontsize=fs, labelpad=20)
    plt.legend(loc='upper right', fontsize=ls)

    i=1
    ax[i].plot(m2x, depthi, ls='dashed', color='black', lw=lw, label=r'$\overline{dbd\hat{x}}$')
    ax[i].plot(m2y, depthi, color='black', lw=lw, label=r'$\overline{dbd\hat{y}}$')
    ax[i].plot(-m2, depthi, 'black', ls=':', lw=lw, label=r'$- |\Delta_h \overline{b}|$')    

    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(-m2.isel(depthi=slice(0,80)).max(), m2.isel(depthi=slice(0,80)).max())
    ax[i].set_xlabel(r'$M^2 ~ [1/s^2]$ ', fontsize=fs, labelpad=20)
    ax[i].legend(loc='lower left', fontsize=ls)
    ax2 = ax[i].twiny()
    # alpha = m2y / (fcoriolis**2)
    low = (-7e-8/(A.fcoriolis**2)).data
    up = (3e-8/(A.fcoriolis**2)).data
    ax2.set_xlim(low,up)
    ax2.tick_params(labelsize=fl)
    ax2.set_xlabel(r'$\alpha=M^2/f^2$', fontsize=fs, labelpad=20)

    i=2
    ax[i].plot(A.wb_fox_param, A.wb_fox_param.depthi, color='tab:blue', lw=lw, label = r"$\overline{w'b'}_{Fox-Kemper}$")
    ax[i].plot(A.wb_stone_param, A.wb_stone_param.depthi, color='tab:orange', lw=lw, label = r"$\overline{w'b'}_{Stone}$")
    ax[i].plot(wb, depthi, 'black', lw=lw , label = r"$\overline{w'b'}$")
    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlabel(r"$\overline{w'b'} ~ [m^2/s^3]$ ", fontsize=fs, labelpad=20)

    i = 3
    ax[i].plot(A.vb_fox_param, A.vb_fox_param.depthi, color='tab:blue', lw=lw)
    # ax[i].plot(A.vb_stone_param, A.vb_stone_param.depthi, color='tab:orange', lw=lw)
    #plot vlines
    ax[i].vlines(A.vb_stone_param_dmean, 0, A.mld, color='tab:orange', lw=lw)
    ax[i].plot(ub, depthi, color='black', lw=lw, ls='dashed', label = r"$\overline{\hat{u}'b'}$")
    ax[i].plot(vb, depthi, color='black', lw=lw, label = r"$\overline{\hat{v}'b'}$")
    ax[i].set_xlabel(r'$\overline{\hat{v}^\prime b^\prime}  ~ [m^2/s^3]$ ', fontsize=fs, labelpad=20)
    ax[i].set_ylim(ylim) # type: ignore
    # ax[i].set_xlim(-0.5e-5,5e-5)
    ax[i].legend(loc='lower right', fontsize=ls)


    i=4
    ax[i].plot(B.psi_diag_full, B.psi_diag_full.depthi, 'black', lw=lw,  label=r"$ \psi = \frac{\overline{\hat{v'}b'}  ~\overline{d_z b} - \overline{w'b'} ~ \overline{d_y b}}{ |\nabla \overline{b}|^2 }$")
    ax[i].plot(A.psi_fox_full_param, A.psi_fox_full_param.depthi, 'tab:blue', lw=lw, label=r'$\psi_{Fox-Kemper}$')  # r'$\psi_{Fox-Kemper}$'+f', $C_f={cf:.2f}$'
    ax[i].plot(A.psi_stone_full_param, A.psi_stone_full_param.depthi, 'tab:orange', lw=lw,  label=r'$\psi_{Stone}$') # r'$\psi_{Stone}$'+f', $C_s={cs:.2f}$'
    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlabel(r"$ \psi ~ [m^2/s] $", fontsize=fs, labelpad=20)

    for i in np.arange(5):
        ax[i].text(-0.1, 1.02, '('+string.ascii_lowercase[i]+')', transform=ax[i].transAxes, size=20)
        ax[i].grid()
        ax[i].tick_params(labelsize=fl)
        ax[i].xaxis.get_offset_text().set_fontsize(fl)
        ax[i].axhline(Mld, color='green', ls='--', lw=lw-1, label='MLD')

    handles, labels = ax[i].get_legend_handles_labels()
    labels = ['diagnostic', 'PER', 'ALS', 'MLD']
    ax[i].legend(handles, labels, loc='lower center', fontsize=ls)

    if savefig == True: plt.savefig(f'{fig_path}/vertical_profiles/all/name/front{ii+1:02d}_vert.png', dpi=200, format='png', bbox_inches='tight')







def plot_subprofiles(fig, inner_grid, ii, Profiles_full, ds_profile_ml):
    def add_grid_specs(ax,i):
        ax.text(-0.1, 1.02, '('+string.ascii_lowercase[i]+')', transform=ax.transAxes, size=20)
        ax.grid()
        ax.tick_params(labelsize=fl)
        ax.xaxis.get_offset_text().set_fontsize(fl)
        ax.axhline(Mld, color='green', ls='--', lw=lw-1, label='MLD')
        ax.set_ylim(ylim) # type: ignore

    ds   = Profiles_full.isel(fronts=ii)
    n2  = ds.n2
    wb  = ds.wb
    ub  = ds.Vb_along_grad  
    vb  = ds.Vb_cross_grad
    m2x = ds.m2_along_grad
    m2y = ds.m2_cross_grad
    Mld = ds.mld

    ds_profile_ml  = ds_profile_ml.isel(fronts=ii)


    m2            = np.abs(m2x) + np.abs(m2y) # difference lies in averaging!
    m2_sign       = np.sign(m2y)
    ri_diag_f     = eva.calc_richardsonnumber(ds.lat, n2, np.abs(m2y))
    grad_b        = eva.calc_abs_grad_b(ds.m2_along_grad, ds.m2_cross_grad, ds.n2)
    fcoriolis     = eva.calc_coriolis_parameter(ds.lat)
    psi_diag_full = eva.calc_streamfunc_full(ds.wb, ds.m2_cross_grad, ds.Vb_cross_grad, ds.n2, grad_b)

    ylim = np.array([Mld.data*1.6, 5])
    lw   = 4
    fs   = 30
    fl   = 25
    ls   = 20

    ax = fig.add_subplot(inner_grid[0])
    ax.semilogx(n2.squeeze(), depthi, color='black', lw=lw, label=r'$N^2$')
    ax.legend(loc='lower left', fontsize=ls)
    # ax.set_xlim(1e-6,5e-5)
    # set max min range only for mld
    ax.set_xlim(n2.isel(depthi=slice(0,80)).min(), n2.isel(depthi=slice(0,100)).max())
    ax.set_xlabel(r'$N^2 ~ [1/s^2]$ ', fontsize=fs, labelpad=20)
    ax.set_ylabel(r'$z ~ [m]$', fontsize=fs)
    ax2 = ax.twiny()
    ax2.semilogx(ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])), ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])).depthi, 'black', lw=lw, ls='dashed', label=r'$Ri=\frac{N^2 f^2}{M^4}$')
    ax2.tick_params(labelsize=fl)
    ax2.set_xlabel(r'Ri', fontsize=fs, labelpad=20)
    plt.legend(loc='upper right', fontsize=ls)
    add_grid_specs(ax,0)

    ax = fig.add_subplot(inner_grid[1], sharey=ax)
    ax.plot(m2x, depthi, ls='dashed', color='black', lw=lw, label=r'$\overline{dbd\hat{x}}$')
    ax.plot(m2y, depthi, color='black', lw=lw, label=r'$\overline{dbd\hat{y}}$')
    ax.plot(m2*m2_sign, depthi, 'black', ls=':', lw=lw, label=r'$- |\Delta_h \overline{b}|$')    
    # ax.set_xlim(-7e-8,3e-8)
    ax.set_xlim((m2*m2_sign).isel(depthi=slice(0,80)).min(), (m2x).isel(depthi=slice(0,80)).max())
    ax.set_xlabel(r'$M^2 ~ [1/s^2]$ ', fontsize=fs, labelpad=20)
    ax.legend(loc='lower left', fontsize=ls)
    ax2 = ax.twiny()
    low = (-7e-8/(fcoriolis**2)).data
    up = (3e-8/(fcoriolis**2)).data
    ax2.set_xlim(low,up)
    ax2.tick_params(labelsize=fl)
    ax2.set_xlabel(r'$\alpha=M^2/f^2$', fontsize=fs, labelpad=20)
    add_grid_specs(ax,1)
    ax.yaxis.set_visible(False)

    ax = fig.add_subplot(inner_grid[2], sharey=ax)
    ax.plot(ds_profile_ml.wb_fox_param, ds_profile_ml.depthi, color='tab:blue', lw=lw, label = r"$\overline{w'b'}_{Fox-Kemper}$")
    ax.plot(ds_profile_ml.wb_stone_param, ds_profile_ml.depthi, color='tab:orange', lw=lw, label = r"$\overline{w'b'}_{Stone}$")
    ax.plot(wb, depthi, 'black', lw=lw , label = r"$\overline{w'b'}$")
    ax.set_xlabel(r"$\overline{w'b'} ~ [m^2/s^3]$ ", fontsize=fs, labelpad=20)
    add_grid_specs(ax,2)
    ax.yaxis.set_visible(False)

    ax = fig.add_subplot(inner_grid[3], sharey=ax)
    ax.plot(ds_profile_ml.vb_fox_param, ds_profile_ml.depthi, color='tab:blue', lw=lw)
    # ax.vlines(ds_profile_ml.vb_stone_param, 0, ds_profile_ml.mld, color='tab:orange', lw=lw)
    ax.plot(ds_profile_ml.vb_stone_param, ds_profile_ml.depthi, color='tab:orange', lw=lw)

    ax.plot(ub, depthi, color='black', lw=lw, ls='dashed', label = r"$\overline{\hat{u}'b'}$")
    ax.plot(vb, depthi, color='black', lw=lw, label = r"$\overline{\hat{v}'b'}$")
    ax.set_xlabel(r'$\overline{\hat{v}^\prime b^\prime}  ~ [m^2/s^3]$ ', fontsize=fs, labelpad=20)
    # ax.set_xlim(-0.5e-5,2.5e-5)
    ax.legend(loc='lower right', fontsize=ls)
    add_grid_specs(ax,3)
    ax.yaxis.set_visible(False)

    ax = fig.add_subplot(inner_grid[4], sharey=ax)
    ax.plot(psi_diag_full, psi_diag_full.depthi, 'black', lw=lw,  label=r"$ \psi = \frac{\overline{\hat{v'}b'}  ~\overline{d_z b} - \overline{w'b'} ~ \overline{d_y b}}{ |\nabla \overline{b}|^2 }$")
    ax.plot(ds_profile_ml.psi_fox_full_param, ds_profile_ml.depthi, 'tab:blue', lw=lw, label=r'$\psi_{Fox-Kemper}$')  # r'$\psi_{Fox-Kemper}$'+f', $C_f={cf:.2f}$'
    ax.plot(ds_profile_ml.psi_stone_full_param, ds_profile_ml.depthi, 'tab:orange', lw=lw,  label=r'$\psi_{Stone}$') # r'$\psi_{Stone}$'+f', $C_s={cs:.2f}$'
    ax.set_xlabel(r"$ \psi ~ [m^2/s] $", fontsize=fs, labelpad=20)
    add_grid_specs(ax,4)
    ax.yaxis.set_visible(False)

    handles, labels = ax.get_legend_handles_labels()
    labels = ['diagnostic', 'PER', 'ALS', 'MLD']
    ax.legend(handles, labels, loc='lower center', fontsize=ls)
    ax.set_title(f'F{ds.fronts.values}', fontsize=45, loc='right')

    return fig



def plot_subprofiles_clean(fig, inner_grid, ii, Profiles_full, ds_profile_ml):
    def add_grid_specs(ax,i):
        ax.text(-0.1, 1.02, '('+string.ascii_lowercase[i]+')', transform=ax.transAxes, size=20)
        ax.grid()
        ax.tick_params(labelsize=fl)
        ax.xaxis.get_offset_text().set_fontsize(fl)
        ax.axhline(Mld, color='green', ls='--', lw=lw-1, label='MLD')
        ax.set_ylim(ylim) # type: ignore

    ds   = Profiles_full.isel(fronts=ii)
    n2  = ds.n2
    wb  = ds.wb
    ub  = ds.Vb_along_grad
    vb  = ds.Vb_cross_grad
    m2x = ds.m2_along_grad
    m2y = ds.m2_cross_grad
    Mld = ds.mld
    m2 = np.abs(m2x) + np.abs(m2y) # difference lies in averaging!

    ri_diag_f     = eva.calc_richardsonnumber(ds.lat, n2, np.abs(m2y))


    cs_med        = ds_profile_ml.cs
    cf_med        = ds_profile_ml.cf
    ds_profile_ml = ds_profile_ml.isel(fronts=ii)
    cals          = ds_profile_ml.cs_ind.values/cs_med
    cper          = ds_profile_ml.cf_ind.values/cf_med



    ylim = np.array([Mld.data*1.6, 0])
    lw   = 4
    fs   = 30
    fl   = 25
    ls   = 20

    ax = fig.add_subplot(inner_grid[0])
    ax.semilogx(n2.squeeze(), depthi, color='black', lw=lw, label=r'$N^2$')
    ax.legend(loc='lower left', fontsize=ls)
    ax.set_ylim(ylim) # type: ignore
    # ax.set_xlim(1e-6,5e-5)
    ax.set_xlim(n2.isel(depthi=slice(0,80)).min(), n2.isel(depthi=slice(0,100)).max())
    ax.set_xlabel(r'$N^2 ~ [1/s^2]$ ', fontsize=fs, labelpad=20)
    ax.set_ylabel(r'$z ~ [m]$', fontsize=fs)
    ax2 = ax.twiny()
    ax2.semilogx(ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])), ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])).depthi, 'black', lw=lw, ls='dashed', label=r'$Ri=\frac{N^2 f^2}{M^4}$')
    ax2.tick_params(labelsize=fl)
    ax2.set_xlabel(r'Ri', fontsize=fs, labelpad=20)
    plt.legend(loc='upper right', fontsize=ls)
    add_grid_specs(ax,0)

    ax = fig.add_subplot(inner_grid[1], sharey=ax)
    ax.plot(m2x, depthi, ls='dashed', color='black', lw=lw, label=r'$\overline{dbd\hat{x}}$')
    ax.plot(m2y, depthi, color='black', lw=lw, label=r'$\overline{dbd\hat{y}}$')
    ax.plot(-m2, depthi, 'black', ls=':', lw=lw, label=r'$- |\Delta_h \overline{b}|$')    
    ax.set_ylim(ylim) # type: ignore
    # ax.set_xlim(-7e-8,3e-8)
    ax.set_xlim((-m2).isel(depthi=slice(0,80)).min(), (m2x).isel(depthi=slice(0,80)).max())
    ax.set_xlabel(r'$M^2 ~ [1/s^2]$ ', fontsize=fs, labelpad=20)
    ax.legend(loc='lower left', fontsize=ls)
    ax2 = ax.twiny()
    low = (-7e-8/(ds_profile_ml.fcoriolis**2)).data
    up = (3e-8/(ds_profile_ml.fcoriolis**2)).data
    ax2.set_xlim(low,up)
    ax2.tick_params(labelsize=fl)
    ax2.set_xlabel(r'$\alpha=M^2/f^2$', fontsize=fs, labelpad=20)
    add_grid_specs(ax,1)
    ax.yaxis.set_visible(False)

    ax = fig.add_subplot(inner_grid[2], sharey=ax)
    ax.plot(ds_profile_ml.wb_fox_param, ds_profile_ml.wb_fox_param.depthi, color='tab:blue', lw=lw, label = r"$\overline{w'b'}_{Fox-Kemper}$")
    ax.plot(ds_profile_ml.wb_stone_param, ds_profile_ml.wb_stone_param.depthi, color='tab:orange', lw=lw, label = r"$\overline{w'b'}_{Stone}$")
    ax.plot(wb, depthi, 'black', lw=lw , label = r"$\overline{w'b'}$")
    ax.set_ylim(ylim) # type: ignore
    ax.set_xlabel(r"$\overline{w'b'} ~ [m^2/s^3]$ ", fontsize=fs, labelpad=20)
    add_grid_specs(ax,2)
    ax.yaxis.set_visible(False)

    str1 = fr'$C_{{per}}/\overline{{C_{{per}}}}={cper:.1f}$, $C_{{als}}/\overline{{C_{{als}}}}={cals:.1f}$'
    ax.text(0.5, 1.1, str1, transform=ax.transAxes, fontsize=20, verticalalignment='top', horizontalalignment='center', color='r')


    ax = fig.add_subplot(inner_grid[3], sharey=ax)
    ax.plot(ds_profile_ml.vb_fox_param, ds_profile_ml.vb_fox_param.depthi, color='tab:blue', lw=lw)
    ax.vlines(ds_profile_ml.vb_stone_param_dmean, 0, ds_profile_ml.mld, color='tab:orange', lw=lw)
    ax.plot(ub, depthi, color='black', lw=lw, ls='dashed', label = r"$\overline{\hat{u}'b'}$")
    ax.plot(vb, depthi, color='black', lw=lw, label = r"$\overline{\hat{v}'b'}$")
    ax.set_xlabel(r'$\overline{\hat{v}^\prime b^\prime}  ~ [m^2/s^3]$ ', fontsize=fs, labelpad=20)
    ax.set_ylim(ylim) # type: ignore
    # ax.set_xlim(-0.5e-5,5e-5)
    ax.legend(loc='lower right', fontsize=ls)
    add_grid_specs(ax,3)
    ax.yaxis.set_visible(False)

    ax = fig.add_subplot(inner_grid[4], sharey=ax)
    ax.plot(ds.psi_diag_full, ds.psi_diag_full.depthi, 'black', lw=lw,  label=r"$ \psi = \frac{\overline{\hat{v'}b'}  ~\overline{d_z b} - \overline{w'b'} ~ \overline{d_y b}}{ |\nabla \overline{b}|^2 }$")
    ax.plot(ds_profile_ml.psi_fox_full_param, ds_profile_ml.psi_fox_full_param.depthi, 'tab:blue', lw=lw, label=r'$\psi_{Fox-Kemper}$')  # r'$\psi_{Fox-Kemper}$'+f', $C_f={cf:.2f}$'
    ax.plot(ds_profile_ml.psi_stone_full_param, ds_profile_ml.psi_stone_full_param.depthi, 'tab:orange', lw=lw,  label=r'$\psi_{Stone}$') # r'$\psi_{Stone}$'+f', $C_s={cs:.2f}$'
    ax.set_ylim(ylim) # type: ignore
    ax.set_xlabel(r"$ \psi ~ [m^2/s] $", fontsize=fs, labelpad=20)

    add_grid_specs(ax,4)
    ax.yaxis.set_visible(False)

    handles, labels = ax.get_legend_handles_labels()
    labels = ['diagnostic', 'PER', 'ALS', 'MLD']
    ax.legend(handles, labels, loc='lower center', fontsize=ls)
    ax.set_title(f'F{ds.fronts.values}', fontsize=45, loc='right')

    return fig


def plot_profiles_poster_osm(Profiles_full, ii, mask_nan, use_profile_for_param, A, wb_fox_param, wb_stone_param, vb_fox_param, psi_fox_held_param, psi_fox_full_param, psi_stone_held_param, psi_stone_full_param, cf, cs,  fig_path, savefig):
    B   = Profiles_full.isel(fronts=ii)
    n2  = B.n2
    wb  = B.wb
    ub  = B.Vb_along_grad
    vb  = B.Vb_cross_grad
    m2x = B.m2_along_grad
    m2y = B.m2_cross_grad
    Mld = B.mld

    #replacing absolute values
    m2 = np.abs(m2x) + np.abs(m2y) # difference lies in averaging!
    VB = np.abs(ub) + np.abs(vb)
    m2_sign = np.sign(m2y)

    ri_diag_f     = eva.calc_richardsonnumber(B.lat, n2, np.abs(m2y))
    psi_diag_held = eva.calc_streamfunc_held_schneider(B.wb, B.m2_along_grad, B.m2_cross_grad)
    grad_b        = eva.calc_abs_grad_b(B.m2_along_grad, B.m2_cross_grad, B.n2)
    fcoriolis     = eva.calc_coriolis_parameter(B.lat)
    psi_diag_full = eva.calc_streamfunc_full(B.wb, B.m2_cross_grad, B.Vb_cross_grad, B.n2, grad_b)
    if use_profile_for_param== False:  vb_stone_param = A.vb_stone_param.expand_dims(dim={'depthi': mask_nan.isel(fronts=ii) * A.wb_stone_param.depthi}) # type: ignore
    else: vb_stone_param = A.vb_stone_param
    
    fig, ax = plt.subplots(1,5, figsize=(25,12), sharey=True, squeeze=True) #16,7 for 5
    # fig.suptitle(rf'Front {ii+1}', fontsize=30)
    ylim = np.array([600, 0])
    lw   = 5
    fs   = 30
    fl   = 30
    ls   = 20
    i=0
    ax[i].semilogx(n2.squeeze(), depthi, color='black', lw=lw, label=r'$N^2$')
    # ax[i].legend(loc='lower left', fontsize=ls)
    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(1e-6,5e-5)
    # ax[i].set_xlim(0,1.5e-5)
    ax[i].set_title(r'$N^2 ~ [1/s^2]$ ', fontsize=fs)
    ax[i].set_ylabel(r'$z ~ [m]$', fontsize=fs)
    # ax2 = ax[i].twiny()
    # ax2.semilogx(ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])), ri_diag_f.sel(depthi=slice(ylim[1], ylim[0])).depthi, 'black', lw=lw, ls='dashed', label=r'$Ri=\frac{N^2 f^2}{M^4}$')
    # ax2.tick_params(labelsize=fl)
    # ax2.set_xlabel(r'Ri', fontsize=fs, labelpad=20)
    # plt.legend(loc='upper right', fontsize=ls)

    i=1
    # ax[i].plot(m2x, depthi, ls='dashed', color='black', lw=lw, label=r'$\overline{dbd\hat{x}}$')
    ax[i].plot(m2y, depthi, color='black', lw=lw, label=r'$\overline{dbd\hat{y}}$')
    # ax[i].plot(m2*m2_sign, depthi, 'black', ls=':', lw=lw, label=r'$- (|\overline{dbd\hat{x}}|+|\overline{dbd\hat{y}}|)$')    
    # ax[i].plot(m2*m2_sign, depthi, 'black', ls=':', lw=lw, label=r'$- |\Delta_h \overline{b}|$')    

    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(-7e-8,3e-8)
    ax[i].set_title(r'$M^2 ~ [1/s^2]$ ', fontsize=fs)
    # ax[i].legend(loc='lower left', fontsize=ls)

    i=2
    ax[i].plot(wb, depthi, 'black', lw=lw , label = r"$\overline{w'b'}$")
    # ax[i].plot(wb_fox_param.isel(fronts=ii), wb_fox_param.depthi, color='tab:blue', lw=5, label = r"$\overline{w'b'}_{Fox-Kemper}$")
    # ax[i].plot(wb_stone_param.isel(fronts=ii), wb_stone_param.depthi, color='tab:orange', lw=5, label = r"$\overline{w'b'}_{Stone}$")
    ax[i].set_ylim(ylim) # type: ignore
    # ax[i].set_xlim(-0.5e-7,2e-7)
    ax[i].set_title(r"$\overline{w'b'} ~ [m^2/s^3]$ ", fontsize=fs)
    # ax[i].legend(loc='lower right', fontsize=ls)   

    i = 3
    # ax[i].plot(ub, depthi, color='black', lw=lw, ls='dashed', label = r"$\overline{\hat{x}'b'}$")
    ax[i].plot(vb, depthi, color='black', lw=lw, label = r"$\overline{\hat{v}'b'}$")
    # ax[i].plot(VB, depthi, color='black', lw=lw, label = r"$\overline{VB}$")
    # ax[i].plot(vb_fox_param.isel(fronts=ii), vb_fox_param.depthi, color='tab:blue', lw=5)
    # ax[i].plot(vb_stone_param.isel(fronts=ii), vb_stone_param.depthi, color='tab:orange', lw=5)
    ax[i].set_title(r'$\overline{\hat{v}^\prime b^\prime}  ~ [m^2/s^3]$ ', fontsize=fs)
    ax[i].set_ylim(ylim) # type: ignore
    ax[i].set_xlim(-0.5e-5,5e-5)
    # ax[i].set_xlabel(r"$\overline{v'b'} & \overline{u'b'} $ ", fontsize=fs)
    # ax[i].legend(loc='lower right', fontsize=ls)


    i=4
    # ax[i].plot(psi_diag_held, psi_diag_held.depthi, 'black', lw=lw, ls='dashed', label=r"$ \psi_{held} = - \frac{\overline{w'b'} * d_y b }{ (|d_x b|+|d_y b|)^2 }$")
    ax[i].plot(psi_diag_full, psi_diag_full.depthi, 'black', lw=lw,  label=r"$ \psi = \frac{\overline{\hat{v'}b'}  ~\overline{d_z b} - \overline{w'b'} ~ \overline{d_y b}}{ |\nabla \overline{b}|^2 }$")
    # ax[i].plot(psi_diag_traditional.isel(fronts=ii), psi_diag_traditional.depthi, 'black', ls=':', lw=lw, label=r"$ \psi = v'b' * N^2 / |N^2|^2 $")
    # ax[i].plot(psi_fox_brg.isel(fronts=ii), psi_fox_brg.depthi, 'red', lw=lw, ls='dashed', label=r'$- C_f ~ \mu_f ~ \frac{H^2 M^2}{f}, C_f=0.06$')
    # ax[i].plot(psi_stone_brg.isel(fronts=ii), psi_stone_brg.depthi, 'red', lw=lw, ls='-.', label=r'$-C_s ~\mu_s ~ \frac{H^2 M^2}{\sqrt{1+Ri}}$'+f', C_s={cs:.2f}')
    # ax[i].plot(psi_fox_held_param.isel(fronts=ii), psi_fox_held_param.depthi, 'red', lw=lw, ls='dashed', label=r'$\psi_{held} (Fox)$'+f', C_f={cf:.4f}')
    # ax[i].plot(psi_fox_full_param.isel(fronts=ii), psi_fox_full_param.depthi, 'tab:blue', lw=lw, label=r'$\psi_{Fox-Kemper}$')  # r'$\psi_{Fox-Kemper}$'+f', $C_f={cf:.2f}$'
    # ax[i].plot(psi_stone_held_param.isel(fronts=ii), psi_stone_held_param.depthi, 'blue', lw=lw, ls='dashed', label=r'$\psi_{held} (Stone)$'+f', C_s={cs:.2f}')
    # ax[i].plot(psi_stone_full_param.isel(fronts=ii), psi_stone_full_param.depthi, 'tab:orange', lw=lw,  label=r'$\psi_{Stone}$') # r'$\psi_{Stone}$'+f', $C_s={cs:.2f}$'
    ax[i].set_ylim(ylim) # type: ignore
    #ax[i].set_xlim(0,8)
    # ax[i].set_xlim(-1,5)
    ax[i].set_title(r"$ \psi ~ [m^2/s] $", fontsize=fs)
    # ax[i].legend(loc='lower right', fontsize=ls, markerscale=1.5)

    for i in np.arange(5):
        # ax[i].text(-0.00, 1.02, string.ascii_lowercase[i]+')', transform=ax[i].transAxes, size=20, weight='bold')
        ax[i].grid()
        ax[i].tick_params(labelsize=fl)
        ax[i].xaxis.get_offset_text().set_fontsize(fl)
        ax[i].axhline(Mld, color='green', ls='--', lw=lw-1, label='MLD')

    handles, labels = ax[i].get_legend_handles_labels()
    labels = ['diagnostic', 'Fox-Kemper', 'Stone', 'MLD']
    ax[i].legend(handles, labels, loc='lower center', fontsize=ls)

    if savefig == True: plt.savefig(f'{fig_path}/vertical_profiles/front{ii+1}structure_osm_sans.png', dpi=200, format='png', bbox_inches='tight')


# def plot_slice_front_psi_full(x, y, wb, vb, mldx, mldx_monte, rho, m2x, m2y, n2, lat_mean, dl, cf_opt0, cs_opt0, grain, ylim=[5000,0], title=None, fig_path=None, savefig=False):
#     """plot vertical parameters of front"""
#     fig, ax = plt.subplots(3, 1, figsize=(12,15), sharex=True,)
#     lw      = 4
#     color   = 'r'
#     ls      = 'dashed'
#     fs      = 25
#     fl      = 15
#     depth_s = (np.abs(depthi - ylim[0])).argmin()

#     psi_held_diag = eva.calc_streamfunc_held_schneider(wb, m2x, m2y)
#     psi_held_diag = psi_held_diag.isel(depthi=slice(0,depth_s))
#     grad_b        = eva.calc_abs_grad_b(m2x, m2y, n2)
#     psi_full_diag = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)

#     cf                 = cf_opt0
#     cs                 = cs_opt0
#     if grain == True:
#         cf = 0.04272727272727273
#         cs = 0.2828282828282829

#     #mean values should be used here as well!!
#     ri_diag            = eva.calc_richardsonnumber(lat_mean, n2, m2y)
#     # ri_diag            = eva.calc_richardsonnumber(lat_mean, n2.mean(dim='depthi', skipna=True), m2y.mean(dim='depthi', skipna=True))
#     wb_fox_param       = eva.calc_wb_fox_param(lat_mean, mldx, m2y, -m2y.depthi, cf, structure=True )
#     vb_fox_param       = eva.calc_vb_fox_param(lat_mean, mldx, m2y, ri_diag, -m2y.depthi, cf, structure=True )
#     psi_fox_full_param = eva.calc_streamfunc_full(wb_fox_param, m2y, vb_fox_param, n2, grad_b)

#     wb_stone_param       = eva.calc_wb_stone_param(lat_mean, mldx, m2y, ri_diag, cs, -m2y.depthi)
#     vb_stone_param       = eva.calc_vb_stone_param(lat_mean, mldx, m2y, ri_diag, cs)
#     psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param, m2y, vb_stone_param, n2, grad_b)


#     scale = 1.2
#     data  = psi_fox_full_param.isel(depthi=slice(0,depth_s))#.where((psi_fox_full_param.lat > psi_fox_full_param.lat.min() + dl) & (psi_fox_full_param.lat < psi_fox_full_param.lat.max() - dl))
#     # data  = psi_full_diag.isel(depthi=slice(0,depth_s))#.where((psi_fox_full_param.lat > psi_fox_full_param.lat.min() + dl) & (psi_fox_full_param.lat < psi_fox_full_param.lat.max() - dl))
#     data  = data.where(psi_fox_full_param.depthi < mldx, drop=True)
#     data  = data.isel(crossfront=slice(dl[0], dl[1]))
#     cmax  = np.nanmax(data)
#     cmin  = np.nanmin(data)
#     if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
#     else: ulimit = -scale*cmin; llimit = scale*cmin
#     levels = np.linspace(llimit,ulimit, 15)

#     i = 0
#     cf = ax[i].contourf(x, y, psi_full_diag.data, levels, cmap='PiYG_r', extend='both')
#     cb = fig.colorbar(cf, ax=ax[i])
#     levels_iso = np.linspace(1025,1027,120)
#     ax[i].contour(x, rho.depthi, rho.data, levels_iso, colors='gray', label='rho contour')
#     ax[i].plot([], [], 'grey', label="Isopycnal")
#     ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
#     ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
#     # ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
#     ax[i].axvline(dl[0], color='black')
#     ax[i].axvline(dl[1], color='black')
#     ax[i].legend(loc='lower right')
#     ax[i].set_ylim(ylim)
#     ax[i].set_title(r"$ \psi = (v'b'  N^2 - w'b'  d_y b) / |\nabla b|^2 $ diagonised",fontsize=fs)
#     ax[i].tick_params(labelsize=fl)

#     i = 1
#     data  = psi_fox_full_param.isel(depthi=slice(0,depth_s)).data
#     cf = ax[i].contourf(x, y, psi_fox_full_param.data, levels, cmap='PiYG_r', extend='both')
#     cb = fig.colorbar(cf, ax=ax[i])
#     ax[i].contour(x, rho.depthi, rho.data, levels_iso, colors='gray', label='rho contour')
#     ax[i].plot([], [], 'grey', label="Isopycnal")
#     ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
#     ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
#     # ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
#     ax[i].axvline(dl[0], color='black')
#     ax[i].axvline(dl[1], color='black')
#     ax[i].legend(loc='lower right')
#     ax[i].set_ylim(ylim)
#     ax[i].set_title(r"$ \psi $ Fox-Kemper Param.",fontsize=fs)
#     ax[i].tick_params(labelsize=fl)

#     i = 2
#     data  = psi_stone_full_param.isel(depthi=slice(0,depth_s)).data#psi_full.data
#     levels = np.linspace(llimit,ulimit, 15)
#     cf = ax[i].contourf(x, y, psi_stone_full_param.data, levels, cmap='PiYG_r', extend='both')
#     cb = fig.colorbar(cf, ax=ax[i])
#     ax[i].contour(x, rho.depthi, rho.data, levels_iso, colors='gray', label='rho contour')
#     ax[i].plot([], [], 'grey', label="Isopycnal")
#     ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
#     ax[i].plot(x, mldx_monte, color='purple', linewidth=lw, linestyle=ls, label='MLD rho=0.03')
#     # ax[i].axhline(argo_mld, color='tab:green', ls=ls, linewidth=lw, label=f'MLD Argo monthly {int(argo_mld)}m')
#     ax[i].axvline(dl[0], color='black')
#     ax[i].axvline(dl[1], color='black')
#     ax[i].legend(loc='lower right')
#     ax[i].set_ylim(ylim)
#     ax[i].set_title(r"$ \psi $ Stone Param.",fontsize=fs)
#     ax[i].set_xlabel(r'$\hat{x}$', fontsize=fs)
#     ax[i].tick_params(labelsize=fl)

#     for i in np.arange(3):
#         ax[i].set_xlim(15,90)

#     if savefig == True: plt.savefig(f'{fig_path}', dpi=100, format='png', bbox_inches='tight')


def plot_slice_front_psi_full(x, y, wb, vb, mldx, mldx_monte, rho, m2x, m2y, n2, lat_mean, dl, cf_opt0, cs_opt0, grain, ylim=[5000,0], title=None, fig_path=None, savefig=False):
    """plot vertical parameters of front"""
    fig, ax = plt.subplots(3, 1, figsize=(12,15), sharex=True, constrained_layout=True)
    lw      = 4
    color   = 'green'
    ls      = 'dashed'
    fs      = 25
    fl      = 15
    depth_s = (np.abs(depthi - ylim[0])).argmin()

    psi_held_diag = eva.calc_streamfunc_held_schneider(wb, m2x, m2y)
    psi_held_diag = psi_held_diag.isel(depthi=slice(0,depth_s))
    grad_b        = eva.calc_abs_grad_b(m2x, m2y, n2)
    psi_full_diag = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)

    cf                 = cf_opt0
    cs                 = cs_opt0
    if grain == True:
        cf = 0.04272727272727273
        cs = 0.2828282828282829

    #mean values should be used here as well!!
    ri_diag            = eva.calc_richardsonnumber(lat_mean, n2, m2y)
    # ri_diag            = eva.calc_richardsonnumber(lat_mean, n2.mean(dim='depthi', skipna=True), m2y.mean(dim='depthi', skipna=True))
    wb_fox_param       = eva.calc_wb_fox_param(lat_mean, mldx, m2y, -m2y.depthi, cf, structure=True )
    vb_fox_param       = eva.calc_vb_fox_param(lat_mean, mldx, m2y, ri_diag, -m2y.depthi, cf, structure=True )
    psi_fox_full_param = eva.calc_streamfunc_full(wb_fox_param, m2y, vb_fox_param, n2, grad_b)
    psi_fox_held_param = eva.calc_streamfunc_held_schneider(wb_fox_param, m2x, m2y)

    wb_stone_param       = eva.calc_wb_stone_param(lat_mean, mldx, m2y, ri_diag, cs, -m2y.depthi, structure=True)
    vb_stone_param       = eva.calc_vb_stone_param(lat_mean, mldx, m2y, ri_diag, cs)
    psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param, m2y, vb_stone_param, n2, grad_b)
    psi_stone_held_param = eva.calc_streamfunc_held_schneider(wb_stone_param, m2x, m2y)

    scale = 0.7
    data  = psi_fox_full_param.isel(depthi=slice(0,depth_s))#.where((psi_fox_full_param.lat > psi_fox_full_param.lat.min() + dl) & (psi_fox_full_param.lat < psi_fox_full_param.lat.max() - dl))
    # data  = psi_full_diag.isel(depthi=slice(0,depth_s))#.where((psi_fox_full_param.lat > psi_fox_full_param.lat.min() + dl) & (psi_fox_full_param.lat < psi_fox_full_param.lat.max() - dl))
    data  = data.where(psi_fox_full_param.depthi < mldx, drop=True)
    data  = data.isel(crossfront=slice(dl[0], dl[1]))
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)

    i = 0
    cf = ax[i].contourf(x, y, psi_full_diag.data, levels, cmap='PiYG_r', extend='both')
    ax[i].set_title(r"$ \psi = (\bar{v'b'}  d_zb - \bar{w'b'}  d_y b) / |\nabla b|^2 $",fontsize=fs)
    ax[i].set_ylabel('diagonised', fontsize=fs)

    i += 1
    data  = psi_fox_full_param.isel(depthi=slice(0,depth_s)).data
    cf = ax[i].contourf(x, y, psi_fox_full_param.data, levels, cmap='PiYG_r', extend='both')
    ax[i].set_ylabel(" Fox-Kemper param.", fontsize=fs)

    i += 1
    data  = psi_stone_full_param.isel(depthi=slice(0,depth_s)).data#psi_full.data
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, psi_stone_full_param.data, levels, cmap='PiYG_r', extend='both')
    ax[i].set_ylabel(" Stone param.", fontsize=fs)
    ax[i].set_xlabel(r'$\hat{x}$', fontsize=fs)


    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.82, 0.25, 0.025, 0.5])
    fig.colorbar(cf, cax=cbar_ax, shrink= 0.6)

    levels_iso = np.linspace(1025,1027,120)
    for i in np.arange(3):
        ax[i].contour(x, rho.depthi, rho.data, levels_iso, colors='gray', label='rho contour')
        ax[i].plot([], [], 'grey', label="Isopycnal")
        ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label=r'MLD $\rho=0.2$')
        ax[i].axvline(dl[0], color='black')
        ax[i].axvline(dl[1], color='black')
        ax[i].set_xlim(15,90)
        ax[i].set_ylim(ylim)
        ax[i].tick_params(labelsize=fl)
    ax[i].legend(loc='lower right')

    if savefig == True: plt.savefig(f'{fig_path}', dpi=150, format='png', bbox_inches='tight')

def plot_slice_front_psi_full_held(x, y, wb, vb, mldx, mldx_monte, rho, m2x, m2y, n2, lat_mean, dl, cf_opt0, cs_opt0, grain, ylim=[5000,0], title=None, fig_path=None, savefig=False):
    """plot vertical parameters of front"""
    fig, ax = plt.subplots(3, 2, figsize=(20,15), sharex=True, sharey=True, constrained_layout=True)
    lw      = 4
    color   = 'green'
    ls      = 'dashed'
    fs      = 25
    fl      = 15
    depth_s = (np.abs(depthi - ylim[0])).argmin()

    psi_held_diag = eva.calc_streamfunc_held_schneider(wb, m2x, m2y)
    # psi_held_diag = psi_held_diag.isel(depthi=slice(0,depth_s))
    grad_b        = eva.calc_abs_grad_b(m2x, m2y, n2)
    psi_full_diag = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)

    cf                 = cf_opt0
    cs                 = cs_opt0
    if grain == True:
        cf = 0.04272727272727273
        cs = 0.2828282828282829

    #mean values should be used here as well!!
    ri_diag            = eva.calc_richardsonnumber(lat_mean, n2, m2y)
    # ri_diag            = eva.calc_richardsonnumber(lat_mean, n2.mean(dim='depthi', skipna=True), m2y.mean(dim='depthi', skipna=True))
    wb_fox_param       = eva.calc_wb_fox_param(lat_mean, mldx, m2y, -m2y.depthi, cf, structure=True )
    vb_fox_param       = eva.calc_vb_fox_param(lat_mean, mldx, m2y, ri_diag, -m2y.depthi, cf, structure=True )
    psi_fox_full_param = eva.calc_streamfunc_full(wb_fox_param, m2y, vb_fox_param, n2, grad_b)
    psi_fox_held_param = eva.calc_streamfunc_held_schneider(wb_fox_param, m2x, m2y)

    wb_stone_param       = eva.calc_wb_stone_param(lat_mean, mldx, m2y, ri_diag, cs, -m2y.depthi, structure=True)
    vb_stone_param       = eva.calc_vb_stone_param(lat_mean, mldx, m2y, ri_diag, cs)
    psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param, m2y, vb_stone_param, n2, grad_b)
    psi_stone_held_param = eva.calc_streamfunc_held_schneider(wb_stone_param, m2x, m2y)

    scale = 0.7
    data  = psi_fox_full_param.isel(depthi=slice(0,depth_s))#.where((psi_fox_full_param.lat > psi_fox_full_param.lat.min() + dl) & (psi_fox_full_param.lat < psi_fox_full_param.lat.max() - dl))
    # data  = psi_full_diag.isel(depthi=slice(0,depth_s))#.where((psi_fox_full_param.lat > psi_fox_full_param.lat.min() + dl) & (psi_fox_full_param.lat < psi_fox_full_param.lat.max() - dl))
    data  = data.where(psi_fox_full_param.depthi < mldx, drop=True)
    data  = data.isel(crossfront=slice(dl[0], dl[1]))
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)

    i = 0
    j = 0
    cf = ax[i,j].contourf(x, y, psi_full_diag.data, levels, cmap='PiYG_r', extend='both')
    ax[i,j].set_title(r"$ \psi = (\bar{v'b'}  d_zb - \bar{w'b'}  d_y b) / |\nabla b|^2 $",fontsize=fs)
    ax[i,j].set_ylabel('diagonised', fontsize=fs)

    i += 1
    cf = ax[i,j].contourf(x, y, psi_fox_full_param.data, levels, cmap='PiYG_r', extend='both')
    ax[i,j].set_ylabel(" Fox-Kemper param.", fontsize=fs)

    i += 1
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i,j].contourf(x, y, psi_stone_full_param.data, levels, cmap='PiYG_r', extend='both')
    ax[i,j].set_ylabel(" Stone param.", fontsize=fs)
    ax[i,j].set_xlabel(r'$\hat{x}$', fontsize=fs)
    cb = fig.colorbar(cf, ax=ax[i,j], location='bottom', shrink=0.6)


    #---------
    data  = psi_fox_held_param.isel(depthi=slice(0,depth_s))#.where((psi_fox_full_param.lat > psi_fox_full_param.lat.min() + dl) & (psi_fox_full_param.lat < psi_fox_full_param.lat.max() - dl))
    data  = data.where(psi_fox_full_param.depthi < mldx, drop=True)
    data  = data.isel(crossfront=slice(dl[0], dl[1]))
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)

    i = 0
    j = 1
    cf = ax[i,j].contourf(x, y, psi_held_diag.data, levels, cmap='PiYG_r', extend='both')
    ax[i,j].set_title(r"$ \psi = - \bar{w'b'}  d_y b / |\nabla b|^2 $",fontsize=fs)

    i += 1
    cf = ax[i,j].contourf(x, y, psi_fox_held_param.data, levels, cmap='PiYG_r', extend='both')

    i += 1
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i,j].contourf(x, y, psi_stone_held_param.data, levels, cmap='PiYG_r', extend='both')
    ax[i,j].set_xlabel(r'$\hat{x}$', fontsize=fs)
    cb = fig.colorbar(cf, ax=ax[i,j], location='bottom', shrink=0.6)


    levels_iso = np.linspace(1025,1027,120)
    for i in np.arange(3):
        for j in np.arange(2):
            ax[i,j].contour(x, rho.depthi, rho.data, levels_iso, colors='gray', label='rho contour')
            ax[i,j].plot([], [], 'grey', label="Isopycnal")
            ax[i,j].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label=r'MLD $\rho=0.2$')
            ax[i,j].axvline(dl[0], color='black')
            ax[i,j].axvline(dl[1], color='black')
            ax[i,j].set_xlim(15,90)
            ax[i,j].set_ylim(ylim)
            ax[i,j].tick_params(labelsize=fl)
    ax[i,j].legend(loc='lower right')

    if savefig == True: plt.savefig(f'{fig_path}', dpi=250, format='png', bbox_inches='tight')



    # %%
# compare slice parameters

def plot_slice_front_wide(x, y, vb, wb, mldx, rho, m2x, m2y, n2, lat, data_coarse, dl, ylim=[5000,0], fig_path=None, savefig=False):
    """plot vertical parameters of front"""
    fig, ax = plt.subplots(5, 1, figsize=(12,18), sharex=True)

    ri = n2 * np.power(eva.calc_coriolis_parameter(lat),2) / np.power(m2y,2)

    color   = 'r'
    ls      = 'dashed'
    fs      = 20
    depth_s = 83

    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_held = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)

    s = m2y.crossfront.shape[0]
    front_lower_limit = int(s/2)-int(s*dl)
    front_upper_limit = int(s/2)+int(s*dl)
    print(s)
    print(front_lower_limit, front_upper_limit)
    dst = 0.02

    i=0

    # i+=1
    lw     = 4
    color  = 'green'
    scale  = 0.7
    data_p = m2y.isel(depthi=slice(0,depth_s))
    data   = data_p
    cmax   = np.nanmax(data)
    cmin   = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), data, levels, cmap='PuOr_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i], pad = dst)
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].set_ylim(ylim)
    ax[i].set_title(r'M^2  ~ [1/s^2]$',fontsize=fs)

    i+=1
    scale  = 1
    data_p = ri.isel(depthi=slice(0,depth_s))
    levels = np.linspace(0,50, 19)
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), data_p, levels,  cmap='Blues_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i], pad = dst)
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].set_title(r'$ Ri $',fontsize=fs)

    i+=1
    scale  = 1
    data_p = wb.isel(depthi=slice(0,depth_s))
    cmax   = np.nanmax(data_p)
    cmin   = np.nanmin(data_p)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), data_p, levels, cmap='RdBu_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i], pad = dst)
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].set_title(r'$ \overline{w^{\prime} b^{\prime}}  ~ [m^2/s^3]$',fontsize=fs)

    i+=1
    scale = 0.7
    data  = vb.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(x, y, vb.data, levels,  cmap='RdBu_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i], pad = dst)
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \overline{\hat{v}'b'}   ~ [m^2/s^3] $",fontsize=fs)

    i+=1
    scale = 0.7
    data  = psi_held.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, psi_held.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i], pad = dst)
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label=r'MLD with: $\Delta \rho=0.2$')
    ax[i].legend(loc='lower right', fontsize=15)
    ax[i].set_title(r"$ \psi   ~ [m^2/s]$",fontsize=fs)
    ax[i].set_xlabel(r'$\hat{y}$', fontsize=fs)

    for i in np.arange(5):
        ax[i].tick_params(labelsize=15)
        levels = np.linspace(1025,1027,120)
        ax[i].contour(x, rho.depthi, rho.data, levels, colors='gray', label='rho contour')
        ax[i].axvline(front_lower_limit, color='black')
        ax[i].axvline(front_upper_limit, color='black')
        ax[i].set_ylabel(r'$z ~ [m]$', fontsize=fs, rotation=90)
        ax[i].set_ylim(ylim)
        ax[i].set_xlim(15,90)
        ax[i].xaxis.label.set_size(fs)
        ax[i].text(-0.00, 1.02, string.ascii_lowercase[i]+')', transform=ax[i].transAxes, size=15, weight='bold')


    if savefig == True: plt.savefig(fig_path, dpi=250, format='png', bbox_inches='tight')



def plot_slice_front_wide_pyic(x, y, vb, wb, mldx, rho, m2x, m2y, n2, lat, data_coarse, dl, ylim=[5000,0], fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(1, 5, plot_cb=True, asp=0.3, fig_size_fac=0.9, sharex=True, sharey=False, daxt=0.9) # type: ignore
    ii=-1

    ri       = n2 * np.power(eva.calc_coriolis_parameter(lat),2) / np.power(m2y,2)
    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_held = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)

    # s                 = m2y.crossfront.shape[0]
    # front_lower_limit = int(s/2)-int(s*dl)
    # front_upper_limit = int(s/2)+int(s*dl)
    s                 = m2y.crossfront.shape[0]*(x[2]-x[1])
    front_lower_limit = int(s/2)-int(s*dl)*(x[2]-x[1])
    front_upper_limit = int(s/2)+int(s*dl)*(x[2]-x[1])
    depth_s           = 83
    nclev             = 20
    cticks            = 6
    scale             = 3

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = m2y.isel(depthi=slice(0,depth_s))
    clim = np.array([-6e-8,6e-8]) * scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p, ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PuOr_r', extend='both')
    ax.set_title(r'$M^2  ~ [1/s^2]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = ri.isel(depthi=slice(0,depth_s))
    levels = np.linspace(0,50, 19)
    pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True,  clim=[0,50], cmap='Blues_r', extend='both')
    ax.set_title(r'$ Ri $')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = wb.isel(depthi=slice(0,depth_s))
    clim = np.array([-2e-7,2e-7]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.set_title(r'$ \overline{w^{\prime} b^{\prime}}  ~ [m^2/s^3]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-5e-5,5e-5]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, vb.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.plot([], [], 'grey', label="Isopycnal")
    ax.set_title(r"$ \overline{\hat{v}'b'}   ~ [m^2/s^3] $")
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-10,10]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    pyic.shade(x, y, psi_held.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi   ~ [m^2/s]$")
    ax.set_xlabel(r'$\hat{y} [km]$')

    for ax in hca:
        levels = np.linspace(1025,1027,120)
        ax.contour(x, rho.depthi, rho.data, levels, colors='gray', linewidths=0.75, label='rho contour')
        ax.axvline(front_lower_limit, color='black')
        ax.axvline(front_upper_limit, color='black')
        ax.set_ylabel(r'$z ~ [m]$', rotation=90)
        ax.set_ylim(ylim)
        # ax.set_xlim(15,90)
        # ax.set_xlim(20,65)
        ax.plot(x, mldx, color='green', linewidth=2.5, linestyle='dashed', label=r'MLD with: $\Delta \rho=0.2$')
        ax.set_xlim(28,162)

    ax.plot([], [], 'grey', label="Isopycnal")
    ax.legend(loc='lower right', fontsize = 8)


    if savefig == True: plt.savefig(fig_path, dpi=250, format='png', bbox_inches='tight')

def plot_slice_front_wide_pyic_2(x, y, vb, wb, mldx, rho, m2x, m2y, n2, lat, data_coarse, dl, ylim=[5000,0], fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(2, 3, plot_cb=True, asp=0.6, fig_size_fac=1, sharex=True, sharey=True, daxt=0.9, f_dcbr = 1.2, reverse_order=True) # type: ignore
    ii=-1

    ri       = n2 * np.power(eva.calc_coriolis_parameter(lat),2) / np.power(m2y,2)
    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_held = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)

    # s                 = m2y.crossfront.shape[0]
    # front_lower_limit = int(s/2)-int(s*dl)
    # front_upper_limit = int(s/2)+int(s*dl)
    s                 = m2y.crossfront.shape[0]*(x[2]-x[1])
    front_lower_limit = int(s/2)-int(s*dl)*(x[2]-x[1])
    front_upper_limit = int(s/2)+int(s*dl)*(x[2]-x[1])
    depth_s           = 83
    nclev             = 20
    cticks            = 6
    scale             = 0.5

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = m2y.isel(depthi=slice(0,depth_s))
    clim = np.array([-9e-8,9e-8]) * scale 
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p, ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PuOr_r', extend='both')
    ax.set_title(r'$M^2  ~ [1/s^2]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = n2.isel(depthi=slice(0,depth_s))
    clim = np.array([1e-7,2e-5]) * scale * 1.5
    cbticks = np.linspace(clim[0],clim[1],cticks)
    # logaritmic scale
    # cbticks = np.logspace(np.log10(clim[0]), np.log10(clim[1]), cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p, ax=ax, cax=cax, contfs=False, nclev=nclev, cbticks=cbticks,  logplot=False, clim=clim,  cmap='Spectral_r', extend='both')
    ax.set_title(r'$N^2  ~ [1/s^2]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = ri.isel(depthi=slice(0,depth_s))
    levels = np.linspace(0,20, 19)
    pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True,  clim=[0,20], cmap='Blues_r', extend='both')
    ax.set_title(r'$ Ri $')
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)
    ax.set_xlabel(r'$\hat{y} [km]$')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = wb.isel(depthi=slice(0,depth_s))
    clim = np.array([-2e-7,2e-7]) * scale *0.5
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.set_title(r'$ \overline{w^{\prime} b^{\prime}}  ~ [m^2/s^3]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-6e-5,6e-5]) * scale *0.5
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, vb.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.plot([], [], 'grey', label="Isopycnal")
    ax.set_title(r"$ \overline{\hat{v}'b'}   ~ [m^2/s^3] $")
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-12,12]) * scale * 0.5
    cbticks = np.linspace(clim[0],clim[1],cticks)
    pyic.shade(x, y, psi_held.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi   ~ [m^2/s]$")
    ax.set_xlabel(r'$\hat{y} [km]$')
    

    for ax in hca:
        levels = np.linspace(1025,1027,120)
        ax.contour(x, rho.depthi, rho.data, levels, colors='gray', linewidths=0.75, label='rho contour')
        ax.axvline(front_lower_limit, color='black')
        ax.axvline(front_upper_limit, color='black')
    
        ax.set_ylim(ylim)
        # ax.set_xlim(15,90)
        # ax.set_xlim(20,65)
        # ax.set_xlim(28,162)
        # ax.set_xlim(94-35,94+35)
        ax.set_xlim(40,130)
        ax.plot(x, mldx, color='green', linewidth=2.5, linestyle='dashed', label=r'MLD')

    ax.plot([], [], 'grey', label="Isopycnal")
    ax.legend(loc='lower right', fontsize = 8)


    if savefig == True: plt.savefig(fig_path, dpi=250, format='png', bbox_inches='tight')


def plot_slice_front_wide_pyic_2_front9(x, y, vb, wb, mldx, rho, m2x, m2y, n2, lat, data_coarse, dl, ylim=[5000,0], fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(2, 3, plot_cb=True, asp=0.6, fig_size_fac=1, sharex=True, sharey=True, daxt=0.9, f_dcbr = 1.2, reverse_order=True) # type: ignore
    ii=-1

    ri       = n2 * np.power(eva.calc_coriolis_parameter(lat),2) / np.power(m2y,2)
    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_held = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)

    # s                 = m2y.crossfront.shape[0]
    # front_lower_limit = int(s/2)-int(s*dl)
    # front_upper_limit = int(s/2)+int(s*dl)
    s                 = m2y.crossfront.shape[0]*(x[2]-x[1])
    front_lower_limit = int(s/2)-int(s*dl)*(x[2]-x[1])
    front_upper_limit = int(s/2)+int(s*dl)*(x[2]-x[1])
    depth_s           = 83
    nclev             = 20
    cticks            = 6
    scale             = 0.5

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = m2y.isel(depthi=slice(0,depth_s))
    clim = np.array([-9e-8,9e-8]) * scale 
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p, ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PuOr_r', extend='both')
    ax.set_title(r'$M^2  ~ [1/s^2]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = n2.isel(depthi=slice(0,depth_s))
    clim = np.array([1e-7,2e-5]) * scale * 1.5
    cbticks = np.linspace(clim[0],clim[1],cticks)
    # logaritmic scale
    # cbticks = np.logspace(np.log10(clim[0]), np.log10(clim[1]), cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p, ax=ax, cax=cax, contfs=False, nclev=nclev, cbticks=cbticks,  logplot=False, clim=clim,  cmap='Spectral_r', extend='both')
    ax.set_title(r'$N^2  ~ [1/s^2]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = ri.isel(depthi=slice(0,depth_s))
    levels = np.linspace(0,20, 19)
    pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True,  clim=[0,20], cmap='Blues_r', extend='both')
    ax.set_title(r'$ Ri $')
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)
    ax.set_xlabel(r'$\hat{y} [km]$')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = wb.isel(depthi=slice(0,depth_s))
    clim = np.array([-2e-7,2e-7]) * scale *0.5
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.set_title(r'$ \overline{w^{\prime} b^{\prime}}  ~ [m^2/s^3]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-6e-5,6e-5]) * scale *0.5
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, vb.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.plot([], [], 'grey', label="Isopycnal")
    ax.set_title(r"$ \overline{\hat{v}'b'}   ~ [m^2/s^3] $")
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-12,12]) * scale * 0.5
    cbticks = np.linspace(clim[0],clim[1],cticks)
    pyic.shade(x, y, psi_held.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi   ~ [m^2/s]$")
    ax.set_xlabel(r'$\hat{y} [km]$')
    

    for ax in hca:
        levels = np.linspace(1025,1027,120)
        ax.contour(x, rho.depthi, rho.data, levels, colors='gray', linewidths=0.75, label='rho contour')
        ax.axvline(front_lower_limit, color='black')
        ax.axvline(front_upper_limit, color='black')
    
        ax.set_ylim(ylim)
        # ax.set_xlim(15,90)
        # ax.set_xlim(20,65)
        # ax.set_xlim(28,162)
        # ax.set_xlim(94-35,94+35)
        ax.set_xlim(40,130)
        ax.plot(x, mldx, color='green', linewidth=2.5, linestyle='dashed', label=r'MLD')

    ax.plot([], [], 'grey', label="Isopycnal")
    ax.legend(loc='lower right', fontsize = 8)


    if savefig == True: plt.savefig(fig_path, dpi=250, format='png', bbox_inches='tight')



def plot_slice_front_wide_pyic_2_front4(x, y, vb, wb, mldx, rho, m2x, m2y, n2, lat, data_coarse, dl, ylim=[5000,0], fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(2, 3, plot_cb=True, asp=0.6, fig_size_fac=1, sharex=True, sharey=True, daxt=0.9, f_dcbr = 1.2, reverse_order=True) # type: ignore
    ii=-1

    ri       = n2 * np.power(eva.calc_coriolis_parameter(lat),2) / np.power(m2y,2)
    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_held = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)

    # s                 = m2y.crossfront.shape[0]
    # front_lower_limit = int(s/2)-int(s*dl)
    # front_upper_limit = int(s/2)+int(s*dl)
    s                 = m2y.crossfront.shape[0]*(x[2]-x[1])
    front_lower_limit = int(s/2)-int(s*dl)*(x[2]-x[1])
    front_upper_limit = int(s/2)+int(s*dl)*(x[2]-x[1])
    depth_s           = 83
    nclev             = 20
    cticks            = 6
    scale             = 0.6

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = m2y.isel(depthi=slice(0,depth_s))
    clim = np.array([-9e-8,9e-8]) * scale 
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p, ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PuOr_r', extend='both')
    ax.set_title(r'$M^2  ~ [1/s^2]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = n2.isel(depthi=slice(0,depth_s))
    clim = np.array([1e-7,2e-5]) * scale *1.5
    cbticks = np.linspace(clim[0],clim[1],cticks)
    # logaritmic scale
    # cbticks = np.logspace(np.log10(clim[0]), np.log10(clim[1]), cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p, ax=ax, cax=cax, contfs=False, nclev=nclev, cbticks=cbticks,  logplot=False, clim=clim,  cmap='Spectral_r', extend='both')
    ax.set_title(r'$N^2  ~ [1/s^2]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = ri.isel(depthi=slice(0,depth_s))
    levels = np.linspace(0,20, 19)
    pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True,  clim=[0,20], cmap='Blues_r', extend='both')
    ax.set_title(r'$ Ri $')
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)
    ax.set_xlabel(r'$\hat{y} [km]$')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = wb.isel(depthi=slice(0,depth_s))
    clim = np.array([-2e-7,2e-7]) * scale *1.2
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.set_title(r'$ \overline{w^{\prime} b^{\prime}}  ~ [m^2/s^3]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-6e-5,6e-5]) * scale *0.8
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, vb.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.plot([], [], 'grey', label="Isopycnal")
    ax.set_title(r"$ \overline{\hat{v}'b'}   ~ [m^2/s^3] $")
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-12,12]) * scale 
    cbticks = np.linspace(clim[0],clim[1],cticks)
    pyic.shade(x, y, psi_held.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi   ~ [m^2/s]$")
    ax.set_xlabel(r'$\hat{y} [km]$')
    

    for ax in hca:
        levels = np.linspace(1025,1027,120)
        ax.contour(x, rho.depthi, rho.data, levels, colors='gray', linewidths=0.75, label='rho contour')
        ax.axvline(front_lower_limit, color='black')
        ax.axvline(front_upper_limit, color='black')
    
        ax.set_ylim(ylim)
        # ax.set_xlim(15,90)
        # ax.set_xlim(20,65)
        # ax.set_xlim(28,162)
        # ax.set_xlim(94-35,94+35)
        ax.set_xlim(30,170)
        ax.plot(x, mldx, color='green', linewidth=2.5, linestyle='dashed', label=r'MLD')

    ax.plot([], [], 'grey', label="Isopycnal")
    ax.legend(loc='lower right', fontsize = 8)


    if savefig == True: plt.savefig(fig_path, dpi=250, format='png', bbox_inches='tight')


def plot_slice_front_wide_pyic_2_hor(x, y, vb, wb, mldx, rho, m2x, m2y, n2, lat, data_coarse, dl, ylim=[5000,0], fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(1, 6, plot_cb=True, asp=0.25, fig_size_fac=1, sharex=True, sharey=True, daxt=0.9, f_dcbr = 1.2, reverse_order=True) # type: ignore
    ii=-1

    ri       = n2 * np.power(eva.calc_coriolis_parameter(lat),2) / np.power(m2y,2)
    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_held = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)

    # s                 = m2y.crossfront.shape[0]
    # front_lower_limit = int(s/2)-int(s*dl)
    # front_upper_limit = int(s/2)+int(s*dl)
    s                 = m2y.crossfront.shape[0]*(x[2]-x[1])
    front_lower_limit = int(s/2)-int(s*dl)*(x[2]-x[1])
    front_upper_limit = int(s/2)+int(s*dl)*(x[2]-x[1])
    depth_s           = 83
    nclev             = 20
    cticks            = 6
    scale             = 1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = m2y.isel(depthi=slice(0,depth_s))
    clim = np.array([-9e-8,9e-8]) * scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p, ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PuOr_r', extend='both')
    ax.set_title(r'$M^2  ~ [1/s^2]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = n2.isel(depthi=slice(0,depth_s))
    clim = np.array([1e-7,2e-5]) * scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    # logaritmic scale
    # cbticks = np.logspace(np.log10(clim[0]), np.log10(clim[1]), cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p, ax=ax, cax=cax, contfs=False, nclev=nclev, cbticks=cbticks,  logplot=False, clim=clim,  cmap='Spectral_r', extend='both')
    ax.set_title(r'$N^2  ~ [1/s^2]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = ri.isel(depthi=slice(0,depth_s))
    levels = np.linspace(0,20, 19)
    pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True,  clim=[0,20], cmap='Blues_r', extend='both')
    ax.set_title(r'$ Ri $')
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)


    ii+=1; ax=hca[ii]; cax=hcb[ii]
    data_p = wb.isel(depthi=slice(0,depth_s))
    clim = np.array([-1e-7,1e-7]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.set_title(r'$ \overline{w^{\prime} b^{\prime}}  ~ [m^2/s^3]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-6e-5,6e-5]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, vb.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.plot([], [], 'grey', label="Isopycnal")
    ax.set_title(r"$ \overline{\hat{v}'b'}   ~ [m^2/s^3] $")
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-5 ,5]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    pyic.shade(x, y, psi_held.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi   ~ [m^2/s]$")
    ax.set_xlabel(r'$\hat{y} [km]$')
    

    for ax in hca:
        levels = np.linspace(1025,1027,60)
        ax.contour(x, rho.depthi, rho.data, levels, colors='gray', linewidths=0.75, label='rho contour')
        # ax.axvline(front_lower_limit, color='black')
        # ax.axvline(front_upper_limit, color='black')
    
        ax.set_ylim(ylim)
        # ax.set_xlim(15,90)
        # ax.set_xlim(20,65)
        # ax.set_xlim(28,162)
        # ax.set_xlim(94-35,94+35)
        ax.plot(x, mldx, color='green', linewidth=2.5, linestyle='dashed', label=r'MLD')

    ax.plot([], [], 'grey', label="Isopycnal")
    ax.legend(loc='lower right', fontsize = 8)


    if savefig == True: plt.savefig(fig_path, dpi=250, format='png', bbox_inches='tight')


def plot_overturning(x, y, vb, wb, mldx, rho, m2x, m2y, n2, lat, data_coarse, dl, ylim=[5000,0], fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=.75, fig_size_fac=1.6, sharex=True, sharey=False, daxt=0.9, axlab_kw=None) # type: ignore
    ii=-1

    # ri       = n2 * np.power(eva.calc_coriolis_parameter(lat),2) / np.power(m2y,2)
    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_held = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)

    s                 = m2y.crossfront.shape[0]
    front_lower_limit = int(s/2)-int(s*dl)
    front_upper_limit = int(s/2)+int(s*dl)
    # depth_s           = 83
    nclev             = 20
    cticks            = 6
    scale             = 1

    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # data_p = m2y.isel(depthi=slice(0,depth_s))
    # clim = np.array([-6e-8,6e-8]) * scale
    # cbticks = np.linspace(clim[0],clim[1],cticks)
    # hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p, ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PuOr_r', extend='both')
    # ax.set_title(r'$M^2  ~ [1/s^2]$')
    # hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    # hm[1].formatter.set_useMathText(True)

    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # data_p = ri.isel(depthi=slice(0,depth_s))
    # levels = np.linspace(0,50, 19)
    # pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True,  clim=[0,50], cmap='Blues_r', extend='both')
    # ax.set_title(r'$ Ri $')

    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # data_p = wb.isel(depthi=slice(0,depth_s))
    # clim = np.array([-2e-7,2e-7]) /scale
    # cbticks = np.linspace(clim[0],clim[1],cticks)
    # hm = pyic.shade(x, y.isel(depthi=slice(0,depth_s)), data_p,  ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    # ax.set_title(r'$ \overline{w^{\prime} b^{\prime}}  ~ [m^2/s^3]$')
    # hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    # hm[1].formatter.set_useMathText(True)

    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # clim = np.array([-5e-5,5e-5]) /scale
    # cbticks = np.linspace(clim[0],clim[1],cticks)
    # hm = pyic.shade(x, y, vb.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    # ax.plot([], [], 'grey', label="Isopycnal")
    # ax.set_title(r"$ \overline{\hat{v}'b'}   ~ [m^2/s^3] $")
    # hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    # hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-10,10]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    pyic.shade(x, y, psi_held.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi   ~ [m^2/s]$")
    ax.set_xlabel(r'$\hat{y}$')

    for ax in hca:
        levels = np.linspace(1025,1027,120)
        ax.contour(x, rho.depthi, rho.data, levels, colors='gray', linewidths=0.75, label='rho contour')
        ax.axvline(front_lower_limit, color='black')
        ax.axvline(front_upper_limit, color='black')
        ax.set_ylabel(r'$z ~ [m]$', rotation=90)
        ax.set_ylim(ylim)
        ax.set_xlim(35,68)
        # ax.set_xlim(20,65)
        ax.plot(x, mldx, color='green', linewidth=2.5, linestyle='dashed', label=r'MLD with: $\Delta \rho=0.2$')

    ax.plot([], [], 'grey', label="Isopycnal")
    ax.legend(loc='lower right')


    if savefig == True: plt.savefig(fig_path, dpi=250, format='png', bbox_inches='tight')



def plot_overturning_comp(x, y, vb, wb, mldx, rho, m2x, m2y, n2, lat, data_coarse, dl, ylim=[5000,0], fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(3, 1, plot_cb=True, asp=.75, fig_size_fac=1.6, sharex=True, sharey=True, daxt=0.9, axlab_kw=None) # type: ignore
    ii=-1

    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_full = (vb * n2 - wb * m2y) / np.power(grad_b, 2)
    psi_wb   = - wb * m2y / np.power(grad_b, 2)
    psi_vb   = vb * n2 / np.power(grad_b, 2)

    s                 = m2y.crossfront.shape[0]
    front_lower_limit = int(s/2)-int(s*dl)
    front_upper_limit = int(s/2)+int(s*dl)
    # depth_s           = 83
    nclev             = 20
    cticks            = 6
    scale             = 1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-10,10]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    pyic.shade(x, y, psi_vb.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks, clim=clim,  cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi = \frac{\overline{v^\prime b^\prime} N^2}{\Delta b^2}   ~ [m^2/s]$")
    ax.set_xlabel(r'$\hat{y}$')
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-5e-4,5e-4]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, psi_wb.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks, clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi= \frac{-\overline{w^\prime b^\prime} M^2}{\Delta b^2}   ~ [m^2/s]$")
    ax.set_xlabel(r'$\hat{y}$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-10,10]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    pyic.shade(x, y, psi_full.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi= \frac{\overline{v^\prime b^\prime} N^2 - \overline{w^\prime b^\prime} M^2}{\Delta b^2}   ~ [m^2/s]$")
    ax.set_xlabel(r'$\hat{y}$')

    for ax in hca:
        levels = np.linspace(1025,1027,120)
        ax.contour(x, rho.depthi, rho.data, levels, colors='gray', linewidths=0.75, label='rho contour')
        ax.axvline(front_lower_limit, color='black')
        ax.axvline(front_upper_limit, color='black')
        ax.set_ylim(ylim)
        ax.set_xlim(35,68)
        # ax.set_xlim(20,65)
        ax.plot(x, mldx, color='green', linewidth=2.5, linestyle='dashed', label=r'MLD with: $\Delta \rho=0.2$')

    ax.plot([], [], 'grey', label="Isopycnal")
    ax.legend(loc='lower right')


    if savefig == True: plt.savefig(fig_path, dpi=250, format='png', bbox_inches='tight')


def plot_overturning_contribution(x, y, vb, wb, mldx, rho, m2x, m2y, n2, lat, data_coarse, dl, ylim=[5000,0], fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(4, 1, plot_cb=True, asp=.75, fig_size_fac=1.6, sharex=True, sharey=True, daxt=0.9, axlab_kw=None) # type: ignore
    ii=-1

    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_full = (vb * n2 - wb * m2y) / np.power(grad_b, 2)
    psi_wb   = - wb * m2y #/ np.power(grad_b, 2)
    psi_vb   = vb * n2 #/ np.power(grad_b, 2)

    s                 = m2y.crossfront.shape[0]
    front_lower_limit = int(s/2)-int(s*dl)
    front_upper_limit = int(s/2)+int(s*dl)
    # depth_s           = 83
    nclev             = 20
    cticks            = 6
    scale             = 1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([0,1e-5]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, grad_b.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='Spectral_r', extend='max')
    ax.set_title(r"$ {\Delta b^2}   ~ [1/s^2]$")
    ax.set_xlabel(r'$\hat{y}$')
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # clim = np.array([-10,10]) /scale
    clim = np.array([-5e-10,5e-10]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, psi_vb.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks, clim=clim,  cmap='RdBu_r', extend='both')
    ax.set_title(r"$\overline{v^\prime b^\prime} N^2}   ~ [m^2/s^5]$")
    ax.set_xlabel(r'$\hat{y}$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # clim = np.array([-5e-4,5e-4]) /scale
    clim = np.array([-1e-14,1e-14]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, psi_wb.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks, clim=clim, cmap='RdBu_r', extend='both')
    ax.set_title(r"$  -\overline{w^\prime b^\prime} M^2}   ~ [m^2/s^5]$")
    ax.set_xlabel(r'$\hat{y}$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-10,10]) /scale
    cbticks = np.linspace(clim[0],clim[1],cticks)
    pyic.shade(x, y, psi_full.data,  ax=ax, cax=cax, contfs=True,  nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi= \frac{\overline{v^\prime b^\prime} N^2 - \overline{w^\prime b^\prime} M^2}{\Delta b^2}   ~ [m^2/s]$")
    ax.set_xlabel(r'$\hat{y}$')


    for ax in hca:
        levels = np.linspace(1025,1027,120)
        ax.contour(x, rho.depthi, rho.data, levels, colors='gray', linewidths=0.75, label='rho contour')
        ax.axvline(front_lower_limit, color='black')
        ax.axvline(front_upper_limit, color='black')
        ax.set_ylim(ylim)
        ax.set_xlim(35,68)
        # ax.set_xlim(20,65)
        ax.plot(x, mldx, color='green', linewidth=2.5, linestyle='dashed', label=r'MLD with: $\Delta \rho=0.2$')

    ax.plot([], [], 'grey', label="Isopycnal")
    ax.legend(loc='lower right')


    if savefig == True: plt.savefig(fig_path, dpi=250, format='png', bbox_inches='tight')



def plot_slice_eddie(x, y, vb, wb, mldx, rho, m2x, m2y, n2, lat, ylim=[5000,0], fig_path=None, savefig=False):
    """plot vertical parameters of eddie"""
    hca, hcb = pyic.arrange_axes(3, 1, plot_cb='right', asp=1, fig_size_fac=1.2, sharey=True, axlab_kw=None) # type: ignore
    ii=-1

    color   = 'green'
    ls      = 'dashed'
    lw      = 4
    clim    = np.array([-10,10])
    nclev   = 20
    cticks  = 6
    cbticks = np.linspace(clim[0],clim[1],cticks)

    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_diag = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)
    # calc psi fox-kemper
    H       = mldx
    z       = - rho.depthi
    Ri      = n2 * np.power(eva.calc_coriolis_parameter(lat),2) / np.power(m2y,2)
    wb_fox  = eva.calc_wb_fox_param(lat, H, m2y, z, 0.06, structure = True)
    vb_fox  = eva.calc_vb_fox_param(lat, H, m2y, Ri, z, structure = True)
    psi_fox = eva.calc_streamfunc_full(wb_fox, m2y, vb_fox.data, n2, grad_b)
    # calc psi Stone
    wb_stone  = eva.calc_wb_stone_param(lat, H, m2y, Ri, 0.33, z, structure = True)
    vb_stone  = eva.calc_vb_stone_param(lat, H, m2y, Ri, 0.33)
    psi_stone = eva.calc_streamfunc_full(wb_stone, m2y, vb_stone.data, n2, grad_b)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(x, y, psi_diag.data, ax=ax, cax = cax, contfs=True, nclev=nclev, cbticks=cbticks, clim=clim, cmap='PiYG_r', extend='both')
    # ax.legend(loc='lower right', fontsize = 3)
    ax.set_title(r"$ \psi_{diag} $")
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(x, y, psi_fox.where(y < mldx, np.nan).data, ax=ax, cax = cax, contfs=True, nclev=nclev, cbticks=cbticks, clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi_{Fox-Kemper}   $")

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(x, y, psi_stone.where(y < mldx, np.nan).data, ax=ax, cax = cax, contfs=True, nclev=nclev, cbticks=cbticks, clim=clim, cmap='PiYG_r', extend='both', cbtitle=r'[m$^2$/s]'+' \n ')
    ax.set_title(r"$ \psi_{Stone} $")

    for ax in hca:
        levels = np.linspace(1025,1030,120)
        ax.plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label=r'MLD')
        ax.plot([], [], 'grey', label="Isopycnal")
        ax.contour(x, rho.depthi, rho.data, levels, colors='gray', label='rho contour')
        ax.set_ylim(ylim)
        ax.set_xlabel(r'$y [km]$')
        # ax.set_xlim(20,60)
        ax.set_xlim(35,112)
        # ax.set_xlim(50,200)
        # ax.xaxis.label.set_size(fs)
    
    ax.legend(loc='lower center', fontsize = 8)
    figstr = ['(b)','(c)','(d)']
    eva.add_string_like_pyic(hca, figstr)

    if savefig == True: plt.savefig(fig_path, dpi=250, format='png', bbox_inches='tight')




def plot_slice_front_wide_map(x, y, alpha, vb, wb, mldx, rho, m2x, m2y, n2, data_coarse, dl, ylim=[5000,0], fig_path=None, savefig=False):
    """plot vertical parameters of front"""
    fig, ax = plt.subplots(5, 1, figsize=(12,22))

 
    color   = 'r'
    ls      = 'dashed'
    fs      = 20
    depth_s = 83

    grad_b   = eva.calc_abs_grad_b(m2x=m2x, m2y= m2y, n2=n2)
    psi_held = eva.calc_streamfunc_full(wb, m2y, vb.data, n2, grad_b)

    s = alpha.crossfront.shape[0]
    dl = 0.1
    front_lower_limit = int(s/2)-int(s*dl)
    front_upper_limit = int(s/2)+int(s*dl)
    print(front_lower_limit, front_upper_limit)
    dst = 0.02

    i=0
    cmap = 'BuPu'

    llimit = 0
    ulimit = 1e-7
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(data_coarse.lon, data_coarse.lat, data_coarse.data, levels, cmap=cmap, extend='both')
    cb = fig.colorbar(cf, ax=ax[i], pad = dst)
    ax[i].set_title(r'$|dbdx|+|db/dy| $',fontsize=fs)

    points = eva.get_points_of_inclined_fronts()
    P = points[0:2,:]
    Y, X, m, b = eva.calc_line(P)
    delta=0.8*0.1 # 0.8 view 0.1 actual front
    color = 'r'
    lw = 3
    ls = ':'
    ax[i].plot(X,Y+delta, lw=lw, color=color, ls=ls)
    ax[i].plot(X,Y-delta, lw=lw, color=color, ls=ls)
    ax[i].vlines(X[0],(Y+delta)[0], (Y-delta)[0], lw=lw, color=color, ls=ls)
    ax[i].vlines(X[-1],(Y+delta)[-1], (Y-delta)[-1], lw=lw, color=color, ls=ls)
    t = ax[i].text(P[0,0]-0.05, P[0,1]+0.2, f'F{1}', color='black', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
 
    i+=1
    lw      = 4
    ls      = 'dashed'
    color='green'
    scale  = 0.7
    data_p = alpha.isel(depthi=slice(0,depth_s))
    data = data_p
    cmax   = np.nanmax(data)
    cmin   = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), data, levels, cmap='PuOr_r', extend='both')
    # cf = ax[i].contourf(x, y[0,depth_s], alpha.data, levels, cmap='PuOr_r', extend='both')
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthi, rho.data, levels, colors='gray', label='rho contour')
    cb = fig.colorbar(cf, ax=ax[i], pad = dst)
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    ax[i].set_ylim(ylim)
    ax[i].set_title(r'$db/d\hat{y} $',fontsize=fs)

    i+=1
    scale  = 1.5
    data_p = wb.isel(depthi=slice(0,depth_s))
    cmax   = np.nanmax(data)
    cmin   = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(x, y.isel(depthi=slice(0,depth_s)), data_p, levels, cmap='RdBu_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i], pad = dst)
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls)
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthi, rho.data, levels, colors='gray', label='rho contour')
    ax[i].set_title(r'$ \overline{w^{\prime} b^{\prime}}$',fontsize=fs)

    i+=1
    scale = 0.7
    data  = vb.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 19)
    cf = ax[i].contourf(x, y, vb.data, levels,  cmap='RdBu_r', extend='both')
    # cb = fig.colorbar(cf, ax=ax[i], format=ticker.FuncFormatter(fmt))
    cb = fig.colorbar(cf, ax=ax[i], pad = dst)
    # cb.formatter.set_powerlimits((0, 0))
    # cb.formatter.set_useMathText(True)
    # cb.update_ticks()
    # cb.ax.tick_params(labelsize=fs)
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label='MLD rho=0.2')
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthi, rho.data, levels, colors='gray', label='rho contour')
    ax[i].set_ylim(ylim)
    ax[i].set_title(r"$ \overline{\hat{v}'b'} $",fontsize=fs)

    i+=1
    scale = 0.7
    data  = psi_held.data
    cmax  = np.nanmax(data)
    cmin  = np.nanmin(data)
    if np.abs(cmax) > np.abs(cmin): ulimit = scale*cmax; llimit = -scale*cmax
    else: ulimit = -scale*cmin; llimit = scale*cmin
    levels = np.linspace(llimit,ulimit, 15)
    cf = ax[i].contourf(x, y, psi_held.data, levels, cmap='PiYG_r', extend='both')
    cb = fig.colorbar(cf, ax=ax[i], pad = dst)
    levels = np.linspace(1025,1027,120)
    ax[i].contour(x, rho.depthi, rho.data, levels, colors='gray', label='rho contour')
    ax[i].plot([], [], 'grey', label="Isopycnal")
    ax[i].plot(x, mldx, color=color, linewidth=lw, linestyle=ls, label=r'MLD with: $\Delta \rho=0.2$')
    ax[i].legend(loc='lower right')
    # ax[i].set_title(r"$ \psi = ({\overline{\hat{v}'b'}}  d_z b - \overline{w'b'}   d_\hat{y} b) / |\nabla b|^2 $",fontsize=fs)
    ax[i].set_title(r"$ \psi$",fontsize=fs)
    ax[i].set_xlabel(r'$\hat{x}$', fontsize=fs)

    # fig.subplots_adjust(top=1.1)
    for i in np.arange(5):
        ax[i].tick_params(labelsize='large')
        if i == 0: continue
        ax[i].axvline(front_lower_limit, color='black')
        ax[i].axvline(front_upper_limit, color='black')
        ax[i].set_ylim(ylim)
        ax[i].set_xlim(15,90)

    if savefig == True: plt.savefig(fig_path, dpi=250, format='png', bbox_inches='tight')

def fmt(x, pos):
    '''convert scientific number'''
    a, b = '{:.2e}'.format(x).split('e')
    b = int(b)
    return r'${}E{{{}}}$'.format(a, b)

def add_front_lines_box(ax):
    points = eva.get_points_of_inclined_fronts()
    for ii in np.arange(int(points.shape[0]/2)):
        P = points[2*ii:2*ii+2,:]
        y, x, m, b = eva.calc_line(P)
        delta=0.8*0.1 # 0.8 view 0.1 actual front
        color = 'r'
        lw = 1
        ls = ':'
        # ax.plot(x,y, lw=lw, color=color, ls=ls)
        ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
        ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
        ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
        ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
        # t = ax.text(P[0,0]-0.05, P[0,1], f'F{1}', color='black', fontsize=20)
        # t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

def eval_mld_methods(ds_F, A_threshold, A_fox, A_wb30, dl, savefig=False):
    # MLD Profiles eval
    # hca, hcb = pyic.arrange_axes(2,1, plot_cb=False, sharey=True)
    fig, hca = plt.subplots(1,2, sharey=True)
    i = 0
    Data = ds_F.n2.isel(fronts=i).dropna(dim='crossfront', how='all').dropna(dim='alongfront', how='all')
    s    = Data.crossfront.shape[0]
    dat  = Data.isel(crossfront=slice(int(s/2)-int(s*dl), int(s/2)+int(s*dl))) #cut out ocean front
    data = dat.mean(dim=["alongfront", "crossfront"], skipna=True)
    N2 = data#.isel(depthi=slice(0,80))
    Data = ds_F.wb.isel(fronts=i).dropna(dim='crossfront', how='all').dropna(dim='alongfront', how='all')
    s    = Data.crossfront.shape[0]
    dat  = Data.isel(crossfront=slice(int(s/2)-int(s*dl), int(s/2)+int(s*dl))) #cut out ocean front
    data = dat.mean(dim=["alongfront", "crossfront"], skipna=True)
    WB = data.isel(depthi=slice(0,100))
    cm     = 3
    sum    = N2.cumsum(dim='depthi') * cm / (depthi )
    mldx_threshold = A_threshold.isel(fronts=i).mld
    mldx_fox       = A_fox.isel(fronts=i).mld
    mldx_wb        = A_wb30.isel(fronts=i).mld
    xmin = -1e-5
    xmax = 4e-5
    ax = hca[0]
    ax.plot(N2.data, N2.depthi, label='dbdz', color='b')
    # ax.plot(sum, sum.depthi, color='purple', ls='-.', label=f'cumsum(N2) with cm = {cm}')
    ax.hlines(mldx_threshold, xmin=xmin, xmax=xmax, color='k', ls=':', label='MLD 0.3')
    ax.hlines(mldx_fox, xmin=xmin, xmax=xmax, color='k', ls='-.', label='MLD Fox')
    ax.hlines(mldx_wb, xmin=xmin, xmax=xmax,  color='k', ls='--', label='MLD 30% wb max')
    ax.set_ylim(700,0)
    ax.set_xlim(0, 3e-5)
    ax.set_xlabel(r'$N^2$')
    # ax.legend(loc='lower left')
    ax.set_ylabel('depth')
    ax.set_title(f'Front {i+1}')
    ax.tick_params(axis='x', colors='b')
    ax.xaxis.label.set_color('b')
    ax2 = ax.twiny()
    ax2.plot(WB.data, WB.depthi, color='red', label ='WB')
    ax2.set_xlim(-1e-8, 1e-7)
    ax2.set_xlabel(r'$\overline{w^\prime b^\prime}$')
    ax2.tick_params(axis='x', colors='red')
    ax2.xaxis.label.set_color('red')

    i = 26
    Data = ds_F.n2.isel(fronts=i).dropna(dim='crossfront', how='all').dropna(dim='alongfront', how='all')
    s    = Data.crossfront.shape[0]
    dat  = Data.isel(crossfront=slice(int(s/2)-int(s*dl), int(s/2)+int(s*dl))) #cut out ocean front
    data = dat.mean(dim=["alongfront", "crossfront"], skipna=True)
    N2 = data#isel(depthi=slice(0,80))
    Data = ds_F.wb.isel(fronts=i).dropna(dim='crossfront', how='all').dropna(dim='alongfront', how='all')
    s    = Data.crossfront.shape[0]
    dat  = Data.isel(crossfront=slice(int(s/2)-int(s*dl), int(s/2)+int(s*dl))) #cut out ocean front
    data = dat.mean(dim=["alongfront", "crossfront"], skipna=True)
    WB = data.isel(depthi=slice(0,100))
    sum    = N2.cumsum(dim='depthi') * cm / (depthi )
    mldx_threshold = A_threshold.isel(fronts=i).mld
    mldx_fox       = A_fox.isel(fronts=i).mld
    mldx_wb        = A_wb30.isel(fronts=i).mld
    ax = hca[1]
    ax.plot(N2.data, N2.depthi, label='dbdz', color='b')
    # ax.plot(sum, sum.depthi, color='purple', ls='-.', label=f'cumsum(N2), cm= {cm}')
    ax.hlines(mldx_threshold, xmin=xmin, xmax=xmax, color='k', ls=':', label=r'MLD $\Delta \rho = 0.2 kg/m^3$')
    ax.hlines(mldx_fox, xmin=xmin, xmax=xmax, color='k', ls='-.', label=r'$MLD_{Fox} ~ N^2(z) =\frac{\sum(N^2(z)}{n}) $')
    ax.hlines(mldx_wb, xmin=xmin, xmax=xmax,  color='k', ls='--', label=r'MLD 30% $\overline{w^\prime b^\prime}$ max')
    ax.set_xlabel(r'$N^2$')
    ax.set_ylim(700,0)
    ax.set_xlim(0, 3e-5)
    ax.set_title(f'Front {i+1}')
    ax.tick_params(axis='x', colors='b')
    # ax.legend(loc='lower left')
    ax.xaxis.label.set_color('b')
    ax2 = ax.twiny()
    ax2.plot(WB.data, WB.depthi, color='red', label =r'$\overline{w^\prime b^\prime}$')
    ax2.set_xlim(-1e-8, 2e-8)
    # ax2.legend(loc='upper left')
    ax2.set_xlabel(r'$\overline{w^\prime b^\prime}$')
    ax2.tick_params(axis='x', colors='red')
    ax2.xaxis.label.set_color('red')
    lines, labels = ax.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    ax2.legend(lines2 + lines, labels2 + labels, loc=0)
    if savefig == True: plt.savefig(f'/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/inclined_fronts/intercomparison/mld_jan23.png', dpi=150, format='png', bbox_inches='tight')

def plot_mld_n2_wb_all(ds_F, ds_F2, ds_F03, A_threshold2, A_threshold03, A_fox, A_wb05, A_wb15, A_wb30, dl, savefig=False):
    def calc_data(ii, ds_F, dl):
        Data = ds_F.isel(fronts=ii).dropna(dim='crossfront', how='all').dropna(dim='alongfront', how='all')
        s    = Data.m2.crossfront.shape[0]
        dat  = Data.isel(crossfront=slice(int(s/2)-int(s*dl), int(s/2)+int(s*dl))) #cut out ocean front
        # data = dat.mean(dim=["crossfront"], skipna=True)
        data = dat.mean(dim=["alongfront"], skipna=True)
        # merge dimensions
        # data = dat.stack(z=('alongfront', 'crossfront'))
        return data

    def plot(ax, data, Profiles_full, ii):
        # ax.plot(data.n2, depthi, color='tab:blue', lw=0.2, alpha=0.8)
        ax.plot(Profiles_full.isel(fronts=ii).n2, Profiles_full.isel(fronts=0).depthi, color='tab:blue', lw=2)
        ax.tick_params(axis='x', colors='tab:blue')
        ax.set_xlabel(r'$N^2 ~ [1/s^2]$', color='tab:blue')
        ax.set_title('Front '+str(ii+1))
        ax.set_xlim(0,Profiles_full.isel(fronts=ii).isel(depthi=slice(0,83)).n2.max())


        ax2 = ax.twiny()
        # ax2.plot(data.wb, depthi, color='tab:red', lw=0.2, alpha=0.8)
        ax2.plot(Profiles_full.isel(fronts=ii).wb, Profiles_full.isel(fronts=0).depthi, color='tab:red', lw=2)
        ax2.tick_params(axis='x', colors='tab:red')
        ax2.set_xlabel(r'$\overline{w^\prime b^\prime} ~ [m^2/s^3]$', color='tab:red')
        return ax2

    def plot_mld(ax, mld, mld_mean, string, color, pos0, ls):
        # ax.hlines(mld, xmin=0, xmax=3e-5, color=color, lw=0.3)
        if mld_mean.fronts.data == 25 or mld_mean.fronts.data == 16 or mld_mean.fronts.data == 31: ax.hlines(mld_mean, xmin=0, xmax=10e-5, color=color, ls=ls, lw=2, label=string)#; ax.legend()
        else: ax.hlines(mld_mean, xmin=0, xmax=10e-5, color=color, ls=ls, lw=2)
        # pos =np.array([pos0, mld_mean])
        # ax.text(pos[0], pos[1]-10, string, color=color, fontsize=13)
        # ax.text(pos[0], pos[1]-10, string, color=color, fontsize=13, bbox=dict(facecolor='white', edgecolor='None', alpha=0.8), zorder=20)

    if False:
        fig, hca = plt.subplots(3,5, figsize=(10,15), sharey=True)
        ii=-1

        # F = np.array([0,1,2,3,4, 33,34,35,36,37, 22, 23, 24, 25, 26])
        F = ds_F.fronts.data[30:45]-1
        # F = ds_F.fronts.data[15:30]-1
        # F = ds_F.fronts.data[:15]-1
        l=0
        for k in F:
            l +=1
            print(k)
            ii+=1
            if l <= 5: ax=hca[0,ii]; 
            elif (l>5) & (l<=10): ax=hca[1,ii-5]
            else: ax=hca[2,ii-10]

            data = calc_data(k, ds_F2, dl)
            ax2 = plot(ax, data, A_threshold2, k)

            plot_mld(ax, data.mld, A_threshold2.isel(fronts=k).mld, string=r'$\Delta \rho=0.2$', color='tab:green', pos0=0.1e-5, ls='-')
            data = calc_data(k, ds_F03, dl)
            plot_mld(ax, data.mld, A_threshold03.isel(fronts=k).mld, string=r'$\Delta \rho=0.03$', color='tab:green', pos0=0.1e-5, ls=':')
            plot_mld(ax, np.NaN, A_wb05.isel(fronts=k).mld, string=r'$5\% ~ \overline{w^\prime b^\prime}_{max}$',  color='tab:purple', pos0=2.e-5, ls=':')
            plot_mld(ax, np.NaN, A_wb15.isel(fronts=k).mld, string=r'$15\% ~ \overline{w^\prime b^\prime}_{max}$', color='tab:purple', pos0=2.e-5, ls='-.')
            plot_mld(ax, np.NaN, A_wb30.isel(fronts=k).mld, string=r'$30\% ~ \overline{w^\prime b^\prime}_{max}$', color='tab:purple', pos0=2.e-5, ls='--')
            plot_mld(ax, np.NaN, A_fox.isel(fronts=k).mld, string=r'$N^2(z) =\frac{\sum N^2(z)}{n} $', color='tab:orange', pos0=2.e-5, ls='--')

            if ii==0 or ii==5 or ii==10: ax.set_ylabel(r'depth [m]')
            if l == 1: 
                leg = ax.legend(loc='upper center')
                leg.remove()
                ax2.add_artist(leg)
            ax.grid()
            ax.set_ylim(550,0)

        fig.tight_layout()

        if savefig == True: plt.savefig(f'/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/inclined_fronts/intercomparison/grand_comp_{F[-1]+1}.png', dpi=150, format='png', bbox_inches='tight')
    else:
        fig, hca = plt.subplots(1,2, figsize=(5,5), sharey=True)
        ii=-1

        F = np.array([3,23])

        kend = F.size
        l=0
        for k in F:
            l +=1
            print(k)
            ii+=1
            ax=hca[ii]; 

            data = calc_data(k, ds_F2, dl)
            ax2 = plot(ax, data, A_threshold2, k)

            plot_mld(ax, data.mld, A_threshold2.isel(fronts=k).mld, string=r'$\Delta \rho=0.2$', color='tab:green', pos0=0.1e-5, ls='-')
            data = calc_data(k, ds_F03, dl)
            plot_mld(ax, data.mld, A_threshold03.isel(fronts=k).mld, string=r'$\Delta \rho=0.03$', color='tab:green', pos0=0.1e-5, ls=':')
            plot_mld(ax, np.NaN, A_wb05.isel(fronts=k).mld, string=r'$15\% ~ \overline{w^\prime b^\prime}_{max}$',  color='tab:purple', pos0=2.e-5, ls=':')
            plot_mld(ax, np.NaN, A_wb15.isel(fronts=k).mld, string=r'$45\% ~ \overline{w^\prime b^\prime}_{max}$', color='tab:purple', pos0=2.e-5, ls='-.')
            plot_mld(ax, np.NaN, A_wb30.isel(fronts=k).mld, string=r'$30\% ~ \overline{w^\prime b^\prime}_{max}$', color='tab:purple', pos0=2.e-5, ls='--')
            plot_mld(ax, np.NaN, A_fox.isel(fronts=k).mld, string=r'$N^2(z) =\frac{cm}{H}\frac{\sum N^2(z)}{n} $', color='tab:orange', pos0=2.e-5, ls='--')
            ax.text(-0.10, 1.02, '('+string.ascii_lowercase[l-1]+')', transform=ax.transAxes, size=10)

            if ii==0: 
                ax.set_ylabel(r'depth [m]')
            if l == 2: 
                leg = ax.legend(loc='lower right', fontsize=8)
                leg.remove()
                ax2.add_artist(leg)
            ax.grid()
            ax.set_ylim(550,0)
        fig.tight_layout()

        if savefig == True: plt.savefig(f'/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/inclined_fronts/intercomparison/mld_single2_{F[-1]+1}_24.png', dpi=150, format='png', bbox_inches='tight')


def wb_mld_sensitivity(A_wb30, A_wb15, A_wb45, A_threshold03, A_threshold2, A_fox, savefig=False):
    fig, ax = plt.subplots(1,1, figsize=(5,5))
    plt.grid()
    lim_x   = 0,500
    lim_y   = 0,500
    lim_min = np.min([lim_x[0], lim_y[0]])
    lim_max = np.max([lim_x[1], lim_y[1]])
    x = np.linspace(lim_min, lim_max,2)
    y = x
    ax.plot(x,y, lw=2, color='purple', ls='-', alpha=0.7)

    x = A_wb30.mld
    lower_error = A_wb30.mld - A_wb15.mld
    upper_error = A_wb45.mld - A_wb30.mld
    asymmetric_error = np.array(list(zip(lower_error, upper_error))).T
    plt.errorbar(x, x, yerr=asymmetric_error, fmt='.', alpha=0.7, ecolor = 'tab:purple',capsize=3, barsabove=True, color='tab:purple', label=r'$30\% \overline{w^\prime b^\prime}_{max}$')

    ax.scatter(A_wb30.mld, A_threshold03.mld, label=r'$\Delta \rho=0.03$', marker='o', facecolor='None', color='grey')
    ax.scatter(A_wb30.mld, A_threshold2.mld, label=r'$\Delta \rho=0.2$', marker='s', facecolor='None',  color='tab:green', zorder=20)
    ax.scatter(A_wb30.mld, A_fox.mld, label=r'$N^2(z) =\frac{cm}{H}\frac{\sum N^2(z)}{n} $', marker='^', facecolor='None',  color='tab:orange', zorder=19)

    ax.set_xlabel(r'MLD $30\% \overline{w^\prime b^\prime}_{max}$ [m]')
    ax.set_ylabel(r'MLD for different methods [m]')
    ax.set_xlim(0,500)
    ax.set_ylim(0,500)
    # increase tick size and label size
    ax.tick_params(axis='both', which='major', labelsize=12)
    ax.tick_params(axis='both', which='minor', labelsize=12)
    ax.text(-0.05, 1.02, '('+string.ascii_lowercase[2]+')', transform=ax.transAxes, size=12)
    legend = ax.legend(loc='upper center', fontsize=8)
    legend.set_zorder(21)

    if savefig == True: plt.savefig(f'/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/front/inclined_fronts/intercomparison/mld_comp_new.png', dpi=150, format='png', bbox_inches='tight')

def eval_single_fronts_slope_ri(A, ri_dmean, optimize_var, use_profile_for_param, fig_path, savefig=False):
    ### 4 in a row
    lim       = 1e4
    xlabel    = r'$log_{10} Ri$'
    hca, hcb = pyic.arrange_axes(4, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    ii = -1
    ### Fig 1
    ylabel    = r'$log_{10} |\overline{\hat{v}^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    name      = 'ri_alpha_vb'
    var_fox   = A.vb_fox_param
    var_stone = A.vb_stone_param
    var_diag  = A.Vb_cross_grad

    lhs_fox         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    lhs_stone_dmean = lhs_stone#.mean(dim='depthi')
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag

    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(ri_dmean < lim).data)

    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    ylabel    = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    name      = 'ri_alpha_wb'
    var_fox   = A.wb_fox_param
    var_stone = A.wb_stone_param
    var_diag  = A.wb

    lhs_fox         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    lhs_stone_dmean = lhs_stone.mean(dim='depthi')
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag

    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(ri_dmean < lim).data)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )


    ylabel    = r'$log_{10} |\overline{\psi}|/(H^2 f \alpha) $'
    name      = 'ri_alpha_psi'
    var_fox   = A.psi_fox_full_param
    var_stone = A.psi_stone_full_param
    var_diag  = A.psi_diag_full


    lhs_fox         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,1) / np.power(A.alpha,1)
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,1) / np.power(A.alpha,1)
    lhs_stone_dmean = lhs_stone.mean(dim='depthi')
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,1) / np.power(A.alpha,1)
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag

    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(ri_dmean < lim).data)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )


    ylabel    = r'$log_{10} |\overline{K_{dia}}|/(H^2 f \alpha) $'
    name      = 'ri_alpha_K'
    var_fox   = A.K_fox
    var_stone = A.K_stone
    var_diag  = A.K

    lhs_fox         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,1) / np.power(A.alpha,1)
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,1) / np.power(A.alpha,1)
    lhs_stone_dmean = lhs_stone.mean(dim='depthi')
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,1) / np.power(A.alpha,1)
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag

    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(ri_dmean < lim).data)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/correlation_ri_alpha_sensitivity_{optimize_var}_{lim}.png', dpi=150, format='png', bbox_inches='tight')


def eval_single_fronts_slope_ri_paper(A, ri_dmean, optimize_var, use_profile_for_param, fig_path, savefig=False):
    ### 4 in a row
    lim       = 1e4
    xlabel    = r'$log_{10} Ri$'
    hca, hcb = pyic.arrange_axes(2, 1, plot_cb=False, asp=1, fig_size_fac=2.1)
    ii = -1
    ### Fig 1
    ylabel    = r'$log_{10} |\overline{\hat{v}^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    name      = 'ri_alpha_vb'
    var_fox   = A.vb_fox_param
    var_stone = A.vb_stone_param_dmean
    var_diag  = A.Vb_cross_grad

    lhs_fox         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    lhs_stone_dmean = lhs_stone#.mean(dim='depthi')
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag

    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(ri_dmean < lim).data)

    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    ylabel    = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    name      = 'ri_alpha_wb'
    var_fox   = A.wb_fox_param
    var_stone = A.wb_stone_param
    var_diag  = A.wb

    lhs_fox         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    lhs_stone_dmean = lhs_stone.mean(dim='depthi')
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha,2)
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag

    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(ri_dmean < lim).data)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/correlation_ri_alpha_sensitivity_{optimize_var}_{lim}_paper.png', dpi=150, format='png', bbox_inches='tight')



def eval_single_fronts_slope_ri_paper_n(A,  optimize_var, use_profile_for_param, fig_path, savefig=False):
    ### 4 in a row
    lim       = 1e3
    xlabel    = r'$log_{10} Ri$'
    hca, hcb = pyic.arrange_axes(2, 1, plot_cb=False, asp=1, fig_size_fac=2.1)
    ii = -1
    ### Fig 1
    ylabel    = r'$log_{10} |\overline{\hat{v}^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^3) $'
    name      = 'ri_alpha_vb'
    var_fox   = A.vb_fox_param_dmean
    var_stone = A.vb_stone_param_dmean
    var_diag  = A.Vb_cross_grad_dmean

    lhs_fox_dmean         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha_dmean,3)
    lhs_stone_dmean       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha_dmean,3)
    lhs_diag_dmean        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha_dmean,3)

    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(A.ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(A.ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(A.ri_dmean < lim).data)

    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    ylabel    = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    name      = 'ri_alpha_wb'
    var_fox   = A.wb_fox_param_dmean
    var_stone = A.wb_stone_param_dmean
    var_diag  = A.wb_dmean

    lhs_fox_dmean         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha_dmean,2)
    lhs_stone_dmean       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha_dmean,2)
    lhs_diag_dmean        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) / np.power(A.alpha_dmean,2)

    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(A.ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(A.ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(A.ri_dmean < lim).data)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/bug_corrected/correlation_ri_alpha_sensitivity_{optimize_var}_{lim}_paper.png', dpi=150, format='png', bbox_inches='tight')



def eval_single_front_slope_alpha_paper_n(A, optimize_var, use_profile_for_param, fig_path, savefig=False):
    lim=None
    xlabel = r'$log_{10} \alpha$'
    hca, hcb = pyic.arrange_axes(2, 1, plot_cb=False, asp=1, fig_size_fac=2.1)
    ii = -1

    ylabel    = r'$log_{10} |\overline{\hat{v}^{\prime} b^{\prime}}|/(H^2 f^3) $'
    name      = 'alpha_vb'
    var_fox   = A.vb_fox_param_dmean
    var_stone = A.vb_stone_param_dmean
    var_diag  = A.Vb_cross_grad_dmean

    lhs_fox_dmean         = var_fox/ np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_stone_dmean       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_diag_dmean        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) 

    logx_diag , logy_diag  = calc_log10(A.alpha_dmean,  lhs_diag_dmean)
    logx_fox  , logy_fox   = calc_log10(A.alpha_dmean,   lhs_fox_dmean)
    logx_stone, logy_stone = calc_log10(A.alpha_dmean, lhs_stone_dmean)

    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    ylabel    = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3) $'
    name      = 'alpha_wb'
    var_fox   = A.wb_fox_param_dmean
    var_stone = A.wb_stone_param_dmean
    var_diag  = A.wb_dmean

    lhs_fox_dmean         = var_fox/ np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_stone_dmean       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_diag_dmean        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) 

    logx_diag , logy_diag  = calc_log10(A.alpha_dmean,  lhs_diag_dmean)
    logx_fox  , logy_fox   = calc_log10(A.alpha_dmean,   lhs_fox_dmean)
    logx_stone, logy_stone = calc_log10(A.alpha_dmean, lhs_stone_dmean)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/bug_corrected/correlation_alpha_spindown_{optimize_var}_paper.png', dpi=150, format='png', bbox_inches='tight')

def eval_single_fronts_combined(A, optimize_var, use_profile_for_param, fig_path, savefig=False):
    lim_ri = 1e3
    lim_alpha = None

    # Create a 2x2 grid of subplots
    hca, hcb = pyic.arrange_axes(3, 2, plot_cb=False, asp=1, fig_size_fac=1.9, reverse_order=True, daxt=1.2)
    ii = -1



    # Plot 3: Alpha vs Vb
    xlabel = r'$log_{10} \overline{\alpha}$'
    ylabel = r'$log_{10} |\overline{\hat{v}^{\prime} b^{\prime}}|/(H^2 f^3) $'
    name = 'alpha_vb'
    var_fox = A.vb_fox_param_dmean
    var_stone = A.vb_stone_param_dmean
    var_diag = A.Vb_cross_grad_dmean

    lhs_fox_dmean   = var_fox / np.power(A.mld, 2) / np.power(A.fcoriolis, 3)
    lhs_stone_dmean = var_stone / np.power(A.mld, 2) / np.power(A.fcoriolis, 3)
    lhs_diag_dmean  = var_diag / np.power(A.mld, 2) / np.power(A.fcoriolis, 3)

    logx_diag, logy_diag = calc_log10(A.alpha_dmean, lhs_diag_dmean)
    logx_fox, logy_fox = calc_log10(A.alpha_dmean, lhs_fox_dmean)
    logx_stone, logy_stone = calc_log10(A.alpha_dmean, lhs_stone_dmean)


    plot_slope_4_a(hca, hcb, ii, logx_diag, logy_diag, logx_fox, logy_fox, logx_stone, logy_stone, lim_alpha, xlabel, ylabel, name)

    # Plot 4: Alpha vs Wb
    ylabel    = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3) $'
    name      = 'alpha_wb'
    var_fox   = A.wb_fox_param_dmean
    var_stone = A.wb_stone_param_dmean
    var_diag  = A.wb_dmean

    lhs_fox_dmean   = var_fox / np.power(A.mld, 2) / np.power(A.fcoriolis, 3)
    lhs_stone_dmean = var_stone / np.power(A.mld, 2) / np.power(A.fcoriolis, 3)
    lhs_diag_dmean  = var_diag / np.power(A.mld, 2) / np.power(A.fcoriolis, 3)

    logx_diag, logy_diag = calc_log10(A.alpha_dmean, lhs_diag_dmean)
    logx_fox, logy_fox = calc_log10(A.alpha_dmean, lhs_fox_dmean)
    logx_stone, logy_stone = calc_log10(A.alpha_dmean, lhs_stone_dmean)

    ii += 1
    plot_slope_4_a(hca, hcb, ii, logx_diag, logy_diag, logx_fox, logy_fox, logx_stone, logy_stone, lim_alpha, xlabel, ylabel, name)

    # Plot 1: Ri vs Vb
    xlabel = r'$log_{10} Ri$'
    ylabel = r'$log_{10} |\overline{\hat{v}^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^3) $'
    name = 'ri_alpha_vb'
    var_fox = A.vb_fox_param_dmean
    var_stone = A.vb_stone_param_dmean
    var_diag = A.Vb_cross_grad_dmean

    lhs_fox_dmean = var_fox / np.power(A.mld, 2) / np.power(A.fcoriolis, 3) / np.power(A.alpha_dmean, 3)
    lhs_stone_dmean = var_stone / np.power(A.mld, 2) / np.power(A.fcoriolis, 3) / np.power(A.alpha_dmean, 3)
    lhs_diag_dmean = var_diag / np.power(A.mld, 2) / np.power(A.fcoriolis, 3) / np.power(A.alpha_dmean, 3)

    logx_diag, logy_diag = calc_log10(A.ri_dmean.where(A.ri_dmean < lim_ri).data, lhs_diag_dmean.where(A.ri_dmean < lim_ri).data)
    logx_fox, logy_fox = calc_log10(A.ri_dmean.where(A.ri_dmean < lim_ri).data, lhs_fox_dmean.where(A.ri_dmean < lim_ri).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim_ri).data, lhs_stone_dmean.where(A.ri_dmean < lim_ri).data)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag, logy_diag, logx_fox, logy_fox, logx_stone, logy_stone, lim_ri, xlabel, ylabel, name)

    # Plot 2: Ri vs Wb
    ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    name = 'ri_alpha_wb'
    var_fox = A.wb_fox_param_dmean
    var_stone = A.wb_stone_param_dmean
    var_diag = A.wb_dmean

    lhs_fox_dmean = var_fox / np.power(A.mld, 2) / np.power(A.fcoriolis, 3) / np.power(A.alpha_dmean, 2)
    lhs_stone_dmean = var_stone / np.power(A.mld, 2) / np.power(A.fcoriolis, 3) / np.power(A.alpha_dmean, 2)
    lhs_diag_dmean = var_diag / np.power(A.mld, 2) / np.power(A.fcoriolis, 3) / np.power(A.alpha_dmean, 2)

    logx_diag, logy_diag = calc_log10(A.ri_dmean.where(A.ri_dmean < lim_ri).data, lhs_diag_dmean.where(A.ri_dmean < lim_ri).data)
    logx_fox, logy_fox = calc_log10(A.ri_dmean.where(A.ri_dmean < lim_ri).data, lhs_fox_dmean.where(A.ri_dmean < lim_ri).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim_ri).data, lhs_stone_dmean.where(A.ri_dmean < lim_ri).data)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag, logy_diag, logx_fox, logy_fox, logx_stone, logy_stone, lim_ri, xlabel, ylabel, name)

    ii += 1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    x = np.linspace(4e-7,1e-4,100)
    y = x
    
    xvals   = [np.nan, np.nan, 9e-7, 7e-6, np.nan, 8e-6 ]
    lim_max = x.max()
    lim_min = x.min()

    ax.set_ylim(lim_min,lim_max)
    ax.set_xlim(lim_min,lim_max)
    ax.grid()


    ax.loglog(A.Vb_cross_grad_dmean, A.vb_fox_param_dmean, 'o', color='tab:blue', mfc='none', label=r'$\overline{\hat{v}^\prime b^\prime}_{PER} $', zorder=20)
    ax.loglog(A.Vb_cross_grad_dmean, A.vb_stone_param_dmean, 'x', color='tab:orange', label=r'$\overline{\hat{v}^\prime b^\prime}_{ALS} $', zorder=21)
    ax.legend(loc='lower center')
    ax.plot(x,y, 'grey', label = r'$\overline{\hat{v}^\prime b^\prime}_{diag}  = \overline{\hat{v}^\prime b^\prime}_{param}$')

    ax.loglog(x, y * 10, 'r', ls='-', label=r'$ 10 ~\overline{\hat{v}^\prime b^\prime}_{diag}$ ')
    ax.loglog(x, y * 0.1, 'r', ls='-')
    ax.loglog(x, y * 5, 'r', ls=':', label=r'$ 5 ~\overline{\hat{v}^\prime b^\prime}_{diag}$ ')
    ax.loglog(x, y * 0.2, 'r', ls=':')

    tools.labelLines(ax.get_lines(), align = True, xvals=xvals, zorder=2)

    ax.set_xlabel(r'$\overline{\hat{v}^\prime b^\prime}_{diag}~[m^2/s^3]$')
    ax.set_ylabel(r'$\overline{\hat{v}^\prime b^\prime}_{param}~[m^2/s^3]$')
    # ax.text(4.3e-7, 7e-5, r'Overestimated $\overline{\hat{v}^\prime b^\prime}$', color='grey')
    # ax.text(7e-5, 4.9e-7, r'Underestimated $\overline{\hat{v}^\prime b^\prime}$', color='grey', rotation=90)
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    x = np.linspace(3e-10,5e-7,100)
    y = x

    xvals   = [np.nan, np.nan, 1e-9, 1e-9, np.nan, 1e-9 ]
    lim_max = x.max()
    lim_min = x.min()

    ax.set_ylim(lim_min,lim_max)
    ax.set_xlim(lim_min,lim_max)
    ax.grid()

    ax.loglog(A.wb_dmean, A.wb_fox_param_dmean, 'o', color='tab:blue', mfc='none', label=r'$\overline{w^\prime b^\prime}_{PER} $', zorder=20)
    ax.loglog(A.wb_dmean, A.wb_stone_param_dmean, 'x', color='tab:orange', label=r'$\overline{w^\prime b^\prime}_{ALS} $', zorder=21)
    ax.legend(loc='upper right')
    ax.plot(x,y, 'grey', label = r'$\overline{w^\prime b^\prime}_{diag}  = \overline{w^\prime b^\prime}_{param}$')

    ax.loglog(x, y * 10, 'r', ls='-', label=r'$ 10 ~\overline{w^\prime b^\prime}_{diag}$ ')
    ax.loglog(x, y * 0.1, 'r', ls='-')
    ax.loglog(x, y * 5, 'r', ls=':', label=r'$ 5 ~\overline{w^\prime b^\prime}_{diag}$ ')
    ax.loglog(x, y * 0.2, 'r', ls=':')

    tools.labelLines(ax.get_lines(), align = True, xvals=xvals, zorder=2)

    ax.set_xlabel(r'$\overline{w^\prime b^\prime}_{diag} ~[m^2/s^3]$')
    ax.set_ylabel(r'$\overline{w^\prime b^\prime}_{param}~[m^2/s^3]$')
    # ax.text(3.5e-10, 3.3e-7, r'Overestimated $\overline{w^\prime b^\prime}$', color='grey')
    # ax.text(3.3e-7, 4e-10, r'Underestimated $\overline{w^\prime b^\prime}$', color='grey', rotation=90)


    if savefig:
        plt.savefig(f'{fig_path}/param_vs_diag/bug_corrected/correlation_combined_{optimize_var}_paper.png', dpi=150, format='png', bbox_inches='tight')


def eval_single_fronts_slope_ri_spin(A, ri_dmean, optimize_var, use_profile_for_param, fig_path, savefig=False):

    xlabel = r'$log_{10} Ri$'
    lim    = 1e4
    print('limit Richardsonnumber for evaluation Ri <', lim)
    hca, hcb = pyic.arrange_axes(4, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    ii = -1

    ylabel    = r'$log_{10} |\overline{\hat{v}^{\prime} b^{\prime}}|/(H^2 f^3) $'
    name      = 'ri_vb'
    var_fox   = A.vb_fox_param
    var_stone = A.vb_stone_param
    var_diag  = A.Vb_cross_grad

    lhs_fox         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_stone_dmean = lhs_stone#.mean(dim='depthi') #stone mean profile
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag

    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(ri_dmean < lim).data)


    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )


    ylabel    = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3) $'
    name      = 'ri_wb'
    var_fox   = A.wb_fox_param
    var_stone = A.wb_stone_param
    var_diag  = A.wb

    lhs_fox         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_stone_dmean = lhs_stone.mean(dim='depthi') #stone mean profile
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag


    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(ri_dmean < lim).data)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )


    ylabel    = r'$log_{10} |\overline{\psi}|/(H^2 f) $'
    name      = 'ri_psi'
    var_fox   = A.psi_fox_full_param
    var_stone = A.psi_stone_full_param
    var_diag  = A.psi_diag_full

    lhs_fox         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    lhs_stone_dmean = lhs_stone.mean(dim='depthi') 
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag


    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(ri_dmean < lim).data)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )


    ylabel    = r'$log_{10} |\overline{K_{dia}}|/(H^2 f) $'
    name      = 'ri_K'
    var_fox   = A.K_fox
    var_stone = A.K_stone
    var_diag  = A.K

    lhs_fox         = var_fox / np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    lhs_stone_dmean = lhs_stone.mean(dim='depthi') 
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag


    logx_diag , logy_diag  = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,  lhs_diag_dmean.where(ri_dmean < lim).data)
    logx_fox  , logy_fox   = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data,   lhs_fox_dmean.where(ri_dmean < lim).data)
    logx_stone, logy_stone = calc_log10(A.ri_dmean.where(A.ri_dmean < lim).data, lhs_stone_dmean.where(ri_dmean < lim).data)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/correlation_ri_spindwon_{optimize_var}_{lim}.png', dpi=150, format='png', bbox_inches='tight')



def eval_single_front_slope_alpha(A, optimize_var, use_profile_for_param, fig_path, savefig=False):
    lim    = 1e8
    print('limit Richardsonnumber for evaluation Ri <', lim)
    xlabel = r'$log_{10} \alpha$'
    hca, hcb = pyic.arrange_axes(4, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    ii = -1

    ylabel    = r'$log_{10} |\overline{\hat{v}^{\prime} b^{\prime}}|/(H^2 f^3) $'
    name      = 'alpha_vb'
    var_fox   = A.vb_fox_param
    var_stone = A.vb_stone_param
    var_diag  = A.Vb_cross_grad

    lhs_fox         = var_fox/ np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_stone_dmean = lhs_stone #.mean(dim='depthi') # constant vertical profile
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag


    logx_diag , logy_diag  = calc_log10(A.alpha,  lhs_diag_dmean)
    logx_fox  , logy_fox   = calc_log10(A.alpha,   lhs_fox_dmean)
    logx_stone, logy_stone = calc_log10(A.alpha, lhs_stone_dmean)

    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    ylabel    = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3) $'
    name      = 'alpha_wb'
    var_fox   = A.wb_fox_param
    var_stone = A.wb_stone_param
    var_diag  = A.wb

    lhs_fox         = var_fox/ np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_stone_dmean = lhs_stone.mean(dim='depthi')
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag

    logx_diag , logy_diag  = calc_log10(A.alpha,  lhs_diag_dmean)
    logx_fox  , logy_fox   = calc_log10(A.alpha,   lhs_fox_dmean)
    logx_stone, logy_stone = calc_log10(A.alpha, lhs_stone_dmean)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    ylabel    = r'$log_{10} |\overline{\psi}|/(H^2 f) $'
    name      = 'alpha_psi'
    var_fox   = A.psi_fox_full_param
    var_stone = A.psi_stone_full_param
    var_diag  = A.psi_diag_full

    lhs_fox         = var_fox/ np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    lhs_stone_dmean = lhs_stone.mean(dim='depthi')
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag


    logx_diag , logy_diag  = calc_log10(A.alpha,  lhs_diag_dmean)
    logx_fox  , logy_fox   = calc_log10(A.alpha,   lhs_fox_dmean)
    logx_stone, logy_stone = calc_log10(A.alpha, lhs_stone_dmean)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    ylabel    = r'$log_{10} |\overline{K_{dia}}|/(H^2 f) $'
    name      = 'alpha_K'
    var_fox   = A.K_fox
    var_stone = A.K_stone
    var_diag  = A.K

    lhs_fox         = var_fox/ np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    lhs_stone_dmean = lhs_stone.mean(dim='depthi')
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,1) 
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag


    logx_diag , logy_diag  = calc_log10(A.alpha,  lhs_diag_dmean)
    logx_fox  , logy_fox   = calc_log10(A.alpha,   lhs_fox_dmean)
    logx_stone, logy_stone = calc_log10(A.alpha, lhs_stone_dmean)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/correlation_alpha_spindown_{optimize_var}_{lim}.png', dpi=150, format='png', bbox_inches='tight')


def eval_single_front_slope_alpha_paper(A, optimize_var, use_profile_for_param, fig_path, savefig=False):
    lim    = 1e8
    print('limit Richardsonnumber for evaluation Ri <', lim)
    xlabel = r'$log_{10} \alpha$'
    hca, hcb = pyic.arrange_axes(2, 1, plot_cb=False, asp=1, fig_size_fac=2.1)
    ii = -1

    ylabel    = r'$log_{10} |\overline{\hat{v}^{\prime} b^{\prime}}|/(H^2 f^3) $'
    name      = 'alpha_vb'
    var_fox   = A.vb_fox_param
    var_stone = A.vb_stone_param
    var_diag  = A.Vb_cross_grad

    lhs_fox         = var_fox/ np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_stone_dmean = lhs_stone #.mean(dim='depthi') # constant vertical profile
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag


    logx_diag , logy_diag  = calc_log10(A.alpha,  lhs_diag_dmean)
    logx_fox  , logy_fox   = calc_log10(A.alpha,   lhs_fox_dmean)
    logx_stone, logy_stone = calc_log10(A.alpha, lhs_stone_dmean)

    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    ylabel    = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3) $'
    name      = 'alpha_wb'
    var_fox   = A.wb_fox_param
    var_stone = A.wb_stone_param
    var_diag  = A.wb

    lhs_fox         = var_fox/ np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_fox_dmean   = lhs_fox.mean(dim='depthi')
    lhs_stone       = var_stone / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    lhs_stone_dmean = lhs_stone.mean(dim='depthi')
    lhs_diag        = var_diag / np.power(A.mld,2) / np.power(A.fcoriolis,3) 
    if use_profile_for_param == True:  lhs_diag_dmean  = lhs_diag.mean(dim='depthi')
    else: lhs_diag_dmean  = lhs_diag

    logx_diag , logy_diag  = calc_log10(A.alpha,  lhs_diag_dmean)
    logx_fox  , logy_fox   = calc_log10(A.alpha,   lhs_fox_dmean)
    logx_stone, logy_stone = calc_log10(A.alpha, lhs_stone_dmean)

    ii += 1
    plot_slope_4(hca, hcb, ii, logx_diag , logy_diag, logx_fox  , logy_fox, logx_stone, logy_stone, lim, xlabel, ylabel, name )

    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/correlation_alpha_spindown_{optimize_var}_{lim}_paper.png', dpi=150, format='png', bbox_inches='tight')



def plot_histograms(data, fig_path, savefig):
    plt.figure(figsize=([5,5]))
    r, p = stats.pearsonr(data.psi_diag_full, data.psi_stone_full_param)
    plt.hist(data.psi_diag_full - data.psi_stone_full_param, bins=10, alpha =0.5, label=r'$\psi_{diag}-\psi_{Stone}$'"\n" + f'r:{r:.2}'+f' p:{p:.2}')
    r, p = stats.pearsonr(data.psi_diag_full, data.psi_fox_full_param)
    plt.hist(data.psi_diag_full - data.psi_fox_full_param, bins=10, alpha=0.5, label=r'$\psi_{diag}-\psi_{Fox-Kemper}$'"\n" + f'r:{r:.2}'+f' p:{p:.2}')
    plt.title('Histogram psi diff')
    plt.legend()
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/hist_psi_diag.png', dpi=150, format='png', bbox_inches='tight')

    plt.figure(figsize=([5,5]))
    r, p = stats.pearsonr(data.wb, data.wb_stone_param)
    plt.hist(data.wb - data.wb_stone_param, bins=10, alpha =0.5, label=r'$wb_{diag}-wb_{Stone}$'"\n" + f'r:{r:.2}'+f' p:{p:.2}')
    r, p = stats.pearsonr(data.wb, data.wb_fox_param)
    plt.hist(data.wb - data.wb_fox_param  , bins=10, alpha=0.5, label=r' $wb_{diag}-wb_{Fox-Kemper}$'"\n" + f'r:{r:.2}'+f' p:{p:.2}')
    plt.title('Histogram wb diff')
    plt.legend()
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/hist_wb_diag.png', dpi=150, format='png', bbox_inches='tight')
    
    plt.figure(figsize=([5,5]))
    r, p = stats.pearsonr(data.Vb_cross_grad, data.vb_stone_param)
    plt.hist(data.Vb_cross_grad - data.vb_stone_param, bins=10, alpha =0.5, label=r'$vb_{diag}-vb_{Stone}$'"\n" + f'r:{r:.2}'+f' p:{p:.2}')
    r, p = stats.pearsonr(data.Vb_cross_grad, data.vb_fox_param)
    plt.hist(data.Vb_cross_grad - data.vb_fox_param  , bins=10, alpha=0.5, label=r' $vb_{diag}-vb_{Fox-Kemper}$'"\n" + f'r:{r:.2}'+f' p:{p:.2}')
    plt.title('Histogram vb diff')
    plt.legend()
    if savefig == True: plt.savefig(f'{fig_path}/param_vs_diag/hist_vb_diag.png', dpi=150, format='png', bbox_inches='tight')

def plot_1d_hist(A, fig_path, savefig):
    fig, ax = plt.subplots(2, 3, figsize=(15, 10))

    bins= 20
    ax[0, 0].hist(A.n2.data, bins=bins)
    ax[0, 0].set_title(r'$N^2$')
    ax[0, 1].hist(A.m2_cross_grad.data, bins=bins)
    ax[0, 1].set_title(r'$M^2$')
    ax[0, 2].hist(A.ri_dmean.data, bins=bins)
    ax[0, 2].set_title('Ri')
    ax[1, 0].hist(A.wb.data, bins=bins)
    ax[1, 0].set_title(r'$\overline{w^\prime b^\prime}$')
    ax[1, 1].hist(A.Vb_cross_grad.data, bins=bins)
    ax[1, 1].set_title(r'$\overline{\hat{v}^\prime b^\prime}$')
    ax[1, 2].hist(A.psi_diag_full.data, bins=bins)
    ax[1, 2].set_title(r'$\psi$')

    for ax in ax.flat:
        ax.set(ylabel='bins')

    plt.tight_layout()
    if savefig: fig.savefig(fig_path+'histograms_averaged.png', dpi=150, bbox_inches='tight')


# %%
