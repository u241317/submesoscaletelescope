# %%
import numpy as np
import matplotlib.pyplot as plt

# %%
H = 300
z = np.linspace(100, -400, 100)

fkt_stone = lambda z: -4 * z/H * (z/H + 1)
fkt_fox   = lambda z: - 4 * z / H * ( z / H +1) * (1 + np.power((5/21*(2*z/H+1)), 2))
mu_stone  = fkt_stone(z)
mu_fox    = fkt_fox(z)

plt.plot(mu_stone, z)
plt.plot(mu_fox, z)
plt.axhline(y=-H, color='r', linestyle='--')
plt.grid()
plt.ylabel("z [m]")
plt.xlabel("mu")
plt.legend([r"Stone: $ -4 * z/H * (z/H + 1)$", r"FK: $- 4 * z / H * ( z / H +1) * np.power((1 + 5/21*(2*z/H+1)), 2)$", "MLD"], loc='lower left', fontsize=8)
plt.savefig("vertical_profiles.png")
# %%
