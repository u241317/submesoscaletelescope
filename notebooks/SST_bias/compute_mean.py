#cluster
import dask
import datetime
import multiprocessing
from dask_jobqueue import SLURMCluster # Setting up distributed memories via slurm
from dask.utils import format_bytes
from dask.distributed import Client, LocalCluster, progress # Libaray to orchestrate distributed resources

from tempfile import NamedTemporaryFile, TemporaryDirectory # Creating temporary Files/Dirs
from getpass import getuser # Libaray to copy things
from pathlib import Path # Object oriented libary to deal with paths
# %%
!echo $HOSTNAME
# Set some user specific variables
account_name = 'mh0033' # Account that is going to be 'charged' fore the computation
queue = 'compute' # Name of the partition we want to use
job_name = 'PostProc' # Job name that is submitted via sbatch
memory = "256GiB" # Max memory per node that is going to be used - this depends on the partition #for averaging max=256
cores = 16 # Max number of cores per task that are reserved - also partition dependend
processes = 16 #number of workers
walltime = '02:00:00' # Walltime - also partition dependent

# %%
#calculate memory per worker and core
mem =256
memory_per_worker = mem/processes
memory_per_core = memory_per_worker/(cores/processes)
print('memory_per_worker =', memory_per_worker)
print('memory_per_core =', memory_per_core)

# %%

scratch_dir = Path('/scratch') / getuser()[0] / getuser() # Define the users scratch dir
# Create a temp directory where the output of distributed cluster will be written to, after this notebook
# is closed the temp directory will be closed
dask_tmp_dir = TemporaryDirectory(dir=scratch_dir, prefix=job_name)
cluster = SLURMCluster(memory=memory,
                       cores=cores,
                       processes=processes,
                       project=account_name,
                       walltime=walltime,
                       queue=queue,
                       name=job_name,
                       scheduler_options={'dashboard_address': ':8989'},
                       local_directory=dask_tmp_dir.name,
                       job_extra=[f'-J {job_name}', 
                                  f'-D {dask_tmp_dir.name}',
                                  f'--begin=now',
                                  f'--output={dask_tmp_dir.name}/LOG_cluster.%j.o'
                                 ],
                       interface='ib0')

print(cluster.job_script())

# %%
%%capture
import sys
import glob, os
os.chdir('/home/m/m300878/submesoscaletelescope/notebooks/may22/')
sys.path.insert(1, '../')
from icon_smt_levels import dzw, dzt, depthc, depthi
sys.path.insert(1, '/home/m/m300878/submesoscaletelescope/run_batch_job/model_evaluation/')
import smt_modules.all_funcs as eva

import warnings
warnings.filterwarnings('ignore')  #suppress some warnings about future code changes

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates
from scipy import stats    #Used for 2D binned statistics
from mpl_toolkits.axes_grid1 import make_axes_locatable #For plotting interior colobars
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import pyicon as pyic

sst_smt       = eva.load_smt_sst()
sst_smt_mean  = sst_smt.groupby('time.month').mean()
sst_smt_mean  = sst_smt_mean.isel(month=2)

sst_smt_mean.to_netcdf()


# %%
from dask.utils import format_bytes
from dask.distributed import Client, LocalCluster, progress # Libaray to orchestrate distributed resources

cluster = LocalCluster(scheduler_options={'dashboard_address': ':8989'})
client = Client(cluster)
# %%#cluster
# import dask
# import datetime
# import multiprocessing
# from dask_jobqueue import SLURMCluster # Setting up distributed memories via slurm
# from dask.utils import format_bytes
# from dask.distributed import Client, LocalCluster, progress # Libaray to orchestrate distributed resources

# from tempfile import NamedTemporaryFile, TemporaryDirectory # Creating temporary Files/Dirs
# from getpass import getuser # Libaray to copy things
# from pathlib import Path # Object oriented libary to deal with paths
# #from distributed.utils import tmpfile
# #dask.config.set({"distributed.comm.timeouts.tcp": "50s"})

# # %%
