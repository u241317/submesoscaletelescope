# %%
# %%capture
import sys
import glob, os

# os.chdir('/home/m/m300878/submesoscaletelescope/notebooks/may22/')
# sys.path.insert(1, '../')
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
# sys.path.insert(1, '/home/m/m300878/submesoscaletelescope/run_batch_job/model_evaluation/')
import smt_modules.all_funcs as eva

import warnings
warnings.filterwarnings('ignore')  #suppress some warnings about future code changes

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates
from scipy import stats    #Used for 2D binned statistics
from mpl_toolkits.axes_grid1 import make_axes_locatable #For plotting interior colobars
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import pyicon as pyic

# %%
lon_reg       = np.array([-90, -10])
lat_reg       = np.array([15, 60])
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.10_180W-180E_90S-90N.nc'

grid01        = xr.open_dataset(fpath_ckdtree)
grid01_sel    = grid01.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0],lat_reg[1]))

# %%
# # fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
# smt_grid = eva.load_smt_grid()
# # idx_irr_grid = smt_grid.cell.where((smt_grid.clat > lat_reg[0]) & (smt_grid.clat < lat_reg[1]), drop=False) 
# clon = smt_grid.clon.compute().data * 180./np.pi
# clat = smt_grid.clat.compute().data * 180./np.pi

# ireg_c = np.where(
#         (clon>lon_reg[0]) & (clon<=lon_reg[1]) & (clat>lat_reg[0]) & (clat<=lat_reg[1])
#     )[0]
# sst_smt_mean_crop = sst_smt_mean[ireg_c]


# %% load data
sst_satellite = eva.load_satellite_monthly_sst()
sst_satellite = sst_satellite.isel(time=2)
# %%
sst_smt       = eva.load_smt_sst()
sst_smt_mean  = sst_smt.groupby('time.month').mean()
sst_smt_mean  = sst_smt_mean.isel(month=2)

# %%
# %%
from dask.diagnostics import ProgressBar
# sst_smt_mean = sst_smt_mean.compute()
with ProgressBar():
    sst_smt_mean.to_netcdf('/work/mh0033/m300878/smt/sst_mean/sst_march_mean.nc')
# %% interpolate to same grid
sst_smt_mean = xr.open_dataset('/work/mh0033/m300878/smt/sst_mean/sst_march_mean.nc')
# test = sst_smt.isel(time=10)
# data = pyic.interp_to_rectgrid_xr(test, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
smt_data = pyic.interp_to_rectgrid_xr(sst_smt_mean.T001_sp, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%

sst_sat_interp = sst_satellite.sst_night.interp_like(grid01_sel, method='linear', assume_sorted=False, kwargs=None)


# %%
bias = smt_data - sst_sat_interp

# %%
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
hca, hcb = pyic.arrange_axes(1, 3, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
clim = 0, 27
ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
ax.set_title('sst smt')
pyic.shade(smt_data.lon, smt_data.lat, smt_data, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim)
ii+=1; ax=hca[ii]; cax=hcb[ii]
ax.set_title('sst modis aqua')
pyic.shade(sst_sat_interp.lon, sst_sat_interp.lat, sst_sat_interp, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim)
ii+=1; ax=hca[ii]; cax=hcb[ii]
climb = -4, 4
contfs = np.arange(-6.,7, 1)
ax.set_title('bias mod-sat')
pyic.shade(bias.lon, bias.lat, bias, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, contfs=contfs, cmap='Spectral_r', clim=climb )

for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore

plt.savefig(f'../images/forcing/sst_bias.png', dpi=150, bbox_inches='tight')   
# %%
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=2, projection=ccrs_proj, axlab_kw=None)
clim = 0, 27
ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
climb = -4, 4
contfs = np.arange(-6.,7, 1)
ax.set_title('SST bias [°C]')
pyic.shade(bias.lon, bias.lat, bias, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, contfs=contfs, cmap='RdYlBu_r' )
# cax.set_title('C°')

for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    #change maaxnlocator
    ax.yaxis.set_major_locator(plt.MaxNLocator(4))

plt.savefig(f'../images/forcing/sst_bias_single.png', dpi=250,  bbox_inches='tight')   
# %%
