
# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 




def plot_intercomparison(data_tides_t0, data_notides_t0, data_tides_t1, data_notides_t1, var, fig_path, savefig, clim, clim_bias, cmap=' RdYlBu_r', cmap_bias='RdBu_r'):

    fname = f'{var}_tides_notides_' + f'{data_tides_t0.time.data}'[:13]

    if var == 'MLD': units = '[m]'
    elif var == 'vort': units = '[1/s] \n .'
    elif var == 'SST': units = '[C°]'
    elif var == 'SSS': units = '[psu]'
    elif var == 'SSH': units = '[m]'
    elif var == 'u' or var == 'v': units = '[m/s]'

    print(fname)
    lon_reg = np.array([data_tides_t0.lon.min(), data_tides_t0.lon.max()])
    lat_reg = np.array([data_tides_t0.lat.min(), data_tides_t0.lat.max()])
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

    hca, hcb = pyic.arrange_axes(2, 3, plot_cb=[0,1,0,1,0,1], asp=asp, fig_size_fac=1, projection=ccrs_proj, sharex=True, sharey=True, reverse_order=True)

    ii     = -1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_tides_t0.lon, data_tides_t0.lat, data_tides_t0, ax=ax, cax=cax, clim=clim, cmap = cmap)
    ax.set_ylabel(f'tides')
    col = f'{data_tides_t0.time.data}'[:13]
    pad = 10
    ax.annotate(col, xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                size='large', ha='center', va='baseline')
    
    ax.annotate(f'{var}', xy=(0.5, 1), xytext=(95, 25),
                xycoords='axes fraction', textcoords='offset points',
                size='x-large', ha='center', va='baseline')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_notides_t0.lon, data_notides_t0.lat, data_notides_t0, ax=ax, cax=cax, clim=clim, cmap = cmap)
    ax.set_ylabel(f'no tides')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_tides_t0.lon, data_tides_t0.lat, data_tides_t0-data_notides_t0, ax=ax, cax=cax, clim=clim_bias, cmap=cmap_bias)
    ax.set_ylabel('bias: tides - no tides')


    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_tides_t1.lon, data_tides_t1.lat, data_tides_t1, ax=ax, cax=cax, clim=clim, cmap = cmap)
    cax.set_title(f'{units}')
    col = f'{data_tides_t1.time.data}'[:13]
    ax.annotate(col, xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                size='large', ha='center', va='baseline')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_notides_t1.lon, data_notides_t1.lat, data_notides_t1, ax=ax, cax=cax, clim=clim, cmap = cmap)
    cax.set_title(f'{units}')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_tides_t1.lon, data_tides_t1.lat, data_tides_t1-data_notides_t1, ax=ax, cax=cax, clim=clim_bias, cmap=cmap_bias)
    cax.set_title(f'{units}')


    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore   
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore



    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=150, format='png', bbox_inches='tight')



def plot_intercomparison_filtered(data_tides_t0, data_notides_t0, data_tides_t1, data_notides_t1, data_tides_tmean, data_notides_tmean, var, fig_path, savefig, clim, clim_bias, cmap=' RdYlBu_r', cmap_bias='RdBu_r'):

    fname = f'{var}_tides_notides_' + f'{data_tides_t0.time.data}'[:13]

    if var == 'MLD': units = '[m]'
    elif var == 'vort': units = '[1/s] \n .'
    elif var == 'SST': units = '[C°]'
    elif var == 'SSS': units = '[psu]'
    elif var == 'SSH': units = '[m]'
    elif var == 'u' or var == 'v': units = '[m/s]'

    print(fname)
    lon_reg = np.array([data_tides_t0.lon.min(), data_tides_t0.lon.max()])
    lat_reg = np.array([data_tides_t0.lat.min(), data_tides_t0.lat.max()])
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

    hca, hcb = pyic.arrange_axes(3, 3, plot_cb=[0,0,1,0,0,1,0,0,1], asp=asp, fig_size_fac=1, projection=ccrs_proj, sharex=True, sharey=True, reverse_order=True)

    ii     = -1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_tides_t0.lon, data_tides_t0.lat, data_tides_t0, ax=ax, cax=cax, clim=clim, cmap = cmap)
    ax.set_ylabel(f'tides')
    col = f'{data_tides_t0.time.data}'[:13]
    pad = 10
    ax.annotate(col, xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                size='large', ha='center', va='baseline')
    
    ax.annotate(f'{var}', xy=(0.5, 1), xytext=(95, 25),
                xycoords='axes fraction', textcoords='offset points',
                size='x-large', ha='center', va='baseline')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_notides_t0.lon, data_notides_t0.lat, data_notides_t0, ax=ax, cax=cax, clim=clim, cmap = cmap)
    ax.set_ylabel(f'no tides')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_tides_t0.lon, data_tides_t0.lat, data_tides_t0-data_notides_t0, ax=ax, cax=cax, clim=clim_bias, cmap=cmap_bias)
    ax.set_ylabel('bias: tides - no tides')


    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_tides_t1.lon, data_tides_t1.lat, data_tides_t1, ax=ax, cax=cax, clim=clim, cmap = cmap)
    col = f'{data_tides_t1.time.data}'[:13]
    ax.annotate(col, xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                size='large', ha='center', va='baseline')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_notides_t1.lon, data_notides_t1.lat, data_notides_t1, ax=ax, cax=cax, clim=clim, cmap = cmap)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_tides_t1.lon, data_tides_t1.lat, data_tides_t1-data_notides_t1, ax=ax, cax=cax, clim=clim_bias, cmap=cmap_bias)


    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_tides_tmean.lon, data_tides_tmean.lat, data_tides_tmean, ax=ax, cax=cax, clim=clim, cmap = cmap)
    cax.set_title(f'{units}')
    col = f'3d mean'
    ax.annotate(col, xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                size='large', ha='center', va='baseline')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_notides_tmean.lon, data_notides_tmean.lat, data_notides_tmean, ax=ax, cax=cax, clim=clim, cmap = cmap)
    cax.set_title(f'{units}')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_tides_tmean.lon, data_tides_tmean.lat, data_tides_tmean-data_notides_tmean, ax=ax, cax=cax, clim=clim_bias, cmap=cmap_bias)
    cax.set_title(f'{units}')

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore   
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore



    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=150, format='png', bbox_inches='tight')




def plot_intercomparison_tidal_impact(data_tides_tmean, data_notides_tmean, var, fig_path, savefig, time_average_string,  clim_bias, cmap_bias='RdBu_r'):

    fname = f'{var}_tides_notides_impact_{time_average_string}'

    if var == 'MLD': units = '[m]'
    elif var == 'vort': units = '[1/s] \n .'
    elif var == 'SST': units = '[C°]'
    elif var == 'SSS': units = '[psu]'
    elif var == 'SSH': units = '[m]'
    elif var == 'u' or var == 'v': units = '[m/s]'

    print(fname)
    lon_reg = np.array([data_tides_tmean.lon.min(), data_tides_tmean.lon.max()])
    lat_reg = np.array([data_tides_tmean.lat.min(), data_tides_tmean.lat.max()])
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=3, projection=ccrs_proj, sharex=True, sharey=True, reverse_order=True)
    ii     = -1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data_tides_tmean.lon, data_tides_tmean.lat, data_tides_tmean-data_notides_tmean, ax=ax, cax=cax, clim=clim_bias, cmap=cmap_bias)
    cax.set_title(f'{units}')
    ax.set_title(f'Tides - no tides {var} \n {time_average_string}')


    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore   
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    if savefig == True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, format='png', bbox_inches='tight')
