# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import funcs as fu


# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='03:00:00', wait=False)

# client, cluster = scluster.init_my_cluster()
cluster
client

# %%
reload(eva)
# small
lon_reg = np.array([-5, 10])
lat_reg = np.array([-42, -32])
# large
lon_reg = np.array([-25, 20])
lat_reg = np.array([-50, -20])

lon_reg = np.array([3.5, 9])
lat_reg = np.array([-34, -30])
savefig = False

asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
print(asp)
# %%
############## plot config ##############
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/sensitivity/bias/tidal_impact_eddy_zoom/'


fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
# fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'

# %%
# load data with tides at t0 min possible
path_data   = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190704T001500Z.nc'
ds_tides_t0 = xr.open_dataset(path_data, chunks={'time': 1})

path_data     = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0009/outdata_2d/smtwv0009_oce_2d_PT1H_20190704T001500Z.nc'
ds_notides_t0 = xr.open_dataset(path_data, chunks={'time': 1})

path_data   = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190711T201500Z.nc'
ds_tides_t1 = xr.open_dataset(path_data, chunks={'time': 1})

path_data     = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0009/outdata_2d/smtwv0009_oce_2d_PT1H_20190711T201500Z.nc'
ds_notides_t1 = xr.open_dataset(path_data, chunks={'time': 1})

# %%
reload(eva)
begin = '2019-07-09T00:00:00'
end   = '2019-07-11T23:15:00'

begin = '2019-07-20T00:00:00'
end   = '2019-07-22T23:15:00'

time_average_string = f'{begin}'[:10] + f':{end}'[:11]
# %%
ds_tides_tmean = eva.load_smt_wave_2d_exp7_07(it=1)
# %%
reload(eva)
ds_notides_tmean = eva.load_smt_wave_2d_exp9_07(it=1)
# %%
tides_tmean = ds_tides_tmean.sel(time=slice(begin, end)).mean('time')

notides_tmean = ds_notides_tmean.sel(time=slice(begin, end)).mean('time')


# %%
var = 'MLD'
# %%
tides_t0   = ds_tides_t0.mlotst.isel(time=0)
notides_t0 = ds_notides_t0.mlotst.isel(time=0)
tides_t1   = ds_tides_t1.mlotst.isel(time=0)
notides_t1 = ds_notides_t1.mlotst.isel(time=0)
# %%
tides_tmean   = tides_tmean.mlotst
notides_tmean = notides_tmean.mlotst

clim = [0, 300]
clim_bias = 100 

cmap      = 'gist_rainbow_r'
cmap_bias = 'RdBu_r'

# %%

var = 'vort'
# %%
tides_t0   = ds_tides_t0.vort.isel(time=0, depth=1)
notides_t0 = ds_notides_t0.vort.isel(time=0, depth=1)
tides_t1   = ds_tides_t1.vort.isel(time=0, depth=1)
notides_t1 = ds_notides_t1.vort.isel(time=0, depth=1)

data_tides_t0   = pyic.interp_to_rectgrid_xr(  tides_t0, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg, coordinates='vlat vlon')
data_notides_t0 = pyic.interp_to_rectgrid_xr(notides_t0, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg, coordinates='vlat vlon')
data_tides_t1   = pyic.interp_to_rectgrid_xr(  tides_t1, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg, coordinates='vlat vlon')
data_notides_t1 = pyic.interp_to_rectgrid_xr(notides_t1, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg, coordinates='vlat vlon')

# %% #means
tides_tmean   = tides_tmean.vort.isel(depth=1)
notides_tmean = notides_tmean.vort.isel(depth=1)

data_tides_tmean   = pyic.interp_to_rectgrid_xr(  tides_tmean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg, coordinates='vlat vlon')
data_notides_tmean = pyic.interp_to_rectgrid_xr(notides_tmean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg, coordinates='vlat vlon')


cmap = 'RdBu_r'
cmap_bias = 'RdBu_r'
clim = 3e-5
clim_bias = 3e-5

# %%
tides_t0   = ds_tides_t0.to.isel(time=0, depth=0)
notides_t0 = ds_notides_t0.to.isel(time=0, depth=0)
tides_t1   = ds_tides_t1.to.isel(time=0, depth=0)
notides_t1 = ds_notides_t1.to.isel(time=0, depth=0)
# %%
tides_tmean   = tides_tmean.to.isel(depth=0)
notides_tmean = notides_tmean.to.isel(depth=0)

var = 'SST'
clim = [11, 18]
clim_bias = 1

# %%
tides_t0   = ds_tides_t0.zos.isel(time=0)
notides_t0 = ds_notides_t0.zos.isel(time=0)
tides_t1   = ds_tides_t1.zos.isel(time=0)
notides_t1 = ds_notides_t1.zos.isel(time=0)
# %%
tides_tmean   = tides_tmean.zos
notides_tmean = notides_tmean.zos

var = 'SSH'
clim = 1
clim_bias = 0.2

cmap = 'PiYG_r'
cmap_bias = 'RdBu_r'

# %%
tides_t0   = ds_tides_t0.so.isel(time=0, depth=0)
notides_t0 = ds_notides_t0.so.isel(time=0, depth=0)
tides_t1   = ds_tides_t1.so.isel(time=0, depth=0)
notides_t1 = ds_notides_t1.so.isel(time=0, depth=0)

# %%
tides_tmean   = tides_tmean.so.isel(depth=0)
notides_tmean = notides_tmean.so.isel(depth=0)


var = 'SSS'
clim = [34.5, 35.8]
clim_bias = 0.2

cmap = 'viridis'
cmap_bias = 'RdBu_r'

# %%
tides_t0   = ds_tides_t0.u.isel(time=0, depth=0)
notides_t0 = ds_notides_t0.u.isel(time=0, depth=0)
tides_t1   = ds_tides_t1.u.isel(time=0, depth=0)
notides_t1 = ds_notides_t1.u.isel(time=0, depth=0)
# %%
tides_tmean   = tides_tmean.u.isel(depth=0)
notides_tmean = notides_tmean.u.isel(depth=0)

var = 'u'
clim = [-0.5, 0.5]
clim_bias = 0.15

cmap = 'RdBu_r'
cmap_bias = 'RdBu_r'

# %%
tides_t0   = ds_tides_t0.v.isel(time=0, depth=0)
notides_t0 = ds_notides_t0.v.isel(time=0, depth=0)
tides_t1   = ds_tides_t1.v.isel(time=0, depth=0)
notides_t1 = ds_notides_t1.v.isel(time=0, depth=0)
# %%
tides_tmean   = tides_tmean.v.isel(depth=0)
notides_tmean = notides_tmean.v.isel(depth=0)


var = 'v'
clim = [-0.5, 0.5]
clim_bias = 0.15

cmap = 'RdBu_r'
cmap_bias = 'RdBu_r'

# %%
path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190712T061500Z.nc'

ds = xr.open_dataset(path, chunks={'time': 1})
tides_t0 = ds.zos



# %% plot
data_tides_t0   = pyic.interp_to_rectgrid_xr(tides_t0, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) # does not work properly with xarray...?
# data_notides_t0 = pyic.interp_to_rectgrid_xr(notides_t0, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) # does not work properly with xarray...?
# %%
data_tides_t1   = pyic.interp_to_rectgrid_xr(tides_t1, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) # does not work properly with xarray...?
data_notides_t1 = pyic.interp_to_rectgrid_xr(notides_t1, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) # does not work properly with xarray...?
#  %%
data_tides_tmean  = pyic.interp_to_rectgrid_xr(tides_tmean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_notides_tmean  = pyic.interp_to_rectgrid_xr(notides_tmean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)


# %%
if var            == 'SSH':
   data_tides_t0   = data_tides_t0 - data_tides_t0.mean()
   data_notides_t0 = data_notides_t0 - data_notides_t0.mean()
   data_tides_t1   = data_tides_t1 - data_tides_t1.mean()
   data_notides_t1 = data_notides_t1 - data_notides_t1.mean()
# %%
reload(fu)

fu.plot_intercomparison(data_tides_t0, data_notides_t0, data_tides_t1, data_notides_t1, var, fig_path, savefig, clim=clim, clim_bias=clim_bias, cmap=cmap, cmap_bias=cmap_bias)
fu.plot_intercomparison_filtered(data_tides_t0, data_notides_t0, data_tides_t1, data_notides_t1, data_tides_tmean, data_notides_tmean, var, fig_path, savefig, clim, clim_bias, cmap='RdYlBu_r', cmap_bias='RdBu_r')

# %%
reload(fu)
# clim_bias =0.08
fu.plot_intercomparison_tidal_impact(data_tides_tmean, data_notides_tmean, var, fig_path, savefig, time_average_string, clim_bias, cmap_bias)
# %%
lon_reg = np.array([-20, 10])
lat_reg = np.array([-45, -25])

data_tides_tmean  = pyic.interp_to_rectgrid_xr(tides_tmean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_notides_tmean  = pyic.interp_to_rectgrid_xr(notides_tmean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

# %%
(data_tides_tmean - data_notides_tmean).plot(vmin=-50, vmax=50, cmap='RdBu_r')
plt.savefig(f'{fig_path}tidal_impact/MLD_tidal_impact_20190709-11_large.png', dpi=100, bbox_inches='tight')
# %%
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=1, projection=ccrs_proj, sharex=True, sharey=True, reverse_order=True)
ii     = -1
ii+=1; ax=hca[ii]; cax=hcb[ii]
pyic.shade(data_tides_tmean.lon, data_tides_tmean.lat, (data_tides_tmean - data_notides_tmean), ax=ax, cax=cax, clim=[-40,40], cmap='RdBu_r', nclev=21, contfs=True)
for ax in hca:
   pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
# %%
ds = eva.load_smt_wave_all('forcing', exp=7, it =2)
# %%
