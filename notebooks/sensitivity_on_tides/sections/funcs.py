# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 


def plot_section(data, rho, mld, fname, ylim=([500,0]), fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = True, asp=0.3, fig_size_fac=2)  
    ii=-1  
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clevs = np.linspace(-5e-8,5e-8, 15)
    levels = np.linspace(1025,1030,60)
    
    hm2 = pyic.shade(data.lat, data.depth, data.data, ax=ax, cax=cax, clevs=clevs, cmap='RdBu_r')
    ax.contour(data.lat, data.depth, rho.data, levels, colors='gray', linewidths=0.3, label='rho contour')
    ax.plot(data.lat, mld.data, color='green', lw=1, label='MLD')
    ax.set_xlabel('lat')
    ax.set_ylabel('depth [m]')
    ax.set_title(r'$\overline{w^\prime b^\prime}$ [m$^2$/s$^2$]')
    ax.set_ylim(ylim)

    ax.set_xlim([-40,-35])
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)
    if savefig==True: plt.savefig(fig_path+fname, bbox_inches='tight', dpi=200)

def plot_section_tke(data, mld, fname, ylim=([500,0]), fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = True, asp=0.3, fig_size_fac=2)  
    ii=-1  
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    
    hm2 = pyic.shade(data.lat, data.depth, data.data, ax=ax, cax=cax, norm=colors.LogNorm(vmin=1e-5, vmax=1e-3), cmap='RdYlBu_r')
    ax.plot(data.lat, mld.data, color='green', lw=1, label='MLD')

    ax.set_xlabel('lat')
    ax.set_ylabel('depth ')
    ax.set_title(rf'TKE $[m^2/s^2]$ at {data.time.data:.13} ')
    ax.set_ylim(ylim)

    # ax.set_xlim([-40,-35])
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    # hm2[1].forma  tter.set_useMathText(True)
    if savefig==True: plt.savefig(fig_path+fname, bbox_inches='tight', dpi=200)