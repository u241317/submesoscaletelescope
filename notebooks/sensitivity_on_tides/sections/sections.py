# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='02:15:00')
cluster
client
# %%
############## plot config ##############
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/sensitivity/section/'


fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'
lon_reg = np.array([-40, 30])
lat_reg = np.array([-55, -15])

# zoom1
lon_reg1 = np.array([-5, 10])
lat_reg1 = np.array([-42, -32])
lon_reg2 = np.array([-2, 0.5])
lat_reg2 = np.array([-38, -36.75])

clim_vort = 1
clim_sst  = 2, 26# close 14.5,21.7
clim_m2   = 0, 2e-7

savefig = True

# %%
reload(eva)
tid = 'tides'
wb_mean, w_mean, b_mean, tave = eva.load_wb_variables(tid)
wb_prime = wb_mean.wb_mean - w_mean.w * b_mean.b
rho      = eva.compute_density(b_mean.b)
mld      = eva.load_mld_mean(tid)
# %%
i = 1

# sec_list    = ['nps3800_10E45S_10E20S.nc', 'nps3000_10E60S_10E10S.nc']
sec_list    = ['nps3800_0E45S_0E20S.nc', 'nps3000_0E60S_0E10S.nc']
tgname      = 'smtwv_oce_2022'
fname       = f'{tgname}_{sec_list[i]}'
path_to_sec = f'/work/mh0033/m300878/{tgname}/ckdtree/sections/{fname}'
ds_sec      = xr.open_dataset(path_to_sec)

# %%

wb_sec = wb_prime.isel(ncells=ds_sec.ickdtree_c.compute())
wb_sec = wb_sec.assign_coords({'lon':ds_sec.lon_sec, 'lat':ds_sec.lat_sec})
wb_sec = wb_sec.compute()

rho_sec = rho.isel(ncells=ds_sec.ickdtree_c.compute())
mld_sec = mld.isel(ncells=ds_sec.ickdtree_c.compute())
# %%
reload(fu)
fname = f'{tid}_sec_wb_prime_{sec_list[i][:-3]}_zoom.png'
ylim  = ([500,0])
fu.plot_section(wb_sec, rho_sec, mld_sec, fname, ylim, fig_path, savefig)
# %%

############### tke snapshot ###############
time = '2019-08-03-T23:15:00'
tid  = 'notides'
if tid == 'tides':
    ds_tke = eva.load_smt_wave_tke_exp7_08(it=1)
    ds_2d  = eva.load_smt_wave_all('2d', 7, it=1)
    ds_2d = ds_2d.drop(['clon', 'clat'])
elif tid == 'notides':
    ds_tke = eva.load_smt_wave_9(variable='tke', it=1)
    ds_2d  = eva.load_smt_wave_9(variable='2d', it=1)
# %%
mld    = ds_2d.sel(time=time).mlotst
tke    = ds_tke.sel(time=time).tke

# %%
tke_sec = tke.isel(ncells=ds_sec.ickdtree_c.compute())
tke_sec = tke_sec.assign_coords({'lon':ds_sec.lon_sec, 'lat':ds_sec.lat_sec})
tke_sec = tke_sec.compute()
mld_sec = mld.isel(ncells=ds_sec.ickdtree_c.compute())
mld_sec = mld_sec.compute()

# %%
reload(fu)
fname = f'{tid}_sec_tke_{sec_list[i][:-3]}.png'
ylim  = ([350,0])
fu.plot_section_tke(tke_sec, mld_sec, fname, ylim, fig_path, savefig)
# %%
