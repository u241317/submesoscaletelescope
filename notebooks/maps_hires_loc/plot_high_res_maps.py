# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp
import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors

import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, dash_address='8787')
cluster
client
# %%
reload(eva)
reload(smt_vort)
savefig = False
# %%
############## plot config ##############

path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
# time_averaged = '_2d_mean'
time_averaged = ''
# time_averaged_b = '_2D_mean'

fig_path       = '/home/m/m300878/submesoscaletelescope/notebooks/images/first_paper/overview/'
fpath_ckdtree  = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.npz'
fpath_ckdtree2  = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.10_180W-180E_90S-90N.nc'

fname          = 'na'

lon_reg = np.array([-90, -10])
lat_reg = np.array([15, 60])

lon_reg_1 = lon_reg
lat_reg_1 = lat_reg
# lon_reg_2 = np.array([-72, -49])
# lat_reg_2 = np.array([23.5, 36.5])
lon_reg_2 = np.array([-75, -46])
lat_reg_2 = np.array([23.5, 40])
# lon_reg_3 = np.array([-63.8, -61.5])
# lat_reg_3 = np.array([34.4, 35.7])

fn     = 1
hedge  = 1.25
asp3   = 1.279999999
points = eva.get_points_of_inclined_fronts()

P = points[(2*fn-2):2*fn,:]
P_lon_mean = np.mean(P[:,0])
P_lat_mean = np.mean(P[:,1])
lon_reg_3 = np.array([P_lon_mean-hedge, P_lon_mean+hedge])
lat_reg_3 = np.array([P_lat_mean-hedge/asp3, P_lat_mean+hedge/asp3])



lon_R   = np.array([-70, -54])
lat_R   = np.array([23.5, 36])
# lon_RF1 = np.array([-63.9, -61.5])
# lat_RF1 = np.array([34, 35.875])

# lon_reg_3 = lon_RF1
# lat_reg_3 = lat_RF1

clim_vort = 1
clim_sst  = 2, 26# close 14.5,21.7
clim_m2   = 0, 2e-7

savefig = False


# %% ############## load grid ##############
gg  = eva.load_smt_grid()
res = np.sqrt(gg.cell_area_p)
res = res.rename(cell='ncells')
rlon, rlat, data_r_v = pyic.interp_to_rectgrid(res, fpath_ckdtreeZ, lon_reg=lon_reg, lat_reg=lat_reg) # does not work properly with xarray...?
data_r = pyic.interp_to_rectgrid_xr(res, fpath_ckdtree2, lon_reg=lon_reg, lat_reg=lat_reg) # does not work properly with xarray...?

############## load temperature ##############
# %% instantan
ds        = eva.load_smt_sst()
sst       = ds.isel(time=867)
a         = sst.where(~(sst==0), np.nan)
mask_land = ~np.isnan(a)
mask_land = mask_land.where(mask_land==True, np.nan)
# %%

sst             = sst * mask_land
data_sst_coarse = pyic.interp_to_rectgrid_xr(sst, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
Tri_sst, data_reg_sst = eva.calc_triangulation_obj(sst, lon_reg_3, lat_reg_3)
data_sst              = pyic.interp_to_rectgrid_xr(sst, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
############## load vorticity ##############
vort             = eva.load_smt_vorticity()
# vorticity        = vort.isel(time=620)
vorticity        = vort.sel(time='2010-03-18T19')
# %%
data_vort_coarse = pyic.interp_to_rectgrid_xr(vorticity.vort_f_cells_50m, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
Tri_vort, data_reg_vort = eva.calc_triangulation_obj(vorticity.vort_f_cells_50m, lon_reg_3, lat_reg_3)
data_vort               = pyic.interp_to_rectgrid_xr(vorticity.vort_f_cells_50m, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

############### load M2 ###############
# %% 
path_data       = f'{path_root_dat}bx{time_averaged}.nc'
dbdx_mean       = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data       = f'{path_root_dat}by{time_averaged}.nc'
dbdy_mean       = xr.open_dataset(path_data, chunks=dict(depthi=1))
dbdx            = dbdx_mean.isel(depthi=16).dbdx
dbdy            = dbdy_mean.isel(depthi=16).dbdy
data_m2         = np.abs(dbdx) + np.abs(dbdy)

if time_averaged != '':
    data_m2 = data_m2.isel(time=0)

# data_M2_mean   = np.abs(data_dbdx_mean) + np.abs(data_dbdy_mean)
#data_dbdx_mean = pyic.interp_to_rectgrid_xr(dbdx, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# data_m2   = pyic.interp_to_rectgrid_xr(M2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_m2_coarse   = pyic.interp_to_rectgrid_xr(data_m2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_m2, data_reg_m2 = eva.calc_triangulation_obj(data_m2, lon_reg_3, lat_reg_3)

################ wb prime ###############
# %%
path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
chunks_i = {'depthi': 1}

path_data     = f'{path_root_dat}wb{time_averaged}_prime.nc'
wb_prime_mean = xr.open_dataset(path_data, chunks=chunks_i)
wb_prime_mean = wb_prime_mean.rename(__xarray_dataarray_variable__='wb_prime_mean')
wb_prime_mean = wb_prime_mean.isel(depthi=1).wb_prime_mean
# %%
if time_averaged != '':
    wb_prime_mean = wb_prime_mean.isel(time=0)

data_wb_prime_mean_coarse = pyic.interp_to_rectgrid_xr(wb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_wb, data_reg_wb_prime_mean   = eva.calc_triangulation_obj(wb_prime_mean, lon_reg_3, lat_reg_3)

# %%

if time_averaged != '':
    path_data     = f'{path_root_dat}ub_prime{time_averaged_b}.nc'
else:
    path_data          = f'{path_root_dat}ub{time_averaged}_prime.nc'
ub_prime_mean      = xr.open_dataset(path_data, chunks={'depthc': 1})
if time_averaged == '': 
    ub_prime_mean      = ub_prime_mean.rename(__xarray_dataarray_variable__='ub_prime_mean')
    ub_prime_mean      = ub_prime_mean.drop(['clon', 'clat'])
ub_prime_mean      = ub_prime_mean.isel(depthc=16).ub_prime_mean
# %%
if time_averaged != '':
    ub_prime_mean = ub_prime_mean.isel(time=0)

data_ub_prime_mean_coarse = pyic.interp_to_rectgrid_xr(ub_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_ub, data_reg_ub_prime_mean   = eva.calc_triangulation_obj(ub_prime_mean, lon_reg_3, lat_reg_3)

# %%
if time_averaged != '':
    path_data     = f'{path_root_dat}vb_prime{time_averaged_b}.nc'
else:
    path_data          = f'{path_root_dat}vb{time_averaged}_prime.nc'
vb_prime_mean      = xr.open_dataset(path_data, chunks={'depthc': 1})
if time_averaged == '':
    vb_prime_mean      = vb_prime_mean.rename(__xarray_dataarray_variable__='vb_prime_mean')
    vb_prime_mean      = vb_prime_mean.drop(['clon', 'clat'])
vb_prime_mean      = vb_prime_mean.isel(depthc=16).vb_prime_mean

if time_averaged != '':
    vb_prime_mean = vb_prime_mean.isel(time=0)

data_vb_prime_mean_coarse = pyic.interp_to_rectgrid_xr(vb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_vb, data_reg_vb_prime_mean   = eva.calc_triangulation_obj(vb_prime_mean, lon_reg_3, lat_reg_3)


#######################################################################################################
#######################################################################################################
# %% classic 3 figure plot
reload(smt_temp)
reload(fu)

# fu.plot_temperature_zoom(data_sst, data_sst_coarse, Tri_sst, data_reg_sst, lat_reg_1, lon_reg_1, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, rlon, rlat, data_r, fig_path, savefig )
# fu.plot_temperature_zoom2(data_sst, Tri_sst, data_reg_sst,  data_r, rlon, rlat, fig_path, savefig)
fu.plot_T_zoom(data_sst, Tri_sst, data_reg_sst, lon_reg_1, lat_reg_1, lon_R, lat_R, lon_RF1, lat_RF1, rlon, rlat, data_r, fig_path, savefig)



# %% vorticity
reload(smt_vort)
reload(fu)
reload(eva)

fu.plot_vorticity_zoom(data_vort, data_vort_coarse, Tri_vort, data_reg_vort, lat_reg_1, lon_reg_1,  rlon, rlat, data_r_v, fig_path, savefig, fn)

# %% 
reload(fu)
reload(fu)
lon_reg = -78, -58
lat_reg = 32, 42

# data_vort_coarse_sel = data_vort_coarse.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0], lat_reg[1]))   
data_vort_coarse_sel = pyic.interp_to_rectgrid_xr(vorticity.vort_f_cells_50m, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

data_r_sel = data_r.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0], lat_reg[1]))

fu.plot_vort_sel_na(data_vort, lat_reg, lon_reg, data_r_sel, fig_path, savefig)


# %%
reload(fu)
reload(tools)
lon_reg = -78, -58
lat_reg = 32, 42
var = vorticity.vort_f_cells_50m

var.attrs["long_name"] = ""
var.attrs["units"] = "" 


fu.plot_vort_sel_na2(var, lat_reg, lon_reg, data_r_sel, fig_path, savefig)

# %%
# %%
reload(tools)
reload(fu)
reload(tools)

tsring    = '2010-03-16T07'
vorticity = vort.sel(time=tsring)
lon_reg   = -78, -58
lat_reg   = 32,  42

lon_reg   = -78, -56
lat_reg   = 31,  42
lon_reg   = -81, -40
lat_reg   = 23,  37
var       = vorticity.vort_f_cells_50m

var.attrs["long_name"] = ""
var.attrs["units"] = "" 

data_r_sel = data_r.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0], lat_reg[1]))
# %%
reload(tools)
fig, ax, artist = tools.plot_ds_with_cartopy(var, lon_range=lon_reg, lat_range=lat_reg)

levels = 550+np.arange(20)*50
CS     = ax.contour(data_r_sel.lon, data_r_sel.lat, data_r_sel, levels, linewidths=0.9, colors='grey', linestyles='solid' )
fmt    = '%.0f'+'m'
l      = ax.clabel(CS,  inline=True, inline_spacing=-8, fmt=fmt, fontsize=9)
for t in l:
    t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white', pad=0.1))

def add_front_lines_box(ax):
    points = eva.get_points_of_inclined_fronts()
    for ii in np.arange(int(points.shape[0]/2)):
        P = points[2*ii:2*ii+2,:]
        y, x, m, b = eva.calc_line(P)
        delta = 0.8*0.1 # 0.8 view 0.1 actual front
        color = 'm'
        lw    = 3
        ls    = '-'
        ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
        ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
        ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
        ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)

add_front_lines_box(ax)

ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
if savefig == True: plt.savefig(f'{fig_path}{fname}_vort_slm_fronts_ds2_250_20_{tsring}.png', dpi=250, format='png', bbox_inches='tight')



   # %% wb_prime_mean
reload(fu)
fu.plot_wb_prime_zoom_2(data_wb_prime_mean_coarse, data_wb_prime_mean_coarse, Tri_wb, data_reg_wb_prime_mean, lat_reg_1, lon_reg_1, data_r.lon, data_r.lat, data_r, fig_path, savefig)
# %%
fu.plot_ub_prime_zoom_2(data_ub_prime_mean_coarse, data_ub_prime_mean_coarse, Tri_ub, data_reg_ub_prime_mean, lat_reg_1, lon_reg_1, rlon, rlat, data_r, fig_path, savefig)
fu.plot_vb_prime_zoom_2(data_vb_prime_mean_coarse, data_vb_prime_mean_coarse, Tri_vb, data_reg_vb_prime_mean, lat_reg_1, lon_reg_1, rlon, rlat, data_r, fig_path, savefig)

# %% m2
reload(fu)
fu.plot_m2_zoom_2(data_m2_coarse, data_m2_coarse, Tri_m2, data_reg_m2, lat_reg_1, lon_reg_1, rlon, rlat, data_r, fig_path, savefig)


# %% general plot function
fname = 'na'
data  = data_sst
clim  = clim_sst
cmap  = 'plasma' #'RdPu_r'
fu.plot_na(data, lat_reg, lon_reg, clim, rlon, rlat, data_r, cmap, fig_path, fname)

################### data for m2 n2 and ri:#############################################################
#######################################################################################################
# %% 
mask_land = eva.load_smt_land_mask(17)
#%%

# lon_reg_3 = np.array([-63.9, -61.5])
# lat_reg_3 = np.array([34, 35.875])
# select front 
fn     = 4
hedge  = 1.25
asp3   = 1.279999999
points = eva.get_points_of_inclined_fronts()

P = points[(2*fn-2):2*fn,:]
P_lon_mean = np.mean(P[:,0])
P_lat_mean = np.mean(P[:,1])
lon_reg_3 = np.array([P_lon_mean-hedge, P_lon_mean+hedge])
lat_reg_3 = np.array([P_lat_mean-hedge/asp3, P_lat_mean+hedge/asp3])

lon_reg_2 = np.array([-70, -54])
lat_reg_2 = np.array([23.5, 36])

asp3 = (lat_reg_3[1]-lat_reg_3[0])/(lon_reg_3[1]-lon_reg_3[0])
asp2 = (lat_reg_2[1]-lat_reg_2[0])/(lon_reg_2[1]-lon_reg_2[0])
print('asp2', asp2, 'asp3',asp3)
idepth        = 17

path_data          = f'{path_root_dat}bx{time_averaged}.nc'
dbdx_mean          = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data          = f'{path_root_dat}by{time_averaged}.nc'
dbdy_mean          = xr.open_dataset(path_data, chunks=dict(depthi=1))
m2_mean            = np.abs(dbdx_mean.dbdx.isel(depthi=idepth)) + np.abs(dbdy_mean.dbdy.isel(depthi=17))
data_coarse_m2     = pyic.interp_to_rectgrid_xr(m2_mean*mask_land, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
Tri, data_reg_m2   = eva.calc_triangulation_obj(m2_mean*mask_land, lon_reg_3, lat_reg_3)

path_data          = f'{path_root_dat}n2{time_averaged}.nc'
n2_mean            = xr.open_dataset(path_data, chunks=dict(depthi=1))
n2_mean            = n2_mean.N2.isel(depthi=idepth)
data_coarse_n2     = pyic.interp_to_rectgrid_xr(n2_mean*mask_land, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
Tri, data_reg_n2   = eva.calc_triangulation_obj(n2_mean*mask_land, lon_reg_3, lat_reg_3)

data_coarse_ri = eva.calc_richardsonnumber(data_coarse_m2.lat, data_coarse_n2, data_coarse_m2)
data_reg_ri    = eva.calc_richardsonnumber(lat_reg_3.mean(), data_reg_n2, data_reg_m2)


f = eva.calc_coriolis_parameter(np.rad2deg(gg.clat.data))
Tri, data_reg_m2_f   = eva.calc_triangulation_obj(m2_mean/(f**2), lon_reg_3, lat_reg_3)


# Tri, data_reg_mask   = eva.calc_triangulation_obj(mask_land, lon_reg_3, lat_reg_3)
# mask_land_reg = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
# %%
reload(fu)
# fu.plot_region_zoom_vertical(data_coarse_m2, data_reg_m2, data_reg_m2_f, data_coarse_n2, data_reg_n2, data_coarse_ri, data_reg_ri, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2)
# fu.plot_region_zoom_vertical_front4(data_coarse_m2, data_reg_m2, data_reg_m2_f, data_coarse_n2, data_reg_n2, data_coarse_ri, data_reg_ri, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2, fn)
fu.plot_region_zoom_vertical_front4_cm(data_coarse_m2, data_reg_m2, data_reg_m2_f, data_coarse_n2, data_reg_n2, data_coarse_ri, data_reg_ri, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2, fn)


# %%
fu.plot_region_zoom_horizontal(data_coarse_m2, data_reg_m2, data_coarse_n2, data_reg_n2, data_coarse_ri, data_reg_ri, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2)
# %% 
fu.plot_m2_zoom(data_coarse_m2, Tri, data_reg_m2, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2)

#######################################################################################################
#######################################################################################################
# %% plot eddies

idepth = 17
lon_reg_2 = np.array([-74, -60])
lat_reg_2 = np.array([24, 38])

path_data      = f'{path_root_dat}bx{time_averaged}.nc'
dbdx_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
path_data      = f'{path_root_dat}by{time_averaged}.nc'
dbdy_mean      = xr.open_dataset(path_data, chunks=dict(depthi=1))
m2_mean        = np.abs(dbdx_mean.dbdx.isel(depthi=idepth)) + np.abs(dbdy_mean.dbdy.isel(depthi=17))
data_coarse_m2 = pyic.interp_to_rectgrid_xr(m2_mean, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)

asp2 = (lat_reg_2[1]-lat_reg_2[0])/(lon_reg_2[1]-lon_reg_2[0])
# %%
reload(fu)
reload(eva)
fu.plot_sel_eddies(data_coarse_m2, idepth, lat_reg_2, lon_reg_2, fig_path, savefig, asp2)

# %%
vort = eva.load_smt_vorticity()
# %%
t0 = '2010-03-15T21'
t1 = '2010-03-22T21'
vort = vort.sel(time=slice(t0, t1))
vort = vort.vort_f_cells_50m.mean('time').drop(['clon', 'clat'])
vort = vort.compute()
# %%
vort_interp = pyic.interp_to_rectgrid_xr(vort, fpath_ckdtree, lon_reg=lon_reg_2, lat_reg=lat_reg_2)
# %%
reload(fu)
reload(eva)
fu.plot_sel_eddies_vort(vort_interp, lat_reg_2, lon_reg_2, fig_path, savefig, asp2)
# %%
