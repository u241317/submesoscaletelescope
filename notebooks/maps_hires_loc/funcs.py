# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp
import string
from matplotlib.ticker import FormatStrFormatter

import seaborn as sns

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import cartopy.feature as cfeature

import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
from matplotlib.patches import Rectangle


# %%
def plot_single_front_margins(ax):
    points = eva.get_points_of_inclined_fronts()
    P = points[0:2,:]
    y, x, m, b = eva.calc_line(P)
    delta = 0.8*0.1 # 0.8 view 0.1 actual front
    color = 'r'
    lw    = 3
    ls    = ':'
    ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
    ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
    ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
    ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
    t = ax.text(P[0,0]-0.05, P[0,1]+0.2, f'F{1}', color='black', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

def plot_single_front_margins_fn(ax, fn):
    points = eva.get_points_of_inclined_fronts()
    if fn == 1:
        P = points[0:2,:]
    elif fn == 4:
        P = points[6:8,:]
    elif fn == 9:
        P = points[16:18,:]
    y, x, m, b = eva.calc_line(P)
    delta = 0.8*0.1 # 0.8 view 0.1 actual front
    color = 'r'
    lw    = 3
    ls    = ':'
    ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
    ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
    ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
    ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
    t = ax.text(P[0,0]-0.05, P[0,1]+0.2, f'F{fn}', color='black', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


def add_front_lines(ax):
    points = eva.get_points_of_inclined_fronts()
    for ii in np.arange(int(points.shape[0]/2)):
        P = points[2*ii:2*ii+2,:]
        y, x, m, b = eva.calc_line(P)
        delta=0.8#eva.convert_degree_to_meter(int(y[0]))*0.1
        ax.plot(x,y,lw=3, color='k')
        # ax.plot(x,y+delta,lw=1, color='r')
        # ax.plot(x,y-delta,lw=1, color='r')
        #ax.text(P[0,0], P[0,1], f'F{ii+1}', color='black', fontsize=20)

def add_front_lines_box(ax):
    points = eva.get_points_of_inclined_fronts()
    for ii in np.arange(int(points.shape[0]/2)):
        P = points[2*ii:2*ii+2,:]
        y, x, m, b = eva.calc_line(P)
        delta=0.8*0.1 # 0.8 view 0.1 actual front
        color = 'r'
        lw = 1
        ls = ':'
        # ax.plot(x,y, lw=lw, color=color, ls=ls)
        ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
        ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
        ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
        ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
        # t = ax.text(P[0,0]-0.05, P[0,1], f'F{1}', color='black', fontsize=20)
        # t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

def add_scale(ax, fi=0):
    points = eva.get_points_of_inclined_fronts()

    P = points[2*fi-2:2*fi,:]
    y, x, m, b = eva.calc_line(P)
    # draw red line with shift below
    shift = 0.3
    ax.plot(x,y-shift, lw=3, color='r', ls='-')
    #add vertival lines at the ends of the line
    ax.vlines(x[0],y[0]-shift-0.1, y[0]-shift+0.1, lw=3, color='r', ls='-')
    ax.vlines(x[-1],y[-1]-shift-0.1, y[-1]-shift+0.1, lw=3, color='r', ls='-')
    # and to the side
    delta=0.8*0.1 # 0.8 view 0.1 actual front
    ax.vlines(x[0]-0.1,(y+delta)[0], (y-delta)[0], lw=3, color='r', ls='-')
    ax.hlines((y+delta)[0], x[0]-0.1-0.07, x[0]-0.1+0.07, lw=3, color='r', ls='-')
    ax.hlines((y-delta)[0], x[0]-0.1-0.07, x[0]-0.1+0.07, lw=3, color='r', ls='-')

    # add text in the middle of the line above the line
    #compute distance in km from first to last point
    dist_deg = np.sqrt((P[0,0]-P[1,0])**2 + (P[0,1]-P[1,1])**2)
    dist_km = eva.convert_degree_to_meter(y.mean())*dist_deg/1000
    t = ax.text(x.mean(), P[0,1]-0.4, f'{dist_km:.0f}km', color='red', fontsize=22, rotation=8, ha='center')
    dist_km_v = eva.convert_degree_to_meter(y[0])/1000 * 2 * delta
    t1 = ax.text(x[0]-0.35, y[0], f'{dist_km_v:.0f}km', color='red', fontsize=22, rotation=90, va='center')
    return ax


def add_scale4(ax, fi=0):
    points = eva.get_points_of_inclined_fronts()

    P = points[(2*fi-2):2*fi,:]
    y, x, m, b = eva.calc_line(P)
    # draw red line with shift below
    shift = 0.3
    ax.plot(x,y-shift, lw=3, color='r', ls='-')
    #add vertival lines at the ends of the line
    ax.vlines(x[0],y[0]-shift-0.1, y[0]-shift+0.1, lw=3, color='r', ls='-')
    ax.vlines(x[-1],y[-1]-shift-0.1, y[-1]-shift+0.1, lw=3, color='r', ls='-')
    # and to the side
    delta=0.8*0.1 # 0.8 view 0.1 actual front
    ax.vlines(x[0]-0.1,(y+delta)[0], (y-delta)[0], lw=3, color='r', ls='-')
    ax.hlines((y+delta)[0], x[0]-0.1-0.07, x[0]-0.1+0.07, lw=3, color='r', ls='-')
    ax.hlines((y-delta)[0], x[0]-0.1-0.07, x[0]-0.1+0.07, lw=3, color='r', ls='-')

    # add text in the middle of the line above the line
    #compute distance in km from first to last point
    dist_deg = np.sqrt((P[0,0]-P[1,0])**2 + (P[0,1]-P[1,1])**2)
    dist_km = eva.convert_degree_to_meter(y.mean())*dist_deg/1000
    t = ax.text(x.mean(), P[0,1]-0.8, f'{dist_km:.0f}km', color='red', fontsize=22, rotation=np.rad2deg(m), ha='center')
    dist_km_v = eva.convert_degree_to_meter(y[0])/1000 * 2 * delta
    t1 = ax.text(x[0]-0.35, y[1], f'{dist_km_v:.0f}km', color='red', fontsize=22, rotation=90, va='center')
    return ax

def add_rectangles(ax, lon_reg, lat_reg, txt, color='black'):
    rect, right, top = eva.draw_rect(lat_reg, lon_reg,  color=color)
    ax.add_patch(rect)
    ax.text(lon_reg[1], lat_reg[1], f'{txt}', color=color, fontsize=20)

def add_eddy_slice(ax, color='r'):
    points = eva.get_eddies()
    # ax.scatter(points[:,0], points[:,1], s=10, c='r' )
    i=0
    for ii in np.arange(points.shape[0]):
        i+=2
        ax.plot(points[i:i+2,0], points[i:i+2,1], c=color, linewidth=2)

def get_lon_lat_horizontal_sst(k, n):
    lon1 = -72.5, - 56
    dlat = 0.04166412
    y1 = 28.98
    lati = y1, y1+dlat
    #lati = 25.8, 26.2
    lati = lati + np.ones(2)*1.7*k
    Lat = np.zeros((n, 2))
    Lon = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lati
        Lon[ii,:] = lon1
        lati += np.ones(2)*0.05 # slightly larger then grid spacing
    return(Lat, Lon)

def get_lon_lat_vertical_sst(k,n):
    lat1 = 28, 40
    dlon = 0.04166412
    y1   = -68
    loni = y1, y1+dlon
    loni = loni + np.ones(2)*1.7*k
    Lat  = np.zeros((n, 2))
    Lon  = np.zeros((n, 2))
    for ii in np.arange(n):
        Lat[ii,:] = lat1
        Lon[ii,:] = loni
        loni += np.ones(2)*0.05 # slightly larger then grid spacing
    return(Lat, Lon)

def add_sst_eval_lines(ax):
    alpha = 0.9
    color='darkviolet'
    lw = 0.1
    for k in range(6):
        Lat, Lon = get_lon_lat_horizontal_sst(k, n=30) #TODO wrong name for regions
        for ii in np.arange(30):
            ax.plot(Lon[ii,:], (Lat[ii,0], Lat[ii,0]), transform=ccrs_proj, color=color, linewidth=lw, alpha =alpha)
        Lat, Lon = get_lon_lat_vertical_sst(k, n=30)
        for ii in np.arange(30):
            ax.plot((Lon[ii,0],Lon[ii,0]), (Lat[ii,1], Lat[ii,0]), transform=ccrs_proj, color=color, linewidth=lw, alpha =alpha)

def plot_na(data, lat_reg, lon_reg, clim, rlon, rlat, data_r, cmap, fig_path, fname):

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=5, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap)
    levels = 600+np.arange(20)*100

    CS = ax.contour(rlon, rlat, data_r, levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)

    add_rectangles(ax, np.array([-69, -54]), np.array([23.5, 36.5]), 'b')
    add_rectangles(ax, np.array([-72.5, - 56]), np.array([28, 40]), 'a')

    add_sst_eval_lines(ax)
    add_front_lines(ax)

    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)


    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    plt.savefig(f'{fig_path}{fname}.png', dpi=150, format='png', bbox_inches='tight')

def plot_temperature_zoom(data, data_sst_coarse, Tri_sst, data_reg_sst, lat_reg_1, lon_reg_1, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, rlon, rlat, data_r, fig_path, savefig ):
    fname = 'na_zoom_sst'
    cmap = 'RdYlBu_r' 
    clim  = 2, 26# close 14.5,21.7
    dpi = 250
    fig, hca, hcb = smt_temp.make_axes(lon_reg_1, lat_reg_1, lon_reg_2, lat_reg_2, lon_reg_3, lat_reg_3, dpi)
    ax=hca[0]; cax=hcb[0]

    hm1 = pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, cmap = cmap, 
                    transform=ccrs_proj, rasterized=False)

    cax.set_ylabel('SST in [°C]')
    levels = 600+np.arange(20)*100
    CS = ax.contour(rlon, rlat, data_r, levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)
    add_rectangles(ax, lon_reg_2, lat_reg_2, 'a')
    # add_rectangles(ax, np.array([-72.5, - 56]), np.array([28, 40]), 'a')
    add_sst_eval_lines(ax)
    # add_front_lines(ax)

    pyic.plot_settings(ax, xlim=lon_reg_1, ylim=lat_reg_1) # type: ignore

    clim = 15, 23
    ax=hca[1]; cax=hcb[1]
    hm2 = pyic.shade(data_sst_coarse.lon, data_sst_coarse.lat, data_sst_coarse, ax=ax, cax=cax, clim=clim, cmap = cmap,
                    transform=ccrs_proj, rasterized=False)
    # add_front_lines(ax)
    add_rectangles(ax, lon_reg_3, lat_reg_3, 'b')

    ax=hca[2]; cax=hcb[2]
    clim = 17.5, 19
    hm3 = pyic.shade(Tri_sst, data_reg_sst, ax=ax, cax=cax, clim=clim, cmap = cmap,
                    transform=ccrs_proj, rasterized=False)

    # points = eva.get_points_of_inclined_fronts()
    # P = points[0:2,:]
    # y, x, m, b = eva.calc_line(P)
    # delta=0.8*0.1 # 0.8 view 0.1 actual front
    # color = 'k'
    # lw = 3
    # ls = ':'
    # ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
    # ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
    # ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
    # ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
    # ax.text(P[0,0]-0.05, P[0,1], f'F{1}', color='black', fontsize=20)

    pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3, do_xyticks=False) # type: ignore
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=dpi, format='png', bbox_inches='tight')


def plot_vort_zoom(data_vort, patches_v, data_native_interp, lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, data_r, tave, fig_path, savefig):
    fname = f'sa_zoom_vort_tave{tave}'
    cmap  = 'RdBu_r'
    # cmap  = 'PRGn'
    dpi   = 250
    clim  = 2

    fig, hca, hcb = smt_vort.make_axes3(lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, dpi)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid' )
    # fmt = '%.1f'
    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_reg1[1]-2, lat_reg1[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_reg1, lat_reg1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    rectprop = dict(facecolor='none', edgecolor='0.7', lw=2)
    ax.add_patch(Rectangle((-12,-37.5), 5, 4, **rectprop))
    ax.text(-9.5, -33.5, 'SONETT II', ha='center', va='bottom', fontsize=8)

    rectprop = dict(facecolor='none', edgecolor='0.7', lw=2)
    ax.add_patch(Rectangle((1,-35), 9, 5, **rectprop))
    ax.text(5.5, -30., 'SONETT I', ha='center', va='bottom', fontsize=8)

    ax.text(-2.5, -35, 'Walvis Ridge', va='center', ha='center', rotation=45, fontsize=8)


    #########
    ax=hca[1]; cax=hcb[0]
    hm2 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-1.2, lat_RF1[1]-0.6, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_reg1[1]-0.6, lat_reg1[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_RF1, lat_RF1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg1, ylim=lat_reg1, do_xyticks=False) # type: ignore

    #########
    ax=hca[2]; cax=hcb[0]
    hm3 = pyic.patch_plot_shade(patches_v, data_native_interp, ax=ax, cax=cax, clim=clim)

    t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_RF1, ylim=lat_RF1, do_xyticks=False) # type: ignore

    hcb[0].set_title(r'$\zeta / f$', fontsize=20, rotation=0, pad=20)
    hcb[0].tick_params(labelsize=14)


    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=dpi, format='png', bbox_inches='tight')

def plot_vort_sel(data_vort, lon_reg, lat_reg, data_r, fig_path, savefig):
    cmap  = 'RdBu_r'
    dpi   = 250
    clim  = 1
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

    hue_neg, hue_pos = 20, 250
    cmap  = f'sns–{hue_neg}-{hue_pos}-1'
    fname = f'sa_sel_vort2_{cmap}'
    import seaborn as sns
    cmap = sns.diverging_palette(hue_neg, hue_pos, center="light", as_cmap=True)

    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=2, projection=ccrs_proj, axlab_kw=None)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    #change position of colorbar into upper right corner
    cax.set_position([0.84, 0.7, 0.01, 0.12])
    levels = 550+np.arange(20)*10
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, linewidths=0.8, colors='grey', linestyles='solid' )
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, fmt=fmt, fontsize=8)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white', pad=0.1))

    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore

    rectprop = dict(facecolor='none', edgecolor='0.7', lw=2)
    ax.add_patch(Rectangle((-12,-37.5), 5, 4, **rectprop))
    # ax.text(-9.5, -33.5, 'SONETT II', ha='center', va='bottom', fontsize=8)

    rectprop = dict(facecolor='none', edgecolor='0.7', lw=2)
    # ax.add_patch(Rectangle((1,-35), 9, 5, **rectprop))
    # ax.text(5.5, -30., 'SONETT I', ha='center', va='bottom', fontsize=8)

    # ax.text(-2.5, -35, 'Walvis Ridge', va='center', ha='center', rotation=45, fontsize=8)


    hcb[0].set_title(r'$\zeta / f$', fontsize=10, rotation=0, pad=10)
    hcb[0].tick_params(labelsize=7)


    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=dpi, format='png', bbox_inches='tight')


def plot_temperature_zoom2(data, Tri_sst, data_reg_sst, data_r, rlon, rlat, fig_path, savefig ):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = np.array([-63.9, -61.5])
    lat_RF1 = np.array([34, 35.875])

    lon_reg_1 = [data.lon[0], data.lon[-1]]
    lat_reg_1 = [data.lat[0], data.lat[-1]]

    fname = 'na_zoom_sst2'
    cmap = 'plasma' 
    clim  = 2, 26# close 14.5,21.7
    dpi = 250
    fig, hca, hcb = smt_vort.make_axes3(lon_reg_1, lat_reg_1, lon_R, lat_R, lon_RF1, lat_RF1, dpi)
    ax=hca[0]; cax=hcb[0]

    hm1 = pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, cmap = cmap, 
                    transform=ccrs_proj, rasterized=False)

    cax.set_ylabel('SST in [°C]')
    levels = 600+np.arange(20)*100
    CS = ax.contour(rlon, rlat, data_r, levels, colors='grey', linestyles='solid')
    ax.clabel(CS, inline=True, fontsize=10)

    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_R[1]-2, lat_R[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_R, lat_R, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg_1, ylim=lat_reg_1) # type: ignore

    clim = 15, 23
    ax=hca[1]; cax=hcb[1]
    hm2 = pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, cmap = cmap,
                    transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-1.2, lat_RF1[1]-0.6, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_R[1]-0.6, lat_R[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_R, lat_R, '', color='red')


    ax=hca[2]; cax=hcb[2]
    clim = 17.5, 19
    hm3 = pyic.shade(Tri_sst, data_reg_sst, ax=ax, cax=cax, clim=clim, cmap = cmap,
                    transform=ccrs_proj, rasterized=False)
    
    t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_RF1, ylim=lat_RF1, do_xyticks=False) # type: ignore
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=dpi, format='png', bbox_inches='tight')



def plot_tke_zoom(data_vort, Tri_vort, data_reg_vort, lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, data_r, fig_path, savefig):
    fname = 'na_zoom_tke_mean'
    cmap  = 'plasma'
    dpi   = 250
    clim  = np.array([1e-6, 5e-4])

    fig, hca, hcb = smt_vort.make_axes3(lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, dpi)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid' )
    # fmt = '%.1f'
    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_reg1[1]-2, lat_reg1[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_reg1, lat_reg1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    #########
    ax=hca[1]; cax=hcb[0]
    hm2 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-1.2, lat_RF1[1]-0.6, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_reg1[1]-0.6, lat_reg1[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_RF1, lat_RF1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg1, ylim=lat_reg1, do_xyticks=False) # type: ignore

    #########
    ax=hca[2]; cax=hcb[0]
    hm3 = pyic.shade(Tri_vort, data_reg_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_RF1, ylim=lat_RF1, do_xyticks=False) # type: ignore

    hcb[0].set_title(r'$TKE [m^2/s^2]$', fontsize=10, rotation=0, pad=20)
    hcb[0].tick_params(labelsize=14)

    if savefig == True: plt.savefig(f'{fig_path}{fname}_new3.png', dpi=dpi, format='png', bbox_inches='tight')


def plot_wb_prime_zoom(data_vort, Tri_vort, data_reg_vort, lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, data_r, tave, fig_path, savefig):
    fname = f'sa_zoom_wb_prime_{tave}'
    cmap  = 'RdBu_r'
    # cmap  = 'PRGn'
    dpi   = 250
    clim  = np.array([-1.5e-7, 1.5e-7])

    fig, hca, hcb = smt_vort.make_axes3(lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, dpi)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid' )
    # fmt = '%.1f'
    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_reg1[1]-2, lat_reg1[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_reg1, lat_reg1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    #########
    ax=hca[1]; cax=hcb[0]
    hm2 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-1.2, lat_RF1[1]-0.6, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_reg1[1]-0.6, lat_reg1[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_RF1, lat_RF1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg1, ylim=lat_reg1, do_xyticks=False) # type: ignore

    #########
    ax=hca[2]; cax=hcb[0]
    hm3 = pyic.shade(Tri_vort, data_reg_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_RF1, ylim=lat_RF1, do_xyticks=False) # type: ignore

    hcb[0].set_title(r'$\overline{w^\prime b^\prime}$', fontsize=10, rotation=0, pad=20)
    hcb[0].tick_params(labelsize=14)

    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=dpi, format='png', bbox_inches='tight')


def plot_T_zoom(data_vort, Tri_vort, data_reg_vort, lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, rlon, rlat, data_r, fig_path, savefig):
    fname = 'na_zoom_SST'
    cmap  = 'RdBu_r'
    dpi   = 250
    clim  = 2, 26# close 14.5,21.7

    fig, hca, hcb = smt_vort.make_axes2(lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, dpi)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*100
    CS = ax.contour(rlon, rlat, data_r, levels, colors='grey', linestyles='solid' )
    # fmt = '%.1f'
    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_reg1[1]-2, lat_reg1[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_reg1, lat_reg1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    #########
    clim = 15, 23
    ax=hca[1]; cax=hcb[0]
    hm2 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-1.2, lat_RF1[1]-0.6, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_reg1[1]-0.6, lat_reg1[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_RF1, lat_RF1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg1, ylim=lat_reg1, do_xyticks=False) # type: ignore

    #########
    clim = 17.5, 19

    ax=hca[2]; cax=hcb[0]
    hm3 = pyic.shade(Tri_vort, data_reg_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_RF1, ylim=lat_RF1, do_xyticks=False) # type: ignore

    hcb[0].set_title(r'SST [°C]', fontsize=10, rotation=0, pad=20)
    hcb[0].tick_params(labelsize=14)

    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=dpi, format='png', bbox_inches='tight')


def plot_m2_zoom2(data_vort, Tri_vort, data_reg_vort, lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, data_r, fig_path, savefig):
    fname = 'na_zoom_m2_mean'
    cmap  = 'RdPu_r'
    dpi   = 250
    clim  = np.array([0, 1e-7])

    fig, hca, hcb = smt_vort.make_axes3(lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, dpi)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid' )
    # fmt = '%.1f'
    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_reg1[1]-2, lat_reg1[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_reg1, lat_reg1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    #########
    ax=hca[1]; cax=hcb[0]
    hm2 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-1.2, lat_RF1[1]-0.6, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_reg1[1]-0.6, lat_reg1[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_RF1, lat_RF1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg1, ylim=lat_reg1, do_xyticks=False) # type: ignore

    #########
    ax=hca[2]; cax=hcb[0]
    hm3 = pyic.shade(Tri_vort, data_reg_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_RF1, ylim=lat_RF1, do_xyticks=False) # type: ignore

    hcb[0].set_title(r'$\nabla_h b $', fontsize=10, rotation=0, pad=20)
    hcb[0].tick_params(labelsize=14)

    if savefig == True: plt.savefig(f'{fig_path}{fname}_new3.png', dpi=dpi, format='png', bbox_inches='tight')


def plot_MLD_zoom(data_vort, Tri_vort, data_reg_vort, lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, data_r, fig_path, savefig):
    fname = 'zoom_mld_mean'
    dpi   = 250
    levels = np.array([200])
    cmap   = 'gist_rainbow_r'
    contfs = np.array([10, 15, 20, 40 ,60, 80, 100, 125, 150, 175, 200, 250, 300, 350, 400, 500])#np.arange(50.,550.,50.)
    clim   = contfs[0], contfs[-1]

    fig, hca, hcb = smt_vort.make_axes3(lon_reg, lat_reg, lon_reg1, lat_reg1, lon_RF1, lat_RF1, dpi)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, contfs=contfs, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid' )
    # fmt = '%.1f'
    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_reg1[1]-2, lat_reg1[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_reg1, lat_reg1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    #########
    ax=hca[1]; cax=hcb[0]
    hm2 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, contfs=contfs, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-1.2, lat_RF1[1]-0.6, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_reg1[1]-0.6, lat_reg1[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_RF1, lat_RF1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg1, ylim=lat_reg1, do_xyticks=False) # type: ignore

    #########
    ax=hca[2]; cax=hcb[0]
    hm3 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, contfs=contfs, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_RF1, ylim=lat_RF1, do_xyticks=False) # type: ignore

    hcb[0].set_title(r'$\nabla_h b $', fontsize=10, rotation=0, pad=20)
    hcb[0].tick_params(labelsize=14)

    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=dpi, format='png', bbox_inches='tight')


def plot_vorticity_zoom(data_vort, data_vort_coarse, Tri_vort, data_reg_vort, lat_reg_1, lon_reg_1,  rlon, rlat, data_r, fig_path, savefig, fn):
    fname = 'na_zoom_vort'
    cmap  = 'RdBu_r'
    dpi   = 250
    clim  = 1

    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])

    hedge  = 1.25
    asp3   = 1.279999999
    points = eva.get_points_of_inclined_fronts()

    P = points[(2*fn-2):2*fn,:]
    P_lon_mean = np.mean(P[:,0])
    P_lat_mean = np.mean(P[:,1])
    lon_reg_3 = np.array([P_lon_mean-hedge, P_lon_mean+hedge])
    lat_reg_3 = np.array([P_lat_mean-hedge/asp3, P_lat_mean+hedge/asp3])




    fig, hca, hcb = smt_vort.make_axes2(lon_reg_1, lat_reg_1, lon_R, lat_R, lon_reg_3, lat_reg_3, dpi)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*100
    CS = ax.contour(rlon, rlat, data_r, levels, colors='grey', linestyles='solid' )
    # fmt = '%.1f'
    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_R[1]-2, lat_R[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_R, lat_R, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg_1, ylim=lat_reg_1) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    #########
    ax=hca[1]; cax=hcb[0]
    hm2 = pyic.shade(data_vort_coarse.lon, data_vort_coarse.lat, data_vort_coarse, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_reg_3[1]-1.2, lat_reg_3[1]-0.6, f'RF{fn}', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_R[1]-0.6, lat_R[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_reg_3, lat_reg_3, '', color='red')

    pyic.plot_settings(ax, xlim=lon_R, ylim=lat_R, do_xyticks=False) # type: ignore

    #########
    ax=hca[2]; cax=hcb[0]
    hm3 = pyic.shade(Tri_vort, data_reg_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_reg_3[1]-0.3, lat_reg_3[1]-0.15, f'RF{fn}', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3, do_xyticks=False) # type: ignore

    hcb[0].set_title(r'$~\frac{\zeta}{f}$', fontsize=25, rotation=0, pad=20)
    hcb[0].tick_params(labelsize=14)

    if savefig == True: plt.savefig(f'{fig_path}{fname}_new{fn}.png', dpi=dpi, format='png', bbox_inches='tight')



def plot_vort_sel_na(data_vort, lat_reg, lon_reg, data_r, fig_path, savefig):
    fname = 'na_sel_vort'
    dpi   = 250
    clim  = 1

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

    hue_neg, hue_pos = 110, 20
    cmap  = f'sns–{hue_neg}-{hue_pos}-1'
    import seaborn as sns
    cmap = sns.diverging_palette(hue_neg, hue_pos, s=90, center="light", as_cmap=True)
    cmap  = 'RdBu_r'
    fname = f'sa_sel_vort2_{cmap}'

    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=2, projection=ccrs_proj, axlab_kw=None)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    #change position of colorbar into upper right corner
    cax.set_position([0.1, 0.73, 0.01, 0.12])
    # cax.set_major_locator(plt.MaxNLocator(2))
    # set only 2 ticks in colorbar
    cax.yaxis.set_major_locator(plt.MaxNLocator(2))
    # set rcparam to ascii hypen
    plt.rcParams['axes.unicode_minus'] = False
    levels = 550+np.arange(20)*10
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, linewidths=0.8, colors='grey', linestyles='solid' )
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=-8, fmt=fmt, fontsize=8)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white', pad=0.1))

    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    hcb[0].set_title(r'$~\zeta /f$', fontsize=12, rotation=0, pad=5)
    hcb[0].tick_params(labelsize=9)

    if savefig == True: plt.savefig(f'{fig_path}{fname}_new3.png', dpi=dpi, format='png', bbox_inches='tight')

def plot_vort_sel_na2(var, lat_reg, lon_reg, data_r, fig_path, savefig):
    fname = 'na_sel_vort'
    dpi   = 250
    clim  = 1

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

    # hue_neg, hue_pos = 110, 20
    # cmap  = f'sns–{hue_neg}-{hue_pos}-1'
    # cmap = sns.diverging_palette(hue_neg, hue_pos, s=90, center="light", as_cmap=True)
    # cmap  = 'RdBu_r'
    # fname = f'sa_sel_vort2_{cmap}'

    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=2, axlab_kw=None)
    print('no projection')
    # hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=0.5, fig_size_fac=2,  axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # fig = plt.gcf()

    ax = tools.basic_plot(var, ax, cax,  native_grid=True, lon_range=lon_reg, lat_range=lat_reg, time_pos=0, vminmax=[-1,1], cmap='RdBu_r')
    cax.set_position([0.1, 0.73, 0.01, 0.12])
    cax.yaxis.set_major_locator(plt.MaxNLocator(2))

    plt.rcParams['axes.unicode_minus'] = False
    levels = 550+np.arange(20)*10
    CS     = ax.contour(data_r.lon, data_r.lat, data_r, levels, linewidths=0.8, colors='grey', linestyles='solid' )
    fmt    = '%.0f'+'m'
    l      = ax.clabel(CS,  inline=True, inline_spacing=-8, fmt=fmt, fontsize=8)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white', pad=0.1))

    ax.add_feature(cartopy.feature.LAND, facecolor='black')

    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    # ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    hcb[0].set_title(r'$~\zeta /f$', fontsize=12, rotation=0, pad=5)
    hcb[0].tick_params(labelsize=9)

    if savefig == True: plt.savefig(f'{fig_path}{fname}_ds.png', dpi=dpi, format='png', bbox_inches='tight')



def plot_wb_prime_zoom_2(data_vort, data_vort_coarse, Tri_vort, data_reg_vort, lat_reg_1, lon_reg_1,  rlon, rlat, data_r, fig_path, savefig):
    fname = 'na_zoom_wb_prime_top'
    cmap  = 'RdBu_r'
    dpi   = 250
    clim  = 5e-8#1.3e-7

    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = np.array([-63.9, -61.5])
    lat_RF1 = np.array([34, 35.875])

    fig, hca, hcb = smt_vort.make_axes2(lon_reg_1, lat_reg_1, lon_R, lat_R, lon_RF1, lat_RF1, dpi)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*100
    CS = ax.contour(rlon, rlat, data_r, levels, colors='grey', linestyles='solid' )
    # fmt = '%.1f'
    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_R[1]-2, lat_R[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_R, lat_R, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg_1, ylim=lat_reg_1) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    #########
    ax=hca[1]; cax=hcb[0]
    hm2 = pyic.shade(data_vort_coarse.lon, data_vort_coarse.lat, data_vort_coarse, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-1.2, lat_RF1[1]-0.6, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_R[1]-0.6, lat_R[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_RF1, lat_RF1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_R, ylim=lat_R, do_xyticks=False) # type: ignore

    #########
    ax=hca[2]; cax=hcb[0]
    hm3 = pyic.shade(Tri_vort, data_reg_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_RF1, ylim=lat_RF1, do_xyticks=False) # type: ignore

    hcb[0].set_title(r'$~\overline{w^\prime b^\prime} [\frac{m^2}{s^3}]$', fontsize=13, rotation=0, pad=20)
    hcb[0].tick_params(labelsize=14)

    if savefig == True: plt.savefig(f'{fig_path}{fname}_new3.png', dpi=dpi, format='png', bbox_inches='tight')



def plot_ub_prime_zoom_2(data_vort, data_vort_coarse, Tri_vort, data_reg_vort, lat_reg_1, lon_reg_1,  rlon, rlat, data_r, fig_path, savefig):
    fname = 'na_zoom_ub_prime'
    cmap  = 'RdBu_r'
    dpi   = 250
    clim  = 7e-5

    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = np.array([-63.9, -61.5])
    lat_RF1 = np.array([34, 35.875])

    fig, hca, hcb = smt_vort.make_axes2(lon_reg_1, lat_reg_1, lon_R, lat_R, lon_RF1, lat_RF1, dpi)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*100
    CS = ax.contour(rlon, rlat, data_r, levels, colors='grey', linestyles='solid' )
    # fmt = '%.1f'
    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_R[1]-2, lat_R[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_R, lat_R, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg_1, ylim=lat_reg_1) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    #########
    ax=hca[1]; cax=hcb[0]
    hm2 = pyic.shade(data_vort_coarse.lon, data_vort_coarse.lat, data_vort_coarse, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-1.2, lat_RF1[1]-0.6, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_R[1]-0.6, lat_R[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_RF1, lat_RF1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_R, ylim=lat_R, do_xyticks=False) # type: ignore

    #########
    ax=hca[2]; cax=hcb[0]
    hm3 = pyic.shade(Tri_vort, data_reg_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_RF1, ylim=lat_RF1, do_xyticks=False) # type: ignore

    hcb[0].set_title(r'$~\overline{u^\prime b^\prime} [\frac{m^2}{s^3}]$', fontsize=13, rotation=0, pad=20)
    hcb[0].tick_params(labelsize=14)

    if savefig == True: plt.savefig(f'{fig_path}{fname}_new3.png', dpi=dpi, format='png', bbox_inches='tight')


def plot_vb_prime_zoom_2(data_vort, data_vort_coarse, Tri_vort, data_reg_vort, lat_reg_1, lon_reg_1,  rlon, rlat, data_r, fig_path, savefig):
    fname = 'na_zoom_vb_prime'
    cmap  = 'RdBu_r'
    dpi   = 250
    clim  = 7e-5

    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = np.array([-63.9, -61.5])
    lat_RF1 = np.array([34, 35.875])

    fig, hca, hcb = smt_vort.make_axes2(lon_reg_1, lat_reg_1, lon_R, lat_R, lon_RF1, lat_RF1, dpi)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*100
    CS = ax.contour(rlon, rlat, data_r, levels, colors='grey', linestyles='solid' )
    # fmt = '%.1f'
    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_R[1]-2, lat_R[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_R, lat_R, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg_1, ylim=lat_reg_1) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    #########
    ax=hca[1]; cax=hcb[0]
    hm2 = pyic.shade(data_vort_coarse.lon, data_vort_coarse.lat, data_vort_coarse, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-1.2, lat_RF1[1]-0.6, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_R[1]-0.6, lat_R[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_RF1, lat_RF1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_R, ylim=lat_R, do_xyticks=False) # type: ignore

    #########
    ax=hca[2]; cax=hcb[0]
    hm3 = pyic.shade(Tri_vort, data_reg_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_RF1, ylim=lat_RF1, do_xyticks=False) # type: ignore

    hcb[0].set_title(r'$~\overline{v^\prime b^\prime} [\frac{m^2}{s^3}]$', fontsize=13, rotation=0, pad=20)
    hcb[0].tick_params(labelsize=14)

    if savefig == True: plt.savefig(f'{fig_path}{fname}_new3.png', dpi=dpi, format='png', bbox_inches='tight')




def plot_m2_zoom_2(data_vort, data_vort_coarse, Tri_vort, data_reg_vort, lat_reg_1, lon_reg_1,  rlon, rlat, data_r, fig_path, savefig):
    fname = 'na_zoom_m2'
    cmap  = 'BuPu'
    dpi   = 250
    clim  = [0, 1e-7]

    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = np.array([-63.9, -61.5])
    lat_RF1 = np.array([34, 35.875])

    fig, hca, hcb = smt_vort.make_axes2(lon_reg_1, lat_reg_1, lon_R, lat_R, lon_RF1, lat_RF1, dpi)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*100
    CS = ax.contour(rlon, rlat, data_r, levels, colors='grey', linestyles='solid' )
    # fmt = '%.1f'
    l = ax.clabel(CS, inline=True, fontsize=13)
    # set background color of contourlabels
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white'))
    t = ax.text(lon_R[1]-2, lat_R[1]-2, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_R, lat_R, '', color='red')

    pyic.plot_settings(ax, xlim=lon_reg_1, ylim=lat_reg_1) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    #########
    ax=hca[1]; cax=hcb[0]
    hm2 = pyic.shade(data_vort_coarse.lon, data_vort_coarse.lat, data_vort_coarse, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-1.2, lat_RF1[1]-0.6, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    t = ax.text(lon_R[1]-0.6, lat_R[1]-0.6, 'R', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    add_rectangles(ax, lon_RF1, lat_RF1, '', color='red')

    pyic.plot_settings(ax, xlim=lon_R, ylim=lat_R, do_xyticks=False) # type: ignore

    #########
    ax=hca[2]; cax=hcb[0]
    hm3 = pyic.shade(Tri_vort, data_reg_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)

    t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    pyic.plot_settings(ax, xlim=lon_RF1, ylim=lat_RF1, do_xyticks=False) # type: ignore

    hcb[0].set_title(r'$M2$', fontsize=13, rotation=0, pad=20)
    hcb[0].tick_params(labelsize=14)

    if savefig == True: plt.savefig(f'{fig_path}{fname}_new3.png', dpi=dpi, format='png', bbox_inches='tight')



def plot_m2_zoom(data_coarse, Tri, data_reg, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2):
    clim_m2   = 0, 1e-7
    cmap = 'BuPu'
    # ---
    hca, hcb = pyic.arrange_axes(2,1, plot_cb='right', asp=asp2, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, axlab_kw=None,
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # data =  data_coarse
    clim = clim_m2

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    add_front_lines_box(ax)
    add_rectangles(ax, lon_reg_3, lat_reg_3, 'a', color='r')
    ax.set_title(rf'|dbdx| + |dbdy| at {depthi[idepth]}m', fontsize = 15)
    pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) # type: ignore
    # ---
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # clim = 17,18.7
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    points = eva.get_points_of_inclined_fronts()
    P = points[0:2,:]
    y, x, m, b = eva.calc_line(P)
    delta=0.8*0.1 # 0.8 view 0.1 actual front
    color = 'r'
    lw = 3
    ls = ':'
    # ax.plot(x,y, lw=lw, color=color, ls=ls)
    ax.set_title('a', fontsize=15)
    ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
    ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
    ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
    ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
    t = ax.text(P[0,0]-0.05, P[0,1]+0.2, f'F{1}', color='black', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    #----
    # ht = ax.set_title(str(f'{timesd[step].data:.13}h').replace('T',' '), loc='right', fontsize=10)

    pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3) # type: ignore
    fname = 'location_fronts'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250, format='png', bbox_inches='tight')

def plot_region_zoom_vertical(data_coarse_m2, data_reg_m2, data_reg_m2_f, data_coarse_n2, data_reg_n2, data_coarse_ri, data_reg_ri, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2):
    hca, hcb = pyic.arrange_axes(2, 3,  asp=asp2, plot_cb=[0,1,0,1,0,1], fig_size_fac=2, daxl=0.9,  # type: ignore
                                 projection=ccrs_proj,
                                 daxt=1, f_dcbt=1.5)
    ii=-1

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0, 1e-7
    cmap = 'BuPu'
    data_coarse = data_coarse_m2
    data_reg    = data_reg_m2
    ax.set_title(rf'$|\Delta_h \overline{{b}}|~[ 1/s^2]$ at {depthi[idepth]}m')
    # ax.set_ylabel(rf'$|\Delta_h \overline{{b}} | ~[ 1/s^2]$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1e-6, 4e-5
    cmap = 'Spectral_r'
    data_coarse = data_coarse_n2
    data_reg    = data_reg_n2
    ax.set_title(r'${N^2} ~[1/s^2]$'+f' at {depthi[idepth]}m')
    # ax.set_ylabel(r'${N^2}$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)


    ############################################

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0, 20
    cmap = 'Blues_r'
    data_coarse = data_coarse_ri
    data_reg    = data_reg_ri
    ax.set_title(rf'$Ri$ at {depthi[idepth]}m')
    # ax.set_ylabel(rf'$Ri$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    add_scale(ax)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    ############################################ Alpha
    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # clim = 0, 20
    # cmap = 'RdPu'
    # data_coarse = data_coarse_m2
    # f = eva.calc_coriolis_parameter(data_coarse.lat)
    # data_reg    = data_reg_m2_f

    # ax.set_title(rf'$|\Delta_h \overline{{b}}|/f^2$ at {depthi[idepth]}m')

    # hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse/f**2, ax=ax, cax=cax, clim=clim, cmap=cmap,
    #                 transform=ccrs_proj, rasterized=False)

    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
    #                 transform=ccrs_proj, rasterized=False)
    # hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    # hm2[1].formatter.set_useMathText(True)

    ############################################

    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = np.array([-63.9, -61.5])
    lat_RF1 = np.array([34, 35.875])

    i=0
    for ax in hca:
        i += 1
        if i%2 == 1:
            add_front_lines_box(ax)
            add_rectangles(ax, lon_reg_3, lat_reg_3, '', color='red')
            t = ax.text(lon_R[1]-0.9, lat_R[1]-1, 'R', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
            pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) # type: ignore
        else:
            pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3) # type: ignore
            plot_single_front_margins(ax)
            t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore


    fname = 'map_param_fronts_portrait_8'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250, format='png', bbox_inches='tight')



def plot_region_zoom_vertical_front4(data_coarse_m2, data_reg_m2, data_reg_m2_f, data_coarse_n2, data_reg_n2, data_coarse_ri, data_reg_ri, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2, fi):
    hca, hcb = pyic.arrange_axes(2, 3,  asp=asp2, plot_cb=[0,1,0,1,0,1], fig_size_fac=2, daxl=0.9,  # type: ignore
                                 projection=ccrs_proj,
                                 daxt=1, f_dcbt=1.5)
    ii=-1

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0, 1e-7
    cmap = 'BuPu'
    data_coarse = data_coarse_m2
    data_reg    = data_reg_m2
    ax.set_title(rf'$|\Delta_h \overline{{b}}|~[ 1/s^2]$ at {depthi[idepth]}m')
    # ax.set_ylabel(rf'$|\Delta_h \overline{{b}} | ~[ 1/s^2]$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1e-6, 4e-5
    cmap = 'Spectral_r'
    data_coarse = data_coarse_n2
    data_reg    = data_reg_n2
    ax.set_title(r'${N^2} ~[1/s^2]$'+f' at {depthi[idepth]}m')
    # ax.set_ylabel(r'${N^2}$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)


    ############################################

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0, 20
    cmap = 'Blues_r'
    data_coarse = data_coarse_ri
    data_reg    = data_reg_ri
    ax.set_title(rf'$Ri$ at {depthi[idepth]}m')
    # ax.set_ylabel(rf'$Ri$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)
    add_scale4(ax, fi)



    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = lon_reg_3
    lat_RF1 = lat_reg_3

    i=0
    for ax in hca:
        i += 1
        if i%2 == 1:
            add_front_lines_box(ax)
            add_rectangles(ax, lon_reg_3, lat_reg_3, '', color='red')
            t = ax.text(lon_R[1]-0.9, lat_R[1]-1, 'R', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
            pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) # type: ignore
        else:
            pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3) # type: ignore
            plot_single_front_margins_fn(ax, fi)
            t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, f'RF{fi}', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
            #plot coastlines
            ax.coastlines(resolution='10m', color='black', linewidth=1)

        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore


    fname = f'map_param_fronts_portrait_{fi}'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250, format='png', bbox_inches='tight')


def plot_region_zoom_vertical_front4_cm(data_coarse_m2, data_reg_m2, data_reg_m2_f, data_coarse_n2, data_reg_n2, data_coarse_ri, data_reg_ri, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2, fi):
    hca, hcb = pyic.arrange_axes(2, 3,  asp=asp2, plot_cb=[0,1,0,1,0,1], fig_size_fac=2, daxl=0.9,  # type: ignore
                                 projection=ccrs_proj,
                                 daxt=1, f_dcbt=1.5)
    ii=-1

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0, 1e-7
    cmap = 'BuPu'
    data_coarse = data_coarse_m2
    data_reg    = data_reg_m2
    ax.set_title(rf'$|\Delta_h \overline{{b}}|~[ 1/s^2]$ at {depthi[idepth]}m')
    # ax.set_ylabel(rf'$|\Delta_h \overline{{b}} | ~[ 1/s^2]$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1e-6, 4e-5
    cmap = 'Spectral_r'
    data_coarse = data_coarse_n2
    data_reg    = data_reg_n2
    ax.set_title(r'${N^2} ~[1/s^2]$'+f' at {depthi[idepth]}m')
    # ax.set_ylabel(r'${N^2}$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)


    ############################################

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim=0, 100
    cmap = plt.cm.Spectral_r
    colors_s = cmap(np.linspace(0, 1, 10))
    cmap = colors.ListedColormap(colors_s)
    cmap.set_bad(color='white')
    cmap.set_over(color='white')
    data_coarse = data_coarse_ri
    data_reg    = data_reg_ri
    ax.set_title(rf'$Ri$ at {depthi[idepth]}m')
    # ax.set_ylabel(rf'$Ri$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)
    add_scale4(ax, fi)



    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = lon_reg_3
    lat_RF1 = lat_reg_3

    i=0
    for ax in hca:
        i += 1
        if i%2 == 1:
            add_front_lines_box(ax)
            add_rectangles(ax, lon_reg_3, lat_reg_3, '', color='red')
            t = ax.text(lon_R[1]-0.9, lat_R[1]-1, 'R', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
            pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) # type: ignore
        else:
            pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3) # type: ignore
            plot_single_front_margins_fn(ax, fi)
            t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, f'RF{fi}', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
            #plot coastlines
            ax.coastlines(resolution='10m', color='black', linewidth=1)

        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore


    fname = f'map_param_fronts_portrait_{fi}_cm'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250, format='png', bbox_inches='tight')


def plot_region_zoom_horizontal(data_coarse_m2, data_reg_m2, data_coarse_n2, data_reg_n2, data_coarse_ri, data_reg_ri, Tri, idepth, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, fig_path, savefig, asp2):
    hca, hcb = pyic.arrange_axes(3, 2,  asp=asp2, plot_cb=[0,0,0,1,1,1], fig_size_fac=2, daxl=0.9,  # type: ignore
                                 projection=ccrs_proj, sharey=True,
                                 daxt=1, f_dcbt=1.5, reverse_order=True)
    ii=-1
    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0, 1e-7
    cmap = 'BuPu'
    data_coarse = data_coarse_m2
    data_reg    = data_reg_m2
    ax.set_title(rf'$|\overline{{dbdx}}| + |\overline{{dbdy}}|$ at {depthi[idepth]}m')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1e-6, 4e-5
    cmap = 'Spectral_r'
    data_coarse = data_coarse_n2
    data_reg    = data_reg_n2
    ax.set_title(rf'$\overline{{N^2}}$ at {depthi[idepth]}m')


    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)


    ############################################

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0, 50
    cmap = 'Blues_r'
    data_coarse = data_coarse_ri
    data_reg    = data_reg_ri
    ax.set_title(rf'$Ri$ at {depthi[idepth]}m')

    norm = colors.LogNorm(vmin=data_coarse.min(), vmax=data_coarse.max())

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    # hm2[1].remove()
    # # remove colorbar and plot new one
    # cbar = plt.colorbar(hm2,  cax, orientation='horizontal')

    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)


    i=0
    for ax in hca:
        i += 1
        if i%2 == 1:
            add_front_lines_box(ax)
            add_rectangles(ax, lon_reg_3, lat_reg_3, 'R', color='red')
            pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) # type: ignore
        else:
            pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3) # type: ignore
            plot_single_front_margins(ax)
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore


    fname = 'map_param_fronts'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250, format='png', bbox_inches='tight')

def plot_sel_eddies(data_coarse_m2, idepth, lat_reg_2, lon_reg_2, fig_path, savefig, asp2):
    hca, hcb = pyic.arrange_axes(1, 1,  asp=asp2, plot_cb=False, fig_size_fac=1.3, daxl=0.9,  # type: ignore
                             projection=ccrs_proj,
                             daxt=1, f_dcbt=1.5)
    ii=-1

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0,1e-7
    cmap = 'BuPu'
    data_coarse = data_coarse_m2

    ax.set_title(rf'$|\Delta_h \overline{{b}}|~[ 1/s^2]$ at {depthi[idepth]}m')
    # ax.set_ylabel(rf'$|\Delta_h \overline{{b}} | ~[ 1/s^2]$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)


    points = eva.get_eddies()
    i =0
    for ii in range(0, int(len(points)/2)):
        if ii == 0: continue
        i = ii*2 #+1
        t = ax.text(points[i,0]+0.15, points[i,1], f'E{ii}', color='red', fontsize=6, fontweight='bold')
        # t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        # above textbox but with tighter box
        
    
    pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) # type: ignore
    add_eddy_slice(ax)
    
    ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore


    fname = 'map_eddy_slice'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250, format='png', bbox_inches='tight')

def plot_sel_eddies_vort(data_coarse_m2, lat_reg_2, lon_reg_2, fig_path, savefig, asp2):
    hca, hcb = pyic.arrange_axes(1, 1,  asp=asp2, plot_cb=False, fig_size_fac=1.3, daxl=0.9,  # type: ignore
                             projection=ccrs_proj,
                             daxt=1, f_dcbt=1.5)
    ii=-1

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = -1,1
    cmap = 'RdBu_r'
    data_coarse = data_coarse_m2

    ax.set_title(rf'$\overline{{\zeta}}/f$ at {50}m')

    # ax.set_ylabel(rf'$|\Delta_h \overline{{b}} | ~[ 1/s^2]$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)


    points = eva.get_eddies()
    i =0
    for ii in range(0, int(len(points)/2)):
        if ii == 0: continue
        i = ii*2 #+1
        t = ax.text(points[i,0]+0.15, points[i,1], f'E{ii}', color='purple', fontsize=7, fontweight='bold')
        # t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        # above textbox but with tighter box
        
    
    pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) # type: ignore
    add_eddy_slice(ax, color='purple')
    
    ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore


    fname = 'map_eddy_slice_vort'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250, format='png', bbox_inches='tight')


def plot_slice_front(x, y, M2, wb_prime, TKE, rho, mld,  ylim=[5000,0], fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(1, 3, plot_cb=True, asp=0.5, fig_size_fac=1, sharex=True, sharey=False, daxt=0.9) # type: ignore
    ii=-1
    nclev             = 20
    cticks            = 6

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-1.5e-7,1.5e-7]) 
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, M2, ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='PuOr', extend='both')
    ax.set_title(r'$M^2  ~ [1/s^2]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([-2e-7,2e-7])
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, wb_prime,  ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.set_title(r'$ \overline{w^{\prime} b^{\prime}}  ~ [m^2/s^3]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = np.array([0,5e-4])
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, TKE,  ax=ax, cax=cax, contfs=True, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='plasma', extend='both')
    ax.set_title(r'$ TKE ~ [m^2/s^2]$')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)


    for ax in hca:
        levels = np.linspace(1025,1030,60)
        ax.contour(x, rho.depth, rho.data, levels, colors='gray', linewidths=0.75, label='rho contour')
        ax.set_ylabel(r'$z ~ [m]$', rotation=90)
        ax.set_ylim(ylim)
        ax.plot(x, mld, color='green', linewidth=2, linestyle='dashed', label=r'MLD with: $\Delta \rho=0.2$')




    ax.plot([], [], 'grey', label="Isopycnal")
    ax.legend(loc='lower right')


    if savefig == True: plt.savefig(fig_path+'slice_mean2.png', dpi=250, format='png', bbox_inches='tight')



def plot_slice_front_tidenotide(x, y, data_tides, data_notides, rho_tide, rho_notide, mld_tide, mld_notide,  ylim=[5000,0], fig_path=None, savefig=False):
    hca, hcb = pyic.arrange_axes(1, 2, plot_cb='right', asp=0.5, fig_size_fac=2, sharex=True, sharey=False, daxt=0.9) # type: ignore
    ii=-1
    nclev  = 20
    cticks = 6
    levels = np.linspace(1025,1030,60)
    clim   = np.array([-1e-7,1e-7])

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, data_notides,  ax=ax, cax=cax, contfs=False, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.set_title(r'$ \overline{w^{\prime} b^{\prime}}  ~ [m^2/s^3]$'+' \n no tides')
    # hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    # hm[1].formatter.set_useMathText(True)
    ax.plot(x, mld_tide, color='green', linewidth=2, linestyle='dashed', label=r'MLD with: $\Delta \rho=0.2$')
    ax.contour(x, rho_tide.depth, rho_tide.data, levels, colors='gray', linewidths=0.75, label='rho contour')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    cbticks = np.linspace(clim[0],clim[1],cticks)
    hm = pyic.shade(x, y, data_tides,  ax=ax, cax=cax, contfs=False, nclev=nclev, cbticks=cbticks,  clim=clim, cmap='RdBu_r', extend='both')
    ax.set_title(r'$tides$')
    # hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    # hm[1].formatter.set_useMathText(True)
    ax.plot(x, mld_notide, color='green', linewidth=2, linestyle='dashed', label=r'MLD with: $\Delta \rho=0.2$')
    ax.contour(x, rho_notide.depth, rho_notide.data, levels, colors='gray', linewidths=0.75, label='rho contour')
    ax.set_xlabel('lat')

    for ax in hca:
        ax.set_ylabel(r'$z ~ [m]$', rotation=90)
        ax.set_ylim(ylim)

    ax.plot([], [], 'grey', label="Isopycnal")
    ax.legend(loc='lower right')

    if savefig == True: plt.savefig(fig_path+'slice_tidenotide.png', dpi=250, format='png', bbox_inches='tight')


# %%
def compute_slice(data, fpath_ckdtree, lon_reg, lat_reg, lon_slice=None, lat_slice=None):
    """ computes slice on interpolated grid along lon or lat """
    data_interp = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    if lon_slice != None:
        data_slice  = data_interp.sel(lon=lon_slice, method='nearest')
    elif lat_slice != None:
        data_slice  = data_interp.sel(lat=lat_slice, method='nearest')
    else: raise ValueError('lon_slice or lat_slice must be given')
    return data_slice
# %%
def compute_density(buoyancy):
    g    = 9.80665
    rho0 = 1025.022
    rho  = rho0 - buoyancy / g * rho0
    return rho
# %%
def load_wb_variables(tid):
    if tid == 'tides' or tid == 'notides':
        interval = '84H'
        tave     = '2019_07_T31H07_08_T07H05'
    elif tid == '':
        tave     = '2019_07_mean_T05-T12'
        interval = '7D'
    else:
        raise ValueError('strings not valid')

    path_wb = f'/work/bm1239/m300878/smt_data/tave/jul19_mean/{tave}/{tid}/'

    chunks  = {'depth': 1, 'time': 1}
    path    = f'{path_wb}wb/wb_{interval}_mean.nc'
    var     = xr.open_dataset(path, chunks=chunks)
    wb_mean = var.rename({'__xarray_dataarray_variable__': 'wb_mean'})

    path   = f'{path_wb}w/w_{interval}_mean.nc'
    w_mean = xr.open_dataset(path, chunks=chunks)

    path   = f'{path_wb}/b/b_{interval}_mean.nc'
    b_mean = xr.open_dataset(path, chunks=chunks)
    b_mean['depth'] = w_mean.depth

    return wb_mean, w_mean, b_mean, tave

def load_mld_mean(tid):
    beginn   = '2019-07-31-T07:15:00'
    end      = '2019-08-07-T05:15:00'
    if tid == 'tides':   ds_2d    = eva.load_smt_wave_all('2d', 7, remove_bnds=True, it=2)
    if tid == 'notides': ds_2d    = eva.load_smt_wave_9('2d', it=2)
    ds_mld   = ds_2d.mlotst.sel(time=slice(f'{beginn}', f'{end}'))
    mld_mean = ds_mld.mean(dim='time')
    return mld_mean
# %%
