# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp
import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 


# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
# %%
client, cluster = scluster.init_dask_slurm_cluster_gpu(walltime='01:00:00', wait=False)

# client, cluster = scluster.init_my_cluster()
cluster
client
# %%
reload(eva)
reload(smt_vort)
# %%
############## plot config ##############
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/high_res/'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'
lon_reg = np.array([-40, 30])
lat_reg = np.array([-55, -15])

# zoom1
lon_reg1 = np.array([-5, 10])
lat_reg1 = np.array([-42, -32])
lon_reg2 = np.array([-2, 0.5])
lat_reg2 = np.array([-38, -36.75])

savefig = False

# %%
############## load grid ##############
# %%
gg  = eva.load_smt_wave_grid()
res = np.sqrt(gg.cell_area_p)
res = res.rename(cell='ncells')
data_r = pyic.interp_to_rectgrid_xr(res, fpath_ckdtreeZ, lon_reg=lon_reg, lat_reg=lat_reg) # does not work properly with xarray...?
############## load temperature ##############
# %% instantan
ds        = eva.load_smt_wave_to_exp7_08(it=2)
sst       = ds.isel(time=0, depth=0).to
a         = sst.where(~(sst==0), np.nan)
mask_land = ~np.isnan(a)
mask_land = mask_land.where(mask_land==True, np.nan)
# %%
sst             = sst * mask_land
data_sst_coarse = pyic.interp_to_rectgrid_xr(sst, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_sst, data_reg_sst = eva.calc_triangulation_obj(sst, lon_reg2, lat_reg2)
data_sst              = pyic.interp_to_rectgrid_xr(sst, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
############## load tke ##############
ds_tke        = eva.load_smt_wave_tke_exp7_08(it=2)
tke        = ds_tke.isel(time=0, depth=15).tke
# %%
data_tke_coarse = pyic.interp_to_rectgrid_xr(tke, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_tke, data_reg_tke = eva.calc_triangulation_obj_wave(tke, lon_reg2, lat_reg2)
# data_tke               = pyic.interp_to_rectgrid_xr(tke, fpath_ckdtree, lon_reg=lon_reg1, lat_reg=lat_reg1)

############### load M2 ###############
# %%  instantan
path_data = '/work/bm1239/m300878/smt_data/buoyancy/b/pp_calc_b_2019-08-06T04:00:00.000000000.nc'

ds_m2 = xr.open_dataset(path_data, chunks=dict(depth=1))
ds    = ds_m2#.isel(depth=15)

m2 = np.abs(ds.dbdx*mask_land) + np.abs(ds.dbdy*mask_land)

b = pyic.interp_to_rectgrid_xr(ds.b, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_m2_coarse   = pyic.interp_to_rectgrid_xr(m2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_m2, data_regm = eva.calc_triangulation_obj_wave(m2, lon_reg2, lat_reg2)



# %%
######################## load mean data #############################
# mean
chunks  = {'depth': 1, 'time': 1}
path    = '/work/bm1239/m300878/smt_data/tave/jul19_mean/dbdx/dbdx_7D_mean.nc'
dbdx_mean = xr.open_dataset(path, chunks=chunks)
dbdx_mean = dbdx_mean.isel(time=0).dbdx

#load dbdy
path    = '/work/bm1239/m300878/smt_data/tave/jul19_mean/dbdy/dbdy_7D_mean.nc'
dbdy_mean = xr.open_dataset(path, chunks=chunks)
dbdy_mean = dbdy_mean.isel(time=0).dbdy

# load tke
path    = '/work/bm1239/m300878/smt_data/tave/jul19_mean/tke/tke_7D_mean.nc'
tke_mean = xr.open_dataset(path, chunks=chunks)
tke_mean = tke_mean.isel(time=0).tke

# %%
############### load wb_prime ###############
path    = '/work/bm1239/m300878/smt_data/tave/jul19_mean/wb/wb_7D_mean.nc'
var     = xr.open_dataset(path, chunks=chunks)
wb_mean = var.rename({'__xarray_dataarray_variable__': 'wb_mean'})

path   = '/work/bm1239/m300878/smt_data/tave/jul19_mean/b/b_7D_mean.nc'
b_mean = xr.open_dataset(path, chunks=chunks)

g     = 9.80665
rho0  = 1025.022
# b       = - g * (rhop - rho0)/rho0
rhop = rho0 - b_mean.b.isel(time=0) / g * rho0

path   = '/work/bm1239/m300878/smt_data/tave/jul19_mean/w/w_7D_mean.nc'
w_mean = xr.open_dataset(path, chunks=chunks)
# %%
wb_prime = wb_mean.wb_mean.isel(time=0) - w_mean.w.isel(time=0) * b_mean.b.isel(time=0)
wb_prime = wb_prime * mask_land
# %%

#######################################################################################################
#######################################################################################################
# %% classic 3 figure plot
reload(smt_temp)

fu.plot_temperature_zoom(data_sst, data_sst_coarse, Tri_sst, data_reg_sst, lat_reg_1, lon_reg_1, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, rlon, rlat, data_r, fig_path, savefig )

# %%
reload(smt_vort)
reload(fu)
reload(eva)

fu.plot_tke_zoom(data_tke_coarse, Tri_tke, data_reg_tke, lon_reg, lat_reg,  lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, fig_path, savefig)


# %%
reload(smt_vort)
reload(fu)
reload(eva)

fu.plot_m2_zoom2(data_m2_coarse, Tri_m2, data_regm, lon_reg, lat_reg,  lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, fig_path, savefig)


# %% general plot function
fname = 'na'
data  = data_sst
clim  = clim_sst
cmap  = 'plasma' #'RdPu_r'
fu.plot_na(data, lat_reg, lon_reg, clim, data_r.lon, data_r.lat, data_r, cmap, fig_path, fname)

# %% slice m2
m2        = np.abs(dbdx_mean) + np.abs(dbdy_mean)
m2_interp = pyic.interp_to_rectgrid_xr(m2, fpath_ckdtree, lon_reg=lon_reg2, lat_reg=lat_reg2)
slice_m2  = m2_interp.sel(lon=0, method='nearest')
# %% plot tke slice
tke = tke_mean
tke_interp = pyic.interp_to_rectgrid_xr(tke, fpath_ckdtree, lon_reg=lon_reg2, lat_reg=lat_reg2)
slice_tke = tke_interp.sel(lon=0, method='nearest')

# %% rhop
# %% plot tke slice
rho_interp = pyic.interp_to_rectgrid_xr(rhop, fpath_ckdtree, lon_reg=lon_reg2, lat_reg=lat_reg2)
slice_rho = rho_interp.sel(lon=0, method='nearest')
# %%
# slice = tke_interp.sel(lat=lat_reg2.mean(), method='nearest')

plt.figure()
plt.pcolormesh(slice_tke.lat, slice_tke.depth, slice_tke, cmap='plasma')
# color
plt.colorbar()
plt.ylim(300,0)
plt.xlabel('lat')
plt.ylabel('depth')
plt.savefig(f'{fig_path}tke_slice.png', dpi=200, bbox_inches='tight')

# %%
plt.figure()
tke_interp.isel(depth=15).plot(cmap='plasma')
# plot vertical line
plt.vlines(0, lat_reg2.min(), lat_reg2.max(), color='r', linewidth=3)
# plt.plot([lat_reg2[0], lat_reg2[1]], [300,300], color='r', linewidth=3)
plt.savefig(f'{fig_path}tke_map2.png', dpi=200, bbox_inches='tight')

# %%
wb_prime        = wb_mean.wb_mean.isel(time=0) - w_mean.w.isel(time=0) * b_mean.b.isel(time=0)
wb_prime        = wb_prime * mask_land
wb_prime_interp = pyic.interp_to_rectgrid_xr(wb_prime, fpath_ckdtree, lon_reg=lon_reg2, lat_reg=lat_reg2)
slice_wb_prime  = wb_prime_interp.sel(lon=0, method='nearest')
# %%
plt.figure()
plt.pcolormesh(slice_wb_prime.lat, slice_wb_prime.depth, slice_wb_prime, cmap='RdBu_r', vmin=-2e-7, vmax=2e-7)
# color
plt.colorbar()
plt.ylim(300,0)
plt.xlabel('lat')
plt.ylabel('depth')
plt.savefig(f'{fig_path}wb_prime_slice.png', dpi=200, bbox_inches='tight')

# %% load mld
beginn  = '2019-08-01-T00:00:00'
end     = '2019-08-07-T23:15:00'
ds_2d = eva.load_smt_wave_all('2d', 7, remove_bnds=True, it=2)
ds_mld = ds_2d.mlotst.sel(time=slice(f'{beginn}', f'{end}'))
mld_mean = ds_mld.mean(dim='time')

# %%
data_mld_coarse = pyic.interp_to_rectgrid_xr(mld_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_mld, data_reg_mld = eva.calc_triangulation_obj_wave(mld_mean, lon_reg2, lat_reg2)

# %%
reload(smt_vort)
reload(fu)
reload(eva)
fu.plot_MLD_zoom(data_mld_coarse, Tri_mld, data_reg_mld, lon_reg, lat_reg,  lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, fig_path, savefig)

# %% mld slice
mld_interp = pyic.interp_to_rectgrid_xr(mld_mean, fpath_ckdtree, lon_reg=lon_reg2, lat_reg=lat_reg2)
slice_mld  = mld_interp.sel(lon=0, method='nearest')
# %%
eva.plot_quick_wave(mld_mean, clim=np.array([0, 200]), cmap='viridis')
plt.savefig(f'{fig_path}mld_mean.png', dpi=200, bbox_inches='tight')
# %%
reload(fu)
x        = slice_tke.lat
y        = slice_tke.depth

fu.plot_slice_front(x, y, slice_m2, slice_wb_prime, slice_tke, slice_rho, slice_mld,  ylim=np.array([150,0]), fig_path=fig_path, savefig=savefig)


# %% mean

tke        = tke_mean.isel( depth=15)
# %%
data_tke_coarse = pyic.interp_to_rectgrid_xr(tke, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_tke, data_reg_tke = eva.calc_triangulation_obj_wave(tke, lon_reg2, lat_reg2)

############### load M2 ###############
# %%  m2
m2_mean   = np.abs(dbdx_mean) + np.abs(dbdy_mean)
m2_mean   = m2_mean.isel( depth=15)
# %%
data_m2_coarse   = pyic.interp_to_rectgrid_xr(m2_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_m2, data_reg_m2 = eva.calc_triangulation_obj_wave(m2_mean, lon_reg2, lat_reg2)
# %%
wb_prime_ = wb_prime.isel(depth=15)
data_wb_prime_coarse = pyic.interp_to_rectgrid_xr(wb_prime_, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_wb_prime, data_reg_wb_prime = eva.calc_triangulation_obj_wave(wb_prime_, lon_reg2, lat_reg2)

# %%
# %%
reload(smt_vort)
reload(fu)
reload(eva)

fu.plot_wb_prime_zoom(data_wb_prime_coarse, Tri_wb_prime, data_reg_wb_prime, lon_reg, lat_reg,  lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, fig_path, savefig)

# %%
reload(smt_vort)
reload(fu)
reload(eva)
fu.plot_m2_zoom2(data_m2_coarse, Tri_m2, data_reg_m2, lon_reg, lat_reg,  lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, fig_path, savefig)

# %%
# %%
reload(smt_vort)
reload(fu)
reload(eva)

fu.plot_tke_zoom(data_tke_coarse, Tri_tke, data_reg_tke, lon_reg, lat_reg,  lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, fig_path, savefig)

# %%
ds = eva.load_smt_wave_all('w', 7, remove_bnds=True, it=10)
gg = gg.rename(cell='ncells')
# %%
lon_reg = -2, 20
lat_reg = -38, -28

lon_reg = -2, 20
lat_reg = -42, -32

w = ds.w.isel(time=60, depth=96)
var = w
var.attrs["long_name"] = ""
var.attrs["units"] = "" 
# %%
var = var.drop('clon').drop('clat')
# add clon clat
# rename cell to ncells
clon = gg.clon
clat = gg.clat
var = var.assign_coords(clon=clon, clat=clat)

# %%
reload(tools)
fig, ax, artist = tools.plot_ds_with_cartopy_w(var, lon_range=lon_reg, lat_range=lat_reg, vmin=-5e-3, vmax=5e-3)
# plot vertical line from lon=0 and from lat=lat_reg2[0] to lat=lat_reg2[1]

ax.axvline(0, color='black', linestyle='--', linewidth=2)
if savefig == True: plt.savefig(f'{fig_path}_w_s_{var.depth.values:.2f}m.png', dpi=250, format='png', bbox_inches='tight')

# %% section
var = ds.w.isel(time=60)
var = var.drop('clon').drop('clat')
var_interp_ = pyic.interp_to_rectgrid_xr(var, fpath_ckdtree, lon_reg=lon_reg2, lat_reg=lat_reg1)
var_interp = var_interp_.sel(lon=0, method='nearest')
# %%
def plot_w_slice(data):
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=0.65, fig_size_fac=2, sharex=True, sharey=False, daxt=0.9, axlab_kw=None) # type: ignore
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 5e-3 

    hm = pyic.shade(data.lat, data.depth, data, ax=ax, cax=cax, clim=clim, cmap='coolwarm', extend='both')
    ax.set_title(r'$w  ~ [m/s]$ at lon=4')
    hm[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm[1].formatter.set_useMathText(True)

    ax.set_ylim(5000,0)
    ax.set_xlabel('lat')
    ax.set_ylabel('depth [m]')
    if savefig == True: plt.savefig(fig_path+'slice_lon3.png', dpi=250, format='png', bbox_inches='tight')

# %%
plot_w_slice(var_interp)
# %%
p1 = 13, -38
p2 = 6, -34

lon_sec, lat_sec, dist_sec = pyic.derive_section_points(p1, p2, npoints=200)

lon_min_max = np.array([p2[0], p1[0]])
lat_min_max = np.array([p1[1], p2[1]])

var_interp_ = pyic.interp_to_rectgrid_xr(var, fpath_ckdtree, lon_reg=lon_min_max, lat_reg=lat_min_max)

# %%
# find nearest neighbors along section
var_section = var_interp_.sel(lon=lon_sec, lat=lat_sec, method='nearest')
# %%
# interpolate along section
var_section = var_interp_.isel(depth=100).interp(lon=lon_sec, lat=lat_sec)
# reduce to 2d field


