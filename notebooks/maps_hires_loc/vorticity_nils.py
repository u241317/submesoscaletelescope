# %%
import sys
import numpy as np
import matplotlib 
# --- avoid errors with display when executed by sbatch
if len(sys.argv)>1 and sys.argv[1]=='--no_backend':
  print('apply: matplotlib.use(\'Agg\')')
  matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pyicon as pyic
import cartopy.crs as ccrs
import glob, os
import xarray as xr
# import maps_icon_smtwv_zoom as smt
from matplotlib import patches
from matplotlib.patches import Rectangle
from netCDF4 import Dataset

#run = 'ts3_smt_wave'
#path_data = f'/mnt/lustre02/work/bm1102/m300761/Projects/smtWave/icon-oes/experiments/{run}/'
#run = 'smtwv0002'
#path_data = f'/mnt/lustre02/work/bm1102/m300602/proj_smtwv/icon-oes-smt_zstar/experiments/{run}/'
#run = 'smtwv0003'
#path_data = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{run}/'
#run = 'smtwv0004'
#run = 'smtwv0008'
#run = 'smtwv0009'
# run = sys.argv[2]
run = 'smtwv0007'
path_data = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{run}/outdata_2d/'
snap_freq = 1

#gname = 'smtwv_oce_2018'
gname = 'smtwv_oce_2022'
path_grid = f'/work/mh0033/m300602/icon/grids/{gname}/'
fpath_ckdtree    = f'{path_grid}/ckdtree/rectgrids/{gname}_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtree_03 = f'{path_grid}/ckdtree/rectgrids/{gname}_res0.30_180W-180E_90S-90N.nc'
fpath_tgrid      = f'{path_grid}/{gname}_tgrid.nc'

# --- initialize timer
timi = pyic.timing([0], 'start')




# %%
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
cluster
client



# %%
# --- set path where figures are storred 
savefig = True
path_fig = '../movies/%s_%s/' % (__file__.split('/')[-1][:-3], run)
figure_prefix = __file__.split('/')[-1][:-3]
nnf=0

try:
  os.makedirs(path_fig)
except:
  pass

# --- topography
timi = pyic.timing(timi, 'topography')
path_data_topo = '/sw/rhel6-x64/ferret-6.9.3/dsets/data/'

#if True:
#  fname = 'etopo40.cdf'
#  f = Dataset(path_data_topo+fname, 'r')
#  lont = f.variables['ETOPO40X'][:]
#  latt = f.variables['ETOPO40Y'][:]
#  topo = f.variables['ROSE'][:]
#  f.close()
#else:
#  fname = 'etopo20.cdf'
#  f = Dataset(path_data_topo+fname, 'r')
#  lont = f.variables['ETOPO20X1_1081'][:]
#  latt = f.variables['ETOPO20Y'][:]
#  topo = f.variables['ROSE'][:]
#  f.close()
#
#def shiftgrid(lont, latt, topo):
#  iw =np.where(lont>180.)[0][0]
#  lont = np.concatenate((lont[iw:], lont[:iw]))
#  lont[lont>180.] += -360.
#  topo = np.concatenate((topo[:,iw:], topo[:,:iw]), axis=1)
#  topo = np.ma.array(topo, mask=topo>0)
#  
#  topo *=-1
#  return lont, latt, topo
#
#lont, latt, topo = shiftgrid(lont, latt, topo)

# --- open files with xarray
#search_str = f'{run}_2D_????????????????.nc'
#search_str = f'{run}_oce_2d_PT2H_????????????????.nc'
search_str = f'{run}_oce_2d_PT1H_????????????????.nc'
print(f'Looking for these files: {path_data+search_str}')
flist = glob.glob(path_data+search_str)
flist.sort()

#f1st = flist[0:1]
#flist = flist[1:-1]
#flist = [:-1]
#times, flist_ts, its = pyic.get_timesteps(flist, time_mode='num2date')

mfdset_kwargs = dict(
  combine='nested', concat_dim='time', 
  data_vars='minimal', coords='minimal', 
  compat='override', join='override',
  #parallel=True, # this causes an error for smtwv0009
)

timi = pyic.timing(timi, 'open_mfdataset')
ds = xr.open_mfdataset(flist, **mfdset_kwargs, chunks={'time': 1})
#ds = ds.rename({'ncells': 'asdf'})
#ds = ds.rename({'ncells_2': 'ncells'})
#ds = ds.rename({'asdf': 'ncells_2'})

#ds = ds.isel(depth=slice(0,1))
#ds1st = xr.open_mfdataset(f1st, **mfdset_kwargs, chunks={'time':1})
#ds = xr.concat([ds1st, ds], dim='time')

# --- tripolar grid
ds_tg = xr.open_dataset(fpath_tgrid)

# --- resolution
#timi = pyic.timing(timi, 'load res')
#cell_areai = pyic.interp_to_rectgrid_xr(ds_tg.cell_area.rename(cell='ncells'), 
#  fpath_ckdtree_03)
#resi = np.sqrt(cell_areai)

# %%
# --- Coriolis parameter
timi = pyic.timing(timi, 'fcor')
fcor = 2. * 2.*np.pi/86400.*np.sin(ds_tg.vlat.rename(vertex='ncells'))
fcor = fcor.compute()

# --- specify regions
#lon_reg_1 = [-48, 40]
#lat_reg_1 = [ -80, -17]
##lon_reg_2 = [5, 25]
##lat_reg_2 = [-45, -35]
#lon_reg_2 = [-12, 10]
#lat_reg_2 = [-40, -29]
##lon_reg_3 = [10, 14]
##lat_reg_3 = [-40, -38]
#lon_reg_3 = [6, 10]
#lat_reg_3 = [-32, -30]

#lon_reg_1 = [-26, 40]
#lat_reg_1 = [ -64.25, -17]
lon_reg_1 = [-14, 34]
lat_reg_1 = [-43, -11]
lon_reg_2 = [0, 12]
lat_reg_2 = [-35, -29]
lon_reg_3 = [5, 9]
lat_reg_3 = [-32, -30]
use_interp = False
#use_interp = True

# --- derive Triangulation object
if not use_interp:
  timi = pyic.timing(timi, 'derive triangulation')
  ##ind_reg, Tri = pyic.triangulation(ds_tg, lon_reg=lon_reg_3, lat_reg=lat_reg_3)
  #ireg_v = (  (ds_tg.vlon>lon_reg_3[0]) 
  #          & (ds_tg.vlon<=lon_reg_3[1]) 
  #          & (ds_tg.vlat>lat_reg_3[0]) 
  #          & (ds_tg.vlat<=lat_reg_3[1])
  #         )
  #ireg_c = (  (ds_tg.clon>lon_reg_3[0]) 
  #          & (ds_tg.clon<=lon_reg_3[1]) 
  #          & (ds_tg.clat>lat_reg_3[0]) 
  #          & (ds_tg.clat<=lat_reg_3[1])
  #         )
  #clon_bnds, clat_bnds, vlon_bnds, vlat_bnds, cells_of_vertex = pyic.patch_plot_derive_bnds(ds_tg)
  #vlon_bnds_reg = vlon_bnds[ireg_v]
  #vlat_bnds_reg = vlat_bnds[ireg_v]
  #cells_of_vertex_reg = cells_of_vertex[ireg_v]
  #clon_bnds_reg = clon_bnds[ireg_c]
  #clat_bnds_reg = clat_bnds[ireg_c]
  #patches_c, patches_v = pyic.patch_plot_patches_from_bnds(clon_bnds_reg, clat_bnds_reg, vlon_bnds_reg, vlat_bnds_reg, cells_of_vertex_reg)
  clon = ds_tg.clon.compute().data * 180./np.pi
  clat = ds_tg.clat.compute().data * 180./np.pi
  ireg_c = np.where(
      (clon>lon_reg_3[0]) & (clon<=lon_reg_3[1]) & (clat>lat_reg_3[0]) & (clat<=lat_reg_3[1])
  )[0]
  ds_tg_cut = pyic.xr_crop_tgrid(ds_tg, ireg_c)
  ireg_v = ds_tg_cut['ireg_v'].data
  
  clon_bnds, clat_bnds, vlon_bnds, vlat_bnds, cells_of_vertex = pyic.patch_plot_derive_bnds(ds_tg_cut)
  patches_c, patches_v = pyic.patch_plot_patches_from_bnds(
      clon_bnds.compute(), clat_bnds.compute(), 
      vlon_bnds.compute(), vlat_bnds.compute(), 
      cells_of_vertex#.compute()
  )

# --- load data
timi = pyic.timing(timi, 'load data')
step = 1 # dummy step

data_xr = ds['vort'].isel(depth=0, time=step).rename(ncells_2='ncells')
data_xr *= 1./(fcor+1e-33)

# --- interpolate to regular grid
timi = pyic.timing(timi, 'interp_to_rectgrid_xr')
data_xr = data_xr.compute()
data_xr = data_xr.where(data_xr!=0)
data_xr_interp_1 = pyic.interp_to_rectgrid_xr(data_xr, fpath_ckdtree, 
  lon_reg=lon_reg_1, lat_reg=lat_reg_1, coordinates='vlat vlon')
data_xr_interp_2 = pyic.interp_to_rectgrid_xr(data_xr, fpath_ckdtree, 
  lon_reg=lon_reg_2, lat_reg=lat_reg_2, coordinates='vlat vlon')
if use_interp:
  data_xr_interp_3 = pyic.interp_to_rectgrid_xr(data_xr, fpath_ckdtree, 
    lon_reg=lon_reg_3, lat_reg=lat_reg_3, coordinates='vlat vlon')
else:
  data_xr_interp_3 = data_xr[ireg_v]

# --- specify which steps should be plotted
steps = np.arange(ds.time.size)

# -------------------------------------------------------------------------------- 
# Here starts plotting
# -------------------------------------------------------------------------------- 

timi = pyic.timing(timi, 'initialize plot')

plt.close('all')
ccrs_proj = ccrs.PlateCarree()

#hca, hcb = pyic.arrange_axes(1,1, plot_cb=True, fig_size_fac=2., asp=0.5, projection=ccrs_proj)
dpi = 300
clim = 1.
fig, hca, cax = smt.make_axes(lon_reg_1, lat_reg_1, lon_reg_2, lat_reg_2, lon_reg_3, lat_reg_3, dpi)

ax=hca[0]; cax=cax
hm1 = pyic.shade(data_xr_interp_1.lon, data_xr_interp_1.lat, data_xr_interp_1.data, 
                 ax=ax, cax=cax, clim=clim)
ax=hca[1]; cax=cax
hm2 = pyic.shade(data_xr_interp_2.lon, data_xr_interp_2.lat, data_xr_interp_2.data, 
                 ax=ax, cax=cax, clim=clim)
ax=hca[2]; cax=cax
if use_interp:
  hm3 = pyic.shade(data_xr_interp_3.lon, data_xr_interp_3.lat, data_xr_interp_3.data, 
                   ax=ax, cax=cax, clim=clim)
else:
  hm3 = pyic.patch_plot_shade(patches_v, data_xr_interp_3, ax=ax, cax=cax, clim=clim)
  hm3 = (hm3,)

# --- colorbar
cax.set_ylabel("rel. vort. / plan. vort.")
x0, y0 = cax.get_position().x0, cax.get_position().y0
width, height = cax.get_position().width, cax.get_position().height
cax.set_position([0.45, 0.65, width, height])
cax.set_facecolor('0.9')

# --- 1st axes settings
ax=hca[0]; cax=cax

ax.set_xticks(np.arange(-170,170,10), crs=ccrs_proj)
ax.set_yticks(np.arange(-80,80,10), crs=ccrs_proj)
ax.set_xlim(lon_reg_1)
ax.set_ylim(lat_reg_1)

#rectprop = dict(facecolor='none', edgecolor='k', lw=2)
#ax.add_patch(Rectangle((-12,-38), 22, 7, **rectprop))
#ax.text(-1, -31., 'SONETT', ha='center', va='bottom', fontsize=8)

rectprop = dict(facecolor='none', edgecolor='0.7', lw=2)
ax.add_patch(Rectangle((-12,-37.5), 5, 4, **rectprop))
ax.text(-9.5, -33.5, 'SONETT II', ha='center', va='bottom', fontsize=8)

rectprop = dict(facecolor='none', edgecolor='0.7', lw=2)
ax.add_patch(Rectangle((1,-35), 9, 5, **rectprop))
ax.text(5.5, -30., 'SONETT I', ha='center', va='bottom', fontsize=8)

ax.text(-2.5, -35, 'Walvis Ridge', va='center', ha='center', rotation=45, fontsize=8)

#ax.contour(lont, latt, topo, np.arange(0,6000.,1000.), colors='0.7', linewidths=1.0, zorder=1)

# --- time string (also updated)
bbox=dict(facecolor='w', alpha=1., edgecolor='none')
ht = ax.text(0.02, 0.96, str(data_xr.time.data)[:16], transform=ax.transAxes, bbox=bbox)

def update_fig(step):
  # --- load new data
  data_xr = ds['vort'].isel(depth=0, time=step).rename(ncells_2='ncells')
  data_xr *= 1./(fcor+1e-33)

  # --- interp to rectgrid 
  data_xr = data_xr.compute()
  data_xr = data_xr.where(data_xr!=0)
  data_xr_interp_1 = pyic.interp_to_rectgrid_xr(data_xr, fpath_ckdtree, 
    lon_reg=lon_reg_1, lat_reg=lat_reg_1, coordinates='vlat vlon')
  data_xr_interp_2 = pyic.interp_to_rectgrid_xr(data_xr, fpath_ckdtree, 
    lon_reg=lon_reg_2, lat_reg=lat_reg_2, coordinates='vlat vlon')
  if use_interp:
    data_xr_interp_3 = pyic.interp_to_rectgrid_xr(data_xr, fpath_ckdtree, 
      lon_reg=lon_reg_3, lat_reg=lat_reg_3, coordinates='vlat vlon')
  else:
    data_xr_interp_3 = data_xr[ireg_v]

  # --- update plot
  hm1[0].set_array(data_xr_interp_1.data.flatten())
  hm2[0].set_array(data_xr_interp_2.data.flatten())
  hm3[0].set_array(data_xr_interp_3.data.flatten())
  ht.set_text(str(data_xr.time.data)[:16])
  return
 
# --- (optional) verify whether plot looks like you want it to look
#update_fig(step)
if use_interp:
  plt.show()
  sys.exit()

# --- parallel execution of script (usually no need to change anything below)
# >>> start parallel (comment out if not wanted) <<<
try:
  # --- load mpi if possible and define `rank`=processor ID and `npro`=number of total processors
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

# --- modify previously defined variable `steps` such that each processor gets
#     its own `steps` variable containing only the steps which it should work on 
list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]
# >>> end parallel (comment out if not wanted) <<<

# --- start loop
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
  timi = pyic.timing(timi, 'loop step')

  update_fig(step)

  nnf+=1
  if savefig:
    fpath = '%s%s_%04d.jpg' % (path_fig,figure_prefix, step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=250)

timi = pyic.timing(timi, 'all done')

