# %%
import sys
import glob, os
import pyicon as pyic
import sys
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
# import funcs1 as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# %% Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
# from validate_catalog import all_params
# params_dict, cat = all_params()
# params_dict.keys()
import gcm_filters

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, dash_address='8787')
cluster
client

# %%
fig_path = '/work/mh0033/m300878/videos/vorticity/'
savefig  = False 

#%%
t0 = '20190901T011500Z'
t1 = '20200501T011500Z'
exp=7
path = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv000{exp}/outdata_2d/smtwv000{exp}_oce_2d_PT1H_{t0}.nc'
ds = xr.open_dataset(path)
ds_exp7_sep = ds.vort.isel(depth=1, time=0).compute()

path = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv000{exp}/outdata_2d/smtwv000{exp}_oce_2d_PT1H_{t1}nc'
ds = xr.open_dataset(path)
ds_exp7_may = ds.vort.isel(depth=1, time=0).compute()

exp=9
path = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv000{exp}/outdata_2d/smtwv000{exp}_oce_2d_PT1H_{t0}.nc'
ds = xr.open_dataset(path)
ds_exp9_sep = ds.vort.isel(depth=1, time=0).compute()

path = f'/work/mh0033/m300878/run_icon/icon-oes-zstar-r2b9/experiments/exp.ocean_era51h_zstar_r2b9_22255-ERA/outdata_2d_lev_1/exp.ocean_era51h_zstar_r2b9_22255-ERA_oce_2d_lev_1_PT1H_{t0}.nc'
ds = xr.open_dataset(path)
ds_r2b9 = ds.vort.isel(depth=2, time=0).compute()


#%%
fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
lon_reg= [-5,10]
lat_reg= [-40,-30]
d70 = pyic.interp_to_rectgrid_xr(ds_exp7_sep, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg, coordinates='vlat vlon')
# %%
d71 = pyic.interp_to_rectgrid_xr(ds_exp7_may, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg, coordinates='vlat vlon')
d90 = pyic.interp_to_rectgrid_xr(ds_exp9_sep, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg, coordinates='vlat vlon')

fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b9_oce_r0004/ckdtree/rectgrids/r2b9_oce_r0004_res0.02_180W-180E_90S-90N.nc'
dr2b90 = pyic.interp_to_rectgrid_xr(ds_r2b9, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg, coordinates='vlat vlon')
# %%
f = eva.calc_coriolis_parameter(d70.lat)
fr2b9 = eva.calc_coriolis_parameter(dr2b90.lat)

V = [d70/f, d71/f, d90/f, dr2b90/fr2b9]
# %%
def plot_vorticity(d0, d1, d2, d3, asp, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2, 2,  asp=asp, plot_cb='bottom', fig_size_fac=2, projection=ccrs_proj, sharex=True, sharey=True)
    clim = 1/2
    cmap = 'RdBu_r'

    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT-Wave 2019-09-01')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(d1.lon, d1.lat, d1, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT-Wave 2020-05-01')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(d2.lon, d2.lat, d2, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT 2019-09-01')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(d3.lon, d3.lat, d3, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-R2B9-Wave 2019-09-01')

    #colorbar title
    hm1[1].set_label(r'$\zeta/f$', fontsize=12, labelpad=-50)
    
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
        # ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    fname = f'0m_scaled'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250,format='png', bbox_inches='tight')

asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

plot_vorticity(d70/f, d71/f, d90/f, dr2b90/fr2b9, asp, fig_path, savefig)
# %%
lon_reg= [0,7]
lat_reg= [-40,-30]

lon_reg1= [0,7]
lat_reg1= [-40,-30]
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
def plot_vorticity(d0, d1, asp, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2, 1,  asp=asp, plot_cb='bottom', fig_size_fac=2, projection=ccrs_proj, sharex=True, sharey=True, daxr=0.03)
    clim = 1
    cmap = 'RdBu_r'
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    ax.set_title('ICON-SMT-Wave')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(d1.lon, d1.lat, d1, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    pyic.plot_settings(ax, xlim=lon_reg1, ylim=lat_reg1) # type: ignore
    ax.set_title('ICON-R2B9-Wave')
    hm1[1].set_label(r'$\zeta/f$', fontsize=12, labelpad=-47)
    for ax in hca:
        ax.yaxis.set_major_locator(plt.MaxNLocator(3)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(3)) # type: ignore
    fname = f'xx'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250,format='png', bbox_inches='tight')

plot_vorticity(d70/f, dr2b90/fr2b9, asp, fig_path, savefig)
# %%
# lon_reg= [0,7]
# lat_reg= [-40,-30]

# lon_reg1= [0,7]
# lat_reg1= [-40,-30]
# asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
# def plot_vorticity(d0, d1, asp, fig_path, savefig):
#     hca, hcb = pyic.arrange_axes(2, 1,  asp=asp, plot_cb='bottom', fig_size_fac=2, projection=ccrs_proj, sharex=True, sharey=True, daxr=0.03)
#     clim = 1
#     cmap = 'RdBu_r'
#     ii=-1
#     ii+=1; ax=hca[ii]; cax=hcb[ii]
#     hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim, cmap=cmap,
#                     transform=ccrs_proj, rasterized=False)
#     pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
#     ax.set_title('ICON-SMT-Wave')
    
#     ii+=1; ax=hca[ii]; cax=hcb[ii]
#     hm1 = pyic.shade(d1.lon, d1.lat, d1, ax=ax,cax=cax, clim=clim, cmap=cmap,
#                     transform=ccrs_proj, rasterized=False)
#     pyic.plot_settings(ax, xlim=lon_reg1, ylim=lat_reg1) # type: ignore
#     ax.set_title('ICON-SMT')
#     hm1[1].set_label(r'$\zeta/f$', fontsize=12, labelpad=-47)
#     for ax in hca:
#         ax.yaxis.set_major_locator(plt.MaxNLocator(3)) # type: ignore
#         ax.xaxis.set_major_locator(plt.MaxNLocator(3)) # type: ignore
#     fname = f'xx'
#     if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250,format='png', bbox_inches='tight')

# plot_vorticity(d70/f, d90/f, asp, fig_path, savefig)
# %%


#%% vertical velocity
t0 = '20190901T011500Z'
t1 = '20200501T011500Z'


exp=7
# path = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv000{exp}/outdata_w/smtwv000{exp}_oce_3d_w_PT1H_{t0}.nc'
path = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv000{exp}/outdata_lev/smtwv000{exp}_oce_2d_lev_PT1H_{t0}.nc'
ds = xr.open_dataset(path)
# ds_exp7_sep = ds.w.isel(depth=76, time=0).compute()
ds_exp7_sep = ds.w.isel(depth_2=3, time=0).compute()


exp=7
path = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv000{exp}/outdata_lev/smtwv000{exp}_oce_2d_lev_PT1H_{t1}.nc'
ds = xr.open_dataset(path)
ds_exp7_may = ds.w.isel(depth_2=3, time=0).compute()

exp=9
# path = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv000{exp}/outdata_w/smtwv000{exp}_oce_3d_w_PT1H_{t0}.nc'
path = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv000{exp}/outdata_lev/smtwv000{exp}_oce_2d_lev_PT1H_{t0}.nc'
ds = xr.open_dataset(path)
# ds_exp9_sep = ds.w.isel(depth=76, time=0).compute()
ds_exp9_sep = ds.w.isel(depth_2=3, time=0).compute()

path = f'/work/mh0033/m300878/run_icon/icon-oes-zstar-r2b9/experiments/exp.ocean_era51h_zstar_r2b9_22255-ERA/outdata_2d_lev_0/exp.ocean_era51h_zstar_r2b9_22255-ERA_oce_2d_lev_0_PT1H_{t0}.nc'
ds = xr.open_dataset(path)
ds_r2b9 = ds.w.isel(depth_2=10, time=0).compute()

# %%
fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
lon_reg= [-5,10]
lat_reg= [-40,-30]
d70 = pyic.interp_to_rectgrid_xr(ds_exp7_sep, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
d71 = pyic.interp_to_rectgrid_xr(ds_exp7_may, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
d90 = pyic.interp_to_rectgrid_xr(ds_exp9_sep, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b9_oce_r0004/ckdtree/rectgrids/r2b9_oce_r0004_res0.02_180W-180E_90S-90N.nc'
dr2b90 = pyic.interp_to_rectgrid_xr(ds_r2b9, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
def plot_w(d0, d1, d2, d3, asp, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2, 2,  asp=asp, plot_cb='bottom', fig_size_fac=2, projection=ccrs_proj, sharex=True, sharey=True)
    clim = 2e-3
    cmap = 'Greys'

    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT-Wave 2019-09-01')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(d1.lon, d1.lat, d1, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT-Wave 2020-05-01')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(d2.lon, d2.lat, d2, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT 2019-09-01')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(d3.lon, d3.lat, d3, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-R2B9-Wave 2019-09-01')

    #colorbar title
    hm1[1].set_label(r'w[m/s]', fontsize=12, labelpad=-50)
    
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
        # ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    fname = f'w_1000m_cb'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250,format='png', bbox_inches='tight')

asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

plot_w(d70, d71, d90, dr2b90, asp, fig_path, savefig)
# %%
W = [d70, d71, d90, dr2b90]
# %%




def plot_all(V, W, asp, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2, 4,  asp=asp, plot_cb=False, fig_size_fac=2, projection=ccrs_proj, sharex=True, sharey=True) #[0,0,0,0,0,0,1,1]

    ii=-1
    clim_v = 1
    cmap_v = 'RdBu_r'
    clim_w = 2e-3
    cmap_w = 'Greys'

    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    d0 = V[0]
    hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim_v, cmap=cmap_v,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT-Wave 2019-09-01')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    d0 = W[0]
    hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim_w, cmap=cmap_w,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT-Wave 2019-09-01')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    d0 = V[1]
    hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim_v, cmap=cmap_v,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT-Wave 2020-05-01')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    d0 = W[1]
    hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim_w, cmap=cmap_w,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT-Wave 2020-05-01')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    d0 = V[2]
    hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim_v, cmap=cmap_v,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT 2019-09-01')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    d0 = W[2]
    hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim_w, cmap=cmap_w,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-SMT 2019-09-01')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    d0 = V[3]
    hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim_v, cmap=cmap_v,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-R2B9-Wave 2019-09-01')
    # hm1[1].set_label(r'$\zeta/f$', fontsize=12, labelpad=-50)
    #plot colorbar below
    
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    d0 = W[3]
    hm1 = pyic.shade(d0.lon, d0.lat, d0, ax=ax,cax=cax, clim=clim_w, cmap=cmap_w,
                    transform=ccrs_proj, rasterized=False)
    ax.set_title('ICON-R2B9-Wave 2019-09-01')
    
    # hm1[1].set_label(r'w[m/s]', fontsize=12, labelpad=-50)
    
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
        # ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    fname = f'vort_w'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250,format='png', bbox_inches='tight')

asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

plot_all(V, W, asp, fig_path, savefig)
# %%
