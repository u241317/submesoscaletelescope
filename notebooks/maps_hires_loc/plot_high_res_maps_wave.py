# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp
import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster_gpu(walltime='01:00:00', wait=False)
cluster
client
# %%
reload(eva)
reload(smt_vort)
savefig = False
# %%
############## plot config ##############
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/high_res/sa/'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'
lon_reg = np.array([-40, 30])
lat_reg = np.array([-55, -15])

# zoom1
lon_reg1 = np.array([-5, 10])
lat_reg1 = np.array([-42, -32])
lon_reg2 = np.array([-2, 0.5])
lat_reg2 = np.array([-38, -36.75])

clim_vort = 1
clim_sst  = 2, 26# close 14.5,21.7
clim_m2   = 0, 2e-7

savefig = False

# %%
############## load grid ##############
# %%
gg     = eva.load_smt_wave_grid()
res    = np.sqrt(gg.cell_area_p)
res    = res.rename(cell='ncells')
data_r = pyic.interp_to_rectgrid_xr(res, fpath_ckdtreeZ, lon_reg=lon_reg, lat_reg=lat_reg) # does not work properly with xarray...?
############## load temperature ##############
# %% instantan
ds        = eva.load_smt_wave_to_exp7_08(it=2)
sst       = ds.isel(time=0, depth=0).to
# %%
# sst_mask  = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_to/smtwv0007_oce_3d_to_PT1H_20190831T011500Z.nc')
# a         = sst_mask.to.where(~(sst_mask==0), np.nan)
# mask_land = ~np.isnan(a)
# mask_land = mask_land.where(mask_land==True, np.nan)

reload(eva)
mask_land = eva.load_smt_wave_land_mask()
# %%
sst             = sst * mask_land
data_sst_coarse = pyic.interp_to_rectgrid_xr(sst, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_sst, data_reg_sst = eva.calc_triangulation_obj(sst, lon_reg2, lat_reg2)
data_sst              = pyic.interp_to_rectgrid_xr(sst, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
############## load tke ##############
ds_tke          = eva.load_smt_wave_tke_exp7_08(it=2)
tke             = ds_tke.isel(time=0, depth=15).tke
data_tke_coarse = pyic.interp_to_rectgrid_xr(tke, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_tke, data_reg_tke   = eva.calc_triangulation_obj_wave(tke, lon_reg2, lat_reg2)
# data_tke               = pyic.interp_to_rectgrid_xr(tke, fpath_ckdtree, lon_reg=lon_reg1, lat_reg=lat_reg1)

# %%
############### load vorticity #############
ds_2d = eva.load_smt_wave_2d_exp7_07(it=2)
ds_2d = ds_2d.drop('clon').drop('clat')
# %%
vort             = ds_2d.isel(time=100).isel(depth=1).vort
# %%
# vort = ds_2d.vort.isel(depth=1).sel(time='2019-08-18T21:15:00')

# %%
ds_0 = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190704T001500Z.nc', chunks=dict(depth=1))
# vort = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190826T221500Z.nc', chunks=dict(depth=1))
vort = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190818T211500Z.nc', chunks=dict(depth=1))

vort = vort.vort.isel(depth=1).isel(time=0)
lat              = np.rad2deg(ds_0.vlat)
f                = eva.calc_coriolis_parameter(lat)
fcor = 2. * 2.*np.pi/86400.*np.sin(ds_0.vlat)
fcor = fcor.compute()
Ro               = vort / f
# %%
data_vort_coarse = pyic.interp_to_rectgrid_xr(Ro, fpath_ckdtree, coordinates='vlat vlon', lon_reg=lon_reg, lat_reg=lat_reg)

# %%
data_mask_coarse = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
data_vort_coarse = data_vort_coarse * data_mask_coarse
# %%
reload(eva)
Ro = Ro.compute()
patches_v, data_native_interp = eva.calc_triangulation_obj_wave_vertices(Ro, gg, lon_reg2, lat_reg2)

############### load M2 ###############
# %%  instantan
path_data = '/work/bm1239/m300878/smt_data/buoyancy/b/pp_calc_b_2019-08-06T04:00:00.000000000.nc'

ds_m2 = xr.open_dataset(path_data, chunks=dict(depth=1))
ds    = ds_m2#.isel(depth=15)

m2 = np.abs(ds.dbdx*mask_land) + np.abs(ds.dbdy*mask_land)

b = pyic.interp_to_rectgrid_xr(ds.b, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_m2_coarse   = pyic.interp_to_rectgrid_xr(m2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_m2, data_regm = eva.calc_triangulation_obj_wave(m2, lon_reg2, lat_reg2)


# %%
############### load wb_prime ###############

tid = 'tides'
wb_mean, w_mean, b_mean, tave = fu.load_wb_variables(tid)


# %%
idepth =  58
if tid == '': wb_prime = wb_mean.wb_mean.isel(time=0) - w_mean.w.isel(time=0) * b_mean.b.isel(time=0)
else: wb_prime = wb_mean.wb_mean - w_mean.w * b_mean.b
wb_prime = wb_prime * mask_land
wb_prime = wb_prime.isel(depth=idepth)  #18 > 53m

# %%
data_wb_prime_coarse = pyic.interp_to_rectgrid_xr(wb_prime, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
Tri_wb_prime, data_reg_wb_prime = eva.calc_triangulation_obj_wave(wb_prime, lon_reg2, lat_reg2)


# %%
data_reg_wb_prime_tides = data_reg_wb_prime
data_wb_prime_coarse_tides =  data_wb_prime_coarse
# %%
data_reg_wb_prime_notides = data_reg_wb_prime
data_wb_prime_coarse_notides =  data_wb_prime_coarse
# %% bias
data_reg_wb_prime_bias = data_reg_wb_prime_tides - data_reg_wb_prime_notides
data_wb_prime_coarse_bias =  data_wb_prime_coarse_tides - data_wb_prime_coarse_notides


#%%

#######################################################################################################
#######################################################################################################
# %% classic 3 figure plot
reload(smt_temp)

fu.plot_temperature_zoom(data_sst, data_sst_coarse, Tri_sst, data_reg_sst, lat_reg_1, lon_reg_1, lat_reg_2, lon_reg_2, lat_reg_3, lon_reg_3, rlon, rlat, data_r, fig_path, savefig )

# %%
reload(fu)
lon_reg = -2, 20
lat_reg = -38, -28

# lon_reg = 2.5, 12.5
# lat_reg = -35, -28

data_vort_coarse_sel = data_vort_coarse.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0], lat_reg[1]))   
data_r_sel = data_r.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0], lat_reg[1]))

fu.plot_vort_sel(data_vort_coarse_sel, lon_reg, lat_reg, data_r_sel, fig_path, savefig)
    # %%
reload(fu)
reload(smt_vort)

# fu.plot_vort_zoom(data_vort_coarse, data_vort_coarse, Tri_vort, data_reg_vort, lat_reg, lon_reg, lat_reg1, lon_reg1, lat_reg2, lon_reg2, data_r, fig_path, savefig )
stri='t'
fu.plot_vort_zoom(data_vort_coarse, patches_v, data_native_interp, lon_reg, lat_reg, lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, stri, fig_path, savefig )
# fu.plot_wb_prime_zoom(data_wb_prime_coarse, Tri_wb_prime, data_reg_wb_prime, lon_reg, lat_reg,  lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, stri, fig_path, savefig)

# %%
reload(smt_vort)
reload(fu)
reload(eva)

fu.plot_tke_zoom(data_tke_coarse, Tri_tke, data_reg_tke, lon_reg, lat_reg,  lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, fig_path, savefig)

# %%
reload(smt_vort)
reload(fu)
reload(eva)
# stri = f'{tave}'+f'_bias'
# fu.plot_wb_prime_zoom(data_wb_prime_coarse_bias, Tri_wb_prime, data_reg_wb_prime_bias, lon_reg, lat_reg,  lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, stri, fig_path, savefig)

stri = f'{tave}'+f'_{tid}_{w_mean.depth[idepth].values}'
fu.plot_wb_prime_zoom(data_wb_prime_coarse, Tri_wb_prime, data_reg_wb_prime, lon_reg, lat_reg,  lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, stri, fig_path, savefig)

# %%
reload(smt_vort)
reload(fu)
reload(eva)

fu.plot_m2_zoom2(data_m2_coarse, Tri_m2, data_regm, lon_reg, lat_reg,  lon_reg1, lat_reg1, lon_reg2, lat_reg2, data_r, fig_path, savefig)

#################################################################################
# %% general plot function
fname = 'na'
data  = data_sst
clim  = clim_sst
cmap  = 'plasma' #'RdPu_r'
fu.plot_na(data, lat_reg, lon_reg, clim, data_r.lon, data_r.lat, data_r, cmap, fig_path, fname)

# %% slice m2
m2        = m2
m2_interp = pyic.interp_to_rectgrid_xr(m2, fpath_ckdtree, lon_reg=lon_reg2, lat_reg=lat_reg2)
slice_m2  = m2_interp.sel(lon=0, method='nearest')
# %% plot tke slice
tke = ds_tke.isel(time=0).tke 
tke_interp = pyic.interp_to_rectgrid_xr(tke, fpath_ckdtree, lon_reg=lon_reg2, lat_reg=lat_reg2)
slice_tke = tke_interp.sel(lon=0, method='nearest')

# %% plot tke slice
rhop       = fu.compute_density(b_mean.b)
rho_interp = pyic.interp_to_rectgrid_xr(rhop, fpath_ckdtree, lon_reg=lon_reg2, lat_reg=lat_reg2)
slice_rho = rho_interp.sel(lon=0, method='nearest')

# %%
# wb_prime        = wb_mean.wb_mean.isel(time=0) - w_mean.w.isel(time=0) * b_mean.b.isel(time=0)
wb_prime        = wb_mean.wb_mean - w_mean.w * b_mean.b
wb_prime        = wb_prime * mask_land
wb_prime_interp = pyic.interp_to_rectgrid_xr(wb_prime, fpath_ckdtree, lon_reg=lon_reg2, lat_reg=lat_reg2)
slice_wb_prime  = wb_prime_interp.sel(lon=0, method='nearest')

# %%

mld_mean  = fu.load_mld_mean(tid)
slice_mld = fu.compute_slice(mld_mean, fpath_ckdtree, lon_reg, lat_reg, lon_slice=0)
# %%
reload(fu)
x        = slice_tke.lat
y        = slice_tke.depth

fu.plot_slice_front(x, y, slice_m2, slice_wb_prime, slice_tke, slice_rho, slice_mld,  ylim=np.array([300,0]), fig_path=fig_path, savefig=savefig)


################################################################################
# %% tides
reload(fu)
lon_reg = lon_reg2
lat_reg = lat_reg2


tid = 'tides'
wb_mean, w_mean, b_mean, tave = fu.load_wb_variables(tid)

wb_prime        = wb_mean.wb_mean - w_mean.w * b_mean.b
wb_prime        = wb_prime * mask_land
slice_wb_prime  = fu.compute_slice(wb_prime, fpath_ckdtree, lon_reg, lat_reg, lon_slice=0)

rho       = fu.compute_density(b_mean.b)
slice_rho = fu.compute_slice(rho, fpath_ckdtree, lon_reg, lat_reg, lon_slice=0)

mld_mean  = fu.load_mld_mean(tid)
slice_mld = fu.compute_slice(mld_mean, fpath_ckdtree, lon_reg, lat_reg, lon_slice=0)

slice_wb_prime_tides = slice_wb_prime
slice_rho_tides      = slice_rho
slice_mld_tides      = slice_mld

# %% notides
tid = 'notides'
wb_mean, w_mean, b_mean, tave = fu.load_wb_variables(tid)

wb_prime        = wb_mean.wb_mean - w_mean.w * b_mean.b
wb_prime        = wb_prime * mask_land
slice_wb_prime  = fu.compute_slice(wb_prime, fpath_ckdtree, lon_reg, lat_reg, lon_slice=0)

rho       = fu.compute_density(b_mean.b)
slice_rho = fu.compute_slice(rho, fpath_ckdtree, lon_reg, lat_reg, lon_slice=0)

mld_mean  = fu.load_mld_mean(tid)
slice_mld = fu.compute_slice(mld_mean, fpath_ckdtree, lon_reg, lat_reg, lon_slice=0)

slice_wb_prime_notides = slice_wb_prime
slice_rho_notides      = slice_rho
slice_mld_notides      = slice_mld

#%%
reload(fu)
x        = slice_wb_prime.lat
y        = slice_wb_prime.depth

fu.plot_slice_front_tidenotide(x, y, slice_wb_prime_tides, slice_wb_prime_notides, slice_rho_tides, slice_rho_notides, slice_mld_tides, slice_mld_notides, ylim=np.array([300,0]), fig_path=fig_path, savefig=savefig)

##########################################################
# %%
reload(eva)
ds_tke          = eva.load_smt_wave_tke_exp7_08(it=1)
ds_tke = ds_tke.drop('clon_bnds').drop('clat_bnds')
# %%
beginn = '2019-07-31-T07:15:00'
end    = '2019-08-07-T05:15:00'
tke    = ds_tke.sel(time=slice(beginn, end))
# %%
lat = -37.5
lat = 0
# %%
reload(eva)
idx = eva.compute_ncell_idx(lon=0, lat=lat)
# %%

tke_time_series = tke.tke.isel(ncells=idx).isel(depth=slice(0, 25)).compute()
# %%

hca, hcb = pyic.arrange_axes(1,1, plot_cb = True, asp=0.5, fig_size_fac=2)  
ii=-1  
ii+=1; ax=hca[ii]; cax=hcb[ii]
#make meshgrid
X,Y = np.meshgrid(tke_time_series.time, tke_time_series.depth)
# transpose

pyic.shade(X,Y, np.transpose(tke_time_series.data), ax=ax, cax=cax, cmap='plasma')

# flip y axis
ax.set_ylim(ax.get_ylim()[::-1])
ax.set_ylabel('Depth [m]')
ax.set_title('TKE timeseries at lon=0, lat=-37.5')
plt.savefig(f'{fig_path}/tke_timeseries.png', dpi=150, bbox_inches='tight')

# %%

# %% vorticity nils

ds = vort
# --- tripolar grid
ds_tg = gg

# --- resolution
#timi = pyic.timing(timi, 'load res')
#cell_areai = pyic.interp_to_rectgrid_xr(ds_tg.cell_area.rename(cell='ncells'), 
#  fpath_ckdtree_03)
#resi = np.sqrt(cell_areai)

# %%
# --- Coriolis parameter

fcor = 2. * 2.*np.pi/86400.*np.sin(ds_tg.vlat.rename(vertex='ncells'))
fcor = fcor.compute()


# %%
lon_reg_1 = [-14, 34]
lat_reg_1 = [-43, -11]
lon_reg_2 = [0, 12]
lat_reg_2 = [-35, -29]
lon_reg_3 = [5, 9]
lat_reg_3 = [-32, -30]
use_interp = False
#use_interp = True

# --- derive Triangulation object
if not use_interp:
  ##ind_reg, Tri = pyic.triangulation(ds_tg, lon_reg=lon_reg_3, lat_reg=lat_reg_3)
  #ireg_v = (  (ds_tg.vlon>lon_reg_3[0]) 
  #          & (ds_tg.vlon<=lon_reg_3[1]) 
  #          & (ds_tg.vlat>lat_reg_3[0]) 
  #          & (ds_tg.vlat<=lat_reg_3[1])
  #         )
  #ireg_c = (  (ds_tg.clon>lon_reg_3[0]) 
  #          & (ds_tg.clon<=lon_reg_3[1]) 
  #          & (ds_tg.clat>lat_reg_3[0]) 
  #          & (ds_tg.clat<=lat_reg_3[1])
  #         )
  #clon_bnds, clat_bnds, vlon_bnds, vlat_bnds, cells_of_vertex = pyic.patch_plot_derive_bnds(ds_tg)
  #vlon_bnds_reg = vlon_bnds[ireg_v]
  #vlat_bnds_reg = vlat_bnds[ireg_v]
  #cells_of_vertex_reg = cells_of_vertex[ireg_v]
  #clon_bnds_reg = clon_bnds[ireg_c]
  #clat_bnds_reg = clat_bnds[ireg_c]
  #patches_c, patches_v = pyic.patch_plot_patches_from_bnds(clon_bnds_reg, clat_bnds_reg, vlon_bnds_reg, vlat_bnds_reg, cells_of_vertex_reg)
  clon = ds_tg.clon.compute().data * 180./np.pi
  clat = ds_tg.clat.compute().data * 180./np.pi
  ireg_c = np.where(
      (clon>lon_reg_3[0]) & (clon<=lon_reg_3[1]) & (clat>lat_reg_3[0]) & (clat<=lat_reg_3[1])
  )[0]
  ds_tg_cut = pyic.xr_crop_tgrid(ds_tg, ireg_c)
  ireg_v = ds_tg_cut['ireg_v'].data
  
  clon_bnds, clat_bnds, vlon_bnds, vlat_bnds, cells_of_vertex = pyic.patch_plot_derive_bnds(ds_tg_cut)
  patches_c, patches_v = pyic.patch_plot_patches_from_bnds(
      clon_bnds.compute(), clat_bnds.compute(), 
      vlon_bnds.compute(), vlat_bnds.compute(), 
      cells_of_vertex#.compute()
  )

# --- load data
step = 1 # dummy step
# %%
data_xr = ds.rename(ncells_2='ncells')
data_xr *= 1./(fcor+1e-33)

# --- interpolate to regular grid
data_xr = data_xr.compute()
data_xr = data_xr.where(data_xr!=0)
data_xr_interp_1 = pyic.interp_to_rectgrid_xr(data_xr, fpath_ckdtree, 
  lon_reg=lon_reg_1, lat_reg=lat_reg_1, coordinates='vlat vlon')
data_xr_interp_2 = pyic.interp_to_rectgrid_xr(data_xr, fpath_ckdtree, 
  lon_reg=lon_reg_2, lat_reg=lat_reg_2, coordinates='vlat vlon')
if use_interp:
  data_xr_interp_3 = pyic.interp_to_rectgrid_xr(data_xr, fpath_ckdtree, 
    lon_reg=lon_reg_3, lat_reg=lat_reg_3, coordinates='vlat vlon')
else:
  data_xr_interp_3 = data_xr[ireg_v]

# %%
from matplotlib.patches import Rectangle

reload(smt_vort)
plt.close('all')
ccrs_proj = ccrs.PlateCarree()

#hca, hcb = pyic.arrange_axes(1,1, plot_cb=True, fig_size_fac=2., asp=0.5, projection=ccrs_proj)
dpi = 300
clim = 1.
fig, hca, cax = smt_vort.make_axes_nils(lon_reg_1, lat_reg_1, lon_reg_2, lat_reg_2, lon_reg_3, lat_reg_3, dpi)

ax=hca[0]; cax=cax
hm1 = pyic.shade(data_xr_interp_1.lon, data_xr_interp_1.lat, data_xr_interp_1.data, 
                 ax=ax, cax=cax, clim=clim)
ax=hca[1]; cax=cax
hm2 = pyic.shade(data_xr_interp_2.lon, data_xr_interp_2.lat, data_xr_interp_2.data, 
                 ax=ax, cax=cax, clim=clim)
ax=hca[2]; cax=cax
if use_interp:
  hm3 = pyic.shade(data_xr_interp_3.lon, data_xr_interp_3.lat, data_xr_interp_3.data, 
                   ax=ax, cax=cax, clim=clim)
else:
  hm3 = pyic.patch_plot_shade(patches_v, data_xr_interp_3, ax=ax, cax=cax, clim=clim)
  hm3 = (hm3,)

# --- colorbar
cax.set_ylabel("rel. vort. / plan. vort.")
x0, y0 = cax.get_position().x0, cax.get_position().y0
width, height = cax.get_position().width, cax.get_position().height
cax.set_position([0.45, 0.65, width, height])
cax.set_facecolor('0.9')

# --- 1st axes settings
ax=hca[0]; cax=cax

ax.set_xticks(np.arange(-170,170,10), crs=ccrs_proj)
ax.set_yticks(np.arange(-80,80,10), crs=ccrs_proj)
ax.set_xlim(lon_reg_1)
ax.set_ylim(lat_reg_1)

#rectprop = dict(facecolor='none', edgecolor='k', lw=2)
#ax.add_patch(Rectangle((-12,-38), 22, 7, **rectprop))
#ax.text(-1, -31., 'SONETT', ha='center', va='bottom', fontsize=8)

# rectprop = dict(facecolor='none', edgecolor='0.7', lw=2)
# ax.add_patch(Rectangle((-12,-37.5), 5, 4, **rectprop))
# ax.text(-9.5, -33.5, 'SONETT II', ha='center', va='bottom', fontsize=8)

rectprop = dict(facecolor='none', edgecolor='0.7', lw=5)
ax.add_patch(Rectangle((1,-35), 9, 5, **rectprop))
ax.text(5.5, -30., 'SONETT', ha='center', va='bottom', fontsize=18)

# ax.text(-2.5, -35, 'Walvis Ridge', va='center', ha='center', rotation=45, fontsize=8)

#ax.contour(lont, latt, topo, np.arange(0,6000.,1000.), colors='0.7', linewidths=1.0, zorder=1)

# --- time string (also updated)
bbox=dict(facecolor='w', alpha=1., edgecolor='none')
ht = ax.text(0.02, 0.96, str(data_xr.time.data)[:16], transform=ax.transAxes, bbox=bbox)
ht.set_text(str(data_xr.time.data)[:16])

if savefig == True: plt.savefig(f'{fig_path}/vorticity_sonett.png', dpi=250, bbox_inches='tight')

# %% # evaluation of agulhas ring - anticyclone anomaly within single rings

ds = eva.load_smt_wave_all('w', 9, it =2)

# %%
# lon_reg = np.array([-20, 20])
# lat_reg = np.array([-50, -10])
# fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.01_180W-180E_90S-90N.nc'
fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/high_res/'


w_sel = ds.w.isel(time=100).isel(depth=30)
w_reg = pyic.interp_to_rectgrid_xr(w_sel, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# w_reg.plot(vmin = -1e-3, vmax=1e-3, cmap='RdBu_r')
# %%
# import rectangle
ccrs_proj = ccrs.PlateCarree()
from matplotlib.patches import Rectangle
import cmocean.cm as cm
lon_reg = np.array([0, 20])
lat_reg = np.array([-40, -20])
lon_reg_z = np.array([6, 9])
lat_reg_z = np.array([-33.5, -30.5])
# %%

# triagnulation obj
Tri, data_reg = eva.calc_triangulation_obj_wave(w_sel, lon_reg_z, lat_reg_z)

# %%

def plot_w(w_reg, Tri, data_reg, clim):
  cmap = cm.balance
  # clim = 4e-3

  hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=4, projection=ccrs_proj)  
  ii=-1  
  ii+=1; ax=hca[ii]; cax=hcb[ii]
  pyic.shade(w_reg.lon, w_reg.lat, w_reg.data, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
  ax.set_title(f'depth = {w_sel.depth.values:.0f} m')
  rectprop = dict(facecolor='none', edgecolor='r', lw=2)
  ax.add_patch(Rectangle((lon_reg_z[0], lat_reg_z[0]), lon_reg_z[1]-lon_reg_z[0], lat_reg_z[1]-lat_reg_z[0], **rectprop))
  pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

  ii+=1; ax=hca[ii]; cax=hcb[ii]
  # pyic.shade(w_reg.lon, w_reg.lat, w_reg.data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
  hm1 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)
  ax.set_title(f'{w_sel.time.values:.13}')
  hm1[1].ax.yaxis.offsetText.set_position((2.1,0))
  hm1[1].formatter.set_useMathText(True)

  pyic.plot_settings(ax, xlim=lon_reg_z, ylim=lat_reg_z)

  for ax in hca:
      ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
      ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

  if savefig==True: plt.savefig(f'{fig_path}agulhas_rings/w_{w_reg.depth.values:.0f}.png', dpi=250, bbox_inches='tight')
# %%
idepths = [17, 30, 45, 90, 100, 110]

for idepth in idepths:
  w_sel = ds.w.isel(time=100).isel(depth=idepth)
  w_reg = pyic.interp_to_rectgrid_xr(w_sel, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
  Tri, data_reg = eva.calc_triangulation_obj_wave(w_sel, lon_reg_z, lat_reg_z)
  plot_w(w_reg, Tri, data_reg)
# %% and for vorticity
vort = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190717T171500Z.nc', chunks=dict(depth=1))

vort = vort.vort.isel(depth=1).isel(time=0)
lat              = np.rad2deg(ds_0.vlat)
f                = eva.calc_coriolis_parameter(lat)
fcor = 2. * 2.*np.pi/86400.*np.sin(ds_0.vlat)
fcor = fcor.compute()
Ro               = vort / f
data_vort_coarse = pyic.interp_to_rectgrid_xr(Ro, fpath_ckdtree, coordinates='vlat vlon', lon_reg=lon_reg, lat_reg=lat_reg)
data_mask_coarse = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_vort_coarse = data_vort_coarse * data_mask_coarse
# %%
reload(eva)
Ro = Ro.compute()
patches_v, data_native_interp = eva.calc_triangulation_obj_wave_vertices(Ro, gg, lon_reg_z, lat_reg_z)

# %%

def plot_vort(w_reg, patches_v, data_native_interp, clim=1):
  cmap = cm.balance
  # clim = 4e-3

  hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=4, projection=ccrs_proj)  
  ii=-1  
  ii+=1; ax=hca[ii]; cax=hcb[ii]
  pyic.shade(w_reg.lon, w_reg.lat, w_reg.data, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
  ax.set_title(f'depth = {w_reg.depth.values:.0f} m')
  rectprop = dict(facecolor='none', edgecolor='r', lw=2)
  ax.add_patch(Rectangle((lon_reg_z[0], lat_reg_z[0]), lon_reg_z[1]-lon_reg_z[0], lat_reg_z[1]-lat_reg_z[0], **rectprop))
  pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

  ii+=1; ax=hca[ii]; cax=hcb[ii]
  # pyic.shade(w_reg.lon, w_reg.lat, w_reg.data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
  # hm1 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)
  hm1 = pyic.patch_plot_shade(patches_v, data_native_interp, ax=ax, cax=cax, clim=clim)
  ax.set_title(f'{w_reg.time.values:.13}')
  # hm1.ax.yaxis.offsetText.set_position((2.1,0))
  # hm1.formatter.set_useMathText(True)

  pyic.plot_settings(ax, xlim=lon_reg_z, ylim=lat_reg_z)

  for ax in hca:
      ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
      ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

  if savefig==True: plt.savefig(f'{fig_path}agulhas_rings/vort2_{w_reg.depth.values:.0f}.png', dpi=250, bbox_inches='tight')


# %%
plot_vort(data_vort_coarse, patches_v, data_native_interp)
# %%
