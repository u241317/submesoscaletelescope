# %%
import sys
import glob, os
import cartopy
import pyicon as pyic
sys.path.append('../../../')
import smt_modules.all_funcs as eva

from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
# import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
# from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
# import gsw

# from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
# import matplotlib.patches as patches
# import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

from matplotlib.patches import Rectangle
import cmocean.cm as cm
# %% get cluster
# reload(scluster)

# client, cluster = scluster.init_dask_slurm_cluster_gpu(walltime='00:10:00', wait=True)
# cluster
# client
# %%
# fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.01_180W-180E_90S-90N.nc'
fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
fig_path       = '/home/m/m300878//submesoscaletelescope/results/smt_wave/high_res/agulhas_rings/vorticity/animation/'
# %%
# import rectangle
lon_reg = np.array([0, 20])
lat_reg = np.array([-40, -20])
lon_reg_z = np.array([6, 9])
lat_reg_z = np.array([-33.5, -30.5])

# %% ###############################################vertical velocity###########################
# vort      = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190717T171500Z.nc', chunks=dict(depth=1))
if False:
  ds_all   = eva.load_smt_wave_9('2d', it =1)
else:
  ds_all   = eva.load_smt_wave_all('2d', 7, it =1)
  ds_all   = ds_all.drop('vlon').drop('vlat')
ds_0      = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190704T001500Z.nc', chunks=dict(depth=1))
mask_land = eva.load_smt_wave_land_mask()
tgrid     = eva.load_smt_wave_grid()
# # evaluation of agulhas ring - anticyclone anomaly within single rings
# ds_all = eva.load_smt_wave_all('w', 9, it =1)
ds     = ds_all.vort.isel(time=slice(50,450)).isel(depth=1)
print('finished loading dataset')

data_mask_coarse = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
lat              = np.rad2deg(ds_0.vlat)
f                = eva.calc_coriolis_parameter(lat)
fcor             = 2. * 2.*np.pi/86400.*np.sin(ds_0.vlat)
fcor             = fcor.compute()
# %%
ts = pyic.timing([0], 'start')

run      = 'eddy_zoom'
path_fig = f'{fig_path}images/'
nnf      = 0
savefig  = True

if not os.path.exists(path_fig):
  try:
    os.makedirs(path_fig)
  except:
    pass


# %%

print('--- '+run)
itd0   = 0
itdEnd = ds.time.shape
timesd = ds.time.data

##################################################################################
steps = np.arange(itd0, itdEnd[0])
print('delegate steps')
# %%
# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

print('selected steps',steps)
# %%
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
  # t1 = timesd[step]
  # t2 = timesd[step+1]
  # print(t1, t2)
  # if t1==t2:
  #   continue

  ### load file for oine timestep
  # %
  print(f'my timestep: {step}')

  # data = ds.isel(time=step)
  asp  = 1
  clim = 1
  cmap = cm.curl

  # % compute
  print('compute time step %d' % (step))
  # w_reg         = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
  # Tri, data_tri = eva.calc_triangulation_obj_wave(data, lon_reg_z, lat_reg_z)


  vort     = ds.isel(time=step)
  Ro       = vort / f
  Ro       = Ro.compute()
  data_reg = pyic.interp_to_rectgrid_xr(Ro, fpath_ckdtree, coordinates='vlat vlon', lon_reg=lon_reg, lat_reg=lat_reg)
  data_reg = data_reg * data_mask_coarse
  # %
  # reload(eva)
  # patches_v, data_tri = eva.calc_triangulation_obj_wave_vertices(Ro, tgrid, lon_reg_z, lat_reg_z)

  if nn ==0:
    clon = tgrid.clon.compute().data * 180./np.pi
    clat = tgrid.clat.compute().data * 180./np.pi
    ireg_c = np.where(
        (clon>lon_reg_z[0]) & (clon<=lon_reg_z[1]) & (clat>lat_reg_z[0]) & (clat<=lat_reg_z[1])
    )[0]
    ds_tg_cut = pyic.xr_crop_tgrid(tgrid, ireg_c)
    ireg_v = ds_tg_cut['ireg_v'].data

    clon_bnds, clat_bnds, vlon_bnds, vlat_bnds, cells_of_vertex = pyic.patch_plot_derive_bnds(ds_tg_cut)
    patches_c, patches_v = pyic.patch_plot_patches_from_bnds(
        clon_bnds.compute(), clat_bnds.compute(), 
        vlon_bnds.compute(), vlat_bnds.compute(), 
        cells_of_vertex#.compute()
    )

  Ro_tri = Ro[ireg_v]


  # %
  # ================================================================================   
  # Here start plotting
  # ================================================================================   

  # %
  if nn==0:
    plt.close('all')
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=4, projection=ccrs_proj)  
    ii=-1  
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm0 = pyic.shade(data_reg.lon, data_reg.lat, data_reg.data, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(f'depth = {data_reg.depth.values:.0f} m')
    rectprop = dict(facecolor='none', edgecolor='r', lw=2)
    ax.add_patch(Rectangle((lon_reg_z[0], lat_reg_z[0]), lon_reg_z[1]-lon_reg_z[0], lat_reg_z[1]-lat_reg_z[0], **rectprop))
    
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    hm1 = pyic.patch_plot_shade(patches_v, Ro_tri, ax=ax, cax=cax, clim=clim, cmap=cmap)
    hm1 = (hm1,)
    ht = ax.set_title(f'{data_reg.time.values:.13}')
    # hm1[1].ax.yaxis.offsetText.set_position((2.1,0))
    # hm1[1].formatter.set_useMathText(True)

    pyic.plot_settings(ax, xlim=lon_reg_z, ylim=lat_reg_z)

    for ax in hca:
      ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
      ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
  else:
    ## --- update figures
    hm0[0].set_array(data_reg.data.flatten())
    hm1[0].set_array(Ro_tri.data.flatten())
    ht.set_text(str(f'{vort.time.values:.13}').replace('T',' '))

  if savefig:
    fpath = '%s%s_%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], run, step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, bbox_inches='tight', dpi=250)
  else:
    plt.draw()
    input()

ts = pyic.timing(ts, 'all done')


# %%
