
# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp
#add folder to path
sys.path.append('../')
import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

from matplotlib.patches import Rectangle
import cmocean.cm as cm
# %% get cluster
# reload(scluster)

# client, cluster = scluster.init_dask_slurm_cluster_gpu(walltime='01:00:00', wait=False)
# cluster
# client
# %%
reload(eva)
reload(smt_vort)
savefig = False
# fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.01_180W-180E_90S-90N.nc'
fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/high_res/'
# %%
# import rectangle
lon_reg = np.array([0, 20])
lat_reg = np.array([-40, -20])
lon_reg_z = np.array([6, 9])
lat_reg_z = np.array([-33.5, -30.5])

# %% ###############################################vertical velocity###########################
# # evaluation of agulhas ring - anticyclone anomaly within single rings
ds = eva.load_smt_wave_all('w', 9, it =2)

# %%

def plot_w(w_reg, Tri, data_reg, clim):
  cmap = cm.balance
  # clim = 4e-3

  hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=4, projection=ccrs_proj)  
  ii=-1  
  ii+=1; ax=hca[ii]; cax=hcb[ii]
  pyic.shade(w_reg.lon, w_reg.lat, w_reg.data, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
  ax.set_title(f'depth = {w_sel.depth.values:.0f} m')
  rectprop = dict(facecolor='none', edgecolor='r', lw=2)
  ax.add_patch(Rectangle((lon_reg_z[0], lat_reg_z[0]), lon_reg_z[1]-lon_reg_z[0], lat_reg_z[1]-lat_reg_z[0], **rectprop))
  pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

  ii+=1; ax=hca[ii]; cax=hcb[ii]
  # pyic.shade(w_reg.lon, w_reg.lat, w_reg.data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
  hm1 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)
  ax.set_title(f'{w_sel.time.values:.13}')
  hm1[1].ax.yaxis.offsetText.set_position((2.1,0))
  hm1[1].formatter.set_useMathText(True)

  pyic.plot_settings(ax, xlim=lon_reg_z, ylim=lat_reg_z)

  for ax in hca:
      ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
      ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

  if savefig==True: plt.savefig(f'{fig_path}agulhas_rings/w_{w_reg.depth.values:.0f}.png', dpi=250, bbox_inches='tight')
# %%
idepths = [17, 30, 45, 90, 100, 110]

for idepth in idepths:
  w_sel = ds.w.isel(time=100).isel(depth=idepth)
  w_reg = pyic.interp_to_rectgrid_xr(w_sel, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
  Tri, data_reg = eva.calc_triangulation_obj_wave(w_sel, lon_reg_z, lat_reg_z)
  plot_w(w_reg, Tri, data_reg, clim=4e-3)


# %% ##################################### vorticity#######################
vort      = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190717T171500Z.nc', chunks=dict(depth=1))
ds_0      = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190704T001500Z.nc', chunks=dict(depth=1))
mask_land = eva.load_smt_wave_land_mask()
tgrid     = eva.load_smt_wave_grid()

vort             = vort.vort.isel(depth=0).isel(time=0)
lat              = np.rad2deg(ds_0.vlat)
f                = eva.calc_coriolis_parameter(lat)
fcor             = 2. * 2.*np.pi/86400.*np.sin(ds_0.vlat)
fcor             = fcor.compute()
Ro               = vort / f
data_vort_coarse = pyic.interp_to_rectgrid_xr(Ro, fpath_ckdtree, coordinates='vlat vlon', lon_reg=lon_reg, lat_reg=lat_reg)
data_mask_coarse = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_vort_coarse = data_vort_coarse * data_mask_coarse
# %%
reload(eva)
Ro = Ro.compute()
patches_v, data_tri = eva.calc_triangulation_obj_wave_vertices(Ro, tgrid, lon_reg_z, lat_reg_z)

# %%

def plot_vort(data_reg, patches_v, data_tri, clim=1):
  cmap = cm.balance
  # clim = 4e-3

  hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=4, projection=ccrs_proj)  
  ii=-1  
  ii+=1; ax=hca[ii]; cax=hcb[ii]
  pyic.shade(data_reg.lon, data_reg.lat, data_reg.data, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
  ax.set_title(f'depth = {data_reg.depth.values:.0f} m')
  rectprop = dict(facecolor='none', edgecolor='r', lw=2)
  ax.add_patch(Rectangle((lon_reg_z[0], lat_reg_z[0]), lon_reg_z[1]-lon_reg_z[0], lat_reg_z[1]-lat_reg_z[0], **rectprop))
  pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

  ii+=1; ax=hca[ii]; cax=hcb[ii]
  # pyic.shade(data_reg.lon, data_reg.lat, data_reg.data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
  # hm1 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)
  hm1 = pyic.patch_plot_shade(patches_v, data_tri, ax=ax, cax=cax, clim=clim)
  ax.set_title(f'{data_reg.time.values:.13}')
  # hm1.ax.yaxis.offsetText.set_position((2.1,0))
  # hm1.formatter.set_useMathText(True)

  pyic.plot_settings(ax, xlim=lon_reg_z, ylim=lat_reg_z)

  for ax in hca:
      ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
      ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

  if savefig==True: plt.savefig(f'{fig_path}agulhas_rings/vort2_{data_reg.depth.values:.0f}.png', dpi=250, bbox_inches='tight')


# %%
plot_vort(data_vort_coarse, patches_v, data_tri)
# %%
