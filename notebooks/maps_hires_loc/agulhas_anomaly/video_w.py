# %%
import sys
import glob, os
import cartopy
import pyicon as pyic
sys.path.append('../../../')
import smt_modules.all_funcs as eva
# from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
# import smt_modules.tools as tools
# import smt_modules.maps_icon_smt_vort as smt_vort
# import smt_modules.maps_icon_smt_temp as smt_temp
#add folder to path
# sys.path.append('../')
# import funcs as fu
# import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
# import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
# from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
# import gsw

# from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
# import matplotlib.patches as patches
# import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

from matplotlib.patches import Rectangle
import cmocean.cm as cm
# # %% get cluster
# reload(scluster)

# client, cluster = scluster.init_dask_slurm_cluster(walltime='00:10:00', dash_address="8686", wait=True)
# cluster
# client
# %%
# fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.01_180W-180E_90S-90N.nc'
fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
fig_path       = '/home/m/m300878//submesoscaletelescope/results/smt_wave/high_res/agulhas_rings/vertical_velocity/animation/'
# %%
# import rectangle
lon_reg   = np.array([0, 20])
lat_reg   = np.array([-40, -20])
lon_reg_z = np.array([6, 9])
lat_reg_z = np.array([-33.5, -30.5])

# %% ###############################################vertical velocity###########################
# # evaluation of agulhas ring - anticyclone anomaly within single rings
if False:
  ds_all   = eva.load_smt_wave_9('w', it =1)
else:
  ds_all = eva.load_smt_wave_all('w', 7, it =1)
  ds_all = ds_all.drop('clon_bnds').drop('clat_bnds')
  ds_all = ds_all.drop('clon').drop('clat')
ds     = ds_all.w.isel(time=slice(50,250)).isel(depth=45)
print('finished loading dataset')

# %%
ts = pyic.timing([0], 'start')

run      = 'eddy_zoom_w'
path_fig = f'{fig_path}images/'
nnf      = 0
savefig  = True

if not os.path.exists(path_fig):
  try:
    os.makedirs(path_fig)
  except:
    pass


# %%

print('--- '+run)
itd0   = 0
itdEnd = ds.time.shape
timesd = ds.time.data

##################################################################################
steps = np.arange(itd0, itdEnd[0])
print('delegate steps')
# %%
# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1
print('proc %d/%d: Hello world!' % (rank, npro))

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

print('selected steps',steps)
# %%
for nn, step in enumerate(steps):
  print('proc %d/%d: Step %d/%d' % (rank, npro, nn, len(steps)))
  # t1 = timesd[step]
  # t2 = timesd[step+1]
  # print(t1, t2)
  # if t1==t2:
  #   continue

  ### load file for oine timestep
  # %
  print(f'my timestep: {step}')

  data = ds.isel(time=step)
  asp  = 1
  clim = 4e-3
  cmap = cm.balance

  # % compute
  print('compute time step %d' % (step))
  w_reg         = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
  Tri, data_tri = eva.calc_triangulation_obj_wave(data, lon_reg_z, lat_reg_z)

  # %
  # ================================================================================   
  # Here start plotting
  # ================================================================================   

  # %
  if nn==0:
    plt.close('all')
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=4, projection=ccrs_proj)  
    ii=-1  
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm0 = pyic.shade(w_reg.lon, w_reg.lat, w_reg.data, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
    hd = ax.set_title(f'depth = {data.depth.values:.0f} m')
    rectprop = dict(facecolor='none', edgecolor='r', lw=2)
    ax.add_patch(Rectangle((lon_reg_z[0], lat_reg_z[0]), lon_reg_z[1]-lon_reg_z[0], lat_reg_z[1]-lat_reg_z[0], **rectprop))
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # pyic.shade(w_reg.lon, w_reg.lat, w_reg.data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
    hm1 = pyic.shade(Tri, data_tri, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)
    ht = ax.set_title(f'{data.time.values:.13}')
    hm1[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm1[1].formatter.set_useMathText(True)

    pyic.plot_settings(ax, xlim=lon_reg_z, ylim=lat_reg_z)
  else:
    ## --- update figures
    hm0[0].set_array(w_reg.data.flatten())
    hm1[0].set_array(data_tri.data)
    hd.set_text(str(f'depth = {data.depth.values:.0f} m').replace('T',' '))
    ht.set_text(str(f'{data.time.values:.13}').replace('T',' '))

  if savefig:
    fpath = '%s%s_%s_%04d.jpg' % (path_fig,__file__.split('/')[-1][:-3], run, step)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, bbox_inches='tight', dpi=250)
  else:
    plt.draw()
    input()

ts = pyic.timing(ts, 'all done')


# %%
