#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --time=01:00:00
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j
#SBATCH --ntasks=16
#SBATCH --partition=compute
#SBATCH --account=mh0033

module list
source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
# source /home/m/m300602/pyicon/tools/conda_act_mistral_pyicon_env.sh 
# source /home/m/m300602/pyicon/tools/act_pyicon_py39.sh

which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`

run="randompath1"
path_data="randompath2"

mpirun -np 16 python -u video_vort.py ${path_data} ${run}


enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

