# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools


import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors


#%%

path_2_rectgrid = '/home/m/m300602/work/icon/grids/r2b9_oce_r0004/ckdtree/rectgrids/r2b9_oce_r0004_res0.10_180W-180E_90S-90N.nc'
path_data = '/work/bm1344/k203123/experiments/erc1011/run_20020101T000000-20020131T235845/erc1011_oce_ml_1d_mean_20020102T000000Z.nc'
# %%
ds = xr.open_dataset(path_data, chunks={'time': 1, 'depth': 1})

# %%
lon_reg = np.array([-180, -100])
lat_reg = np.array([-5, 10])


data_interpolated = pyic.interp_to_rectgrid_xr(ds.u.isel(depth=1), path_2_rectgrid, lon_reg, lat_reg)
# %%
data_interpolated.plot()

# %%
