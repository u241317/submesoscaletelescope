# %%
url       = "https://eerie.cloud.dkrz.de/datasets"
dataset_id = "icon-esm-er.eerie-control-1950.atmos.native.2d_daily_min"

zarr_endpoint = '/'.join([url,dataset_id,"zarr"])
opendap_endpoint = '/'.join([url,dataset_id,"opendap"])

print(zarr_endpoint)
print(opendap_endpoint)

# %%
# client.close()
from distributed import Client

client = Client(n_workers=2, threads_per_worker=2)
client
# %%
import intake

eerie_cat = intake.open_catalog(
    "https://raw.githubusercontent.com/eerie-project/intake_catalogues/main/eerie.yaml"
)
cat = eerie_cat["dkrz.cloud"]
# %%
all_dkrz_datasets = list(cat)
print(all_dkrz_datasets[0])
# %%
dset = "icon-esm-er.eerie-control-1950.atmos.native.2d_daily_min"
icon_control_atm_2d_1mth_mean = cat[dset]
# %%
list(icon_control_atm_2d_1mth_mean)
# %%
ds = icon_control_atm_2d_1mth_mean[dset + "-zarr"].to_dask()
ds

# %%
import hvplot.xarray

towrite = ds["tas"].isel(time=slice(200, 250))
allbytes = towrite.nbytes / 1024 / 1024
print(allbytes, " MB to write")

#%%
ds.tas.isel(time=1).drop("time").hvplot.contourf()

# %%
ds_sel = ds.isel(time=1000).tas
# %%
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

# %%
import matplotlib.pyplot as plt

ds = ds.rename({"lon": "clon", "lat": "clat"})

ds = ds.isel(time=slice(10,12))

# %%
%%time
ds = ds.compute()
#%%

lon_reg = [-90,45]
lat_reg = [0,20]
fig = plt.figure(figsize=(60, 12), facecolor='white')
gs = fig.add_gridspec(1,1)
ax = fig.add_subplot(gs[0, 0])
# ax = basic_plot(data_native[variable], ax, fig, native_grid=True, time_pos=0, lon_range=None, lat_range=[-40, 40], vminmax=[0, 80])
ax =tools.basic_plot(ds.tas, ax, fig, native_grid=True, time_pos=100, lon_range=lon_reg, lat_range=lat_reg)


# %%
import xarray as xr

tasmin = xr.open_zarr("/work/bm0021/k204210/tets2.zarr").drop("cell_sea_land_mask")



# %%
import numpy as np

tasmin1 = ds.copy()
tasmin1["lat"] = np.rad2deg(ds["lat"])
tasmin1["lat"].attrs["units"] = "degrees"
tasmin1["lon"] = np.rad2deg(ds["lon"])
tasmin1["lon"].attrs["units"] = "degrees"
# %%
#############################################################

# %%
import xarray as xr
import zarr
from fsspec.implementations.http import HTTPFileSystem

fs = HTTPFileSystem()

http_map = fs.get_mapper("https://eerie.cloud.dkrz.de/datasets/hadgem3-gc5-n216-orca025.eerie-picontrol.atmos.native.atmos_monthly_aermon/zarr")

# open as a zarr group
zg = zarr.open_consolidated(http_map, mode="r")

# or open as another Xarray Dataset
ds = xr.open_zarr(http_map, consolidated=True)

ds
# %%
import matplotlib.pyplot as plt
plt.pcolormesh(ds.lon.values, ds.lat.values, ds.bldep.isel(time=0).values)
# %%
import intake
eerie_cat=intake.open_catalog("https://raw.githubusercontent.com/eerie-project/intake_catalogues/main/eerie.yaml")


# %%
cat = eerie_cat["dkrz.cloud"]
# %%
all_dkrz_datasets = list(cat)
print(all_dkrz_datasets[0])
# %%


# %%
cat = 'icon-esm-er.eerie-control-1950.ocean.native.2d_daily_mean'
cat = 'icon-esm-er.eerie-control-1950.ocean.gr025.2d_monthly_mean/zarr'

# %%
cat        = intake.open_catalog("https://raw.githubusercontent.com/eerie-project/intake_catalogues/main/eerie.yaml")
model      = 'icon-esm-er'
expid      = 'eerie-control-1950'
gridspec   = 'native'
realm      = 'ocean'
cat_regrid = cat['dkrz.disk.model-output'][model][expid][realm][gridspec]
print(list(cat_regrid))

# %%
ds = cat_regrid['2d_daily_mean'].to_dask()
ds = ds.ssh.isel(time=slice(0,365))

# %%
ds = cat.to_dask()
# %%
http_map = fs.get_mapper("https://eerie.cloud.dkrz.de/datasets/icon-esm-er.eerie-control-1950.ocean.gr025.2d_monthly_mean/zarr")
http_map = fs.get_mapper("https://eerie.cloud.dkrz.de/datasets/icon-esm-er.eerie-control-1950.ocean.native.2d_daily_mean/zarr")
# open as a zarr group
zg = zarr.open_consolidated(http_map, mode="r")
# or open as another Xarray Dataset
ds = xr.open_zarr(http_map, consolidated=True)
http_map = fs.get_mapper("https://eerie.cloud.dkrz.de/datasets/icon-esm-er.eerie-control-1950.ocean.native.2d_grid/zarr")
zg = zarr.open_consolidated(http_map, mode="r")
ds_grid = xr.open_zarr(http_map, consolidated=True)
# %%
plt.pcolormesh(ds.lon.values, ds.lat.values, ds.to.isel(time=0).squeeze().values)

# %%
ds_sel = ds.to.isel(time=1000).drop('cell_sea_land_mask').squeeze()
ds_sel = ds_sel.rename({"lon": "clon", "lat": "clat"})
# rad to degree
# ds_sel["clat"] = np.rad2deg(ds_sel["clat"])
# ds_sel["clon"] = np.rad2deg(ds_sel["clon"])


ds_sel
# %%

lon_reg = [-90,-45]
lat_reg = [0,45]
fig     = plt.figure()#figsize=(60, 12), facecolor='white'
gs      = fig.add_gridspec(1,1)
ax      = fig.add_subplot(gs[0, 0])
# ax      = basic_plot(data_native[variable], ax, fig, native_grid=True, time_pos=0, lon_range=None, lat_range=[-40, 40], vminmax=[0, 80])
ax      = tools.basic_plot(ds_sel, ax, fig, native_grid=True, time_pos=100, lon_range=lon_reg, lat_range=lat_reg, vminmax=[-1, 1], cmap='plasma')
# ax      = tools.plot_ds_with_cartopy(ds_sel, lon_reg, lat_reg, vmin=10, vmax=30)

# %%
import datetime

import cmocean
import datashader as DS
import numpy as np
from math import atan2,degrees
import pandas as pd
from datashader.mpl_ext import dsshow


import time, os, re
import xarray as xr
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib.ticker as mticker

import cartopy.crs as ccrs
import cartopy.feature as cfeature
from   cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

fig_path       = '/home/m/m300878/submesoscaletelescope/results/tapgfd/'
savefig        = True

# %%
def basic_DS_plot(ds_variable, lon_range, lat_range, vminmax=None, cmap='plasma'):
    projection = ccrs.Mollweide()
    # fig, ax = plt.subplots(subplot_kw={"projection": projection})

    fig     = plt.figure()#figsize=(60, 12), facecolor='white'
    fig.canvas.draw_idle()  # necessary to make things work

    gs      = fig.add_gridspec(1,1)
    ax      = fig.add_subplot(gs[0, 0])     
    # add projection 
    ax = plt.axes(projection=projection)
    # Get the data in a dataframe to use dasher plot
    data_set = pd.DataFrame(data={
        "variable": np.squeeze(ds_variable.values),
        "x": np.rad2deg(ds_variable.clon.values),
        "y": np.rad2deg(ds_variable.clat.values),
    },)
    data_set = data_set[(data_set['x']>lon_range[0]) & (data_set['x']<lon_range[1]) & 
                        (data_set['y']>lat_range[0]) & (data_set['y']<lat_range[1])]
    if vminmax is not None:
        artist = dsshow(data_set, DS.Point('x', 'y'), DS.mean('variable'), cmap=cmap, vmax=vminmax[1], vmin=vminmax[0], ax=ax)

    return fig, ax

def plot_datashader_proj(var, lon_range, lat_range, vmin, vmax, colormap, savefig, fig_path):
    if lon_range is None:
        lon_range = [-180, 180]
    if lat_range is None:
        lat_range = [-90, 90]
    projection = ccrs.PlateCarree()

    #-- transform radians to geodetic coordinates
    coords = projection.transform_points(
                            ccrs.Geodetic(),
                            np.rad2deg(var.clon.values),
                            np.rad2deg(var.clat.values))

    #-- create data frame of the variable values and the geodetic coordinates.
    df = pd.DataFrame({'val': var.values, 'x': coords[:,0], 'y': coords[:,1]})
    fig, ax = plt.subplots(figsize=(5,5), subplot_kw={"projection": projection})
    fig.canvas.draw_idle()  # necessary to make things work
    # change color of land

    # gl = ax.gridlines(crs=projection,
    #                 #   draw_labels=True,
    #                   color='black',
    #                   linewidth=0.5,
    #                   alpha=0.7,
    #                   x_inline=False)
    # gl.xlocator   = mticker.FixedLocator(range(-180, 180+1, 60))
    # gl.xformatter = LONGITUDE_FORMATTER

    df = df[(df['x']>lon_range[0]) & (df['x']<lon_range[1]) & 
            (df['y']>lat_range[0]) & (df['y']<lat_range[1])]

    artist = dsshow(df          = df,
                    glyph       = DS.Point('x', 'y'),
                    aggregator  = DS.mean('val'),
                    vmin        = vmin,
                    vmax        = vmax,
                    cmap        = colormap,
                    plot_width  = 200,
                    plot_height = 100,
                    ax          = ax)

    ax.add_feature(cfeature.LAND, facecolor='lightgrey', zorder=10)
    ax.add_feature(cfeature.COASTLINE, linewidth=0.8, zorder=11)
    # extent = [lon_range[0], lon_range[1], lat_range[0], lat_range[1]]
    # ax.set_extent(extent)
    # add longitude and latitude labels
    ax.set_xticks(np.arange(lon_range[0], lon_range[1], 4), crs=projection)
    ax.set_yticks(np.arange(lat_range[0], lat_range[1], 4), crs=projection)
    ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore

    # apply the default formatter to the ticks
    ax.xaxis.set_major_formatter(LONGITUDE_FORMATTER)
    ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)

    if savefig:
        plt.savefig(f'{fig_path}/test.png', dpi=250)
    return fig, ax

# %%
lon_reg = None
lat_reg = None
lon_reg = [45,90]
lat_reg = [0,30]
lon_reg = [70,80]
lat_reg = [5,10]

fig, ax = plot_datashader_proj(ds_sel, lon_reg, lat_reg, 20, 30, 'plasma', savefig, fig_path)
# %%
fig, ax = basic_DS_plot(ds_sel, lon_reg, lat_reg, vminmax=[20, 30], cmap='plasma')
# %%


########################################################################

# Very fast plotting on projected maps with Pandas and Datashader


def transform_coords(data, projection):
    """Projects coordinates of the dataset into the desired map projection.

    Expects data.lon to be the longitudes and data.lat to be the latitudes

    """
    lon = data.lon
    lat = data.lat
    if "rad" in data.lon.units:  # icon comes in radian, ifs in degree.
        lon = np.rad2deg(data.lon)
        lat = np.rad2deg(data.lat)
    coords = (
        projection.transform_points(  # This re-projects our data to the desired grid.
            ccrs.Geodetic(), lon, lat
        )
    )
    return coords


def plot_map(
    data,
    projection,
    # coords=None,  # we can compute them ourselves, if given data with lat and lon attached.
    colorbar_label="",
    title="",
    coastlines=True,
    extent=None,
    **kwargs,
):
    """Use datashader to plot a dataset from a pandas dataframe with a given projection.

    Required: data and projection.
    All other arguments are optional. Additional arguments will be passed directly to the plot routine (dsshow).
    """

    # If we are not given projected data, we project it ourselves.
    # if coords is None:
        # coords = projection.transform_points(ccrs.Geodetic(), data.lon, data.lat)
    coords = projection.transform_points(
                            ccrs.Geodetic(),
                            np.rad2deg(data.clon.values),
                            np.rad2deg(data.clat.values))
    # For datashader, we need the data in a pandas dataframe object.
    df = pd.DataFrame(
        data={
            "val": np.squeeze(data),  # here we select which data to plot! - the squeeze removes empty dimensions.
            "x": coords[:, 0],  # from the projection above
            "y": coords[:, 1],  # from the projection above
        }
    )

    # the basis for map plotting.
    fig, ax = plt.subplots( subplot_kw={"projection": projection})
    fig.canvas.draw_idle()  # necessary to make things work

    # the plot itself
    artist = dsshow(df, DS.Point("x", "y"), DS.mean("val"), ax=ax, **kwargs)

    # making things pretty
    plt.title(title)
    if coastlines:
        ax.coastlines(color="grey")
    fig.colorbar(artist, label=colorbar_label)

    # for regional plots:
    if extent is not None:
        ax.set_extent(extent)

    return fig
# %%
projection = (
    ccrs.PlateCarree()
)  # We will plot in mollweide projection - needs to match the plot calls later.

# %%
fig = plot_map(
      data           = ds_sel,                              # our data for the plot
      projection     = projection,                          # generated in the cell above.
      # coords         = coords,                              # generated in the cell above.
      cmap           = 'plasma',                            # nice colorbar for temperature
      vmin           = 12,                                  # minimum for the plot ~-33C
      vmax           = 31,                                  # maximum for the plot ~37C
      colorbar_label = "near-surface air temperature in K",
      title          = f"1",                                # dataset name and time stamp.
    #   extent         = [-90,-45,25,70]
      extent         = [70,80,5,10]

)
# %% plot on native grid
import matplotlib 

def calc_triangulation_obj(data, ds_grid, lon_reg, lat_reg):
    """create triangulation object for highres plot
    Example:
    pyic.shade(Tri, data_reg...)
    Todo: make it a full xarray calculation
    """ 
    print('Deriving triangulation object, this can take a while...')
    clon = np.rad2deg(ds_grid.clon.values)
    clat = np.rad2deg(ds_grid.clat.values)
    vlon = np.rad2deg(ds_grid.vlon.values)
    vlat = np.rad2deg(ds_grid.vlat.values)
    vertex_of_cell = ds_grid.vertex_of_cell.drop('cell_sea_land_mask').values.transpose()-1

    # f = Dataset('/home/m/m300602/work/icon/grids/smtwv_oce_2022/smtwv_oce_2022_tgrid.nc', 'r')
    # clon = f.variables['clon'][:] * 180./np.pi
    # clat = f.variables['clat'][:] * 180./np.pi
    # vlon = f.variables['vlon'][:] * 180./np.pi
    # vlat = f.variables['vlat'][:] * 180./np.pi
    # vertex_of_cell = f.variables['vertex_of_cell'][:].transpose()-1
    # f.close()

    ind_reg = np.where(   (clon>lon_reg[0])
                        & (clon<=lon_reg[1])
                        & (clat>lat_reg[0])
                        & (clat<=lat_reg[1]) )[0]
    vertex_of_cell_reg = vertex_of_cell[ind_reg,:]
    Tri = matplotlib.tri.Triangulation(vlon, vlat, triangles=vertex_of_cell_reg)
    data_reg = data.compute().data[ind_reg]
    print('Done deriving triangulation object.')
    return Tri, data_reg
# %%

Tri, data_reg = calc_triangulation_obj(ds_sel, ds_grid, lon_reg, lat_reg)
# %%
# plot on native grid
fig, ax = plt.subplots()
ax.tricontourf(Tri, data_reg, cmap='plasma')


# %%
import easygems.healpix as egh
cat = intake.open_catalog("https://data.nextgems-h2020.eu/catalog.yaml")
experiment = cat.ICON["ngc3028"]
# %%
def worldmap(var, **kwargs):
    projection = ccrs.Robinson(central_longitude=-135.5808361)
    fig, ax = plt.subplots(
        figsize=(8, 4), subplot_kw={"projection": projection}, constrained_layout=True
    )
    ax.set_global()

    egh.healpix_show(var, ax=ax, **kwargs)
    ax.add_feature(cf.COASTLINE, linewidth=0.8)
    ax.add_feature(cf.BORDERS, linewidth=0.4)
# %%
import healpy as hp
# %%
# %%
import pyicon as pyic
pyic.hp_to_rectgrid()

import datashader as dshader
# %%
def dshader_plot_mp(var, lon_range, lat_range, vminmax=None, cmap='plasma'):
    projection = ccrs.PlateCarree()
    if lon_range is None:
        lon_range = [-180, 180]
    if lat_range is None:
        lat_range = [-90, 90]
        
    fig, ax = plt.subplots(figsize=(10,5), subplot_kw={'projection': projection})
    fig.canvas.draw_idle()  # necessary to make things work
    # canvas = Canvas(plot_width=2000, plot_height=1000)


    #-- transform radians to geodetic coordinates
    coords = projection.transform_points(
                            ccrs.Geodetic(),
                            np.rad2deg(var.clon.values),
                            np.rad2deg(var.clat.values))
    # Get the data in a dataframe to use dasher plot
    df = pd.DataFrame(data={
        "var": np.squeeze(var.values),
        "x": coords[:,0], 
        "y": coords[:,1],
    },)
    
    df = df[(df['x']>lon_range[0]) & (df['x']<lon_range[1]) & 
            (df['y']>lat_range[0]) & (df['y']<lat_range[1])]
    
    # artist = dsshow(df, dshader.Point('x', 'y'), dshader.mean('var'), cmap=cmap, vmax=vminmax[1], vmin=vminmax[0], ax=ax)
    artist = dsshow(df          = df,
                    glyph       = dshader.Point('x', 'y'),
                    aggregator  = dshader.mean('var'),
                    vmin        = vminmax[0],
                    vmax        = vminmax[1],
                    cmap        = cmap,
                    aspect      = 'auto',
                    shade_hook  = lambda img: dshader.transfer_functions.dynspread(img, threshold=1, max_px=5, shape='square'),
                    # plot_width  = 600,
                    # plot_width  = 200,
                    # plot_height = 100,
                    ax          = ax)
    
    # extent = [lon_range[0], lon_range[1], lat_range[0], lat_range[1]]
    # ax.set_extent(extent)
    ax.add_feature(cfeature.LAND, facecolor='lightgrey', zorder=5)
    ax.add_feature(cfeature.COASTLINE, linewidth=0.8, zorder=6)
    ax.add_feature(cfeature.BORDERS, linewidth=0.5, zorder=7)
    # add longitude and latitude labels
    ax.set_xticks(np.arange(lon_range[0], lon_range[1], 4), crs=projection)
    ax.set_yticks(np.arange(lat_range[0], lat_range[1], 4), crs=projection)
    ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore

    # apply the default formatter to the ticks
    ax.xaxis.set_major_formatter(LONGITUDE_FORMATTER)
    ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)
    return fig, ax