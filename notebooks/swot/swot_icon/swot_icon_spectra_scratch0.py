# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp
# import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
import xrft
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

import numpy as np
from scipy.spatial import cKDTree
import xarray as xr
import pandas as pd
# %% ############### FUnctions ############################
def compute_spectra_uchida(data, samp_freq=250):
    data['num_lines'] = np.arange(int(data.num_lines.size))*samp_freq
    P = xrft.power_spectrum(data, dim=['num_lines'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
    P = P.isel(freq_num_lines=slice(len(P.freq_num_lines)//2,None)) * 2
    return P

# %% ################################START MULTIPLE SWATHS ANALYSIS################################
path_fig = "/home/m/m300878/submesoscaletelescope/results/smt_natl/swot/l3/"
savefig  = False
path     = '/work/mh0033/m300878/observation/satellite/SWOT/NA/SWOT_L3_LR_SSH_1.0/'
files    = glob.glob(path + '*.nc')
# Domain
lon_reg = [-60,-40]
lat_reg = [10, 45]


# %% #################### ICON SPECTRA ############################
ds_tgrid = eva.load_smt_grid()
ds_ssh = eva.load_smt_ssh()

# %%

def spherical_to_cartesian(lon, lat):
  earth_radius = 6371e3
  x = earth_radius * np.cos(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  y = earth_radius * np.sin(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  z = earth_radius * np.sin(lat*np.pi/180.)
  return x, y, z

# %% now get whole swath width
num_lines = ds_swot.num_lines.data.flatten()
num_pixels = ds_swot.num_pixels.data.flatten()
lon_o = ds_swot.longitude.data.flatten()
lat_o = ds_swot.latitude.data.flatten()

# %%
def interp_to_reg_grid(lon_i, lat_i, lon_o, lat_o, path):
    xi, yi, zi = spherical_to_cartesian(lon_i, lat_i)
    xo, yo, zo = spherical_to_cartesian(lon_o, lat_o)

    lzip_i = np.concatenate((xi[:,np.newaxis],yi[:,np.newaxis],zi[:,np.newaxis]), axis=1)
    lzip_o = np.concatenate((xo[:,np.newaxis],yo[:,np.newaxis],zo[:,np.newaxis]), axis=1) 
    tree   = cKDTree(lzip_i)
    dckdtree, ickdtree = tree.query(lzip_o , k=1, workers=-1)
    # dckdtree, ickdtree = tree.query(lzip_o , k=n_nearest_neighbours)

    Dind_dist = dict()
    Dind_dist['dckdtree_c'] = dckdtree
    Dind_dist['ickdtree_c'] = ickdtree

    ds_grid = xr.Dataset(coords=dict(longitude=ds_swot.longitude, latitude=ds_swot.latitude))
    for var in Dind_dist.keys(): 
        ds_grid[var] = xr.DataArray(Dind_dist[var].reshape(num_lines.size, num_pixels.size), dims=['num_lines', 'num_pixels'])

    return ds_grid
# %%
%%time
ds_grid = interp_to_reg_grid(clon, clat, lon_o, lat_o, path='reg_grids')
# %%
def apply_grid(ds, ds_grid):
    arr_interp = ds.isel(ncells=ds_grid.ickdtree_c.compute().data.flatten())
    arr_interp = arr_interp.assign_coords(ncells=pd.MultiIndex.from_product([num_lines, num_pixels], names=("num_lines", "num_pixels"))).unstack()
    arr_interp = arr_interp.drop('num_lines').drop('num_pixels')
    arr_interp = arr_interp.assign_coords(latitude=ds_grid.latitude, longitude=ds_grid.longitude)
    return arr_interp
# %%
ds_icon_swath = apply_grid(ds_sel, ds_grid)
# %% map with projection
fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection=ccrs.PlateCarree())
ax.coastlines()

# plot swath
plt.scatter(ds_swot.longitude, ds_swot.latitude, s=0.1, c='r', transform=ccrs.PlateCarree())
plt.scatter(ds_icon_swath.longitude, ds_icon_swath.latitude, s=0.00001, c='b', transform=ccrs.PlateCarree())
# %%
plt.figure()
for i in range(ds_swot.num_pixels.size)[::10]:
    ds_icon_swath.isel(num_pixels=i).h_sp.plot(color='b')
    (ds_swot.ssha+ds_swot.mdt).isel(num_pixels=i).plot()


# %%######################## ICON and SWOT SPectra ############################
# do mean over all relevant swaths
clon = np.rad2deg(ds_tgrid.clon.values)
clat = np.rad2deg(ds_tgrid.clat.values)

P_swot      = []
ds_swot_map = []
P_icon      = []
ds_icon_map = []

n=0
for i in range(len(files)):
    #select and prep SWOT
    ds_swot = xr.open_dataset(files[i])
    ds_swot_time = ds_swot.time.mean().values
    ds_swot = ds_swot.ssha + ds_swot.mdt
    ds_swot['longitude'] = xr.where(ds_swot.longitude > 180, ds_swot.longitude - 360, ds_swot.longitude)
    #check if in region
    if not (ds_swot.longitude > lon_reg[0]).any() or not (ds_swot.longitude < lon_reg[1]).any() or not (ds_swot.latitude > lat_reg[0]).any() or not (ds_swot.latitude < lat_reg[1]).any():
        print('no point in domain')
        continue
    #check if swath is in region and
    if not ((ds_swot.longitude > lon_reg[0]) & (ds_swot.longitude < lon_reg[1]) & (ds_swot.latitude > lat_reg[0]) & (ds_swot.latitude < lat_reg[1]) ).any():
        print('no swath in region')
        continue

    ds_swot_sel = ds_swot.where((ds_swot.longitude > lon_reg[0]) & (ds_swot.longitude < lon_reg[1]) & (ds_swot.latitude > lat_reg[0]) & (ds_swot.latitude < lat_reg[1]), drop=True)

    print(ds_swot_sel.shape)
    if ds_swot_sel.shape[0] != 1990: #1431:
        continue
    n+=1
    print('swath number', i, 'swaths included', n)
    ds_swot_map.append(ds_swot_sel)
    ds_swot_sel = ds_swot_sel.interpolate_na(dim='num_lines', method="linear", fill_value="extrapolate")
    ds_swot_sel = ds_swot_sel.dropna('num_pixels', how='all')
    ds_swot_sel = ds_swot_sel.drop(['latitude', 'longitude'])
    # ds0_sel = ds0_sel.drop(['latitude_nadir', 'longitude_nadir'])

    #select ICON
    print('select and interpolate ICON data')
    time_mean_ = ds_swot_time - pd.DateOffset(years=14)
    ds_icon    = ds_ssh.sel(time=time_mean_, method='nearest').h_sp

    # interpolate to satellite track
    num_lines  = ds_swot.num_lines.data.flatten()
    num_pixels = ds_swot.num_pixels.data.flatten()
    lon_o      = ds_swot.longitude.data.flatten()
    lat_o      = ds_swot.latitude.data.flatten()

    ds_grid       = interp_to_reg_grid(clon, clat, lon_o, lat_o, path='reg_grids')
    ds_icon_swath = apply_grid(ds_icon, ds_grid)
    ds_icon_map.append(ds_icon_swath)
    ds_icon_swath = ds_icon_swath.drop(['latitude', 'longitude'])

    #compute spectra
    print('compute spectra')
    p_swot = compute_spectra_uchida(ds_swot_sel)
    p_swot_mean = p_swot.mean(dim='num_pixels')
    P_swot.append(p_swot_mean)

    p_icon = compute_spectra_uchida(ds_icon_swath)
    p_icon_mean = p_icon.mean(dim='num_pixels')
    P_icon.append(p_icon_mean)

P_swot      = xr.concat(P_swot, dim='swath')
ds_swot_map = xr.concat(ds_swot_map, dim='swath')
P_icon      = xr.concat(P_icon, dim='swath')
ds_icon_map = xr.concat(ds_icon_map, dim='swath')

# %%
plt.figure(figsize=(10, 8))
plt.loglog(P_swot.freq_num_lines, P_swot.mean(dim='swath').values, color = 'red', label='swot')
plt.loglog(P_icon.freq_num_lines, P_icon.mean(dim='swath').values, color = 'blue', label='icon')
plt.legend()
# %%
