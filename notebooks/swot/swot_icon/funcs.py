# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
import xrft
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import cmocean
import numpy as np
from scipy.spatial import cKDTree
import xarray as xr
import pandas as pd
import matplotlib.gridspec as gridspec

# % ############### FUnctions ############################
def compute_spectra_uchida(data, samp_freq=2000):
    data['num_lines'] = np.arange(int(data.num_lines.size))*samp_freq
    P = xrft.power_spectrum(data, dim=['num_lines'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
    P = P.isel(freq_num_lines=slice(len(P.freq_num_lines)//2,None)) * 2
    return P

def spherical_to_cartesian(lon, lat):
  earth_radius = 6371e3
  x = earth_radius * np.cos(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  y = earth_radius * np.sin(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  z = earth_radius * np.sin(lat*np.pi/180.)
  return x, y, z

def interp_to_reg_grid(lon_i, lat_i, lon_o, lat_o, ds_orig):
    xi, yi, zi = spherical_to_cartesian(lon_i, lat_i)
    xo, yo, zo = spherical_to_cartesian(lon_o, lat_o)

    lzip_i = np.concatenate((xi[:,np.newaxis],yi[:,np.newaxis],zi[:,np.newaxis]), axis=1)
    lzip_o = np.concatenate((xo[:,np.newaxis],yo[:,np.newaxis],zo[:,np.newaxis]), axis=1) 
    tree   = cKDTree(lzip_i)
    dckdtree, ickdtree = tree.query(lzip_o , k=1, workers=-1)
    # dckdtree, ickdtree = tree.query(lzip_o , k=n_nearest_neighbours)

    Dind_dist = dict()
    Dind_dist['dckdtree_c'] = dckdtree
    Dind_dist['ickdtree_c'] = ickdtree

    ds_grid = xr.Dataset(coords=dict(longitude=ds_orig.longitude, latitude=ds_orig.latitude))
    for var in Dind_dist.keys(): 
        ds_grid[var] = xr.DataArray(Dind_dist[var].reshape(ds_orig.num_lines.size, ds_orig.num_pixels.size), dims=['num_lines', 'num_pixels'])

    return ds_grid

def apply_grid(ds, ds_grid):
    arr_interp = ds.isel(ncells=ds_grid.ickdtree_c.compute().data.flatten())
    arr_interp = arr_interp.assign_coords(ncells=pd.MultiIndex.from_product([ds_grid.num_lines.data, ds_grid.num_pixels.data], names=("num_lines", "num_pixels"))).unstack()
    arr_interp = arr_interp.drop('num_lines').drop('num_pixels')
    arr_interp = arr_interp.assign_coords(latitude=ds_grid.latitude, longitude=ds_grid.longitude)
    return arr_interp

def expand_num_lines(ds_swot_sel, fac=2):
    print('expanding sampling frequency')
    #example
    # plt.figure()
    # plt.plot(ds_swot_sel_new.longitude.isel(num_pixels=10)[::100], ds_swot_sel_new.latitude.isel(num_pixels=10)[::100], 'o')
    # plt.plot(ds_swot_sel.longitude.isel(num_pixels=10)[::100], ds_swot_sel.latitude.isel(num_pixels=10)[::100], 'x')
    
    # double number of lines
    num_pixels = ds_swot_sel.dims['num_pixels']
    num_lines = ds_swot_sel.dims['num_lines']

    # Initialize arrays to store the results
    LON_all = np.zeros((fac*num_lines, num_pixels)) * np.nan
    LAT_all = np.zeros((fac*num_lines, num_pixels)) * np.nan

    for pixel in range(num_pixels):
        lon = ds_swot_sel.longitude.isel(num_pixels=pixel).values
        lat = ds_swot_sel.latitude.isel(num_pixels=pixel).values

        LON = np.zeros(fac*len(lon)) * np.nan
        LAT = np.zeros(fac*len(lat)) * np.nan

        for i in range(fac*len(lon)):
            if i % 2:
                LON[i] = lon[int(i/2)]
                LAT[i] = lat[int(i/2)]

        LON = pd.Series(LON).interpolate()
        LAT = pd.Series(LAT).interpolate()

        LON_all[:, pixel] = LON
        LAT_all[:, pixel] = LAT

    ds_swot_sel_new = xr.Dataset({'latitude': (['num_lines', 'num_pixels'], LAT_all[1:]), 'longitude': (['num_lines', 'num_pixels'], LON_all[1:])})
    return ds_swot_sel_new

def load_r2b8_ssh_mep():
    # data of R2B8 run of Mia, TKE mixing scheme  like smt model, diffrent spinup
    path_data  = '/work/mh0033/m300878/run_icon/icon-oes-1.3.01_mia/experiments/exp.ocean_era51h_r2b8_mep23214-ERA/outdata/'
    search_str = f'exp.ocean_era51h_r2b8_mep23214-ERA_PT1H_zos_2010*' 
    # search_str = f'exp.ocean_era51h_r2b8_jse22126-ERA_data_P1D_2010*'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()
    r2b8 = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    r2b8 = r2b8.drop('clat').drop('clon')
    return(r2b8.zos)

def load_r2b9_ssh():
    path_data = '/work/mh0033/m300878/run_icon/icon-oes-zstar-r2b9/experiments/exp.ocean_era51h_zstar_r2b9_22255-ERA/outdata_2d/'
    search_str = f'exp.ocean_era51h_zstar_r2b9_22255-ERA_oce_2d_PT1H*'
    flist = np.array(glob.glob(path_data+search_str))
    flist.sort()
    r2b9 = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    r2b9 = r2b9.zos
    return(r2b9)

def swot_l3_spectra_inter(P_swot, P_icon, P_r2b8, savefig, path_fig):
    plt.figure(figsize=(10, 8))
    plt.loglog(P_swot.freq_num_lines, (P_swot['ssha'].mean(dim='swath').values+P_swot['mdt'].mean(dim='swath').values), label='adt=ssha+mdt', color='black')
    for var in P_swot.variables:
        if var in ['freq_num_lines']: continue
        plt.loglog(P_swot.freq_num_lines, P_swot[var].mean(dim='swath').values, label=var)
    
    plt.loglog(P_icon.freq_num_lines, P_icon.mean(dim='swath').values, color = 'blue', label='ICON-SMT')
    plt.loglog(P_r2b8.freq_num_lines, P_r2b8.mean(dim='swath').values, color = 'green', label='ICON-R2B8')
    plt.legend()
    # grid with more lones
    plt.grid(which='both')
    plt.xlabel('Wavenumber [1/m]')
    plt.ylabel('PSD [m^2/m]')
    
    if savefig == True: plt.savefig(path_fig+'spectra_inter_5.png', bbox_inches='tight')

def swot_l3_spectra_inter_sa(P_swot, P_icon, P_r2b9, savefig, path_fig):
    plt.figure(figsize=(10, 8))
    plt.loglog(P_swot.freq_num_lines, (P_swot['ssha'].mean(dim='swath').values+P_swot['mdt'].mean(dim='swath').values), label='adt=ssha+mdt', color='black')
    for var in P_swot.variables:
        if var in ['freq_num_lines']: continue
        plt.loglog(P_swot.freq_num_lines, P_swot[var].mean(dim='swath').values, label=var)
    
    plt.loglog(P_icon.freq_num_lines, P_icon.mean(dim='swath').values, color = 'blue', label='ICON-SMT')
    plt.loglog(P_r2b9.freq_num_lines, P_r2b9.mean(dim='swath').values, color = 'green', label='ICON-R2B9')
    plt.legend()
    # grid with more lones
    plt.grid(which='both')
    plt.xlabel('Wavenumber [1/m]')
    plt.ylabel('PSD [m^2/m]')
    
    if savefig == True: plt.savefig(path_fig+'spectra_inter_3.png', bbox_inches='tight')

def plot_swot_icon_spectra(P_swot, P_icon, P_r2b8, savefig, path_fig):
    P_r2b8_low = P_r2b8.where(P_r2b8.freq_num_lines < 1/(2*10000), drop=True)
    #figure with cloud of all swaths in red for swot and blue for icon
    fig = plt.figure(figsize=(10, 8))
    # add -2 slope
    f = P_swot.freq_num_lines
    plt.loglog(f, 1e-9*f**(-2), color='k', lw=1, linestyle='-')
    plt.text(6e-7, 2e3, r'$f^{-2}$', fontsize=13, rotation=0, va='bottom', ha='left')
    # and -3 slope
    plt.loglog(f, 1e-14*f**(-3), color='k', lw=1, linestyle='--')
    plt.text(6e-7, 4e4, r'$f^{-3}$', fontsize=13, rotation=0, va='bottom', ha='left')
    # and -11/3
    plt.loglog(f, 6e-18*f**(-11/3), color='k', lw=1, linestyle='-.')
    plt.text(6e-7, 3e5, r'$f^{-11/3}$', fontsize=13, rotation=0, va='bottom', ha='left')


    for i in range(P_icon.swath.size):
        plt.loglog(P_icon.freq_num_lines, P_icon.isel(swath=i), color='grey', alpha=0.08)
    plt.loglog(P_icon.freq_num_lines, P_icon.mean(dim='swath').values, color = 'tab:blue', label='ICON SMT',zorder=11, linewidth=2.6)
    for i in range(P_swot.swath.size):
        plt.loglog(P_r2b8_low.freq_num_lines, P_r2b8_low.isel(swath=i), color='mediumaquamarine', alpha=0.08)
    plt.loglog(P_r2b8_low.freq_num_lines, P_r2b8_low.mean(dim='swath').values, color = 'tab:green', label='ICON R2B8',zorder=10, linewidth=2.6)
    P_mdt = P_swot['mdt'].mean(dim='swath').values
    for i in range(P_swot.swath.size):
        plt.loglog(P_swot.freq_num_lines, P_mdt + P_swot['ssha_noiseless'].isel(swath=i), color='wheat', alpha=0.08)
    plt.loglog(P_swot.freq_num_lines, P_mdt + P_swot['ssha_noiseless'].mean(dim='swath').values, color = 'tab:red', label='SWOT mdt + ssha noiseless',zorder=9, linewidth=2.6)
    # plt.loglog(P_swot.freq_num_lines, P_swot['ssha'].mean(dim='swath').values, color = 'tab:red', label='SWOT ssha',zorder=9, alpha=0.5, linewidth=1.6)

    plt.legend()
    plt.grid(which='both', color='grey', linestyle='--', linewidth=0.1)
    plt.xlabel('Wavenumber [1/m]')
    plt.ylabel('PSD [m^2/m]')
    ax2 = plt.gca().secondary_xaxis('top', functions=(lambda x: 1/x, lambda x: 1/x))
    ax2.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax2.set_xlabel('Wavelength [m]')

    if savefig == True: plt.savefig(path_fig+'spectra_select_5.png', bbox_inches='tight')

def plot_swot_icon_spectra_sa(P_swot, P_icon, P_r2b9, savefig, path_fig):
    P_r2b9_low = P_r2b9.where(P_r2b9.freq_num_lines < 1/(2*5000), drop=True)
    #figure with cloud of all swaths in red for swot and blue for icon
    fig = plt.figure(figsize=(10, 8))
    # add -2 slope
    x = np.linspace(1e-6, 1e-4, 100)
    y = 1e-8*x**-2
    plt.loglog(x, y, color='black', linestyle='--', label='-2 slope', linewidth=0.8)
    # and -3 slope
    y = 1e-13*x**-3
    plt.loglog(x, y, color='black', linestyle=':', label='-3 slope', linewidth=0.8)

    for i in range(P_icon.swath.size):
        plt.loglog(P_icon.freq_num_lines, P_icon.isel(swath=i), color='grey', alpha=0.08)
    plt.loglog(P_icon.freq_num_lines, P_icon.mean(dim='swath').values, color = 'tab:blue', label='ICON SMT',zorder=11, linewidth=2.6)
    for i in range(P_swot.swath.size):
        plt.loglog(P_r2b9_low.freq_num_lines, P_r2b9_low.isel(swath=i), color='mediumaquamarine', alpha=0.08)
    plt.loglog(P_r2b9_low.freq_num_lines, P_r2b9_low.mean(dim='swath').values, color = 'tab:green', label='ICON R2B9',zorder=10, linewidth=2.6)
    P_mdt = P_swot['mdt'].mean(dim='swath').values
    for i in range(P_swot.swath.size):
        plt.loglog(P_swot.freq_num_lines, P_mdt + P_swot['ssha_noiseless'].isel(swath=i), color='wheat', alpha=0.08)
    plt.loglog(P_swot.freq_num_lines, P_mdt + P_swot['ssha_noiseless'].mean(dim='swath').values, color = 'tab:red', label='SWOT mdt + ssha noiseless',zorder=9, linewidth=2.6)
    plt.loglog(P_swot.freq_num_lines, P_mdt + P_swot['ssha'].mean(dim='swath').values, color = 'tab:red', label='SWOT mdt + ssha',zorder=9, alpha=0.5, linewidth=2.6)

    plt.legend()
    plt.grid(which='both', color='grey', linestyle='--', linewidth=0.1)
    plt.xlabel('Wavenumber [1/m]')
    plt.ylabel('PSD [m^2/m]')
    ax2 = plt.gca().secondary_xaxis('top', functions=(lambda x: 1/x, lambda x: 1/x))
    ax2.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax2.set_xlabel('Wavelength [m]')

    if savefig == True: plt.savefig(path_fig+'spectra_select_3_slope2.png', bbox_inches='tight')


def plot_map_spectra(ds_swot_map, P_swot, P_icon, P_r2b8, savefig, path_fig):
    lon_vis = [-80, -50]
    lat_vis = [15, 60]
    
    fig = plt.figure(figsize=(14, 7))
    gs = gridspec.GridSpec(1, 2, width_ratios=[1, 2])
    ax1 = plt.subplot(gs[0], projection=ccrs.PlateCarree())
    ax1.coastlines()
    ax1.add_feature(cartopy.feature.LAND, zorder=0, edgecolor='black', facecolor='lightgrey')
    for i in range(ds_swot_map.swath.size):
        cb = ax1.scatter(ds_swot_map.sel(swath=i).longitude, ds_swot_map.sel(swath=i).latitude, s=1, c=ds_swot_map.sel(swath=i), cmap='Spectral_r', transform=ccrs.PlateCarree(), vmin=-0.45, vmax=0.25)
    
    cbar = plt.colorbar(cb, ax=ax1, orientation='horizontal', label='SSH [m]', fraction=0.026, pad=0.06)
    
    ax1.set_extent([lon_vis[0], lon_vis[1], lat_vis[0], lat_vis[1]], ccrs.PlateCarree())
    ax1.set_xticks(np.arange(lon_vis[0], lon_vis[1], 10), crs=ccrs.PlateCarree())
    ax1.set_yticks(np.arange(lat_vis[0], lat_vis[1], 10), crs=ccrs.PlateCarree())
    ax1.set_title('SSH along SWOT tracks')
    #write (a) above upper left corner
    ax1.text(-0.10, 1.02, '('+string.ascii_lowercase[0]+')', transform=ax1.transAxes, size=10)


    
    ############### second figure ################
    ax2 = plt.subplot(gs[1])
    P_r2b8_low = P_r2b8.where(P_r2b8.freq_num_lines < 1/(2*10000), drop=True)
    
    f = P_swot.freq_num_lines
    ax2.loglog(f, 1e-9*f**(-2), color='k', lw=1, linestyle='-')
    ax2.text(6e-7, 2e3, r'${-2}$', fontsize=13, rotation=0, va='bottom', ha='left')
    # and -3 slope
    ax2.loglog(f, 1e-14*f**(-3), color='k', lw=1, linestyle='--')
    ax2.text(6e-7, 4e4, r'${-3}$', fontsize=13, rotation=0, va='bottom', ha='left')
    # and -11/3
    ax2.loglog(f, 6e-18*f**(-11/3), color='k', lw=1, linestyle='-.')
    ax2.text(6e-7, 3e5, r'${-11/3}$', fontsize=13, rotation=0, va='bottom', ha='left')
    ax2.text(-0.10, 1.02, '('+string.ascii_lowercase[1]+')', transform=ax2.transAxes, size=10)


    for i in range(P_icon.swath.size):
        ax2.loglog(P_icon.freq_num_lines, P_icon.isel(swath=i), color='grey', alpha=0.08)
    ax2.loglog(P_icon.freq_num_lines, P_icon.mean(dim='swath').values, color = 'tab:blue', label='ICON SMT',zorder=11, linewidth=2.6)
    for i in range(P_swot.swath.size):
        ax2.loglog(P_r2b8_low.freq_num_lines, P_r2b8_low.isel(swath=i), color='mediumaquamarine', alpha=0.08)
    ax2.loglog(P_r2b8_low.freq_num_lines, P_r2b8_low.mean(dim='swath').values, color = 'tab:green', label='ICON R2B8',zorder=10, linewidth=2.6)
    P_mdt = P_swot['mdt'].mean(dim='swath').values
    for i in range(P_swot.swath.size):
        ax2.loglog(P_swot.freq_num_lines, P_mdt + P_swot['ssha_noiseless'].isel(swath=i), color='wheat', alpha=0.08)
    ax2.loglog(P_swot.freq_num_lines, P_mdt + P_swot['ssha_noiseless'].mean(dim='swath').values, color = 'tab:red', label='SWOT mdt + ssha noiseless',zorder=9, linewidth=2.6)
    plt.loglog(P_swot.freq_num_lines, P_mdt + P_swot['ssha'].mean(dim='swath').values, color = 'tab:red', label='SWOT mdt + ssha',zorder=9, alpha=0.5, linewidth=2.6)
    plt.legend()
    plt.grid(which='both', color='grey', linestyle='--', linewidth=0.1)
    plt.xlabel('wavenumber [1/m]')
    plt.ylabel(r'PSD $[m^2/m]$')
    ax3 = plt.gca().secondary_xaxis('top', functions=(lambda x: 1/x/1000, lambda x: 1/x/1000))
    ax3.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax3.set_xlabel('wavelength [km]')
    ax2.autoscale(enable=True, axis='both', tight=True)

    for tick in ax2.xaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    for tick in ax1.yaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    for tick in ax1.yaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    for tick in ax1.xaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    if savefig == True: plt.savefig(path_fig+'swot_map_spectra.png', bbox_inches='tight', dpi=300)
    

def plot_map_spectra_sa(ds_swot_map, P_swot, P_icon, P_r2b9, savefig, path_fig):
    lon_vis = [-15, 15]
    lat_vis = [-50, -10]
    
    fig = plt.figure(figsize=(14, 7))
    gs = gridspec.GridSpec(1, 2, width_ratios=[1, 2])
    ax1 = plt.subplot(gs[0], projection=ccrs.PlateCarree())
    ax1.coastlines()
    ax1.add_feature(cartopy.feature.LAND, zorder=0, edgecolor='black', facecolor='lightgrey')
    for i in range(ds_swot_map.swath.size):
        cb = ax1.scatter(ds_swot_map.sel(swath=i).longitude, ds_swot_map.sel(swath=i).latitude, s=1, c=ds_swot_map.sel(swath=i), cmap='Spectral_r', transform=ccrs.PlateCarree(), vmin=-0.05, vmax=0.3)
    
    cbar = plt.colorbar(cb, ax=ax1, orientation='horizontal', label='SSH [m]', fraction=0.026, pad=0.06, extend='both')
    
    ax1.set_extent([lon_vis[0], lon_vis[1], lat_vis[0], lat_vis[1]], ccrs.PlateCarree())
    ax1.set_xticks(np.arange(lon_vis[0], lon_vis[1], 10), crs=ccrs.PlateCarree())
    ax1.set_yticks(np.arange(lat_vis[0], lat_vis[1], 10), crs=ccrs.PlateCarree())
    ax1.set_title('SWOT Tracks', fontsize=12)
    ax1.text(-0.10, 1.02, '('+string.ascii_lowercase[0]+')', transform=ax1.transAxes, size=10)
    
    ############### second figure ################
    ax2 = plt.subplot(gs[1])
    P_r2b9_low = P_r2b9.where(P_r2b9.freq_num_lines < 1/(2*5000), drop=True)
    
    f = P_swot.freq_num_lines
    plt.loglog(f, 1e-9*f**(-2), color='k', lw=1, linestyle='-')
    plt.text(6e-7, 2e3, r'$f^{-2}$', fontsize=13, rotation=0, va='bottom', ha='left')
    # and -3 slope
    plt.loglog(f, 1e-14*f**(-3), color='k', lw=1, linestyle='--')
    plt.text(6e-7, 4e4, r'$f^{-3}$', fontsize=13, rotation=0, va='bottom', ha='left')
    # and -11/3
    plt.loglog(f, 6e-18*f**(-11/3), color='k', lw=1, linestyle='-.')
    plt.text(6e-7, 3e5, r'$f^{-11/3}$', fontsize=13, rotation=0, va='bottom', ha='left')
    ax2.text(-0.10, 1.02, '('+string.ascii_lowercase[1]+')', transform=ax2.transAxes, size=10)

    for i in range(P_icon.swath.size):
        ax2.loglog(P_icon.freq_num_lines, P_icon.isel(swath=i), color='grey', alpha=0.08)
    ax2.loglog(P_icon.freq_num_lines, P_icon.mean(dim='swath').values, color = 'tab:blue', label='ICON-SMT-Wave',zorder=11, linewidth=2.6)
    for i in range(P_swot.swath.size):
        ax2.loglog(P_r2b9_low.freq_num_lines, P_r2b9_low.isel(swath=i), color='mediumaquamarine', alpha=0.08)
    ax2.loglog(P_r2b9_low.freq_num_lines, P_r2b9_low.mean(dim='swath').values, color = 'tab:green', label='ICON-R2B9-Wave',zorder=10, linewidth=2.6)
    P_mdt = P_swot['mdt'].mean(dim='swath').values
    for i in range(P_swot.swath.size):
        ax2.loglog(P_swot.freq_num_lines, P_mdt + P_swot['ssha_noiseless'].isel(swath=i), color='wheat', alpha=0.08)
    ax2.loglog(P_swot.freq_num_lines, P_mdt + P_swot['ssha_noiseless'].mean(dim='swath').values, color = 'tab:red', label='SWOT mdt + ssha noiseless',zorder=9, linewidth=2.6)
    plt.loglog(P_swot.freq_num_lines, P_mdt + P_swot['ssha'].mean(dim='swath').values, color = 'tab:red', label='SWOT mdt + ssha',zorder=9, alpha=0.5, linewidth=2.6)
    plt.legend()
    plt.grid(which='both', color='grey', linestyle='--', linewidth=0.1)
    plt.xlabel('wavenumber [1/m]', fontsize=12)
    plt.ylabel(r'$PSD [m^2/m]$')
    ax3 = plt.gca().secondary_xaxis('top', functions=(lambda x: 1/x/1000, lambda x: 1/x/1000))
    ax3.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax3.set_xlabel('wavelength [km]')


    #increase all ticks
    for tick in ax2.xaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    for tick in ax2.yaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    for tick in ax1.yaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    for tick in ax1.xaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    for tick in ax3.xaxis.get_major_ticks():
        tick.label.set_fontsize(12)


    
    if savefig == True: plt.savefig(path_fig+'swot_map_spectra_3.png', bbox_inches='tight', dpi=300)
    
    # %%
    