# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
import xrft
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import cmocean
import numpy as np
from scipy.spatial import cKDTree
import xarray as xr
import pandas as pd


# %% get cluster
reload(scluster)

# client, cluster = scluster.init_dask_slurm_cluster_gpu(walltime='01:00:00', wait=True)
client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)

cluster
client

# %% ################################START MULTIPLE SWATHS ANALYSIS################################
path_2_data  = '/work/mh0033/m300878/spectra/smt_wave/sa/old/'
path_fig     = "/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/swot/l3/icon_swot_swaths/"
savefig      = False
calc_spectra = False
lon_reg      = [-15,  15]
lat_reg      = [-45, -15]

# %%
if calc_spectra:
    # % load Swot DATA
    path     = '/work/mh0033/m300878/observation/satellite/SWOT/NA/SWOT_L3_LR_SSH_1.0/'
    files    = glob.glob(path + '*.nc')

    # % #################### ICON Data ############################
    ds_tgrid = eva.load_smt_wave_grid()
    ds       = eva.load_smt_wave_all_ext(variable='2d', exp=7,  remove_bnds=True, it=1)
    ds       = ds.drop_duplicates(dim='time')
    ds_ssh   = ds.zos
    clon     = np.rad2deg(ds_tgrid.clon.values)
    clat     = np.rad2deg(ds_tgrid.clat.values)

    ds_r2b9       = fu.load_r2b9_ssh()
    path_tgrid    = '/work/mh0033/m300602/icon/grids/r2b9_oce_r0004/r2b9_oce_r0004_tgrid.nc'
    ds_r2b9_tgrid = xr.open_dataset(path_tgrid)
    clon_r2b9     = np.rad2deg(ds_r2b9_tgrid.clon.values)
    clat_r2b9     = np.rad2deg(ds_r2b9_tgrid.clat.values)

    # %######################## ICON and SWOT SPectra ############################
    # do mean over all relevant swaths

    P_swot = {}
    ds_swot_map = []
    P_icon      = []
    ds_icon_map = []
    P_r2b9      = []
    ds_r2b9_map = []
    # %
    n=0
    for i in range(len(files)):
        ds_swot           = xr.open_dataset(files[i])
        ds_swot_time      = ds_swot.time.mean().values
        variables_to_keep = ['ssha', 'ssha_noiseless', 'ssha_unedited', 'mdt', 'ocean_tide', 'mss', 'dac', 'longitude', 'latitude']
        variables_to_drop = [var for var in ds_swot.variables if var not in variables_to_keep]
        ds_swot           = ds_swot.drop_vars(variables_to_drop)

        ds_swot['longitude'] = xr.where(ds_swot.longitude > 180, ds_swot.longitude - 360, ds_swot.longitude)
        #check if in region
        if not (ds_swot.longitude > lon_reg[0]).any() or not (ds_swot.longitude < lon_reg[1]).any() or not (ds_swot.latitude > lat_reg[0]).any() or not (ds_swot.latitude < lat_reg[1]).any():
            print('no point in domain')
            continue
        #check if swath is in region and
        if not ((ds_swot.longitude > lon_reg[0]) & (ds_swot.longitude < lon_reg[1]) & (ds_swot.latitude > lat_reg[0]) & (ds_swot.latitude < lat_reg[1]) ).any():
            print('no swath in region')
            continue

        ds_swot_sel = ds_swot.where((ds_swot.longitude > lon_reg[0]) & (ds_swot.longitude < lon_reg[1]) & (ds_swot.latitude > lat_reg[0]) & (ds_swot.latitude < lat_reg[1]), drop=True)

        print(ds_swot_sel.ssha.shape)
        max_num_lines = 1711
        if ds_swot_sel.ssha.shape[0] != max_num_lines:
            print('satellite track to short')
            continue

        ds_line = ds_swot_sel.ssha.isel(num_pixels=15)
        if ds_line.isnull().sum() > int(0.15*max_num_lines):
            print('more than 15% nans in alongtrack direction')
            continue

        n+=1
        print('swath number', i, 'total swaths used', n)
        ds_swot_map.append(ds_swot_sel.ssha)

        for var in variables_to_keep:
            if var in ['latitude', 'longitude']: continue
            ds_swot_sel2 = ds_swot_sel[var].interpolate_na(dim='num_lines', method="linear", fill_value="extrapolate")
            ds_swot_sel2 = ds_swot_sel2.dropna('num_pixels', how='all')
            ds_swot_sel2 = ds_swot_sel2.drop(['latitude', 'longitude'])

            p_swot      = fu.compute_spectra_uchida(ds_swot_sel2)
            p_swot_mean = p_swot.mean(dim='num_pixels')
            if var not in P_swot:
                P_swot[var] = []

            P_swot[var].append(p_swot_mean)

        #select ICON
        time_mean_ = ds_swot_time - pd.DateOffset(years=4)
        num_lines  = ds_swot_sel.num_lines.data.flatten()
        num_pixels = ds_swot_sel.num_pixels.data.flatten()
        lon_o      = ds_swot_sel.longitude.data.flatten()
        lat_o      = ds_swot_sel.latitude.data.flatten()

        print('select and interpolate ICON SMT data')
        ds_icon       = ds_ssh.sel(time=time_mean_, method='nearest')
        ds_grid       = fu.interp_to_reg_grid(clon, clat, lon_o, lat_o, ds_swot_sel)
        ds_icon_swath = fu.apply_grid(ds_icon, ds_grid)
        ds_icon_map.append(ds_icon_swath)
        ds_icon_swath = ds_icon_swath.drop(['latitude', 'longitude'])

        p_icon      = fu.compute_spectra_uchida(ds_icon_swath)
        p_icon_mean = p_icon.mean(dim='num_pixels')
        P_icon.append(p_icon_mean)

        print('select and interpolate ICON r2b9 data')
        num_lines  = ds_swot_sel.num_lines[::3].data.flatten()
        lon_o      = ds_swot_sel.longitude[::3].data.flatten()
        lat_o      = ds_swot_sel.latitude[::3].data.flatten()

        ds_icon_r2b9       = ds_r2b9.sel(time=time_mean_, method='nearest')
        ds_grid_r2b9       = fu.interp_to_reg_grid(clon_r2b9, clat_r2b9, lon_o, lat_o, ds_swot_sel.isel(num_lines=slice(0, None, 3)))
        ds_icon_r2b9_swath = fu.apply_grid(ds_icon_r2b9, ds_grid_r2b9)
        ds_r2b9_map.append(ds_icon_r2b9_swath)
        ds_icon_r2b9_swath = ds_icon_r2b9_swath.drop('longitude').drop('latitude')

        p_r2b9      = fu.compute_spectra_uchida(ds_icon_r2b9_swath, 6000) #subsampled to 6000m
        p_r2b9_mean = p_r2b9.mean(dim='num_pixels')
        P_r2b9.append(p_r2b9_mean)


    ds_swot_map = xr.concat(ds_swot_map, dim='swath')
    P_swot      = {key: xr.concat(val, dim='swath') for key, val in P_swot.items()}
    P_swot      = xr.Dataset(P_swot)

    P_icon      = xr.concat(P_icon, dim='swath')
    ds_icon_map = xr.concat(ds_icon_map, dim='swath')
    P_r2b9      = xr.concat(P_r2b9, dim='swath')
    ds_r2b9_map = xr.concat(ds_r2b9_map, dim='swath')

    # % save spectra
    P_swot.to_netcdf(path_2_data+'P_swot.nc')
    ds_swot_map.to_netcdf(path_2_data+'ds_swot_map.nc')
    P_icon.to_netcdf(path_2_data+'P_icon.nc')
    ds_icon_map.to_netcdf(path_2_data+'ds_icon_map.nc')
    P_r2b9.to_netcdf(path_2_data+'P_r2b9_3.nc')
    ds_r2b9_map.to_netcdf(path_2_data+'ds_r2b9_map_3.nc')
else:
    # %%
    P_swot      = xr.open_dataset(path_2_data+'P_swot.nc')
    ds_swot_map = xr.open_dataset(path_2_data+'ds_swot_map.nc')
    ds_swot_map = ds_swot_map.ssha
    P_icon      = xr.open_dataset(path_2_data+'P_icon.nc')
    P_r2b9      = xr.open_dataset(path_2_data+'P_r2b9_3.nc')
    ds_icon_map = xr.open_dataset(path_2_data+'ds_icon_map.nc')
    ds_icon_map = ds_icon_map.zos
    ds_r2b9_map = xr.open_dataset(path_2_data+'ds_r2b9_map_3.nc')

# %%
# P_swot = P_swot.drop_isel(swath=1)
# ds_swot_map = ds_swot_map.drop_isel(swath=1)

# %% compute
P_icon = P_icon.compute()
# rename variable in P_icon
P_icon = P_icon.to_array().squeeze()
P_swot = P_swot.compute()
P_r2b9 = P_r2b9.compute()
P_r2b9 = P_r2b9.to_array().squeeze()
ds_icon_map = ds_icon_map.compute()
ds_swot_map = ds_swot_map.compute()
ds_r2b9_map = ds_r2b9_map.compute()

# %% ####################### Visualization ############################
fu.swot_l3_spectra_inter_sa(P_swot, P_icon, P_r2b9, savefig, path_fig)
# %%
reload(fu)
fu.plot_swot_icon_spectra_sa(P_swot, P_icon, P_r2b9, savefig, path_fig)
# %%
reload(fu)
fu.plot_map_spectra_sa(ds_swot_map, P_swot, P_icon, P_r2b9, savefig, path_fig)


# %%
if False:
    #Distance range from 180 tp 185
    i=5
    P1 = ds_swot_map.isel(swath=i).isel(num_pixels=10).isel(num_lines=0)
    P2 = ds_swot_map.isel(swath=i).isel(num_pixels=10).isel(num_lines=1)
    # at end
    P1_end = ds_swot_map.isel(swath=i).isel(num_pixels=30).isel(num_lines=-2)
    P2_end = ds_swot_map.isel(swath=i).isel(num_pixels=30).isel(num_lines=-1)
    
    dist = np.sqrt((P1.longitude-P2.longitude)**2 + (P1.latitude-P2.latitude)**2)
    dist_end = np.sqrt((P1_end.longitude-P2_end.longitude)**2 + (P1_end.latitude-P2_end.latitude)**2)
    #and in meter at latitude
    dist_m = dist * eva.convert_degree_to_meter(P1.latitude.values-P2.latitude.values)
    dist_m_end = dist_end * eva.convert_degree_to_meter(P1_end.latitude.values-P2_end.latitude.values)
    print('dist in degree', dist.values, 'and in meter', dist_m.values)
    print('dist in degree at end', dist_end.values, 'and in meter at end', dist_m_end.values)

    # %% 
    reload(eva)
    #compute dist for all points
    i =30
    dist = np.sqrt((ds_swot_map.isel(swath=i).isel(num_pixels=10).longitude - ds_swot_map.isel(swath=i).isel(num_pixels=11).longitude)**2 + (ds_swot_map.isel(swath=i).isel(num_pixels=10).latitude - ds_swot_map.isel(swath=i).isel(num_pixels=11).latitude)**2)
    plt.figure()
    x = np.arange(dist.size)
    plt.plot(x, dist)
    # and in meter at corresopnding latitude
    plt.figure()
    dist_m = dist * eva.convert_degree_to_meter(ds_swot_map.isel(swath=i).isel(num_pixels=10).latitude.values)
    plt.plot(x, dist_m)

# %% 
if False:
    plt.figure()
    ds_swot_map.isel(swath=0).isel(num_pixels=10).plot()
    ds_icon_map.isel(swath=0).isel(num_pixels=10).plot()

    plt.figure()
    # detrend above linear trend
    a = ds_swot_map.isel(swath=5).isel(num_pixels=10).interpolate_na(dim='num_lines', method="linear", fill_value="extrapolate").data
    b = ds_icon_map.isel(swath=5).isel(num_pixels=10).data

    # a = a - np.polyval(np.polyfit(np.arange(a.size), a, 1), np.arange(a.size))
    # b = b - np.polyval(np.polyfit(np.arange(b.size), b, 1), np.arange(b.size))

    plt.plot(a)
    plt.plot(b)



# %%
if False: #simple check of swaths maps
    plt.figure()
    for i in range(ds_icon_map.swath.size)[::10]:
        plt.scatter(ds_icon_map.sel(swath=i).longitude, ds_icon_map.sel(swath=i).latitude, s=1, c=ds_icon_map.sel(swath=i), cmap=cmocean.cm.thermal)
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.colorbar()
    if savefig == True: plt.savefig(path_fig+'smt_map.png', bbox_inches='tight')
    # %%
    # plt.figure()
    # for i in range(ds_r2b9_map.swath.size):
    #     plt.scatter(ds_r2b9_map.sel(swath=i).lon, ds_r2b9_map.sel(swath=i).lat, s=1, c=ds_r2b9_map.sel(swath=i), cmap=cmocean.cm.thermal)
    # plt.xlabel('Longitude')
    # plt.ylabel('Latitude')
    # plt.colorbar()
    # if savefig == True: plt.savefig(path_fig+'r2b9_map.png', bbox_inches='tight')
    # %%
    plt.figure()
    for i in range(ds_swot_map.swath.size)[::10]:
        plt.scatter(ds_swot_map.sel(swath=i).longitude, ds_swot_map.sel(swath=i).latitude, s=1, c=ds_swot_map.sel(swath=i), cmap=cmocean.cm.thermal)
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.colorbar()
    if savefig == True: plt.savefig(path_fig+'swot_map.png', bbox_inches='tight')

    # %% above code using projection
    fig = plt.figure(figsize=(10, 10))
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.coastlines()
    for i in range(ds_swot_map.swath.size):
        plt.scatter(ds_swot_map.sel(swath=i).longitude, ds_swot_map.sel(swath=i).latitude, s=1, c=ds_swot_map.sel(swath=i), cmap=cmocean.cm.thermal, transform=ccrs.PlateCarree())

    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.colorbar()
    lon_vis = [-15, 25]
    lat_vis = [-45, -10]
    ax.set_extent([lon_vis[0], lon_vis[1], lat_vis[0], lat_vis[1]], ccrs.PlateCarree())
    # add ticks
    ax.set_xticks(np.arange(lon_vis[0], lon_vis[1], 10), crs=ccrs.PlateCarree())
    ax.set_yticks(np.arange(lat_vis[0], lat_vis[1], 10), crs=ccrs.PlateCarree())
    if savefig == True: plt.savefig(path_fig+'swot_map_large.png', bbox_inches='tight')

    #%% plot all realisations
    plt.figure(figsize=(10, 8))
    #iterate over swath spectra
    for i in range(P_icon.swath.size)[::]:
        plt.loglog(P_icon.freq_num_lines, P_icon.isel(swath=i), label=f'swath {i}')
    plt.legend()
    # %%
    # and for sowt
    plt.figure(figsize=(10, 8))
    #iterate over swath spectra
    for i in range(P_swot.swath.size)[::]:
        plt.loglog(P_swot.freq_num_lines, P_swot['ssha'].isel(swath=i), label=f'swath {i}')
    plt.legend()
    # %%
    plt.figure()
    plt.loglog(P_r2b9.freq_num_lines, P_r2b9.mean(dim='swath').values, color = 'tab:green', label='ICON r2b9',zorder=10)

# %%

# %%
