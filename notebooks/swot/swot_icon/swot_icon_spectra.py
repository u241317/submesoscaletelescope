# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
import xrft
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import cmocean
import numpy as np
from scipy.spatial import cKDTree
import xarray as xr
import pandas as pd
# %%
reload(scluster)

# client, cluster = scluster.init_dask_slurm_cluster_gpu(walltime='01:00:00', wait=True)
client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)

cluster
client

# %% ################################START MULTIPLE SWATHS ANALYSIS################################
path_2_data  = '/work/mh0033/m300878/spectra/smt_wave/na/study_area/oversampled/'
path_fig     = "/home/m/m300878/submesoscaletelescope/results/smt_natl/swot/l3/icon_swot_swaths/study_area/oversampled/"
savefig      = False
# lon_reg      = [-60,-40]
# lat_reg      = [10, 45]
lon_reg      = [-75,-50]
lat_reg      = [20, 40]

calc_spectra = True

if calc_spectra:
    # % load Swot DATA
    path     = '/work/mh0033/m300878/observation/satellite/SWOT/NA/SWOT_L3_LR_SSH_1.0/'
    files    = glob.glob(path + '*.nc')

    # % #################### ICON Data ############################
    ds_tgrid = eva.load_smt_grid()
    ds_ssh   = eva.load_smt_ssh()
    clon     = np.rad2deg(ds_tgrid.clon.values)
    clat     = np.rad2deg(ds_tgrid.clat.values)

    ds_r2b8       = fu.load_r2b8_ssh_mep()
    path_tgrid    = '/work/mh0033/m300602/icon/grids/r2b8_oce_r0004/r2b8_oce_r0004_tgrid.nc'
    ds_r2b8_tgrid = xr.open_dataset(path_tgrid)
    clon_r2b8     = np.rad2deg(ds_r2b8_tgrid.clon.values)
    clat_r2b8     = np.rad2deg(ds_r2b8_tgrid.clat.values)

    # %######################## ICON and SWOT SPectra ############################
    # do mean over all relevant swaths

    P_swot = {}

    ds_swot_map = []
    P_icon      = []
    ds_icon_map = []
    P_r2b8      = []
    ds_r2b8_map = []

    n=0
    for i in range(len(files)):
        ds_swot = xr.open_dataset(files[i])
        ds_swot_time = ds_swot.time.mean().values
        variables_to_keep = ['ssha', 'ssha_noiseless', 'ssha_unedited', 'mdt', 'ocean_tide', 'mss', 'dac', 'longitude', 'latitude']
        variables_to_drop = [var for var in ds_swot.variables if var not in variables_to_keep]
        ds_swot = ds_swot.drop_vars(variables_to_drop)

        ds_swot['longitude'] = xr.where(ds_swot.longitude > 180, ds_swot.longitude - 360, ds_swot.longitude)
        #check if in region
        if not (ds_swot.longitude > lon_reg[0]).any() or not (ds_swot.longitude < lon_reg[1]).any() or not (ds_swot.latitude > lat_reg[0]).any() or not (ds_swot.latitude < lat_reg[1]).any():
            print('no point in domain')
            continue
        #check if swath is in region and
        if not ((ds_swot.longitude > lon_reg[0]) & (ds_swot.longitude < lon_reg[1]) & (ds_swot.latitude > lat_reg[0]) & (ds_swot.latitude < lat_reg[1]) ).any():
            print('no swath in region')
            continue

        ds_swot_sel = ds_swot.where((ds_swot.longitude > lon_reg[0]) & (ds_swot.longitude < lon_reg[1]) & (ds_swot.latitude > lat_reg[0]) & (ds_swot.latitude < lat_reg[1]), drop=True)

        print(ds_swot_sel.ssha.shape)
        max_num_lines = 1144#1990
        if ds_swot_sel.ssha.shape[0] != max_num_lines: #1431:
            continue

        ds_line = ds_swot_sel.ssha.isel(num_pixels=15)
        if ds_line.isnull().sum() > int(0.15*max_num_lines):
            print('more than 15% nans in alongtrack direction')
            continue

        n+=1
        print('swath number', i, 'total swaths used', n)
        ds_swot_map.append(ds_swot_sel.ssha)

        for var in variables_to_keep:
            if var in ['latitude', 'longitude']: continue
            ds_swot_sel2 = ds_swot_sel[var].interpolate_na(dim='num_lines', method="linear", fill_value="extrapolate")
            ds_swot_sel2 = ds_swot_sel2.dropna('num_pixels', how='all')
            ds_swot_sel2 = ds_swot_sel2.drop(['latitude', 'longitude'])

            p_swot      = fu.compute_spectra_uchida(ds_swot_sel2)
            p_swot_mean = p_swot.mean(dim='num_pixels')
            if var not in P_swot:
                P_swot[var] = []

            P_swot[var].append(p_swot_mean)

        #select ICON
        time_mean_ = ds_swot_time - pd.DateOffset(years=14)
        if True:
            fac = 2
            ds_swot_sel_smt = fu.expand_num_lines(ds_swot_sel, fac)
        else:
            fac = 1
            ds_swot_sel_smt = ds_swot_sel

        num_lines  = ds_swot_sel_smt.num_lines.data.flatten()
        num_pixels = ds_swot_sel_smt.num_pixels.data.flatten()
        lon_o      = ds_swot_sel_smt.longitude.data.flatten()
        lat_o      = ds_swot_sel_smt.latitude.data.flatten()

        print('select and interpolate ICON SMT data')
        ds_icon       = ds_ssh.sel(time=time_mean_, method='nearest').h_sp
        ds_grid       = fu.interp_to_reg_grid(clon, clat, lon_o, lat_o, ds_swot_sel_smt)
        ds_icon_swath = fu.apply_grid(ds_icon, ds_grid)
        ds_icon_map.append(ds_icon_swath)
        ds_icon_swath = ds_icon_swath.drop(['latitude', 'longitude'])

        p_icon      = fu.compute_spectra_uchida(ds_icon_swath, int(2000/fac))
        p_icon_mean = p_icon.mean(dim='num_pixels')
        P_icon.append(p_icon_mean)

        print('select and interpolate ICON R2B8 data')
        sub       = 5
        num_lines = ds_swot_sel.num_lines[::sub].data.flatten()
        lon_o     = ds_swot_sel.longitude[::sub].data.flatten()
        lat_o     = ds_swot_sel.latitude[::sub].data.flatten()

        ds_icon_r2b8       = ds_r2b8.sel(time=time_mean_, method='nearest')
        ds_grid_r2b8       = fu.interp_to_reg_grid(clon_r2b8, clat_r2b8, lon_o, lat_o, ds_swot_sel.isel(num_lines=slice(0, None, sub)))
        ds_icon_r2b8_swath = fu.apply_grid(ds_icon_r2b8, ds_grid_r2b8)
        ds_r2b8_map.append(ds_icon_r2b8_swath)
        ds_icon_r2b8_swath = ds_icon_r2b8_swath.drop('longitude').drop('latitude')

        p_r2b8      = fu.compute_spectra_uchida(ds_icon_r2b8_swath, 2000*sub) #subsampled to 10000m
        p_r2b8_mean = p_r2b8.mean(dim='num_pixels')
        P_r2b8.append(p_r2b8_mean)


    ds_swot_map = xr.concat(ds_swot_map, dim='swath')
    P_swot = {key: xr.concat(val, dim='swath') for key, val in P_swot.items()}
    P_swot = xr.Dataset(P_swot)

    P_icon      = xr.concat(P_icon, dim='swath')
    ds_icon_map = xr.concat(ds_icon_map, dim='swath')
    P_r2b8      = xr.concat(P_r2b8, dim='swath')
    ds_r2b8_map = xr.concat(ds_r2b8_map, dim='swath')

    # P_swot.to_netcdf(path_2_data+'P_swot.nc')
    # P_icon.to_netcdf(path_2_data+'P_icon.nc')
    # P_r2b8.to_netcdf(path_2_data+'P_r2b8_5.nc')
    # ds_icon_map.to_netcdf(path_2_data+'ds_icon_map.nc')
    # ds_r2b8_map.to_netcdf(path_2_data+'ds_r2b8_map_5.nc')
    # ds_swot_map.to_netcdf(path_2_data+'ds_swot_map.nc')

else:
    P_swot      = xr.open_dataset(path_2_data+'P_swot.nc')
    P_icon      = xr.open_dataset(path_2_data+'P_icon.nc')
    P_r2b8      = xr.open_dataset(path_2_data+'P_r2b8_5.nc')
    ds_icon_map = xr.open_dataset(path_2_data+'ds_icon_map.nc')
    ds_r2b8_map = xr.open_dataset(path_2_data+'ds_r2b8_map_5.nc')
    ds_swot_map = xr.open_dataset(path_2_data+'ds_swot_map.nc')
    P_icon      = P_icon.to_array().squeeze()
    P_r2b8      = P_r2b8.to_array().squeeze()
# %% ####################### Visualization ############################

fu.swot_l3_spectra_inter(P_swot, P_icon, P_r2b8, savefig, path_fig)

# %%
reload(fu)
fu.plot_swot_icon_spectra(P_swot, P_icon, P_r2b8, savefig, path_fig)


# %%
reload(fu)
fu.plot_map_spectra(ds_swot_map.ssha, P_swot, P_icon, P_r2b8, savefig, path_fig)


# %%
if False:
    #Distance range from 180 tp 185
    P1 = ds_r2b8_map.isel(swath=0).isel(num_pixels=30).isel(num_lines=0)
    P2 = ds_r2b8_map.isel(swath=0).isel(num_pixels=30).isel(num_lines=1)
    # at end
    P1_end = ds_r2b8_map.isel(swath=0).isel(num_pixels=30).isel(num_lines=-2)
    P2_end = ds_r2b8_map.isel(swath=0).isel(num_pixels=30).isel(num_lines=-1)
    
    dist = np.sqrt((P1.longitude-P2.longitude)**2 + (P1.latitude-P2.latitude)**2)
    dist_end = np.sqrt((P1_end.longitude-P2_end.longitude)**2 + (P1_end.latitude-P2_end.latitude)**2)
    #and in meter at latitude
    dist_m = dist * eva.convert_degree_to_meter(int(P1.latitude.values))
    dist_m_end = dist_end * eva.convert_degree_to_meter(int(P1_end.latitude.values))
    print('dist in degree', dist.values, 'and in meter', dist_m.values)
    print('dist in degree at end', dist_end.values, 'and in meter at end', dist_m_end.values)
# %%
if False: #simple check of swaths maps
    plt.figure()
    for i in range(ds_icon_map.swath.size):
        plt.scatter(ds_icon_map.sel(swath=i).longitude, ds_icon_map.sel(swath=i).latitude, s=1, c=ds_icon_map.sel(swath=i), cmap=cmocean.cm.thermal)
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.colorbar()
    if savefig == True: plt.savefig(path_fig+'smt_map.png', bbox_inches='tight')
    # %%
    plt.figure()
    for i in range(ds_r2b8_map.swath.size):
        plt.scatter(ds_r2b8_map.sel(swath=i).lon, ds_r2b8_map.sel(swath=i).lat, s=1, c=ds_r2b8_map.sel(swath=i), cmap=cmocean.cm.thermal)
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.colorbar()
    if savefig == True: plt.savefig(path_fig+'r2b8_map.png', bbox_inches='tight')
    # %%
    plt.figure()
    for i in range(ds_swot_map.swath.size):
        plt.scatter(ds_swot_map.sel(swath=i).longitude, ds_swot_map.sel(swath=i).latitude, s=1, c=ds_swot_map.sel(swath=i), cmap=cmocean.cm.thermal)
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.colorbar()
    if savefig == True: plt.savefig(path_fig+'swot_map.png', bbox_inches='tight')

    # %% above code using projection
    fig = plt.figure(figsize=(10, 10))
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.coastlines()
    for i in range(ds_swot_map.swath.size):
        plt.scatter(ds_swot_map.sel(swath=i).longitude, ds_swot_map.sel(swath=i).latitude, s=1, c=ds_swot_map.sel(swath=i), cmap=cmocean.cm.thermal, transform=ccrs.PlateCarree())

    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.colorbar()
    lon_vis = [-80, -30]
    lat_vis = [0, 60]
    ax.set_extent([lon_vis[0], lon_vis[1], lat_vis[0], lat_vis[1]], ccrs.PlateCarree())
    # add ticks
    ax.set_xticks(np.arange(lon_vis[0], lon_vis[1], 10), crs=ccrs.PlateCarree())
    ax.set_yticks(np.arange(lat_vis[0], lat_vis[1], 10), crs=ccrs.PlateCarree())
    if savefig == True: plt.savefig(path_fig+'swot_map_large.png', bbox_inches='tight')

    #%% plot all realisations
    plt.figure(figsize=(10, 8))
    #iterate over swath spectra
    for i in range(P_icon.swath.size):
        plt.loglog(P_icon.freq_num_lines, P_icon.isel(swath=i), label=f'swath {i}')
    plt.legend()
    # %%
    # and for sowt
    plt.figure(figsize=(10, 8))
    #iterate over swath spectra
    for i in range(P_swot.swath.size):
        plt.loglog(P_swot.freq_num_lines, P_swot['ssha'].isel(swath=i), label=f'swath {i}')
    plt.legend()
    # %%
    plt.figure()
    plt.loglog(P_r2b8.freq_num_lines, P_r2b8.mean(dim='swath').values, color = 'tab:green', label='ICON R2B8',zorder=10)

