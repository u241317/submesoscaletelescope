# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp
# import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
import xrft
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import cmocean

# %% ############### FUnctions ############################
def compute_spectra_uchida(data, samp_freq=2000):
    data['num_lines'] = np.arange(int(data.num_lines.size))*samp_freq
    P = xrft.power_spectrum(data, dim=['num_lines'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
    P = P.isel(freq_num_lines=slice(len(P.freq_num_lines)//2,None)) * 2
    return P

# %% ################################START MULTIPLE SWATHS ANALYSIS################################
path_fig = "/home/m/m300878/submesoscaletelescope/results/smt_natl/swot/l3/icon_swot_swaths/"
savefig  = False
path     = '/work/mh0033/m300878/observation/satellite/SWOT/NA/SWOT_L3_LR_SSH_1.0/'
files    = glob.glob(path + '*.nc')
# Domain
lon_reg = [-60,-40]
lat_reg = [10, 45]


# %% #################### SWOT SPECTRA ############################
# do mean over all relevant swaths
P_swot = []
da = []
for i in range(len(files)):
    ds = xr.open_dataset(files[i])
    # ds['ssha_karin_corrected'] = ds.ssha_karin + ds.height_cor_xover
    ds = ds.ssha
    ds['longitude'] = xr.where(ds.longitude > 180, ds.longitude - 360, ds.longitude)
    #check if in region
    if not (ds.longitude > lon_reg[0]).any() or not (ds.longitude < lon_reg[1]).any() or not (ds.latitude > lat_reg[0]).any() or not (ds.latitude < lat_reg[1]).any():
        print('out of domain')
        continue
    #check if swath is in region and
    if not ((ds.longitude > lon_reg[0]) & (ds.longitude < lon_reg[1]) & (ds.latitude > lat_reg[0]) & (ds.latitude < lat_reg[1]) ).any():
        print('no swath in region')
        continue

    ds0 = ds.where((ds.longitude > lon_reg[0]) & (ds.longitude < lon_reg[1]) & (ds.latitude > lat_reg[0]) & (ds.latitude < lat_reg[1]), drop=True)

    print(ds0.shape)
    if ds0.shape[0] != 1990: #1431:
        continue
    da.append(ds0)
    ds0_sel = ds0.interpolate_na(dim='num_lines', method="linear", fill_value="extrapolate")
    ds0_sel = ds0_sel.dropna('num_pixels', how='all')
    ds0_sel = ds0_sel.drop(['latitude', 'longitude'])
    # ds0_sel = ds0_sel.drop(['latitude_nadir', 'longitude_nadir'])
    P = compute_spectra_uchida(ds0_sel)
    P_mean = P.mean(dim='num_pixels')
    P_swot.append(P_mean)

P_swot = xr.concat(P_swot, dim='swath')

da = xr.concat(da, dim='swath')


# %% #################### ICON SPECTRA ############################
ds_tgrid = eva.load_smt_grid()
ds_ssh = eva.load_smt_ssh()
# %%
# get single swath
ds_swot = xr.open_dataset(files[10])
# get mean time from dataset
time_mean = ds_swot.time.mean().values
print('time of swath',time_mean)
#  minus 14 years
time_mean_ = time_mean - pd.DateOffset(years=14)
print('time of swath - 14 years',time_mean_)

# %%
# select time
ds_sel = ds_ssh.sel(time=time_mean_, method='nearest')
print(ds_sel.time.values)
# %%
lon_swot = ds_swot.isel(num_pixels=35).longitude.values
lat_swot = ds_swot.isel(num_pixels=35).latitude.values
# %%
import numpy as np
from scipy.spatial import cKDTree
import xarray as xr
import pandas as pd

def spherical_to_cartesian(lon, lat):
  earth_radius = 6371e3
  x = earth_radius * np.cos(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  y = earth_radius * np.sin(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  z = earth_radius * np.sin(lat*np.pi/180.)
  return x, y, z

def interp_to_reg_grid(lon_i, lat_i, lon_o, lat_o, path):
    xi, yi, zi = spherical_to_cartesian(lon_i, lat_i)
    xo, yo, zo = spherical_to_cartesian(lon_o, lat_o)

    lzip_i = np.concatenate((xi[:,np.newaxis],yi[:,np.newaxis],zi[:,np.newaxis]), axis=1)
    lzip_o = np.concatenate((xo[:,np.newaxis],yo[:,np.newaxis],zo[:,np.newaxis]), axis=1) 
    tree   = cKDTree(lzip_i)
    dckdtree, ickdtree = tree.query(lzip_o , k=1, workers=-1)
    # dckdtree, ickdtree = tree.query(lzip_o , k=n_nearest_neighbours)

    Dind_dist = dict()
    Dind_dist['dckdtree_c'] = dckdtree
    Dind_dist['ickdtree_c'] = ickdtree

    ds_grid = xr.Dataset(coords=dict(lat=lat_o, lon=lon_o))
    for var in Dind_dist.keys(): 
        ds_grid[var] = xr.DataArray(Dind_dist[var], dims=['num_lines'])
    # print('save grid')
    # ds_grid.to_netcdf(f'{path}/reg_grid_{res}.nc')

    return ds_grid

def apply_grid(ds, ds_grid):
    arr_interp = ds.isel(ncells=ds_grid.ickdtree_c.compute().data.flatten())
    # arr_interp = arr_interp.assign_coords(ncells=pd.MultiIndex.from_product([lat_o, lon_o], names=("lat", "lon"))).unstack()
    #assign llat lon coords from ds_grid 
    arr_interp = arr_interp.assign_coords(lat=ds_grid.lat, lon=ds_grid.lon)
    #rename ncells to num_lines
    arr_interp = arr_interp.rename({'ncells':'num_lines'})

    return arr_interp

# %%
clon = np.rad2deg(ds_tgrid.clon.values)
clat = np.rad2deg(ds_tgrid.clat.values)

lon_o = lon_swot
lat_o = lat_swot

ds_grid = interp_to_reg_grid(clon, clat, lon_o, lat_o, path='reg_grids')

# %%
ds_icon_swath = apply_grid(ds_sel, ds_grid)
# %%
plt.figure()
ds_icon_swath.h_sp.plot()
(ds_swot.mdt+ds_swot.ssha).isel(num_pixels=10).plot()
# %% map with projection
fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection=ccrs.PlateCarree())
ax.coastlines()

# plot swath
plt.scatter(ds_swot.longitude, ds_swot.latitude, s=0.1, c='r', transform=ccrs.PlateCarree())
plt.scatter(ds_icon_swath.lon, ds_icon_swath.lat, s=0.1, c='b', transform=ccrs.PlateCarree())

# %% now get whole swath width
num_lines = ds_swot.num_lines.data.flatten()
num_pixels = ds_swot.num_pixels.data.flatten()
lon_o = ds_swot.longitude.data.flatten()
lat_o = ds_swot.latitude.data.flatten()

# %%
def interp_to_reg_grid(lon_i, lat_i, lon_o, lat_o, ds_orig):
    xi, yi, zi = spherical_to_cartesian(lon_i, lat_i)
    xo, yo, zo = spherical_to_cartesian(lon_o, lat_o)

    lzip_i = np.concatenate((xi[:,np.newaxis],yi[:,np.newaxis],zi[:,np.newaxis]), axis=1)
    lzip_o = np.concatenate((xo[:,np.newaxis],yo[:,np.newaxis],zo[:,np.newaxis]), axis=1) 
    tree   = cKDTree(lzip_i)
    dckdtree, ickdtree = tree.query(lzip_o , k=1, workers=-1)
    # dckdtree, ickdtree = tree.query(lzip_o , k=n_nearest_neighbours)

    Dind_dist = dict()
    Dind_dist['dckdtree_c'] = dckdtree
    Dind_dist['ickdtree_c'] = ickdtree

    ds_grid = xr.Dataset(coords=dict(longitude=ds_orig.longitude, latitude=ds_orig.latitude))
    for var in Dind_dist.keys(): 
        ds_grid[var] = xr.DataArray(Dind_dist[var].reshape(num_lines.size, num_pixels.size), dims=['num_lines', 'num_pixels'])


    return ds_grid
# %%
%%time
ds_grid = interp_to_reg_grid(clon, clat, lon_o, lat_o, path='reg_grids')
# %%
def apply_grid(ds, ds_grid):
    arr_interp = ds.isel(ncells=ds_grid.ickdtree_c.compute().data.flatten())
    arr_interp = arr_interp.assign_coords(ncells=pd.MultiIndex.from_product([num_lines, num_pixels], names=("num_lines", "num_pixels"))).unstack()
    arr_interp = arr_interp.drop('num_lines').drop('num_pixels')
    arr_interp = arr_interp.assign_coords(latitude=ds_grid.latitude, longitude=ds_grid.longitude)
    return arr_interp
# %%
ds_icon_swath = apply_grid(ds_sel, ds_grid)
# %% map with projection
fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection=ccrs.PlateCarree())
ax.coastlines()

# plot swath
plt.scatter(ds_swot.longitude, ds_swot.latitude, s=0.1, c='r', transform=ccrs.PlateCarree())
plt.scatter(ds_icon_swath.longitude, ds_icon_swath.latitude, s=0.00001, c='b', transform=ccrs.PlateCarree())
# %%
plt.figure()
for i in range(ds_swot.num_pixels.size)[::10]:
    ds_icon_swath.isel(num_pixels=i).h_sp.plot(color='b')
    (ds_swot.ssha+ds_swot.mdt).isel(num_pixels=i).plot()

# %%
def load_r2b8_ssh_mep():
    # data of R2B8 run of Mia, TKE mixing scheme  like smt model, diffrent spinup
    path_data  = '/work/mh0033/m300878/run_icon/icon-oes-1.3.01_mia/experiments/exp.ocean_era51h_r2b8_mep23214-ERA/outdata/'
    search_str = f'exp.ocean_era51h_r2b8_mep23214-ERA_PT1H_zos_2010*' 
    # search_str = f'exp.ocean_era51h_r2b8_jse22126-ERA_data_P1D_2010*'
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()
    r2b8 = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=dict(time=1))
    r2b8 = r2b8.drop('clat').drop('clon')
    return(r2b8.zos)

ds_r2b8       = load_r2b8_ssh_mep()
path_tgrid    = '/work/mh0033/m300602/icon/grids/r2b8_oce_r0004/r2b8_oce_r0004_tgrid.nc'
ds_r2b8_tgrid = xr.open_dataset(path_tgrid)
clon_r2b8     = np.rad2deg(ds_r2b8_tgrid.clon.values)
clat_r2b8     = np.rad2deg(ds_r2b8_tgrid.clat.values)


# %%######################## ICON and SWOT SPectra ############################
# do mean over all relevant swaths
clon = np.rad2deg(ds_tgrid.clon.values)
clat = np.rad2deg(ds_tgrid.clat.values)

P_swot = {}

ds_swot_map = []
compute_icon = True
if compute_icon:
    P_icon      = []
    ds_icon_map = []
    P_r2b8      = []
    ds_r2b8_map = []
n=0
for i in range(len(files)):
    #select and prep SWOT
    ds_swot = xr.open_dataset(files[i])
    ds_swot_time = ds_swot.time.mean().values
    #select ssha, ssha_noiseless, ssha_unedited, mdt, ocean_tide, mss, dac
    variables_to_keep = ['ssha', 'ssha_noiseless', 'ssha_unedited', 'mdt', 'ocean_tide', 'mss', 'dac', 'longitude', 'latitude']
    variables_to_drop = [var for var in ds_swot.variables if var not in variables_to_keep]
    ds_swot = ds_swot.drop_vars(variables_to_drop)

    ds_swot['longitude'] = xr.where(ds_swot.longitude > 180, ds_swot.longitude - 360, ds_swot.longitude)
    #check if in region
    if not (ds_swot.longitude > lon_reg[0]).any() or not (ds_swot.longitude < lon_reg[1]).any() or not (ds_swot.latitude > lat_reg[0]).any() or not (ds_swot.latitude < lat_reg[1]).any():
        print('no point in domain')
        continue
    #check if swath is in region and
    if not ((ds_swot.longitude > lon_reg[0]) & (ds_swot.longitude < lon_reg[1]) & (ds_swot.latitude > lat_reg[0]) & (ds_swot.latitude < lat_reg[1]) ).any():
        print('no swath in region')
        continue

    ds_swot_sel = ds_swot.where((ds_swot.longitude > lon_reg[0]) & (ds_swot.longitude < lon_reg[1]) & (ds_swot.latitude > lat_reg[0]) & (ds_swot.latitude < lat_reg[1]), drop=True)

    print(ds_swot_sel.ssha.shape)
    if ds_swot_sel.ssha.shape[0] != 1990: #1431:
        continue
    n+=1
    print('swath number', i, 'total swaths used', n)
    ds_swot_map.append(ds_swot_sel.ssha)

    for var in variables_to_keep:
        if var in ['latitude', 'longitude']: continue
        ds_swot_sel2 = ds_swot_sel[var].interpolate_na(dim='num_lines', method="linear", fill_value="extrapolate")
        ds_swot_sel2 = ds_swot_sel2.dropna('num_pixels', how='all')
        ds_swot_sel2 = ds_swot_sel2.drop(['latitude', 'longitude'])

        p_swot      = compute_spectra_uchida(ds_swot_sel2)
        p_swot_mean = p_swot.mean(dim='num_pixels')
        if var not in P_swot:
            P_swot[var] = []

        # Append the mean to the list for this variable
        P_swot[var].append(p_swot_mean)


    #select ICON
    if compute_icon:
        time_mean_ = ds_swot_time - pd.DateOffset(years=14)
        num_lines  = ds_swot_sel.num_lines.data.flatten()
        num_pixels = ds_swot_sel.num_pixels.data.flatten()
        lon_o      = ds_swot_sel.longitude.data.flatten()
        lat_o      = ds_swot_sel.latitude.data.flatten()

        print('select and interpolate ICON SMT data')
        ds_icon       = ds_ssh.sel(time=time_mean_, method='nearest').h_sp
        ds_grid       = interp_to_reg_grid(clon, clat, lon_o, lat_o, ds_swot_sel)
        ds_icon_swath = apply_grid(ds_icon, ds_grid)
        ds_icon_map.append(ds_icon_swath)
        ds_icon_swath = ds_icon_swath.drop(['latitude', 'longitude'])

        p_icon      = compute_spectra_uchida(ds_icon_swath)
        p_icon_mean = p_icon.mean(dim='num_pixels')
        P_icon.append(p_icon_mean)

        print('select and interpolate ICON R2B8 data')
        ds_icon_r2b8       = ds_r2b8.sel(time=time_mean_, method='nearest')
        ds_grid_r2b8       = interp_to_reg_grid(clon_r2b8, clat_r2b8, lon_o, lat_o, ds_swot_sel)
        ds_icon_r2b8_swath = apply_grid(ds_icon_r2b8, ds_grid_r2b8)
        ds_r2b8_map.append(ds_icon_r2b8_swath)
        ds_icon_r2b8_swath = ds_icon_r2b8_swath.drop('longitude').drop('latitude')

        p_r2b8      = compute_spectra_uchida(ds_icon_r2b8_swath)
        p_r2b8_mean = p_r2b8.mean(dim='num_pixels')
        P_r2b8.append(p_r2b8_mean)
        

ds_swot_map = xr.concat(ds_swot_map, dim='swath')
P_swot = {key: xr.concat(val, dim='swath') for key, val in P_swot.items()}
P_swot = xr.Dataset(P_swot)

if compute_icon:
    P_icon      = xr.concat(P_icon, dim='swath')
    ds_icon_map = xr.concat(ds_icon_map, dim='swath')
    P_r2b8      = xr.concat(P_r2b8, dim='swath')
    ds_r2b8_map = xr.concat(ds_r2b8_map, dim='swath')

# %%
#Distance range from 180 tp 185
P1 = ds_icon_r2b8_swath.isel(num_pixels=30).isel(num_lines=0)
P2 = ds_icon_r2b8_swath.isel(num_pixels=30).isel(num_lines=1)
# at end
P1_end = ds_icon_r2b8_swath.isel(num_pixels=30).isel(num_lines=-2)
P2_end = ds_icon_r2b8_swath.isel(num_pixels=30).isel(num_lines=-1)

dist = np.sqrt((P1.longitude-P2.longitude)**2 + (P1.latitude-P2.latitude)**2)
dist_end = np.sqrt((P1_end.longitude-P2_end.longitude)**2 + (P1_end.latitude-P2_end.latitude)**2)
#and in meter at latitude
dist_m = dist * eva.convert_degree_to_meter(int(P1.latitude.values))
dist_m_end = dist_end * eva.convert_degree_to_meter(int(P1.latitude.values))
print('dist in degree', dist.values, 'and in meter', dist_m.values)
print('dist in degree at end', dist_end.values, 'and in meter at end', dist_m_end.values)
# %%

plt.figure()
for i in range(ds_icon_map.swath.size):
    plt.scatter(ds_icon_map.sel(swath=i).longitude, ds_icon_map.sel(swath=i).latitude, s=1, c=ds_icon_map.sel(swath=i), cmap=cmocean.cm.thermal)
plt.xlabel('Longitude')
plt.ylabel('Latitude')
plt.colorbar()
if savefig == True: plt.savefig(path_fig+'smt_map.png', bbox_inches='tight')
# %%
plt.figure()
for i in range(ds_swot_map.swath.size):
    plt.scatter(ds_swot_map.sel(swath=i).longitude, ds_swot_map.sel(swath=i).latitude, s=1, c=ds_swot_map.sel(swath=i), cmap=cmocean.cm.thermal)
plt.xlabel('Longitude')
plt.ylabel('Latitude')
plt.colorbar()
if savefig == True: plt.savefig(path_fig+'swot_map.png', bbox_inches='tight')

# %% above code using projection
fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection=ccrs.PlateCarree())
ax.coastlines()
for i in range(ds_swot_map.swath.size):
    plt.scatter(ds_swot_map.sel(swath=i).longitude, ds_swot_map.sel(swath=i).latitude, s=1, c=ds_swot_map.sel(swath=i), cmap=cmocean.cm.thermal, transform=ccrs.PlateCarree())

plt.xlabel('Longitude')
plt.ylabel('Latitude')
plt.colorbar()
lon_vis = [-80, -30]
lat_vis = [0, 60]
ax.set_extent([lon_vis[0], lon_vis[1], lat_vis[0], lat_vis[1]], ccrs.PlateCarree())
# add ticks
ax.set_xticks(np.arange(lon_vis[0], lon_vis[1], 10), crs=ccrs.PlateCarree())
ax.set_yticks(np.arange(lat_vis[0], lat_vis[1], 10), crs=ccrs.PlateCarree())
if savefig == True: plt.savefig(path_fig+'swot_map_large.png', bbox_inches='tight')

# %%
plt.figure()
for i in range(ds_r2b8_map.swath.size):
    plt.scatter(ds_r2b8_map.sel(swath=i).lon, ds_r2b8_map.sel(swath=i).lat, s=1, c=ds_r2b8_map.sel(swath=i), cmap=cmocean.cm.thermal)
plt.xlabel('Longitude')
plt.ylabel('Latitude')
plt.colorbar()
if savefig == True: plt.savefig(path_fig+'r2b8_map.png', bbox_inches='tight')

# %%
plt.figure(figsize=(10, 8))
plt.loglog(P_swot.freq_num_lines, (P_swot['ssha'].mean(dim='swath').values+P_swot['mdt'].mean(dim='swath').values), label='adt=ssha+mdt', color='black')
for var in P_swot.variables:
    if var in ['freq_num_lines']: continue
    plt.loglog(P_swot.freq_num_lines, P_swot[var].mean(dim='swath').values, label=var)

plt.loglog(P_icon.freq_num_lines, P_icon.mean(dim='swath').values, color = 'blue', label='ICON-SMT')
plt.loglog(P_r2b8.freq_num_lines, P_r2b8.mean(dim='swath').values, color = 'green', label='ICON-R2B8')
plt.legend()
# grid with more lones
plt.grid(which='both')
plt.xlabel('Wavenumber [1/m]')
plt.ylabel('PSD [m^2/m]')

if savefig == True: plt.savefig(path_fig+'spectra_inter.png', bbox_inches='tight')

# %%
plt.figure(figsize=(10, 8))
#iterate over swath
for i in range(ds_icon_map.swath.size):
    plt.plot(ds_icon_map.isel(swath=i), label=f'swath {i}')

#%%
plt.figure(figsize=(10, 8))
#iterate over swath spectra
for i in range(P_icon.swath.size):
    plt.loglog(P_icon.freq_num_lines, P_icon.isel(swath=i), label=f'swath {i}')
plt.legend()
# %%
# and for sowt
plt.figure(figsize=(10, 8))
#iterate over swath spectra
for i in range(P_swot.swath.size):
    plt.loglog(P_swot.freq_num_lines, P_swot['ssha'].isel(swath=i), label=f'swath {i}')
plt.legend()
# %%
P_r2b8_low = P_r2b8.where(P_r2b8.freq_num_lines < 1/(2*10000), drop=True)
#figure with cloud of all swaths in red for swot and blue for icon
fig = plt.figure(figsize=(10, 8))
# add -2 slope
x = np.linspace(1e-6, 1e-4, 100)
y = 1e-8*x**-2
plt.loglog(x, y, color='black', linestyle='--', label='-2 slope', linewidth=0.8)
# and -3 slope
y = 1e-13*x**-3
plt.loglog(x, y, color='black', linestyle=':', label='-3 slope', linewidth=0.8)

for i in range(P_icon.swath.size):
    plt.loglog(P_icon.freq_num_lines, P_icon.isel(swath=i), color='grey', alpha=0.08)
plt.loglog(P_icon.freq_num_lines, P_icon.mean(dim='swath').values, color = 'tab:blue', label='ICON SMT',zorder=11, linewidth=1.6)
for i in range(P_swot.swath.size):
    plt.loglog(P_r2b8_low.freq_num_lines, P_r2b8_low.isel(swath=i), color='mediumaquamarine', alpha=0.08)
plt.loglog(P_r2b8_low.freq_num_lines, P_r2b8_low.mean(dim='swath').values, color = 'tab:green', label='ICON R2B8',zorder=10, linewidth=1.6)
for i in range(P_swot.swath.size):
    plt.loglog(P_swot.freq_num_lines, P_swot['ssha_noiseless'].isel(swath=i), color='wheat', alpha=0.08)
plt.loglog(P_swot.freq_num_lines, P_swot['ssha_noiseless'].mean(dim='swath').values, color = 'tab:red', label='SWOT ssha noiseless',zorder=9, linewidth=1.3)
# plt.loglog(P_swot.freq_num_lines, P_swot['ssha'].mean(dim='swath').values, color = 'tab:red', label='SWOT ssha',zorder=9, alpha=0.5, linewidth=1.6)

plt.legend()
plt.grid(which='both', color='grey', linestyle='--', linewidth=0.1)
plt.xlabel('Wavenumber [1/m]')
plt.ylabel('PSD [m^2/m]')
ax2 = plt.gca().secondary_xaxis('top', functions=(lambda x: 1/x, lambda x: 1/x))
ax2.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
ax2.set_xlabel('Wavelength [m]')

if savefig == True: plt.savefig(path_fig+'spectra_select_1.png', bbox_inches='tight')
# %%
plt.figure()
plt.loglog(P_r2b8.freq_num_lines, P_r2b8.mean(dim='swath').values, color = 'tab:green', label='ICON R2B8',zorder=10)

# %% select frequenccies below 1e-3
P_swot_low = P_swot.where(P_swot.freq_num_lines < 1/(2*2000), drop=True)
P_r2b8_low = P_r2b8.where(P_r2b8.freq_num_lines < 1/(2*10000), drop=True)
P_icon_low = P_icon.where(P_icon.freq_num_lines < 1/(2*2000), drop=True)

plt.figure(figsize=(10, 8))
plt.loglog(P_swot_low.freq_num_lines, P_swot_low['ssha'].mean(dim='swath').values, color = 'red', label='swot')
plt.loglog(P_icon_low.freq_num_lines, P_icon_low.mean(dim='swath').values, color = 'blue', label='icon')
plt.loglog(P_r2b8_low.freq_num_lines, P_r2b8_low.mean(dim='swath').values, color = 'green', label='icon r2b8')

# add -2 slope
x = np.linspace(1e-6, 1e-4, 100)
y = 1e-8*x**-2
plt.loglog(x, y, color='black', linestyle='--', label='-2 slope', linewidth=0.8)
# and -3 slope
y = 1e-13*x**-3
plt.loglog(x, y, color='black', linestyle=':', label='-3 slope', linewidth=0.8)

plt.legend()
plt.grid(which='both', color='grey', linestyle='--', linewidth=0.1)
plt.xlabel('Wavenumber [1/m]')
plt.ylabel('PSD [m^2/m]')
ax2 = plt.gca().secondary_xaxis('top', functions=(lambda x: 1/x, lambda x: 1/x))
ax2.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
ax2.set_xlabel('Wavelength [m]')




# %%
