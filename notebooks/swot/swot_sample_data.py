# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp
# import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
cluster
client

# %%
path = "/work/mh0033/m300878/observation/satellite/SWOT/L2_LR_SSH/SWOT_L2_LR_SSH_Basic_001_001_20160901T000000_20160901T005126_DG10_01.nc"

DS = xr.open_dataset(path)
# %%
ds = DS.isel(num_lines=slice(0, 2200))


plt.figure()
for i in np.arange(ds.num_pixels.size):
    da = ds.isel(num_pixels=i)
    plt.scatter(da.longitude, da.latitude, c=da.ssha_karin, cmap='RdYlBu', s=0.01)

# plt.xlim([25, 50])
# plt.ylim([-60, -20])

# %%
path = '/work/mh0033/m300878/observation/satellite/SWOT/NA/SWOT_L2_LR_SSH_2.0/'
files = glob.glob(path + 'SWOT_L2_LR_SSH_Basic*')
# ds = xr.open_mfdataset(files, combine='time', parallel=True)
ds = xr.open_dataset(files[18])#16
ds
# %%
savefig = True
path_fig = '/home/m/m300878/submesoscaletelescope/results/smt_natl/swot/l2/'

# %%
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import cm   
plt.figure(figsize=(10,5))

ax = plt.axes(projection=ccrs.Robinson())
ax.coastlines(resolution='110m', color='k')
ax.scatter(ds.longitude.data, ds.latitude.data, s=1, c=ds.ssha_karin.data, cmap=cm.RdBu_r, transform=ccrs.PlateCarree(), vmin=-1, vmax=1)
# apply projection
ax.set_extent([0, 360, -90, 90], crs=ccrs.PlateCarree())
plt.savefig(path_fig + 'single_swot_globe.png', dpi=150)
# %%
da = ds.ssh_karin.where((ds.latitude > 15) & (ds.latitude < 45), drop=True)#.where((ds.latitude < 0), drop=True)
plt.figure()
da.isel(num_pixels=30).plot()
if savefig==True: plt.savefig(path_fig + 'tseries_single_pixel.png', dpi=150)
# %%
plt.scatter(da.longitude, da.latitude, c=da, cmap='RdYlBu', s=0.01, vmin=-1, vmax=1)
# plt.ylim(-1,1)
# %%
import xrft
def compute_spectra_uchida(data, samp_freq=1):
    data['num_lines'] = np.arange(int(data.num_lines.size))*samp_freq
    P = xrft.power_spectrum(data, dim=['num_lines'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
    P = P.isel(freq_num_lines=slice(len(P.freq_num_lines)//2,None)) * 2
    return P
# %%
ds_sel = da.interpolate_na(dim='num_lines').drop(['latitude', 'longitude'])


P = compute_spectra_uchida(ds_sel.fillna(0.), samp_freq=1/250)
# %%
# plot spectra
plt.figure(figsize=(10,10))
for i in np.arange(P.num_pixels.size):
    P.isel(num_pixels=i).plot(x='freq_num_lines', yscale='log', xscale='log', alpha=0.5, color='grey')
# P.isel(num_pixels=30).plot(x='freq_num_lines', yscale='log', xscale='log')
# mean
plt.plot(P.freq_num_lines, P.mean(['num_pixels']), color='r', lw=2)
# plt.ylim([1e-5, 1e0])
# %%
path = '/work/mh0033/m300878/observation/satellite/SWOT/NA/north_atlantic_basic/'
files = glob.glob(path + '*SWOT_L2_LR_SSH_Basic*')
ds = xr.open_mfdataset(files, combine='nested', concat_dim='num_lines', parallel=True)
# ds = xr.open_dataset(files[21])#16
ds
# %%
import cmocean
plt.figure(figsize=(10,5))

ax = plt.axes(projection=ccrs.Robinson())
ax.coastlines(resolution='110m', color='k')
for i in np.arange(len(files)):
    ds = xr.open_dataset(files[i])
    ax.scatter(ds.longitude.data, ds.latitude.data, s=1, c=ds.ssh_karin.data, cmap=cmocean.cm.thermal, transform=ccrs.PlateCarree())
# apply projection
# ax.set_extent([0, 360, 0, 60], crs=ccrs.PlateCarree())
if savefig==True: plt.savefig(path_fig + 'multi_swot_globe.png', dpi=150)
# %%
ds_concat = []
for i in np.arange(len(files)):
    ds = xr.open_dataset(files[i])
    if i == 26 or i == 32 or i == 38 or i == 66 or i == 70 or i == 73: 
        continue
    ds_concat.append(ds)
    print(i, ds.ssh_karin.shape)
ds_concat = xr.concat(ds_concat, dim='num_lines')
ds_concat['longitude'] = xr.where(ds_concat.longitude > 180, ds_concat.longitude - 360, ds_concat.longitude)

# %%
# convert longitude from 0-360 to -180-180
# %%

import numpy as np
from scipy.spatial import cKDTree
import xarray as xr
import pandas as pd

lon_reg = [-80,-40]
lat_reg = [20, 45]
res     = 0.01
# %%
# --- make rectangular grid 
lon = np.arange(lon_reg[0],lon_reg[1],res)
lat = np.arange(lat_reg[0],lat_reg[1],res)
Lon, Lat = np.meshgrid(lon, lat)

#output grid array
lon_o = Lon.flatten()
lat_o = Lat.flatten()
# %%
# input grid array
clon = (ds_concat.longitude.values).flatten()
clat = (ds_concat.latitude.values).flatten()

# %%
def spherical_to_cartesian(lon, lat):
  earth_radius = 6371e3
  x = earth_radius * np.cos(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  y = earth_radius * np.sin(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  z = earth_radius * np.sin(lat*np.pi/180.)
  return x, y, z

def interp_to_reg_grid(lon_i, lat_i, lon_o, lat_o, path):
    xi, yi, zi = spherical_to_cartesian(lon_i, lat_i)
    xo, yo, zo = spherical_to_cartesian(lon_o, lat_o)

    lzip_i = np.concatenate((xi[:,np.newaxis],yi[:,np.newaxis],zi[:,np.newaxis]), axis=1)
    lzip_o = np.concatenate((xo[:,np.newaxis],yo[:,np.newaxis],zo[:,np.newaxis]), axis=1) 
    tree   = cKDTree(lzip_i)
    dckdtree, ickdtree = tree.query(lzip_o , k=1, workers=-1)
    # dckdtree, ickdtree = tree.query(lzip_o , k=n_nearest_neighbours)

    Dind_dist = dict()
    Dind_dist['dckdtree_c'] = dckdtree
    Dind_dist['ickdtree_c'] = ickdtree

    ds_grid = xr.Dataset(coords=dict(lat=lat, lon=lon))
    for var in Dind_dist.keys(): 
        ds_grid[var] = xr.DataArray(Dind_dist[var].reshape(lat.size, lon.size), dims=['lat', 'lon'])
    print('save grid')
    ds_grid.to_netcdf(f'{path}/reg_grid_{res}.nc')

    return ds_grid

ds_grid = interp_to_reg_grid(clon, clat, lon_o, lat_o, path='/work/mh0033/m300878/observation/satellite/SWOT/NA/')
# %%
ds_sel = ds_concat.ssh_karin_2.data.flatten()
# create dataarray with ncells as index
ds_sel = xr.DataArray(ds_sel, dims='ncells', coords={'ncells':np.arange(ds_sel.size)})

def apply_grid(ds, ds_grid):
    arr_interp = ds.isel(ncells=ds_grid.ickdtree_c.compute().data.flatten())
    arr_interp = arr_interp.assign_coords(ncells=pd.MultiIndex.from_product([lat, lon], names=("lat", "lon"))).unstack()
    return arr_interp

arr_interp = apply_grid(ds_sel, ds_grid)
# %%
arr_interp.isel(lon=2000).plot()
# arr_interp.interpolate_na(dim='lon').isel(lon=2000).plot()
# smooth
# arr_interp.interpolate_na(dim='lon').rolling(lon=5).mean().isel(lon=2000).plot()
if savefig==True: plt.savefig(path_fig + 'tseries_single_pixel_interp_reg_grid.png', dpi=150)
# %%
arr_interp.plot(cmap=cmocean.cm.thermal, vmin=-50, vmax=50)
if savefig==True: plt.savefig(path_fig + 'on_reg_grid.png', dpi=150)
# %%
arr_interp.interpolate_na(dim='lat').plot(cmap=cmocean.cm.thermal, vmin=-50, vmax=50)
if savefig==True: plt.savefig(path_fig + 'on_reg_grid_interp_lat.png', dpi=150)
# %%

# %%
def compute_spectra_uchida(data, samp_freq=1):
    data['lon'] = np.arange(int(data.lon.size))*samp_freq
    P = xrft.power_spectrum(data, dim=['lon'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
    P = P.isel(freq_lon=slice(len(P.freq_lon)//2,None)) * 2
    return P

# def compute_spectra_uchida(data, samp_freq=1):
#     data['lat'] = np.arange(int(data.lat.size))*samp_freq
#     P = xrft.power_spectrum(data, dim=['lat'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
#     P = P.isel(freq_lat=slice(len(P.freq_lat)//2,None)) * 2
#     return P

ds_sel = arr_interp.sel(lon=slice(-60, -40)).interpolate_na(dim='lat')#.rolling(lon=5).mean()
P = compute_spectra_uchida(ds_sel.fillna(0.), samp_freq=1/250)
# %%
#plot spectra
plt.figure(figsize=(10,10))
for i in np.arange(P.lat.size)[::10]:
    P.isel(lat=i).plot(x='freq_lon', yscale='log', xscale='log', alpha=0.5, color='grey')
P.mean(['lat']).plot(x='freq_lon', yscale='log', xscale='log', color='r', lw=2)
if savefig==True: plt.savefig(path_fig + 'spectra_on_reg_grid_lat.png', dpi=150)

# %%
plt.figure(figsize=(10,10))
for i in np.arange(P.lon.size)[::10]:
    P.isel(lon=i).plot(x='freq_lat', yscale='log', xscale='log', alpha=0.5, color='grey')
P.mean(['lon']).plot(x='freq_lat', yscale='log', xscale='log', color='r', lw=2)

if savefig==True: plt.savefig(path_fig + 'spectra_on_reg_grid.png', dpi=150)

# %%
import datashader as dshader
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cmocean
from datashader.mpl_ext import dsshow
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from   cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


def dshader_plot_map(var, lon_range, lat_range, vminmax=None, cmap='plasma'):
    projection = ccrs.PlateCarree()
    if lon_range is None:
        lon_range = [-180, 180]
    if lat_range is None:
        lat_range = [-90, 90]
        
    fig, ax = plt.subplots(figsize=(10,5), subplot_kw={'projection': projection})
    fig.canvas.draw_idle()  # necessary to make things work

    #-- transform radians to geodetic coordinates
    coords = projection.transform_points(
                            ccrs.Geodetic(),
                            (clon),
                            (clat))
    # Get the data in a dataframe to use dasher plot
    df = pd.DataFrame(data={
        "var": np.squeeze(var.values),
        "x": coords[:,0], 
        "y": coords[:,1],
    },)
    
    df = df[(df['x']>lon_range[0]) & (df['x']<lon_range[1]) & 
            (df['y']>lat_range[0]) & (df['y']<lat_range[1])]
    
    artist = dsshow(df, dshader.Point('x', 'y'), dshader.mean('var'), cmap=cmap, vmax=vminmax[1], vmin=vminmax[0], ax=ax)
    ax.add_feature(cfeature.LAND, facecolor='lightgrey', zorder=5)
    ax.add_feature(cfeature.COASTLINE, linewidth=0.8, zorder=6)
    ax.add_feature(cfeature.BORDERS, linewidth=0.5, zorder=7)
    # add longitude and latitude labels
    ax.set_xticks(np.arange(lon_range[0], lon_range[1], 4), crs=projection)
    ax.set_yticks(np.arange(lat_range[0], lat_range[1], 4), crs=projection)
    ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore

    # apply the default formatter to the ticks
    ax.xaxis.set_major_formatter(LONGITUDE_FORMATTER)
    ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)
    plt.savefig(path_fig + 'dshader_plot_map.png', dpi=150)
    return fig, ax
# %%
clon = (ds_concat.longitude.values).flatten()
clat = (ds_concat.latitude.values).flatten()
ds_sel = ds_concat.ssha_karin_2.data.flatten()
# create dataarray with ncells as index
ds_sel = xr.DataArray(ds_sel, dims='ncells', coords={'ncells':np.arange(ds_sel.size)})
# ds_sel = ds_sel.assign_coords(longitude=clon, latitude=clat)
dshader_plot_map(ds_sel, lon_reg, lat_reg, vminmax=[-1,1], cmap='plasma')
# %%
lon_reg = [-60,-40]
lat_reg = [20, 45]

# do mean over all relevant swaths
P_all = []
da = []
slon = []
slat = []
for i in range(len(files)):
    ds = xr.open_dataset(files[i])
    ds['longitude'] = xr.where(ds.longitude > 180, ds.longitude - 360, ds.longitude)
    #check if in region
    if not (ds.longitude > lon_reg[0]).any() or not (ds.longitude < lon_reg[1]).any() or not (ds.latitude > lat_reg[0]).any() or not (ds.latitude < lat_reg[1]).any():
        continue
    ds = ds.where((ds.longitude > lon_reg[0]) & (ds.longitude < lon_reg[1]) & (ds.latitude > lat_reg[0]) & (ds.latitude < lat_reg[1]), drop=True)
    ds = ds.drop(['latitude', 'longitude'])

    if ds0.ssh_karin.shape[0] != 1431:
        continue
    print(ds0.ssh_karin.shape)
    ds0 = ds0.where(ds0.ssh_karin_qual == 0)
    ds0_sel = ds0.ssha_karin.interpolate_na(dim='num_lines', method="linear", fill_value="extrapolate")
    ds0_sel = ds0_sel.dropna('num_pixels', how='all')
    #save selected data
    # da.append(ds0_sel)
    # slon.append(ds.longitude)
    # slat.append(ds.latitude)
    # ds0_sel = ds0_sel.drop(['latitude', 'longitude'])
    P = compute_spectra_uchida(ds0_sel)
    P_mean = P.mean(dim='num_pixels')
    P_all.append(P_mean)

P_all = xr.concat(P_all, dim='swath')

# da = xr.concat(da, dim='swath')
# slon = xr.concat(slon, dim='swath')
# slat = xr.concat(slat, dim='swath')

