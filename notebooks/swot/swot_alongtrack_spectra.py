# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp
# import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
#%% obtain daat
# ! podaac-data-downloader -c SWOT_L2_LR_SSH_Basic_2.0 -d ./north_atlantic_basic -sd 2024-03-05T00:00:00Z -ed 2024-03-26T00:00:00Z -b="-80.0,20,-40,45" --subset

# %%
path_fig        = "/home/m/m300878/submesoscaletelescope/results/smt_natl/swot/l2/"
savefig         = False
path            = '/work/mh0033/m300878/observation/satellite/SWOT/NA/north_atlantic_basic/'
files           = glob.glob(path + '*SWOT_L2_LR_SSH_Basic*')
ds              = xr.open_dataset(files[1])#16
ds['longitude'] = xr.where(ds.longitude > 180, ds.longitude - 360, ds.longitude)
ds


# %%
path_fig        = "/home/m/m300878/submesoscaletelescope/results/smt_natl/swot/"
path            = '/work/mh0033/m300878/observation/satellite/SWOT/NA/north_atlantic_expert/'
files           = glob.glob(path + '*SWOT_L2_LR_SSH*')
ds              = xr.open_dataset(files[1])#16
ds['longitude'] = xr.where(ds.longitude > 180, ds.longitude - 360, ds.longitude)
ds

# %%
# path = '/work/mh0033/m300878/observation/satellite/SWOT/NA/SWOT_L3_LR_SSH_1.0/'
# files = glob.glob(path + '*.nc')
# ds = xr.open_dataset(files[0])#16
# ds['longitude'] = xr.where(ds.longitude > 180, ds.longitude - 360, ds.longitude)
# ds

# ds0 = ds
# ds0_sel = ds0.mdt.interpolate_na(dim='num_lines', method="linear", fill_value="extrapolate")
# ds0_sel = ds0_sel.drop(['latitude', 'longitude'])
# ds0_sel.plot()

# %%
ds0 = ds#.isel(num_pixels=slice(0,32))
ds0 = ds0.where(ds0.ssh_karin_qual == 0)
ds0_sel = ds0.ssha_karin.interpolate_na(dim='num_lines', method="linear", fill_value="extrapolate")
# drop longitude and latitude
ds0_sel = ds0_sel.drop(['latitude', 'longitude'])
ds0_sel = ds0_sel.drop(['latitude_nadir', 'longitude_nadir'])
ds0_sel.plot()
if savefig == True: plt.savefig(path_fig + 'single_swath_vis.png')
# %%
# remove entire row if filled with nans
ds0_sel = ds0_sel.dropna('num_pixels', how='all')
ds0_sel.plot()

# %%
import xrft
def compute_spectra_uchida(data, samp_freq=250):
    data['num_lines'] = np.arange(int(data.num_lines.size))*samp_freq
    P = xrft.power_spectrum(data, dim=['num_lines'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
    P = P.isel(freq_num_lines=slice(len(P.freq_num_lines)//2,None)) * 2
    return P
# %%
P = compute_spectra_uchida(ds0_sel)
# %%
plt.figure(figsize=(10, 8))
for i in range(P.num_pixels.size):
    plt.loglog(P.freq_num_lines, P.sel(num_pixels=i).values, color='grey', alpha=0.5)

plt.loglog(P.freq_num_lines, P.isel(num_pixels=slice(0,25)).mean(dim='num_pixels').values, color = 'green', label='left')
plt.loglog(P.freq_num_lines, P.isel(num_pixels=slice(25,50)).mean(dim='num_pixels').values, color = 'yellow', label='right')
plt.loglog(P.freq_num_lines, P.mean(dim='num_pixels').values, color = 'red', label='mean')
plt.legend()
plt.grid()
plt.xlabel('Wavenumber [1/m]')
plt.ylabel('PSD [m^2/m]')
if savefig==True: plt.savefig(path_fig + 'single_swath_spectra.png')
# %%
lon_reg = [-60,-40]
lat_reg = [20, 45]

# do mean over all relevant swaths
P_all = []
da = []
for i in range(len(files)):
    ds = xr.open_dataset(files[i])
    ds['ssha_karin_corrected'] = ds.ssha_karin + ds.height_cor_xover
    ds['longitude'] = xr.where(ds.longitude > 180, ds.longitude - 360, ds.longitude)
    #check if in region
    if not (ds.longitude > lon_reg[0]).any() or not (ds.longitude < lon_reg[1]).any() or not (ds.latitude > lat_reg[0]).any() or not (ds.latitude < lat_reg[1]).any():
        continue
    ds = ds.where((ds.longitude > lon_reg[0]) & (ds.longitude < lon_reg[1]) & (ds.latitude > lat_reg[0]) & (ds.latitude < lat_reg[1]), drop=True)
    ds0 = ds#.drop(['latitude', 'longitude'])
    if ds0.ssh_karin.shape[0] != 1431:
        continue
    print(ds0.ssh_karin.shape)
    da.append(ds0)
    ds0 = ds0.where(ds0.ssh_karin_qual == 0)
    ds0_sel = ds0.ssha_karin_corrected.interpolate_na(dim='num_lines', method="linear", fill_value="extrapolate")
    ds0_sel = ds0_sel.dropna('num_pixels', how='all')
    ds0_sel = ds0_sel.drop(['latitude', 'longitude'])
    ds0_sel = ds0_sel.drop(['latitude_nadir', 'longitude_nadir'])
    P = compute_spectra_uchida(ds0_sel)
    P_mean = P.mean(dim='num_pixels')
    P_all.append(P_mean)

P_all = xr.concat(P_all, dim='swath')

da = xr.concat(da, dim='swath')

# %%
P_nadir = []
for i in range(len(files)):
    ds = xr.open_dataset(files[i])
    ds = ds.drop(['latitude', 'longitude'])
    ds = ds.drop(['latitude_nadir', 'longitude_nadir'])
    ds_sel = ds.isel(num_pixels=0).swh_nadir_altimeter
    ds_sel = ds_sel.interpolate_na(dim='num_lines', method="linear", fill_value="extrapolate")
    if ds_sel.shape[0] != 1431:
        continue
    print(ds_sel.shape)
    P = compute_spectra_uchida(ds_sel)
    # P_mean = P.mean(dim='num_pixels')
    P_nadir.append(P_mean)

P_nadir = xr.concat(P_nadir, dim='swath')

# %%
#plot mean spectra
plt.figure(figsize=(10, 8))
for i in range(P_all.swath.size):
    plt.loglog(P_all.freq_num_lines, P_all.sel(swath=i).values, color='grey', alpha=0.5)

plt.loglog(P_all.freq_num_lines, P_all.mean(dim='swath').values, color = 'red', label='mean')
plt.loglog(P_nadir.freq_num_lines, P_nadir.mean(dim='swath').values, color = 'blue', label='nadirn')

plt.legend()
plt.grid()
plt.xlabel('Wavenumber [1/m]')
plt.ylabel('PSD [m^2/m]')

if savefig==True: plt.savefig(path_fig + 'mean_spectra.png')
# %%
import cmocean
plt.figure()
for i in range(da.swath.size):
    plt.scatter(da.sel(swath=i).longitude, da.sel(swath=i).latitude, s=1, c=da.sel(swath=i).ssh_karin, cmap=cmocean.cm.thermal)
plt.xlabel('Longitude')
plt.ylabel('Latitude')
plt.colorbar()
if savefig==True: plt.savefig(path_fig + 'swath_map.png')

# %%# nadir spectra
# ds = xr.open_dataset(files[1])
# # drop longitude and latitude
# ds = ds.drop(['latitude', 'longitude'])
# ds.isel(num_pixels=10).swh_nadir_altimeter.plot()


# %%
#plot spectra
plt.figure(figsize=(10, 8))
for i in range(P_nadir.swath.size):
    plt.loglog(P_nadir.freq_num_lines, P_nadir.sel(swath=i).values, color='grey', alpha=0.5)

plt.loglog(P_nadir.freq_num_lines, P_nadir.mean(dim='swath').values, color = 'red', label='mean')




# %%
from matplotlib.legend_handler import HandlerLine2D, HandlerTuple 
from matplotlib.legend import Legend
from scipy.stats import chi2
from scipy.special import digamma
from scipy.stats import t

def is_odd(num):
    return num & 0x1

def plot_slopes_ssh3(horizontal, f_sat, BM, f_smt, AM, f_r2b8, CM, powerlaw, axs, idx1, idx2, REG):
    # shift regressions
    shift = 80
    start = 10

    # slope smt
    if horizontal == True:
        end   = -int(39*f_smt.size/40)
        xdata = f_smt[start:end]
        ydata = AM[start:end]
    else:
        end   = -int(39*f_smt.size/40)
        xdata = f_smt[start-2:end]
        ydata = AM[start-2:end]
    
    logx  = np.log10(xdata)
    logy  = np.log10(ydata)
    res   = stats.linregress(logx, logy)
    index = res.slope
    amp   = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="royalblue", label=f'slope {index:3.3}')  

    # slope satellite
    if horizontal == True:
        end   = -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata = BM[start:end]
    else:
        end   = -int(f_sat.size/4)
        xdata = f_sat[start:end]
        ydata = BM[start:end]
    
    logx  = np.log10(xdata)
    logy  = np.log10(ydata)
    res   = stats.linregress(logx, logy)
    index = res.slope
    amp   = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tab:orange", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index),linewidth=1, linestyle='dashed', color="tab:orange", label=f'slope {index:3.3}')  

    # slope r2b8
    if horizontal == True:
        end   = -int(75)
        xdata = f_r2b8[start:end]
        ydata = CM[start:end]
    else:
        end   = -int(60)
        xdata = f_r2b8[start:end]
        ydata = CM[start:end]
    
    logx  = np.log10(xdata)
    logy  = np.log10(ydata)
    res   = stats.linregress(logx, logy)
    index = res.slope
    amp   = 10**res.intercept
    if REG == True: axs[idx1,idx2].plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="tab:green", label=f'slope {index:3.3}')  
    else: plt.plot(xdata, powerlaw(xdata, shift*amp, index), linewidth=1, linestyle='--', color="tab:green", label=f'slope {index:3.3}')  

def calc_CI(m, s, dof, confidence, realisations):
    t_crit = np.abs( t.ppf((1-confidence)/2, dof))
    A = (m-s*t_crit/np.sqrt(realisations), m+s*t_crit/np.sqrt(realisations))
    return A

def plot_spatial_spectra_ssh_ci3(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name,  K, axs, horizontal, n, n_minus_flag):
    powerlaw = lambda x, amp, index: amp * (x**index)
    
    if K == 0: idx1, idx2 = 0,0
    elif K == 1: idx1, idx2 = 0,1
    elif K == 2: idx1, idx2 = 1,0
    elif K == 3: idx1, idx2 = 1,1
    elif K == 4: idx1, idx2 = 2,0
    elif K == 5: idx1, idx2 = 2,1
    else:
        raise ValueError('K must be between 0 and 5')

    plot_slopes_ssh3(horizontal, f_sat, BM, f_smt, AM, f_r2b8, CM, powerlaw, axs, idx1, idx2, REG=True)
    axs[idx1,idx2].legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.5,0.0), frameon=False)

    s     = 0.05
    alpha = 0.8
    for ii in np.arange(n_minus_flag):
        axs[idx1,idx2].scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_sat, BB[ii,:], color='wheat', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(n):
        axs[idx1,idx2].scatter(f_r2b8, CC[ii,:], color='tab:green', s=s, alpha=alpha, rasterized=True)

    confidence = 0.95

    n_smt      = n_minus_flag#*int(984/11)
    CI         = calc_CI(AM, AM_std, n_smt-1, confidence, n_smt)
    l1, = axs[idx1,idx2].loglog(f_smt, AM, label='smt', color='royalblue', linewidth=1)
    f1  = axs[idx1,idx2].fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0,  color='royalblue', alpha=0.5, label=f'CI={confidence}')
    
    n_sat = n
    CI    = calc_CI(BM, BM_std, n_sat-1, confidence, n_sat)
    l2, = axs[idx1,idx2].loglog(f_sat, BM, label='sat', color='tomato', linewidth=1)
    f2  = axs[idx1,idx2].fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tomato', alpha=0.5, label=f'CI={confidence}')

    n_r2b8 = n
    CI     = calc_CI(CM, CM_std, n_r2b8-1, confidence, n_r2b8)
    l3, = axs[idx1,idx2].loglog(f_r2b8, CM, label='r2b8', color='tab:green', linewidth=1)
    f3  = axs[idx1,idx2].fill_between(f_r2b8, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:green', alpha=0.5, label=f'CI={confidence}')


    axs[idx1,idx2].autoscale(enable=True, tight=True)
    axs[idx1,idx2].set_ylim(1e-2, 1e5)

    leg = Legend(axs[idx1,idx2],
    [(l1, f1, f1), (l2, f2, f2), (l3, f3, f3)], # type: ignore
    [f"ICON-SMT ", f"Aviso ", f"ICON-R2B8 "],
    fontsize=15,
    loc="lower left",
    frameon=False,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    axs[idx1,idx2].add_artist(leg) #add artificial legend 

    #axs[idx1,idx2].legend(fontsize=15, loc='lower left')
    axs[idx1,idx2].tick_params(labelsize=15)
    if K >= 4: axs[idx1,idx2].set_xlabel('Wavenumber (1/m)', fontsize=15)
    if is_odd(K)== False: axs[idx1,idx2].set_ylabel('Power Spectral Density m^2/(1/m)',fontsize=15)
    axs[idx1,idx2].set_title(f'{reg_name}',fontsize=15)


# %%
n            = 10
S            = 6 
savefig      = True
horizontal   = bool(False)
calc_spectra = bool(True)
parent_dat   = '/work/mh0033/m300878/model_evaluation/ssh/data/'

if horizontal  == True:
   parent_imag  = '/work/mh0033/m300878/model_evaluation/ssh/images/horizontal/'
   regions      = 'A', 'B', 'C', 'D', 'E', 'F'
   reg_name     = 'zonal'
else: 
   parent_imag  = '/work/mh0033/m300878/model_evaluation/ssh/images/vertical/'
   regions      = 'a', 'b', 'c', 'd', 'e', 'f'
   reg_name     = 'meridional'
 


# Visualization           
reload(eva)
if horizontal == False: 
    AAA = np.zeros((S,int(1001))); AAAA = np.zeros((S*n,int(1001))) 
    BBB = np.zeros((S,int(37)));   BBBB = np.zeros((S*n,int(37)))
    CCC = np.zeros((S,int(91)));   CCCC = np.zeros((S*n,int(91)))
else: 
    AAA = np.zeros((S,int(1401))); AAAA = np.zeros((S*n,int(1401)))
    BBB = np.zeros((S,int(45)));   BBBB = np.zeros((S*n,int(45)))  
    CCC = np.zeros((S,int(111)));  CCCC = np.zeros((S*n,int(111)))


fig, axs = plt.subplots(3,2, figsize=(20,25))

for K in range(S):
    reg_name = f'{regions[K]}'
    path_dat  = f'{parent_dat}{reg_name}/'
    path_imag = f'{parent_imag}{reg_name}/'
    
    print('plot temporal averaged and temporal + spatial averaged spectra')
    # plot each section
    search_str = f'data_sat*.npy' 
    flist      = np.array(glob.glob(path_dat+search_str))
    flist.sort()
    sat_sections = flist
    search_str   = f'data_smt*.npy'
    flist        = np.array(glob.glob(path_dat+search_str))
    flist.sort()
    smt_sections = flist
    search_str   = f'data_r2b8_mep*.npy'
    flist        = np.array(glob.glob(path_dat+search_str))
    flist.sort()
    r2b8_sections = flist

    #load time averages
    for ii in np.arange(n):
        section = r2b8_sections[ii]
        [A_r2b8, f_r2b8, dm_r2b8, A_r2b8_mean, std_r2b8] = np.load(f'{section}',allow_pickle=True)
        section = smt_sections[ii]
        [A_smt, f_smt, dm_smt, A_smt_mean, std_smt] = np.load(f'{section}',allow_pickle=True)
        section = sat_sections[ii]
        [A_sat, f_sat, dm_sat, A_sat_mean, std_sat] = np.load(f'{section}',allow_pickle=True)

        path_save_name = f'{path_imag}ssh_sec_{section[-15:-4:]}'
        figname        = f'{reg_name}{section[-15:-4:]}'
        #plot_spatial_spectra_horizontal(A_smt_mean, std_smt, f_smt, A_sat_mean, std_sat, f_sat, kk, figname, path_save_name) # if activate region plot doesent work figure in figure

    # average over space n=10
    AA = np.zeros((n,A_smt_mean.size))
    BB = np.zeros((n,A_sat_mean.size))
    CC = np.zeros((n,A_r2b8_mean.size))

    for ii in np.arange(n):
        section = smt_sections[ii]
        [A_smt, f_smt, dm_smt, A_smt_mean, std_smt] = np.load(f'{section}',allow_pickle=True)
        AA[ii,:] = A_smt_mean
        if ( (horizontal == True) & (K == 5)) & (ii==1): AA[ii,:] = np.nan; print(f'flag in region {regions[K]} at sec n={ii}'); flag = horizontal, K, n #flag for horizontal sst
        if ( (horizontal == True) & (K == 5)) & (ii==3): AA[ii,:] = np.nan; print(f'flag in region {regions[K]} at sec n={ii}'); flag = horizontal, K, n #flag for horizontal sst

        #satelite
        section = sat_sections[ii]
        [A_sat, f_sat, dm_sat, A_sat_mean, std_sat] = np.load(f'{section}',allow_pickle=True)
        BB[ii,:] = A_sat_mean

        # r2b8
        section = r2b8_sections[ii]
        [A_r2b8, f_r2b8, dm_r2b8, A_r2b8_mean, std_r2b8] = np.load(f'{section}',allow_pickle=True)
        CC[ii,:] = A_r2b8_mean

    AM = np.nanmean(AA, axis=0)
    BM = np.mean(BB, axis=0)
    CM = np.mean(CC, axis=0)
    
    AAA[K,:] = AM
    BBB[K,:] = BM
    CCC[K,:] = CM

    start = K*n; end = K*n + n
    AAAA[start:end,:] = AA
    BBBB[start:end,:] = BB
    CCCC[start:end,:] = CC
    
    AM_std = np.nanstd(AA, axis=0, ddof=1)
    BM_std = np.std(BB, axis=0, ddof=1)
    CM_std = np.std(CC, axis=0, ddof=1)

    #plot time and space averages
    #plot_spatial_spectra6(AM, AM_std, f_smt, BM, BM_std, f_sat, kk, reg_name, K, axs)
    if ((horizontal ==  True) & (K == 5)): n_minus_flag = n-2; 
    else: n_minus_flag = n
    # eva.plot_spatial_spectra_ssh_ci(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, reg_name,  K, axs, horizontal, n, n_minus_flag)
    plot_spatial_spectra_ssh_ci3(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name,  K, axs, horizontal, n, n_minus_flag)


path_save_name = f'{parent_imag}ssh_sec_ave_reg_mep' 
# if savefig == True: plt.savefig(f'{path_save_name}.png', bbox_inches='tight', dpi=200)
# if savefig == True: plt.savefig(f'{path_save_name}.pdf', bbox_inches='tight', dpi=200)
# %%
def plot_spatial_spectra_ssh_all(AA, AM, AM_std, f_smt, BB, BM, BM_std, f_sat, CC, CM, CM_std, f_r2b8, reg_name, path_save_name, secs, horizontal, n, K, savefig):
    powerlaw = lambda x, amp, index: amp * (x**index)
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))

    # plot_slopes_ssh(horizontal, f_sat, BM, f_r2b8, CM, f_smt, AM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)
    # plot_slopes_ssh(horizontal, f_sat, BM, f_smt, AM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)
    plot_slopes_ssh3(horizontal, f_sat, BM, f_smt, AM, f_r2b8, CM, powerlaw, axs=ax, idx1=0, idx2=0, REG=False)

    plt.legend(fontsize=15, loc='lower center', bbox_to_anchor=(0.3,0.0), frameon=False)

    s=0.05
    alpha = 0.8
    for ii in np.arange(K*n):
        plt.scatter(f_smt, AA[ii,:], color='lightgrey', s=s, alpha=alpha, rasterized=True)
    
    for ii in np.arange(K*n):
        plt.scatter(f_sat, BB[ii,:], color='tab:orange', s=s, alpha=alpha, rasterized=True)

    for ii in np.arange(n):
        ax.scatter(f_r2b8, CC[ii,:], color='tab:green', s=s, alpha=alpha, rasterized=True)

    for i in range(P_all.swath.size):
        plt.loglog(P_all.freq_num_lines, P_all.sel(swath=i).values, color='grey', alpha=0.5)

    plt.loglog(P_all.freq_num_lines, P_all.mean(dim='swath').values, color = 'red', label='SWOT')


    confidence = 0.95
    n_smt = K*n#*int(984/11)
    CI    = calc_CI(AM, AM_std, n_smt-1, confidence, n_smt)
    l1,   = ax.loglog(f_smt, AM, label='smt', color='royalblue', linewidth=1)
    f     = ax.fill_between(f_smt, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='royalblue', alpha=0.5, label=f'CI={confidence}')

    n_sat = K*n#*90
    CI    = calc_CI(BM, BM_std, n_sat-1, confidence, n_sat)
    l2,   = ax.loglog(f_sat, BM, label='sat', color='tab:orange', linewidth=1)
    f2    = ax.fill_between(f_sat, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:orange', alpha=0.5, label=f'CI={confidence}')

    n_r2b8 = K*n
    CI     = calc_CI(CM, CM_std, n_r2b8-1, confidence, n_r2b8)
    l3, = ax.loglog(f_r2b8, CM, label='r2b8', color='tab:green', linewidth=1)
    f3  = ax.fill_between(f_r2b8, CI[0], CI[1],  facecolor="none", linewidth=0.0, color='tab:green', alpha=0.5, label=f'CI={confidence}')

    #---------------------------------------#
    x = f_smt
    def forward(x):
        return 1 / x / 1000
    def inverse(x):
        return 1 / x 
    secax = ax.secondary_xaxis('top', functions=(forward, inverse))
    secax.set_xlabel('wavelength in [km]', fontsize=15)

    ax.autoscale(enable=True, tight=True)
    ax.set_ylim(1e-3, 1e6)
    # ax.set_xlim(1e-7, 1e-2)
    fig.tight_layout()

    leg = Legend(ax,
    [(l1, f, f), (l2, f2, f2), (l3, f3, f3)],
    [f"ICON-SMT ", f"Aviso ", f"ICON-R2B8 "],
    fontsize=15,
    loc="lower left",
    frameon=False,
    #handlelength=3.0,
    handler_map={
        tuple: HandlerTuple(ndivide=1, pad=0.0),
    },
    ) 
    ax.add_artist(leg) #add artificial legend 
    
    ax.tick_params(labelsize=15)
    secax.tick_params(labelsize=15)
    plt.grid(ls=':')
    plt.xlabel(r'Wavenumber [1/m]', fontsize=15)
    plt.ylabel(r'Power Spectral Density $m^2/(1/m)$',fontsize=15)
    # plt.title(f'Temporal and spatially averaged spectral estimates of {reg_name} SSH sections',fontsize=15)
    # if savefig == True: plt.savefig(f'{path_save_name}.pdf', bbox_inches='tight', dpi=200)
    if savefig == True: plt.savefig(f'{path_save_name}.png', bbox_inches='tight', dpi=200)


reload(eva)
# average over all vertiacl regions
AAM = np.nanmean(AAA, axis=0)
BBM = np.mean(BBB, axis=0)
CCM = np.mean(CCC, axis=0)

AAM_std = np.nanstd(AAA, axis=0, ddof=1)
BBM_std = np.std(BBB, axis=0, ddof=1)
CCM_std = np.std(CCC, axis=0, ddof=1)

# path_save_name = f'{parent_imag}ssh_sec_ave_all_mep'
path_save_name = path_fig +'meridional'
if horizontal==True: reg_name='zonal'
else: reg_name='meridional'
#plot_spatial_spectra_all_vertical(AAA, AAM, AAM_std, f_smt, BBB, BBM, BBM_std, f_sat, kk, reg_name, path_save_name)
secs=6
plot_spatial_spectra_ssh_all(AAAA, AAM, AAM_std, f_smt, BBBB, BBM, BBM_std, f_sat, CCCC, CCM, CCM_std, f_r2b8, reg_name, path_save_name, secs, horizontal, n, K, savefig)




# %%
