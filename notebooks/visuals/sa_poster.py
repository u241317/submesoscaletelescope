# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../')
# import funcs as fu

from matplotlib.patches import Rectangle

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
import math

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %%
gg  = eva.load_smt_wave_grid()
res = np.sqrt(gg.cell_area_p)
res = res.rename(cell='ncells')
fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'

# %%
#

mask_land = eva.load_smt_wave_land_mask()
ds_0 = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190704T001500Z.nc', chunks=dict(depth=1))

# vort = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190818T211500Z.nc', chunks=dict(depth=1))
# vort = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20200118T211500Z.nc', chunks=dict(depth=1))
vort = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190901T211500Z.nc', chunks=dict(depth=1))

vort = vort.vort.isel(depth=1).isel(time=0)
lat  = np.rad2deg(ds_0.vlat)
f    = eva.calc_coriolis_parameter(lat)
fcor = 2. * 2.*np.pi/86400.*np.sin(ds_0.vlat)
fcor = fcor.compute()
Ro   = vort / f
# %%
lon_reg = -5,20
# lat_reg = -50,0
asp_poster = 84.11/(118.91/3)

lat_reg_lower = -60
lat_reg_upper = asp_poster*(lon_reg[1]-lon_reg[0])+lat_reg_lower
lat_reg = lat_reg_lower, lat_reg_upper
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])


fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'

data_vort_coarse = pyic.interp_to_rectgrid_xr(Ro, fpath_ckdtree, coordinates='vlat vlon', lon_reg=lon_reg, lat_reg=lat_reg)
data_vort_coarse.plot(vmin=-1,vmax=1, cmap='RdBu_r')
data_mask_coarse = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_vort_coarse = data_vort_coarse * data_mask_coarse
# %%
data_r = pyic.interp_to_rectgrid_xr(res, fpath_ckdtreeZ, lon_reg=lon_reg, lat_reg=lat_reg) # does not work properly with xarray...?

# %%
def plot_vort_sel(data_vort, lon_reg, lat_reg, data_r, fig_path, savefig):
    cmap  = 'RdBu_r'
    dpi   = 250
    clim  = 1
    asp   = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

    hue_neg, hue_pos = 20, 250
    cmap  = f'sns–{hue_neg}-{hue_pos}-1'
    fname = f'sa_poster_reg'
    import seaborn as sns
    cmap = sns.diverging_palette(hue_neg, hue_pos, center="light", as_cmap=True)
    cmap='RdBu_r'

    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=asp, fig_size_fac=21, projection=ccrs_proj, axlab_kw=None)
    ax=hca[0]; cax=hcb[0]
    hm1 = pyic.shade(data_vort.lon, data_vort.lat, data_vort, ax=ax, cax=cax, clim=clim, cmap = cmap, transform=ccrs_proj, rasterized=False)


    levels = 600+np.arange(20)*50
    CS     = ax.contour(data_r.lon, data_r.lat, data_r, levels, linewidths=0.8, colors='grey', linestyles='solid' )
    fmt    = '%.0f'+'m'
    l      = ax.clabel(CS,  inline=True, fmt=fmt, fontsize=20)
    for t in l:
        t.set_bbox(dict(facecolor='white', alpha=0.9, edgecolor='white', pad=0.1))

    # change color of land
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
    ax.set_yticks([])
    ax.set_xticks([])
    # ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
    land_color = '0.9'
    ax.add_feature(cartopy.feature.LAND, edgecolor='none', facecolor=land_color, zorder=100)
    # add coastlines
    ax.coastlines(resolution='50m', color='black', linewidth=1.5, zorder=101)
    # rectprop = dict(facecolor='none', edgecolor='0.7', lw=2)
    # ax.add_patch(Rectangle((-12,-37.5), 5, 4, **rectprop))
    # ax.text(-9.5, -33.5, 'SONETT II', ha='center', va='bottom', fontsize=8)

    rectprop = dict(facecolor='none', edgecolor='0.7', lw=4)
    ax.add_patch(Rectangle((1,-35), 9, 5, **rectprop))
    ax.text(5.5, -30., 'SONETT', ha='center', va='bottom', fontsize=30)

    lon_zoom = [10.2,13.2]#[-36,-32.1]
    lat_zoom = [-36.5, -33.5]
    rectprop = dict(facecolor='none', edgecolor='0.7', lw=4)
    ax.add_patch(Rectangle((lon_zoom[0],lat_zoom[0]), lon_zoom[1]-lon_zoom[0], lat_zoom[1]-lat_zoom[0], **rectprop))
    ax.text(np.mean(lon_zoom), -37, 'Anticyclone', ha='center', va='center', fontsize=25)

    lon_zoom = [12.5, 14.5]
    lat_zoom = [-34, -32]
    rectprop = dict(facecolor='none', edgecolor='0.7', lw=4)
    ax.add_patch(Rectangle((lon_zoom[0],lat_zoom[0]), lon_zoom[1]-lon_zoom[0], lat_zoom[1]-lat_zoom[0], **rectprop))
    ax.text(np.mean(lon_zoom), -31.75, 'Cyclone', ha='center', va='center', fontsize=25)

    m_pos = np.array([((7.11666667), (-32.69666667)),((4.63333333), (-32.18833333))])
    ax.plot(m_pos[:,0], m_pos[:,1], 'r*', markersize=35)
    ax.text(-2.5, -35, 'Walvis Ridge', va='center', ha='center', rotation=45, fontsize=25)

    #draw circle with 10 degree radius around mean position
    m_pos_mean = np.mean(m_pos, axis=0)
    ax.add_patch(plt.Circle((m_pos_mean[0], m_pos_mean[1]), 10, fill=False, color='red', lw=1))

    # add cape town use black dot with circle as marker
    ax.plot(18.4232, -33.918, 'ko', markersize=15, zorder=110)
    ax.text(18.4232, -33.918, 'Cape \n Town', va='bottom', ha='left', fontsize=20, zorder=111)
    # add walvis bay
    ax.plot(14.5053, -22.9575, 'ko', markersize=15, zorder=110)
    ax.text(14.5053, -22.9575, 'Walvis Bay', va='bottom', ha='left', fontsize=20, zorder=111)

    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=dpi, format='png', bbox_inches='tight')

# %%
fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/high_res/'
savefig  = True
plot_vort_sel(data_vort_coarse, lon_reg, lat_reg, data_r, fig_path, savefig)
# %%
