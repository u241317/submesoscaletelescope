# %%
import sys
import glob, os
import pyicon as pyic
import sys
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
# import funcs1 as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# %% Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
# from validate_catalog import all_params
# params_dict, cat = all_params()
# params_dict.keys()
import gcm_filters

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, dash_address='8787')
cluster
client

# %%
# fig_path = '/work/mh0033/m300878/videos/lee_waves/time_steps/'
fig_path = '/work/mh0033/m300878/videos/lee_waves/wavenumber/'
savefig  = False 

#%%
#if single
exp=7
path = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv000{exp}/outdata_w/smtwv000{exp}_oce_3d_w_PT1H_20190721T051500Z.nc'
ds = xr.open_dataset(path)

ds = ds.drop('clon').drop('clat').drop('clon_bnds').drop('clat_bnds')
#%%


#%%
def interpolate_reg(ds):
    fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
    # fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
    lat_reg = [-40, -25]
    lon_reg = [0, 15]
    sel             = ds.isel(time=0, depth=90).w.compute()
    data = pyic.interp_to_rectgrid_xr(sel, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    # Tri_tke, data_reg_tke   = eva.calc_triangulation_obj_wave(tke, lon_reg2, lat_reg2)
    return data

data_exp7 = interpolate_reg(ds)
data_exp9 = interpolate_reg(ds)
# %%
fig, ax = plt.subplots(figsize=(11, 10), subplot_kw={'projection': ccrs.PlateCarree()})

(data_exp7).plot(vmin=-1e-3, vmax=1e-3, cmap='Greys')

plt.savefig(f'{fig_path}/tides_w_{sel.depth.values:2f}.png', dpi=300)

#%%


#%%
data_sel = data_exp7.where(data_exp7 != 0., np.nan)
samp_freq = 0.01*eva.convert_degree_to_meter(data_sel.lat.mean().values)/-1000
def compute_wavenumber_spec(data, samp_freq):
    #interpolate where w is excatly 0.0000000
    data_sel = data
    #fill na with linear interpolation
    def compute_spectra_uchida(data, samp_freq=2000):
        data['lat'] = np.arange(int(data.lat.size))*samp_freq
        P = xrft.power_spectrum(data, dim=['lat'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
        P = P.isel(lon=slice(len(P.lon)//2,None)) * 2
        return P
    P_lon = compute_spectra_uchida(data_sel.fillna(0.), samp_freq=samp_freq)
    Plo_mean = P_lon.mean(dim='lon')

    def compute_spectra_uchida(data, samp_freq=2000):
        data['lon'] = np.arange(int(data.lat.size))*samp_freq
        P = xrft.power_spectrum(data, dim=['lon'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
        P = P.isel(lat=slice(len(P.lat)//2,None)) * 2
        return P

    P_lat = compute_spectra_uchida(data_sel.fillna(0.), samp_freq=samp_freq)

    Pla_mean = P_lat.mean(dim='lat')
    #are identical
    return Pla_mean

P_exp7 = compute_wavenumber_spec(data_exp7, samp_freq)
P_exp9 = compute_wavenumber_spec(data_exp9, samp_freq)



#%%
plt.figure(figsize=(13, 10))
data_exp7.plot(cmap='Greys', vmin=-1e-3, vmax=1e-3)
#plot red line bewteen two points
P1 = [6.5,-34]
P2 = [14,-39]
plt.plot([P1[0], P2[0]], [P1[1], P2[1]], color='r', linewidth=2)

P1s = [7,-31]
P2s = [5.5,-30.25]
plt.plot([P1s[0], P2s[0]], [P1s[1], P2s[1]], color='b', linewidth=2)
# compute distance between two points on sphere
def haversine(lon1, lat1, lon2, lat2):
    R = 6371.0088
    dlat = np.radians(lat2 - lat1)
    dlon = np.radians(lon2 - lon1)
    a = np.sin(dlat/2)**2 + np.cos(np.radians(lat1)) * np.cos(np.radians(lat2)) * np.sin(dlon/2)**2
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    return R * c

dist = haversine(P1[0], P1[1], P2[0], P2[1])
wave_count = 5
wave_length = dist / wave_count
print(f'M2 Wave length: {wave_length} km')

dists = haversine(P1s[0], P1s[1], P2s[0], P2s[1])
wave_count = 17
wave_lengths = dists / wave_count
print(f'Lee Wave length: {wave_lengths} km')


# plt.xlim(5,8)
# plt.ylim(-32,-30)
plt.savefig(f'{fig_path}/tides_w_{data_exp9.depth.values:2f}.png', dpi=300)

# %%
plt.figure(figsize=(10, 6))
plt.loglog(P_exp7.freq_lon, P_exp7, label='ICON-SMT-Wave', color='k')
plt.loglog(P_exp9.freq_lon, P_exp9, label='ICON-SMT', color='tab:blue')
# plt.loglog(Pla_mean.freq_lon, Pla_mean, label='lon')
plt.xlabel('wavenumber [1/km]')
plt.ylabel('PSD [km^2/s^2]')
plt.xlim(1/1211, 1/samp_freq/2)
plt.legend()
plt.grid()
#add vertical line for wave length
plt.axvline(1/wave_length, color='r', linestyle='-', label='M2 wavelength')
plt.axvline(1/wave_lengths, color='b', linestyle='-', label='Lee wavelength')
#secondary topaxis
ax2 = plt.gca().secondary_xaxis('top', functions=(lambda x: 1/x, lambda x: 1/x))
ax2.set_xlabel('wavelength [km]')
plt.savefig(f'{fig_path}/w_wavespec_{data_exp9.depth.values:2f}_spec.png', dpi=300)

#%%
ds = eva.load_smt_wave_all('w', 7, it=1)
ds = ds.drop('clon').drop('clat').drop('clon_bnds').drop('clat_bnds')
#%%

#make video at new depth
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
lat_reg = [-40, -25]
lon_reg = [0, 15]
# start = '2019-07-15T03:15:00.000000000'
# end = '2019-08-31T23:15:00.000000000'

# ds_sel = ds.sel(time=slice(start, end))
frames = ds.time.size
clim = [-1e-3, 1e-3]
cmap = 'Greys'
asp = (lat_reg[1] - lat_reg[0]) / (lon_reg[1] - lon_reg[0])

def plot(data, frame):
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb='bottom', asp=asp, fig_size_fac=3, axlab_kw=None, projection=ccrs_proj) # type: ignore
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    hm = pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap)

    ax.set_title(f'{data.time.values:.13}h - {data.depth.values:.5} m')
    hm[1].set_label('w[m/s]')

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    plt.savefig(f'{fig_path}/tides1_w_{data.depth.values:.5}_{frame}.png', dpi=300)
    plt.close()

for frame in np.arange(frames):
    frame = 310 + frame 
    print(frame)
    data = ds.isel(time=frame, depth=90).w.compute()
    data = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    plot(data, frame)


# %% ################################### Hovmöller plot hor. section ###################################
#load eddytracks
if False:
    path = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/anticyclonic/ID0/w/'
    name = 'anticyclonic_2.5x2.5deg_trackID_0time_*'
    files = glob.glob(path+name+'.nc')
    files.sort()
    files = files[:700]
    ds = xr.open_mfdataset(files, concat_dim='time', combine='nested', parallel=False)

    #%%
    ds.loncen.to_netcdf('/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/anticyclonic/ID0/loncen.nc')
    ds.latcen.to_netcdf('/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/anticyclonic/ID0/latcen.nc')
else:
    #%%
    loncen = xr.open_dataarray('/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/anticyclonic/ID0/loncen.nc')
    latcen = xr.open_dataarray('/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/anticyclonic/ID0/latcen.nc')
    

    #drop duplicates time
    loncen = loncen.drop_duplicates('time')
    core = loncen.interp(time=data.time)
#%%

fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.005_20W-20E_50S-10S.nc'
fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'


data = pyic.interp_to_rectgrid_xr(ds.isel(depth=90, time=slice(0,700)), fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
if False:
    xmin=0
    xmax=700
    plt.figure(figsize=(25,15))
    data.isel(lon=839, time=slice(xmin,xmax)).w.plot(x='time', y='lat', cmap='Greys', vmin=-1.5e-3, vmax=1.5e-3)
    # with logarithmic colormap
    # data.isel(lon=400).w.plot(x='time', y='lat', cmap='Spectral_r', norm=clr.SymLogNorm(linthresh=0.03, linscale=0.03, vmin=-1e-3, vmax=1e-3))
    # increse number of x ticks to every 2h
    # plt.xticks(np.arange(0, 700, 48), np.arange(0, 700, 48)*2)
    #plto vline at 2019-07-21 00:00
    #getxlim
    xlim = plt.gca().get_xlim()
    plt.hlines(-31.65, xlim[0], xlim[1],  color='tab:brown', linestyle='-.', linewidth=4)
    plt.axvline(150, color='r', linestyle='-', linewidth=1)
    plt.savefig(f'{fig_path}/notides_w_{data.depth.values:2f}_hov_lat-test1.png', dpi=300)

    #%%
    xmin=0
    xmax=700
    plt.figure(figsize=(25,15))
    data.isel(lat=839, time=slice(xmin,xmax)).w.plot(x='time', y='lon', cmap='Greys', vmin=-1.5e-3, vmax=1.5e-3)

    xlim = plt.gca().get_xlim()
    plt.hlines(8.39, xlim[0], xlim[1],  color='tab:brown', linestyle='-.', linewidth=4)
    plt.axvline(150, color='r', linestyle='-', linewidth=1)
    plt.ylim(2,13)
    plt.savefig(f'{fig_path}/notides_w_{data.depth.values:2f}_hov_lon-tes31.png', dpi=300)

 #%%

def plot_hov_wave(data, core, fig_path, savefig=False):
    fig, axs =plt.subplots(2,1, figsize=(15,25))
    # data.isel(lat=839, time=slice(xmin,xmax)).w.plot(x='time', y='lon', cmap='Greys', vmin=-1.5e-3, vmax=1.5e-3)
    cmap = 'Greys'
    vmin = -1.5e-3
    vmax = 1.5e-3

    core = core.drop_duplicates('time')
    core = core.interp(time=data.time)
    #select common time steps
    core = core.sel(time=slice(data.time[0], data.time[-1]))
    # latcen = latcen.sel(time=data.time)
    # sel =280
    # centt = loncen.time.size-sel

    tt = data.time.size

    ax = axs[0]
    ax.pcolormesh(np.arange(tt), data.lon, data.isel(lat=839).w.transpose(), cmap=cmap, vmin=vmin, vmax=vmax)
    ax.scatter(np.arange(tt), core.loncen, color='r', s=4)
    xlim = plt.gca().get_xlim()
    ax.set_ylim(2,13)
    # ax.axvline(242, color='r', linestyle='-', linewidth=1)
    ax.set_title(f'w at {data.depth.data:.0f}m and {data.isel(lat=839).lat:.2f}°S', fontsize=24)
    ax.set_xticks(np.arange(0, tt, 12))
    ax.set_xticklabels([f'{int(x*2/24)}' for x in np.arange(0, tt, 12)], fontsize=6+10)
    ax.set_ylabel('lon', fontsize=12+10)

    ax = axs[1]
    ax.pcolormesh(np.arange(tt), data.lat, data.isel(lon=839).w.transpose(), cmap=cmap, vmin=vmin, vmax=vmax)
    ax.scatter(np.arange(tt), core.latcen, color='r', s=4)
    # ax.axvline(242, color='r', linestyle='-', linewidth=1)
    xlim = plt.gca().get_xlim()
    ax.set_ylim(-37.5,-27)
    ax.set_title(f'w at {data.depth.data:.0f}m and {data.isel(lon=839).lon:.2f}°E', fontsize=24)
    ax.set_xticks(np.arange(0, tt, 12))
    ax.set_xticklabels([f'{int(x*2/24)}' for x in np.arange(0, tt, 12)], fontsize=6+10)
    ax.set_xlabel('time [days]', fontsize=12+10)
    ax.set_ylabel('lat', fontsize=12+10)
    #increase y ticks size
    for ax in axs:
        ax.tick_params(axis='y', labelsize=6+10)
        ax.tick_params(axis='x', labelsize=6+10)
    plt.subplots_adjust(hspace=0.1)  # Adjust hspace to reduce space between subplots


    if savefig ==True: plt.savefig(f'{fig_path}/notides_w_{data.depth.values:2f}_hov_lon_lat_short.png', dpi=300, bbox_inches='tight')

plot_hov_wave(data9.isel(time=slice(0,400)), core, fig_path, savefig=True)

#%%
def plot_hov_0(hov, savefig=False, cmap=None, vmin=None, vmax=None, fig_path=None):
    tt = hov.shape[0]
    idy = 125
    iday = 315

    fig = plt.figure(figsize=(20, 5))
    gs = fig.add_gridspec(2, 2, height_ratios=[1, 5])

    ax0 = fig.add_subplot(gs[0, 0])
    ax0.plot(np.arange(tt), hov.isel(theta=idy), color='k', lw=1)
    ax0.set_xticks([])  # Hide x-ticks for the first subplot
    ax0.set_title(f'Hovmöller Diagram of {hov.name} at {hov.depth.data:.0f}m', fontsize=14)
    ax0.set_xlim(0, tt)

    ax1 = fig.add_subplot(gs[1, 0])
    if vmin==None and vmax==None:
        cb = ax1.pcolormesh(np.arange(tt), hov.theta / np.pi, hov.transpose(), cmap=cmap)
    else:
        cb = ax1.pcolormesh(np.arange(tt), hov.theta / np.pi, hov.transpose(), cmap=cmap, vmin=vmin, vmax=vmax)
    fig.colorbar(cb, ax=ax1, orientation='horizontal', pad=0.13, aspect=40, shrink=0.8)
    ax1.set_xlabel('time [days]', fontsize=12)
    ax1.set_ylabel(r'$\theta / \pi$', fontsize=12)
    ax1.axhline(hov.theta[idy].data/np.pi, color='white', linestyle='-', linewidth=1)
    ax1.axvline(iday, color='white', linestyle='-', linewidth=1)
    ax1.set_xticks(np.arange(0, tt, 12))
    ax1.set_xticklabels([f'{int(x*2/24)}' for x in np.arange(0, tt, 12)], fontsize=6)

    ax2 = fig.add_axes([0.522, 0.225, 0.037, 0.509]) 
    ax2.plot(hov.isel(time=iday), hov.theta/np.pi, color='k', lw=1)
    ax2.set_ylim(0, 2)
    ax2.set_yticks([])
    ax2.tick_params(axis='x', labelsize=6)

    plt.tight_layout()

    if savefig==True: plt.savefig(fig_path+f'hov_{hov.name}_{hov.depth.data:.0f}.png', dpi=180, bbox_inches='tight')











# %%  ########################## compute wavelengths #######################

plt.figure(figsize=(10,10))
data.w.isel(time=50).plot(vmin=-1e-3, vmax=1e-3, cmap='Greys')
# %%

f       = np.abs(eva.calc_coriolis_parameter(-32)) #rad/s
f       = f*3600*24
P       = 9/7
w_count = 2*np.pi/P
w_disp  = f* np.cos(np.radians(-43))

print(f'Coriolis frequency: {f} 1/day')
print(f'Wave period: {P} days')
print(f'Wave freq: {w_count} 1/day')
print(f'Wave freq. from dispersion: {w_disp} 1/day')

# %%
omega = 2*np.pi*10/(7*86400.) # with 10 cycles per 7 days
omega_earth = 2*np.pi/(86400.) 
# f = 2*omega_earth*sin(phi) => phi = asin(f/(2(omega_earth); with f=omega:
phi = np.arcsin(2.*np.pi*10/(7.*86400.) * (1*86400./(2.*2.*np.pi))) * 180./np.pi
# >> 45deg
print(f'omega: {omega} 1/days')
print(f'omega_earth: {omega_earth} 1/days')
print(f'phi: {phi} deg')

# %%
