# %%
import sys
import glob, os
import pyicon as pyic
import sys
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
# import funcs1 as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# %% Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
# from validate_catalog import all_params
# params_dict, cat = all_params()
# params_dict.keys()
import gcm_filters

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, dash_address='8787')
cluster
client

#%%
fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/grad_temp/eddy_sec/'

#%%
def plot_section(ds, t, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(1,2, plot_cb=True, asp=0.25, fig_size_fac=1.3, sharey=True, sharex=True, daxt=1, axlab_kw=None)
    ii=-1

    clim = -5e-1, 0
    cmap = 'Spectral_r'

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(rf'$dTdz$ at {ds.time.values:.13} ')
    hm2=pyic.shade(ds.dist.data/1000, ds.depth.data, ds.data, ax=ax, cax=cax, cmap=cmap, clim=clim)
    cax.set_title(r'$K/m$', pad=10)
    ax.set_ylim(800,0)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # ax.set_title(rf'$dTdz$ at {ds.time.values:.13} ')
    hm2=pyic.shade(ds.dist.data/1000, ds.depth.data, ds.data, ax=ax, cax=cax, cmap=cmap, clim=clim)
    cax.set_title(r'$K/m$', pad=10)
    ax.set_ylim(5600,0)
    ax.set_xlabel('distance [km]')


    for ax in hca:
        pyic.plot_settings(ax=ax)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) #type: ignore
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) #type: ignore
        # ax.legend(loc='lower left')
        ax.set_ylabel('depth [m]')

    if savefig:
        plt.savefig(f'{fig_path}dto_section_n{t}_eddy.png', dpi=300, bbox_inches='tight')
        plt.close()

#%%
#%%
# # ds = eva.load_smt_N2()
# ds = eva.load_smt_wave_b()
# ds = eva.load_smt_wave_all('w', 7, it=100)
#%%
ds = eva.load_smt_wave_all('w', 9)
ds = ds.drop('clon_bnds').drop('clat_bnds')
ds = ds.drop('clon').drop('clat')
# path = '/home/m/m300602/work/icon/grids/smt/smt_tgrid.nc'
#%%
#ray beam
p1 = [0, -32.5]
p2 = [12.5, -37.5]
# onshore waves
p1 = [12, -27]
p2 = [16, -25]
# eddy sec
p1 = [0, -29]
p2 = [10, -33]


distance = (p1[1]-p2[1])**2 + (p1[0]-p2[0])**2
#fpath_ckdtree  = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
lon_reg = np.array([-10, 20])
lat_reg = np.array([-40, -20])
#%%
path = '/home/m/m300602/work/icon/grids/smtwv_oce_2022/smtwv_oce_2022_tgrid.nc'

rev           = f''
tgname        = f'smtwv_oce_2022'
gname         = f'smtwv_oce_2022_tgrid'
# path_tgrid    = f'/home/m/m300602/work/icon/grids/smt/' 
path_tgrid    = f'/home/m/m300602/work/icon/grids/smtwv_oce_2022/' 
fname_tgrid   = f'{gname}.nc'
path_ckdtree  = f'/work/mh0033/m300878/grids/{tgname}/ckdtree/'
path_rgrid    = path_ckdtree + 'rectgrids/' 
path_sections = path_ckdtree + 'sections/' 
sname         = '62W_200pts'


dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=p1, p2=p2, npoints=1700,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname = sname,
                      gname = gname,
                      tgname = tgname,
                      )
#%%
ds_interp = pyic.interp_to_rectgrid_xr(ds.isel(time=220, depth=50).to.compute(), fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
plt.figure()
plt.pcolormesh(ds_interp.lon, ds_interp.lat, ds_interp, cmap='Spectral_r')
plt.scatter(lon_sec, lat_sec, c='k', s=1)
plt.savefig(f'{fig_path}dto_section__.png', dpi=100)

#%%


tstep = 150

S = []

for t in range(0, tstep):
    print(t)
    tt = 220+300+t
    data      = ds.to.isel(time=tt).compute()
    ds_sec    = data[:,ickdtree]

    # ds_sec[0,:] = np.nan
    # ds_sec[-1,:] = np.nan
    ds_sec['lat'] = xr.DataArray(lat_sec, dims='ncells')
    ds_sec['lon'] = xr.DataArray(lon_sec, dims='ncells')
    ds_sec['dist'] = xr.DataArray(dist_sec, dims='ncells')

    ds_sel = ds_sec.where(ds_sec != 0, np.nan)
    S.append(ds_sel)

    ds_sel_diff = ds_sel.diff('depth')

    print('plot section')
    # plt.figure(figsize=(40,10))
    # ds_sel.plot(y='depth', yincrease=False, vmin=-5e-1, vmax=0, cmap='Spectral_r')
    # plt.ylim(5600,0)
    # plt.title(f'dtdz at {ds_sel.time.values:.13} ')
    # plt.savefig(f'{fig_path}dto_section_n{t}_eddy.png', dpi=300, bbox_inches='tight')
    # plt.close()
    plot_section(ds_sel_diff, tt, fig_path, savefig=True)



print('finished plotting')
S = xr.concat(S, dim='time')
S.to_netcdf(f'/work/mh0033/m300878/smt/smt_wave/section_video/dto_section_eddy_part3.nc')

#%%
print('close cluster')
client.close()
cluster.close()

