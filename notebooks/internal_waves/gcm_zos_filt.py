# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd
import gsw
import matplotlib.cm as cm
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
import gsw
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import eddy_funcs as fu
import scipy.signal as signal

import xarray as xr
import pandas as pd

# %%
import smt_modules.init_slurm_cluster as scluster 
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, scale=2)
cluster
client

#%%
fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/eddy_features/'
savefig  = False
fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.005_20W-20E_50S-10S.nc'
fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'

#%%
#ssh
# ds = eva.load_smt_wave_all('2d', 7, it=500)
# ds7 = ds.drop('clon').drop('clat')
path = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190901T011500Z.nc'
ds = xr.open_dataset(path)
ds7 = ds.zos

path = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0009/outdata_2d/smtwv0009_oce_2d_PT1H_20190901T011500Z.nc'
ds = xr.open_dataset(path)
ds9 = ds.zos


lon_reg = [-5, 10]
lat_reg = [-40, -30]

ds_interp7 = pyic.interp_to_rectgrid_xr(ds7.compute(), fpath_ckdtree, lon_reg, lat_reg)
ds_interp9 = pyic.interp_to_rectgrid_xr(ds9.compute(), fpath_ckdtree, lon_reg, lat_reg)

ds_interp7 = ds_interp7.isel(time=0)
ds_interp9 = ds_interp9.isel(time=0)

ds_interp = ds_interp7
#%%
reload(eva)
filter_scale = 10e3
mask_land_100 = ds_interp.copy(data = np.ones(ds_interp.shape))
filter_30km, mask_land_100_new = eva.create_gcm_filter(ds_interp, 'irregular', mask_land_100, filter_scale)
ds_sel = ds_interp * mask_land_100_new


ds_filt_7 = filter_30km.apply(ds_interp7, dims=['lat', 'lon'])
ds_filt_9 = filter_30km.apply(ds_interp9, dims=['lat', 'lon'])



#%%
def plot_gcm_fields(data7, data_filt7, data9, data_filt9, filter_scale, lon_reg, lat_reg, savefig=False):
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(3, 2,  asp=asp, plot_cb=[0,1,1,0,1,1], fig_size_fac=2, projection=ccrs_proj, sharex=True, sharey=True)

    cmap = 'Greys'
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = [0.6,1.3]
    hm1 = pyic.shade(data7.lon, data7.lat, data7, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.title.set_text('SSH')
    ax.set_ylabel('ICON-SMT-Wave')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(data_filt7.lon, data_filt7.lat, data_filt7, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.title.set_text(f'filtered SSH {filter_scale/1e3} km')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 8e-4
    hm1 = pyic.shade(data7.lon, data7.lat, (data7-data_filt7), ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.title.set_text('SSH - filtered SSH')
    hm1[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm1[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = [-0.25,0.25]
    hm1 = pyic.shade(data9.lon, data9.lat, data9, ax=ax,cax=cax, clim=clim, cmap=cmap, 
                    transform=ccrs_proj, rasterized=False)
    ax.title.set_text('SSH')
    ax.set_ylabel('ICON-SMT')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(data_filt9.lon, data_filt9.lat, data_filt9, ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.title.set_text(f'filtered SSH {filter_scale/1e3} km')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 8e-4
    hm1 = pyic.shade(data9.lon, data9.lat, (data9-data_filt9), ax=ax,cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    ax.title.set_text('SSH - filtered SSH')
    hm1[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm1[1].formatter.set_useMathText(True)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore
        # ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    fname = f'ssh_filter_{filter_scale/1e3}km2'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250,format='png', bbox_inches='tight')

plot_gcm_fields(ds_interp7, ds_filt_7, ds_interp9, ds_filt_9, filter_scale, lon_reg, lat_reg, savefig=savefig)



