# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd
import gsw
import matplotlib.cm as cm
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
import gsw
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import eddy_funcs as fu
import scipy.signal as signal

import xarray as xr
import pandas as pd
import matplotlib.patches as patches
from matplotlib.ticker import FormatStrFormatter

# %%
fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/'
savefig  = False

def plot_dispersion_relation(ax,N, color='k'):
    k_h = np.logspace(-8, 2, 100)  # Horizontal wavenumber (rad/m)
    H   = 5000  # Depth of the domain (m)
    N   = N * 2/np.pi # Buoyancy frequency (rad/s),                    constant for simplicity
    ax.hlines(N, k_h.min(), k_h.max(), color='r', label=f'N:{N:.2e}')
    f   = 7.53305141e-05 * 2/np.pi  # Coriolis frequency (rad/s), constant for simplicity

    modes = np.array([1,10])#np.arange(1, 11)  # Modes 1 to 10
    frequencies = []  # To store frequencies for each mode

    for n in modes:
        m_n = n * np.pi / H  # Vertical wavenumber for mode n
        omega_n = np.sqrt(f**2 + (N**2*k_h**2)/(k_h**2+m_n**2))  # Dispersion relation
        frequencies.append(omega_n)

    for n, omega_n in enumerate(frequencies, start=1):
        ax.plot(k_h, omega_n, color=color,  zorder=100, lw=0.5)
    return ax



def plot_2d_h_wave_spec():
    fig, ax = plt.subplots(1,1,figsize=(8, 6))

    ax.set_xscale('log')
    ax.set_yscale('log')

    # plot box
    #submesocale resolving
    xlim_smd_mod = [1/1e8, 1/540]
    ylim_smd_mod = [1/(356 * 3600), 1/3600]
    # plot grey rectangular box
    rect = patches.Rectangle(
        (xlim_smd_mod[0], ylim_smd_mod[0]),  # Bottom-left corner
        xlim_smd_mod[1] - xlim_smd_mod[0],   # Width
        ylim_smd_mod[1] - ylim_smd_mod[0],   # Height
        linewidth=1,
        edgecolor='grey',
        facecolor='lightgrey',
        alpha=0.5
    )
    ax.add_patch(rect)

    plt.xlabel('Horizontal Wavenumber (rad/m)')
    plt.ylabel('Frequency (rad/s)')
    plt.title('Horizontal Wavenumber–Wave Frequency Spectrum')
    plot_dispersion_relation(ax, 7e-3, color='k')
    plot_dispersion_relation(ax, 1e-3, color='purple')
    plot_dispersion_relation(ax, 1e-4, color='blue')

    ax3 = plt.gca().secondary_xaxis('top', functions=(lambda x: 1/x/1, lambda x: 1/x/1))
    ax3.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax3.set_xlabel('wavelength [m]')

    ax1 = plt.gca().secondary_yaxis('right', functions=(lambda x: 1/x/1, lambda x: 1/x/1))
    ax1.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    # ax1.set_xlabel('period [m]')
    # overwrite with new ticks 1s 1 min 1h 1d 1w 1m 1y 10y 100y
    ax1.set_yticks([1, 60, 3600, 86400, 604800, 2628000, 31536000, 315360000, 3153600000])
    ax1.set_yticklabels(['1s', '1min', '1h', '1d', '1w', '1m', '1y', '10y', '100y'])
    ax1.set_ylabel('period [s]')


    if savefig:
        plt.savefig(f'{fig_path}/2d_hwavenumber_spectra2_deep_core.png')

# %%
plot_2d_h_wave_spec()
# %%
