# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd
import gsw
import matplotlib.cm as cm
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
import gsw
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import eddy_funcs as fu
import scipy.signal as signal

import xarray as xr
import pandas as pd

# %%
import smt_modules.init_slurm_cluster as scluster 
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='03:00:00', wait=False, scale=2)
cluster
client
#%%
fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/eddy_features/'
savefig  = False
id   = 'ID5'
type = 'anticyclonic'

# %%
reload(fu)
to  = fu.load_data('to', id, type, 100)
so  = fu.load_data('so', id, type,100)
u   = fu.load_data('u', id, type, 100)
v   = fu.load_data('v', id, type, 100)
w   = fu.load_data('w', id, type, 100)
w   = w.rename({'depth':'depthi'})
tke = fu.load_data('tke', id, type, 100)
tke = tke.rename({'depth':'depthi'})
# zos = fu.load_data('zos', id, type,100)
# t0  = u.time[0].values
# t1  = u.time[-1].values
# select same time like u
# zos = zos.sel(time=slice(t0, t1))

#%%
f = eva.calc_coriolis_parameter(so.y +so.latcen)

# %%######################################### Select Circle #########################################
fig_path = f'/home/m/m300878/submesoscaletelescope/results/smt_wave/eddy_features//anticyclonic/{id}/detect_waves'
savefig  = True

#%%
reload(fu)
tt     = 5
depthi = w.depthi.values
N2, rho = fu.calc_n2_from_so_to(so.isel(time=tt).so, to.isel(time=tt).to, depthi)
N = np.sqrt(N2)

# %%
if False:
    N.sel(x=0).plot(cmap='Spectral_r')
    plt.ylim(1500,0)
    to.isel(time=5).to.sel(x=0).plot(cmap='Spectral_r')
    plt.ylim(1500,0)
    so.isel(time=5).so.sel(x=0).plot(cmap='Spectral_r', vmin=34.5, vmax=35.5)
    plt.ylim(1500,0)
    (u.isel(time=5).u**2 + v.isel(time=0).v**2).sel(x=0).plot(cmap='RdBu_r')
    plt.ylim(1500,0)


#%%
cx       = 0.3
f_mean   = np.abs(f.isel(time=5).mean(dim='y'))
N_mean   = N.mean(dim='x').mean(dim='y')
N_center = N.sel(x=slice(-cx,cx)).sel(y=slice(-cx,cx)).mean(dim='x').mean(dim='y')

# %%
def plot_N(N, N_mean, N_center, f_mean, cx):
    fig, ax = plt.subplots(1,2, figsize=(10,5))
    N.sel(y=0).plot(cmap='Spectral_r', ax=ax[0])
    ax[0].vlines(-cx, 0, 1500, color='tab:orange', label='f')
    ax[0].vlines(cx, 0, 1500, color='tab:orange')
    ax[0].set_ylim(1500,0)
    ax[0].set_title('N')

    ax[1].plot(N_mean, N_mean.depthi, label='N_all')
    ax[1].plot(N_center, N_center.depthi, label='N_core')
    ax[1].vlines(f_mean/2/np.pi, 0, 1500, color='r', label='f')
    ax[1].set_ylim(1500,0)
    ax[1].grid()
    ax[1].legend()
    ax[1].set_xlabel(r'N [$s^{-1}$]')
    ax[1].set_xscale('log')
    ax[1].set_xlim(1e-5, 1e-2)
    if savefig:
        plt.savefig(f'{fig_path}/N_prof_{id}.png')

plot_N(N, N_mean, N_center, f_mean, cx)

# %%#################################
u   = fu.load_data('u', id, type)
v   = fu.load_data('v', id, type)


#%% #################### Frequency Spectra ####################
d = np.array([1, 5, 17, 30, 60, 80, 105, 120])
# data = w.w.sel(x=0, y=0).isel(depthi=d).compute()
# data = w.w.sel(x=0, y=0).isel(time=500).compute()
# data = w.w.sel(x=slice(-cx,cx)).sel(y=slice(-cx,cx))[:,:,::4,::4].isel(depthi=d).compute()
data = (u.u**2 + v.v**2).sel(x=slice(-cx,cx)).sel(y=slice(-cx,cx))[:,:,::4,::4].isel(depth=d).compute()
data = data - data.mean(dim='time')
f, Pxx = signal.welch(data.data, fs=1/(2*3600) , nperseg=128,axis=0)
Pxx = Pxx.mean(axis=-1).mean(axis=-1)
#%%
def plot_freq_spec(f, Pxx, d):
    plt.figure(figsize=(8,10))
    plt.title(f'Spectra at center')
    colors = cm.Spectral_r(np.linspace(0, 1, len(d)))
    for di in range(len(d)):
        plt.loglog(f*(60**2*24), Pxx[:,di], label=f'depth={data.depth[di].values}', color=colors[di])
    plt.xlabel('Frequency [cpd]')
    ylim = plt.gca().get_ylim()
    plt.vlines(f_mean*(60**2*24)/2/np.pi, ylim[0], ylim[1], color='r', label='f')
    plt.vlines(N_center.isel(depthi=di)*(60**2*24), ylim[0], ylim[1], color='g', label='N')
    plt.legend()
    plt.grid()
    # plt.ylim(8e-4, 2e-2)
    if savefig:
        plt.savefig(f'{fig_path}/freq_spec.png')

plot_freq_spec(f, Pxx, d)




# %% #################### determine horizontal wavenumber
# data = w.w.isel(time=4).isel(depthi=d).compute()
data = (u.u**2 + v.v**2).isel(time=150).isel(depth=d).compute()
dx   = eva.convert_degree_to_meter(data.latcen)*0.01 / 1e3
dx   = dx.data
f, Pxx = signal.welch(data.data, fs=1/dx , nperseg=128, axis=-2)
Pxx = Pxx.mean(axis=-1)
# %%
def plot_hwavenumber_spec(f, Pxx, d):
    plt.figure(figsize=(8,10))
    plt.title(f'hor wavenumber')
    colors = cm.Spectral_r(np.linspace(0, 1, len(d)))

    for di in range(len(d)):
        plt.loglog(f, Pxx[di,:], label=f'depth={data.depth[di].values}', color=colors[di])
    plt.xlabel('Wavenumber [1/km]')
    plt.grid()
    plt.legend()
    if savefig:
        plt.savefig(f'{fig_path}/hwavenumber_spec.png')

plot_hwavenumber_spec(f, Pxx, d)


#%%
def sel_core(ds, cx):
    return ds.sel(x=slice(-cx,cx)).sel(y=slice(-cx,cx))

# data = w.w.isel(depthi=40).compute()
#%%
ds   = (u.u**2 + v.v**2).isel(depth=slice(60,68)).compute()
data = ds
data = data.transpose('time','depth', 'x', 'y')
data.isel(time=0, depth=0).plot()

#%%
data = sel_core(data, 1)
#%%

def calc_3d_spectrum(data):
    L_x = data.x.size * 0.01 * eva.convert_degree_to_meter(data.latcen.mean()) /1e3 # Domain length in km
    L_x = L_x.data
    L_y = data.y.size * 0.01 * eva.convert_degree_to_meter(data.latcen.mean()) /1e3 # Domain length in km
    L_y = L_y.data
    T   = data.time.size * 2   # Time period in hours
    N_x = data.x.size   # Number of horizontal points
    N_y = data.y.size
    N_t = data.time.size   # Number of time steps

    W = np.fft.fftn(data.data)
    W = W.mean(axis=1)

    W_shifted = np.fft.fftshift(W)  # Shift zero frequency to the center
    PSD = np.abs(W_shifted)**2
    PSD = PSD / (N_x * N_t)**2  # Normalize the PSD

    nt = N_t//2+1
    nx = N_x//2+1
    ny = N_y//2+1
    PSD = PSD[nt:, nx:, ny:]

    # Frequency and wavenumber axes
    k_h = np.fft.fftshift(np.fft.fftfreq(N_x, d=L_x / N_x)) * 2 * np.pi  # rad/m
    k_h_y = np.fft.fftshift(np.fft.fftfreq(N_y, d=L_y / N_y)) * 2 * np.pi  # rad/m
    omega = np.fft.fftshift(np.fft.fftfreq(N_t, d=T / N_t)) * 2 * np.pi  # rad/s

    k_h = k_h[N_x//2+1:]
    k_h_y = k_h_y[N_y//2+1:]
    omega = omega[N_t//2+1:]

    return PSD, k_h, k_h_y, omega

PSD, k_h, k_h_y, omega = calc_3d_spectrum(data)
#%%

def plot_2d_h_wave_spec(k_h, omega, PSD, f_mean):
    fig, ax = plt.subplots(1,1,figsize=(8, 6))
    # cb = ax.pcolormesh(k_h, omega/np.pi/2, PSD[:,249,:], shading='auto', cmap='Spectral_r', norm=matplotlib.colors.LogNorm( vmin=1e-7, vmax=1e-1))
    cb = ax.pcolormesh(k_h, omega/np.pi/2, PSD, shading='auto', cmap='Spectral_r', norm=matplotlib.colors.LogNorm( vmin=1e-7, vmax=1e-2))
    # cb = ax.pcolormesh(k_h, omega/np.pi/2, PSD.mean(axis=1), shading='auto', cmap='Spectral_r', norm=matplotlib.colors.LogNorm( vmin=1e-7, vmax=1e-1))

    ax.set_xscale('log')
    ax.set_yscale('log')

    # ax.set_xlim(k_h_y.min()-100, k_h_y.max())
    # ax.set_ylim(1e-3, omega.max()/np.pi/2)

    ax.hlines(f_mean * 3600/np.pi/2,  k_h.min(), k_h.max(), color='r', label='f', ls='--')
    ax.hlines(1.4e-4 * 3600/np.pi/2,  k_h.min(), k_h.max(), color='w', label='M2', lw=0.5,  ls='--')

    cbar = fig.colorbar(cb, ax=ax, orientation='vertical')
    plt.xlabel('Horizontal Wavenumber (rad/km)')
    plt.ylabel('Frequency (rad/h)')
    plt.title('Horizontal Wavenumber–Wave Frequency Spectrum')
    plot_dispersion_relation(ax, 7e-3, color='k')
    plot_dispersion_relation(ax, 1e-3, color='purple')
    plt.legend()
    if savefig:
        plt.savefig(f'{fig_path}/2d_hwavenumber_spectra2_deep_core.png')


def plot_dispersion_relation(ax,N, color='k'):
    k_h = np.logspace(-4, 2, 100)  # Horizontal wavenumber (rad/m)
    H   = 5#000  # Depth of the domain (m)
    N   = N * 3600/2/np.pi # Buoyancy frequency (rad/s),                    constant for simplicity
    ax.hlines(N, k_h.min(), k_h.max(), color='r', label=f'N:{N:.2e}')
    f   = 7.53305141e-05 * 3600/2/np.pi  # Coriolis frequency (rad/s), constant for simplicity

    modes = np.array([1,10])#np.arange(1, 11)  # Modes 1 to 10
    frequencies = []  # To store frequencies for each mode

    for n in modes:
        m_n = n * np.pi / H  # Vertical wavenumber for mode n
        omega_n = np.sqrt(f**2 + (N**2*k_h**2)/(k_h**2+m_n**2))  # Dispersion relation
        frequencies.append(omega_n)


    # fig, ax = plt.subplots(1,1,figsize=(8, 6))
    for n, omega_n in enumerate(frequencies, start=1):
        ax.plot(k_h, omega_n, color=color,  zorder=100, lw=0.5)
    return ax


plot_2d_h_wave_spec(k_h, omega, PSD.mean(axis=-1), f_mean)



# %%
def plot_1d_spectra(omega, PSD, f_mean):
    fig, ax = plt.subplots(1,1,figsize=(8, 6))
    ax.loglog(omega*24/2/np.pi,PSD.mean(axis=-1).mean(axis=-1))
    ax.vlines(f_mean * 3600*24/2/np.pi, 1e-7, 1e-2, color='k', label='f')
    ax.vlines(1.4e-4 * 3600*24/2/np.pi, 1e-7, 1e-2, color='r', label='M2')
    ax.set_ylim(1e-7, 1e-2)
    ax.legend()
    ax.grid()
    if savefig:
        plt.savefig(f'{fig_path}/1d_spectra.png')

plot_1d_spectra(omega, PSD, f_mean)

# %% ##################### vertical wavenumber
w  = fu.load_data('w', id, type)
w = w.rename({'depth':'depthi'})
#%%
# data = w.w.sel(x=slice(-cx,cx)).sel(y=slice(-cx,cx))[:,:].isel(time=200).compute()
data = w.w[:,:,::4,::4].isel(time=200).compute()
# data = (u.u**2 + v.v**2).sel(x=slice(-cx,cx)).sel(y=slice(-cx,cx))[:,:,::4,::4].isel(time=600).compute()
# data = (u.u**2 + v.v**2)[:,:,::4,::4].isel(time=5).compute()

def interp_to_regular_spacing(data, depthi):
    end = 127
    data = data.isel(depthi=slice(0, end))
    data_int = data.interp(depthi=np.linspace(0,depthi[end],end), method='linear', kwargs={'fill_value': 'extrapolate'})
    return data_int

data_int = interp_to_regular_spacing(data, depthi)

if False:
    data.isel(x=5,y=10).plot()
    data_int.isel(x=5,y=10).plot()
# %%
dx = (data_int.depthi[1] - data_int.depthi[0]).values
print(dx)
f, Pxx = signal.welch(data_int.data, fs=1/dx,  axis=0)

Pxx = Pxx.mean(axis=-1).mean(axis=-1)
# Pxx = Pxx[:,65,85]#.mean(axis=-1).mean(axis=-1)
# Pxx = Pxx[:,:,1].mean(axis=-1)#.mean(axis=-1)

# %%
def plot_2d_spectra(f, Pxx, d):
    plt.figure(figsize=(8,10))
    plt.title(f'vertical wavenumber')
    colors = cm.Spectral_r(np.linspace(0, 1, len(d)))
    plt.loglog(f, Pxx)
    plt.xlabel('Wavenumber [1/m]')
    plt.grid()
    plt.loglog(f, 1e-13*f**(-3), label='-3 slope', color='r') 
    plt.loglog(f, 1e-10*f**(-2), label='-2 slope', color='g')
    plt.legend()
    if savefig:
        plt.savefig(f'{fig_path}/vert_wavenumber_spec.png')

plot_2d_spectra(f, Pxx, d)


#%%
data = (u.u**2 + v.v**2).sel(y=0).compute()
# %%

def calc_3d_spectrum(data):
    L_x = data.x.size * 0.01 * eva.convert_degree_to_meter(data.latcen.mean()) /1e3 # Domain length in km
    L_x = L_x.data
    L_y = data.depth.size * 10
    L_y = L_y#.data
    T   = data.time.size * 2   # Time period in hours
    N_x = data.x.size   # Number of horizontal points
    N_y = data.depth.size
    N_t = data.time.size   # Number of time steps

    W = np.fft.fftn(data.data)
    # W = W.mean(axis=1)

    W_shifted = np.fft.fftshift(W)  # Shift zero frequency to the center
    PSD = np.abs(W_shifted)**2
    PSD = PSD / (N_x * N_t)**2  # Normalize the PSD

    nt = N_t//2+1
    nx = N_x//2+1
    ny = N_y//2+1
    PSD = PSD[nt:, nx:, ny:]

    # Frequency and wavenumber axes
    k_h = np.fft.fftshift(np.fft.fftfreq(N_x, d=L_x / N_x)) * 2 * np.pi  # rad/m
    k_h_y = np.fft.fftshift(np.fft.fftfreq(N_y, d=L_y / N_y)) * 2 * np.pi  # rad/m
    omega = np.fft.fftshift(np.fft.fftfreq(N_t, d=T / N_t)) * 2 * np.pi  # rad/s

    k_h = k_h[N_x//2+1:]
    k_h_y = k_h_y[N_y//2+1:]
    omega = omega[N_t//2+1:]

    return PSD, k_h, k_h_y, omega

data = data.transpose('time', 'x', 'depth')
PSD, k_h, k_h_y, omega = calc_3d_spectrum(data)
# %%
def plot_2d_spectra(k_h, omega, PSD, f_mean):
    fig, ax = plt.subplots(1,1,figsize=(8, 6))
    # cb = ax.pcolormesh(k_h, omega/np.pi/2, PSD[:,249,:], shading='auto', cmap='Spectral_r', norm=matplotlib.colors.LogNorm( vmin=1e-7, vmax=1e-1))
    cb = ax.pcolormesh(k_h, omega/np.pi/2, PSD, shading='auto', cmap='Spectral_r', norm=matplotlib.colors.LogNorm( vmin=1e-7, vmax=1e-2))
    # cb = ax.pcolormesh(k_h, omega/np.pi/2, PSD.mean(axis=1), shading='auto', cmap='Spectral_r', norm=matplotlib.colors.LogNorm( vmin=1e-7, vmax=1e-1))

    ax.set_xscale('log')
    ax.set_yscale('log')

    ax.set_xlim(k_h.min(), k_h.max())
    # ax.set_ylim(1e-3, omega.max()/np.pi/2)

    ax.hlines(f_mean * 3600/np.pi/2,  k_h.min(), k_h.max(), color='r', label='f', ls='--')
    ax.hlines(1.4e-4 * 3600/np.pi/2,  k_h.min(), k_h.max(), color='w', label='M2', lw=0.5,  ls='--')

    cbar = fig.colorbar(cb, ax=ax, orientation='vertical')
    plt.xlabel('Vertical Wavenumber (rad/km)')
    plt.ylabel('Frequency (rad/h)')
    plt.title('Vetical Wavenumber–Wave Frequency Spectrum')
    if savefig:
        plt.savefig(f'{fig_path}/2d_vert_wave.png')

plot_2d_spectra(k_h_y, omega, PSD.mean(axis=1), f_mean)
# %%

def plot_1d_spectra_kz(k_h_y, PSD, f_mean):
    fig, ax = plt.subplots(1,1,figsize=(8, 6))
    # ax.loglog(omega*24/2/np.pi,PSD.mean(axis=-1).mean(axis=-1))
    ax.loglog(k_h_y,PSD.mean(axis=0).mean(axis=0))
    ax.vlines(f_mean * 3600*24/2/np.pi, 1e-7, 1e-2, color='k', label='f')
    ax.vlines(1.4e-4 * 3600*24/2/np.pi, 1e-7, 1e-2, color='r', label='M2')
    ax.set_ylim(1e-10, 1e-2)
    ax.legend()
    ax.grid()
    if savefig:
        plt.savefig(f'{fig_path}/1d_spectra_kz.png')

plot_1d_spectra_kz(k_h_y, PSD, f_mean)
# %%
