# %%
import sys
import glob, os
import pyicon as pyic
import sys
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
# import funcs1 as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# %% Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
# from validate_catalog import all_params
# params_dict, cat = all_params()
# params_dict.keys()
import gcm_filters

fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/grad_temp/eddy_sec/'
savefig = False
# %%
T1 = xr.open_dataset(f'/work/mh0033/m300878/smt/smt_wave/section_video/dto_section_eddy_part1.nc')
T2 = xr.open_dataset(f'/work/mh0033/m300878/smt/smt_wave/section_video/dto_section_eddy_part2.nc')
T3 = xr.open_dataset(f'/work/mh0033/m300878/smt/smt_wave/section_video/dto_section_eddy_part3.nc')
T = xr.concat([T1, T2, T3], dim='time')
#%% and salinity
S1 = xr.open_dataset(f'/work/mh0033/m300878/smt/smt_wave/section_video/dso_section_eddy_backup.nc')
#%%select common time range
common_start_time = max(T1.time.min(), S1.time.min())
common_end_time = min(T1.time.max(), S1.time.max())
T1 = T1.sel(time=slice(common_start_time, common_end_time))
S1 = S1.sel(time=slice(common_start_time, common_end_time))

# compute N2 from potential temperature T1 and absolute salinity S1
CT = gsw.CT_from_pt(S1.so, T1.to)
depth_interface = CT.depth[1:].data-CT.depth[:-1].data


# p = gsw.p_from_z(-CT.depth.values, CT.lat.values)
N2, p_mid = gsw.Nsquared(S1.so.values, CT.values, -CT.depth.data[np.newaxis,:,np.newaxis], lat=S1.lat.data[np.newaxis,np.newaxis,:], axis=1)
#make xarray
N2 = xr.DataArray(N2, dims=['time', 'depth', 'ncells'], coords={'time':T1.time, 'depth':-p_mid[0,:,0], 'ncells':CT.ncells})
#add dist
N2['dist'] = T1.dist
# %%
T_diff = T.diff('depth')
T_diffX = T_diff.diff('dist')
# S_an = S_diff-S_diff.mean('time')


# %%

def plot_section_c(ds, t, vmin, vmax, cmap, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(1,2, plot_cb=True, asp=0.25, fig_size_fac=1.3, sharey=True, sharex=True, daxt=1, axlab_kw=None)
    ii=-1

    clim = vmin,vmax
    # cmap = 'Spectral_r'

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(rf'$dTdz$ at {ds.time.values:.13} ')
    hm2=pyic.shade(ds.dist.data/1000, ds.depth.data, ds.data, ax=ax, cax=cax, cmap=cmap, clim=clim)
    cax.set_title(r'$K/m$', pad=10)
    ax.set_ylim(1500,0)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # ax.set_title(rf'$dTdz$ at {ds.time.values:.13} ')
    hm2=pyic.shade(ds.dist.data/1000, ds.depth.data, ds.data, ax=ax, cax=cax, cmap=cmap, clim=clim)
    cax.set_title(r'$K/m$', pad=10)
    ax.set_ylim(5600,0)
    ax.set_xlabel('distance [km]')


    for ax in hca:
        pyic.plot_settings(ax=ax)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) #type: ignore
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) #type: ignore
        # ax.legend(loc='lower left')
        ax.set_ylabel('depth [m]')

    if savefig:
        plt.savefig(f'{fig_path}dto_section_n{t}_eddy_ano.png', dpi=300, bbox_inches='tight')
        

# %%
vmin = -2e-5
vmax = 2e-5
cmap = 'RdBu'
plot_section_c(N2.isel(time=91), 0, vmin, vmax, cmap, fig_path, savefig)

# %%
#fill nans with 0
T_diff = T_diff.fillna(0)
S_sel = T_diff.rolling(time=12).mean()
# %%
vmin = -4e-1
vmax = 0
cmap = 'Spectral_r'
plot_section_c(S_sel.to.isel(time=110), 0, vmin, vmax, cmap, fig_path, savefig)
# %%
vmin = -4e-1
vmax = 0
plt.figure(figsize=(15,5))
(T_diff-T_diff.mean('time')).to.isel(ncells=605).plot(y='depth', cmap='RdBu_r', vmin=-1e-1, vmax=1e-1)
plt.ylim(5200,0)
if savefig==True: plt.savefig(f'{fig_path}dto_section_n605_eddy_ano.png', dpi=300, bbox_inches='tight')
# %%
vmin = -4e-1
vmax = 0
plt.figure(figsize=(15,5))
(T_diff-T_diff.mean('time')).to.isel(ncells=605).plot(y='depth', cmap='RdBu_r', vmin=-3e-1, vmax=3e-1)
plt.ylim(1500,0)
plt.savefig(f'{fig_path}dto_section_n605_eddy_ano_s.png', dpi=300, bbox_inches='tight')
# %%
var = T_diffX#.isel(time=slice(200, 450))
#rolling time mean
var_rol = var.rolling(time=12).mean()

#%%
vmin = -1e-1
vmax = 1e-1
plt.figure(figsize=(15,5))
(var-var_rol).to.isel(ncells=610).plot(y='depth', cmap='RdBu_r', vmin=vmin, vmax=vmax)
plt.ylim(5200,0)
if savefig==True: plt.savefig(f'{fig_path}dto_section_n605_eddy_ano.png', dpi=300, bbox_inches='tight')
# %%
vmin = -2e-1
vmax = 2e-1
plt.figure(figsize=(15,5))
(var-var_rol).to.isel(time=106).plot(y='depth', cmap='RdBu_r', vmin=vmin, vmax=vmax)
plt.ylim(2000,0)
if savefig==True: plt.savefig(f'{fig_path}dto_section_n605_eddy_ano.png', dpi=300, bbox_inches='tight')
# %%
