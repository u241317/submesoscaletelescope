# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd
import gsw
import matplotlib.cm as cm
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
import gsw
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import eddy_funcs as fu
import scipy.signal as signal

import xarray as xr
import pandas as pd

# %%
import smt_modules.init_slurm_cluster as scluster 
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, scale=2)
cluster
client

#%%
fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/eddy_features/'
savefig  = False
# fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.005_20W-20E_50S-10S.nc'
fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'

#%%
#ssh
if True:
    ds = eva.load_smt_wave_all('2d', 7, it=1)
    ds = ds.drop('clon').drop('clat')
    ds = ds.zos
    #%%
    ds_sel = ds.isel(time=slice(1000, 1500))
    path = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/001deg/'

    lon_reg = [-10, 10]
    lat_reg = [-40, -20]

    for i in np.arange(ds_sel.time.size):
        ds_interp = pyic.interp_to_rectgrid_xr(ds_sel.isel(time=i), fpath_ckdtree,  lon_reg, lat_reg)
        fname = f'{path}ssh_{ds_sel.isel(time=i).time.values}.nc'
        ds_interp.to_netcdf(fname)
#%%open
path = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/001deg/'
ds = xr.open_mfdataset(f'{path}*.nc', combine='nested', concat_dim='time')


#%%
# path ='/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/smtwv0007_ssh_02.nc'
# da = xr.open_dataarray(path)

# #%%
# # ds_sel = ds.u**2 + ds.v**2
# ds_sel = (ds.u-ds.u.mean('time'))**2 + (ds.v-ds.v.mean('time'))**2
# ds_sel = ds_sel.isel(depth=0)
# #%%


#%%
f = eva.calc_coriolis_parameter(ds_interp.lat)
cx       = 0.3
f_mean   = np.abs(f.mean(dim='lat'))
N_mean   = 1.4e-4
N_center = 1.4e-4

# %%######################################### Select Circle #########################################
#detrend
import scipy.signal as signal
ds_interp_detrended = signal.detrend(ds_interp, axis=0)
#%%
#make xarray

data = ds_interp.copy(data =ds_interp_detrended)
#%%
#lowpass 48h filter
# data = data.rolling(time=48, center=True).mean()



#%%

def calc_3d_spectrum(data):
    L_x = data.lon.size * 0.01 * eva.convert_degree_to_meter(data.lat.mean()) /1e3 # Domain length in km
    L_x = L_x.data
    L_y = data.lat.size * 0.01 * eva.convert_degree_to_meter(data.lat.mean()) /1e3 # Domain length in km
    L_y = L_y.data
    T   = data.time.size   # Time period in hours
    N_x = data.lon.size   # Number of horizontal points
    N_y = data.lat.size
    N_t = data.time.size   # Number of time steps

    W = np.fft.fftn(data.data)
    W = W#.mean(axis=1) #average over depths

    W_shifted = np.fft.fftshift(W)  # Shift zero frequency to the center
    PSD = np.abs(W_shifted)**2
    PSD = PSD / (N_x * N_t)**2  # Normalize the PSD

    nt = N_t//2+1
    nx = N_x//2+1
    ny = N_y//2+1
    PSD = PSD[nt:, nx:, ny:]

    # Frequency and wavenumber axes
    k_h   = np.fft.fftshift(np.fft.fftfreq(N_x, d=L_x / N_x)) * 2 * np.pi  # rad/m
    k_h_y = np.fft.fftshift(np.fft.fftfreq(N_y, d=L_y / N_y)) * 2 * np.pi  # rad/m
    omega = np.fft.fftshift(np.fft.fftfreq(N_t, d=T / N_t)) * 2 * np.pi  # rad/s

    k_h = k_h[N_x//2+1:]
    k_h_y = k_h_y[N_y//2+1:]
    omega = omega[N_t//2+1:]

    return PSD, k_h, k_h_y, omega

PSD, k_h, k_h_y, omega = calc_3d_spectrum(data)
#%%

def plot_2d_h_wave_spec(k_h, omega, PSD, f_mean, savefig=False):
    fig, ax = plt.subplots(1,1,figsize=(8, 6))
    # cb = ax.pcolormesh(k_h, omega/np.pi/2, PSD[:,249,:], shading='auto', cmap='Spectral_r', norm=matplotlib.colors.LogNorm( vmin=1e-7, vmax=1e-1))
    cb = ax.pcolormesh(k_h, omega/np.pi/2, PSD, shading='auto', cmap='Spectral_r', norm=matplotlib.colors.LogNorm( vmin=1e-8, vmax=1e0)) #vmin=1e-10, vmax=1e-4
    # cb = ax.pcolormesh(k_h, omega/np.pi/2, PSD.mean(axis=1), shading='auto', cmap='Spectral_r', norm=matplotlib.colors.LogNorm( vmin=1e-7, vmax=1e-1))

    ax.set_xscale('log')
    ax.set_yscale('log')

    # ax.set_xlim(k_h_y.min()-100, k_h_y.max())
    # ax.set_ylim(1e-3, omega.max()/np.pi/2)

    ax.hlines(f_mean * 3600/np.pi/2,  k_h.min(), k_h.max(), color='k', label='f', lw=0.5, ls='--')
    ax.hlines(1.4e-4 * 3600/np.pi/2,  k_h.min(), k_h.max(), color='k', label='M2', lw=0.5,  ls='--')

    # cbar = fig.colorbar(cb, ax=ax, orientation='vertical')
    plt.xlabel('horizontal Wavenumber (rad/km)')
    plt.ylabel('frequency (rad/h)')
    # plt.title('horizontal Wavenumber–Wave Frequency Spectrum')
    plot_dispersion_relation(ax, 1.6e-3, color='k')
    # plot_dispersion_relation(ax, 2e-3, color='purple')
    plt.legend(loc='lower left')
    plt.xlim(k_h.min(), k_h.max())
    plt.ylim(omega.min(), 0.5)
    if savefig:
        plt.savefig(f'{fig_path}/2d_spec_ssh2.png', dpi=300, bbox_inches='tight')


def plot_dispersion_relation(ax,N, color='k'):
    k_h = np.logspace(-4, 2, 100)  # Horizontal wavenumber (rad/m)
    H   = 5#000  # Depth of the domain (m)
    N   = N * 3600/2/np.pi # Buoyancy frequency (rad/s),                    constant for simplicity
    # ax.hlines(N, k_h.min(), k_h.max(), color='r', label=f'N:{N:.2e}')
    f   = 7.53305141e-05 * 3600/2/np.pi  # Coriolis frequency (rad/s), constant for simplicity

    modes = np.array([1,2,3])#np.arange(1, 11)  # Modes 1 to 10
    frequencies = []  # To store frequencies for each mode

    for n in modes:
        m_n = n * np.pi / H  # Vertical wavenumber for mode n
        omega_n = np.sqrt(f**2 + (N**2*k_h**2)/(k_h**2+m_n**2))  # Dispersion relation
        frequencies.append(omega_n)


    # fig, ax = plt.subplots(1,1,figsize=(8, 6))
    for n, omega_n in enumerate(frequencies, start=1):
        ax.plot(k_h, omega_n, color=color,  zorder=100, lw=0.5, label=f'Mode {n}')
    return ax
#%%
# k = np.sqrt(k_h**2 + k_h_y**2)

plot_2d_h_wave_spec(k, omega, PSD.mean(axis=-1), f_mean)



# %%
ssh = ds.zos.transpose('lon', 'lat', 'time')

Nx = ssh.lon.size
Ny = ssh.lat.size
Nt = ssh.time.size

L_x = ssh.lon.size * 0.01 * eva.convert_degree_to_meter(ssh.lat.mean()) /1e3 # Domain length in km
L_y = ssh.lat.size * 0.01 * eva.convert_degree_to_meter(ssh.lat.mean()) /1e3 # Domain length in km
L_x = L_x.data
L_y = L_y.data
T  = ssh.time.size   # Time period in hours

ssh = ssh.compute()
#%%
# 1. Remove the temporal mean at each grid point
ssh_prime = ssh - ssh.mean(dim='time') #np.mean(ssh, axis=2)
#%%
# 2. Perform 3D FFT
fft3d = np.fft.fftn(ssh_prime, axes=(0, 1, 2))  # FFT over (x, y, t)

# 3. Compute the power spectrum

# power_spectrum = fft3d * np.conj(fft3d)  # Element-wise multiplication
# # Take the real part (optional, as this is already real due to conjugation)
# power_spectrum = np.real(power_spectrum)
#%%
#shift
power_spectrum = np.fft.fftshift(fft3d, axes=(0, 1, 2))
power_spectrum = np.abs(power_spectrum) ** 2

# Normalize the power spectrum
power_spectrum /= Nx * Ny * Nt  # Normalize by the number of points
#%%
nt = Nt//2+1
nx = Nx//2+1
ny = Ny//2+1

power_spectrum_pos = power_spectrum[nx:, ny:, nt:]

#%%
kx    = np.fft.fftshift(np.fft.fftfreq(Nx, d=L_x / Nx)) * 2 * np.pi  # rad/m
ky    = np.fft.fftshift(np.fft.fftfreq(Ny, d=L_y / Ny)) * 2 * np.pi  # rad/m
omega = np.fft.fftshift(np.fft.fftfreq(Nt, d=T / Nt)) * 2 * np.pi  # rad/s

kx    = kx[Nx//2+1:]
ky    = ky[Ny//2+1:]
omega = omega[Nt//2+1:]

#%%
kx_grid, ky_grid, omega_grid = np.meshgrid(kx, ky, f, indexing="ij")
k = np.sqrt(kx_grid**2 + ky_grid**2)  # Radial wavenumber

# 5. Bin the spectrum into (k, f) space
k_bins = np.linspace(0, np.max(k), 50)  # Define wavenumber bins
f_bins = np.fft.fftshift(f)  # Centered frequency bins
fk_spectrum = np.zeros((len(k_bins) - 1, len(f_bins)))

for i in range(len(k_bins) - 1):
    # Find indices where k falls in the current bin
    k_mask = (k >= k_bins[i]) & (k < k_bins[i + 1])
    for j, f_val in enumerate(f_bins):
        # Average power spectrum for each (k, f) pair
        fk_spectrum[i, j] = np.mean(power_spectrum[k_mask & (omega_grid == f_val)])


# 6. Visualize the f-k spectrum
plt.figure(figsize=(10, 6))
plt.contourf(f_bins, k_bins[:-1], fk_spectrum, levels=50, cmap='viridis')
plt.colorbar(label='Power')
plt.xlabel('Frequency (f)')
plt.ylabel('Wavenumber (k)')
plt.title('Frequency-Wavenumber Spectrum')
plt.show()


# %%
#%%
PSD = power_spectrum_pos.mean(axis=1).transpose()
#sel postive k and f
plot_2d_h_wave_spec(kx, omega, PSD, f_mean=1.4e-4)
# %% bin x y wavenumbers


#%%
from scipy.stats import binned_statistic
kx_grid, ky_grid = np.meshgrid(kx, ky, indexing="ij")  # 2D wavenumber grid
k = np.sqrt(kx_grid**2 + ky_grid**2)  # Radial wavenumber
k_flat = k.flatten()
psd_flat = power_spectrum_pos.reshape(-1, Nt//2-1)  # Flatten spatial dimensions, keep frequency

# 2. Define k-bins
k_bins = kx#np.linspace(0, np.max(k), 900)  # 50 radial wavenumber bins
k_bin_centers = 0.5 * (k_bins[:-1] + k_bins[1:])  # Bin centers for plotting

# 3. Bin the PSD for each omega
psd_binned = np.zeros((len(k_bin_centers), Nt//2-1))  # Initialize binned PSD

for i in range(Nt//2-1):  # Loop over frequencies
    # Bin the flattened PSD for the current omega
    psd_binned[:, i], _, _ = binned_statistic(k_flat, psd_flat[:, i], statistic='mean', bins=k_bins)


# %%
f = eva.calc_coriolis_parameter(ssh.lat)
f_mean   = np.abs(f.mean(dim='lat'))

plot_2d_h_wave_spec(k_bins[:-1], omega, psd_binned.transpose(), f_mean, savefig)
# %%
plt.figure(figsize=(8, 10))
plt.pcolormesh(kx[:-1], omega, psd_binned.transpose(), norm=matplotlib.colors.LogNorm( vmin=1e-8, vmax=1e-3), cmap='Greys' )

plt.xlabel(r'$k_x$ [rad/m]')
plt.ylabel(r'$\omega$ [rad/h]')
plt.savefig(f'{fig_path}/2d_spec_sshbw.png', dpi=300, bbox_inches='tight')
# %%
