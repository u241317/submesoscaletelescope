# %%
import sys
import glob, os
import pyicon as pyic
import sys
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
# import funcs1 as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# %% Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
# from validate_catalog import all_params
# params_dict, cat = all_params()
# params_dict.keys()
import gcm_filters

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, dash_address='8989')
cluster
client

#%%
path = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/anticyclonic/ID0/w/'
name = 'anticyclonic_2.5x2.5deg_trackID_0time_*'
files = glob.glob(path+name+'.nc')
files.sort()
files = files[:500]
ds = xr.open_mfdataset(files, concat_dim='time', combine='nested', parallel=False)
# %%

ds.isel(time =40, depth= 90).w.plot(cmap='Greys', vmin=-1e-3, vmax=1e-3)
# %%
sel = ds.isel( depth= 90, y=250).w.compute()
sel = sel.sortby('time')

#%%
plt.figure(figsize=(20,5))
sel.plot(x='time', y='x', cmap='Greys', vmin=-1e-3, vmax=1e-3)
# %%
def compute_spectra_uchida(data, samp_freq=2000):
    data['x'] = np.arange(int(data.x.size))*samp_freq
    P = xrft.power_spectrum(data, dim=['x'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
    P = P.isel(time=slice(len(P.time)//2,None)) * 2
    return P

samp_freq = 0.01*eva.convert_degree_to_meter(-32)/-1000

P_lon = compute_spectra_uchida(sel.fillna(0.), samp_freq=samp_freq)
Plo_mean = P_lon.mean(dim='time')
# %%
plt.figure(figsize=(10, 6))
plt.loglog(Plo_mean.freq_x, Plo_mean, label='ICON-SMT-Wave', color='k')
# plt.loglog(P_exp9.freq_lon, P_exp9, label='ICON-SMT', color='tab:blue')
# plt.loglog(Pla_mean.freq_lon, Pla_mean, label='lon')
plt.xlabel('wavenumber [1/km]')
plt.ylabel('PSD [km^2/s^2]')
plt.xlim(1/1211, 1/samp_freq/2)
plt.legend()
plt.grid()
#add vertical line for wave length
plt.axvline(1/170, color='r', linestyle='-', label='M2 wavelength')
plt.axvline(1/15, color='b', linestyle='-', label='Lee wavelength')
#secondary topaxis
ax2 = plt.gca().secondary_xaxis('top', functions=(lambda x: 1/x, lambda x: 1/x))
ax2.set_xlabel('wavelength [km]')
# plt.savefig(f'{fig_path}/w_wavespec_{data_exp9.depth.values:2f}_spec.png', dpi=300)

# %%
