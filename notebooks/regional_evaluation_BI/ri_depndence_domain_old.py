# %%
# Evaluate slope of over MLD depth and front averaged profiles
# manually selected MLD, visualization of vertical structure function
# %%
import sys
#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
sys.path.insert(0, "../multiple_fronts_evaluation_BI/")

import glob, os
import dask as da
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload

import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
# import slope_param_vs_diagnostic_inclined_funcs as hal
import domain_funcs as domfunc
import simple_regression_func as mylinreg
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)
client, cluster = scluster.init_dask_slurm_cluster(walltime='01:30:00', wait=False)
cluster
client
# %%
grain         = False
savefig       = False
time_averaged = ''

fpath_ckdtree      = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
path_save_img_root = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/regional/'
if False: fpath_ckdtree = '/Users/mepke/smt_local_data/smt_res0.02_180W-180E_90S-90N.nc'
grid = xr.open_dataset(fpath_ckdtree)

# %% load cropped and interpolated data
path_to_interp_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/2d_mean/'
# path_to_interp_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/'
if False: path_to_interp_data = '/Users/mepke/smt_local_data/NA_002dgr/'

data_wb_prime_mean       = xr.open_dataarray(f'{path_to_interp_data}/data_wb_prime_mean.nc')
data_ub_prime_mean_depthi= xr.open_dataarray(f'{path_to_interp_data}/data_ub_prime_mean_depthi.nc')
data_vb_prime_mean_depthi= xr.open_dataarray(f'{path_to_interp_data}/data_vb_prime_mean_depthi.nc')
data_n2_mean             = xr.open_dataarray(f'{path_to_interp_data}/data_n2_mean.nc')
data_dbdx_mean           = xr.open_dataarray(f'{path_to_interp_data}/data_dbdx_mean.nc')
data_dbdy_mean           = xr.open_dataarray(f'{path_to_interp_data}/data_dbdy_mean.nc')
data_M2_mean             = xr.open_dataarray(f'{path_to_interp_data}/data_M2_mean.nc')
data_rho_mean_depthi     = xr.open_dataarray(f'{path_to_interp_data}/data_rho_mean_depthi.nc')
mask_land                = xr.open_dataarray(f'{path_to_interp_data}/mask_land.nc')
mask_mld                 = xr.open_dataarray(f'{path_to_interp_data}/mask_mld.nc')
mldx                     = xr.open_dataarray(f'{path_to_interp_data}/mldx.nc')

# %%
lon_reg = -80.5, -55
lat_reg = 23,38
# lon_reg = np.array([-70, -54])
# lat_reg = np.array([23.5, 36])
reload(eva)
mask_land_100 = eva.load_smt_land_mask(depthc=90) #adapted due to island outliers
mask_land_100 = pyic.interp_to_rectgrid_xr(mask_land_100, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land_100 = mask_land_100.drop('depthc')


# %% Masking land, MLD and filtering M2
mask_used = mask_land_100 * mask_mld
# mldx_used = mldx
n2_sel    = data_n2_mean

m2_sel  = data_M2_mean
m2x_sel = data_dbdx_mean
m2y_sel = data_dbdy_mean
rho     = data_rho_mean_depthi

#### Filter
m2_min_lim = 0 #obsolet

m2         = m2_sel.where(np.abs(m2_sel) > m2_min_lim, np.nan)
m2         = m2.where(~np.isinf(m2),np.nan) #infs to nans
m2_mask    = m2 * mask_used
m2_mask    = m2_mask.where(~(m2_mask == 0), np.nan) # remove zeros
m2_mask    = m2_mask.rename('m2')

m2x      = m2x_sel.where(np.abs(m2x_sel) > m2_min_lim, np.nan)
m2x      = m2x.where(~np.isinf(m2x),np.nan) #infs to nans
m2x_mask = m2x * mask_used 
m2x_mask = m2x_mask.where(~(m2x_mask == 0), np.nan) # remove zeros
m2x_mask = m2x_mask.rename('m2x')

m2y      = m2y_sel.where(np.abs(m2y_sel) > m2_min_lim, np.nan)
m2y      = m2y.where(~np.isinf(m2y),np.nan) #infs to nans
m2y_mask = m2y * mask_used 
m2y_mask = m2y_mask.where(~(m2y_mask == 0), np.nan) # remove zeros
m2y_mask = m2y_mask.rename('m2y')

n2      = n2_sel.where(~np.isinf(n2_sel),np.nan) #infs to nans
n2_mask = n2 * mask_used  #application of mask leads to new zero values
n2_mask = n2_mask.where(~(n2_mask == 0), np.nan) # remove zeros
n2_mask = n2_mask.rename('n2')

wb_mask = data_wb_prime_mean * mask_used 
wb_mask = wb_mask.where(~(wb_mask == 0), np.nan) # remove zeros
wb_mask = wb_mask.rename('wb')

vb_mask = data_vb_prime_mean_depthi * mask_used
vb_mask = vb_mask.where(~(m2_mask == 0), np.nan) # remove zeros
vb_mask = vb_mask.rename('vb')

ub_mask = data_ub_prime_mean_depthi * mask_used
ub_mask = ub_mask.where(~(m2_mask == 0), np.nan) # remove zeros
ub_mask = ub_mask.rename('ub')

rho_mask = rho * mask_used
rho_mask = rho_mask.where(~(m2_mask == 0), np.nan) # remove zeros
rho_mask = rho_mask.rename('rho')

#######################################################
#######################################################
# %% diagnostic
A = rho_mask.to_dataset()
A = A.assign(ub = ub_mask)
A = A.assign(vb = vb_mask)
A = A.assign(wb = wb_mask)
A = A.assign(n2 = n2_mask)
# A = A.assign(m2 = m2_mask)
print('Set alongfront m2 to zero and crossfront value as absolute value of m2x and m2y')
A = A.assign(m2_along_grad = 0)
A = A.assign(m2_cross_grad = np.sqrt(np.power(m2x_mask,2) + np.power(m2y_mask,2)))
# A = A.assign(m2_along_grad = m2x_mask)
# A = A.assign(m2_cross_grad = m2x_mask + m2y_mask)
A = A.assign(mld           = mldx)
if False:
    print('vb calculation with sign')
    print('not ready for parameterizations')
    A = A.assign(Vb_cross_grad = ( m2x_mask * ub_mask + m2y_mask * vb_mask) / (np.sqrt(np.power(m2x_mask,2) + np.power(m2y_mask,2))) ) # projection of fluctuation on front gradient
else:
    print('negative absolute value of vb')
    A = A.assign(Vb_cross_grad =  - np.abs( ( m2x_mask * ub_mask + m2y_mask * vb_mask) / (np.sqrt(np.power(m2x_mask,2) + np.power(m2y_mask,2))) ) )# projection of fluctuation on front gradient

A = A.assign(fcoriolis     = eva.calc_coriolis_parameter(A.lat))
A = A.assign(alpha         = eva.calc_alpha( A.m2_cross_grad, A.fcoriolis))
# A = A.assign(alpha_x       = eva.calc_alpha( m2x_mask, A.fcoriolis))
# A = A.assign(alpha_y       = eva.calc_alpha( m2y_mask, A.fcoriolis))
A = A.assign(m2x           = m2x_mask)
A = A.assign(m2y           = m2y_mask)


A_depth = A
A       = A.mean(dim='depthi', skipna=True)
print('calc ri always after all averaging steps')
A = A.assign(ri_diag       = eva.calc_richardsonnumber(A.lat, A.n2, A.m2_cross_grad))

if False: psi_x, psi_y, psi_z =  eva.calc_streamfunc_full_3d(A.ub, A.vb, A.wb, A.m2x, A.m2y, A.n2)


A = A.assign(grad_b        = eva.calc_abs_grad_b(A.m2_along_grad, A.m2_cross_grad, A.n2))
A = A.assign(psi_diag_held = eva.calc_streamfunc_held_schneider(A.wb, A.m2_along_grad, A.m2_cross_grad))
A = A.assign(psi_diag_full = eva.calc_streamfunc_full(A.wb, A.m2_cross_grad, A.Vb_cross_grad, A.n2, A.grad_b))
A = A.assign(K             = eva.calc_diapycnal_diffusivity(A.Vb_cross_grad, A.m2_cross_grad, A.wb, A.n2, A.grad_b))
if False:
    A = A.assign(psi_x = psi_x)
    A = A.assign(psi_y = psi_z) # todo
    A = A.assign(psi_z = psi_x)

    # %%
if False:
    ######################################## Uchida analysis ########################################
    depthi      = xr.DataArray(depthi, dims='depthi')
    dz_diff     = depthi.diff(dim='depthi')
    dz_diff_ext = np.append(dz_diff, np.nan)
    dz_diff_ext = xr.DataArray(dz_diff_ext, dims='depthi')
    area        = (0.02 * eva.convert_degree_to_meter(A.lat))**2

    M2_sum = np.sqrt(((A_depth.m2x)**2  + (A_depth.m2y)**2))
    # M2_sum = (np.abs(A_depth.m2x)  + np.abs(A_depth.m2y))
    M2_sum = (M2_sum * dz_diff_ext).sum(dim='depthi', skipna=True) 
    MLI    = M2_sum**2 / A_depth.fcoriolis
    MLI    = MLI * mask_land_100
    MLI.plot(vmin=0, vmax=1e-6, cmap='Blues')
    # %%
    wb = (A_depth.wb * dz_diff_ext).sum(dim='depthi', skipna=True) / A_depth.mld * mask_land_100 #scale depths with depth of each cell
    wb.plot(vmin=-1e-7,vmax=1e-7, cmap='RdBu_r')
    # %%



    lon_reg       = np.array([-70, -54])
    lat_reg       = np.array([23.5, 36])
    lat_start_idx = np.abs(wb.lat - lat_reg[0]).argmin().data
    lat_end_idx   = np.abs(wb.lat - lat_reg[1]).argmin().data
    lon_start_idx = np.abs(wb.lon - lon_reg[0]).argmin().data
    lon_end_idx   = np.abs(wb.lon - lon_reg[1]).argmin().data
    # print(lat_start_idx, lat_end_idx, lon_start_idx, lon_end_idx)

    # %%
    # select region
    Ce = wb[lat_start_idx: lat_end_idx, lon_start_idx:lon_end_idx] / MLI[lat_start_idx: lat_end_idx, lon_start_idx:lon_end_idx]
    Ce.plot(vmin=-1, vmax=1, cmap='RdBu_r')
    # %%
    print('mean',Ce.mean().values,'median', Ce.median().values)
    (Ce.median()*MLI[lat_start_idx: lat_end_idx, lon_start_idx:lon_end_idx]).plot(vmin=-1e-7, vmax=1e-7, cmap='RdBu_r')

    # %% compute RI

    # %% Stone tuning coefficient
    N2_sum = ((A_depth.n2*dz_diff_ext).sum(dim='depthi', skipna=True)) / A_depth.mld * mask_land_100
    N2_sum.plot(vmin=5e-6, vmax=5e-5)
    # %%
    Ri = ((N2_sum * A_depth.fcoriolis**2) / M2_sum**2) 
    Ri.plot(vmin=0, vmax=1e-3, cmap='Blues_r')

    # %%
    A.ri_diag.plot(vmin=0, vmax=100, cmap='Blues_r')

    # %%
    M2 = np.sqrt(((A_depth.m2x)**2  + (A_depth.m2y)**2)) * mask_land_100
    N2 = A_depth.n2 * mask_land_100
    Ri = ((N2 * A_depth.fcoriolis**2) / M2**2)
    # %%
    # N2_mean = A_depth.n2.isel(depthi=30)
    # M2_mean = M2_sum.isel(depthi=30)
    N2_mean = ((A_depth.n2*dz_diff_ext).mean(dim='depthi', skipna=True)) * mask_land_100 #/ A_depth.mld
    M2      = np.sqrt(((A_depth.m2x)**2  + (A_depth.m2y)**2))
    M2_mean = (M2*dz_diff_ext).mean(dim='depthi', skipna=True) #/ A_depth.mld
    Ri = ((N2_mean * A_depth.fcoriolis**2) / M2_mean**2) 

    Ri.plot(vmin=0, vmax=100, cmap='Blues_r')

    # %%
    N2_mean = ((A_depth.n2).mean(dim='depthi', skipna=True)) * mask_land_100 #/ A_depth.mld
    M2 = np.sqrt(((A_depth.m2x)**2  + (A_depth.m2y)**2))
    M2_mean = (M2).mean(dim='depthi', skipna=True) #/ A_depth.mld

    Ri = ((N2_mean * A_depth.fcoriolis**2) / M2_mean**2) 
    Ri.plot(vmin=0, vmax=100, cmap='Blues_r')

    # %%
    x = np.linspace(0, 200, 100)
    y = 1/np.sqrt(1+x)
    plt.plot(x,y)
    plt.xlabel('Ri')
    plt.ylabel('1/sqrt(1+Ri)')

    # %%
    Cs = wb[lat_start_idx: lat_end_idx, lon_start_idx:lon_end_idx] / MLI[lat_start_idx: lat_end_idx, lon_start_idx:lon_end_idx] * 1/np.sqrt(1+Ri[lat_start_idx: lat_end_idx, lon_start_idx:lon_end_idx])
    Cs.plot(vmin=-1, vmax=1, cmap='RdBu_r')
    print('mean',Cs.mean().values,'median', Cs.median(skipna=True).values)
    # %%
    (1/np.sqrt(1+Ri[lat_start_idx: lat_end_idx, lon_start_idx:lon_end_idx])).plot(vmin=0.9, vmax=1 , cmap='RdBu')
    # %%
    (Cs.median(skipna=True)*MLI[lat_start_idx: lat_end_idx, lon_start_idx:lon_end_idx]).plot(vmin=-1e-7, vmax=1e-7, cmap='RdBu_r')

# %%

# lon_reg = np.array([-70, -54])
# lat_reg = np.array([23.5, 36])
lon_reg = np.array([-66, -54])
lat_reg = np.array([30, 36])
A = A.sel(lon=slice(lon_reg[0], lon_reg[1])).sel(lat=slice(lat_reg[0], lat_reg[1]))
#####################################################################
# %% cf and cs tuning by data
A_sel   = A.sel(lon=slice(lon_reg[0], lon_reg[1])).sel(lat=slice(lat_reg[0], lat_reg[1]))
mask_ri = A_sel.ri_diag.where(A_sel.ri_diag < 8, np.nan)
mask_ri = mask_ri.where(np.isnan(mask_ri), 1)
mask_ri.plot()
mask_ri = mask_ri.rename('None')
mask_ri = mask_ri.rename(None)

for var in A_sel.data_vars:
    A_sel[var] = A_sel[var] * mask_ri #* mask_vb

cf_wb = A_sel.wb * 1 / (A_sel.alpha**2 * A_sel.mld**2 * A_sel.fcoriolis**3)
cs_wb = A_sel.wb * np.sqrt(1 + A_sel.ri_diag) * 1 / (A_sel.alpha**2 * A_sel.mld**2 * A_sel.fcoriolis**3)
cf_vb = - A_sel.Vb_cross_grad * 1 / (2 * A_sel.ri_diag * A_sel.alpha**3 * A_sel.mld**2 * A_sel.fcoriolis**3)
cs_vb = - 5/8 * A_sel.Vb_cross_grad / (np.sqrt(1 + A_sel.ri_diag) * A_sel.alpha**3 * A_sel.mld**2 * A_sel.fcoriolis**3)

cf_wb_mean = cf_wb.mean(skipna=True).values
cs_wb_mean = cs_wb.mean(skipna=True).values
cf_vb_mean = cf_vb.mean(skipna=True).values
cs_vb_mean = cs_vb.mean(skipna=True).values

cf_wb_median = cf_wb.median(skipna=True).values
cs_wb_median = cs_wb.median(skipna=True).values
cf_vb_median = cf_vb.median(skipna=True).values
cs_vb_median = cs_vb.median(skipna=True).values

# print('mean values: cf_wb', cf_wb_mean, 'cs_wb', cs_wb_mean, 'cf_vb', cf_vb_mean, 'cs_vb', cs_vb_mean)
# print('median values: cf_wb', cf_wb_median, 'cs_wb', cs_wb_median, 'cf_vb', cf_vb_median, 'cs_vb', cs_vb_median)

#above but with only 4 digits
print('mean values: cf_wb', np.round(cf_wb.mean().values,4), 'cs_wb', np.round(cs_wb.mean().values,4),  ' \n cf_vb', np.round(cf_vb.mean().values,4), 'cs_vb', np.round(cs_vb.mean().values,4))
# print('median values: cf_wb', np.round(cf_wb.median().values,4), 'cs_wb', np.round(cs_wb.median().values,4), 'cf_vb', np.round(cf_vb.median().values,4), 'cs_vb', np.round(cs_vb.median().values,4))

opt_var = 'wb'
if opt_var == 'wb':
    cf = cf_wb_mean
    cs = cs_wb_mean
elif opt_var == 'vb':
    cf = cf_vb_mean
    cs = cs_vb_mean#.interpolate_na(dim='lat', method='linear').interpolate_na(dim='lon', method='linear').values
elif opt_var == 'both': #mean of both
    cf = (cf_wb_mean + cf_vb_mean) / 2
    cs = (cs_wb_mean + cs_vb_mean) / 2

print('cf', cf, 'cs', cs)

# %% Errors wb
wb_fox_param   = eva.calc_wb_fox_param(A_sel.lat, A_sel.mld,  A_sel.m2_cross_grad, np.nan, cf_wb_mean, structure=False )
wb_stone_param = eva.calc_wb_stone_param(A_sel.lat, A_sel.mld,  A_sel.m2_cross_grad, A_sel.ri_diag, cs_wb_mean, np.nan, structure=False )
error_wb_fox   = np.nanmean(np.sqrt(np.power(wb_fox_param - A_sel.wb, 2)))
error_wb_stone = np.nanmean(np.sqrt(np.power(wb_stone_param - A_sel.wb, 2)))
print(f'error wb fox {error_wb_fox:.2e}', f'\n error wb stone {error_wb_stone:.2e}')

# %% Errors vb
Vb_cross_grad_fox_param = eva.calc_vb_fox_param(A_sel.lat, A_sel.mld,  A_sel.m2_cross_grad, A_sel.ri_diag, np.nan, cf_vb_mean, structure=False ) #no sign dependence of vb cross gradient and no sign dependence of crossfront gradient
vb_stone_param          = eva.calc_vb_stone_param(A_sel.lat, A_sel.mld,  A_sel.m2_cross_grad, A_sel.ri_diag, cs_vb_mean)
error_vb_fox            = np.nanmean(np.sqrt(np.power(Vb_cross_grad_fox_param - A_sel.Vb_cross_grad, 2)))
error_vb_stone          = np.nanmean(np.sqrt(np.power(vb_stone_param - A_sel.Vb_cross_grad, 2)))
print(f'error vb fox {error_vb_fox:.2e}', f'\n error vb stone {error_vb_stone:.2e}')

# %%
#above with logarithmic axis
# plt.figure()
# plt.scatter(A.ri_diag.data.flatten(),wb_fox_param.data.flatten())
# plt.xscale('log')
# plt.yscale('log')
# %%
if False:
    # # vertical profiles are calculated 
    mask_nan             = A_depth.m2_cross_grad
    mask_nan             = mask_nan.where(np.isnan(mask_nan),1.)
    depthI               = A_depth.m2_cross_grad.depthi
    wb_fox_param         = mask_nan * eva.calc_wb_fox_param(A_depth.lat, A_depth.mld,  A.m2_cross_grad, -depthI, cf, structure=True )
    vb_fox_param         = mask_nan * eva.calc_vb_fox_param(A_depth.lat, A_depth.mld,  A.m2_cross_grad, A.ri_diag, -depthI, cf, structure=True )
    wb_stone_param       = mask_nan * eva.calc_wb_stone_param(A_depth.lat, A_depth.mld,  A.m2_cross_grad, A.ri_diag, cs, -depthI, structure = True)
    wb_fox_param         = wb_fox_param.mean(dim='depthi', skipna=True)
    vb_fox_param         = vb_fox_param.mean(dim='depthi', skipna=True)
    wb_stone_param       = wb_stone_param.mean(dim='depthi', skipna=True)
else:
    print('no vertical structure')
    wb_fox_param            = eva.calc_wb_fox_param(A.lat, A.mld,  A.m2_cross_grad, np.nan, cf, structure=False )
    if True:
        print('vb fox without sign')
        Vb_cross_grad_fox_param =  eva.calc_vb_fox_param(A.lat, A.mld,  A.m2_cross_grad, A.ri_diag, np.nan, cf, structure=False ) #no sign dependence of vb cross gradient and no sign dependence of crossfront gradient
    else: 
        ub_fox_param            = eva.calc_vb_fox_param(A.lat, A.mld,  A.m2x, A.ri_diag, np.nan, cf, structure=False )
        vb_fox_param            = eva.calc_vb_fox_param(A.lat, A.mld,  A.m2y, A.ri_diag, np.nan, cf, structure=False )
        Vb_cross_grad_fox_param =  ( A.m2x * ub_fox_param + A.m2y * vb_fox_param) / (np.sqrt(np.power(A.m2x,2) + np.power(A.m2y,2)))  # projection of fluctuation on front gradient

    wb_stone_param       = eva.calc_wb_stone_param(A.lat, A.mld,  A.m2_cross_grad, A.ri_diag, cs, np.nan, structure=False )

vb_stone_param       = eva.calc_vb_stone_param(A.lat, A.mld,  A.m2_cross_grad, A.ri_diag, cs)
psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param,  A.m2_cross_grad, vb_stone_param, A.n2, A.grad_b)
psi_stone_held_param = eva.calc_streamfunc_held_schneider(wb_stone_param, A.m2_along_grad,   A.m2_cross_grad)
K_stone              = eva.calc_diapycnal_diffusivity(vb_stone_param, A.m2_cross_grad, wb_stone_param, A.n2, A.grad_b)

psi_x_fox, psi_y_fox = eva.calc_streamfunc_full_3d_fox(A.lat, A.mld,  A.m2x, A.m2y, np.nan, cf, structure=False)
if False: ub_fox_param_3d, vb_fox_param_3d,  wb_fox_param_3d = eva.calc_cross_product(psi_x_fox, psi_y_fox, 0, A.m2x, A.m2y, A.n2)
# psi_x_fox_ind, psi_y_fox_ind, psi_z_fox_ind =  eva.calc_streamfunc_full_3d(ub_fox_param_3d, vb_fox_param_3d,  wb_fox_param_3d, A.m2x, A.m2y, A.n2) # its the same!!! very good!

psi_fox_full_param   = eva.calc_streamfunc_full(wb_fox_param,  A.m2_cross_grad, Vb_cross_grad_fox_param, A.n2, A.grad_b)
psi_fox_held_param   = eva.calc_streamfunc_held_schneider(wb_fox_param, A.m2_along_grad,  A.m2_cross_grad)
print('Held Schneider does not have sign of overturning')
K_fox                = eva.calc_diapycnal_diffusivity(Vb_cross_grad_fox_param, A.m2_cross_grad, wb_fox_param, A.n2, A.grad_b)

A = A.assign(wb_fox_param         = wb_fox_param         )
# A = A.assign(ub_fox_param         = ub_fox_param         )
# A = A.assign(vb_fox_param         = vb_fox_param         )
A = A.assign(Vb_cross_grad_fox_param = Vb_cross_grad_fox_param )
A = A.assign(psi_fox_full_param   = psi_fox_full_param   )
A = A.assign(psi_fox_held_param   = psi_fox_held_param   )

A = A.assign(wb_stone_param       = wb_stone_param       )
A = A.assign(vb_stone_param       = vb_stone_param       )
A = A.assign(psi_stone_full_param = psi_stone_full_param )
A = A.assign(psi_stone_held_param = psi_stone_held_param )
A = A.assign(K_fox                = K_fox                )  
A = A.assign(K_stone              = K_stone              )  
A = A.assign(psi_x_fox          = psi_x_fox      )
A = A.assign(psi_y_fox          = psi_y_fox      )


# %%
# A.wb_stone_param[:,:] = A.wb[:,:]

# reload(hal)
reload(domfunc)
domain    = 'multiple'
var       = 'vb'
if var == 'psi': print('in the calculation of psi, the sign dependence is not resolved in the parameterizations and might not be correct in the diagnostic >> it affects the shape of the cloud strongly')
filter    = 'ri'

if filter == 'm2':
    path = f'{path_save_img_root}area/filter_{filter}/'
    ds_m2_0   = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=0   )
    ds_m2_1e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=1e-8)
    ds_m2_3e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=3e-8)
    ds_m2_5e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=5e-8)

    path = f'{path_save_img_root}ri_vs_{var}_regional/'
    domfunc.plot_slopes_scatter_all_m2(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path, domain, savefig, var)
elif filter == 'ri':
    path = f'{path_save_img_root}area/filter_{filter}/'
    ds_m2_0   = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=1e4)
    ds_m2_1e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=1e3)
    ds_m2_3e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=1e2)
    ds_m2_5e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=1e1)

    path = f'{path_save_img_root}ri_vs_{var}_regional/'
    # domfunc.plot_slopes_scatter_all_ri(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path, domain, savefig, var)
else:
    raise Warning('Error: no filter selected')


# %% Linear regression
reload(mylinreg)
if False:
    mylinreg.plot_regression(logx, logy, filename='linreg_all', path=path, savefig=True)
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, 80)
    # mylinreg.plot_regression(bin_centers, bin_means, filename='linreg_binned_no_weights', path=path, savefig=True)
    mylinreg.plot_regression_binned(bin_centers, bin_means, bin_stds)
    mylinreg.plot_regression_binned(bin_centers, bin_means, 1)

    mylinreg.plot_normalized_vs_non_normalized(bin_centers, bin_means, bin_stds)
    mylinreg.plot_two_slopes(bin_centers, bin_means, bin_stds)
    mylinreg.plot_two_slopes_AI(bin_centers, bin_means, bin_stds)

    reload(domfunc)
    domfunc.plot_2slopes_binned_ri(ds_m2_0, path, domain, savefig, var, time_window='1W')

# %
reload(domfunc)
domfunc.plot_2slopes_binned_ri_new(ds_m2_0, path, domain, savefig, var, time_window='2d')

# %% Plot maps
reload(domfunc)
data = ds_m2_0
path = f'{path_save_img_root}area/visual_eval/'

domfunc.plot_wb_maps      (data, domain, path, savefig)
domfunc.plot_vb_maps      (data, domain, path, savefig)
domfunc.plot_K_maps       (data, domain, path, savefig)
domfunc.plot_N2_map       (data, domain, path, savefig)
domfunc.plot_M2_map       (data, domain, path, savefig)
domfunc.plot_psi_maps     (data, domain, path, savefig)
domfunc.plot_psi_held_maps(data, domain, path, savefig)

domfunc.plot_ri_diag_fronts(data, savefig, path_save_img_root)



# %%
# reload(domfunc)
# path_save_img = f'{path_save_img_root}ri_vs_{var}_regional/'
# domfunc.plot_slopes_scatter(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path_save_img, domain, savefig)

# %%
# for raw data with negative and positive ri numbers: adapt log mask in calc_log2
if False:
    path = f'{path_save_img_root}ri_vs_{var}_regional/raw_data/'
    domfunc.plot_slopes_scatter_raw(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path_save_img, domain, savefig)
# %%
# domfunc.plot_wb_scatter(ds_m2_0)
# %%
# reload(hal)
path = f'{path_save_img_root}area/'
domfunc.plot_ri_diag_fronts(ds_m2_0, savefig, path)


# %%
