# %%
# Evaluate slope of over MLD depth and front averaged profiles
# manually selected MLD, visualization of vertical structure function

import sys

sys.path.insert(0, "../../")
sys.path.insert(0, "../multiple_fronts_evaluation_BI/")

import glob, os
import dask as da
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload

import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
# import slope_param_vs_diagnostic_inclined_funcs as hal
import domain_funcs as domfunc
import simple_regression_func as mylinreg
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)
client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00', wait=False)
cluster
client
# %%
savefig            = False
path_save_img_root = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/regional/'

# %%
reload(domfunc)
time_ave     = '1W'
prime_filter = 'temporal' #only for multiple domain
m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx = domfunc.load_clean_dataset(time_ave, prime_filter)
#%%
reload(domfunc)
ds, ds_depth = domfunc.concat_dataset(m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx)
# %%
reload(eva)
reload(domfunc)
domain      = 'multiple'
ri_critical = 8
ds_sel      = domfunc.select_domain(ds, domain)

cf, cs   = domfunc.calc_tuning_coefficients(ds_sel.copy(), ri_critical, opt_var='wb', ave_type='median')
# cf, cs   = domfunc.calc_tuning_coefficients_min_error2(ds_sel.copy(), ri_critical, opt_var='wb', ave_type='median')
# cf, cs   = domfunc.calc_tuning_coefficients_min_error(ds_sel.copy(), ri_critical, opt_var='wb')

ds_param = domfunc.apply_parameterization(ds_sel, cf, cs)

path   = f'{path_save_img_root}area/vb_wb/'
#%%
ds_vb = domfunc.apply_log_mask(ds_param, 'vb', varx='ri')
ds_wb = domfunc.apply_log_mask(ds_param, 'wb', varx='ri')

reload(domfunc)
string = f'{domain}_ri{ri_critical}_{time_ave}_cs{cs:.2f}_cf{cf:.2f}_{prime_filter}_filt'
# domfunc.plot_2slopes_binned_ri_new(ds_vb, path, domain, savefig, 'vb', time_ave)
# domfunc.plot_2slopes_binned_ri_new(ds_wb, path, domain, savefig, 'wb', time_ave)
domfunc.plot_2slopes_binned_ri_both(ds_vb, ds_wb, path, savefig, ri_critical, string)

# %% ############################## ALPHA DEPENDENCE ########################################
path   = f'{path_save_img_root}area/vb_wb/alpha/'
reload(domfunc)
string = f'alpha_dep_ALS_ri_filt_l'
ds_vb = domfunc.apply_log_mask(ds_param, 'vb', varx='alpha_ALS')
ds_wb = domfunc.apply_log_mask(ds_param, 'wb', varx='alpha_ALS')

reload(domfunc)
# ylabel1 = r'$log_{10} \sqrt[3]{|\overline{v^{\prime} b^{\prime}}|/(H^2 f^3 Ri)}  $'
# ylabel2 = r'$log_{10} \sqrt{|\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 Ri)} $'

ylabel1 = r'$log_{10} |\overline{v^{\prime} b^{\prime}}|/(H^2 f^3 \sqrt{1+Ri})  $'
ylabel2 = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 ) \sqrt{1+Ri} $'
domfunc.plot_alpha_reg(ds_vb, ds_wb, ylabel1, ylabel2, path, savefig, string)


# %%
if True:
    # %%
    string = f'alpha_dep_no_ri'
    reload(domfunc)
    ds_vb_no_ri = domfunc.apply_log_mask(ds_param, 'vb', varx='alpha_no_ri')
    ds_wb_no_ri = domfunc.apply_log_mask(ds_param, 'wb', varx='alpha_no_ri')
    
    ylabel1 = r'$log_{10} \sqrt[3]{|\overline{v^{\prime} b^{\prime}}|/(H^2 f^3)}  $'
    ylabel2 = r'$log_{10} \sqrt{|\overline{w^{\prime} b^{\prime}}|/(H^2 f^3)} $'
    
    domfunc.plot_alpha_reg(ds_vb_no_ri, ds_wb_no_ri, ylabel1, ylabel2, path, savefig, string)
    
    #%% both
    reload(domfunc)
    string = f'alpha_dep_all'
    domfunc.plot_alpha_reg_all(ds_vb, ds_wb, ds_vb_no_ri, ds_wb_no_ri, ylabel1, ylabel2, path, savefig, string)
    #%%
    print('change calulatoion of lhs')
    string = f'alpha_dep_no_ri_old'
    reload(domfunc)
    ds_vb = domfunc.apply_log_mask(ds_param, 'vb', varx='alpha_no_ri')
    ds_wb = domfunc.apply_log_mask(ds_param, 'wb', varx='alpha_no_ri')

    ylabel1 = r'$log_{10} |\overline{v^{\prime} b^{\prime}}|/(H^2 f^3)  $'
    ylabel2 = r'$log_{10}|\overline{w^{\prime} b^{\prime}}|/(H^2 f^3) $'
    # domfunc.plot_alpha_reg(ds_vb, ds_wb, ylabel1, ylabel2, path, savefig, string)
    domfunc.plot_alpha_reg_new(ds_vb, ds_wb, ylabel1, ylabel2, path, savefig, string)

    #%%
    print('change calulatoion of lhs')
    string = f'alpha_dep_PER_m'
    reload(domfunc)
    ds_vb = domfunc.apply_log_mask(ds_param, 'vb', varx='alpha_PER')
    ds_wb = domfunc.apply_log_mask(ds_param, 'wb', varx='alpha_PER')

    ylabel1 = r'$log_{10} |\overline{v^{\prime} b^{\prime}}|/(H^2 f^3) / Ri  $'
    ylabel2 = r'$log_{10}|\overline{w^{\prime} b^{\prime}}|/(H^2 f^3) $'
    domfunc.plot_alpha_reg_new(ds_vb, ds_wb, ylabel1, ylabel2, path, savefig, string)

    #%%
    print('change calulatoion of lhs')
    string = f'alpha_dep_PER_m'
    reload(domfunc)
    # ds_vb_per = domfunc.apply_log_mask(domfunc.mask_ri(ds_param,None), 'vb', varx='alpha_PER')
    # ds_wb_per = domfunc.apply_log_mask(domfunc.mask_ri(ds_param,None), 'wb', varx='alpha_PER')
    # ds_vb_als = domfunc.apply_log_mask(ds_param, 'vb', varx='alpha_ALS')
    # ds_wb_als = domfunc.apply_log_mask(ds_param, 'wb', varx='alpha_ALS')

    vb_per = []
    wb_per = []
    ri_threshold = np.array([None, 50, 20, 10])
    for ri in ri_threshold:
        print(ri)
        ds_vb_per = domfunc.apply_log_mask(domfunc.mask_ri(ds_param,ri), 'vb', varx='alpha_PER')
        ds_wb_per = domfunc.apply_log_mask(domfunc.mask_ri(ds_param,ri), 'wb', varx='alpha_PER')
        vb_per.append(ds_vb_per)
        wb_per.append(ds_wb_per)

    vb_als = []
    wb_als = []
    # ri_threshold = np.array([None, 100, 50, 10])
    for ri in ri_threshold:
        print(ri)
        ds_vb_als = domfunc.apply_log_mask(domfunc.mask_ri(ds_param,ri), 'vb', varx='alpha_ALS')
        ds_wb_als = domfunc.apply_log_mask(domfunc.mask_ri(ds_param,ri), 'wb', varx='alpha_ALS')
        vb_als.append(ds_vb_als)
        wb_als.append(ds_wb_als)

    #%%
    reload(domfunc)
    string = f'alpha_dep_PER_ALS'

    ylabel1 = r'$log_{10} 1/2 |\overline{v^{\prime} b^{\prime}}|/(H^2 f^3) / Ri   $'
    ylabel2 = r'$log_{10}|\overline{w^{\prime} b^{\prime}}|/(H^2 f^3) $'
    ylabel3 = r'$log_{10} 5/8 |\overline{v^{\prime} b^{\prime}}|/(H^2 f^3 \sqrt{1+Ri})  $'
    ylabel4 = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 ) \sqrt{1+Ri} $'
    domfunc.plot_alpha_reg_4a(vb_per, wb_per, vb_als, wb_als, ylabel1, ylabel2, ylabel3, ylabel4, ri_threshold, path, savefig, string)

# %%

# generate discret colormap from 0 to 200 with 10 steps
import matplotlib.colors as mcolors
cmap = plt.cm.Spectral
colors = cmap(np.linspace(0, 1, 10))
cmap = mcolors.ListedColormap(colors)
cmap.set_bad(color='white')
cmap.set_over(color='white')


plt.figure(figsize=(10, 10))

ds_param.ri_diag.plot(cmap=cmap, vmin=0, vmax=100)

# ds_param.ri_diag.plot(cmap='Spectral', vmin=0, vmax=200)


#%%
ds_sel = ds_param.copy()
mask_ri = ds_sel.ri_diag.where(ds_sel.ri_diag < 50, np.nan)
mask_ri = mask_ri.where(np.isnan(mask_ri), 1)
# mask_ri.plot()
mask_ri = mask_ri.rename('None')
mask_ri = mask_ri.rename(None)
for var in ds_sel.data_vars:
    ds_sel[var] = ds_sel[var] * mask_ri #* mask_vb



# %%
reload(domfunc)
ydata = ds_param['ri_diag'].values
xdata = ds_param['alpha'].values
xlabel = r'$log10( \alpha^2)$'
ylabel = r'$log10(Ri)$'
string = f'alpha_ri_log'

domfunc.plot_heatmap_log(xdata, ydata, xlabel, ylabel, path, savefig, string)
# %%
reload(domfunc)
xdata = ds_param['alpha'].values**2
ydata = ( ds_param['wb']  / (ds_param['fcoriolis']**3 * ds_param['mld']**2)).values
xlabel = r'$log10( \alpha^2)$'
ylabel = r'$log10( wb_{prime} / H^2/f^3)$'
string = f'alpha_wb'

domfunc.plot_heatmap_log(xdata, ydata, xlabel, ylabel, path, savefig, string)
# %%
reload(domfunc)
xdata = ds_param['alpha'].values**3
ydata = ( ds_param['Vb_cross_grad']  / (ds_param['fcoriolis']**3 * ds_param['mld']**2)).values
xlabel = r'$log10( \alpha^3)$'
ylabel = r'$log10( vb_{prime} / H^2/f^3)$'
string = f'alpha_vb'

domfunc.plot_heatmap_log(np.abs(xdata), np.abs(ydata), xlabel, ylabel, path, savefig, string)
# %%
