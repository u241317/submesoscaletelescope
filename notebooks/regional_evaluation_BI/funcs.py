import sys
#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")

import glob, os
import dask as da
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import string

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload

import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
# import slope_param_vs_diagnostic_inclined_funcs as hal
import domain_funcs as domfunc
import cartopy


def rossby_radius(data, data_r, lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])


    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb='bottom', asp=asp, fig_size_fac=2,  projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'YlGnBu'
    contfs = np.arange(0, 60, 5)
    clim   = contfs[0], contfs[-1]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)

    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid')
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    # ax.contour(data.lon, data.lat, data, levels=[1000], colors='r', transform=ccrs_proj)
    # ax.contour(data.lon, data.lat, data, levels=[6000], colors='purple', transform=ccrs_proj)

    ax.set_title('Rossby Radius of Deformation [km]', fontsize = fs)
    ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    # add colorbar non scientific notation
    # cax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    #add land
    ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.7')
    ax.add_feature(cartopy.feature.COASTLINE, zorder=6, linewidth=1)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore


    if savefig == True: plt.savefig(f'{fig_path}_rossby_radius.png', dpi=150, format='png', bbox_inches='tight')


def rossby_radius_l_ml(data, data_r, lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])


    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb='bottom', asp=asp, fig_size_fac=2,  projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'YlGnBu'
    contfs = np.arange(0, 60, 5)
    clim   = contfs[0], contfs[-1]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)

    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid')
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    # ax.contour(data.lon, data.lat, data, levels=[1000], colors='r', transform=ccrs_proj)
    # ax.contour(data.lon, data.lat, data, levels=[6000], colors='purple', transform=ccrs_proj)

    ax.set_title(r'$L_{ML}$ [km]', fontsize = fs)
    ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    # add colorbar non scientific notation
    # cax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    #add land
    ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.7')
    ax.add_feature(cartopy.feature.COASTLINE, zorder=6, linewidth=1)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore


    if savefig == True: plt.savefig(f'{fig_path}_rossby_radius_lml.png', dpi=150, format='png', bbox_inches='tight')



def ml_growth_rate(data, data_r, lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])


    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb='bottom', asp=asp, fig_size_fac=2,  projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'ocean'
    contfs = np.arange(0, 1.5e-5, 1e-6)
    clim   = contfs[0], contfs[-1]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)

    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid')
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    # ax.contour(data.lon, data.lat, data, levels=[1000], colors='r', transform=ccrs_proj)
    # ax.contour(data.lon, data.lat, data, levels=[6000], colors='purple', transform=ccrs_proj)

    ax.set_title(r'$\sigma_{ML}$ [1/s]', fontsize = fs)
    ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    # add colorbar non scientific notation
    # cax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    #add land
    ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.7')
    ax.add_feature(cartopy.feature.COASTLINE, zorder=6, linewidth=1)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore


    if savefig == True: plt.savefig(f'{fig_path}_growth_rate_lml.png', dpi=150, format='png', bbox_inches='tight')


def rossby_radius_bias(data, data_r, lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])


    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb='bottom', asp=asp, fig_size_fac=2,  projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'plasma'
    #take large value of data min max
    contfs = np.arange(0, 30, 2)
    clim  = 0, 30

    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid', zorder=1)
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)

    # add contour at 10
    ax.contour(data.lon, data.lat, data, levels=[10], colors='tab:green', transform=ccrs_proj, linewidths=0.7, alpha=0.8)

    ax.set_title('Rossby Radius / hor. Model res.', fontsize = fs)
    ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.7')
    ax.add_feature(cartopy.feature.COASTLINE, zorder=6, linewidth=1)


    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore

    if savefig == True: plt.savefig(f'{fig_path}_rossby_radius_bias.png', dpi=150, format='png', bbox_inches='tight')



def rossby_radius_res(data, data_r, lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 2, plot_cb=True, asp=asp, fig_size_fac=2,  projection=ccrs_proj)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'YlGnBu'
    contfs = np.arange(0, 15, 1)
    clim   = contfs[0], contfs[-1]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)

    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid')
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    ax.set_title('Mixed Layer Rossby Radius of Deformation [km]', fontsize = fs)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    cmap   = 'plasma'
    contfs = np.arange(0, 30, 2)
    clim  = 0, 30
    data = data/(data_r/1e3)
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid', zorder=1)
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    ax.set_title('Mixed Layer Rossby Radius / hor. Model res.', fontsize = fs)

    # add contour at 10
    ax.contour(data.lon, data.lat, data, levels=[10], colors='tab:green', transform=ccrs_proj, linewidths=0.7, alpha=0.8)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
        t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

        ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.7')
        ax.add_feature(cartopy.feature.COASTLINE, zorder=6, linewidth=1)


    if savefig == True: plt.savefig(f'{fig_path}_rossby_radius_ratio.png', dpi=150, format='png', bbox_inches='tight')



def wavelength_res(data, data_r, lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 2, plot_cb=True, asp=asp, fig_size_fac=2,  projection=ccrs_proj)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'YlGnBu'
    contfs = np.arange(0, 60, 5)
    clim   = contfs[0], contfs[-1]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)

    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid')
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    ax.set_title(r'$L_{ML}$ [km]', fontsize = fs)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    cmap   = 'plasma'
    contfs = np.arange(0, 120, 10)
    clim  = 0, 30
    data = data/(data_r/1e3)
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid', zorder=1)
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    ax.set_title(r'$L_{ML}$ / hor. Model res.', fontsize = fs)

    # add contour at 10
    ax.contour(data.lon, data.lat, data, levels=[50], colors='tab:green', transform=ccrs_proj, linewidths=0.7, alpha=0.8)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
        t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

        ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.7')
        ax.add_feature(cartopy.feature.COASTLINE, zorder=6, linewidth=1)


    if savefig == True: plt.savefig(f'{fig_path}_wavelength_res_ratio.png', dpi=150, format='png', bbox_inches='tight')



def rossby_radius_res_pi(data, data_r, lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 2, plot_cb=True, asp=asp, fig_size_fac=2,  projection=ccrs_proj)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'YlGnBu'
    contfs = np.arange(0, 4, 0.5)
    clim   = contfs[0], contfs[-1]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)

    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid')
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    ax.set_title('Mixed Layer Rossby Radius of Deformation [km]', fontsize = fs)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    cmap   = 'plasma'
    contfs = np.arange(0, 10, 1)
    clim  = contfs[0], contfs[-1]
    data = data/(data_r/1e3)
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid', zorder=1)
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    ax.set_title('Rossby Radius / hor. Model res.', fontsize = fs)

    # add contour at 10
    ax.contour(data.lon, data.lat, data, levels=[4], colors='tab:green', transform=ccrs_proj, linewidths=0.7, alpha=0.8)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
        t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

        ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.7')
        ax.add_feature(cartopy.feature.COASTLINE, zorder=6, linewidth=1)


    if savefig == True: plt.savefig(f'{fig_path}_rossby_radius_ratio_pi.png', dpi=150, format='png', bbox_inches='tight')


def rossby_radius_res_pi_all(rd_1, data, data_r, lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 3, plot_cb=True, asp=asp, fig_size_fac=2,  projection=ccrs_proj)

    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'YlGnBu'
    contfs = np.arange(0, 100, 10)
    clim   = contfs[0], contfs[-1]
    pyic.shade(rd_1.lon, rd_1.lat, rd_1, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)

    rossby_rad_def_c = rd_1.coarsen(lon=30, lat=30, boundary='trim').mean()
    contour = ax.contour(rossby_rad_def_c.lon, rossby_rad_def_c.lat, rossby_rad_def_c, levels=[10, 20, 30, 40 , 50 , 60, 70 ,80, 100, 150], colors='k', transform=ccrs_proj)
    ax.clabel(contour, inline=True, fontsize=12, fmt='%d')
    ax.set_title('1st baroclinic Rossby Radius of Deformation [km]', fontsize = fs)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'YlGnBu'
    contfs = np.arange(0, 15, 1)
    clim   = contfs[0], contfs[-1]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)

    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid')
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    ax.set_title('Mixed Layer Rossby Radius of Deformation [km]', fontsize = fs)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    cmap   = 'plasma'
    contfs = np.arange(0, 30, 2)
    clim  = 0, 30
    data = data/(data_r/1e3)
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid', zorder=1)
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    ax.set_title('Mixed Layer Rossby Radius / hor. Model res.', fontsize = fs)

    # add contour at 10
    ax.contour(data.lon, data.lat, data, levels=[10], colors='tab:green', transform=ccrs_proj, linewidths=0.7, alpha=0.8)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
        t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

        ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.7')
        ax.add_feature(cartopy.feature.COASTLINE, zorder=6, linewidth=1)


    if savefig == True: plt.savefig(f'{fig_path}_rossby_radius_ratio_pi_all.png', dpi=150, format='png', bbox_inches='tight')


def rossby_radius_deformation(rd_1,  lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=2,  projection=ccrs_proj, axlab_kw=None)

    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'YlGnBu'
    contfs = np.arange(0, 100, 10)
    clim   = contfs[0], contfs[-1]
    pyic.shade(rd_1.lon, rd_1.lat, rd_1, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)

    rossby_rad_def_c = rd_1.coarsen(lon=30, lat=30, boundary='trim').mean()
    contour = ax.contour(rossby_rad_def_c.lon, rossby_rad_def_c.lat, rossby_rad_def_c, levels=[10, 20, 30, 40 , 50 , 60, 70 ,80, 100, 150], colors='k', transform=ccrs_proj)
    ax.clabel(contour, inline=True, fontsize=12, fmt='%d')
    ax.set_title('1st baroclinic Rossby Radius of Deformation [km]', fontsize = fs)

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
        t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

        ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.7')
        ax.add_feature(cartopy.feature.COASTLINE, zorder=6, linewidth=1)

    if savefig == True: plt.savefig(f'{fig_path}_rossby_radius_deformation.png', dpi=150, format='png', bbox_inches='tight')


def rossby_radius_deformation_ilmar(rd_1, C, lon_reg, lat_reg, fig_path, savefig=False):


    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=2,  projection=ccrs_proj, axlab_kw=None)

    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'YlGnBu'
    contfs = np.arange(0, 15, 1)
    # contfs = np.arange(0, 100, 10)
    clim   = contfs[0], contfs[-1]
    pyic.shade(rd_1.lon, rd_1.lat, rd_1, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)

    # rossby_rad_def_c = rd_1.coarsen(lon=30, lat=30, boundary='trim').mean()
    # contour = ax.contour(rossby_rad_def_c.lon, rossby_rad_def_c.lat, rossby_rad_def_c, levels=[10, 20, 30, 40 , 50 , 60, 70 ,80, 100, 150], colors='k', transform=ccrs_proj)
    # ax.clabel(contour, inline=True, fontsize=12, fmt='%d')
    # ax.set_title('1st baroclinic Rossby Radius of Deformation [km]', fontsize = fs)

    ax.set_title('Mixed Layer Rossby Radius of Deformation [km]', fontsize = fs)

    for i in np.arange(7):
        lon_reg_s = C[0, i]
        lat_reg_s = C[1, i]
        ax.plot([lon_reg_s[0], lon_reg_s[0]], [lat_reg_s[0], lat_reg_s[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_reg_s[1], lon_reg_s[1]], [lat_reg_s[0], lat_reg_s[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_reg_s[0], lon_reg_s[1]], [lat_reg_s[0], lat_reg_s[0]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_reg_s[0], lon_reg_s[1]], [lat_reg_s[1], lat_reg_s[1]], color='r',  transform=ccrs.PlateCarree())
        t = ax.text(lon_reg_s[1]-2.5, lat_reg_s[1]-2.5, string.ascii_lowercase[i], color='red', fontsize=10)
        t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore

        ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.7')
        ax.add_feature(cartopy.feature.COASTLINE, zorder=6, linewidth=1)

    if savefig == True: plt.savefig(f'{fig_path}_rossby_radius_ml_mean_reg.png', dpi=150, format='png', bbox_inches='tight')


def two_mld(data, mld_deBoyer, lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 2, plot_cb='bottom', asp=asp, fig_size_fac=2, sharex=True, projection=ccrs_proj)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    levels = np.array([200])
    fs     = 12
    cmap   = 'YlGnBu'
    fname  = 'monthly_03'
    contfs = np.array([10, 15, 20, 40 ,60, 80, 100, 125, 150, 175, 200, 250, 300, 350, 400, 500, 600, 700, 800])#np.arange(50.,550.,50.)
    clim   = contfs[0], contfs[-1]

    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title(r'ICON-SMT March 2010 $\Delta \rho =0.03$', fontsize = fs)
    # add red box for regions
    ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    # ax.contour(data.lon, data.lat, data, levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(mld_deBoyer.lon, mld_deBoyer.lat, mld_deBoyer.isel(time=2).mld_dr003, ax=ax, cax=cax, contfs=contfs, cmap = cmap, clim = clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'de Boyer Montégut climatology March $\Delta \rho =0.03$', fontsize =fs)
    # ax.contour(mld_deBoyer.lon, mld_deBoyer.lat, mld_deBoyer.isel(time=2).mld_dr003, levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    cax.set_title('Mixed Layer Depth [m]')
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore

    if savefig == True: plt.savefig(f'{fig_path}{fname}_new.png', dpi=150, format='png', bbox_inches='tight')


def rossby_radius_res_mld(data, data_r, mldx_03, mld_deBoyer, lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(2, 2, plot_cb=True, asp=asp, fig_size_fac=2,  projection=ccrs_proj, reverse_order=True, sharex=True, sharey=True)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    fs     = 12
    cmap   = 'YlGnBu'
    contfs = np.arange(0, 12, 1)
    clim   = contfs[0], contfs[-1]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)

    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid')
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    ax.set_title('Mixed Layer Rossby Radius of Deformation [km]', fontsize = fs)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    cmap   = 'plasma'
    contfs = np.arange(0, 30, 2)
    clim  = 0, 30
    data = data/(data_r/1e3)
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    levels = 600+np.arange(20)*100
    CS = ax.contour(data_r.lon, data_r.lat, data_r, levels, colors='grey', linestyles='solid', zorder=1)
    fmt = '%.0f'+'m'
    l = ax.clabel(CS,  inline=True, inline_spacing=2, fmt=fmt, fontsize=12)
    ax.set_title('Rossby Radius / hor. Model res.', fontsize = fs)

    # add contour at 10
    ax.contour(data.lon, data.lat, data, levels=[10], colors='tab:green', transform=ccrs_proj, linewidths=0.7, alpha=0.8)


    ii+=1; ax=hca[ii]; cax=hcb[ii]
    levels = np.array([200])
    cmap   = 'YlGnBu'
    fname  = 'monthly_03'
    contfs = np.array([10, 15, 20, 40 ,60, 80, 100, 125, 150, 175, 200, 250, 300, 350, 400, 500, 600, 700, 800])#np.arange(50.,550.,50.)
    clim   = contfs[0], contfs[-1]
    data   = mldx_03  

    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title(r'ICON-SMT March 2010 $\Delta \rho =0.03$', fontsize = fs)
    cax.set_title('[m]')


    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(mld_deBoyer.lon, mld_deBoyer.lat, mld_deBoyer.isel(time=2).mld_dr003, ax=ax, cax=cax, contfs=contfs, cmap = cmap, clim = clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'de Boyer Montégut climatology March $\Delta \rho =0.03$', fontsize =fs)
    cax.set_title('[m]')
    # shift colorbar
    for cax in hcb:
        cax.yaxis.set_label_position("right")
        cax.yaxis.set_ticks_position("right")


    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
        ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r',  transform=ccrs.PlateCarree())
        ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r',  transform=ccrs.PlateCarree())
        t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
        t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

        ax.add_feature(cartopy.feature.LAND, zorder=5, facecolor='0.7')
        ax.add_feature(cartopy.feature.COASTLINE, zorder=6, linewidth=1)


    if savefig == True: plt.savefig(f'{fig_path}_rossby_radius_ratio_mld.png', dpi=150, format='png', bbox_inches='tight')
