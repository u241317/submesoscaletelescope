#%%
import sys

#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import dask as da

import pandas as pd
import netCDF4 as nc
import xarray as xr    
#from dask.diagnostics import ProgressBar
import numpy as np
from scipy.optimize import minimize


import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload
from matplotlib.ticker import FormatStrFormatter
from scipy import stats    #Used for 2D binned statistics # type: ignore
from scipy.ndimage.interpolation import rotate
sys.path.insert(0, '../multiple_fronts_evaluation_BI/')
import slope_param_vs_diagnostic_inclined_funcs as hal
import simple_regression_func as mylinreg


# %% Functions
def load_clean_dataset(time_ave, prime_filter):
    if time_ave == '2d':
        path_to_interp_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/2d_mean/'
    elif time_ave == '1W':
        path_to_interp_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/'

    if prime_filter == 'temporal':
        print('use temporal filtered primes')
        data_wb_prime_mean        = xr.open_dataarray(f'{path_to_interp_data}/data_wb_prime_mean.nc')
        data_ub_prime_mean_depthi = xr.open_dataarray(f'{path_to_interp_data}/data_ub_prime_mean_depthi.nc')
        data_vb_prime_mean_depthi = xr.open_dataarray(f'{path_to_interp_data}/data_vb_prime_mean_depthi.nc')
    elif prime_filter == 'spatial':
        print('use spatially filtered primes, and also temporally averaged')
        path_wb_space_filt        = '/work/mh0033/m300878/smt/uchida_tseries/our_domain/gridded'
        data_wb_prime_mean        = xr.open_dataarray(f'{path_wb_space_filt}/wb_prime_space_mean.nc')
        data_ub_prime_mean_depthi = xr.open_dataarray(f'{path_wb_space_filt}/ub_prime_space_mean_depthi.nc')
        data_vb_prime_mean_depthi = xr.open_dataarray(f'{path_wb_space_filt}/vb_prime_space_mean_depthi.nc')

    data_n2_mean             = xr.open_dataarray(f'{path_to_interp_data}/data_n2_mean.nc')
    data_dbdx_mean           = xr.open_dataarray(f'{path_to_interp_data}/data_dbdx_mean.nc')
    data_dbdy_mean           = xr.open_dataarray(f'{path_to_interp_data}/data_dbdy_mean.nc')
    data_M2_mean             = xr.open_dataarray(f'{path_to_interp_data}/data_M2_mean.nc')
    data_rho_mean_depthi     = xr.open_dataarray(f'{path_to_interp_data}/data_rho_mean_depthi.nc')
    # mask_land                = xr.open_dataarray(f'{path_to_interp_data}/mask_land.nc')
    mask_mld                 = xr.open_dataarray(f'{path_to_interp_data}/mask_mld.nc')
    mldx                     = xr.open_dataarray(f'{path_to_interp_data}/mldx.nc')

    lon_reg = np.array([data_M2_mean.lon.min(), data_M2_mean.lon.max()])   #-80.5, -55
    lat_reg = np.array([data_M2_mean.lat.min(), data_M2_mean.lat.max()])   #23,38

    reload(eva)
    print('load land mask at lev', 90)
    mask_land_100 = eva.load_smt_land_mask(depthc=90) #adapted due to island outliers
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
    mask_land_100 = pyic.interp_to_rectgrid_xr(mask_land_100, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    mask_land_100 = mask_land_100.drop('depthc')
    mask_used     = mask_land_100 * mask_mld

    data_M2_mean = data_M2_mean.where(~np.isinf(data_M2_mean),np.nan) #infs to nans
    m2_mask      = data_M2_mean * mask_used
    m2_mask      = m2_mask.where(~(m2_mask == 0), np.nan) # remove zeros
    m2_mask      = m2_mask.rename('m2')

    data_dbdx_mean = data_dbdx_mean.where(~np.isinf(data_dbdx_mean),np.nan) #infs to nans
    m2x_mask       = data_dbdx_mean * mask_used
    m2x_mask       = m2x_mask.where(~(m2x_mask == 0), np.nan) # remove zeros
    m2x_mask       = m2x_mask.rename('m2x')

    data_dbdy_mean = data_dbdy_mean.where(~np.isinf(data_dbdy_mean),np.nan) #infs to nans
    m2y_mask       = data_dbdy_mean * mask_used
    m2y_mask       = m2y_mask.where(~(m2y_mask == 0), np.nan) # remove zeros
    m2y_mask       = m2y_mask.rename('m2y')

    n2      = data_n2_mean.where(~np.isinf(data_n2_mean),np.nan) #infs to nans
    n2_mask = n2 * mask_used  #application of mask leads to new zero values
    n2_mask = n2_mask.where(~(n2_mask == 0), np.nan) # remove zeros
    n2_mask = n2_mask.rename('n2')

    wb_mask = data_wb_prime_mean * mask_used 
    wb_mask = wb_mask.where(~(wb_mask == 0), np.nan) # remove zeros
    wb_mask = wb_mask.rename('wb')

    vb_mask = data_vb_prime_mean_depthi * mask_used
    vb_mask = vb_mask.where(~(vb_mask == 0), np.nan) # remove zeros
    vb_mask = vb_mask.rename('vb')

    ub_mask = data_ub_prime_mean_depthi * mask_used
    ub_mask = ub_mask.where(~(ub_mask == 0), np.nan) # remove zeros
    ub_mask = ub_mask.rename('ub')

    rho_mask = data_rho_mean_depthi * mask_used
    rho_mask = rho_mask.where(~(m2_mask == 0), np.nan) # remove zeros
    rho_mask = rho_mask.rename('rho')
    return m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx

def concat_dataset(m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx):
    ds = rho_mask.to_dataset()
    ds = ds.assign(ub = ub_mask)
    ds = ds.assign(vb = vb_mask)
    ds = ds.assign(wb = wb_mask)
    ds = ds.assign(n2 = n2_mask)
    # A = A.assign(m2 = m2_mask)
    print('Set alongfront m2 to zero and crossfront value as absolute value of m2x and m2y')
    ds = ds.assign(m2_along_grad = 0)
    ds = ds.assign(m2_cross_grad = np.sqrt(np.power(m2x_mask,2) + np.power(m2y_mask,2)))
    # A = A.assign(m2_along_grad = m2x_mask)
    # A = A.assign(m2_cross_grad = m2x_mask + m2y_mask)
    ds = ds.assign(mld           = mldx)
    if False:
        print('vb calculation with sign')
        print('not ready for parameterizations')
        ds = ds.assign(Vb_cross_grad = ( m2x_mask * ub_mask + m2y_mask * vb_mask) / (np.sqrt(np.power(m2x_mask,2) + np.power(m2y_mask,2))) ) # projection of fluctuation on front gradient
    else:
        Vb_cross_grad = ( m2x_mask * ub_mask + m2y_mask * vb_mask) / (np.sqrt(np.power(m2x_mask,2) + np.power(m2y_mask,2)))  # projection of fluctuation on front gradient
        # print('negative absolute value of vb')
        # ds = ds.assign(Vb_cross_grad =  - np.abs( Vb_cross_grad ))# projection of fluctuation on front gradient
        print('negative values of VB cross gradient')
        ds = ds.assign(Vb_cross_grad =  Vb_cross_grad.where(Vb_cross_grad < 0, np.nan))

    ds = ds.assign(fcoriolis     = eva.calc_coriolis_parameter(ds.lat))
    ds = ds.assign(alpha         = eva.calc_alpha( ds.m2_cross_grad, ds.fcoriolis))
    ds = ds.assign(m2x           = m2x_mask)
    ds = ds.assign(m2y           = m2y_mask)
    ds_depth = ds
    print('apply mldx average')
    ds       = ds.mean(dim='depthi', skipna=True)
    # print('calc ri always after all averaging steps')
    ds = ds.assign(ri_diag       = eva.calc_richardsonnumber(ds.lat, ds.n2, ds.m2_cross_grad))
    ds = ds.assign(grad_b        = eva.calc_abs_grad_b(ds.m2_along_grad, ds.m2_cross_grad, ds.n2))
    ds = ds.assign(psi_diag_held = eva.calc_streamfunc_held_schneider(ds.wb, ds.m2_along_grad, ds.m2_cross_grad))
    ds = ds.assign(psi_diag_full = eva.calc_streamfunc_full(ds.wb, ds.m2_cross_grad, ds.Vb_cross_grad, ds.n2, ds.grad_b))
    ds = ds.assign(K             = eva.calc_diapycnal_diffusivity(ds.Vb_cross_grad, ds.m2_cross_grad, ds.wb, ds.n2, ds.grad_b))

    print('filter wb larger 0')
    ds      = ds.where(ds.wb>0)
    print('filter ri_diag larger 0')
    ds      = ds.where(ds.ri_diag>0)

    return ds, ds_depth





def filter_select_domain(A, m2y_mask, domain, savefig, path_save_img, var, filter, m2_filter):
    # m2_filter = 0
    # print(f'filter abs(m2) larger {m2_filter}')
    # if   filter == 'm2': mask_m2   = np.abs(A.m2_cross_grad) > m2_filter
    # elif filter == 'ri': mask_m2   = np.abs(A.ri_diag) < m2_filter
    # else: raise Warning('no filter selcted') 
    # A         = A * mask_m2

    print('filter wb larger 0')
    A         = A.where(A.wb > 0)

    if domain == 'single':
        print('single', domain)
        points = eva.get_points_of_inclined_fronts()
        ii=0
        P = points[2*ii:2*ii+2,:]
        y, x, m, b = hal.calc_line(P)
        data = hal.select_data(m2y_mask, x, y, delta = 0.2)
        lon_front = data.lon.min(), data.lon.max()
        lat_front = data.lat.min(), data.lat.max()

        lon_reg = lon_front
        lat_reg = lat_front

        A = A.sel(lon=slice(lon_reg[0], lon_reg[1])).sel(lat=slice(lat_reg[0], lat_reg[1]))
        # A.m2_cross_grad.plot()
    elif domain == 'multiple':
        lon_reg = np.array([-70, -54])
        lat_reg = np.array([23.5, 36])
        A = A.sel(lon=slice(lon_reg[0], lon_reg[1])).sel(lat=slice(lat_reg[0], lat_reg[1]))
    elif domain == 'na':
        lon_reg = A.lon.min(), A.lon.max()
        lat_reg = A.lat.min(), A.lat.max()
        print('domain is NA')
    
    if False: plot_m2_thresholds(A, savefig, path_save_img, domain, name=m2_filter)

    if var == 'wb':
        lhs_diag   , rhs_ri       = calc_lhs_rhs_wb_ri(A, A.wb)
        lhs_stone  , rhs_ri       = calc_lhs_rhs_wb_ri(A, A.wb_stone_param)
        lhs_fox    , rhs_ri       = calc_lhs_rhs_wb_ri(A, A.wb_fox_param)

        logx_diag  , logy_diag    = hal.calc_log10_2(rhs_ri.data,  lhs_diag.data )
        logx_fox   , logy_fox     = hal.calc_log10_2(rhs_ri.data,  lhs_fox.data  ) # .where(A.ri_diag < lim).data
        logx_stone , logy_stone   = hal.calc_log10_2(rhs_ri.data,  lhs_stone.data)
    elif var == 'vb':
        lhs_diag   , rhs_ri       = calc_lhs_rhs_vb_ri(A, A.Vb_cross_grad)
        lhs_stone  , rhs_ri       = calc_lhs_rhs_vb_ri(A, A.vb_stone_param)
        lhs_fox    , rhs_ri       = calc_lhs_rhs_vb_ri(A, A.Vb_cross_grad_fox_param)

        logx_diag  , logy_diag    = hal.calc_log10_2(rhs_ri.data,  lhs_diag.data )
        logx_fox   , logy_fox     = hal.calc_log10_2(rhs_ri.data,  lhs_fox.data  )
        logx_stone , logy_stone   = hal.calc_log10_2(rhs_ri.data,  lhs_stone.data)
    elif var == 'psi':
        lhs_diag   , rhs_ri       = calc_lhs_rhs_psi_ri(A, np.abs(A.psi_diag_full))
        lhs_fox    , rhs_ri       = calc_lhs_rhs_psi_ri(A, np.abs(A.psi_fox_full_param))
        lhs_stone  , rhs_ri       = calc_lhs_rhs_psi_ri(A, np.abs(A.psi_stone_full_param))

        logx_diag  , logy_diag    = hal.calc_log10_2(rhs_ri.data,  lhs_diag.data )
        logx_fox   , logy_fox     = hal.calc_log10_2(rhs_ri.data,  lhs_fox.data  ) # .where(A.ri_diag < lim).data
        logx_stone , logy_stone   = hal.calc_log10_2(rhs_ri.data,  lhs_stone.data)
    # elif var == 'psi':
    #     lhs_diag   , rhs_ri       = calc_lhs_rhs_psi_ri(A, np.abs(A.psi_y))
    #     lhs_fox    , rhs_ri       = calc_lhs_rhs_psi_ri(A, np.abs(A.psi_y_fox))
    #     lhs_stone  , rhs_ri       = calc_lhs_rhs_psi_ri(A, np.abs(A.psi_stone_full_param))

    #     logx_diag  , logy_diag    = hal.calc_log10_2(rhs_ri.data,  lhs_diag.data )
    #     logx_fox   , logy_fox     = hal.calc_log10_2(rhs_ri.data,  lhs_fox.data  ) # .where(A.ri_diag < lim).data
    #     logx_stone , logy_stone   = hal.calc_log10_2(rhs_ri.data,  lhs_stone.data)
    else:
        raise Warning('no variable selected')

    A = A.assign(logx_diag  = xr.DataArray(name='logx_diag' , data=logx_diag ,   dims='dim_diag' ))
    A = A.assign(logy_diag  = xr.DataArray(name='logy_diag' , data=logy_diag ,   dims='dim_diag' ))
    A = A.assign(logx_fox   = xr.DataArray(name='logx_fox ' , data=logx_fox  ,   dims='dim_fox'  ))
    A = A.assign(logy_fox   = xr.DataArray(name='logy_fox ' , data=logy_fox  ,   dims='dim_fox'  ))
    A = A.assign(logx_stone = xr.DataArray(name='logx_stone', data=logx_stone,   dims='dim_stone'))
    A = A.assign(logy_stone = xr.DataArray(name='logy_stone', data=logy_stone,   dims='dim_stone'))

    return A

def select_domain(ds, domain, m2y_mask=None):
    if domain == 'single':
        print('single', domain)
        points = eva.get_points_of_inclined_fronts()
        ii=0
        P = points[2*ii:2*ii+2,:]
        y, x, m, b = hal.calc_line(P)
        data = hal.select_data(m2y_mask, x, y, delta = 0.2)
        lon_front = data.lon.min(), data.lon.max()
        lat_front = data.lat.min(), data.lat.max()

        lon_reg = lon_front
        lat_reg = lat_front

        ds = ds.sel(lon=slice(lon_reg[0], lon_reg[1])).sel(lat=slice(lat_reg[0], lat_reg[1]))
        # A.m2_cross_grad.plot()
    elif domain == 'multiple':
        lon_reg = np.array([-70, -54])
        lat_reg = np.array([23.5, 36])
        ds = ds.sel(lon=slice(lon_reg[0], lon_reg[1])).sel(lat=slice(lat_reg[0], lat_reg[1]))
    elif domain == 'na':
        lon_reg = ds.lon.min(), ds.lon.max()
        lat_reg = ds.lat.min(), ds.lat.max()
        print('domain is NA')
    return ds

def calc_tuning_coefficients(ds, ri_critical, opt_var, ave_type):
    # print('critical ri', ri_critical)
    # print('min lon', ds.lon.min().values, 'max lon', ds.lon.max().values)
    # print('min lat', ds.lat.min().values, 'max lat', ds.lat.max().values)
    calc_tuning_coefficients_no_filter(ds.copy(), ave_type='median')
    mask_ri = ds.ri_diag.where(ds.ri_diag < ri_critical, np.nan)
    mask_ri = mask_ri.where(np.isnan(mask_ri), 1)
    # mask_ri.plot()
    mask_ri = mask_ri.rename('None')
    mask_ri = mask_ri.rename(None)

    for var in ds.data_vars:
        ds[var] = ds[var] * mask_ri #* mask_vb

    cf_wb = ds.wb * 1 / (ds.alpha**2 * ds.mld**2 * ds.fcoriolis**3)
    cs_wb = ds.wb * np.sqrt(1 + ds.ri_diag) * 1 / (ds.alpha**2 * ds.mld**2 * ds.fcoriolis**3)
    cf_vb = - ds.Vb_cross_grad * 1 / (2 * ds.ri_diag * ds.alpha**3 * ds.mld**2 * ds.fcoriolis**3)
    cs_vb = - 5/8 * ds.Vb_cross_grad / (np.sqrt(1 + ds.ri_diag) * ds.alpha**3 * ds.mld**2 * ds.fcoriolis**3)

    if ave_type == 'mean':
        cf_wb_sel = cf_wb.mean(skipna=True).values
        cs_wb_sel = cs_wb.mean(skipna=True).values
        cf_vb_sel = cf_vb.mean(skipna=True).values
        cs_vb_sel = cs_vb.mean(skipna=True).values
    elif ave_type == 'median':
        cf_wb_sel = cf_wb.median(skipna=True).values
        cs_wb_sel = cs_wb.median(skipna=True).values
        cf_vb_sel = cf_vb.median(skipna=True).values
        cs_vb_sel = cs_vb.median(skipna=True).values
    else: raise Warning('no average type selected')

    # print(f'{ave_type} values: \n \n cf_wb', cf_wb_sel, 'cs_wb', cs_wb_sel,  ' \n cf_vb', cf_vb_sel, 'cs_vb', cs_vb_sel, '\n')
    # print('mean values: cf_wb', np.round(cf_wb.mean().values,4), 'cs_wb', np.round(cs_wb.mean().values,4),  ' \n cf_vb', np.round(cf_vb.mean().values,4), 'cs_vb', np.round(cs_vb.mean().values,4))
    #above line with 4 digits
    print(f'{ave_type} values:\n cf_wb ', np.round(cf_wb_sel,4), 'cs_wb', np.round(cs_wb_sel,4),  ' \n cf_vb', np.round(cf_vb_sel,4), 'cs_vb', np.round(cs_vb_sel,4), '\n')

    if opt_var == 'wb':
        cf = cf_wb_sel
        cs = cs_wb_sel
    elif opt_var == 'vb':
        cf = cf_vb_sel
        cs = cs_vb_sel#.interpolate_na(dim='lat', method='linear').interpolate_na(dim='lon', method='linear').values
    elif opt_var == 'both': #mean of both
        cf = (cf_wb_sel + cf_vb_sel) / 2
        cs = (cs_wb_sel + cs_vb_sel) / 2

    # % Errors wb
    wb_fox_param   = eva.calc_wb_fox_param(ds.lat, ds.mld,  ds.m2_cross_grad, np.nan, cf, structure=False )
    wb_stone_param = eva.calc_wb_stone_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, cs, np.nan, structure=False )
    error_wb_fox   = np.nanmean(np.sqrt(np.power(wb_fox_param - ds.wb, 2)))
    error_wb_stone = np.nanmean(np.sqrt(np.power(wb_stone_param - ds.wb, 2)))
    print(f'error wb fox {error_wb_fox:.2e}', f'\n error wb stone {error_wb_stone:.2e}')

    # % Errors vb
    Vb_cross_grad_fox_param = eva.calc_vb_fox_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, np.nan, cf, structure=False ) #no sign dependence of vb cross gradient and no sign dependence of crossfront gradient
    vb_stone_param          = eva.calc_vb_stone_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, cs)
    error_vb_fox            = np.nanmean(np.sqrt(np.power(Vb_cross_grad_fox_param - ds.Vb_cross_grad, 2)))
    error_vb_stone          = np.nanmean(np.sqrt(np.power(vb_stone_param - ds.Vb_cross_grad, 2)))
    print(f'error vb fox {error_vb_fox:.2e}', f'\n error vb stone {error_vb_stone:.2e}')
    print('cf', cf, 'cs', cs)
    return cf, cs

def calc_tuning_coefficients_no_filter(ds, ave_type):
    cf_wb = ds.wb * 1 / (ds.alpha**2 * ds.mld**2 * ds.fcoriolis**3)
    cs_wb = ds.wb * np.sqrt(1 + ds.ri_diag) * 1 / (ds.alpha**2 * ds.mld**2 * ds.fcoriolis**3)
    cf_vb = - ds.Vb_cross_grad * 1 / (2 * ds.ri_diag * ds.alpha**3 * ds.mld**2 * ds.fcoriolis**3)
    cs_vb = - 5/8 * ds.Vb_cross_grad / (np.sqrt(1 + ds.ri_diag) * ds.alpha**3 * ds.mld**2 * ds.fcoriolis**3)

    if ave_type == 'mean':
        cf_wb_sel = cf_wb.mean(skipna=True).values
        cs_wb_sel = cs_wb.mean(skipna=True).values
        cf_vb_sel = cf_vb.mean(skipna=True).values
        cs_vb_sel = cs_vb.mean(skipna=True).values
    elif ave_type == 'median':
        cf_wb_sel = cf_wb.median(skipna=True).values
        cs_wb_sel = cs_wb.median(skipna=True).values
        cf_vb_sel = cf_vb.median(skipna=True).values
        cs_vb_sel = cs_vb.median(skipna=True).values
    else: raise Warning('no average type selected')


    print(f'{ave_type} entire domain values:\n cf_wb ', np.round(cf_wb_sel,4), 'cs_wb', np.round(cs_wb_sel,4),  ' \n cf_vb', np.round(cf_vb_sel,4), 'cs_vb', np.round(cs_vb_sel,4), '\n')



def calc_tuning_coefficients_min_error(ds, ri_critical, opt_var):
    # print('critical ri', ri_critical)
    # print('min lon', ds.lon.min().values, 'max lon', ds.lon.max().values)
    # print('min lat', ds.lat.min().values, 'max lat', ds.lat.max().values)
    mask_ri = ds.ri_diag.where(ds.ri_diag < ri_critical, np.nan)
    mask_ri = mask_ri.where(np.isnan(mask_ri), 1)
    # mask_ri.plot()
    mask_ri = mask_ri.rename('None')
    mask_ri = mask_ri.rename(None)

    for var in ds.data_vars:
        ds[var] = ds[var] * mask_ri #* mask_vb

    initial_guess = [1.0]

    if opt_var == 'wb':
        def objective_per(cf, ds):
            wb_fox_param = eva.calc_wb_fox_param(ds.lat, ds.mld, ds.m2_cross_grad, np.nan, cf, structure=False)
            error_wb_fox = np.nanmean(np.sqrt(np.power(wb_fox_param - ds.wb, 2)))
            return error_wb_fox

        result = minimize(objective_per, initial_guess, args=(ds,), method='Nelder-Mead')
        cf = result.x

        def objective_als(cs, ds):
            wb_stone_param = eva.calc_wb_stone_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, cs, np.nan, structure=False )
            error_wb_stone = np.nanmean(np.sqrt(np.power(wb_stone_param - ds.wb, 2)))
            return error_wb_stone

        result = minimize(objective_als, initial_guess, args=(ds,), method='Nelder-Mead')
        cs = result.x
    elif opt_var == 'vb':
        def objective_per(cf, ds):
            Vb_cross_grad_fox_param = eva.calc_vb_fox_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, np.nan, cf, structure=False )
            error_vb_fox = np.nanmean(np.sqrt(np.power(Vb_cross_grad_fox_param - ds.Vb_cross_grad, 2)))
            return error_vb_fox
        result = minimize(objective_per, initial_guess, args=(ds,), method='Nelder-Mead')
        cf = result.x

        def objective_als(cs, ds):
            vb_stone_param = eva.calc_vb_stone_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, cs)
            error_vb_stone = np.nanmean(np.sqrt(np.power(vb_stone_param - ds.Vb_cross_grad, 2)))
            return error_vb_stone
        result = minimize(objective_als, initial_guess, args=(ds,), method='Nelder-Mead')
        cs = result.x

    print('cf', cf, 'cs', cs)

    return cf, cs



def calc_tuning_coefficients_min_error2(ds, ri_critical, opt_var, ave_type):
    mask_ri = ds.ri_diag.where(ds.ri_diag < ri_critical, np.nan)
    mask_ri = mask_ri.where(np.isnan(mask_ri), 1)
    # mask_ri.plot()
    mask_ri = mask_ri.rename('None')
    mask_ri = mask_ri.rename(None)

    for var in ds.data_vars:
        ds[var] = ds[var] * mask_ri #* mask_vb

    cf = np.linspace(0, 0.1, 100)
    cf = xr.DataArray(cf, dims='cf')
    cs = np.linspace(0, 0.1, 100)
    cs = xr.DataArray(cs, dims='cs')
    
    if opt_var == 'wb':
        wb_fox_param   = eva.calc_wb_fox_param(ds.lat, ds.mld,  ds.m2_cross_grad, np.nan, cf, structure=False )
        error = np.sqrt(np.power(wb_fox_param - ds.wb, 2))
        if ave_type == 'mean':
            error_sum = error.sum(dim='lat', skipna=True).sum(dim='lon', skipna=True)
        elif ave_type == 'median':
            error_sum = error.median(dim='lat', skipna=True).median(dim='lon', skipna=True)
        error_min_cf = error_sum.argmin(dim='cf', skipna=True)
        cf = cf[error_min_cf]

        wb_stone_param = eva.calc_wb_stone_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, cs, np.nan, structure=False )
        error          = np.sqrt(np.power(wb_stone_param - ds.wb, 2))
        if ave_type == 'mean':
            error_sum      = error.sum(dim='lat', skipna=True).sum(dim='lon', skipna=True)
        elif ave_type == 'median':
            error_sum      = error.median(dim='lat', skipna=True).median(dim='lon', skipna=True)
        error_min_cs   = error_sum.argmin(dim='cs', skipna=True)
        cs             = cs[error_min_cs]

    elif opt_var == 'vb':
        Vb_cross_grad_fox_param = eva.calc_vb_fox_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, np.nan, cf, structure=False )
        error        = np.sqrt(np.power(Vb_cross_grad_fox_param - ds.Vb_cross_grad, 2))
        error_sum    = error.sum(dim='lat', skipna=True).sum(dim='lon', skipna=True)
        error_min_cf = error_sum.argmin(dim='cf', skipna=True)
        cf           = cf[error_min_cf]

        vb_stone_param = eva.calc_vb_stone_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, cs)
        error          = np.sqrt(np.power(vb_stone_param - ds.Vb_cross_grad, 2))
        error_sum      = error.sum(dim='lat', skipna=True).sum(dim='lon', skipna=True)
        error_min_cs   = error_sum.argmin(dim='cs', skipna=True)
        cs             = cs[error_min_cs]
    
    print('cf', cf, 'cs', cs)

    return cf, cs



def apply_parameterization(ds, cf, cs):
    if False:
        # # vertical profiles are calculated 
        mask_nan             = A_depth.m2_cross_grad
        mask_nan             = mask_nan.where(np.isnan(mask_nan),1.)
        depthI               = A_depth.m2_cross_grad.depthi
        wb_fox_param         = mask_nan * eva.calc_wb_fox_param(A_depth.lat, A_depth.mld,  ds.m2_cross_grad, -depthI, cf, structure=True )
        vb_fox_param         = mask_nan * eva.calc_vb_fox_param(A_depth.lat, A_depth.mld,  ds.m2_cross_grad, ds.ri_diag, -depthI, cf, structure=True )
        wb_stone_param       = mask_nan * eva.calc_wb_stone_param(A_depth.lat, A_depth.mld,  ds.m2_cross_grad, ds.ri_diag, cs, -depthI, structure = True)
        wb_fox_param         = wb_fox_param.mean(dim='depthi', skipna=True)
        vb_fox_param         = vb_fox_param.mean(dim='depthi', skipna=True)
        wb_stone_param       = wb_stone_param.mean(dim='depthi', skipna=True)
    else:
        print('no vertical structure')
        wb_fox_param            = eva.calc_wb_fox_param(ds.lat, ds.mld,  ds.m2_cross_grad, np.nan, cf, structure=False )
        if True:
            print('vb fox without sign')
            Vb_cross_grad_fox_param =  eva.calc_vb_fox_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, np.nan, cf, structure=False ) #no sign dependence of vb cross gradient and no sign dependence of crossfront gradient
        else: 
            ub_fox_param            = eva.calc_vb_fox_param(ds.lat, ds.mld,  ds.m2x, ds.ri_diag, np.nan, cf, structure=False )
            vb_fox_param            = eva.calc_vb_fox_param(ds.lat, ds.mld,  ds.m2y, ds.ri_diag, np.nan, cf, structure=False )
            Vb_cross_grad_fox_param =  ( ds.m2x * ub_fox_param + ds.m2y * vb_fox_param) / (np.sqrt(np.power(ds.m2x,2) + np.power(ds.m2y,2)))  # projection of fluctuation on front gradient

        wb_stone_param       = eva.calc_wb_stone_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, cs, np.nan, structure=False )

    vb_stone_param       = eva.calc_vb_stone_param(ds.lat, ds.mld,  ds.m2_cross_grad, ds.ri_diag, cs)
    psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param,  ds.m2_cross_grad, vb_stone_param, ds.n2, ds.grad_b)
    psi_stone_held_param = eva.calc_streamfunc_held_schneider(wb_stone_param, ds.m2_along_grad,   ds.m2_cross_grad)
    K_stone              = eva.calc_diapycnal_diffusivity(vb_stone_param, ds.m2_cross_grad, wb_stone_param, ds.n2, ds.grad_b)

    # psi_x_fox, psi_y_fox = eva.calc_streamfunc_full_3d_fox(ds.lat, ds.mld,  ds.m2x, ds.m2y, np.nan, cf, structure=False)
    if False: ub_fox_param_3d, vb_fox_param_3d,  wb_fox_param_3d = eva.calc_cross_product(psi_x_fox, psi_y_fox, 0, ds.m2x, ds.m2y, ds.n2)
    # psi_x_fox_ind, psi_y_fox_ind, psi_z_fox_ind =  eva.calc_streamfunc_full_3d(ub_fox_param_3d, vb_fox_param_3d,  wb_fox_param_3d, A.m2x, A.m2y, A.n2) # its the same!!! very good!

    psi_fox_full_param   = eva.calc_streamfunc_full(wb_fox_param,  ds.m2_cross_grad, Vb_cross_grad_fox_param, ds.n2, ds.grad_b)
    psi_fox_held_param   = eva.calc_streamfunc_held_schneider(wb_fox_param, ds.m2_along_grad,  ds.m2_cross_grad)
    print('Held Schneider does not have sign of overturning')
    K_fox                = eva.calc_diapycnal_diffusivity(Vb_cross_grad_fox_param, ds.m2_cross_grad, wb_fox_param, ds.n2, ds.grad_b)

    ds = ds.assign(wb_fox_param         = wb_fox_param         )
    # A = A.assign(ub_fox_param         = ub_fox_param         )
    # A = A.assign(vb_fox_param         = vb_fox_param         )
    ds = ds.assign(Vb_cross_grad_fox_param = Vb_cross_grad_fox_param )
    ds = ds.assign(psi_fox_full_param   = psi_fox_full_param   )
    ds = ds.assign(psi_fox_held_param   = psi_fox_held_param   )

    ds = ds.assign(wb_stone_param       = wb_stone_param       )
    ds = ds.assign(vb_stone_param       = vb_stone_param       )
    ds = ds.assign(psi_stone_full_param = psi_stone_full_param )
    ds = ds.assign(psi_stone_held_param = psi_stone_held_param )
    ds = ds.assign(K_fox                = K_fox                )  
    ds = ds.assign(K_stone              = K_stone              )  
    # ds = ds.assign(psi_x_fox          = psi_x_fox      )
    # ds = ds.assign(psi_y_fox          = psi_y_fox      )
    ds = ds.assign(cf                   = cf                   )
    ds = ds.assign(cs                   = cs                   )
    return ds


def apply_log_mask(A, var, varx):
    # print('filter wb larger 0')
    # A         = A.where(A.wb > 0)
    
    if var == 'wb':
        if varx == 'ri':
            lhs_diag   , rhs       = calc_lhs_rhs_wb_ri(A, A.wb)
            lhs_stone  , rhs       = calc_lhs_rhs_wb_ri(A, A.wb_stone_param)
            lhs_fox    , rhs       = calc_lhs_rhs_wb_ri(A, A.wb_fox_param)
        elif varx == 'alpha_ALS':
            lhs_diag   , rhs       = calc_lhs_rhs_wb_alpha_ALS(A, A.wb)
            lhs_stone  , rhs       = calc_lhs_rhs_wb_alpha_ALS(A, A.wb_stone_param)
            lhs_fox    , rhs       = calc_lhs_rhs_wb_alpha_ALS(A, A.wb_fox_param)
        elif varx == 'alpha_PER':
            lhs_diag   , rhs       = calc_lhs_rhs_wb_alpha_PER(A, A.wb)
            lhs_stone  , rhs       = calc_lhs_rhs_wb_alpha_PER(A, A.wb_stone_param)
            lhs_fox    , rhs       = calc_lhs_rhs_wb_alpha_PER(A, A.wb_fox_param)

        logx_diag  , logy_diag    = hal.calc_log10_2(rhs.data,  lhs_diag.data )
        logx_fox   , logy_fox     = hal.calc_log10_2(rhs.data,  lhs_fox.data  ) 
        logx_stone , logy_stone   = hal.calc_log10_2(rhs.data,  lhs_stone.data)
    elif var == 'vb':
        if varx == 'ri':
            lhs_diag   , rhs       = calc_lhs_rhs_vb_ri(A, A.Vb_cross_grad)
            lhs_stone  , rhs       = calc_lhs_rhs_vb_ri(A, A.vb_stone_param)
            lhs_fox    , rhs       = calc_lhs_rhs_vb_ri(A, A.Vb_cross_grad_fox_param)
        elif varx == 'alpha_ALS':
            lhs_diag   , rhs       = calc_lhs_rhs_vb_alpha_ALS(A, A.Vb_cross_grad)
            lhs_stone  , rhs       = calc_lhs_rhs_vb_alpha_ALS(A, A.vb_stone_param)
            lhs_fox    , rhs       = calc_lhs_rhs_vb_alpha_ALS(A, A.Vb_cross_grad_fox_param)
        elif varx == 'alpha_PER':
            lhs_diag   , rhs       = calc_lhs_rhs_vb_alpha_PER(A, A.Vb_cross_grad)
            lhs_stone  , rhs       = calc_lhs_rhs_vb_alpha_PER(A, A.vb_stone_param)
            lhs_fox    , rhs       = calc_lhs_rhs_vb_alpha_PER(A, A.Vb_cross_grad_fox_param)

        logx_diag  , logy_diag    = hal.calc_log10_2(rhs.data,  lhs_diag.data )
        logx_fox   , logy_fox     = hal.calc_log10_2(rhs.data,  lhs_fox.data  )
        logx_stone , logy_stone   = hal.calc_log10_2(rhs.data,  lhs_stone.data)
    elif var == 'psi':
        lhs_diag   , rhs       = calc_lhs_rhs_psi_ri(A, np.abs(A.psi_diag_full))
        lhs_fox    , rhs       = calc_lhs_rhs_psi_ri(A, np.abs(A.psi_fox_full_param))
        lhs_stone  , rhs       = calc_lhs_rhs_psi_ri(A, np.abs(A.psi_stone_full_param))

        logx_diag  , logy_diag    = hal.calc_log10_2(rhs.data,  lhs_diag.data )
        logx_fox   , logy_fox     = hal.calc_log10_2(rhs.data,  lhs_fox.data  ) 
        logx_stone , logy_stone   = hal.calc_log10_2(rhs.data,  lhs_stone.data)
    else:
        raise Warning('no variable selected')

    A = A.assign(logx_diag  = xr.DataArray(name='logx_diag' , data=logx_diag ,   dims='dim_diag' ))
    A = A.assign(logy_diag  = xr.DataArray(name='logy_diag' , data=logy_diag ,   dims='dim_diag' ))
    A = A.assign(logx_fox   = xr.DataArray(name='logx_fox ' , data=logx_fox  ,   dims='dim_fox'  ))
    A = A.assign(logy_fox   = xr.DataArray(name='logy_fox ' , data=logy_fox  ,   dims='dim_fox'  ))
    A = A.assign(logx_stone = xr.DataArray(name='logx_stone', data=logx_stone,   dims='dim_stone'))
    A = A.assign(logy_stone = xr.DataArray(name='logy_stone', data=logy_stone,   dims='dim_stone'))

    return A


def calc_lhs_rhs_wb_ri(data, var):
    lhs = var / np.power(data.mld,2) / np.power(data.fcoriolis,3) / np.power(data.alpha,2)
    rhs = data.ri_diag
    return lhs, rhs

def calc_lhs_rhs_vb_ri(data, var):
    lhs = np.abs(var) / np.power(data.mld,2) / np.power(data.fcoriolis,3) / np.power(data.alpha,3) 
    rhs = data.ri_diag
    return lhs, rhs

def calc_lhs_rhs_wb_alpha(data, var):
    lhs = np.sqrt(var / np.power(data.mld,2) / np.power(data.fcoriolis,3) / data.ri_diag)
    rhs = data.alpha
    return lhs, rhs

def calc_lhs_rhs_vb_alpha(data, var):
    lhs = np.cbrt(np.abs(var) / np.power(data.mld,2) / np.power(data.fcoriolis,3) / data.ri_diag )
    rhs = data.alpha
    return lhs, rhs

def calc_lhs_rhs_wb_alpha_ALS(data, var):
    lhs = var / np.power(data.mld,2) / np.power(data.fcoriolis,3) * np.sqrt(1+data.ri_diag)
    rhs = data.alpha
    return lhs, rhs

def calc_lhs_rhs_vb_alpha_ALS(data, var):
    lhs = 5/8 * np.abs(var) / np.power(data.mld,2) / np.power(data.fcoriolis,3) / np.sqrt(1+ data.ri_diag) 
    rhs = data.alpha
    return lhs, rhs

def calc_lhs_rhs_wb_alpha_no_ri(data, var):
    lhs = np.sqrt(var / np.power(data.mld,2) / np.power(data.fcoriolis,3))
    # lhs = var / np.power(data.mld,2) / np.power(data.fcoriolis,3)
    rhs = data.alpha
    return lhs, rhs

def calc_lhs_rhs_vb_alpha_no_ri(data, var):
    lhs = np.cbrt(np.abs(var) / np.power(data.mld,2) / np.power(data.fcoriolis,3)  )
    # lhs = np.abs(var) / np.power(data.mld,2) / np.power(data.fcoriolis,3)
    rhs = data.alpha
    return lhs, rhs

def calc_lhs_rhs_wb_alpha_PER(data, var):
    # lhs = np.sqrt(var / np.power(data.mld,2) / np.power(data.fcoriolis,3))
    lhs = var / np.power(data.mld,2) / np.power(data.fcoriolis,3)
    rhs = data.alpha
    return lhs, rhs

def calc_lhs_rhs_vb_alpha_PER(data, var):
    # lhs = np.cbrt(np.abs(var) / np.power(data.mld,2) / np.power(data.fcoriolis,3)  )
    lhs = np.abs(var) / np.power(data.mld,2) / np.power(data.fcoriolis,3) / data.ri_diag / 2
    rhs = data.alpha
    return lhs, rhs


def calc_lhs_rhs_psi_ri(data, psi):
    lim = 1e8
    # print('cut Ri larger then ', lim)
    lhs_psi         = psi / np.power(data.mld,2) / np.power(data.fcoriolis,1) / np.power(data.alpha,1)
    rhs_ri          = data.ri_diag.where(data.ri_diag < lim)
    return lhs_psi, rhs_ri

def plot_m2_thresholds(A, savefig, path_save_img, domain, name):
    fig = plt.figure(figsize=([10,8]))
    levels= np.array([0,1,2,3,5]) * 1e-8
    cf = plt.contourf(A.m2_cross_grad.lon, A.m2_cross_grad.lat, A.m2_cross_grad, cmap='BuPu', projection=ccrs_proj,  levels=levels, extend='max')
    cb = fig.colorbar(cf)
    plt.xlabel('lon')
    plt.ylabel('lat')
    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_front_m2_lim_var_filter_{name}_contf.png', dpi=150, format='png', bbox_inches='tight')

def plot_ri_diag_fronts(ds_m2_0, savefig, path_save_img):
    clim   = 0, 50
    cmap = 'Blues_r'
    # cmap = 'BuPu_r'
    # ---
    hca, hcb = pyic.arrange_axes(1,1, plot_cb='right', asp=0.8,  fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, axlab_kw=None,
                                 )
    ii=-1
    # ---
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm1 = pyic.shade(ds_m2_0.lon, ds_m2_0.lat, ds_m2_0.ri_diag, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    hal.add_front_lines_box(ax)
    pyic.plot_settings(ax) # type: ignore
    ax.set_title('Ri_diag')
    fname = 'location_fronts2'
    if savefig == True: plt.savefig(f'{path_save_img}{fname}.png', dpi=250, format='png', bbox_inches='tight')

# %%
def plot_slopes_scatter(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path_save_img, domain, savefig):
    # %% wb diagnostic in loglog
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    ii = -1
    fs = 10

    alpha =0.08

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = ds_m2_0.logx_diag
    logy = ds_m2_0.logy_diag
    ax.scatter(logx, logy , color='tab:orange', marker='^', s=0.5, alpha =alpha, rasterized=True)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'orange', label=f'M2 > 0e-0: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_1e8.logx_diag
    logy = ds_m2_1e8.logy_diag
    ax.scatter(logx, logy , color='tab:blue', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'tab:blue', label=f'M2 > 1e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_3e8.logx_diag
    logy = ds_m2_3e8.logy_diag
    ax.scatter(logx, logy , color='tab:green', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'green', label=f'M2 > 3e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_5e8.logx_diag
    logy = ds_m2_5e8.logy_diag
    ax.scatter(logx, logy , color='tab:red', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'red', label=f'M2 > 5e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    pyic.plot_settings(ax)   # type: ignore
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.grid()
    ax.legend(loc='lower left', bbox_to_anchor=(-0.3, 1.02, 1, 0.2))
    xlabel = r'$log_{10} \overline{Ri}$'
    ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)

    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_front_m2_lim_diag.png', dpi=150, format='png', bbox_inches='tight')


    # %% wb diagnostic, fox, stone in loglog
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    ii = -1
    fs = 10

    alpha =0.08

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = ds_m2_0.logx_fox     
    logy = ds_m2_0.logy_fox
    ax.scatter(logx, logy , color='tab:orange', marker='^', s=0.5, alpha =alpha, rasterized=True)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'orange', label=f'M2 > 0e-0: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_1e8.logx_fox
    logy = ds_m2_1e8.logy_fox
    ax.scatter(logx, logy , color='tab:blue', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'tab:blue', label=f'M2 > 1e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_3e8.logx_fox
    logy = ds_m2_3e8.logy_fox
    ax.scatter(logx, logy , color='tab:green', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'green', label=f'M2 > 3e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_5e8.logx_fox
    logy = ds_m2_5e8.logy_fox
    ax.scatter(logx, logy , color='tab:red', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'red', label=f'M2 > 5e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    pyic.plot_settings(ax)   # type: ignore
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.grid()
    ax.legend(loc='lower left', bbox_to_anchor=(-0.3, 1.02, 1, 0.2))
    xlabel = r'$log_{10} \overline{Ri}$'
    ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)

    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_front_m2_lim_fox.png', dpi=150, format='png', bbox_inches='tight')

    # %% wb diagnostic, stone, stone in loglog
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    ii = -1
    fs = 10

    alpha =0.08

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = ds_m2_0.logx_stone     
    logy = ds_m2_0.logy_stone
    ax.scatter(logx, logy , color='tab:orange', marker='^', s=0.5, alpha =alpha, rasterized=True)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'orange', label=f'M2 > 0e-0: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_1e8.logx_stone
    logy = ds_m2_1e8.logy_stone
    ax.scatter(logx, logy , color='tab:blue', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'tab:blue', label=f'M2 > 1e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_3e8.logx_stone
    logy = ds_m2_3e8.logy_stone
    ax.scatter(logx, logy , color='tab:green', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'green', label=f'M2 > 3e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_5e8.logx_stone
    logy = ds_m2_5e8.logy_stone
    ax.scatter(logx, logy , color='tab:red', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'red', label=f'M2 > 5e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    pyic.plot_settings(ax)   # type: ignore
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.grid()
    ax.legend(loc='lower left', bbox_to_anchor=(-0.3, 1.02, 1, 0.2))
    xlabel = r'$log_{10} \overline{Ri}$'
    ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)

    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_front_m2_lim_stone.png', dpi=150, format='png', bbox_inches='tight')



def plot_slopes_scatter_all_m2(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path_save_img, domain, savefig, var):
    # %% wb diagnostic in loglog
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    ii = -1
    fs = 10

    alpha =0.08

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = ds_m2_0.logx_diag
    logy = ds_m2_0.logy_diag
    ax.scatter(logx, logy , color='tab:orange', marker='^', s=0.5, alpha =alpha, rasterized=True)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'orange', label=f'M2 > 0e-0: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_1e8.logx_diag
    logy = ds_m2_1e8.logy_diag
    ax.scatter(logx, logy , color='tab:blue', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'tab:blue', label=f'M2 > 1e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_3e8.logx_diag
    logy = ds_m2_3e8.logy_diag
    ax.scatter(logx, logy , color='tab:green', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'green', label=f'M2 > 3e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_5e8.logx_diag
    logy = ds_m2_5e8.logy_diag
    ax.scatter(logx, logy , color='tab:red', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'red', label=f'M2 > 5e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_0.logx_fox
    logy = ds_m2_0.logy_fox
    ax.scatter(logx, logy , color='tab:pink', marker='o', s=0.5, alpha =alpha, label='Fox-Kemper Param.')

    logx = ds_m2_0.logx_stone
    logy = ds_m2_0.logy_stone
    ax.scatter(logx, logy , color='tab:purple', marker='x', s=0.5, alpha =alpha, label='Stone Param.')


    pyic.plot_settings(ax)   # type: ignore
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.grid()
    legend =  ax.legend(loc='lower left', bbox_to_anchor=(-0.3, 1.02, 1, 0.2), markerscale=10)
    for lh in legend.legendHandles:
        lh.set_alpha(1.0)
    xlabel = r'$log_{10} \overline{Ri}$'
    if var == 'wb':  ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    elif var == 'psi': ylabel = r'$log_{10} |\overline{\psi}|/(H^2 f \alpha) $'
    else: raise Warning('no variable selected')
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)

    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_front_m2_lim_diag_param_structure_m2_filter.png', dpi=150, format='png', bbox_inches='tight')


def plot_slopes_scatter_all_ri(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path_save_img, domain, savefig, var):
    # %% wb diagnostic in loglog
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    ii = -1
    fs = 10

    alpha =0.08

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = ds_m2_0.logx_diag
    logy = ds_m2_0.logy_diag
    ax.scatter(logx, logy , color='tab:orange', marker='^', s=0.5, alpha =alpha, rasterized=True)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'orange', label=f'Ri < 1e4: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_1e8.logx_diag
    logy = ds_m2_1e8.logy_diag
    ax.scatter(logx, logy , color='tab:blue', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'tab:blue', label=f'Ri < 1e3: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_3e8.logx_diag
    logy = ds_m2_3e8.logy_diag
    ax.scatter(logx, logy , color='tab:green', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'green', label=f'Ri < 1e2: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_5e8.logx_diag
    logy = ds_m2_5e8.logy_diag
    ax.scatter(logx, logy , color='tab:red', marker='^', s=0.5, alpha =alpha)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'red', label=f'Ri < 1e1: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_0.logx_fox
    logy = ds_m2_0.logy_fox
    ax.scatter(logx, logy , color='tab:pink', marker='o', s=0.5, alpha =alpha, label='Fox-Kemper Param.')

    logx = ds_m2_0.logx_stone
    logy = ds_m2_0.logy_stone
    ax.scatter(logx, logy , color='tab:purple', marker='x', s=0.5, alpha =alpha, label='Stone Param.')


    pyic.plot_settings(ax)   # type: ignore
    ax.set_xticks([0,1, 2,3, 4])
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    # ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    ax.grid()
    legend =  ax.legend(loc='lower left', bbox_to_anchor=(-0.3, 1.02, 1, 0.2), markerscale=10)
    for lh in legend.legendHandles:
        lh.set_alpha(1.0)
    xlabel = r'$log_{10} \overline{Ri}$'
    if var == 'wb':  ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    elif var == 'vb': ylabel = r'$log_{10} |\overline{v^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    elif var == 'psi': ylabel = r'$log_{10} |\overline{\psi}|/(H^2 f \alpha) $'
    else: raise Warning('no variable selected')
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)

    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_front_m2_lim_diag_param_structure_ri_filter.png', dpi=150, format='png', bbox_inches='tight')


def plot_slopes_scatter_raw(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path_save_img, domain, savefig):
    # %% wb diagnostic in loglog
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=2.2, axlab_kw=None)
    ii = -1
    fs = 10

    alpha =0.08

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = ds_m2_0.logx_diag
    logy = ds_m2_0.logy_diag
    ax.scatter(logx, logy , color='tab:orange', marker='^', s=0.5, alpha =alpha, rasterized=True, label=f'diag: M2 > 0e-0')
    res = stats.linregress(logx, logy)
    # ax.plot(logx, res.intercept + res.slope*logx, 'orange', label=f'M2 > 0e-0: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_1e8.logx_diag
    logy = ds_m2_1e8.logy_diag
    ax.scatter(logx, logy , color='tab:blue', marker='^', s=0.5, alpha =alpha, label=f'diag: M2 > 1e-8')
    res = stats.linregress(logx, logy)
    # ax.plot(logx, res.intercept + res.slope*logx, 'tab:blue', label=f'M2 > 1e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_3e8.logx_diag
    logy = ds_m2_3e8.logy_diag
    ax.scatter(logx, logy , color='tab:green', marker='^', s=0.5, alpha =alpha, label=f'diag: M2 > 3e-8')
    res = stats.linregress(logx, logy)
    # ax.plot(logx, res.intercept + res.slope*logx, 'green', label=f'M2 > 3e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_5e8.logx_diag
    logy = ds_m2_5e8.logy_diag
    ax.scatter(logx, logy , color='tab:red', marker='^', s=0.5, alpha =alpha, label=f'diag: M2 > 5e-8')
    res = stats.linregress(logx, logy)
    # ax.plot(logx, res.intercept + res.slope*logx, 'red', label=f'M2 > 5e-8: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    logx = ds_m2_0.logx_fox
    logy = ds_m2_0.logy_fox
    ax.scatter(logx, logy , color='tab:pink', marker='o', s=0.5, alpha =alpha, label='Fox-Kemper Param.')

    logx = ds_m2_0.logx_stone
    logy = ds_m2_0.logy_stone
    ax.scatter(logx, logy , color='tab:purple', marker='x', s=0.5, alpha =alpha, label='Stone Param.')


    pyic.plot_settings(ax)   # type: ignore
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.grid()

    # #Spacing between each line
    # intervals = 10.#float(sys.argv[1])
    # import matplotlib.ticker as plticker
    # loc = plticker.MultipleLocator(base=intervals)
    # ax.xaxis.set_major_locator(loc)
    # ax.yaxis.set_major_locator(loc)
    # # Add the grid
    # ax.grid(which='major', axis='both', linestyle='-')

    # major_tick = np.power(10, np.linspace(-8,5, 14))
    # major_tick = np.linspace(-8,5, 14)
    # ax.set_xticks(major_tick)

    legend = ax.legend(loc='lower right', markerscale=10)
    for lh in legend.legendHandles:
        lh.set_alpha(1.0)
    xlabel = r'$log_{10} \overline{Ri}$'
    ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)

    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_front_m2_lim_diag_param_raw_structure.png', dpi=150, format='png', bbox_inches='tight')

def plot_2slopes_binned_ri(ds_m2_0, path_save_img, domain, savefig, var, time_window):
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=2.6, axlab_kw=None)
    ii     = -1
    fs     = 15
    alpha  = 0.2
    ms     = 3
    linreg = True

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    colors = ['gray', 'tab:blue', 'tab:orange']
    ri_idx = 1#0.8
    bins   = 60

    logx = ds_m2_0.logx_diag
    logy = ds_m2_0.logy_diag
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[0]}', alpha=1, zorder=0, label=f'diagnosed')

    logx = ds_m2_0.logx_fox
    logy = ds_m2_0.logy_fox
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[1]}', alpha=1, zorder=0, label=f'Fox-Kemper')

    logx = ds_m2_0.logx_stone
    logy = ds_m2_0.logy_stone
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[2]}', alpha=1, zorder=0, label=f'Stone')
    
    if var == 'wb': legend1 = plt.legend(loc='upper right', markerscale=1)

    for ii in range(3):
        if ii == 0:
            logx = ds_m2_0.logx_diag
            logy = ds_m2_0.logy_diag
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        elif ii == 1:
            logx = ds_m2_0.logx_fox
            logy = ds_m2_0.logy_fox
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        elif ii == 2:
            logx = ds_m2_0.logx_stone
            logy = ds_m2_0.logy_stone
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        else: raise Warning('ii out of bound')
        if linreg == True:
            i=0
            for bin_idx in [bin_centers < ri_idx, bin_centers > ri_idx]:
                x = bin_centers[bin_idx]
                y = bin_means[bin_idx]
                X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(bin_centers[bin_idx], bin_means[bin_idx], bin_stds[bin_idx])
                a1 = X[0]
                a0 = X[1]
                # print(f'X: {X}, err_covX: {err_covX}, CI: {CI}')

                if i == 0: 
                    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                    plt.plot(x, a0 + a1*x, color=f'{colors[ii]}', linewidth=2, label=rf'$ ({intercept} )~Ri^{{{slope}}}$ ')
                    # plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color=f'{colors[ii]}', alpha=alpha)
                else:
                    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                    plt.plot(x, a0 + a1*x, color=f'{colors[ii]}',linewidth=2, label=rf'$ ({intercept})~ Ri^{{{slope}}}$')
                    # plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color=f'{colors[ii]}', alpha=alpha)
                i += 1
                

    plt.grid()
    h,l = hca[0].get_legend_handles_labels()
    legend2 = plt.legend(l[:6], loc='upper left', markerscale=1)
    if var == 'wb': hca[0].add_artist(legend1) #type: ignore
    hca[0].add_artist(legend2)
    if var == 'vb': hca = add_string_like_pyic(hca, ['(a)'])
    if var == 'wb': hca = add_string_like_pyic(hca, ['(b)'])

    xlabel = r'$log_{10} \overline{Ri}$'
    if   var == 'wb':  ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    elif var == 'vb':  ylabel = r'$log_{10} |\overline{v^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^3)  $'
    elif var == 'psi': ylabel = r'$log_{10} |\overline{\psi}|/(H^2 f \alpha) $'
    else: raise Warning('no variable selected')
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)

    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_binned_2slope_paper_{var}_ri_{time_window}.png', dpi=250, format='png', bbox_inches='tight')

def plot_2slopes_binned_ri_new(ds_m2_0, path_save_img, domain, savefig, var, time_window):
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=2.6, axlab_kw=None)
    ii     = -1
    fs     = 15
    alpha  = 0.2
    ms     = 3
    linreg = True

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    colors = ['gray', 'tab:blue', 'tab:orange']
    ri_idx = np.log10(8)
    # add red vertical line
    bins   = 60

    logx = ds_m2_0.logx_diag
    logy = ds_m2_0.logy_diag
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[0]}', alpha=1, zorder=0, label=f'diagnosed')

    logx = ds_m2_0.logx_fox
    logy = ds_m2_0.logy_fox
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[1]}', alpha=1, zorder=0, label=f'Fox-Kemper')

    logx = ds_m2_0.logx_stone
    logy = ds_m2_0.logy_stone
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[2]}', alpha=1, zorder=0, label=f'Stone')
    
    if var == 'wb': legend1 = plt.legend(loc='upper right', markerscale=1)

    for ii in range(3):
        if ii == 0:
            logx = ds_m2_0.logx_diag
            logy = ds_m2_0.logy_diag
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        elif ii == 1:
            logx = ds_m2_0.logx_fox
            logy = ds_m2_0.logy_fox
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        elif ii == 2:
            logx = ds_m2_0.logx_stone
            logy = ds_m2_0.logy_stone
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        else: raise Warning('ii out of bound')
        if linreg == True:
            if ii == 0:
                i=0
                for bin_idx in [bin_centers < ri_idx, bin_centers > ri_idx]:
                    x = bin_centers[bin_idx]
                    y = bin_means[bin_idx]
                    X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(bin_centers[bin_idx], bin_means[bin_idx], bin_stds[bin_idx])
                    a1 = X[0]
                    a0 = X[1]
                    # print(f'X: {X}, err_covX: {err_covX}, CI: {CI}')

                    if i == 0: 
                        slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                        intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                        plt.plot(x, a0 + a1*x, color=f'{colors[ii]}', linewidth=3, label=rf'$ ({intercept} )~Ri^{{{slope}}}$ ', zorder=20)
                        # plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color=f'{colors[ii]}', alpha=alpha)
                    else:
                        slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                        intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                        plt.plot(x, a0 + a1*x, color=f'{colors[ii]}',linewidth=3, label=rf'$ ({intercept})~ Ri^{{{slope}}}$', zorder=21)
                        # plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color=f'{colors[ii]}', alpha=alpha)
                    i += 1
            else:
                x = bin_centers
                y = bin_means
                X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(bin_centers, bin_means, bin_stds)
                a1 = X[0]
                a0 = X[1]
                # print(f'X: {X}, err_covX: {err_covX}, CI: {CI}')
                i=0

                if i == 0: 
                    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                    plt.plot(x, a0 + a1*x, color=f'{colors[ii]}', linewidth=2, label=rf'$ ({intercept} )~Ri^{{{slope}}}$ ')
                    # plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color=f'{colors[ii]}', alpha=alpha)
                else:
                    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                    plt.plot(x, a0 + a1*x, color=f'{colors[ii]}',linewidth=2, label=rf'$ ({intercept})~ Ri^{{{slope}}}$')

    plt.grid()
    h,l = hca[0].get_legend_handles_labels()
    legend2 = plt.legend(l[:4], loc='upper left', markerscale=1)
    if var == 'wb': hca[0].add_artist(legend1) #type: ignore
    hca[0].add_artist(legend2)
    if var == 'vb': hca = add_string_like_pyic(hca, ['(a)'])
    if var == 'wb': hca = add_string_like_pyic(hca, ['(b)'])
    if var == 'vb': plt.ylim(-1.5,3)
    if var == 'wb': plt.ylim(-3.5,1)

    xlabel = r'$log_{10} \overline{Ri}$'
    if   var == 'wb':  ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    elif var == 'vb':  ylabel = r'$log_{10} |\overline{v^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^3)  $'
    elif var == 'psi': ylabel = r'$log_{10} |\overline{\psi}|/(H^2 f \alpha) $'
    else: raise Warning('no variable selected')
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)
    ax.axvline(ri_idx, color='tab:red', linestyle='-', linewidth=1.5, alpha=0.5, zorder=0)

    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_binned_2slope_{var}_ri_{time_window}_new.png', dpi=250, format='png', bbox_inches='tight')


def plot_2slopes_binned_ri_both(ds_vb, ds_wb, path_save_img, savefig, ri_critical, string):
    hca, hcb = pyic.arrange_axes(2, 1, plot_cb=False, asp=1, fig_size_fac=2.6, axlab_kw=None)
    ii     = -1
    fs     = 15
    ms     = 3
    linreg = True
    colors = ['gray', 'tab:blue', 'tab:orange']
    ri_idx = np.log10(ri_critical)
    bins   = 60

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.text(0, 1.05, '(a)', transform = ax.transAxes, horizontalalignment = 'right', fontdict=None)

    ds = ds_vb
    var = 'vb'
    logx = ds.logx_diag
    logy = ds.logy_diag
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[0]}', alpha=1, zorder=0, label=f'diag.')

    logx = ds.logx_fox
    logy = ds.logy_fox
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[1]}', alpha=1, zorder=0, label=f'PER')

    logx = ds.logx_stone
    logy = ds.logy_stone
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[2]}', alpha=1, zorder=0, label=f'ALS')
    
    for iii in range(3):
        if iii == 0:
            logx = ds.logx_diag
            logy = ds.logy_diag
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        elif iii == 1:
            logx = ds.logx_fox
            logy = ds.logy_fox
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        elif iii == 2:
            logx = ds.logx_stone
            logy = ds.logy_stone
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        else: raise Warning('ii out of bound')
        if linreg == True:
            if iii == 0:
                i=0
                for bin_idx in [bin_centers < ri_idx, bin_centers > ri_idx]:
                    x = bin_centers[bin_idx]
                    y = bin_means[bin_idx]
                    X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(bin_centers[bin_idx], bin_means[bin_idx], bin_stds[bin_idx])
                    a1 = X[0]
                    a0 = X[1]

                    if i == 0: 
                        slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                        intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                        ax.plot(x, a0 + a1*x, color=f'{colors[iii]}', linewidth=3, label=rf'$ ({intercept} )~Ri^{{{slope}}}$ ', zorder=20)
                    else:
                        slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                        intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                        ax.plot(x, a0 + a1*x, color=f'{colors[iii]}',linewidth=3, label=rf'$ ({intercept})~ Ri^{{{slope}}}$', zorder=21)
                    i += 1
            else:
                x = bin_centers
                y = bin_means
                X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(bin_centers, bin_means, bin_stds)
                a1 = X[0]
                a0 = X[1]
                i=0

                if i == 0: 
                    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                    ax.plot(x, a0 + a1*x, color=f'{colors[iii]}', linewidth=2, label=rf'$ ({intercept} )~Ri^{{{slope}}}$ ')
                    # plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color=f'{colors[ii]}', alpha=alpha)
                else:
                    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                    ax.plot(x, a0 + a1*x, color=f'{colors[iii]}',linewidth=2, label=rf'$ ({intercept})~ Ri^{{{slope}}}$')

    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:4], loc='upper left', markerscale=1)
    ax.add_artist(legend2)
    if var == 'vb': plt.ylim(-1.5,3)
    if   var == 'wb':  ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    elif var == 'vb':  ylabel = r'$log_{10} |\overline{v^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^3)  $'
    elif var == 'psi': ylabel = r'$log_{10} |\overline{\psi}|/(H^2 f \alpha) $'
    else: raise Warning('no variable selected')

    xlabel = r'$log_{10} \overline{Ri}$'
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)
    ax.axvline(ri_idx, color='tab:red', linestyle='-', linewidth=1.5, alpha=0.5, zorder=0)
    ax.grid()

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ds = ds_wb
    var = 'wb'

    logx = ds.logx_diag
    logy = ds.logy_diag
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[0]}', alpha=1, zorder=0, label=f'diag.')

    logx = ds.logx_fox
    logy = ds.logy_fox
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[1]}', alpha=1, zorder=0, label=f'PER')

    logx = ds.logx_stone
    logy = ds.logy_stone
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors[2]}', alpha=1, zorder=0, label=f'ALS')
    
    if var == 'wb': legend1 = plt.legend(loc='upper right', markerscale=1)

    for iii in range(3):
        if iii == 0:
            logx = ds.logx_diag
            logy = ds.logy_diag
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        elif iii == 1:
            logx = ds.logx_fox
            logy = ds.logy_fox
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        elif iii == 2:
            logx = ds.logx_stone
            logy = ds.logy_stone
            bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
        else: raise Warning('ii out of bound')
        if linreg == True:
            if iii == 0:
                i=0
                for bin_idx in [bin_centers < ri_idx, bin_centers > ri_idx]:
                    x = bin_centers[bin_idx]
                    y = bin_means[bin_idx]
                    X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(bin_centers[bin_idx], bin_means[bin_idx], bin_stds[bin_idx])
                    a1 = X[0]
                    a0 = X[1]
                    # print(f'X: {X}, err_covX: {err_covX}, CI: {CI}')

                    if i == 0: 
                        slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                        intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                        plt.plot(x, a0 + a1*x, color=f'{colors[iii]}', linewidth=3, label=rf'$ ({intercept} )~Ri^{{{slope}}}$ ', zorder=20)
                        # plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color=f'{colors[ii]}', alpha=alpha)
                    else:
                        slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                        intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                        plt.plot(x, a0 + a1*x, color=f'{colors[iii]}',linewidth=3, label=rf'$ ({intercept})~ Ri^{{{slope}}}$', zorder=21)
                        # plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color=f'{colors[ii]}', alpha=alpha)
                    i += 1
            else:
                x = bin_centers
                y = bin_means
                X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(bin_centers, bin_means, bin_stds)
                a1 = X[0]
                a0 = X[1]
                # print(f'X: {X}, err_covX: {err_covX}, CI: {CI}')
                i=0

                if i == 0: 
                    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                    plt.plot(x, a0 + a1*x, color=f'{colors[iii]}', linewidth=2, label=rf'$ ({intercept} )~Ri^{{{slope}}}$ ')
                    # plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color=f'{colors[ii]}', alpha=alpha)
                else:
                    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
                    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
                    plt.plot(x, a0 + a1*x, color=f'{colors[iii]}',linewidth=2, label=rf'$ ({intercept})~ Ri^{{{slope}}}$')

    ax.grid()
    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:4], loc='upper left', markerscale=1)
    if var == 'wb': ax.add_artist(legend1) #type: ignore
    ax.add_artist(legend2)
    # if var == 'vb': hca = add_string_like_pyic(hca, ['(a)'])
    # if var == 'wb': hca = add_string_like_pyic(hca, ['(b)'])
    if var == 'vb': plt.ylim(-1.5,3)
    if var == 'wb': plt.ylim(-3.5,1)

    xlabel = r'$log_{10} \overline{Ri}$'
    if   var == 'wb':  ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    elif var == 'vb':  ylabel = r'$log_{10} |\overline{v^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^3)  $'
    elif var == 'psi': ylabel = r'$log_{10} |\overline{\psi}|/(H^2 f \alpha) $'
    else: raise Warning('no variable selected')
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)
    ax.axvline(ri_idx, color='tab:red', linestyle='-', linewidth=1.5, alpha=0.5, zorder=0)

    #add string manually
    ax.text(0, 1.05, '(b)', transform = ax.transAxes, horizontalalignment = 'right', fontdict=None)
    if savefig == True: plt.savefig(f'{path_save_img}/vb_wb_slope_{string}.png', dpi=250, format='png', bbox_inches='tight')



def add_string_like_pyic(hca, figstr, posx=[-0.00], posy=[1.05], fontdict=None):
    for nn, ax in enumerate(hca):
        ht = ax.text(posx[nn], posy[nn], figstr[nn], 
                      transform = hca[nn].transAxes, 
                      horizontalalignment = 'right',
                      fontdict=fontdict)

def plot_slopes_binned_ri(ds_m2_0, path_save_img, domain, savefig, var, time_window):
    # % wb diagnostic in loglog
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=3, axlab_kw=None)
    ii = -1
    fs = 15
    alpha = 0.2

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = ds_m2_0.logx_diag
    logy = ds_m2_0.logy_diag

    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, 60)
    ri_idx = 0.7
    i=0
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=3, color='dimgrey', alpha=1, zorder=0, label=f'binned means and stds')
    for bin_idx in [bin_centers < ri_idx, bin_centers > ri_idx]:
        x = bin_centers[bin_idx]
        y = bin_means[bin_idx]
        X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(bin_centers[bin_idx], bin_means[bin_idx], bin_stds[bin_idx])
        # X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(bin_centers[bin_idx], bin_means[bin_idx])
        a1 = X[0]
        a0 = X[1]
        # print(f'a1: {a1}, a0: {a0}, CI: {CI}')
        print(f'X: {X}, err_covX: {err_covX}, CI: {CI}')

        # plt.scatter(logx, logy, s=3, label='data')
        # if i == 0: 
        #     plt.plot(x, a0 + a1*x, 'tab:red', label=rf'Ri < 10: a1: {X[0]:.2f} $\pm$ {err_covX[0]:.2f}, a0: {X[1]:.2f} $\pm$ {err_covX[1]:.2f} ''\n'rf' R2: {R2:.2f}, rXY: {rXY:.2f}, $\sigma_e$: {sigma_e:.2f}')
        #     plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color='red', alpha=0.2)
        # else:
        #     plt.plot(x, a0 + a1*x, 'tab:red', label=rf'Ri > 10: a1: {X[0]:.2f} $\pm$ {err_covX[0]:.2f}, a0: {X[1]:.2f} $\pm$ {err_covX[1]:.2f} ''\n'rf' R2: {R2:.2f}, rXY: {rXY:.2f}, $\sigma_e$: {sigma_e:.2f}')
        #     plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color='red', alpha=0.2)
        # i += 1

        if i == 0: 
            slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
            intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
            plt.plot(x, a0 + a1*x, 'tab:red', linewidth=3, label=rf'$ ({intercept} )~Ri^{{{slope}}}$ ')
            plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color='tab:red', alpha=0.2)

        else:
            slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
            intercept = rf'{10**X[1]:.4f} \pm {10**err_covX[1]:.2f}'
            plt.plot(x, a0 + a1*x, 'tab:purple',linewidth=3, label=rf'$ ({intercept})~ Ri^{{{slope}}}$')
            plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color='tab:purple', alpha=0.2)
        i += 1

    logx = ds_m2_0.logx_fox
    logy = ds_m2_0.logy_fox
    X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(logx, logy)
    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
    ax.scatter(logx, logy , color='tab:blue', marker='o', s=1, alpha =1, label= rf'$ ({intercept})~ Ri^{{{slope}}}~$ Fox-Kemper')

    logx = ds_m2_0.logx_stone
    logy = ds_m2_0.logy_stone
    X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(logx, logy)
    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
    ax.scatter(logx, logy , color='tab:orange', marker='x', s=1, alpha =1,label= rf'$ ({intercept})~ Ri^{{{slope}}}$ Stone')


    ax.set_xlim(0, 3.5)
    ax.set_ylim(-3, 1.5)
    plt.grid()
    plt.legend(loc='upper left', markerscale=2)
    xlabel = r'$log_{10} \overline{Ri}$'
    if var == 'wb':  ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    elif var == 'psi': ylabel = r'$log_{10} |\overline{\psi}|/(H^2 f \alpha) $'
    else: raise Warning('no variable selected')
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)

    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_binned_all_{var}_ri_{time_window}.png', dpi=250, format='png', bbox_inches='tight')


def plot_wb_scatter(ds_m2_0):
    fig, ax = plt.subplots(1,1, figsize=(5,5), sharey=False, squeeze=True) 

    x = np.linspace(1e-11,1e-5,100)
    y = x

    xvals = [np.nan, np.nan, 3e-2, 3e-2, 3e-2, 3e-2 ]
    lim_max = 1e-5 #psi_diag_full_dmean.max()
    lim_min = 1e-11 #psi_diag_full_dmean.min()
    ax.set_ylim(lim_min,lim_max)
    ax.set_xlim(lim_min,lim_max)
    ax.loglog(x,y, 'k')
    ax.loglog(ds_m2_0.wb, ds_m2_0.wb_fox_param, 'o', color='red', markersize=0.5)
    ax.loglog(ds_m2_0.wb, ds_m2_0.wb_stone_param, 'x', color='blue', markersize=0.5)
    ax.set_xlabel('diag')
    ax.set_ylabel('param')
    plt.grid()

# maps

def plot_wb_maps(data, domain, path, savefig):
    lat_reg = np.array([data.lat[0],data.lat[-1]])
    lon_reg = np.array([data.lon[0],data.lon[-1]])
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(3, 1, plot_cb='right', asp=asp, fig_size_fac=2, projection=ccrs_proj, sharey=True, axlab_kw=None) # type: ignore
    clim = 0, 1e-6
    cmap = 'Reds'
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_ylabel(r'$\overline{w^\prime b^\prime}$')
    ax.set_title(r'Diagnostic')
    pyic.shade(data.lon, data.lat, data.wb, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='None')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(r'Fox-Kemper Parameterization')
    pyic.shade(data.lon, data.lat, data.wb_fox_param, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='None')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(r'Stone Parameterization')
    pyic.shade(data.lon, data.lat, data.wb_stone_param, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='neither')

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore

    if savefig == True: plt.savefig(f'{path}{domain}_wb.png', dpi=150, bbox_inches='tight')   

def plot_vb_maps(data, domain, path, savefig):
    lat_reg = np.array([data.lat[0],data.lat[-1]])
    lon_reg = np.array([data.lon[0],data.lon[-1]])
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(3, 1, plot_cb='right', asp=asp, fig_size_fac=2, projection=ccrs_proj, sharey=True, axlab_kw=None) # type: ignore
    clim = -1e-5, 1e-5
    cmap = 'RdBu'
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_ylabel(r'|$\overline{v^\prime b^\prime}$|')
    ax.set_title(r'Diagnostic')
    pyic.shade(data.lon, data.lat, data.Vb_cross_grad, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='None')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(r'Fox-Kemper Parameterization')
    pyic.shade(data.lon, data.lat, data.Vb_cross_grad_fox_param, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='None')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(r'Stone Parameterization')
    pyic.shade(data.lon, data.lat, data.vb_stone_param, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='max')

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore

    if savefig == True: plt.savefig(f'{path}{domain}_vb.png', dpi=150, bbox_inches='tight')   


def plot_psi_maps(data, domain, path, savefig):
    lat_reg = np.array([data.lat[0],data.lat[-1]])
    lon_reg = np.array([data.lon[0],data.lon[-1]])
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(3, 1, plot_cb='right', asp=asp, fig_size_fac=2, projection=ccrs_proj, sharey=True, axlab_kw=None) # type: ignore
    clim = -5, 5
    cmap = 'RdBu'
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_ylabel(r'$|\overline{\psi}|$')
    ax.set_title(r'Diagnostic')
    pyic.shade(data.lon, data.lat, data.psi_diag_full, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='None')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(r'Fox-Kemper Parameterization')
    pyic.shade(data.lon, data.lat, data.psi_fox_full_param, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='None')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(r'Stone Parameterization')
    pyic.shade(data.lon, data.lat, data.psi_stone_full_param, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='max')

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore

    if savefig == True: plt.savefig(f'{path}{domain}_psi.png', dpi=150, bbox_inches='tight')   


def plot_psi_held_maps(data, domain, path, savefig):
    lat_reg = np.array([data.lat[0],data.lat[-1]])
    lon_reg = np.array([data.lon[0],data.lon[-1]])
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(3, 1, plot_cb='right', asp=asp, fig_size_fac=2, projection=ccrs_proj, sharey=True, axlab_kw=None) # type: ignore
    clim = 0, 10
    cmap = 'Blues'
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_ylabel(r'$|\overline{\psi}|$ Held-Schneider')
    ax.set_title(r'Diagnostic')
    pyic.shade(data.lon, data.lat, np.abs(data.psi_diag_held), ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='None')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(r'Fox-Kemper Parameterization')
    pyic.shade(data.lon, data.lat, np.abs(data.psi_fox_held_param), ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='None')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(r'Stone Parameterization')
    pyic.shade(data.lon, data.lat, np.abs(data.psi_stone_held_param), ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='max')

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore

    if savefig == True: plt.savefig(f'{path}{domain}_psi_held.png', dpi=150, bbox_inches='tight')   


def plot_K_maps(data, domain, path, savefig):
    lat_reg = np.array([data.lat[0],data.lat[-1]])
    lon_reg = np.array([data.lon[0],data.lon[-1]])
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(3, 1, plot_cb='right', asp=asp, fig_size_fac=2, projection=ccrs_proj, sharey=True, axlab_kw=None) # type: ignore
    clim = 0, 5e-2
    cmap = 'Oranges'
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_ylabel(r'K ')
    ax.set_title(r'Diagnostic')
    pyic.shade(data.lon, data.lat, data.K, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='None')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(r'Fox-Kemper Parameterization')
    pyic.shade(data.lon, data.lat, data.K_fox, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='None')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(r'Stone Parameterization')
    pyic.shade(data.lon, data.lat, data.K_stone, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='max')

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore

    if savefig == True: plt.savefig(f'{path}{domain}_K.png', dpi=150, bbox_inches='tight')   


def plot_N2_map(data, domain, path, savefig):
    lat_reg = np.array([data.lat[0],data.lat[-1]])
    lon_reg = np.array([data.lon[0],data.lon[-1]])
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb='right', asp=asp, fig_size_fac=2, projection=ccrs_proj, sharey=True, axlab_kw=None) # type: ignore
    clim = 5e-6, 5e-5
    cmap = 'Spectral_r'
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_ylabel(r'$N^2$ ')
    ax.set_title(r'Diagnostic')
    pyic.shade(data.lon, data.lat, data.n2, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='both')

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore

    if savefig == True: plt.savefig(f'{path}{domain}_N2.png', dpi=150, bbox_inches='tight')   


def plot_M2_map(data, domain, path, savefig):
    lat_reg = np.array([data.lat[0],data.lat[-1]])
    lon_reg = np.array([data.lon[0],data.lon[-1]])
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb='right', asp=asp, fig_size_fac=2, projection=ccrs_proj, sharey=True, axlab_kw=None) # type: ignore
    clim = 0, 1e-7
    cmap = 'Spectral_r'
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_ylabel(r'$M^2$ ')
    ax.set_title(r'Diagnostic')
    pyic.shade(data.lon, data.lat, data.m2_cross_grad, ax=ax, cax=cax,  transform=ccrs_proj, rasterized=False, clim=clim, cmap=cmap, contfs=True, extend='both')

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) # type: ignore

    if savefig == True: plt.savefig(f'{path}{domain}_m2.png', dpi=150, bbox_inches='tight')   

# %% NEW ORDER


def plot_regression_line(ax, logx, logy, colors ):
    bins   = 60
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    x = bin_centers
    y = bin_means
    X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(bin_centers, bin_means, bin_stds)
    a1 = X[0]
    a0 = X[1]

    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
    ax.plot(x, a0 + a1*x, color=f'{colors}', linewidth=3, label=rf'$ ({intercept} )~\alpha^{{{slope}}}$ ', zorder=20)
    return ax

def plot_errorbars(ax, logx, logy, colors, label=None):
    bins=60
    ms=3
    bin_means, bin_stds, bin_counts, bin_centers = mylinreg.calc_bin_means(logx, logy, bins)
    ax.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', markersize=ms, color=f'{colors}', alpha=1, zorder=0, label=label)
    return ax

def plot_alpha_reg(ds_vb, ds_wb, ylabel1, ylabel2, path_save_img, savefig, string):
    hca, hcb = pyic.arrange_axes(2, 1, plot_cb=False, asp=1, fig_size_fac=2.6, sharex=True)
    colors = ['gray', 'tab:blue', 'tab:orange']
    ii     = -1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax = plot_errorbars(ax, ds_vb.logx_diag, ds_vb.logy_diag, colors[0])
    # ax = plot_errorbars(ax, ds_vb.logx_fox, ds_vb.logy_fox, colors[1])
    ax = plot_errorbars(ax, ds_vb.logx_stone, ds_vb.logy_stone, colors[2])

    ax = plot_regression_line(ax, ds_vb.logx_diag, ds_vb.logy_diag, colors[0])
    # ax = plot_regression_line(ax, ds_vb.logx_fox, ds_vb.logy_fox, colors[1])
    ax = plot_regression_line(ax, ds_vb.logx_stone, ds_vb.logy_stone, colors[2])

    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:3], loc='lower right', markerscale=1)
    ax.add_artist(legend2)

    ax.set_ylabel(ylabel1, fontsize=15)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax = plot_errorbars(ax, ds_wb.logx_diag, ds_wb.logy_diag, colors[0], label='diag.')
    # ax = plot_errorbars(ax, ds_wb.logx_fox, ds_wb.logy_fox, colors[1], label='PER')
    ax = plot_errorbars(ax, ds_wb.logx_stone, ds_wb.logy_stone, colors[2], label='ALS')
    
    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:3], loc='upper left', markerscale=1)
    ax.add_artist(legend2)

    ax = plot_regression_line(ax, ds_wb.logx_diag, ds_wb.logy_diag, colors[0])
    # ax = plot_regression_line(ax, ds_wb.logx_fox, ds_wb.logy_fox, colors[1])
    ax = plot_regression_line(ax, ds_wb.logx_stone, ds_wb.logy_stone, colors[2])

    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:2], loc='lower right', markerscale=1)
    ax.add_artist(legend2)

    ax.set_ylabel(ylabel2, fontsize=15)

    for ax in hca:
        ax.set_xlabel(r'$log_{10} \overline{\alpha}$', fontsize=15)
        ax.grid()

    if savefig == True: plt.savefig(f'{path_save_img}/{string}.png', dpi=250, format='png', bbox_inches='tight')



def plot_alpha_reg_new(ds_vb, ds_wb, ylabel1, ylabel2, path_save_img, savefig, string):
    hca, hcb = pyic.arrange_axes(2, 1, plot_cb=False, asp=1, fig_size_fac=2.6, sharex=True)
    colors = ['gray', 'tab:blue', 'tab:orange']
    ii     = -1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax = plot_errorbars(ax, ds_vb.logx_diag, ds_vb.logy_diag, colors[0])
    ax = plot_errorbars(ax, ds_vb.logx_fox, ds_vb.logy_fox, colors[1])
    # ax = plot_errorbars(ax, ds_vb.logx_stone, ds_vb.logy_stone, colors[2])

    ax = plot_regression_line(ax, ds_vb.logx_diag, ds_vb.logy_diag, colors[0])
    ax = plot_regression_line(ax, ds_vb.logx_fox, ds_vb.logy_fox, colors[1])
    # ax = plot_regression_line(ax, ds_vb.logx_stone, ds_vb.logy_stone, colors[2])

    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:3], loc='lower right', markerscale=1)
    ax.add_artist(legend2)

    ax.set_ylabel(ylabel1, fontsize=15)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax = plot_errorbars(ax, ds_wb.logx_diag, ds_wb.logy_diag, colors[0], label='diag.')
    ax = plot_errorbars(ax, ds_wb.logx_fox, ds_wb.logy_fox, colors[1], label='PER')
    # ax = plot_errorbars(ax, ds_wb.logx_stone, ds_wb.logy_stone, colors[2], label='ALS')
    
    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:3], loc='upper left', markerscale=1)
    ax.add_artist(legend2)

    ax = plot_regression_line(ax, ds_wb.logx_diag, ds_wb.logy_diag, colors[0])
    ax = plot_regression_line(ax, ds_wb.logx_fox, ds_wb.logy_fox, colors[1])
    # ax = plot_regression_line(ax, ds_wb.logx_stone, ds_wb.logy_stone, colors[2])

    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:2], loc='lower right', markerscale=1)
    ax.add_artist(legend2)

    ax.set_ylabel(ylabel2, fontsize=15)

    for ax in hca:
        ax.set_xlabel(r'$log_{10} \overline{\alpha}$', fontsize=15)
        ax.grid()

    if savefig == True: plt.savefig(f'{path_save_img}/{string}.png', dpi=250, format='png', bbox_inches='tight')



def plot_alpha_reg_4(ds_vb_per, ds_wb_per, ds_vb_als, ds_wb_als, ylabel1, ylabel2, ylabel3, ylabel4, ri, path_save_img, savefig, string):
    hca, hcb = pyic.arrange_axes(2, 2, plot_cb=False, asp=1, fig_size_fac=2.6, sharex=True)
    colors = ['gray', 'tab:blue', 'tab:orange']
    ii     = -1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax = plot_errorbars(ax, ds_vb_per.logx_diag, ds_vb_per.logy_diag, colors[0])
    ax = plot_regression_line(ax, ds_vb_per.logx_diag, ds_vb_per.logy_diag, colors[0])

    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:3], loc='lower right', markerscale=1)
    ax.add_artist(legend2)

    ax.set_ylabel(ylabel1, fontsize=15)
    ax.set_title('PER')

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax = plot_errorbars(ax, ds_wb_per.logx_diag, ds_wb_per.logy_diag, colors[0], label='Ri < None')

    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:3], loc='upper left', markerscale=1)
    ax.add_artist(legend2)

    ax = plot_regression_line(ax, ds_wb_per.logx_diag, ds_wb_per.logy_diag, colors[0])

    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:1], loc='lower right', markerscale=1)
    ax.add_artist(legend2)

    ax.set_ylabel(ylabel2, fontsize=15)
    ax.set_title('PER')

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    color_line = ['grey', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown']
    for i in range(ri.size):
        ax = plot_errorbars(ax, ds_vb_als[i].logx_diag, ds_vb_als[i].logy_diag, color_line[i], label=f'Ri < {ri[i]}')
    #second legend with the rest by keeping frist lagend

    for i in range(ri.size):
        ax = plot_regression_line(ax, ds_vb_als[i].logx_diag, ds_vb_als[i].logy_diag, color_line[i])

    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:ri.size], loc='upper left', markerscale=1)
    ax.add_artist(legend2)
    ax.set_ylabel(ylabel3, fontsize=15)
    h,l = ax.get_legend_handles_labels()
    legend3 = ax.legend(l[ri.size:], loc='lower right', markerscale=1)
    ax.add_artist(legend3)
    ax.set_title('ALS')
    ax.set_xlabel(r'$log_{10} \overline{\alpha}$', fontsize=15)
    # h,l = ax.get_legend_handles_labels()
    # legend3 = ax.legend(l[ri.size:], loc='lower right', markerscale=1)
    # ax.add_artist(legend3)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # ax = plot_errorbars(ax, ds_wb_als.logx_diag, ds_wb_als.logy_diag, colors[0], label='diag.')
    # ax = plot_regression_line(ax, ds_wb_als.logx_diag, ds_wb_als.logy_diag, colors[0])
    for i in range(ri.size):
        ax = plot_errorbars(ax, ds_wb_als[i].logx_diag, ds_wb_als[i].logy_diag, color_line[i])
        ax = plot_regression_line(ax, ds_wb_als[i].logx_diag, ds_wb_als[i].logy_diag, color_line[i])


    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:4], loc='upper left', markerscale=1)
    ax.add_artist(legend2)

    ax.set_ylabel(ylabel4, fontsize=15)
    ax.set_title('ALS')
    ax.set_xlabel(r'$log_{10} \overline{\alpha}$', fontsize=15)


    for ax in hca:
        ax.grid()

    if savefig == True: plt.savefig(f'{path_save_img}/{string}.png', dpi=250, format='png', bbox_inches='tight')


def plot_alpha_reg_4a(ds_vb_per, ds_wb_per, ds_vb_als, ds_wb_als, ylabel1, ylabel2, ylabel3, ylabel4, ri, path_save_img, savefig, string):
    hca, hcb = pyic.arrange_axes(2, 2, plot_cb=False, asp=1, fig_size_fac=2.6, sharex=True)
    colors = ['gray', 'tab:blue', 'tab:orange']
    color_line = ['grey', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown']
    ii     = -1
    ii+=1; ax=hca[ii]; cax=hcb[ii]


    for i in range(ri.size):
        ax = plot_errorbars(ax, ds_vb_per[i].logx_diag, ds_vb_per[i].logy_diag, color_line[i], label=f'Ri < {ri[i]}')
    #second legend with the rest by keeping frist lagend

    for i in range(ri.size):
        ax = plot_regression_line(ax, ds_vb_per[i].logx_diag, ds_vb_per[i].logy_diag, color_line[i])

    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:ri.size], loc='upper left', markerscale=1)
    ax.add_artist(legend2)
    ax.set_ylabel(ylabel3, fontsize=15)
    h,l = ax.get_legend_handles_labels()
    legend3 = ax.legend(l[ri.size:], loc='lower right', markerscale=1)
    ax.add_artist(legend3)
    ax.set_title('PER')
    # ax.set_xlabel(r'$log_{10} \overline{\alpha}$', fontsize=15)
    # h,l = ax.get_legend_handles_labels()
    # legend3 = ax.legend(l[ri.size:], loc='lower right', markerscale=1)
    # ax.add_artist(legend3)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # ax = plot_errorbars(ax, ds_wb_per.logx_diag, ds_wb_per.logy_diag, colors[0], label='diag.')
    # ax = plot_regression_line(ax, ds_wb_per.logx_diag, ds_wb_als.logy_diag, colors[0])
    for i in range(ri.size):
        ax = plot_errorbars(ax, ds_wb_per[i].logx_diag, ds_wb_per[i].logy_diag, color_line[i])
        ax = plot_regression_line(ax, ds_wb_per[i].logx_diag, ds_wb_per[i].logy_diag, color_line[i])


    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:4], loc='upper left', markerscale=1)
    ax.add_artist(legend2)

    ax.set_ylabel(ylabel4, fontsize=15)
    ax.set_title('PER')

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    for i in range(ri.size):
        ax = plot_errorbars(ax, ds_vb_als[i].logx_diag, ds_vb_als[i].logy_diag, color_line[i], label=f'Ri < {ri[i]}')
    #second legend with the rest by keeping frist lagend

    for i in range(ri.size):
        ax = plot_regression_line(ax, ds_vb_als[i].logx_diag, ds_vb_als[i].logy_diag, color_line[i])

    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:ri.size], loc='upper left', markerscale=1)
    ax.add_artist(legend2)
    ax.set_ylabel(ylabel3, fontsize=15)
    h,l = ax.get_legend_handles_labels()
    legend3 = ax.legend(l[ri.size:], loc='lower right', markerscale=1)
    ax.add_artist(legend3)
    ax.set_title('ALS')
    ax.set_xlabel(r'$log_{10} \overline{\alpha}$', fontsize=15)
    # h,l = ax.get_legend_handles_labels()
    # legend3 = ax.legend(l[ri.size:], loc='lower right', markerscale=1)
    # ax.add_artist(legend3)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # ax = plot_errorbars(ax, ds_wb_als.logx_diag, ds_wb_als.logy_diag, colors[0], label='diag.')
    # ax = plot_regression_line(ax, ds_wb_als.logx_diag, ds_wb_als.logy_diag, colors[0])
    for i in range(ri.size):
        ax = plot_errorbars(ax, ds_wb_als[i].logx_diag, ds_wb_als[i].logy_diag, color_line[i])
        ax = plot_regression_line(ax, ds_wb_als[i].logx_diag, ds_wb_als[i].logy_diag, color_line[i])


    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:4], loc='upper left', markerscale=1)
    ax.add_artist(legend2)

    ax.set_ylabel(ylabel4, fontsize=15)
    ax.set_title('ALS')
    ax.set_xlabel(r'$log_{10} \overline{\alpha}$', fontsize=15)


    for ax in hca:
        ax.grid()

    if savefig == True: plt.savefig(f'{path_save_img}/{string}.png', dpi=250, format='png', bbox_inches='tight')


def plot_alpha_reg_all(ds_vb, ds_wb, ds_vb_no_ri, ds_wb_no_ri, ylabel1, ylabel2, path_save_img, savefig, string):
    hca, hcb = pyic.arrange_axes(1, 2, plot_cb=False, asp=1, fig_size_fac=2.6, sharex=True)
    colors = ['gray', 'tab:blue', 'tab:orange']
    ii     = -1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax = plot_errorbars(ax, ds_vb.logx_diag, ds_vb.logy_diag, colors[0])
    ax = plot_errorbars(ax, ds_vb.logx_fox, ds_vb.logy_fox, colors[1])
    ax = plot_errorbars(ax, ds_vb.logx_stone, ds_vb.logy_stone, colors[2])

    ax = plot_regression_line(ax, ds_vb.logx_diag, ds_vb.logy_diag, colors[0])
    ax = plot_regression_line(ax, ds_vb.logx_fox, ds_vb.logy_fox, colors[1])
    ax = plot_regression_line(ax, ds_vb.logx_stone, ds_vb.logy_stone, colors[2])

    ax.set_ylabel(ylabel1, fontsize=15)

    ax = plot_errorbars(ax, ds_vb_no_ri.logx_diag, ds_vb_no_ri.logy_diag, colors[0])
    ax = plot_errorbars(ax, ds_vb_no_ri.logx_fox, ds_vb_no_ri.logy_fox, colors[1])
    ax = plot_errorbars(ax, ds_vb_no_ri.logx_stone, ds_vb_no_ri.logy_stone, colors[2])

    ax = plot_regression_line(ax, ds_vb_no_ri.logx_diag, ds_vb_no_ri.logy_diag, colors[0])
    ax = plot_regression_line(ax, ds_vb_no_ri.logx_fox, ds_vb_no_ri.logy_fox, colors[1])
    ax = plot_regression_line(ax, ds_vb_no_ri.logx_stone, ds_vb_no_ri.logy_stone, colors[2])

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax = plot_errorbars(ax, ds_wb.logx_diag, ds_wb.logy_diag, colors[0], label='diag.')
    ax = plot_errorbars(ax, ds_wb.logx_fox, ds_wb.logy_fox, colors[1], label='PER')
    ax = plot_errorbars(ax, ds_wb.logx_stone, ds_wb.logy_stone, colors[2], label='ALS')
    
    h,l = ax.get_legend_handles_labels()
    legend2 = ax.legend(l[:4], loc='upper left', markerscale=1)
    ax.add_artist(legend2)

    ax = plot_regression_line(ax, ds_wb.logx_diag, ds_wb.logy_diag, colors[0])
    ax = plot_regression_line(ax, ds_wb.logx_fox, ds_wb.logy_fox, colors[1])
    ax = plot_regression_line(ax, ds_wb.logx_stone, ds_wb.logy_stone, colors[2])

    ax.set_ylabel(ylabel2, fontsize=15)
    ax.set_xlabel(r'$log_{10} \overline{\alpha}$', fontsize=15)

    ax = plot_errorbars(ax, ds_wb_no_ri.logx_diag, ds_wb_no_ri.logy_diag, colors[0], label='diag.')
    ax = plot_errorbars(ax, ds_wb_no_ri.logx_fox, ds_wb_no_ri.logy_fox, colors[1], label='PER')
    ax = plot_errorbars(ax, ds_wb_no_ri.logx_stone, ds_wb_no_ri.logy_stone, colors[2], label='ALS')

    ax = plot_regression_line(ax, ds_wb_no_ri.logx_diag, ds_wb_no_ri.logy_diag, colors[0])
    ax = plot_regression_line(ax, ds_wb_no_ri.logx_fox, ds_wb_no_ri.logy_fox, colors[1])
    ax = plot_regression_line(ax, ds_wb_no_ri.logx_stone, ds_wb_no_ri.logy_stone, colors[2])


    for ax in hca:
        ax.grid()

    if savefig == True: plt.savefig(f'{path_save_img}/{string}.png', dpi=250, format='png', bbox_inches='tight')



def plot_heatmap_log(xdata, ydata, xlabel, ylabel, path, savefig, string):
    # Flatten the arrays
    xdata_flat = xdata.flatten()
    ydata_flat = ydata.flatten()

    # Remove NaN values
    mask        = ~np.isnan(xdata_flat) & ~np.isnan(ydata_flat)
    xdata_clean = xdata_flat[mask]
    ydata_clean = ydata_flat[mask]

    if xdata_clean.size == 0 or ydata_clean.size == 0:
        raise ValueError("Cleaned data arrays are empty. Cannot create heatmap.")
    
    #2dhistogram
    H, x_edge, y_edge = np.histogram2d(np.log10(xdata_clean), np.log10(ydata_clean), bins=100)

    fig, ax = plt.subplots(1, 1, figsize=(8, 8))

    cb = ax.pcolormesh(10**x_edge, 10**y_edge, H.T, cmap='Spectral_r')

    # h = ax.hist2d(np.log10(xdata_clean), np.log10(ydata_clean),  bins=100, cmap='Spectral_r')
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.grid()
    # plt.colorbar(h[3], ax=ax)
    # ax.set_aspect('equal', adjustable='box')
    # plot diagonal
    # xlim = ax.get_xlim()
    # x = np.linspace(xlim[0], xlim[1]*1000, 100)
    # y = x**(1)#*1e-3
    # ax.plot(np.log10(x), np.log10(y), 'k-', label=r'$y=x$')
    # y= x**(2)#*1e-3
    # ax.plot(np.log10(x), np.log10(y), 'b-', label=r'$y=x^2$')

    #plot diagonal
    
    plt.legend(ax.get_legend_handles_labels()[1], loc='upper left')

    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    ax.set_title('Heatmap')

    if savefig:
        plt.savefig(f'{path}/heatmap_{string}.png', dpi=300)


def plot_heatmap(xdata, ydata, xlabel, ylabel, path, savefig, string):

    # Flatten the arrays
    xdata_flat = xdata.flatten()
    ydata_flat = ydata.flatten()

    # Remove NaN values
    mask        = ~np.isnan(xdata_flat) & ~np.isnan(ydata_flat)
    xdata_clean = xdata_flat[mask]
    ydata_clean = ydata_flat[mask]

    if xdata_clean.size == 0 or ydata_clean.size == 0:
        raise ValueError("Cleaned data arrays are empty. Cannot create heatmap.")

    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    h = ax.hist2d(xdata_clean, ydata_clean,  bins=1000, cmap='Spectral_r', norm=colors.LogNorm())
    # ax.set_xscale('log')
    # ax.set_yscale('log')
    plt.grid()
    plt.colorbar(h[3], ax=ax)
    # ax.set_xlim(0,20)
    # ax.set_ylim(0,500)
    # ax.set_aspect('equal', adjustable='box')
    # # plot diagonal
    # xlim = ax.get_xlim()
    # x = np.linspace(xlim[0], xlim[1]*1000, 100)
    # y = x**(1)#*1e-3
    # ax.plot(x, y, 'k-', label=r'$y=x$')
    # y= x**(2)#*1e-3
    # ax.plot(x, y, 'b-', label=r'$y=x^2$')

    #plot diagonal
    
    plt.legend(ax.get_legend_handles_labels()[1], loc='upper left')

    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    ax.set_title('Heatmap')

    if savefig:
        plt.savefig(f'{path}/heatmap_{string}.png', dpi=300)

def mask_ri(ds_param, ri=None):
    ds_sel = ds_param.copy()
    if ri is None:
        ri = ds_sel.ri_diag.max()
    mask_ri = ds_sel.ri_diag.where(ds_sel.ri_diag < ri, np.nan)
    mask_ri = mask_ri.where(np.isnan(mask_ri), 1)
    mask_ri = mask_ri.rename('None')
    mask_ri = mask_ri.rename(None)
    for var in ds_sel.data_vars:
        ds_sel[var] = ds_sel[var] * mask_ri #* mask_vb
    return ds_sel