import numpy as np
from scipy import stats
from matplotlib import pyplot as plt


def linear_regression(x, y):
    """
    linear regression for y_i = a0 + a1 * x_i + e_i
    Following notation and formulas from: 
    Statistical Analysis in Climate Research Chapter 8, Hans von Storch, Francis W. Zwiers, 1999
    X: predictor variable
    Y: response variable
    """
    x_mean      = np.mean(x)
    y_mean      = np.mean(y)
    n           = len(x)
    SXY         = np.sum((x-x_mean)*(y-y_mean))         # Covariance of X and Y
    SXX         = np.sum((x-x_mean)*(x-x_mean))         # Variance of X
    SYY         = np.sum((y-y_mean)*(y-y_mean))         # Variance of Y
    a1          = SXY/SXX                               # slope
    a0          = y_mean - a1 * x_mean                  # intercept
    SSE         = np.sum((y - a0 - a1 * x)**2)          # sum of squared errors
    SSR         = a1 * SXY                              # sum of squares due to regression
    SST         = SSR + SSE                             # total sum of squares
    R2          = SSR/SST                               # coefficent of determination "interpretation is that it is an estimate of the model ability to specify unrealized values of the response variable Y. A value of 1 indicates that the model perfectly predicts all responses, while a value of 0 indicates no linear relationship between the response and predictor variables."
    sigma_e     = np.sqrt(SSE/(n-2))                    # standard deviation of the error
    rXY         = SXY/np.sqrt(SXX*SYY)                  # correlation coefficient: a coefficient of correlation close to 1 indicates a strong positive linear relationship while a value close to 0 indicates no relationship. A value close to -1 indicates a strong negative linear relationship.

    # test of the slope  parameter: Null hypothesis: a1^* = 0
    t           = a1 / (sigma_e / np.sqrt(SXX))         # t-statistic for the slope: a value of t close to 0 indicates that the slope parameter is not significantly different from 0

    # pvalue for the slope parameter
    pvalue      = 2 * (1 - stats.t.cdf(np.abs(t), n-2)) # p-value for the slope

    # Standard error of the estimated slope
    SE_a1       = sigma_e / np.sqrt(SXX)                # standard error of the slope

    q            = stats.t.ppf(0.975, n-2)               # quantile for t(n-2) with 95% probability

    # A confidence interval for the slope   
    CI_min_slope = a1 - q * sigma_e / np.sqrt(SXX)       # confidence interval for slope
    CI_max_slope = a1 + q * sigma_e / np.sqrt(SXX)
    a0_min       = y_mean - CI_min_slope * x_mean        # confidence interval for the intercept
    a0_max       = y_mean - CI_max_slope * x_mean        # confidence interval for the intercept

    # A confidence interval for the mean of the response variable
    sigma_mu_y_mean  = sigma_e * sigma_e * np.sqrt(1/n + (x-x_mean)**2/SXX)
    CI_min_mu_y_mean = - q * np.sqrt(sigma_mu_y_mean)
    CI_max_mu_y_mean = + q * np.sqrt(sigma_mu_y_mean)

    # A confidence interval for the response variable
    sigma_mu_y  = sigma_e * sigma_e * np.sqrt(1 + 1/n + (x-x_mean)**2/SXX)
    CI_min_mu_y = - q * np.sqrt(sigma_mu_y)
    CI_max_mu_y = + q * np.sqrt(sigma_mu_y)
    # a CI of 95% means that if we were to repeat the experiment many times, 95% of the time the CI would contain the true value of the parameter.

    print(f'a1: {a1:.2f} a0: {a0:.2f} sigma_e: {sigma_e:.2f} R2: {R2:.4f} rXY: {rXY:.4f} quantile: {q:.4f} \n t: {t:.4f} pvalue: {pvalue:.6f} SE_a1: {SE_a1:.4f}')

    return a1, a0, sigma_e, rXY, R2, CI_min_slope, CI_max_slope, a0_min, a0_max, CI_min_mu_y, CI_max_mu_y, CI_min_mu_y_mean, CI_max_mu_y_mean

# repeat the above code but with weighted least squares
def weighted_linear_regression(x, y, w):
    """
    weighted linear regression for y_i = a0 + a1 * x_i + e_i
    Following notation and formulas from: 
    Statistical Analysis in Climate Research Chapter 8, Hans von Storch, Francis W. Zwiers, 1999
    X: predictor variable
    Y: response variable
    """
    x_mean      = np.mean(x)
    y_mean      = np.mean(y)
    n           = len(x)
    SXY         = np.sum((x-x_mean)*(y-y_mean))         # Covariance of X and Y
    SXX         = np.sum((x-x_mean)*(x-x_mean))         # Variance of X
    SYY         = np.sum((y-y_mean)*(y-y_mean))         # Variance of Y
    a1          = SXY/SXX                               # slope
    a0          = y_mean - a1 * x_mean                  # intercept
    SSE         = np.sum(w*w*(y - a0 - a1 * x)**2)          # sum of squared errors
    SSR         = a1 * SXY                              # sum of squares due to regression
    SST         = SSR + SSE                             # total sum of squares
    R2          = SSR/SST                               # coefficent of determination "interpretation is that it is an estimate of the model ability to specify unrealized values of the response variable Y. A value of 1 indicates that the model perfectly predicts all responses, while a value of 0 indicates no linear relationship between the response and predictor variables."
    sigma_e     = np.sqrt(SSE/(n-2))                    # standard deviation of the error
    rXY         = SXY/np.sqrt(SXX*SYY)                  # correlation coefficient: a coefficient of correlation close to 1 indicates a strong positive linear relationship while a value close to 0 indicates no relationship. A value close to -1 indicates a strong negative linear relationship.

    # test of the slope  parameter: Null hypothesis: a1^* = 0
    t           = a1 / (sigma_e / np.sqrt(SXX))         # t-statistic for the slope: a value of t close to 0 indicates that the slope parameter is not significantly different from 0

    # pvalue for the slope parameter
    pvalue      = 2 * (1 - stats.t.cdf(np.abs(t), n-2)) # p-value for the slope

    # Standard error of the estimated slope
    SE_a1       = sigma_e / np.sqrt(SXX)                # standard error of the slope

    q            = stats.t.ppf(0.975, n-2)               # quantile for t(n-2) with 95% probability

    # A confidence interval for the slope   
    CI_min_slope = a1 - q * sigma_e / np.sqrt(SXX)       # confidence interval for slope
    CI_max_slope = a1 + q * sigma_e / np.sqrt(SXX)
    a0_min       = y_mean - CI_min_slope * x_mean        # confidence interval for the intercept
    a0_max       = y_mean - CI_max_slope * x_mean        # confidence interval for the intercept

    # A confidence interval for the mean of the response variable
    sigma_mu_y_mean  = sigma_e * sigma_e * np.sqrt(1/n + (x-x_mean)**2/SXX)
    CI_min_mu_y_mean = - q * np.sqrt(sigma_mu_y_mean)
    CI_max_mu_y_mean = + q * np.sqrt(sigma_mu_y_mean)

    # A confidence interval for the response variable
    sigma_mu_y  = sigma_e * sigma_e * np.sqrt(1 + 1/n + (x-x_mean)**2/SXX)
    CI_min_mu_y = - q * np.sqrt(sigma_mu_y)
    CI_max_mu_y = + q * np.sqrt(sigma_mu_y)
    # a CI of 95% means that if we were to repeat the experiment many times, 95% of the time the CI would contain the true value of the parameter.

    print(f'a1: {a1:.2f} a0: {a0:.2f} sigma_e: {sigma_e:.2f} R2: {R2:.4f} rXY: {rXY:.4f} quantile: {q:.4f} \n t: {t:.4f} pvalue: {pvalue:.6f} SE_a1: {SE_a1:.4f}')

    return a1, a0, sigma_e, rXY, R2, CI_min_slope, CI_max_slope, a0_min, a0_max, CI_min_mu_y, CI_max_mu_y, CI_min_mu_y_mean, CI_max_mu_y_mean

def linear_regression_matrix_test(x, y, sigma_sq=np.array([1])):
    """
    linear regression for Y +-E = A * X 
    X: predictor variable
    Y: response variable
    sigma: individual standard deviation of the response variable
    """
    if len(sigma_sq) == int(1): print('sigma is None or 1, the data is not normalized') # type: ignore
    else: print('data is normalized with sigma_sq')

    n          = len(x)                     # number of data points
    A          = np.ones((n,2))             # Matrix A design
    A[:,0]     = x/sigma_sq                    # first column of A is the predictor variable normalized by the standard deviation
    A[:,1]     = np.ones(n)/sigma_sq           # second column of A is a vector of ones normalized by the standard deviation
    d          = y/sigma_sq                    # response variable normalized by the standard deviation
    AT         = np.transpose(A)
    ATA        = np.dot(AT,A)               # A^T * A # matrix of the normal equations
    ATA_inv    = np.linalg.inv(ATA)         # covariance of X 
    ATA_inv_AT = np.dot(ATA_inv,AT)
    X          = np.dot(ATA_inv_AT,d)       # X = (A^T * A)^-1 * A^T * d # sloepe and intercept of the linear regression

    err_covX = np.sqrt(np.diag(ATA_inv))    # standard deviation of the slope and intercept

    print(f'SSE {np.sum((y - X[0]*x - X[1])**2)}')
    sigma_e    = np.sqrt(np.sum((y - X[0]*x - X[1])**2)/(n-2))   # standard deviation of the error rms_fit

    Delta = np.sum(x**2/sigma_sq**2) * np.sum(1/sigma_sq**2) - np.sum(x/sigma_sq**2) * np.sum(x/sigma_sq**2) # determinant of the matrix A^T * A
    sigma_a_sq = np.sum(1/sigma_sq**2) / Delta # variance of the slope
    sigma_b_sq = np.sum(x**2/sigma_sq**2) / Delta # variance of the intercept
    sigma_ab_sq = - np.sum(x/sigma_sq**2) / Delta # covariance of the slope and intercept
    sigma_y_sq = sigma_a_sq * np.sum(x**2) + sigma_b_sq + 2 * sigma_ab_sq * np.sum(x) # variance of the response variable
    x_min      = - sigma_ab_sq / sigma_a_sq # minimum of the parabola: That is, the “center of gravity” of the data is the location at which the linear regression has the smallest error
    print(f'sigma y: {sigma_y_sq:.2f}')

    R2         = 1 - np.sum((y - X[0]*x - X[1])**2)/np.sum((y - np.mean(y))**2) # coefficent of determination "interpretation is that it is an estimate of the model ability to specify unrealized values of the response variable Y. A value of 1 indicates that the model perfectly predicts all responses, while a value of 0 indicates no linear relationship between the response and predictor variables."
    rXY        = np.sum((x - np.mean(x))*(y - np.mean(y)))/(np.sqrt(np.sum((x - np.mean(x))**2))*np.sqrt(np.sum((y - np.mean(y))**2))) # correlation coefficient: a coefficient of correlation close to 1 indicates a strong positive linear relationship while a value close to 0 indicates no relationship. A value close to -1 indicates a strong negative linear relationship.

    # confidence interval for the response variable
    q            = stats.t.ppf(0.975, n-2)               # quantile for t(n-2) with 95% probability
    x_mean = np.mean(x)
    SXX    = np.sum((x - x_mean)**2)
    sigma_mu_y  = sigma_e * sigma_e * np.sqrt(1 + 1/n + (x-x_mean)**2/SXX)
    # CI_min_mu_y = - q * np.sqrt(sigma_mu_y)
    # CI_max_mu_y = + q * np.sqrt(sigma_mu_y)
    CI_delta_mu_y = q * np.sqrt(sigma_mu_y)

    #print(f' sigma_mu_y: {sigma_mu_y} \n sigma y_sq: {sigma_y_sq}')

    print(f'X: {X} err_covX: {err_covX} sigma_e: {sigma_e:.2f} \n R2: {R2:.4f} rXY: {rXY:.4f}')
    return X, ATA_inv_AT, ATA_inv, ATA, A, CI_delta_mu_y

def linear_regression_matrix(x, y, sigma):
    """
    linear regression for Y +-E = A * X 
    von Storch and Zwiers, 1999, p. 184
    X: predictor variable
    Y: response variable
    sigma: individual standard deviation of the response variable
    """
    # sigma=1
    n          = len(x)                     # number of data points
    Xi         = np.ones((n,2))             # Matrix A design
    Xi[:,0]    = 1/sigma                    # second column of A is a vector of ones normalized by the standard deviation
    Xi[:,1]    = x/sigma                    # first column of A is the predictor variable normalized by the standard deviation
    Y          = y/sigma                    # response variable normalized by the standard deviation
    XTX        = np.dot(np.transpose(Xi),Xi)               # A^T * A # matrix of the normal equations
    XTX_inv    = np.linalg.inv(XTX) # covariance of a
    XTX_inv_XT = np.dot(XTX_inv,np.transpose(Xi))
    a          = np.dot(XTX_inv_XT,Y)       # X = (A^T * A)^-1 * A^T * d # sloepe and intercept of the linear regression
    YT         = np.transpose(Y)
    I          = np.identity(n)
    # SSE        = np.dot(YT,(I - np.dot(Xi,XTX_inv_XT)),Y)
    IX         = (I - np.dot(Xi,XTX_inv_XT))
    # left to right
    YTIX       = np.dot(YT,IX)
    SSE        = np.dot(YTIX,Y)
    print(f'SSE: {SSE}')
    # right to left
    # IXY        = np.dot(IX,Y)
    # SSE        = np.dot(YT,IXY)


    U          = I /n
    XU        = np.dot(Xi,XTX_inv_XT) - U
    # left to right
    YTXU      = np.dot(YT,XU)
    SSR       = np.dot(YTXU,Y)
    # right to left
    # XUY      = np.dot(XU,Y)
    # SSR       = np.dot(YT,XUY)

    IU       = I - U
    # left to right
    YTIU     = np.dot(YT,IU)
    SST      = np.dot(YTIU,Y)
    # right to left
    # IUY      = np.dot(IU,Y)
    # SST      = np.dot(YT,IUY)

    R2          = SSR/SST                               # coefficent of determination "interpretation is that it is an estimate of the model ability to specify unrealized values of the response variable Y. A value of 1 indicates that the model perfectly predicts all responses, while a value of 0 indicates no linear relationship between the response and predictor variables."
    sigma_e     = np.sqrt(SSE/(n-2))                    # standard deviation of the error
    
    print(f'a: {a} \n cov(a): {XTX_inv} SSE: {SSE} \n SSR: {SSR} \n SST: {SST} \n R2: {R2} \n sigma_e: {sigma_e}')
    # Confidence intervals 
    a1         = a[1]
    a0         = a[0]
    x_mean     = np.mean(x)
    y_mean     = np.mean(y)
    SXX        = np.sum((x - x_mean)**2)
    SYY        = np.sum((y - y_mean)**2)
    SXY        = np.sum((x - x_mean)*(y - y_mean))
    # SXX        = XTX_inv[0,0]
    # SYY        = XTX_inv[1,1]
    # SXY        = XTX_inv[0,1]
    rXY         = SXY/np.sqrt(SXX*SYY) # correlation coefficient: a coefficient of correlation close to 1 indicates a strong positive linear relationship while a value close to 0 indicates no relationship. A value close to -1 indicates a strong negative linear relationship.

    # Standard error of the estimated slope
    SE_a1       = sigma_e / np.sqrt(SXX)                # standard error of the slope

    q            = stats.t.ppf(0.975, n-2)               # quantile for t(n-2) with 95% probability

    # A confidence interval for the slope   
    CI_min_slope = a1 - q * sigma_e / np.sqrt(SXX)       # confidence interval for slope
    CI_max_slope = a1 + q * sigma_e / np.sqrt(SXX)
    a0_min       = y_mean - CI_min_slope * x_mean        # confidence interval for the intercept
    a0_max       = y_mean - CI_max_slope * x_mean        # confidence interval for the intercept
    # a0_min       = a0 - q * sigma_e * np.sqrt(np.sum(x**2))/np.sqrt(SXX)        # confidence interval for the intercept
    # a0_max       = a0 + q * sigma_e * np.sqrt(np.sum(x**2))/np.sqrt(SXX)        # confidence interval for the intercept

    # A confidence interval for the intercept
    CI_min_intercept = - q * sigma_e * np.sqrt(np.sum(x**2))/np.sqrt(SXX)

    # A confidence interval for the mean of the response variable
    sigma_mu_y_mean  = sigma_e * sigma_e * np.sqrt(1/n + (x-x_mean)**2/SXX)
    CI_min_mu_y_mean = - q * np.sqrt(sigma_mu_y_mean)
    CI_max_mu_y_mean = + q * np.sqrt(sigma_mu_y_mean)

    # A confidence interval for the response variable
    sigma_mu_y  = sigma_e * sigma_e * np.sqrt(1 + 1/n + (x-x_mean)**2/SXX)
    CI_min_mu_y = - q * np.sqrt(sigma_mu_y)
    CI_max_mu_y = + q * np.sqrt(sigma_mu_y)
    # a CI of 95% means that if we were to repeat the experiment many times, 95% of the time the CI would contain the true value of the parameter.
    print(sigma_e)
    print(f'a1: {a1:.2f} a0: {a0:.2f} ')
    return  a1, a0, sigma_e, rXY, R2, CI_min_slope, CI_max_slope, a0_min, a0_max, CI_min_mu_y, CI_max_mu_y, CI_min_mu_y_mean, CI_max_mu_y_mean


def plot_regression(x, y, filename, path, savefig):
    a1, a0, sigma_e, rXY, R2, CI_min, CI_max, a0_min, a0_max, CI_min_mu_y, CI_max_mu_y, CI_min_mu_y_mean, CI_max_mu_y_mean = linear_regression(x, y)

    plt.figure(figsize=(10,10))
    plt.scatter(x, y, s=3, label='data')
    plt.plot(x, a0 + a1*x, 'blue', label=f': s={a1:.2f} i={a0:.2} se:{sigma_e:.2} rXY:{rXY:.4f} R2:{R2:.4f}  ')  # type: ignore
    plt.plot(x, CI_min *x + a0_min, 'black', ls=':', label=f'0.95% CI for Slope (inference for intercept)')  # type: ignore
    plt.plot(x, CI_max *x + a0_max, 'black', ls=':')  # type: ignore
    plt.plot(x, a0 + a1 *x + CI_min_mu_y,  color='red', label=f'0.95% Ci for response variable')  
    plt.plot(x, a0 + a1 *x + CI_max_mu_y,  color='red')  
    plt.scatter(x, a0 + a1 *x + CI_min_mu_y_mean, s=1, color='purple', label=f'0.95% Ci for mean of response variable')  
    plt.scatter(x, a0 + a1 *x + CI_max_mu_y_mean, s=1, color='purple')  
    plt.grid()
    plt.legend()
    plt.xlabel('log(x)')
    plt.ylabel('log(y)')
    # plt.ylim([-4, 2])
    if savefig==True:
        plt.savefig(path + filename + '.png', dpi=150)

def plot_regression_binned(x, y, sigma):
    # weights = 1/sigma #supposedly proportional to 1/sigma 
    # weights = sigma/sigma.max()
    # a1, a0, sigma_e, rXY, R2, CI_min_slope, CI_max_slope, a0_min, a0_max, CI_min_mu_y, CI_max_mu_y, CI_min_mu_y_mean, CI_max_mu_y_mean = weighted_linear_regression(x, y, sigma)

    a1, a0, sigma_e, rXY, R2, CI_min_slope, CI_max_slope, a0_min, a0_max, CI_min_mu_y, CI_max_mu_y, CI_min_mu_y_mean, CI_max_mu_y_mean = linear_regression_matrix(x, y, sigma)

    plt.figure(figsize=(10,10))
    lw=4
    # plt.scatter(x, y, s=3, label='data')
    plt.errorbar(x, y, yerr=sigma, fmt='o', markersize=3, capsize=3, elinewidth=0.5, color='black', label='data + bin_stds')
    plt.plot(x, a0 + a1*x, 'blue', lw=lw, label=f': s={a1:.2f} i={a0:.2} se:{sigma_e:.2} rXY:{rXY:.4f} R2:{R2:.4f}  ')  # type: ignore
    # plt.plot(x, a0 + a1*x, 'blue', label=f': s={a1:.2f} i={a0:.2}  ')  # type: ignore

    plt.plot(   x,    a0_min + CI_min_slope *x , 'black', lw=1, label=f'0.95% CI for Slope (inference for intercept)')  # type: ignore
    plt.plot(   x,    a0_max + CI_max_slope *x , 'black', lw=1)  # type: ignore
    plt.plot(   x,    a0 + a1 *x + CI_min_mu_y,  color='red', lw=lw, label=f'0.95% Ci for response variable')  
    plt.plot(   x,    a0 + a1 *x + CI_max_mu_y,  color='red', lw=lw)  
    plt.scatter(x, a0 + a1 *x + CI_min_mu_y_mean, s=3, color='purple', lw=lw, label=f'0.95% Ci for mean of response variable')  
    plt.scatter(x, a0 + a1 *x + CI_max_mu_y_mean, s=3, color='purple', lw=lw)  
    plt.grid()
    plt.legend()
    plt.xlabel('log(x)')
    plt.ylabel('log(y)')
    # plt.ylim([-4, 2])

def calc_bin_means(x, y, N):
    bin_means, bin_edges,  binnumber = stats.binned_statistic(x, y, statistic='mean', bins=N)
    bin_stds, bin_edges,   binnumber = stats.binned_statistic(x, y, statistic='std', bins=N)
    bin_counts, bin_edges, binnumber = stats.binned_statistic(x, y, statistic='count', bins=N)

    bin_width = (bin_edges[1] - bin_edges[0])
    bin_centers = bin_edges[1:] - bin_width/2
    # remove bind with bin counts less n
    n           = 20
    bin_means   = bin_means[bin_counts > n]
    bin_stds    = bin_stds[bin_counts > n]
    bin_centers = bin_centers[bin_counts > n]
    idx = bin_counts < n
    # get inedx of bins with counts less than n
    numbers = np.arange(len(bin_counts))

    print(f'bins with counts less than {n} points: \n idx: {numbers[idx]} \n n:{bin_counts[idx]}')
    bin_counts  = bin_counts[bin_counts > n]
    
    return bin_means, bin_stds, bin_counts, bin_centers


def linear_regression_matrix_simple(x, y, sigma_sq=np.array([1])):
    """
    linear regression for Y +-E = A * X 
    X: predictor variable
    Y: response variable
    sigma: individual standard deviation of the response variable
    """
    if len(sigma_sq) == int(1): print('sigma is None or 1, the data is not normalized') # type: ignore
    else: print('data is normalized with sigma_sq')

    n          = len(x)                     # number of data points
    A          = np.ones((n,2))             # Matrix A design
    A[:,0]     = x/sigma_sq                    # first column of A is the predictor variable normalized by the standard deviation
    A[:,1]     = np.ones(n)/sigma_sq           # second column of A is a vector of ones normalized by the standard deviation
    d          = y/sigma_sq                    # response variable normalized by the standard deviation
    AT         = np.transpose(A)
    ATA        = np.dot(AT,A)               # A^T * A # matrix of the normal equations
    ATA_inv    = np.linalg.inv(ATA)         # covariance of X 
    ATA_inv_AT = np.dot(ATA_inv,AT)
    X          = np.dot(ATA_inv_AT,d)       # X = (A^T * A)^-1 * A^T * d # sloepe and intercept of the linear regression

    err_covX = np.sqrt(np.diag(ATA_inv))    # standard deviation of the slope and intercept

    # print(f'SSE {np.sum((y - X[0]*x - X[1])**2)}')
    sigma_e    = np.sqrt(np.sum((y - X[0]*x - X[1])**2)/(n-2))   # standard deviation of the error rms_fit

    R2         = 1 - np.sum((y - X[0]*x - X[1])**2)/np.sum((y - np.mean(y))**2) # coefficent of determination "interpretation is that it is an estimate of the model ability to specify unrealized values of the response variable Y. A value of 1 indicates that the model perfectly predicts all responses, while a value of 0 indicates no linear relationship between the response and predictor variables."
    rXY        = np.sum((x - np.mean(x))*(y - np.mean(y)))/(np.sqrt(np.sum((x - np.mean(x))**2))*np.sqrt(np.sum((y - np.mean(y))**2))) # correlation coefficient: a coefficient of correlation close to 1 indicates a strong positive linear relationship while a value close to 0 indicates no relationship. A value close to -1 indicates a strong negative linear relationship.

    # confidence interval for the response variable
    q          = stats.t.ppf(0.975, n-2)               # quantile for t(n-2) with 95% probability
    # print('q', q)
    # x_mean     = np.mean(x)
    # SXX        = np.sum((x - x_mean)**2)
    # sigma_mu_y = sigma_e * sigma_e * np.sqrt(1 + 1/n + (x-x_mean)**2/SXX)
    # CI         = q * np.sqrt(sigma_mu_y)
    # print(f'CI: {CI}')

    CI = q * np.sqrt(np.diag(ATA_inv))              # confidence interval for the slope and intercept: including normalization
    # print(f'CI: {CI}')

    #print(f' sigma_mu_y: {sigma_mu_y} \n sigma y_sq: {sigma_y_sq}')

    # print(f'X: {X} err_covX: {err_covX} sigma_e: {sigma_e:.2f} \n R2: {R2:.4f} rXY: {rXY:.4f}')
    # print('returns CI from Intercept')
    return X, err_covX, CI[1], R2, rXY, sigma_e


def plot_two_slopes(bin_centers, bin_means, bin_stds):
    plt.figure(figsize=(10,10))
    ri_idx = 1
    for bin_idx in [bin_centers < ri_idx, bin_centers > ri_idx]:
        x = bin_centers[bin_idx]
        y = bin_means[bin_idx]
        X, err_covX, CI, R2, rXY, sigma_e = linear_regression_matrix_simple(bin_centers[bin_idx], bin_means[bin_idx], bin_stds[bin_idx])
        # X, err_covX, CI, R2, rXY, sigma_e = mylinreg.linear_regression_matrix_simple(bin_centers[bin_idx], bin_means[bin_idx])
        a1 = X[0]
        a0 = X[1]
        print(f'a1: {a1}, a0: {a0}, CI: {CI}')

        # plt.scatter(logx, logy, s=3, label='data')
        plt.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', color='black')
        plt.plot(x, a0 + a1*x, 'blue', label=f'normalized: a1; a0: {X}+/-{err_covX} \n R2: {R2:.2f}, rXY: {rXY:.2f}, sigma_e: {sigma_e:.2f}')
        plt.fill_between(x, a0 + CI + a1*x, a0 - CI + a1*x, color='blue', alpha=0.2)
    plt.legend()
    plt.grid()

def plot_two_slopes_AI(bin_centers, bin_means, bin_stds):
    plt.figure(figsize=(10,10))
    ri_idx = 1
    for bin_idx in [bin_centers < ri_idx, bin_centers > ri_idx]:
        x = bin_centers[bin_idx]
        y = bin_means[bin_idx]

        y_err = bin_stds[bin_idx]

        # Define the weights
        weights = 1 / y_err**2

        # Perform weighted linear regression
        X = np.vstack([x, np.ones(len(x))]).T
        w = np.diag(weights)
        cov = np.linalg.inv(X.T @ w @ X)
        a1, a0 = cov @ X.T @ w @ y

        # Calculate the confidence interval
        # t = 2.306  # 95% confidence interval for 10 degrees of freedom
        t = 1.98
        CI = t * np.sqrt(np.diag(cov))

        # Plot the results
        plt.errorbar(x, y, yerr=y_err, fmt='o', color='black')
        plt.plot(x, a0 + a1*x, 'red', label='weighted linear regression')
        plt.fill_between(x, a0 + CI[1] + a1*x, a0 - CI[1] + a1*x, color='red', alpha=0.2)
        plt.legend()
        plt.grid()
        print(f'a1: {a1}, a0: {a0}, CI: {CI}')

def plot_normalized_vs_non_normalized(bin_centers, bin_means, bin_stds):
    """
    slopes should be correctly computed;
    confidence interval for the response variable: 
    most likely not correct, since mean values do not included normalization...;
    see calculation of CI from covariance in linear_regression_matrix_simple()
    """
    X, ATA_inv_AT, ATA_inv, ATA, A, CI = linear_regression_matrix_test(bin_centers, bin_means, bin_stds)

    a1 = X[0]
    a0 = X[1]
    x=bin_centers
    y=bin_means

    plt.figure(figsize=(10,10))
    # plt.scatter(x, y, s=3, label='data')
    plt.errorbar(bin_centers, bin_means, yerr=bin_stds, fmt='o', color='black')
    plt.plot(x, a0 + a1*x, 'blue', label='normalized: ')
    plt.plot(x, a0 + CI + a1*x, 'blue')
    plt.plot(x, a0 - CI + a1*x, 'blue')
    X, ATA_inv_AT, ATA_inv, ATA, A, CI = linear_regression_matrix_test(bin_centers, bin_means)

    a1 = X[0]
    a0 = X[1]
    x=bin_centers
    y=bin_means

    plt.plot(x, a0 + a1*x, 'red',label='not normalized')
    plt.plot(x, a0 + CI + a1*x, 'red')
    plt.plot(x, a0 - CI + a1*x, 'red')
    plt.grid()
    plt.legend()
