# %%
# Evaluate slope of over MLD depth and front averaged profiles
# manually selected MLD, visualization of vertical structure function
# %%
import sys
#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")

import glob, os
import dask as da
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload
import funcs as fu

import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
# import slope_param_vs_diagnostic_inclined_funcs as hal
import smt_modules.init_slurm_cluster as scluster 
import domain_funcs as domfunc
import cartopy

# %%

savefig       = False
time_averaged = ''
fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/first_paper/overview/rossby_radius/'

fpath_ckdtree      = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
path_save_img_root = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/regional/'
if False: fpath_ckdtree = '/Users/mepke/smt_local_data/smt_res0.02_180W-180E_90S-90N.nc'
grid = xr.open_dataset(fpath_ckdtree)

# %% get cluster
reload(scluster)
client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00', wait=False)
cluster
client

# %%
mask_land = xr.open_dataset('/work/mh0033/m300878/parameterization/time_averages/mask_land.nc')
mask_land = mask_land.mask_land
mask_land = mask_land.drop('depthi').drop('time')
mask_land = mask_land.compute()
# %% Model resolution
# compute n2 on reg grid
path_to_interp_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/'
lon_reg             = np.array([-90, -10])
lat_reg             = np.array([15, 60])

gg             = eva.load_smt_grid()
res            = np.sqrt(gg.cell_area_p)
res            = res.rename(cell='ncells')
res            = res.drop('clon').drop('clat')
fpath_ckdtree2 = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
data_r         = pyic.interp_to_rectgrid_xr(res, fpath_ckdtree2, lon_reg=lon_reg, lat_reg=lat_reg) # does not work properly with xarray...?

if False:
    path_root_dat       = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
    path_data           = f'{path_root_dat}n2{time_averaged}.nc'
    chunks_i            = {'depthi': 1}
    n_mean              = xr.open_dataset(path_data, chunks=chunks_i)
    data_n2_mean_       = pyic.interp_to_rectgrid_xr(n_mean.N2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_n2_mean_.to_netcdf(f'{path_to_interp_data}/data_n2_mean_large.nc')
else:
    data_n2_mean             = xr.open_dataarray(f'{path_to_interp_data}/data_n2_mean_large.nc')

n2_sel    = data_n2_mean.compute()
n2        = n2_sel.where(~np.isinf(n2_sel),np.nan) #infs to nans

N2_interp = []
for i in np.arange(8): #split due to likely memory issues
    N2_interp.append(n2.isel(lon=slice(i*500, (i+1)*500)).interp(depthi=depthc, method='linear'))
n2_depthc = xr.concat(N2_interp, dim='lon')
n2_depthc = n2_depthc.rename(depthi='depthc')

####################################### compute rossby radius of ML #######################################
# %% load cropped and interpolated data
reload(eva)
data_rho_mean_depthi_week = xr.open_dataset('/work/mh0033/m300878/parameterization/time_averages/one_week_march/rho_1W_mean.nc', chunks=dict(depthi=1))
data_rho_mean_depthi_week = data_rho_mean_depthi_week.rho_mean.compute()
mld, mask, mldx = eva.calc_mld_xr(data_rho_mean_depthi_week, depthi, threshold=0.2)
mask_mld  = mask
mldx      = mldx * mask_land
mldx      = mldx.rename('mld')
# %% Masking land, MLD and filtering M2
n2_mask = n2 * mask_mld  #application of mask leads to new zero values
n2_mask = n2_mask.where(~(n2_mask == 0), np.nan) # remove zeros
n2_mask = n2_mask.rename('n2')

# %% # first baroclinic Rossby Radius of ML
dzw_i         = np.append(dzw, np.nan)
dzw_i         = xr.DataArray(dzw_i, dims=['depthi'])
weighted_n2 = n2_mask * dzw_i
n2_mld_mean = weighted_n2.sum(dim='depthi', skipna=True) / (dzw_i * mask_mld).sum(dim='depthi', skipna=True)
# n2_mld_mean = n2_mask.mean(dim='depthi', skipna=True)
H           = mldx
N           = np.sqrt(n2_mld_mean)
fcoriolis   = eva.calc_coriolis_parameter(n2_mask.lat)
rossby_rad  = N * H / fcoriolis / 1e3 #/ np.pi
# rossby_rad.plot(vmin=0, vmax=20)
N.plot(vmin=0.002, vmax=0.025)


# %% compute length scale of fastest growing mode
if False:
    path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
    path_data     = f'{path_root_dat}by{time_averaged}.nc'
    chunks_i      = {'depthi': 1}
    by_mean       = xr.open_dataset(path_data, chunks=chunks_i)
    data_by_mean  = pyic.interp_to_rectgrid_xr(by_mean.dbdy, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    path_data     = f'{path_root_dat}bx{time_averaged}.nc'
    bx_mean       = xr.open_dataset(path_data, chunks=chunks_i)
    data_bx_mean  = pyic.interp_to_rectgrid_xr(bx_mean.dbdx, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_M2_mean  = np.sqrt(np.power(data_bx_mean,2) + np.power(data_by_mean,2))
    data_M2_mean.to_netcdf(f'{path_to_interp_data}/data_M2_mean_large.nc')
else:
    data_M2_mean = xr.open_dataarray(f'{path_to_interp_data}/data_M2_mean_large.nc')

#%%
M2_sel = data_M2_mean.compute()
M2     = M2_sel.where(~np.isinf(M2_sel),np.nan) #infs to nans

m2_mask = M2 * mask_mld  #application of mask leads to new zero values
m2_mask = m2_mask.where(~(m2_mask == 0), np.nan) # remove zeros
m2_mask = m2_mask.rename('m2')

weighted_m2 = m2_mask * dzw_i
m2_mld_mean = weighted_m2.sum(dim='depthi', skipna=True) / (dzw_i * mask_mld).sum(dim='depthi', skipna=True)
# %%
Ri   = n2_mld_mean * fcoriolis**2 / m2_mld_mean**2
# Ri.plot(vmin=0, vmax=20, cmap='Blues_r')
L_ml = (2 * np.pi / 1.6) * np.sqrt((1+Ri)/Ri) * np.sqrt(n2_mld_mean) * H / fcoriolis / 1e3

sigma_ml = 0.3 * np.sqrt(Ri/(1+Ri)) * m2_mld_mean / np.sqrt(n2_mld_mean)

# %%
reload(fu)
fu.rossby_radius_l_ml(L_ml, data_r, lon_reg, lat_reg, fig_path, savefig)
fu.ml_growth_rate(sigma_ml, data_r, lon_reg, lat_reg, fig_path, savefig)

##############################Compute rossby radius of deformation################################
# %%
def calc_3d_land_mask():
    print('compute mask')
    dsT      = eva.load_smt_T()
    dsT_sel  = dsT.isel(time=0)
    t_interp = pyic.interp_to_rectgrid_xr(dsT_sel, fpath_ckdtree, lon_reg=lon_reg,  lat_reg=lat_reg)
    mask     = t_interp.where(t_interp==0.)
    return(mask)
# %%
def calc_topography(ds, depthc):
    print('compute topography from mask')
    """ 
    calculates topography based on depthc
    """
    sum_idx = np.sum(np.isnan(ds), axis=0)
    def func(a):
        return(depthc[a-1])
    depth = xr.apply_ufunc(func, sum_idx)
    return(depth, sum_idx)

mask = calc_3d_land_mask()
topo, sum_idx = calc_topography(mask, depthc)
topo = topo * mask_land
topo.plot(cmap='Spectral_r')

# %% compute first baroclinic rossby radius of deformation
idx = sum_idx #* mask_land
idx = idx - 1
idx = idx.astype(int)

mask_adjusted      = mask
mask_adjusted[idx] = 0
mask_used          = mask_adjusted.where(mask_adjusted==0., 1).astype(bool)

# %%
n2_mask = n2_depthc * mask_used  #application of mask leads to new zero values
n2_mask = n2_mask.where(~(n2_mask == 0), np.nan) # remove zeros
n2_mask = n2_mask.rename('n2')
# %%
dzw_c          = xr.DataArray(dzw, dims=['depthc'])
weighted_n2    = n2_mask * dzw_c
n2_mld_mean    = weighted_n2.sum(dim='depthc', skipna=True) / (dzw_c * mask_used).sum(dim='depthc', skipna=True) # topo #dzw.sum(dim='depthc', skipna=True)
N              = np.sqrt(n2_mld_mean)
H              = topo
fcoriolis      = eva.calc_coriolis_parameter(n2_mask.lat)
rossby_rad_def = N * H / fcoriolis  / 1e3 / np.pi
rossby_rad_def = rossby_rad_def * mask_land

# %%
if False:
    rossby_rad_def_c = rossby_rad_def.coarsen(lon=50, lat=50, boundary='trim').mean()
    cmap   = 'YlGnBu'
    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree()})
    ax.pcolormesh(rossby_rad_def.lon, rossby_rad_def.lat, rossby_rad_def, transform=ccrs_proj, cmap=cmap)
    # add contour lines of constant rossby radius 10 20 30
    contour = ax.contour(rossby_rad_def_c.lon, rossby_rad_def_c.lat, rossby_rad_def_c, levels=[10, 20, 30, 40 , 50 , 60, 70 ,80, 100, 150], colors='k', transform=ccrs_proj)
    ax.clabel(contour, inline=True, fontsize=8, fmt='%d')


# %%
reload(fu)
fu.rossby_radius(rossby_rad, data_r, lon_reg, lat_reg, fig_path, savefig)

# %%
data = rossby_rad/data_r
fu.rossby_radius_bias(data, data_r, lon_reg, lat_reg, fig_path, savefig)
# %%
reload(fu)
# fu.rossby_radius_res(rossby_rad, data_r, lon_reg, lat_reg, fig_path, savefig)

fu.wavelength_res(L_ml, data_r, lon_reg, lat_reg, fig_path, savefig)

# %%
reload(fu)
fu.rossby_radius_res_pi(rossby_rad, data_r, lon_reg, lat_reg, fig_path, savefig)


# %%
reload(fu)
fu.rossby_radius_res_pi_all(rossby_rad_def, rossby_rad, data_r, lon_reg, lat_reg, fig_path, savefig)

#%%
if False:

    path_ilmar = '/home/m/m300878/submesoscaletelescope/results/smt_natl/ilmar/'
    A = np.array([[-50, -40], [-40, -30], [-70, -60], [-60, -50], [-50, -40], [-70, -60], [-60, -50]])
    B = np.array([[40, 50], [40, 50], [30, 40], [30, 40], [30, 40], [20, 30], [20, 30]])
    C = np.array([A, B])

    #compute mean values in C regions
    mean_values = []
    for i in np.arange(7):
        lon_reg_s = C[0, i]
        lat_reg_s = C[1, i]
        print('region', lon_reg_s, lat_reg_s)
        data_sel = rossby_rad_def.sel(lon=slice(lon_reg_s[0], lon_reg_s[1]), lat=slice(lat_reg_s[0], lat_reg_s[1]))
        data_sel = data_sel.mean(dim='lon').mean(dim='lat')
        mean_values.append(data_sel.data)
        print('mean', data_sel.data)

    reg = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    results = np.array([reg, mean_values])
    #save
    np.save(path_ilmar+'rossby_rad_def_region_means.npy', results)
    #save C
    np.save(path_ilmar+'rossby_rad_def_regions.npy', C)

    reload(fu)
    fu.rossby_radius_deformation_ilmar(rossby_rad_def, C, lon_reg, lat_reg, path_ilmar, savefig)

    # %% for mld rb

    #compute mean values in C regions
    mean_values = []
    for i in np.arange(7):
        lon_reg_s = C[0, i]
        lat_reg_s = C[1, i]
        print('region', lon_reg_s, lat_reg_s)
        data_sel = rossby_rad.sel(lon=slice(lon_reg_s[0], lon_reg_s[1]), lat=slice(lat_reg_s[0], lat_reg_s[1]))
        data_sel = data_sel.mean(dim='lon').mean(dim='lat')
        mean_values.append(data_sel.data)
        print('mean', data_sel.data)

    reg = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    results = np.array([reg, mean_values])
    #save
    np.save(path_ilmar+'rossby_rad_ml_region_means.npy', results)
    #save C
    np.save(path_ilmar+'rossby_rad_ml_regions.npy', C)

    reload(fu)
    fu.rossby_radius_deformation_ilmar(rossby_rad, C, lon_reg, lat_reg, path_ilmar, savefig)



# %% combine MLD images
if False:
    # %% get cluster
    reload(scluster)

    client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00')

    # client, cluster = scluster.init_my_cluster()
    cluster
    client

    # %%
    reload(eva)
    reload(tools)
    ############# load data #############
    # %%
    lon_reg       = np.array([-90, -10])
    lat_reg       = np.array([15, 60])
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
    fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/first_paper/overview/'
    path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'

    time_averaged = ''
    savefig       = False

    # %% load climatologies
    argo             = eva.load_argo_climatology()
    path_mld_deBoyer = '/work/mh0033/m300878/observation/MLD/deBoyer_22/mld_dr003_ref10m.nc'
    mld_deBoyer      = xr.open_dataset(path_mld_deBoyer)

    # %% load weekly mean data
    data_rho_mean_depthi_week = xr.open_dataset('/work/mh0033/m300878/parameterization/time_averages/one_week_march/rho_1W_mean.nc', chunks=dict(depthi=1))
    data_rho_mean_depthi_week = data_rho_mean_depthi_week.rho_mean.compute()
    # %% load monthly mean data

    data_rho_mean_depthi_month = xr.open_dataset('/work/mh0033/m300878/parameterization/time_averages/month_mean/rho_1M_mean.nc', chunks=dict(depthi=1))
    data_rho_mean_depthi_month = data_rho_mean_depthi_month.rho_mean.compute()

    # %%
    mask_land = xr.open_dataset('/work/mh0033/m300878/parameterization/time_averages/mask_land.nc')
    mask_land = mask_land.mask_land

    ############################ apply mld algorithm ############################
    # %%
    reload(eva)
    mld, mask, mldx = eva.calc_mld_xr(data_rho_mean_depthi_week, depthi, threshold=0.2)
    mldx            = mldx * mask_land
    mldx            = mldx.rename('mld')
    # %%
    mld, mask, mldx_03 = eva.calc_mld_xr(data_rho_mean_depthi_month, depthi, threshold=0.03) #0.03
    mldx_03            = mldx_03 * mask_land
    mldx_03            = mldx_03.rename('mld')


    #%%
    reload(fu)
    fu.two_mld(mldx_03, mld_deBoyer, lon_reg, lat_reg, fig_path, savefig)
    # %%
    reload(fu)
    fu.rossby_radius_res_mld(rossby_rad/1e3, data_r, mldx_03, mld_deBoyer, lon_reg, lat_reg, fig_path, savefig)

    # %%
