# %%
# Evaluate slope of over MLD depth and front averaged profiles
# manually selected MLD, visualization of vertical structure function
# %%
import sys

#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import dask as da

import pandas as pd
import netCDF4 as nc
import xarray as xr    
#from dask.diagnostics import ProgressBar
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload
from matplotlib.ticker import FormatStrFormatter
from scipy import stats    #Used for 2D binned statistics
from scipy.ndimage.interpolation import rotate

import slope_param_vs_diagnostic_inclined_funcs as hal
import domain_funcs as domfunc



# %%
grain        = False
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
# fpath_ckdtree = '/Users/mepke/smt_local_data/smt_res0.02_180W-180E_90S-90N.nc'

time_averaged = ''
path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
savefig       = False
# path_save_img = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/ri_vs_wb_regional/'
# path_save_img = '/Users/mepke/Documents/PhD/git_smt_project/submesoscaletelescope/results/images/NA_002dgr/ri_vs_psi_regional/'
path_save_img_root = '/home/m/m300878/submesoscaletelescope/notebooks/images/eval_ri/regional/'


# %%
grid = xr.open_dataset(fpath_ckdtree)
# %% load cropped and interpolated data
path_to_interp_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/'
# path_to_interp_data = '/Users/mepke/smt_local_data/NA_002dgr/'


data_wb_prime_mean       = xr.open_dataarray(f'{path_to_interp_data}/data_wb_prime_mean.nc')
data_ub_prime_mean_depthi= xr.open_dataarray(f'{path_to_interp_data}/data_ub_prime_mean_depthi.nc')
data_vb_prime_mean_depthi= xr.open_dataarray(f'{path_to_interp_data}/data_vb_prime_mean_depthi.nc')
data_n2_mean             = xr.open_dataarray(f'{path_to_interp_data}/data_n2_mean.nc')
data_dbdx_mean           = xr.open_dataarray(f'{path_to_interp_data}/data_dbdx_mean.nc')
data_dbdy_mean           = xr.open_dataarray(f'{path_to_interp_data}/data_dbdy_mean.nc')
data_M2_mean             = xr.open_dataarray(f'{path_to_interp_data}/data_M2_mean.nc')
data_rho_mean_depthi     = xr.open_dataarray(f'{path_to_interp_data}/data_rho_mean_depthi.nc')
mask_land                = xr.open_dataarray(f'{path_to_interp_data}/mask_land.nc')
mask_mld                 = xr.open_dataarray(f'{path_to_interp_data}/mask_mld.nc')
mldx                     = xr.open_dataarray(f'{path_to_interp_data}/mldx.nc')

# %% Masking land, MLD and filtering M2
mask_used = mask_land * mask_mld
mldx_used = mldx
n2_sel    = data_n2_mean

m2_sel  = data_M2_mean
m2x_sel = data_dbdx_mean
m2y_sel = data_dbdy_mean
rho     = data_rho_mean_depthi

#### Filter
m2_min_lim = 0 #obsolet

m2         = m2_sel.where(np.abs(m2_sel) > m2_min_lim, np.nan)
m2         = m2.where(~np.isinf(m2),np.nan) #infs to nans
m2_mask    = m2 * mask_used
m2_mask    = m2_mask.where(~(m2_mask == 0), np.nan) # remove zeros
m2_mask    = m2_mask.rename('m2')

m2x      = m2x_sel.where(np.abs(m2x_sel) > m2_min_lim, np.nan)
m2x      = m2x.where(~np.isinf(m2x),np.nan) #infs to nans
m2x_mask = m2x * mask_used 
m2x_mask = m2x_mask.where(~(m2x_mask == 0), np.nan) # remove zeros
m2x_mask = m2x_mask.rename('m2x')

m2y      = m2y_sel.where(np.abs(m2y_sel) > m2_min_lim, np.nan)
m2y      = m2y.where(~np.isinf(m2y),np.nan) #infs to nans
m2y_mask = m2y * mask_used 
m2y_mask = m2y_mask.where(~(m2y_mask == 0), np.nan) # remove zeros
m2y_mask = m2y_mask.rename('m2y')

# n2
n2      = n2_sel.where(~np.isinf(n2_sel),np.nan) #infs to nans
n2_mask = n2 * mask_used  #application of mask leads to new zero values
n2_mask = n2_mask.where(~(n2_mask == 0), np.nan) # remove zeros
n2_mask = n2_mask.rename('n2')

wb_mask = data_wb_prime_mean * mask_used 
wb_mask = wb_mask.where(~(wb_mask == 0), np.nan) # remove zeros
wb_mask = wb_mask.rename('wb')

vb_mask = data_vb_prime_mean_depthi * mask_used
vb_mask = vb_mask.where(~(m2_mask == 0), np.nan) # remove zeros
vb_mask = vb_mask.rename('vb')

ub_mask = data_ub_prime_mean_depthi * mask_used
ub_mask = ub_mask.where(~(m2_mask == 0), np.nan) # remove zeros
ub_mask = ub_mask.rename('ub')

rho_mask = rho * mask_used
rho_mask = rho_mask.where(~(m2_mask == 0), np.nan) # remove zeros
rho_mask = rho_mask.rename('rho')

#######################################################
#######################################################
# %% diagnostic
A = rho_mask.to_dataset()
A = A.assign(ub = ub_mask)
A = A.assign(vb = vb_mask)
A = A.assign(wb = wb_mask)
A = A.assign(n2 = n2_mask)
# A = A.assign(m2 = m2_mask)
print('Set alongfront m2 to zero and crossfront value as absolute value of m2x and m2y')
A = A.assign(m2_along_grad = 0)
A = A.assign(m2_cross_grad = np.sqrt(np.power(m2x_mask,2) + np.power(m2y_mask,2)))
# A = A.assign(m2_along_grad = m2x_mask)
# A = A.assign(m2_cross_grad = m2x_mask + m2y_mask)
A = A.assign(mld           = mldx)
if False:
    print('vb calculation with sign')
    A = A.assign(Vb_cross_grad = ( m2x_mask * ub_mask + m2y_mask * vb_mask) / (np.sqrt(np.power(m2x_mask,2) + np.power(m2y_mask,2))) ) # projection of fluctuation on front gradient
else:
    print('negative absolute value of vb')
    A = A.assign(Vb_cross_grad =  - np.abs( ( m2x_mask * ub_mask + m2y_mask * vb_mask) / (np.sqrt(np.power(m2x_mask,2) + np.power(m2y_mask,2))) ) )# projection of fluctuation on front gradient

A = A.assign(fcoriolis     = eva.calc_coriolis_parameter(A.lat))
A = A.assign(alpha         = eva.calc_alpha( A.m2_cross_grad, A.fcoriolis))


A_depth = A
A = A.mean(dim='depthi', skipna=True)
print('calc ri always after all averaging steps')
A = A.assign(ri_diag       = eva.calc_richardsonnumber(A.lat, A.n2, A.m2_cross_grad))

# %%
A = A.assign(grad_b        = eva.calc_abs_grad_b(A.m2_along_grad, A.m2_cross_grad, A.n2))
A = A.assign(psi_diag_held = eva.calc_streamfunc_held_schneider(A.wb, A.m2_along_grad, A.m2_cross_grad))
A = A.assign(psi_diag_full = eva.calc_streamfunc_full(A.wb, A.m2_cross_grad, A.Vb_cross_grad, A.n2, A.grad_b))
A = A.assign(K             = eva.calc_diapycnal_diffusivity(A.Vb_cross_grad, A.m2_cross_grad, A.wb, A.n2, A.grad_b))

# %% Tuning
cs = 0.33
cf = 0.0564
reload(hal)

if False:
    # # vertical profiles are calculated 
    mask_nan             = A_depth.m2_cross_grad
    mask_nan             = mask_nan.where(np.isnan(mask_nan),1.)
    depthI               = A_depth.m2_cross_grad.depthi
    wb_fox_param         = mask_nan * eva.calc_wb_fox_param(A_depth.lat, A_depth.mld,  A.m2_cross_grad, -depthI, cf, structure=True )
    vb_fox_param         = mask_nan * eva.calc_vb_fox_param(A_depth.lat, A_depth.mld,  A.m2_cross_grad, A.ri_diag, -depthI, cf, structure=True )
    wb_stone_param       = mask_nan * eva.calc_wb_stone_param(A_depth.lat, A_depth.mld,  A.m2_cross_grad, A.ri_diag, cs, -depthI, structure = True)
    wb_fox_param         = wb_fox_param.mean(dim='depthi', skipna=True)
    vb_fox_param         = vb_fox_param.mean(dim='depthi', skipna=True)
    wb_stone_param       = wb_stone_param.mean(dim='depthi', skipna=True)
else:
    print('no vertical structure mu=2/3')
    wb_fox_param         = eva.calc_wb_fox_param(A.lat, A.mld,  A.m2_cross_grad, np.nan, cf, structure=False )
    vb_fox_param         = eva.calc_vb_fox_param(A.lat, A.mld,  A.m2_cross_grad, A.ri_diag, np.nan, cf, structure=False )
    wb_stone_param       = eva.calc_wb_stone_param(A.lat, A.mld,  A.m2_cross_grad, A.ri_diag, cs, np.nan, structure=False )

vb_stone_param       = eva.calc_vb_stone_param(A.lat, A.mld,  A.m2_cross_grad, A.ri_diag, cs)
psi_stone_full_param = eva.calc_streamfunc_full(wb_stone_param,  A.m2_cross_grad, vb_stone_param, A.n2, A.grad_b)
psi_fox_full_param   = eva.calc_streamfunc_full(wb_fox_param,  A.m2_cross_grad, vb_fox_param, A.n2, A.grad_b)
psi_stone_held_param = eva.calc_streamfunc_held_schneider(wb_stone_param, A.m2_along_grad,   A.m2_cross_grad)
psi_fox_held_param   = eva.calc_streamfunc_held_schneider(wb_fox_param, A.m2_along_grad,  A.m2_cross_grad)
K_fox                = eva.calc_diapycnal_diffusivity(vb_fox_param, A.m2_cross_grad, wb_fox_param, A.n2, A.grad_b)
K_stone              = eva.calc_diapycnal_diffusivity(vb_stone_param, A.m2_cross_grad, wb_stone_param, A.n2, A.grad_b)

A = A.assign(wb_fox_param         = wb_fox_param         )
A = A.assign(vb_fox_param         = vb_fox_param         )
A = A.assign(wb_stone_param       = wb_stone_param       )
A = A.assign(vb_stone_param       = vb_stone_param       )
A = A.assign(psi_stone_full_param = psi_stone_full_param )
A = A.assign(psi_fox_full_param   = psi_fox_full_param   )
A = A.assign(psi_stone_held_param = psi_stone_held_param )
A = A.assign(psi_fox_held_param   = psi_fox_held_param   )
A = A.assign(K_fox                = K_fox                )  
A = A.assign(K_stone              = K_stone              )  

# A = A.mean(dim='depthi', skipna=True) # average parameterized values
# %%
reload(hal)
reload(domfunc)
domain    = 'multiple'
var       = 'psi'
filter    = 'ri'

if filter == 'm2':
    path = f'{path_save_img_root}area/filter_{filter}/'

    ds_m2_0   = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=0   )
    ds_m2_1e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=1e-8)
    ds_m2_3e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=3e-8)
    ds_m2_5e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=5e-8)

    path = f'{path_save_img_root}ri_vs_{var}_regional/'
    
    domfunc.plot_slopes_scatter_all_m2(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path, domain, savefig, var)

if filter == 'ri':
    path = f'{path_save_img_root}area/filter_{filter}/'
    ds_m2_0   = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=1e4)
    ds_m2_1e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=1e3)
    ds_m2_3e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=1e2)
    ds_m2_5e8 = domfunc.filter_select_domain(A, m2y_mask, domain, savefig, path, var, filter, m2_filter=1e1)

    path = f'{path_save_img_root}ri_vs_{var}_regional/'
    domfunc.plot_slopes_scatter_all_ri(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path, domain, savefig, var)

# %%
import pathlib
path = pathlib.Path(__file__).resolve()
path
plt.savefig(f'{path_save_img_root}_test_meta.png', metadata={"Title":"moe"})

# %%
reload(domfunc)
data = ds_m2_0
path = f'{path_save_img_root}area/visual_eval/'

domfunc.plot_wb_maps(data, domain, path, savefig)
domfunc.plot_vb_maps(data, domain, path, savefig)
domfunc.plot_K_maps(data, domain, path, savefig)
domfunc.plot_N2_map(data, domain, path, savefig)
domfunc.plot_M2_map(data, domain, path, savefig)
domfunc.plot_psi_maps(data, domain, path, savefig)
domfunc.plot_psi_held_maps(data, domain, path, savefig)


# %% Correlation coefficient
r = stats.pearsonr(data.wb.data.flatten(), data.wb_fox_param.data.flatten())
print(r)
# %%
reload(domfunc)
path_save_img = f'{path_save_img_root}ri_vs_{var}_regional/'
# domfunc.plot_slopes_scatter(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path_save_img, domain, savefig)

# %%
# for raw data with negative and positive ri numbers: adapt log mask in calc_log2
if False:
    path = f'{path_save_img_root}ri_vs_{var}_regional/raw_data/'
    domfunc.plot_slopes_scatter_raw(ds_m2_0, ds_m2_1e8, ds_m2_3e8, ds_m2_5e8, path_save_img, domain, savefig)
# %%
# domfunc.plot_wb_scatter(ds_m2_0)
# %%
reload(hal)
path = f'{path_save_img_root}area/'
domfunc.plot_ri_diag_fronts(ds_m2_0, savefig, path)


# %%
######################################################################################
####################################OLD Evaluation#################################

if False:

    domfunc.plot_wb_scatter(ds_m2_0)

    m2_filter = 'xy'
    lat_reg = np.array([0,1])

    lim = 1e8
    lhs_diag, rhs_ri = domfunc.calc_lhs_rhs_wb_ri(A, A.wb)
    # logx_diag , logy_diag  = hal.calc_log10(rhs_ri.data.reshape(-1),  lhs_diag.where(A.ri_diag < lim).data.reshape(-1))
    logx_diag , logy_diag  = hal.calc_log10_2(rhs_ri.data,  lhs_diag.where(A.ri_diag < lim).data)
    # logx_diag , logy_diag  = hal.calc_log10_2(rhs_ri.data.reshape(-1),  lhs_diag.where(A.ri_diag < lim).data.reshape(-1))


    # %%
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=1.9, axlab_kw=None)
    ii = -1
    fs = 10

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    logx = logx_diag
    logy = logy_diag
    ax.scatter(logx, logy , color='grey', marker='^', s=0.5)
    res = stats.linregress(logx, logy)
    ax.plot(logx, res.intercept + res.slope*logx, 'red', label=f'dia: s={res.slope:.2f} i={res.intercept:.2} r={res.rvalue:.2} p={res.pvalue:.4} se:{res.stderr:.2}')  # type: ignore

    pyic.plot_settings(ax)   # type: ignore
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.grid()
    ax.legend(loc='lower left', bbox_to_anchor=(-0.15, 1.02, 1, 0.2))
    xlabel = r'$log_{10} \overline{Ri}$'
    ylabel = r'$log_{10} |\overline{w^{\prime} b^{\prime}}|/(H^2 f^3 \alpha^2) $'
    ax.set_xlabel(f'{xlabel}', fontsize=fs)
    ax.set_ylabel(f'{ylabel}', fontsize=fs)

    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_front_m2_lim_{m2_filter}.png', dpi=150, format='png', bbox_inches='tight')
    plt.figure()
    A.m2_cross_grad.plot(vmin=0, vmax=5e-7)
    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_front_m2_lim_{m2_filter}_domain.png', dpi=150, format='png', bbox_inches='tight')


    # %%
    mask0 = A.m2_cross_grad > 0
    mask1 = A.m2_cross_grad > 1e-8
    mask2 = A.m2_cross_grad > 3e-8

    mask0 = mask0*1
    mask1 = mask1*1
    mask2 = mask2*1

    Mask = mask0 + mask1 + mask2
    # %%

    A_m2_0 = A
    # %%
    A_m2_1e8 = A
    # %%
    A_m2_3e8 = A

    # %%

    # %%
    fig = plt.figure(figsize=([10,8]))

    levels= np.array([0,1,2,3,5]) * 1e-8
    # levels= [0,1,2,3]
    # pyic.shade(A.m2_cross_grad.lon, A.m2_cross_grad.lat, A.m2_cross_grad.data, ax=ax, cax=cax,  rasterized=False, contfs=True)
    cf = plt.contourf(A.m2_cross_grad.lon, A.m2_cross_grad.lat, A.m2_cross_grad, cmap='BuPu', projection=ccrs_proj,  levels=levels, extend='max')
    cb = fig.colorbar(cf)
    plt.xlabel('lon')
    plt.ylabel('lat')


    if savefig == True: plt.savefig(f'{path_save_img}/{domain}_front_m2_lim_var_filter_domain_contf.png', dpi=150, format='png', bbox_inches='tight')


    # %%
    hca, hcb = pyic.arrange_axes(1, 1, asp =0.8, plot_cb=True,  fig_size_fac=2, axlab_kw=None, projection=ccrs_proj)
    ii = -1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = (0, 1e-7) #all
    hm = pyic.shade(A.m2_cross_grad.lon, A.m2_cross_grad.lat, A.m2_cross_grad.data, ax=ax, cax=cax, nclev=6, clim=clim, cmap='BuPu', projection=ccrs_proj,  rasterized=False, contfs=True)
    pyic.plot_settings(ax) #down
    if savefig == True: plt.savefig(f'{path_save_img}{domain}_front_m2_lim_var_filter_domain.png', dpi=150, format='png', bbox_inches='tight')



    hca, hcb = pyic.arrange_axes(1, 1, asp =0.4, plot_cb=True,  fig_size_fac=2, axlab_kw=None, projection=ccrs_proj)
    ii = -1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = (0, 1e-7) #down
    hm = pyic.shade(A.m2_cross_grad.lon, A.m2_cross_grad.lat, A.m2_cross_grad.data, ax=ax, cax=cax, nclev=6, clim=clim, cmap='BuPu', projection=ccrs_proj,  rasterized=False, contfs=True)
    pyic.plot_settings(ax, ylim=(lat_reg[0], 29.75 )) #down # type: ignore 
    if savefig == True: plt.savefig(f'{path_save_img}{domain}_front_m2_lim_var_filter_domain_down.png', dpi=150, format='png', bbox_inches='tight')

    hca, hcb = pyic.arrange_axes(1, 1, asp =0.4, plot_cb=True,  fig_size_fac=2, axlab_kw=None, projection=ccrs_proj)
    ii = -1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = (0, 5e-8) #up
    hm = pyic.shade(A.m2_cross_grad.lon, A.m2_cross_grad.lat, A.m2_cross_grad.data, ax=ax, cax=cax, nclev=6, clim=clim, cmap='BuPu', projection=ccrs_proj,  rasterized=False, contfs=True)
    pyic.plot_settings(ax, ylim=( 29.75, lat_reg[1] )) #up # type: ignore 
    if savefig == True: plt.savefig(f'{path_save_img}{domain}_front_m2_lim_var_filter_domain_up.png', dpi=150, format='png', bbox_inches='tight')


    # %%
