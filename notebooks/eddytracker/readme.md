Eddy tracker:
active files:
1) run identify_fast.py
    ""identifies eddies from ssh data 0.3 degree is sufficient to detect agulhas rings""
2) run tracking_eddies.py
    ""tracks eddies from ssh data, gaps appear more likely when tidal forcing is acive""
3) run composite_tracks.py or composite_tracks_3d.py
    ""uses tracks to extract eddie 2d or 3d at each time step and composites them to an xarray dataset""

other files:
4) extract_eddies.py
    ""extracts eddy at single time step, inluding N2 and rho""
5) visualize_eddy_spectra.py
    "" computes spectra of eddie timeseries""
6) hovmuller.py
    ""hovmullerplot through 2d eddie over time, extended to 3d planned""

7) 2D Cropping of ssh and KE data happens here:
    /home/m/m300878/submesoscaletelescope/run_batch_job/videos/run_mov/crop_interp_smt.py