# %%
from datetime import datetime, timedelta
import numpy as np
from matplotlib import pyplot as plt
import os 
import glob

import numpy as np
import xarray as xr
import dask
# from dask.distributed import Client
# client = Client(n_workers=4)  # Adjust the number of workers as needed

# %%
import sys
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
sys.path.insert(0, '../')
import pandas as pd
import time as pytime
import pandas as pd
import matplotlib 

key = sys.argv[1]
print('key', key)
eddy_id = sys.argv[2]
print('eddy_id', eddy_id)
# %%
import smt_modules.init_slurm_cluster as scluster 

reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, scale=2)
cluster
client
# %%
exp = 7

if exp == 7:
    root_dir = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/'
elif exp == 9:
    root_dir = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp9/'

# varname     = 'zos'
wavelength  = 700  #choice of spatial cutoff for high pass filter in km
shape_error = 25 #choice of shape error in km
eddy_dir    = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
eddy_type   = 'anticyclonic'
mindays     = 20 #min number of days for tracked eddy
tracker_dir = eddy_dir+'tracks/'

dlon       = 2.5
dlat       = 2.5
res        = 0.01
npts       = int(dlon/res) #number of points from centre

dscorres  = xr.open_dataset(tracker_dir+eddy_type+'_correspondance.nc')
dstracks  = xr.open_dataset(tracker_dir+eddy_type+'_tracks.nc')
dsshort   = xr.open_dataset(tracker_dir+eddy_type+'_short.nc')
dsuntrack = xr.open_dataset(tracker_dir+eddy_type+'_untracked.nc')


# %%
#Get desired region [Agulhas rings and leakage]
# ARidx = np.argwhere((dstracks.latitude.values<=-30) & (dstracks.latitude.values>=-45) & (dstracks.longitude.values>0) & (dstracks.longitude.values<25))
ARidx = np.argwhere((dstracks.latitude.values<=-20) & (dstracks.latitude.values>=-36) & (dstracks.longitude.values>-15) & (dstracks.longitude.values<20))

#Get track IDs for Agulhas rings, remove all duplicates
ARtrackid=np.array(sorted(list(set(dstracks.track.values[ARidx].squeeze()))))
print('Track IDs=',ARtrackid)

#Get number of obs for each track for Agulhas rings
trackIDs=dstracks.track.values
tracklen=[]
for ii in range(trackIDs.max()+1):
    tracklen.append(len(np.argwhere(trackIDs == ii)))
lentrack=np.array(tracklen)[ARtrackid]
print('No. of obs for each tracked ID =',lentrack)

#Remove tracks with less than minimum number of days 
newARtrackid=np.delete(ARtrackid,np.r_[np.argwhere(lentrack<mindays)])
print('Track IDs that last more than ',mindays,' days=',newARtrackid)
lentrack=np.array(tracklen)[newARtrackid]
print('No. of obs for each tracked ID =',lentrack)
del(tracklen)


# %%
def geteddy_alongtrack(ds,lon,lat,dlon,dlat,edges_of_vertex=None,curl_coeffs=None):
    lon    = round(lon,2)
    lat    = round(lat,2)
    lonmin = round(lon-dlon,2)
    lonmax = round(lon+dlon,2)
    latmin = round(lat-dlat,2)
    latmax = round(lat+dlat,2)
    # print('lonmin',lonmin, 'lonmax',lonmax, 'latmin',latmin, 'latmax',latmax)

    ds_interp = interp(ds, lon_reg=[lonmin, lonmax], lat_reg=[latmin, latmax], edges_of_vertex=edges_of_vertex, curl_coeffs=curl_coeffs)

    return ds_interp

def interp(data, lon_reg, lat_reg, edges_of_vertex=None, curl_coeffs=None):
    print('start interpolation')
    print('lon_reg', lon_reg, 'lat_reg', lat_reg)
    fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
    if ds.name=='normal_velocity':
        print('from verticies')
        data        = data.rename({'ncells':'edge'})
        da          = xr_calc_curl(edges_of_vertex, data.compute(), curl_coeffs) #memory intense not easily scalable with cluster
        data_interp = pyic.interp_to_rectgrid_xr(da.compute(), fpath_ckdtree, coordinates='vlat vlon', lon_reg=lon_reg, lat_reg=lat_reg)
    elif ds.name == 'vort':
        data_interp = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, coordinates='vlat vlon', lon_reg=lon_reg, lat_reg=lat_reg)
    else:
        data_interp = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    print('end interpolation')
    print('data_interp', data_interp.shape)
    return data_interp

def extract_eddytrack_raw_sing(dstracks,tridx,ds,dlon,dlat,res):
    # npts          = int(dlon/res)
    alongtrackidx = np.argwhere(dstracks.track.values==tridx)
    loncen        = dstracks.longitude.values[alongtrackidx]
    latcen        = dstracks.latitude.values[alongtrackidx]
    print(loncen[0],latcen[0])
    if np.abs(loncen[0]) > 180:
        print('loncen > 180')
        loncen = loncen-360
    print(loncen[0],latcen[0])
    timearr       = dstracks.time.values[alongtrackidx].flatten()
    timearr       = pd.to_datetime(timearr)
    timearr       = xr.DataArray(timearr,coords={'time':timearr},dims=['time'])
    # interpolate missed observations
    ds_obs        = timearr
    ds_obs        = ds_obs.assign_coords(lon=('time', loncen.flatten()))
    ds_obs        = ds_obs.assign_coords(lat=('time', latcen.flatten()))

    time0   = pd.to_datetime(timearr[0].data)
    time1   = pd.to_datetime(timearr[-1].data)
    time_pd = pd.date_range(start=time0, end=time1, freq='2H')
    print('size timearr', timearr.size, 'size time_pd', len(time_pd))
    # Expand time array to 2h interval
    ds_obs = ds_obs.reindex(time=time_pd)

    # Interpolate NaNs in lat and lon coordinates
    ds_obs['lon'] = ds_obs['lon'].interpolate_na(dim='time', method='linear', fill_value="extrapolate")
    ds_obs['lat'] = ds_obs['lat'].interpolate_na(dim='time', method='linear', fill_value="extrapolate")

    timearr = time_pd #ds_obs.time.drop('lon').drop('lat')
    loncen  = ds_obs.lon.data
    latcen  = ds_obs.lat.data

    print('obs: start',timearr.values[0], '\n end',timearr.values[-1] )
    print('KE: start',ds.time.values[0], '\n end',ds.time.values[-1])
    if ds.time.size != len(timearr):
        print('mismatch time window')
        start0 = ds.time.values[0]
        end0   = ds.time.values[-1]
        start1 = timearr.values[0]
        end1   = timearr.values[-1]
        start  = max(start0,start1)
        end    = min(end0,end1)

        # find idx of closest time using pandas
        start_idx0 = np.argmin(np.abs(ds.time.values-start))
        end_idx0   = np.argmin(np.abs(ds.time.values-end))
        ds         = ds.isel(time=slice(start_idx0,end_idx0))

        start_idx1    = np.argmin(np.abs(timearr.values-start))
        end_idx1      = np.argmin(np.abs(timearr.values-end))
        # timearr       = timearr.isel(time=slice(start_idx1,end_idx1))
        timearr       = timearr[start_idx1:end_idx1]
        alongtrackidx = alongtrackidx[start_idx1:end_idx1]
        loncen        = loncen[start_idx1:end_idx1]
        latcen        = latcen[start_idx1:end_idx1]

        print('size timearr', timearr.size, 'size ds', ds.time.size)
        print('obs: start',timearr.values[0], '\n end',timearr.values[-1] )
        print('KE: start',ds.time.values[0], '\n end',ds.time.values[-1])

    return ds, timearr, loncen, latcen


def load_3d_exp(var):
    reload(eva) # 2h interval combining 3d and 2d output

    if var == 'zos' or var == 'vort':
        variable = '2d'
    else:
        variable = var

    exp    = 7
    folder = f'smtwv000{exp}/outdata_{variable}'
    path   = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    if variable == '2d' or variable == 'forcing':
        chunks = dict(time=1)
        print('delte first 6') #for 2d
        flist    = flist[6:-1]
        it       = 2
        flist    = flist[1::it]
        variable = 'zos'
    else:
        chunks = dict(time=1, depth=1)
    print('chunks =', chunks)

    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp

    # ds = ds.drop(['clon_bnds', 'clat_bnds'])
    # ds = ds.drop(['clon', 'clat'])

    for coord in ['clon_bnds', 'clat_bnds', 'clon', 'clat', 'vlon', 'vlat']:
        if coord in ds:
            ds = ds.drop(coord)
    ds = ds[var]
    return ds

def load_normal_velocity():
    reload(eva) # 2h interval combining 3d and 2d output
    variable='normal_velocity'
    exp=7
    it=1
    folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    print('path to data', path)
    path_data = f'{path}/smtwv000{exp}_oce*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    if variable == '2d' or variable == 'forcing':
        chunks = dict(time=1)
    else:
        chunks = dict(time=1, depth=1)
    print('chunks =', chunks)
    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds = xr.decode_cf(ds) #restores timestamp

    ds = ds.drop(['elon_bnds', 'elat_bnds'])
    ds = ds.drop(['elon', 'elat'])

    return ds.normal_velocity

def compute_curl_coeff():
    print('compute curl coeff, edges of vertex')
    tgrid           = eva.load_smt_wave_grid()
    ds_IcD          = pyic.convert_tgrid_data(tgrid)
    curl_coeffs     = pyic.xr_calc_rot_coeff(ds_IcD)
    edges_of_vertex = ds_IcD["edges_of_vertex"].compute()
    return curl_coeffs, edges_of_vertex

def xr_calc_curl(edges_of_vertex, vector, curl_coeffs):
    assert "edge" in vector.dims
    curl_vec = (
        vector.isel(edge=edges_of_vertex) * curl_coeffs).sum(dim="ne_v")
    return curl_vec



# %%
vars = ['u','v','w','to', 'so', 'tke', 'zos', 'vort']

tridx = int(eddy_id)
ds    = load_3d_exp(vars[int(key)])
if ds.name == 'normal_velocity':
    ds                           = load_normal_velocity()
    curl_coeffs, edges_of_vertex = compute_curl_coeff()
else:
    curl_coeffs     = None
    edges_of_vertex = None
# ds                  = ds.isel(time=slice(50, -1)) # time missing ID0
path_save_composite = f'/composite_vers2/{eddy_type}/ID{tridx}/{ds.name}/'

print('path_save_composite', path_save_composite)
# check if directory exists
if not os.path.exists(tracker_dir+path_save_composite):
    os.makedirs(tracker_dir+path_save_composite)

# ds=ds.isel(depth=slice(0,3), time=slice(0,3))
# select every 10th depth level
# ds=ds.isel(depth=slice(0,ds.depth.size,50))
# ds=ds.isel(depth=slice(0,3))

# %%
ds, time, loncen, latcen = extract_eddytrack_raw_sing(dstracks,tridx,ds,dlon,dlat,res)
# %%
steps = np.arange(time.size)
num   = 1

# === mpi4py ===
try:
  from mpi4py import MPI
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  npro = comm.Get_size()
except:
  print('::: Warning: Proceeding without mpi4py! :::')
  rank = 0
  npro = 1

list_all_pros = [0]*npro
for nn in range(npro):
  list_all_pros[nn] = steps[nn::npro]
steps = list_all_pros[rank]

pytime.sleep(0.1*rank)

for kk, step in enumerate(steps):
    print(f'node: {num}: kk = {kk+1}/{steps.size}, step = {step}')
    print(f'{time[step]}')
    dmvarfile = ds.isel(time=step)
    FIELDcomp = geteddy_alongtrack(dmvarfile,loncen[step],latcen[step],dlon=dlon,dlat=dlat,edges_of_vertex=edges_of_vertex,curl_coeffs=curl_coeffs)
    FIELDcomp = FIELDcomp.assign_coords(lat=np.arange(-npts,npts)*res,lon=np.arange(-npts,npts)*res)
    if ds.name == 'zos' or ds.name == 'mlotst':
        compSSH   = xr.DataArray(data=np.array(FIELDcomp).squeeze(),
                 coords={ 'y':np.arange(-npts,npts)*res, 'x':np.arange(-npts,npts)*res},
                 dims=['y','x'],name=ds.name)
    else:
        compSSH   = xr.DataArray(data=np.array(FIELDcomp).squeeze(),
                 coords={ 'y':np.arange(-npts,npts)*res, 'x':np.arange(-npts,npts)*res, 'depth':ds.depth.values},
                 dims=['depth','y','x'],name=ds.name)
    compSSH=compSSH.assign_coords(time=time[step])
    compSSH=compSSH.assign_coords(loncen=loncen[step])
    compSSH=compSSH.assign_coords(latcen=latcen[step])
    print('save time step',step)
    # fileout = tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+'deg_trackID_'+str(tridx)+'time_'+str(step)+'.nc'
    fileout = tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+'deg_trackID_'+str(tridx)+f'{ds.name}_time_{compSSH.time.values}.nc'
    compSSH.to_netcdf(fileout)

print('node: ',num, 'loop done')
client.close()
cluster.close()

# %%
if False:
    import numpy as np
    import xarray as xr
    import dask
    import glob
    import matplotlib.pyplot as plt
    path = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/to/anticyclonic_2.5x2.5deg_trackID_0time_436.nc'
    ds = xr.open_dataset(path)

    path = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/to/anticyclonic_2.5x2.5deg_trackID_0time_*.nc'
    flist = np.array(glob.glob(path))
    flist.sort()
    ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks={'time':1})

    plt.figure()
    ds.isel(y=250).to.isel(depth=slice(0,100)).isel(time=600).plot()
    plt.ylim(1500,0)
    
