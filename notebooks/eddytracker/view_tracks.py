# %%
from datetime import datetime, timedelta
import numpy as np
from matplotlib import pyplot as plt
import os 
import glob

import numpy as np
import xarray as xr
import dask
import sys
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
import pandas as pd
import time as pytime
import pandas as pd
import matplotlib 
# %%#############################################################
import smt_modules.init_slurm_cluster as scluster 

reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, scale=2)
cluster
client

# %%
ds_all   = eva.load_smt_wave_all('2d', 7, it =1)
ds_all   = ds_all.drop('vlon').drop('vlat')
ds_0     = xr.open_dataset('/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190704T001500Z.nc', chunks=dict(depth=1))
# mask_land = eva.load_smt_wave_land_mask()
# tgrid     = eva.load_smt_wave_grid()
# # evaluation of agulhas ring - anticyclone anomaly within single rings
# ds_all = eva.load_smt_wave_all('w', 9, it =1)
# ds_smt     = ds_all.vort.isel(time=slice(50,450)).isel(depth=1)
ds_smt = ds_all.vort.isel(time=slice(1000,1002)).isel(depth=1)


# %%
def get_pyeddy_track(eddy_type):

    exp = 7

    # root_dir = '/scratch/m/m300878/agulhas_eddies/exp9/'
    if exp == 7:
        root_dir = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/'
    elif exp == 9:
        root_dir = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp9/'

    varname     = 'zos'
    wavelength  = 700  #choice of spatial cutoff for high pass filter in km
    shape_error = 25 #choice of shape error in km
    eddy_dir    = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
    # eddy_type   = 'anticyclonic'
    mindays=20 #min number of days for tracked eddy
    tracker_dir=eddy_dir+'tracks/'

    dlon       = 2.5
    dlat       = 2.5
    res        = 0.01
    npts       = int(dlon/res) #number of points from centre

    dscorres  = xr.open_dataset(tracker_dir+eddy_type+'_correspondance.nc')
    dstracks  = xr.open_dataset(tracker_dir+eddy_type+'_tracks.nc')
    dsshort   = xr.open_dataset(tracker_dir+eddy_type+'_short.nc')
    dsuntrack = xr.open_dataset(tracker_dir+eddy_type+'_untracked.nc')

    #Get desired region [Agulhas rings and leakage]
    # ARidx = np.argwhere((dstracks.latitude.values<=-30) & (dstracks.latitude.values>=-45) & (dstracks.longitude.values>0) & (dstracks.longitude.values<25))
    ARidx = np.argwhere((dstracks.latitude.values<=-20) & (dstracks.latitude.values>=-36) & (dstracks.longitude.values>-15) & (dstracks.longitude.values<20))

    #Get track IDs for Agulhas rings, remove all duplicates
    ARtrackid=np.array(sorted(list(set(dstracks.track.values[ARidx].squeeze()))))
    print('Track IDs=',ARtrackid)

    #Get number of obs for each track for Agulhas rings
    trackIDs=dstracks.track.values
    tracklen=[]
    for ii in range(trackIDs.max()+1):
        tracklen.append(len(np.argwhere(trackIDs == ii)))
    lentrack=np.array(tracklen)[ARtrackid]
    print('No. of obs for each tracked ID =',lentrack)

    #Remove tracks with less than minimum number of days 
    newARtrackid=np.delete(ARtrackid,np.r_[np.argwhere(lentrack<mindays)])
    print('Track IDs that last more than ',mindays,' days=',newARtrackid)
    lentrack=np.array(tracklen)[newARtrackid]
    print('No. of obs for each tracked ID =',lentrack)
    del(tracklen)
    return dstracks


# %%
tridx = []
def export_track_data(dstracks):
    ds_track = []

    for tridx in np.arange(10):
        alongtrackidx = np.argwhere(dstracks.track.values==tridx)
        loncen        = dstracks.longitude.values[alongtrackidx]
        latcen        = dstracks.latitude.values[alongtrackidx]
        timearr       = dstracks.time.values[alongtrackidx].flatten()
        timearr       = pd.to_datetime(timearr)
        timearr       = xr.DataArray(timearr,coords={'time':timearr},dims=['time'])
        ds_obs        = timearr
        ds_obs        = ds_obs.assign_coords(lon=('time', loncen.flatten()))
        ds_obs        = ds_obs.assign_coords(lat=('time', latcen.flatten()))
        ds_track.append(ds_obs)

    ds_track = xr.concat(ds_track, dim ='tridx')
    return ds_track

ds_track_ac = get_pyeddy_track('anticyclonic')
ds_track_cy = get_pyeddy_track('cyclonic')

ds_ac = export_track_data(ds_track_ac)
ds_cy = export_track_data(ds_track_cy)


# %%
lon_reg           = -12, 10
lat_reg           = -36, -20
lon_reg           = -18, 20
lat_reg           = -45, -15
fpath_ckdtree     = '/home/m/m300602/work/icon/grids/r2b9_oce_r0004/ckdtree/rectgrids/r2b9_oce_r0004_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtree_smt = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
fpath_ckdtree_smt = '/home/m/m300602/work/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
fig_path          = '/home/m/m300878/submesoscaletelescope/results/smt_wave/eddy_features/'

# %%
ds0       = ds_smt.isel(time=0)#xr.open_dataset(flist_smt[5*24+100])
data_smt = pyic.interp_to_rectgrid_xr(ds0, fpath_ckdtree_smt, coordinates='vlat vlon', lon_reg=lon_reg, lat_reg=lat_reg)
data_smt.plot(vmin=-5e-5, vmax=5e-5, cmap='RdBu_r')

# %%
ds        = eva.load_smt_wave_to_exp7_08(it=2)
sst       = ds.isel(time=0, depth=110).to
a         = sst.where(~(sst==0), np.nan)
mask_land = ~np.isnan(a)
mask_land = mask_land.where(mask_land==True, np.nan)
mask_land = mask_land.drop('time')

land = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree_smt, lon_reg=lon_reg, lat_reg=lat_reg)

# %%
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature

# Assuming data_smt, land, ds_track, lat_reg, lon_reg, and fig_path are already defined

asp = (lat_reg[1] - lat_reg[0]) / (lon_reg[1] - lon_reg[0])

# Set up the Cartopy projection
projection = ccrs.PlateCarree()

fig, ax = plt.subplots(figsize=(10, 10 * asp), subplot_kw={'projection': projection})

# Add land features with a specific color
ax.add_feature(cfeature.LAND, zorder=20, edgecolor='black', facecolor='gray')

# Plot the data
ax.pcolormesh(data_smt.lon, data_smt.lat, data_smt.data, cmap='RdBu_r', vmin=-5e-5, vmax=5e-5, transform=projection)
land_nan_mask = np.isnan(land)
masked_land = np.ma.masked_where(~land_nan_mask, land_nan_mask)

ax.pcolormesh(land.lon, land.lat, masked_land, cmap='gray', alpha=0.4, transform=projection)

for i in np.arange(10):
    d = ds_ac.isel(tridx=i)
    lon = d.lon
    lat = d.lat
    lon = lon.dropna('time')
    lat = lat.dropna('time')
    if i >= 2 and i <= 4:
        lon = lon - 360
    ax.plot(lon, lat, lw=1, color='purple', transform=projection)
    ax.text(lon.values[10], lat.values[10], f'{i}', fontsize=15, color='purple', ha='center', va='bottom', transform=projection)
for i in np.arange(10):
    d = ds_cy.isel(tridx=i)
    lon = d.lon
    lat = d.lat
    #drop nans
    lon = lon.dropna('time')
    lat = lat.dropna('time')
    ax.plot(lon, lat, lw=1, color='forestgreen', transform=projection)
    ax.text(lon.values[10], lat.values[10], f'{i}', fontsize=15, ha='center', va='bottom', transform=projection)

ax.set_title(f'Eddy Tracks, {data_smt.time.values:.13}, vorticity at {data_smt.depth.values:.1}m, land at {ds.depth[110].values}m')

# Set the extent of the plot
ax.set_extent([lon_reg[0], lon_reg[1], lat_reg[0], lat_reg[1]], crs=projection)

# Add gridlines and labels
gl = ax.gridlines(draw_labels=True, crs=projection)
gl.top_labels = False
gl.right_labels = False
gl.xlabel_style = {'size': 15}
gl.ylabel_style = {'size': 15}

if savefig==True: plt.savefig(fig_path + 'eddy_tracks_update.png', dpi=250)
# %%
