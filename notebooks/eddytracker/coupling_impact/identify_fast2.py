# %%
import xarray as xr
import numpy as np
import matplotlib.pylab as plt
import matplotlib.cm as cm
from scipy.interpolate import CloughTocher2DInterpolator, LinearNDInterpolator, NearestNDInterpolator
import glob
import intake
import dask
import pandas as pd
dask.config.set({"array.slicing.split_large_chunks": True}) 

from py_eddy_tracker.dataset.grid import RegularGridDataset
from datetime import datetime, timedelta
from netCDF4 import Dataset

import io
import os

import warnings
warnings.filterwarnings("ignore")
# %%
import sys
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
# import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 
sys.path.insert(0, '../')
import functions as fu
# %%
## Start Parallel Client
# from concurrent.futures import ProcessPoolExecutor
# from concurrent.futures import ThreadPoolExecutor
# # Note: Could also use Dask Distributed Client
# n_cpu = 16
# %%
# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
cluster
client
# %%
cat        = intake.open_catalog("https://raw.githubusercontent.com/eerie-project/intake_catalogues/main/eerie.yaml")
model      = 'icon-esm-er'
expid      = 'eerie-control-1950'
gridspec   = 'gr025'
realm      = 'ocean'
cat_regrid = cat['dkrz.disk.model-output'][model][expid][realm][gridspec]
print(list(cat_regrid))

# %%
ds = cat_regrid['2d_daily_mean'].to_dask()
ds = ds.ssh.isel(time=slice(0,365))

# %%
datearrs = ds.time.values
datearrs = pd.to_datetime(datearrs)
varname  = 'zos'
root_dir = '/work/mh0033/m300878/eddy_tracks/eerie/eddy_tracks/'

# %% change ICON time string
tsize = ds.time.size
start = '1993-01-01-00:00:00'
# create timestring
timestr = pd.date_range(start, periods=tsize, freq='D')
# replace time string
ds['time'] = timestr