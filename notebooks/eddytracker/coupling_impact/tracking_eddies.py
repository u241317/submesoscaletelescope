# %%
import os 
import glob

from py_eddy_tracker.featured_tracking.area_tracker import AreaTracker
from py_eddy_tracker.tracking import Correspondances

import numpy as np
from datetime import datetime, timedelta
from netCDF4 import Dataset
import xarray as xr
import dask
# %%
def tracking_eddytype(trackerdir,eddydir, eddy_type, zarr, nb_obs_min, raw, cmin):
    #eddy_type='anticyclonic','cyclonic'
    previous_correspondance = os.path.join(trackerdir,eddy_type+'_correspondance.nc')
    print(previous_correspondance)
    search = os.path.join(eddydir+'_'+eddy_type+'*.nc')
    print('search files ',search)
    file_objects = sorted(glob.glob(search))
    print('files for tracking: ', file_objects)
    tracking(file_objects, previous_correspondance, eddy_type, zarr=zarr, nb_obs_min=nb_obs_min, raw=raw, cmin=cmin)
    return None

#Functions from eddy-tracking.py (aided by Malcolm Roberts)
def tracking(file_objects, previous_correspondance, eddy_type, zarr=False, nb_obs_min=10, raw=True, cmin=0.05, virtual=4):
    # We run a tracking with a tracker which uses contour overlap, on first time step
    output_dir = os.path.dirname(previous_correspondance)
    class_kw   = dict(cmin=cmin)
    if not os.path.isfile(previous_correspondance):
        c = Correspondances(
            datasets=file_objects, class_method=AreaTracker, 
            class_kw=class_kw, virtual=virtual
        )
        c.track()
        c.prepare_merging()
    else:
        c = Correspondances(
            datasets=file_objects, class_method=AreaTracker, 
            class_kw=class_kw, virtual=virtual,
            previous_correspondance=previous_correspondance
        )
        c.track()
        c.prepare_merging()
        c.merge()

    new_correspondance = previous_correspondance[:-3]+'_new.nc'
    with Dataset(new_correspondance, "w") as h:
        c.to_netcdf(h)

    try:
        # test can read new file, and then move to replace old file
        nc = Dataset(new_correspondance, 'r')
        os.rename(new_correspondance, previous_correspondance)
    except:
        raise Exception('Error opening new correspondance file '+new_correspondance)

    write_obs_files(c, raw, output_dir, zarr, eddy_type, nb_obs_min)
    

def write_obs_files(c, raw, output_dir, zarr, eddy_type, nb_obs_min):
    kw_write = dict(path=output_dir, zarr_flag=zarr, sign_type=eddy_type)

    fout = os.path.join(output_dir, eddy_type+'_untracked.nc')
    c.get_unused_data(raw_data=raw).write_file(
        filename=fout
    )

    short_c = c._copy()
    short_c.shorter_than(size_max=nb_obs_min)
    short_track = short_c.merge(raw_data=raw)

    if c.longer_than(size_min=nb_obs_min) is False:
        long_track = short_track.empty_dataset()
    else:
        long_track = c.merge(raw_data=raw)

    # We flag obs
    if c.virtual:
        long_track["virtual"][:] = long_track["time"] == 0
        long_track.normalize_longitude()
        long_track.filled_by_interpolation(long_track["virtual"] == 1)
        short_track["virtual"][:] = short_track["time"] == 0
        short_track.normalize_longitude()
        short_track.filled_by_interpolation(short_track["virtual"] == 1)

    print("Longer track saved have %d obs", c.nb_obs_by_tracks.max())
    print(
        "The mean length is %d observations for long track",
        c.nb_obs_by_tracks.mean(),
    )

    fout = os.path.join(output_dir, eddy_type+'_tracks.nc')
    long_track.write_file(filename=fout)
    fout = os.path.join(output_dir, eddy_type+'_short.nc')
    short_track.write_file(
        #filename="%(path)s/%(sign_type)s_track_too_short.nc", **kw_write
        filename=fout
    )

# %%
# %% get cluster
# import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
# import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
cluster
client

# %%
# root_dir = '/scratch/m/m300878/agulhas_eddies/exp9/'
root_dir = '/work/mh0033/m300878/eddy_tracks/eerie/eddy_tracks/'

varname    = 'ssh'

nb_obs_min = 100 # minimum of 10 points in track to be considered a long trajectory
raw        = True #
cmin       = 0.05 # minimum contour
virtual    = 4 # number of consecutive timesteps with missing detection allowed
class_kw   = dict(cmin=cmin)
zarr       = False

# %%
lazy_results = []
# again looping over wavelength and shape error
for wavelength in [700]:
    print('wavelength = ', wavelength)
    for shape_error in [70]:
        print('shape_error = ', shape_error)
        eddy_dir    = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
        # eddy_dir='/scratch/m/m300878/eddy2/'+'_eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
        # eddy_dir='/path/to/eddydata/'+'eddytrack/'+'wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))+'/'
        tracker_dir = eddy_dir+'tracks/'
        if not os.path.exists(tracker_dir):
            os.makedirs(tracker_dir)
        print('eddydir = ', eddy_dir)
        print('tracker_dir = ', tracker_dir)
        for eddy_type in ['cyclonic','anticyclonic']:
            #define computation we want to do without doing it
            lazy_result = (tracking_eddytype)(trackerdir = tracker_dir,
                                              eddydir    = eddy_dir,
                                              eddy_type  = eddy_type,
                                              zarr       = zarr,
                                              nb_obs_min = nb_obs_min,
                                              raw        = raw,
                                              cmin       = cmin)
            # store all computations to be done in parallel
            lazy_results.append(lazy_result)  
# compute the results
futures = dask.compute(*lazy_results)
results = dask.compute(*futures)
# %%
wavelength  = 700
shape_error = 70#25
path        = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))+'tracks/'
ds_cyclonic = xr.open_dataset(path+'anticyclonic_tracks.nc')

# %%
import cartopy 
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.ticker import (LongitudeFormatter, LatitudeFormatter,LatitudeLocator)
import matplotlib.pyplot as plt
# %matplotlib inline
# %%
lonshift = 0
proj     = ccrs.PlateCarree(central_longitude = lonshift)

fig, ax = plt.subplots(1, 1, figsize=(80, 40), subplot_kw={'projection': proj})
ax.set_title('Anticyclonic ocean-eddy tracks', loc='center', fontsize=50)
ax.set_title('2002-01-01 to 2008-12-31', loc='left', fontsize=50)
ax.set_title('(wavelength,shape_error)'+str(int(wavelength))+' '+str(int(shape_error)), loc='right', fontsize=50)
gl            = ax.gridlines(draw_labels=True, y_inline=True) # need cartopy version 0.18!!!
gl.top_labels = False
gl.xformatter = LongitudeFormatter()
gl.yformatter = LatitudeFormatter()

land = cfeature.NaturalEarthFeature(
        category  = 'physical',
        name      = 'land',
        scale     = '50m',
        facecolor = 'grey')
ax.add_feature(land)

ax.scatter(ds_cyclonic.longitude.values, ds_cyclonic.latitude.values, marker='o', s=100, alpha=1, color='red')
# %%
plt.figure()
plt.scatter(ds_cyclonic.isel(NbSample=2).effective_contour_longitude.values, ds_cyclonic.isel(NbSample=2).effective_contour_latitude.values, marker='o', s=1, alpha=1, color='red')
# plt.ylim(-36,-20)
# plt.xlim(0,20)
# %%
