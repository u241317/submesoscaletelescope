# %%
import sys
import glob, os
import pyicon as pyic
# import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
# import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 
sys.path.insert(0, '../')
import functions as fu

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html
from datetime import datetime, timedelta
from netCDF4 import Dataset

from matplotlib.ticker import FormatStrFormatter
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()

from importlib import reload
import dask
import math 

from py_eddy_tracker.dataset.grid import RegularGridDataset

import intake


# %%
reload(fu)

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
cluster
client

# %%
cat        = intake.open_catalog("https://raw.githubusercontent.com/eerie-project/intake_catalogues/main/eerie.yaml")
model      = 'icon-esm-er'
expid      = 'eerie-control-1950'
gridspec   = 'gr025'
realm      = 'ocean'
cat_regrid = cat['dkrz.disk.model-output'][model][expid][realm][gridspec]
print(list(cat_regrid))

# %%
ds = cat_regrid['2d_daily_mean'].to_dask()
ds = ds.ssh.isel(time=slice(0,365))
# %%
tsize      = ds.time.size
start      = ds.time[0].values
start      = np.datetime64(start, 's')
timestr    = pd.date_range(start, periods=tsize, freq='D')
ds['time'] = timestr
# %%
datearrs = ds.time.values
datearrs = pd.to_datetime(datearrs)
varname  = 'ssh'
root_dir = '/work/mh0033/m300878/eddy_tracks/eerie/eddy_tracks/'

# %% Computation
reload(fu)

for wavelength in [700]:
    for shape_error in [70]:
        print('wavelength, shape_error = ', wavelength, shape_error)
        print('year = ', datearrs.size)
        ntsteps_per_loop = 51
        ntsteps          = len(datearrs)
        tcounter         = np.zeros((ntsteps//ntsteps_per_loop)+2)
        tcounter[:-1]    = np.arange(0,(ntsteps//ntsteps_per_loop)+1)*ntsteps_per_loop
        tcounter[-1]     = ntsteps
        dask_steps       = math.ceil(ntsteps/ntsteps_per_loop)
        for x in range(dask_steps):
            print('tt vals = ', np.arange(tcounter[x],tcounter[x+1],1))
            lazy_results = []
            for tt in np.arange(tcounter[x],tcounter[x+1],1):
                tt = int(tt)
                lazy_result = dask.delayed(fu.detection_save_netcdf_output)(
                                                varfile     = ds, # dask.delayed
                                                varname     = varname,
                                                datearr     = datearrs[tt],
                                                tt          = int(tt),
                                                wavelength  = wavelength,
                                                shape_error = shape_error,
                                                root_dir    = root_dir)

                lazy_results.append(lazy_result)  

            futures = dask.compute(*lazy_results)
            results = dask.compute(*futures)




# %%#####################################################
# Test plot
reload(fu)
tt          = 100
i           = 0
wavelength  = 700
shape_error = 70
da = ds.isel(time=tt).load(scheduler='sync')
a0, c0, g0 = fu.detection(da,varname,datearrs[tt],tt,wavelength,shape_error)


# %%
reload(fu)
ax = fu.start_axes(f"Eddies detected over SSH for t={tt}")
m = g0.display(ax, "ssh", vmin=-0.15, vmax=0.15)
a0.display(
    ax,
    lw=0.75,
    label="Anticyclones in the filtered grid ({nb_obs} eddies)",
    ref=-25,
    color="green",
)
c0.display(
    ax,
    lw=0.75,
    label="Cyclones in the filtered grid ({nb_obs} eddies)",
    ref=-25,
    color="blue",
)
ax.legend()
# plt.ylim(-36, -20)
# plt.xlim(-15, 20)
fu.update_axes(ax, m)
# %%
