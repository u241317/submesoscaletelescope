# %%
import numpy as np
import xarray as xr
import pandas as pd
import intake
import os,sys
from datetime import datetime
from dask.diagnostics import ProgressBar
import time as T
import pyicon as pyic

import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings("ignore")


from py_eddy_tracker.dataset.grid import RegularGridDataset
from datetime import datetime, timedelta

from netCDF4 import Dataset
from matplotlib import pyplot as plt
import xarray as xr

import pandas as pd
import netCDF4 as nc
import datetime          #https://docs.python.org/3/library/datetime.html

from importlib import reload
import pyicon as pyic

#
from matplotlib import pyplot as plt
from matplotlib.pyplot import cm 
# from mpl_toolkits.basemap import Basemap
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER,LATITUDE_FORMATTER
import matplotlib.ticker as mticker


# %%
def get_area(da,mask=False):
    print('Computing grid-box area')
    import iris.analysis as ia
    if 'time' in da.dims:
        da = da.isel(time=0).drop('time')
    d = da.to_iris()
    d.coord('longitude').guess_bounds()
    d.coord('latitude').guess_bounds()

    area_weights = ia.cartography.area_weights(d)
    area = xr.ones_like(da) * area_weights
    if mask:
        area = area.where(~np.isnan(da))
    area = area.rename('area').load()
    area.attrs['long_name'] = 'grid_box_area'
    area.attrs['units'] = 'm^2'
    return area

def print_var(ds,filt=None):
    '''
    Print variables (varname,name) in Dataset. 
    If <filt> is provided, print only those where <filt> is present in the <name> attribute (ds[shortname].attrs['name'])

    Usage: print_var(ds,'wind')
    Output: printout of variables in ds whose name (long name in attributes, not short name to access) contains 'wind', e.g.
        10si  :   10 metre wind speed
        10u  :   10 metre U wind component
        10v  :   10 metre V wind component
    '''
    if filt:
        [print('%10s  :   %s' % (d,ds[d].attrs['name'])) for d in ds if filt.lower() in ds[d].attrs['name'].lower()]
    else:
        [print('%10s  :   %s' % (d,ds[d].attrs['name'])) for d in ds]


def start_axes(title):
    fig = plt.figure(figsize=(13, 5))
    ax = fig.add_axes([0.03, 0.03, 0.90, 0.94])
    ax.set_xlim(0,360), ax.set_ylim(-75,75)
    # ax.set_ylim(-50,-5)
    # ax.set_xlim(-25, 35)
    ax.set_aspect("equal")
    ax.set_title(title, weight="bold")
    return ax

def update_axes(ax, mappable=None):
    ax.grid()
    if mappable:
        plt.colorbar(mappable, cax=ax.figure.add_axes([0.94, 0.05, 0.01, 0.9]))

def plot_data(g):
    fig = plt.figure(figsize=(14, 12))
    ax = fig.add_axes([0.02, 0.51, 0.9, 0.45])
    ax.set_title("SSH (m)")
    ax.set_ylim(-50, -5)
    ax.set_aspect("equal")
    m = g.display(ax, name="zos", vmin=-1, vmax=1)
    ax.grid(True)
    plt.colorbar(m, cax=fig.add_axes([0.94, 0.51, 0.01, 0.45]))

# # Function that identifies the eddies
def detection(ncfile,varname,date,tt,wavelength,shape_error):
    # print('varfile', varfile)
    # wavelength: choice of spatial cutoff for high pass filter in km
    step_ht=0.005 #intervals to search for closed contours (5mm in this case)

    # g = RegularGridDataset(datafiles[0], "lon", "lat", centered=True, indexs={'time':tt*2})
    # da = ncfile.isel(time=tt).load(scheduler='sync')
    da_netcdf = Dataset('in-mem-file', mode='r', memory=ncfile.to_netcdf())
    g = RegularGridDataset(None, "lon", "lat", centered=True, nc4file=da_netcdf)  # NOTE: Using 'None' for the .nc file path then requires specifying directly the netcdf4 variable in memory

    # date = datearr[tt] # detect each timestep individually because of memory issues
    g.add_uv(varname)
    g.bessel_high_filter(varname, wavelength, order=1)
    # plot_data(g)
    print('filter done')
    a, c = g.eddy_identification(varname, "u", "v", 
        date,  # Date of identification
        step_ht,  # step between two isolines of detection (m)
        pixel_limit=(50, 400),  # Min and max pixel count for valid contour # fro 0.3 deg
        # pixel_limit=(800, 4000),  # Min and max pixel count for valid contour # for 0.02deg
        shape_error=shape_error  # Error max (%) between ratio of circle fit and contour
        )
    print('detection done')
    return a,c,g

# def detection(ncfile, varname, date): 
#     wavelength=700  #choice of spatial cutoff for high pass filter in km
#     step_ht=0.005 #intervals to search for closed contours (5mm in this case)
#     g = RegularGridDataset(None, "lon", "lat", centered=True, nc4file=ncfile)  # NOTE: Using 'None' for the .nc file path then requires specifying directly the netcdf4 variable in memory
#     g.add_uv(varname)
#     g.bessel_high_filter(varname, wavelength, order=1)
        
#     a, c = g.eddy_identification(varname, "u", "v", 
#     date,  # Date of identification
#     step_ht,  # step between two isolines of detection (m)
#     pixel_limit=(50, 400),  # Min and max pixel count for valid contour
#     shape_error=70  # Error max (%) between ratio of circle fit and contour
#     )
#     return a,c,g

# Parallel function wrapper to the for-loop 
def delayed_ID_and_save(date, tt):
    
    # Load data from xarray into netcdf4 type
    da_ssh = ds_subset.isel(time=tt).ssh
    da_netcdf = Dataset('in-mem-file', mode='r', memory=da_ssh.to_netcdf())
    
    #print('Identifying daily eddies for '+date.strftime('%Y%m%d'))
    a_filtered, c_filtered, g_filtered = detection(da_netcdf,varname,date)
    with Dataset(date.strftime(outdir+"eddyID_anticyclonic_"+date.strftime('%Y%m%d')+".nc"), "w") as h:
        a_filtered.to_netcdf(h)
    with Dataset(date.strftime(outdir+"eddyID_cyclonic_"+date.strftime('%Y%m%d')+".nc"), "w") as h:
        c_filtered.to_netcdf(h)
    del a_filtered
    del c_filtered
    del g_filtered
    del date

# %%
# function that writes data to netCDF files
def detection_save_netcdf_output(varfile, varname, datearr, tt, wavelength, shape_error, root_dir):
    outdir = f'{root_dir}/eddytrack_wv_{int(wavelength)}_se_{int(shape_error)}'
    date   = datearr
    print('date = ', date)
    print('tt = ', tt)
    print('Identifying daily eddies for ', date)
    varfile2 = varfile.isel(time=tt).load(scheduler='sync')
    a_filtered, c_filtered, g_filtered = detection(varfile2,varname,date,tt,wavelength,shape_error)
    print(g_filtered)
    with Dataset(date.strftime(outdir+"_anticyclonic_"+date.strftime('%Y%m%d%H')+".nc"), "w") as h:
        a_filtered.to_netcdf(h)
    with Dataset(date.strftime(outdir+"_cyclonic_"+date.strftime('%Y%m%d%H')+".nc"), "w") as h:
        c_filtered.to_netcdf(h)
    del a_filtered
    del c_filtered
    del g_filtered
    del date
    print('save to netcdf done')

def plot_eddy_contours_dian(dstracks, newARtrackid):

    fig = plt.figure(figsize=(5, 5))
    #ax = fig.add_axes([0.05, 0.03, 0.90, 0.94])

    projection = ccrs.PlateCarree(central_longitude=5)
    # projection = ccrs.LambertConformal(central_longitude=5, central_latitude=-35)
    ax = plt.axes(projection=projection)
    ax.coastlines()
    ax.add_feature(cfeature.LAND,zorder=1,edgecolor='k')

    for tridx in newARtrackid:
        con_lon=dstracks.effective_contour_longitude.values[np.argwhere(dstracks.track.values==tridx)].squeeze()
        con_lat=dstracks.effective_contour_latitude.values[np.argwhere(dstracks.track.values==tridx)].squeeze()
        con_lon=np.where(con_lon>350,con_lon-360,con_lon)
        color = iter(cm.rainbow(np.linspace(0, 1, 1331)))
        for tt in range(con_lon.shape[0]):
            cline = next(color)
            ax.plot(con_lon[tt,:],con_lat[tt,:], c=cline)

    ax.set_title('Long-lived Agulhas anticyclones')
    ax.set_extent([-6, 8, -33, -23], ccrs.PlateCarree())
    ax.gridlines(color='gray',linestyle='--',linewidth=0.5,draw_labels=True, alpha=0.5)
    ax.xlocator = mticker.FixedLocator(np.arange(0,20,5))
    ax.xformatter = LONGITUDE_FORMATTER
    # ax.ylocator = mticker.FixedLocator(np.arange(-45,-20,5))
    ax.yformatter = LATITUDE_FORMATTER
    ax.xlabels_top = False
    ax.ylabels_right = False

    sm = plt.cm.ScalarMappable(cmap=cm.rainbow, norm=plt.Normalize(vmin=0, vmax=250))
    plt.colorbar(sm,orientation='horizontal')

    plt.show()



# %%
