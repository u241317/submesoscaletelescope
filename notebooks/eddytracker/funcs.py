from py_eddy_tracker.dataset.grid import RegularGridDataset
from datetime import datetime, timedelta
import numpy as np
from netCDF4 import Dataset
from matplotlib import pyplot as plt
import xarray as xr

import pandas as pd
import netCDF4 as nc
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic

#
from matplotlib import pyplot as plt
from matplotlib.pyplot import cm 
# from mpl_toolkits.basemap import Basemap
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER,LATITUDE_FORMATTER
import matplotlib.ticker as mticker


def start_axes(title):
    fig = plt.figure(figsize=(13, 5))
    ax = fig.add_axes([0.03, 0.03, 0.90, 0.94])
    # ax.set_xlim(0,360), ax.set_ylim(-75,75)
    ax.set_ylim(-50,-5)
    ax.set_xlim(-25, 35)
    ax.set_aspect("equal")
    ax.set_title(title, weight="bold")
    return ax

def update_axes(ax, mappable=None):
    ax.grid()
    if mappable:
        plt.colorbar(mappable, cax=ax.figure.add_axes([0.94, 0.05, 0.01, 0.9]))

def plot_data(g):
    fig = plt.figure(figsize=(14, 12))
    ax = fig.add_axes([0.02, 0.51, 0.9, 0.45])
    ax.set_title("SSH (m)")
    ax.set_ylim(-50, -5)
    ax.set_aspect("equal")
    m = g.display(ax, name="zos", vmin=-1, vmax=1)
    ax.grid(True)
    plt.colorbar(m, cax=fig.add_axes([0.94, 0.51, 0.01, 0.45]))

# Function that identifies the eddies
def detection(varfile,varname,date,tt,wavelength,shape_error):
    print('varfile', varfile)
    # wavelength: choice of spatial cutoff for high pass filter in km
    step_ht=0.005 #intervals to search for closed contours (5mm in this case)
    print('select every second date manually!')
    g = RegularGridDataset(varfile, "lon", "lat", centered=True, indexs={'time':tt*2})
    # date = datearr[tt] # detect each timestep individually because of memory issues
    g.add_uv(varname)
    g.bessel_high_filter(varname, wavelength, order=1)
    # plot_data(g)
    print('filter done')
    a, c = g.eddy_identification(varname, "u", "v", 
        date,  # Date of identification
        step_ht,  # step between two isolines of detection (m)
        pixel_limit=(20, 400),  # Min and max pixel count for valid contour # fro 0.3 deg
        # pixel_limit=(800, 4000),  # Min and max pixel count for valid contour # for 0.02deg
        shape_error=shape_error  # Error max (%) between ratio of circle fit and contour
        )
    print('detection done')
    return a,c,g
# %%
# function that writes data to netCDF files
def detection_save_netcdf_output(varfile, varname, datearr, tt, wavelength, shape_error, root_dir):
    outdir = f'{root_dir}/eddytrack_wv_{int(wavelength)}_se_{int(shape_error)}'
    date   = datearr
    print('date = ', date)
    print('tt = ', tt)
    print('Identifying daily eddies for ', date)
    a_filtered, c_filtered, g_filtered = detection(varfile,varname,date,tt,wavelength,shape_error)
    print(g_filtered)
    with Dataset(date.strftime(outdir+"_anticyclonic_"+date.strftime('%Y%m%d%H')+".nc"), "w") as h:
        a_filtered.to_netcdf(h)
    with Dataset(date.strftime(outdir+"_cyclonic_"+date.strftime('%Y%m%d%H')+".nc"), "w") as h:
        c_filtered.to_netcdf(h)
    del a_filtered
    del c_filtered
    del g_filtered
    del date
    print('save to netcdf done')

def plot_eddy_contours_dian(dstracks, newARtrackid):

    fig = plt.figure(figsize=(5, 5))
    #ax = fig.add_axes([0.05, 0.03, 0.90, 0.94])

    projection = ccrs.PlateCarree(central_longitude=5)
    # projection = ccrs.LambertConformal(central_longitude=5, central_latitude=-35)
    ax = plt.axes(projection=projection)
    ax.coastlines()
    ax.add_feature(cfeature.LAND,zorder=1,edgecolor='k')

    for tridx in newARtrackid:
        con_lon=dstracks.effective_contour_longitude.values[np.argwhere(dstracks.track.values==tridx)].squeeze()
        con_lat=dstracks.effective_contour_latitude.values[np.argwhere(dstracks.track.values==tridx)].squeeze()
        con_lon=np.where(con_lon>350,con_lon-360,con_lon)
        color = iter(cm.rainbow(np.linspace(0, 1, 1331)))
        for tt in range(con_lon.shape[0]):
            cline = next(color)
            ax.plot(con_lon[tt,:],con_lat[tt,:], c=cline)

    ax.set_title('Long-lived Agulhas anticyclones')
    ax.set_extent([0, 20, -37, -20], ccrs.PlateCarree())
    ax.gridlines(color='gray',linestyle='--',linewidth=0.5,draw_labels=True, alpha=0.5)
    ax.xlocator = mticker.FixedLocator(np.arange(0,20,5))
    ax.xformatter = LONGITUDE_FORMATTER
    # ax.ylocator = mticker.FixedLocator(np.arange(-45,-20,5))
    ax.yformatter = LATITUDE_FORMATTER
    ax.xlabels_top = False
    ax.ylabels_right = False

    sm = plt.cm.ScalarMappable(cmap=cm.rainbow, norm=plt.Normalize(vmin=0, vmax=250))
    plt.colorbar(sm,orientation='horizontal')

    plt.show()

def plot_spectra_KE(P_exp7, P_exp9, lat_mean,  fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=3)
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.loglog(P_exp7.freq_time[1:]*86400,   P_exp7.mean(['x','y'], skipna=True)[1:], label=f'tides Jul-Oct 2019 (t={P_exp7.freq_time.size*2/12:.0f}d)')
    ax.loglog(P_exp9.freq_time[1:]*86400,   P_exp9.mean(['x','y'], skipna=True)[1:], label=f'no tides Jul-Oct 2019 (t={P_exp9.freq_time.size*2/12:.0f}d)')
    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.ylim(1e-2,5e3)
    plt.legend(loc='upper right')
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title('Power Spectral Density of KE at 98m')

    plt.vlines(eva.tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )
    freq_names = eva.tidefreqnames()
    freq_tides = eva.tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-2
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(eva.corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(eva.corfreq(lat_mean)/2/np.pi-0.1*eva.corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')
