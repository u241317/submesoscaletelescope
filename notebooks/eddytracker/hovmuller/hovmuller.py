# %%
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt

import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
reload(eva)
import sys
sys.path.insert(0, '../')
# import funcs as fu
import xrft


# %%
fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/'
savefig  = False
# %%
wavelength  = 700  #choice of spatial cutoff for high pass filter in km
shape_error = 25 #choice of shape error in km
dlon        = 1.8
dlat        = 1.8
eddy_type   = 'anticyclonic'
ID          = 0
# %%
exp                 = 'exp9'
root_dir            = f'/work/mh0033/m300878/eddy_tracks/agulhas_eddies/{exp}/'
path_save_composite = f'/composite_{exp}_KE_100m/'
eddy_dir            = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
tracker_dir         = eddy_dir+'tracks/'

ds_exp9 = xr.open_dataset(tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+f'deg_trackID_{ID}.nc')

# %%
exp                 = 'exp7'
root_dir            = f'/work/mh0033/m300878/eddy_tracks/agulhas_eddies/{exp}_2/'
path_save_composite = f'/composite_{exp}_KE_100m/'
# path_save_composite = f'/composite_{exp}_KE_3d/'

eddy_dir            = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
tracker_dir         = eddy_dir+'tracks/'

ds_exp7 = xr.open_dataset(tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+f'deg_trackID_{ID}.nc')

# %%
path = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/smtwv0007/outdata_u/smtwv0007_oce_3d_u_PT1H_20190817T051500Z.nc'
ds = xr.open_dataset(path)

exp                 = 'exp7'
root_dir            = f'/work/mh0033/m300878/eddy_tracks/agulhas_eddies/{exp}_2/'
path_save_composite = f'/composite_{exp}_KE_3d/'

eddy_dir            = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
tracker_dir         = eddy_dir+'tracks/'

ds_exp7_3d      = xr.open_dataset(tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+f'deg_trackID_{ID}.nc')
ds_exp7_3d      = ds_exp7_3d.rename({'x':'clon','y':'clat'})
ds_exp7_3d['z'] = ds.depth[0:60].data # reconstruct depth coordinate
ds_exp7_3d      = ds_exp7_3d.rename({'z':'depth'})



# %% select comon time
ds_exp9 = ds_exp9.sel(time=slice(ds_exp7.time[0],ds_exp7.time[-1]))


# %%scatter eddy track
fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/'

plt.figure()
cb = plt.scatter(ds_exp9.lon, ds_exp9.lat, c=ds_exp9.time, label='exp9', cmap='Reds', s=0.02)
cb = plt.scatter(ds_exp7.lon, ds_exp7.lat, c=ds_exp7.time, label='exp7', cmap='Blues',s=0.02)
plt.colorbar(cb, label='time')
plt.title('Eddy tracks')
plt.legend()
plt.savefig(fig_path+'/tracks/'+'eddy_tracks.png', dpi=150)
plt.xlabel('longitude')
plt.ylabel('latitude')


# %%
plt.figure()
ds_exp7.effective_area.plot(label='exp7')
ds_exp9.effective_area.plot(label='exp9')
plt.legend()
if savefig == True: plt.savefig(fig_path+'effective_area.png', dpi=150)

# %% #################### Hovmüller plot of KE #########################
plot_properties = {
    "pr": {"vmin": 0.0, "vmax": 15.0 / (24 * 60 * 60), "cmap": "inferno"},
    "uas": {"vmin": -8.0, "vmax": 8.0, "cmap": "PiYG"},
    "prw": {"vmin": 20.0, "vmax": 60.0, "cmap": "magma"},
}
import matplotlib.dates as mdates

def format_axes_lat(ax):
    ax.set_ylabel("latitude / degN")

    ax.grid(True)
    ax.xaxis.set_major_locator(mdates.MonthLocator(bymonth=(1, 7)))
    ax.xaxis.set_minor_locator(mdates.MonthLocator())
    ax.xaxis.set_major_formatter(
        mdates.ConciseDateFormatter(ax.xaxis.get_major_locator())
    )
    ax.set_yscale(
        "function",
        functions=(
            lambda d: np.sin(np.deg2rad(d)),
            lambda d: np.rad2deg(np.arcsin(np.clip(d, -1, 1))),
        ),
    )
    ax.set_ylim(-90, 90)
    ax.set_yticks([-50, -30, -10, 0, 10, 30, 50])


def format_axes_lon(ax):
    ax.set_xticks(np.arange(7) * 60)
    ax.set_xlabel("longitude / degE")

    ax.yaxis.set_major_locator(mdates.MonthLocator())
    ax.yaxis.set_minor_locator(mdates.MonthLocator())
    ax.grid(True)
    ax.yaxis.set_major_formatter(
        mdates.ConciseDateFormatter(ax.yaxis.get_major_locator())
    )


# %% 
fig_path ='/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/KE/hovmuller/'

# rename x to lon
ds7 = ds_exp7.rename({'x':'clon','y':'clat'})
# data gap 
import pandas as pd
time0 = pd.to_datetime('2019-07-16T07:00:00.000000000')
time1 = pd.to_datetime('2019-07-18T23:00:00.000000000')

fig, ax = plt.subplots(1,2,  figsize=(6, 6), sharey=True, constrained_layout=True)

ds7.KE.isel(clat=90).plot(y='time', ax=ax[0], add_colorbar=False, cmap='inferno')
# ax[0].pcolormesh( ds7.KE.isel(clat=90).clon, ds7.KE.isel(clat=90).time, ds7.KE.isel(clat=90), cmap='inferno')
ds7.KE.isel(clon=90).plot(y='time', ax=ax[1], add_colorbar=True, cmap='inferno')

# fill data gap white
ax[0].pcolormesh([ds7.KE.isel(clat=90).clon[0], ds7.KE.isel(clat=90).clon[-1]], [time0, time1], [[0,0],[0,0]], cmap='Blues', alpha=1)
ax[1].pcolormesh([ds7.KE.isel(clon=90).clat[0], ds7.KE.isel(clon=90).clat[-1]], [time0, time1], [[0,0],[0,0]], cmap='Blues', alpha=1)

if savefig == True:
    fig.savefig(fig_path+'KE_exp72.png', dpi=150)
# %%
ds9 = ds_exp9.rename({'x':'clon','y':'clat'})

fig, ax = plt.subplots(1,2,  figsize=(6, 6), sharey=True, constrained_layout=True)

ds9.KE.isel(clat=90).plot(y='time', ax=ax[0], add_colorbar=False, cmap='inferno')
ds9.KE.isel(clon=90).plot(y='time', ax=ax[1], add_colorbar=True, cmap='inferno')

if savefig == True:
    fig.savefig(fig_path+'KE_exp9.png', dpi=150)


# %% 3d compare to 2d dataset
ds7 = ds_exp7.rename({'x':'clon','y':'clat'}).dropna('time')


fig, ax = plt.subplots(1,2,  figsize=(6, 6), sharey=True, constrained_layout=True)
# ds7.KE.isel(clat=90).plot(y='time', ax=ax, add_colorbar=False, cmap='inferno')
ds7.KE.isel(clat=90).plot(y='time', ax=ax[0], add_colorbar=False, cmap='inferno')
ds_exp7_3d.KE.dropna('time').isel(clat=90).isel(depth=27).plot(y='time', ax=ax[1], add_colorbar=False, cmap='inferno')

# %%

fig, ax = plt.subplots(1,4,  figsize=(12, 6), sharey=True, constrained_layout=True)
ds_exp7_3d.KE.dropna('time').isel(clat=90).isel(depth=0).plot(y='time', ax=ax[0], add_colorbar=False, cmap='inferno')
ds_exp7_3d.KE.dropna('time').isel(clat=90).isel(depth=17).plot(y='time', ax=ax[1], add_colorbar=False, cmap='inferno')
ds_exp7_3d.KE.dropna('time').isel(clat=90).isel(depth=27).plot(y='time', ax=ax[2], add_colorbar=False, cmap='inferno')
ds_exp7_3d.KE.dropna('time').isel(clat=90).isel(depth=57).plot(y='time', ax=ax[3], add_colorbar=False, cmap='inferno')

plt.ylim([ds_exp7_3d.time[300], ds_exp7_3d.time[-3]])

if savefig == True:
    fig.savefig(fig_path+'KE_exp7_4.png', dpi=150)

# %%
import matplotlib.gridspec as gridspec

fig_path = fig_path+'KE/hovmuller/depth_time/'

# %%
def plot_hov(clon, clat):
    fig, ax = plt.subplots(1,2,  figsize=(8, 4), sharey=False, constrained_layout=True)
    # ds_exp7_3d.KE.dropna('time').isel(clat=90).isel(clon=45).plot(y='time', ax=ax[0], add_colorbar=False, cmap='inferno')
    ds_exp7_3d.KE.dropna('time').isel(clat=clat).isel(clon=clon).plot(y='depth', ax=ax[0], add_colorbar=True, cmap='inferno')
    plt.xlim([ds_exp7_3d.time[300], ds_exp7_3d.time[-3]])
    plt.gca().invert_yaxis()

def plot_hov_p(clon, clat):
    fig = plt.figure(figsize=(10, 5), constrained_layout=True)
    gs = gridspec.GridSpec(1, 3) 
    ax1 = fig.add_subplot(gs[0, :2])  # First subplot spans first 2 columns
    ax2 = fig.add_subplot(gs[0, 2])  
    ds = ds_exp7_3d.KE.isel(clat=clat).isel(clon=clon).isel(time=slice(200,-3))
    # interpolate timegaps
    ds = ds.interpolate_na(dim='time')
    print('interpolate time gaps')
    ax1.pcolormesh(ds.time, ds.depth, ds.transpose(), cmap='inferno')
    # ax1.set_xlim([ds_exp7_3d.time[300], ds_exp7_3d.time[-3]])
    ax1.set_ylim([ 500,0])
    ax1.set_ylabel('depth [m]')
    ax1.set_xlabel('time')
    # incline xlabel
    for label in ax1.get_xticklabels():
        label.set_rotation(30)
    
    ax1.set_title(f'KE at lon={ds_exp7_3d.clon[clon].values}, lat={ds_exp7_3d.clat[clat].values}')

    for i in np.arange(ds.time.size):
        ax2.plot(ds.isel(time=i), ds.depth, color='grey', alpha=0.5)
    ax2.plot(ds.mean('time'), ds.depth, color='k', lw=3)
    ax2.set_ylim([ 500,0])
    ax2.set_xlabel('KE')
    ax2.yaxis.set_visible(False)
    if savefig == True:
        fig.savefig(fig_path+f'KE_exp7_lon{clon}_lat{clat}.png', dpi=150)
# %%
clon = np.array([10, 30, 40, 50 ,60, 70, 80, 90])
clat = np.ones_like(clon)*90
# clat = np.array([10, 30, 40, 50 ,60, 70, 80, 90])
# clon = np.ones_like(clat)*90
plt.figure()
ds_exp7_3d.isel(time=301).isel(depth=35).KE.plot(vmin=0, vmax=5, cmap='inferno')
plt.scatter(ds_exp7_3d.clon[clon], ds_exp7_3d.clat[clat], c='g', s=30, zorder=10)
if savefig == True:
    plt.savefig(fig_path+'eddy_lat_lon.png', dpi=150)
# %%
for i,j in zip(clon, clat):
    plot_hov_p(i,j)


# %%
from scipy.signal import butter,filtfilt
def butter_filter(data, cutoff, fs, order, btype):
    b, a = butter(order, cutoff, btype=btype, analog=False, fs=fs)
    y = np.apply_along_axis(lambda m: filtfilt(b, a, m), axis=0, arr=data)
    return y

ds_KE = ds_exp7_3d.KE.sel(time=slice(ds_exp7_3d.time[300], ds_exp7_3d.time[-3])).interpolate_na(dim='time')
# %%
# bandpass filter
# fileter near inertial frequency
fs = 1/2 #every two hours
order = 6
cutoff = [1/8, 1/5] #high_freq
cutoff = [1/26, 1/22]
btype = 'bandpass'
y = butter_filter(ds_KE.fillna(0.), cutoff, fs, order, btype)
# make xarray
y_xr = xr.DataArray(y, coords=[ds_KE.time, ds_KE.depth, ds_KE.clat, ds_KE.clon], dims=['time', 'depth', 'clat', 'clon'])
# %%
def plot_hov(clon, clat):
    fig, ax = plt.subplots(1,1,  figsize=(8, 4), sharey=True, constrained_layout=True)
    # ds_exp7_3d.KE.dropna('time').isel(clat=90).isel(clon=45).plot(y='time', ax=ax[0], add_colorbar=False, cmap='inferno')
    y_xr.isel(clat=clat).isel(clon=clon).plot(y='depth', ax=ax, vmin=-0.05, vmax=0.05, add_colorbar=True, cmap='RdBu_r')
    # plt.xlim([ds_exp7_3d.time[300], ds_exp7_3d.time[-3]])
    plt.gca().invert_yaxis()
    # ax.set_title(f'KE at lon={ds_exp7_3d.clon[clon].values}, lat={ds_exp7_3d.clat[clat].values}')
    if savefig == True:
        fig.savefig(fig_path+'bandpass/hifreq/'+f'KE_exp7_lon{clon}_lat{clat}_bandpass_zonal.png', dpi=150)
# %%
for i,j in zip(clon, clat):
    plot_hov(i,j)


# %% Sanford 75 decomposition

P0 = ds_KE.isel(clon=50, clat=90)
# %% substract background
dt =7
KE_bg = 0.5*(P0[0:-dt,:].data + P0[dt:,:].data)
KE_niw = 0.5*(P0[0:-dt,:].data - P0[dt:,:].data)
# make xarray
KE_bg_xr = xr.DataArray(KE_bg, coords=[ds_KE.time[0:-dt], ds_KE.depth], dims=['time', 'depth'])
KE_niw_xr = xr.DataArray(KE_niw, coords=[ds_KE.time[0:-dt], ds_KE.depth], dims=['time', 'depth'])
# %%
KE_niw_xr.plot(y='depth') 
plt.gca().invert_yaxis()
# %%
np.abs(KE_bg_xr).plot(y='depth') 
plt.gca().invert_yaxis()
# %%
np.abs(KE_niw_xr).mean('time').plot(y='depth', label='NIW')
np.abs(KE_bg_xr).mean('time').plot(y='depth', label='BG')
plt.gca().invert_yaxis()
plt.legend()
# %%
