# %%
from datetime import datetime, timedelta
import numpy as np
from matplotlib import pyplot as plt
import os 
import glob

import numpy as np
from datetime import datetime, timedelta
import xarray as xr
# %%
import sys
sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic

sys.path.insert(0, '../')

import pandas as pd
# %%
import smt_modules.init_slurm_cluster as scluster 

reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='03:00:00', wait=False, scale=1)
cluster
client


# %%
exp = 7

# root_dir = '/scratch/m/m300878/agulhas_eddies/exp9/'
if exp == 7:
    root_dir = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/'
elif exp == 9:
    root_dir = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp9/'

varname     = 'zos'
wavelength  = 700  #choice of spatial cutoff for high pass filter in km
shape_error = 25 #choice of shape error in km
eddy_dir    = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
eddy_type   = 'anticyclonic'


mindays=20 #min number of days for tracked eddy

tracker_dir=eddy_dir+'tracks/'

dlon       = 2.5
dlat       = 2.5
res        = 0.01
npts       = int(dlon/res) #number of points from centre

dscorres  = xr.open_dataset(tracker_dir+eddy_type+'_correspondance.nc')
dstracks  = xr.open_dataset(tracker_dir+eddy_type+'_tracks.nc')
dsshort   = xr.open_dataset(tracker_dir+eddy_type+'_short.nc')
dsuntrack = xr.open_dataset(tracker_dir+eddy_type+'_untracked.nc')


# %%
#Get desired region [Agulhas rings and leakage]
# ARidx = np.argwhere((dstracks.latitude.values<=-30) & (dstracks.latitude.values>=-45) & (dstracks.longitude.values>0) & (dstracks.longitude.values<25))
ARidx = np.argwhere((dstracks.latitude.values<=-20) & (dstracks.latitude.values>=-36) & (dstracks.longitude.values>-15) & (dstracks.longitude.values<20))

#Get track IDs for Agulhas rings, remove all duplicates
ARtrackid=np.array(sorted(list(set(dstracks.track.values[ARidx].squeeze()))))
print('Track IDs=',ARtrackid)

#Get number of obs for each track for Agulhas rings
trackIDs=dstracks.track.values
tracklen=[]
for ii in range(trackIDs.max()+1):
    tracklen.append(len(np.argwhere(trackIDs == ii)))
lentrack=np.array(tracklen)[ARtrackid]
print('No. of obs for each tracked ID =',lentrack)

#Remove tracks with less than minimum number of days 
newARtrackid=np.delete(ARtrackid,np.r_[np.argwhere(lentrack<mindays)])
print('Track IDs that last more than ',mindays,' days=',newARtrackid)
lentrack=np.array(tracklen)[newARtrackid]
print('No. of obs for each tracked ID =',lentrack)
del(tracklen)


# %%
def geteddy_alongtrack(ds,lon,lat,dlon,dlat):
    lon    = round(lon,2)
    lat    = round(lat,2)
    lonmin = round(lon-dlon,2)
    lonmax = round(lon+dlon,2)
    latmin = round(lat-dlat,2)
    latmax = round(lat+dlat,2)

    ds_interp = interp_KE(ds, lon_reg=[lonmin, lonmax], lat_reg=[latmin, latmax])

    return ds_interp

def interp_KE(data, lon_reg, lat_reg):
    # fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
    fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
    data_interp = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    return data_interp

def extract_eddytrack_raw_sing(dstracks,tridx,ds,dlon,dlat,res):
    npts          = int(dlon/res)
    alongtrackidx = np.argwhere(dstracks.track.values==tridx)
    loncen        = dstracks.longitude.values[alongtrackidx]
    latcen        = dstracks.latitude.values[alongtrackidx]
    timearr       = dstracks.time.values[alongtrackidx].flatten()
    timearr       = pd.to_datetime(timearr)
    timearr       = xr.DataArray(timearr,coords={'time':timearr},dims=['time'])
    # interpolate missed observations
    ds_obs        = timearr
    ds_obs        = ds_obs.assign_coords(lon=('time', loncen.flatten()))
    ds_obs        = ds_obs.assign_coords(lat=('time', latcen.flatten()))

    time0   = pd.to_datetime(timearr[0].data)
    time1   = pd.to_datetime(timearr[-1].data)
    time_pd = pd.date_range(start=time0, end=time1, freq='2H')
    print('size timearr', timearr.size, 'size time_pd', len(time_pd))
    # Expand time array to 2h interval
    ds_obs = ds_obs.reindex(time=time_pd)

    # Interpolate NaNs in lat and lon coordinates
    ds_obs['lon'] = ds_obs['lon'].interpolate_na(dim='time', method='linear', fill_value="extrapolate")
    ds_obs['lat'] = ds_obs['lat'].interpolate_na(dim='time', method='linear', fill_value="extrapolate")

    timearr = time_pd #ds_obs.time.drop('lon').drop('lat')
    loncen  = ds_obs.lon.data
    latcen  = ds_obs.lat.data

    print('obs: start',timearr.values[0], '\n end',timearr.values[-1] )
    print('KE: start',ds.time.values[0], '\n end',ds.time.values[-1])
    if ds.time.size != len(timearr):
        print('mismatch time window')
        start0 = ds.time.values[0]
        end0   = ds.time.values[-1]
        start1 = timearr.values[0]
        end1   = timearr.values[-1]
        start  = max(start0,start1)
        end    = min(end0,end1)

        # find idx of closest time using pandas
        start_idx0 = np.argmin(np.abs(ds.time.values-start))
        end_idx0   = np.argmin(np.abs(ds.time.values-end))
        ds         = ds.isel(time=slice(start_idx0,end_idx0))

        start_idx1    = np.argmin(np.abs(timearr.values-start))
        end_idx1      = np.argmin(np.abs(timearr.values-end))
        # timearr       = timearr.isel(time=slice(start_idx1,end_idx1))
        timearr       = timearr[start_idx1:end_idx1]
        alongtrackidx = alongtrackidx[start_idx1:end_idx1]
        loncen        = loncen[start_idx1:end_idx1]
        latcen        = latcen[start_idx1:end_idx1]

        print('size timearr', timearr.size, 'size ds', ds.time.size)
        # print('reindex to fill data gaps')
        # ds = ds.reindex(time=timearr)
        print('obs: start',timearr.values[0], '\n end',timearr.values[-1] )
        print('KE: start',ds.time.values[0], '\n end',ds.time.values[-1])

    # DS = []
    for tt in range(len(timearr)):
        print('time',tt)
        dmvarfile = ds.isel(time=tt)
        FIELDcomp = geteddy_alongtrack(dmvarfile,loncen[tt],latcen[tt],dlon=dlon,dlat=dlat)
        FIELDcomp = FIELDcomp.assign_coords(lat=np.arange(-npts,npts)*res,lon=np.arange(-npts,npts)*res)
        compSSH   = xr.DataArray(data=np.array(FIELDcomp).squeeze(),
                 coords={ 'y':np.arange(-npts,npts)*res, 'x':np.arange(-npts,npts)*res, 'depth':ds.depth.values},
                 dims=['depth','y','x'],name=ds.name)
        compSSH=compSSH.assign_coords(time=timearr[tt])
        compSSH=compSSH.assign_coords(loncen=loncen[tt])
        compSSH=compSSH.assign_coords(latcen=latcen[tt])

        print('save time step',tt)
        fileout = tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+'deg_trackID_'+str(tridx)+'time_'+str(tt)+'.nc'
        compSSH.to_netcdf(fileout)
        # DS.append(compSSH)

    # DS = xr.concat(DS, dim='time')

    # fileout = tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+'deg_trackID_'+str(tridx)+'.nc'
    # DS.to_netcdf(fileout)

    return 


# def load_KE_3d_exp7():
#     reload(eva) # 2h interval combining 3d and 2d output

#     ds_u1 = eva.load_smt_wave_all('u', exp=7, remove_bnds=True).u
#     ds_u1 = ds_u1.drop('clon').drop('clat')

#     ds_v1 = eva.load_smt_wave_all('v', exp=7, remove_bnds=True).v
#     ds_v1 = ds_v1.drop('clon').drop('clat')

#     ds_KE = ds_u1**2 + ds_v1**2

#     return ds_KE

def load_3d_exp7(exp):
    reload(eva) # 2h interval combining 3d and 2d output

    # ds_u1 = eva.load_smt_wave_all('u', exp=7, remove_bnds=True).u
    # ds_u1 = ds_u1.drop('clon').drop('clat')

    # ds_v1 = eva.load_smt_wave_all('v', exp=7, remove_bnds=True).v
    # ds_v1 = ds_v1.drop('clon').drop('clat')

    # ds_w = eva.load_smt_wave_all('w', exp=7, remove_bnds=True).w
    # ds_w = ds_w.drop('clon').drop('clat')

    # ds_tke = eva.load_smt_wave_all('tke', exp=7, remove_bnds=True).tke
    # ds_tke = ds_tke.drop('clon').drop('clat')

    ds_to = eva.load_smt_wave_all('to', exp=exp, remove_bnds=True).to
    ds    = ds_to.drop('clon').drop('clat')

    # ds_so = eva.load_smt_wave_all('so', exp=7, remove_bnds=True).so
    # ds_so = ds_so.drop('clon').drop('clat')

    # ds_2d = eva.load_smt_wave_all('2d', exp=7, remove_bnds=True)


    return ds




# %%
#Extract composites along track and save to file
if exp==9:
    path_data          = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/exp9_KE_100m_fine.nc'
    # path_data          = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/exp9_KE_100m.nc'
    path_save_composite = '/composite_exp9_KE_100m/'
elif exp==7:
    # path_data          = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/exp7_KE_100m_fine.nc'
    # path_save_composite = '/composite_exp7_KE_100m/'
    ds = load_3d_exp7(exp=7)
    path_save_composite = f'/composite_vers2/{ds.name}/'
    # ds = ds.isel(depth=slice(0, 60))
    ds = ds.isel(time=slice(50, -1)) # time missing



# %%
tridx = 0
extract_eddytrack_raw_sing(dstracks,tridx,ds,dlon,dlat,res)

# %%
#load DS
path = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/anticyclonic_1.8x1.8deg_trackID_0.nc'
DS = xr.open_dataset(path)
