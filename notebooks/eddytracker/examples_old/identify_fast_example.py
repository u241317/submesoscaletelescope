# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../')

from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()

import smt_modules.init_slurm_cluster as scluster 
from importlib import reload
import dask

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='02:00:00')
cluster
client

# %%
from py_eddy_tracker.dataset.grid import RegularGridDataset
from datetime import datetime, timedelta
import numpy as np
from netCDF4 import Dataset
from matplotlib import pyplot as plt
import xarray as xr

#Read in example SSH data that has been mapped onto a 0.25deg regular grid.
expid='erc1011'
varname='ssh'
fq='dm'

# path to access ssh data to identify eddies
datadir = '/work/bm1344/k203123/reg25/erc1011/ssh/'
# %%
import glob
#find datafiles
datafiles = sorted(glob.glob(datadir+"*.nc"))
print('# data files for identifying eddies: ', len(datafiles))
print('datafiles for identifying eddies: ', datafiles)
#create datetime objects for 2002 to 2008; each year one entry in list
datearrs = []
for x in range(len(datafiles)):
    yyyy=int(2002+x)
    datearrs.append(np.arange(datetime(yyyy,1,1), datetime(yyyy+1,1,1), timedelta(days=1)).astype(datetime))
print('datearrs: ', datearrs)
# %%
# Function that identifies the eddies
def detection(varfile,varname,date,tt,wavelength,shape_error):
    # wavelength: choice of spatial cutoff for high pass filter in km
    step_ht=0.005 #intervals to search for closed contours (5mm in this case)
    g = RegularGridDataset(varfile, "lon", "lat", centered=True, indexs = dict(time=tt))
    # date = datearr[tt] # detect each timestep individually because of memory issues
    g.add_uv(varname)
    g.bessel_high_filter(varname, wavelength, order=1)

    a, c = g.eddy_identification(varname, "u", "v", 
    date,  # Date of identification
    step_ht,  # step between two isolines of detection (m)
    pixel_limit=(50, 400),  # Min and max pixel count for valid contour
    shape_error=shape_error  # Error max (%) between ratio of circle fit and contour
    )
    return a,c,g
# %%
from netCDF4 import Dataset
# function that writes data to netCDF files
def detection_save_netcdf_output(varfile, varname, datearr, tt, wavelength, shape_error,fq):
    outdir='/scratch/m/m300878/eddy/'+expid+'_eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
    date = datearr[tt]
    print('date = ', date)
    print('tt = ', tt)
    print('Identifying daily eddies for '+date.strftime('%Y%m%d'))
    a_filtered, c_filtered, g_filtered = detection(varfile,varname,date,tt,wavelength,shape_error)
    with Dataset(date.strftime(outdir+expid+"_anticyclonic_"+fq+"_"+date.strftime('%Y%m%d')+".nc"), "w") as h:
        a_filtered.to_netcdf(h)
    with Dataset(date.strftime(outdir+expid+"_cyclonic_"+fq+"_"+date.strftime('%Y%m%d')+".nc"), "w") as h:
        c_filtered.to_netcdf(h)
    del a_filtered
    del c_filtered
    del g_filtered
    del date
# %%
#looping over wavelengths for high band pass filter
for wavelength in [200,700]:
    #looping over shape_errors (%)
    for shape_error in [30,70]:
        print('wavelength, shape_error = ', wavelength, shape_error)
        # looping over year (2002,2003...)
        for i in range(len(datearrs)):
            print('year = ', datearrs[i][0].year)
            ntsteps_per_loop = 61
            ntsteps = len(datearrs[i])
            tcounter = np.zeros((ntsteps//ntsteps_per_loop)+2)
            tcounter[:-1] = np.arange(0,(ntsteps//ntsteps_per_loop)+1)*ntsteps_per_loop
            tcounter[-1] = ntsteps
            # looping over each set of 61 time steps i.e. 0-60,61-121,122-182,183-243,244-304,305-365
            for x in range(6):
                print('tt vals = ', np.arange(tcounter[x],tcounter[x+1],1))
                lazy_results = []
                for tt in np.arange(tcounter[x],tcounter[x+1],1):
                    # this defines the computation we want to do without actually doing it
                    lazy_result = dask.delayed(detection_save_netcdf_output)(varfile=datafiles[i], 
                                                                             varname=varname, 
                                                                             datearr=datearrs[i], 
                                                                             tt=int(tt), 
                                                                             wavelength=wavelength, 
                                                                             shape_error=shape_error,
                                                                             fq=fq)
                    # save computations to be done in a list
                    lazy_results.append(lazy_result)  
                # do the computations for next 61 time steps stored in lazy_results
                futures = dask.compute(*lazy_results)
                results = dask.compute(*futures)
# %%
# Test plot

tt=3
i=0
wavelength=700
shape_error=30
a0, c0, g0 = detection(datafiles[i],varname,datearrs[i][tt],tt,wavelength,shape_error)

# %%
def start_axes(title):
    fig = plt.figure(figsize=(13, 5))
    ax = fig.add_axes([0.03, 0.03, 0.90, 0.94])
    ax.set_xlim(0,360), ax.set_ylim(-75,75)
    ax.set_aspect("equal")
    ax.set_title(title, weight="bold")
    return ax


def update_axes(ax, mappable=None):
    ax.grid()
    if mappable:
        plt.colorbar(mappable, cax=ax.figure.add_axes([0.94, 0.05, 0.01, 0.9]))

# %%
ax = start_axes("Eddies detected over SSH for t=0")
m = g0.display(ax, "ssh", vmin=-0.15, vmax=0.15)
a0.display(
    ax,
    lw=0.75,
    label="Anticyclones in the filtered grid ({nb_obs} eddies)",
    ref=-10,
    color="red",
)
c0.display(
    ax,
    lw=0.75,
    label="Cyclones in the filtered grid ({nb_obs} eddies)",
    ref=-10,
    color="blue",
)
ax.legend()
update_axes(ax, m)
# %%
