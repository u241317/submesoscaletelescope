# %%
# first attempt unfinished
#Get Agulhas rings and composite

from matplotlib import pyplot as plt
from matplotlib.path import Path
from numpy import ones
from datetime import datetime
import numpy as np
import warnings
import xarray as xr

from py_eddy_tracker import data
from py_eddy_tracker.dataset.grid import RegularGridDataset, GridDataset
from py_eddy_tracker.observations.observation import EddiesObservations
from py_eddy_tracker.poly import create_vertice
import glob
import pandas as pd

import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
# %%
# %%
def rewrite_lon_lat(DATA):
    res  = 0.02
    npts = int(dlon/res) +1 #number of points from centre
    for ii in range(len(DATA)):
        print(ii)
        data = DATA[ii]
        xlen = data.lon.size
        ylen = data.lat.size
        if xlen!=int(2*npts) or ylen!=int(2*npts): 
            print('xlen = ', xlen, 'ylen = ', ylen)
            M = np.ones((int(2*npts),int(2*npts)))*np.nan
            M[int(npts-ylen/2):int(npts+ylen/2),int(npts-xlen/2):int(npts+xlen/2)] = data

            da         = xr.DataArray(M,dims=['lat','lon'],coords={'lat':np.arange(-npts,npts)*res,'lon':np.arange(-npts,npts)*res})
            da['time'] = data.time
            DATA[ii]   = da

        else:
            DATA[ii]=DATA[ii].assign_coords(lat=np.arange(-npts,npts)*res,lon=np.arange(-npts,npts)*res)
    return DATA


# %%

reload(eva)
path      = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/exp7_KE_100m.nc'
ds_KE     = xr.open_dataset(path)
ds_KE     = ds_KE.rename(__xarray_dataarray_variable__='KE')
mask_land = eva.load_smt_wave_land_mask()

lon_reg = [-15, 20]
lat_reg = [-36, -20]
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
mask_interp = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree, lon_reg, lat_reg)

ds_KE = ds_KE.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0], lat_reg[1]))
ds_KE = ds_KE * mask_interp
ds_KE = ds_KE.drop('depth')

# %%
if False:
    lon_reg = [-25, 35]
    lat_reg = [-50, -5]

    lon_reg = [-15, 10]
    lat_reg = [-36, -20]
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'

    grid_interp = xr.open_dataset(fpath_ckdtree)
    grid_interp = grid_interp.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0], lat_reg[1]))

    ds_KE_interp = ds_KE.interp_like(grid_interp, method='linear', assume_sorted=False, kwargs=None)


# %%
#find datafiles >>> times!
# exp7
# root_dir = '/scratch/m/m300878/agulhas_eddies/exp7/' #old
root_dir = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp9/'
datadir = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/03deg/smtwv0007_ssh'
# datadir = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/smtwv0007_ssh_02'
ds        = xr.open_dataset(datadir+'.nc')
datafiles = sorted(glob.glob(datadir+"*_masked2.nc"))

ds       = xr.open_dataset(datafiles[0])
datearrs = ds.time.values
datearrs = pd.to_datetime(datearrs)

# %%
# exp9
root_dir = '/scratch/m/m300878/agulhas_eddies/exp9/'
datadir = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/03deg/smtwv0009_ssh'
ds        = xr.open_dataset(datadir+'.nc')
datafiles = sorted(glob.glob(datadir+".nc"))

ds       = xr.open_dataset(datafiles[0])
datearrs = ds.time.values
datearrs = pd.to_datetime(datearrs)

# %% exp7 9 50 or 100m
path      = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/exp9_KE_100m.nc'
ds_KE     = xr.open_dataset(path)
# %%

varname     = 'zos'
wavelength  = 700  #choice of spatial cutoff for high pass filter in km
shape_error = 25 #choice of shape error in km
eddy_dir    = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
eddy_type   = 'anticyclonic'

dlon = 1.8
dlat = 1.8

# %%
IDn     = []
for i in range(datearrs.shape[0]):
            date = datearrs[i]
            # month with two digits
            if date.month < 10:
                month = '0'+str(date.month)
            else:
                month = date.month
            #day with two digits
            if date.day < 10:
                day = '0'+str(date.day)
            else:
                day = date.day

            warnings.filterwarnings("ignore", message="File was created with py-eddy-tracker version '0+unknown' but software version is '3.6'")
            a     = EddiesObservations.load_file(eddy_dir+'_'+eddy_type+f'_2019{month}{day}01.nc')
            # ARidx = np.argwhere((a.lat<=-20) & (a.lat>=-45) & (a.lon>2.5) & (a.lon<25))
            ARidx = np.argwhere((a.lat<=-20) & (a.lat>=-36) & (a.lon>-15) & (a.lon<20))
            # ARidx = np.argwhere((a.lat<=lat_reg[0]) & (a.lat>=lat_reg[1]) & (a.lon>lon_reg[0]) & (a.lon<lon_reg[1]))

            for ii in range(len(ARidx)):
                if ARidx[ii][0] not in IDn:
                    IDn.append(ARidx[ii][0])

IDeddies = {f'ID{num}': []
         for num in range(len(IDn))}
print(IDeddies)
# %%

FullComp = []
for i in range(datearrs.shape[0]):
            date = datearrs[i]
            # month with two digits
            if date.month < 10:
                month = '0'+str(date.month)
            else:
                month = date.month
            #day with two digits
            if date.day < 10:
                day = '0'+str(date.day)
            else:
                day = date.day

            warnings.filterwarnings("ignore", message="File was created with py-eddy-tracker version '0+unknown' but software version is '3.6'")
            a     = EddiesObservations.load_file(eddy_dir+'_'+eddy_type+f'_2019{month}{day}01.nc')
            # ARidx = np.argwhere((a.lat<=-20) & (a.lat>=-45) & (a.lon>2.5) & (a.lon<25))
            ARidx = np.argwhere((a.lat<=-20) & (a.lat>=-36) & (a.lon>-15) & (a.lon<20))

            # data = ds.isel(time=i).zos
            data = ds_KE.isel(time=i).KE

            for ii in range(len(ARidx)):
                print('Eddy ID: ',str(ARidx[ii][0]))
                print('Eddy center: lon='+str(a.lon[ARidx][ii][0])+', lat='+str(a.lat[ARidx][ii][0]))
                print('Effective radius = '+str(a.radius_e[ARidx][ii][0])+'m')
                lonmin   = a.lon[ARidx][ii][0] - dlon
                lonmax   = a.lon[ARidx][ii][0] + dlon
                latmin   = a.lat[ARidx][ii][0] - dlat
                latmax   = a.lat[ARidx][ii][0] + dlat
                eddy_sel = data.sel(lon=slice(lonmin,lonmax),lat=slice(latmin,latmax))
                eddy_sel = eddy_sel.assign_coords(ID=ARidx[ii][0])

                FullComp.append(eddy_sel)
                IDeddies[f'ID{ARidx[ii][0]}'].append(eddy_sel)

            # del(dssh)
            # del(SSH)
            # del(ARidx)
            # del(a)
            # del(date)

# %%
plt.figure()
# use rainbow colormap for lineplots
colors = plt.cm.rainbow(np.linspace(0, 1, len(IDn)))

for i in range(len(IDn))[::14]:
    i = 0
    SingleComp = IDeddies[f'ID{i}']
    SingleComp = xr.concat(SingleComp,dim='time')

    for ii in range(len(SingleComp)):
        plt.contour(SingleComp[ii].lon, SingleComp[ii].lat, SingleComp[ii],levels=[0.5],c=colors[i])
# %%
SSHcomp = rewrite_lon_lat(SingleComp)

# %%
ds_comp = xr.concat(SSHcomp,dim='eddy')
ds_mean = ds_comp.mean(dim='eddy')

# %%
fig, ax = plt.subplots()
ds_mean.plot(cmap='viridis', ax=ax)
CS = ds_mean.squeeze().plot.contour(cmap='k', ax=ax, levels=0.5)
ax.clabel(CS)


# %%
plt.figure()
ds.isel(time=50).zos.plot()
# ds_KE.isel(time=50).KE.plot()
for ii in range(len(SSHcomp)):
    plt.contour(SSHcomp[ii].lon,SSHcomp[ii].lat,SSHcomp[ii],levels=[0.,0.1,0.2,0.3],colors='k')
