# %%
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt

import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
reload(eva)
import sys
sys.path.insert(0, '../')
import funcs as fu
import xrft

# %%
fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/KE/inside_eddy/'
savefig  = False
# %%
wavelength  = 700  #choice of spatial cutoff for high pass filter in km
shape_error = 25 #choice of shape error in km
dlon        = 1.8
dlat        = 1.8
eddy_type   = 'anticyclonic'
ID          = 0
# %%
exp                 = 'exp9'
root_dir            = f'/work/mh0033/m300878/eddy_tracks/agulhas_eddies/{exp}/'
path_save_composite = f'/composite_{exp}_KE_100m/'
eddy_dir            = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
tracker_dir         = eddy_dir+'tracks/'

ds_exp9 = xr.open_dataset(tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+f'deg_trackID_{ID}.nc')

# %%
exp                 = 'exp7'
root_dir            = f'/work/mh0033/m300878/eddy_tracks/agulhas_eddies/{exp}/'
path_save_composite = f'/composite_{exp}_KE_100m/'
eddy_dir            = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
tracker_dir         = eddy_dir+'tracks/'

ds_exp7 = xr.open_dataset(tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+f'deg_trackID_{ID}.nc')


# %%scatter eddy track
plt.figure()
cb = plt.scatter(ds_exp9.lon, ds_exp9.lat, c=ds_exp9.time, cmap='plasma', s=0.01)
cb = plt.scatter(ds_exp7.lon, ds_exp7.lat, c=ds_exp7.time, cmap='rainbow',s=0.01)
plt.colorbar(cb, label='time')
# %%
ds = ds_exp7
for i in range(4):
    plt.figure()
    plt.pcolormesh(ds.x,ds.y,ds.KE[(i*300),:,:])
    plt.title(f'KE at {ds.time[i*300].data}')
# %%
time_sel = '2019-10-10T03:00:00.000000000'
plt.figure()
ds_exp9.sel(time=time_sel).KE.plot()
plt.figure()
ds_exp7.sel(time=time_sel).KE.plot()

# %%
reload(eva)
P_exp7 = eva.compute_spectra_uchida(ds_exp7.KE.fillna(0.), samp_freq=2)
P_exp9 = eva.compute_spectra_uchida(ds_exp9.KE.fillna(0.), samp_freq=2)

# %%
lat_mean = -32
fname = f'Eddy_ID_{ID}_KE_100m'
reload(fu)
fu.plot_spectra_KE(P_exp7, P_exp9, lat_mean, fig_path, fname, savefig)


# %%
P = P_exp7
plt.figure()
plt.loglog(P.freq_time*86400,P.mean('x').mean('y'))
plt.xlabel('Frequency [cpd]')
plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
plt.ylim(1e-2,5e3)
plt.legend(loc='upper right')
plt.grid(which='both', linestyle='--', lw=0.3)
