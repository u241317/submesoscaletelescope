# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd
import eddy_funcs as fu

sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
import gsw
from scipy.signal import butter,filtfilt


# %%
import smt_modules.init_slurm_cluster as scluster 
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:30:00', wait=False, scale=2)
cluster
client

# %%
tridx     = 3
eddy_type = 'cyclonic'
fig_path  = f'/home/m/m300878/submesoscaletelescope/notebooks/eddytracker/postprocessing/images/ID{tridx}/'
savefig   = True
# %% Bandpass filter
def butter_filter(data, cutoff, fs, order, btype):
    b, a = butter(order, cutoff, btype=btype, analog=False, fs=fs)
    y = np.apply_along_axis(lambda m: filtfilt(b, a, m), axis=0, arr=data)
    return y


# %%
ds_u = fu.load_eddy_comp('u', tridx, eddy_type)
ds_v = fu.load_eddy_comp('v', tridx, eddy_type)
# %%
# Align the datasets based on the common time values
u_aligned, v_aligned = xr.align(ds_u, ds_v, join='inner')
KE = (u_aligned**2 + v_aligned**2)

# %%
radius = 1.2
center_x, center_y = 0, 0
distance = np.sqrt((KE['x'] - center_x)**2 + (KE['y'] - center_y)**2)
mask = distance <= radius

data = KE.isel(depth=30).where(mask, drop=True)
fig_path0  = f'/home/m/m300878/submesoscaletelescope/results/smt_wave/eddy_features/spectra/{eddy_type}/ID{tridx}/'

plt.figure()
plt.title(f'Kinetic Energy at {KE.depth[30]:.2f}m')
plt.plot(data.time, data.mean(dim='x', skipna=True).mean(dim='y', skipna=True))
plt.xticks(rotation=45)

if savefig: plt.savefig(f'{fig_path0}KE_time.png', bbox_inches='tight', dpi=150)
 # %%
reload(eva)
f_center      = eva.corfreq(KE.latcen)
f_center_t0   = np.abs(f_center[0])/2/np.pi
f_center_t1   = np.abs(f_center[-1])/2/np.pi
f_center_mean = np.abs(f_center.mean())/2/np.pi

f = [f_center_t0.data, f_center_t1.data, f_center_mean.data]
print(f)
# %% eddy freq.
if False:
    dx = eva.convert_degree_to_meter(data_sel.latcen)*0.01
    center_x, center_y = 0, 0
    radius    = 0.7
    distance  = np.sqrt((data_sel['x'] - center_x)**2 + (data_sel['y'] - center_y)**2)
    mask_cen  = distance <= radius
    vort      = 0.5*((v.isel(depth=10, time=100).diff('x')/dx.isel(time=100) - u.isel(depth=10, time=100).diff('y')/dx.isel(time=100)))
    vort_mask = vort.where(mask_cen, drop=True)
    vort_mask.plot()
    f_eddy    = vort_mask.mean(['x','y'], skipna=True).compute()
    f_eddy    = f_eddy.data*60*60*24/2/np.pi
    print(f_eddy)
# %%
order         = 5
sampling_freq = 1/2 # 1/(2h)
if True:
    btype         = 'bandpass'
    cutoff        = np.array([1/6, 1/3]) # 1/(6h)
    cutoff        = np.array([1/8, 1/5]) # 1/(6h)
    cutoff        = np.array([1/26, 1/22]) # diurnal
    cutoff        = np.array([1/14, 1/10]) # semidurnal
    # cutoff   = np.array([f[2]*0.85, f[2]*1.15])/24
    # cutoff        = np.array([np.abs(-f[2]+f_eddy), np.abs(-f[2]-f_eddy)])/24
else:
    btype  = 'highpass'
    cutoff = 1/7
# %%
data_sel = KE.isel(y=250).isel(depth=slice(0,80))
y    = butter_filter(data_sel.fillna(0.), cutoff, sampling_freq, order, btype=btype)
KE_band_slice = xr.DataArray(y, dims=['time', 'depth', 'x'], coords={'time': data_sel.time, 'depth': data_sel.depth, 'x': data_sel.x})

# %%
for i in range(5):
    plt.figure()
    KE_band_slice.isel(time= (i+100)).plot()
    plt.ylim(1000,0)

# %%
#2d
if False:
    y_xr = xr.DataArray(y, dims=['time', 'depth'], coords={'time': data_sel.time, 'depth': data_sel.depth})
    plt.figure()
    y_xr.plot(y='depth')
    plt.ylim(1000,0)

# %%
d = 40
data_sel = KE.isel(depth=d)

# %%
# f_center = eva.calc_coriolis_parameter(data_sel.latcen)
# f_center = np.abs(f_center.mean()*60*60)/2/np.pi
y        = butter_filter(data_sel.fillna(0.), cutoff, sampling_freq, order, btype=btype)
KE_band_map    = xr.DataArray(y, dims=['time', 'x', 'y'], coords={'time': data_sel.time, 'x': data_sel.x, 'y': data_sel.y})
# %%
for i in range(5):
    plt.figure()
    lim=0.1
    KE_band_map.isel(time= (i+100)).plot(cmap='RdBu_r', vmin=-lim, vmax=lim)

# %%
savefig  = True
path_fig = f'/work/mh0033/m300878/video/eddy/bandpass/images/{eddy_type}/ID{tridx}/data/'
run      = 'ke'
file     = 'reg_vid'
lim      = 0.1
# for i in range(5):
for i in range(KE_band_slice.time.size):
    data = KE_band_map.isel(time=i).squeeze()
    data2 = KE_band_slice.isel(time=i).squeeze()
    if i==0:
        fig, ax = plt.subplots(1,2, figsize=(10,5))
        pc = ax[0].pcolormesh(data.x, data.y, data.data, vmin=-lim, vmax=lim, cmap ='RdBu_r')
        ht = ax[0].set_title(f'bandpassed of Kinetic Energy at {data.time.data:.13} \n band: {cutoff}')
        fig.colorbar(pc, ax=ax, orientation='horizontal')
        ax[0].axhline(0, color='r', linestyle='--', lw=1)
        ax[0].set_xlabel('x')
        ax[0].set_ylabel('y')
        ps = ax[1].pcolormesh(data2.x, data2.depth, data2.data, vmin=-lim, vmax=lim, cmap ='RdBu_r')
        ax[1].set_ylim(1200,0)
        ax[1].set_ylabel('depth [m]')
        ax[1].set_xlabel('x')
        ax[1].axhline(data2.depth[d], color='r', linestyle='--', lw=1)
        ax[1].set_title(f'at depth {data2.depth[d].data:.2}')
    else:
        pc.set_array(data.data.flatten())
        ps.set_array(data2.data.flatten())
        ht.set_text(str(f'bandpassed Kinetic Energy at  {data.time.data:.13} \n band: {cutoff}').replace('T',' '))

    if savefig:
        fpath = '%s%s_%s_%04d.jpg' % (path_fig,file.split('/')[-1][:-3], run, i)
        print('save figure: %s' % (fpath))
        plt.savefig(fpath, bbox_inches='tight', dpi=150)
# %%


# %% ############## apply rolling mean to data ############################
tt=4
depth=40

data = u.isel(depth=depth).compute()**2 + v.isel(depth=depth).compute()**2
# data = w.w.isel(depthi=85, time=tt).compute()

data = to.isel(depth=depth).compute()
data.plot()
# %%
l = 3
roll = data.rolling(x=l,y=l, center=True).mean()
# roll.plot()
(data-roll).plot(figsize=(10,10))

# %%
lim=0.1
(data-roll).plot(figsize=(10,10),vmin=-lim, vmax=lim, cmap='RdBu')


# %%
# apply wavlet transform
import pywt
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# Discrete Wavelet Transform
data = KE.isel(depth=40).isel(x=250, y=175).compute()


# %% create morse wavelet
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as signal

# create a morse wavelet
# wavelet = signal.morlet

# Plot the wavelet
widths = np.arange(1, 61)
wavelet = signal.morlet
cwtmatr = signal.cwt(data, wavelet, widths)
# plt.pcolormesh(np.abs(cwtmatr), cmap='RdBu')  # Use magnitude for complex data
# logarithmic scale
plt.pcolormesh((np.abs(cwtmatr)), cmap='Spectral')  # Use magnitude for complex data
plt.show()
# %%
KE.isel(depth=40).mean('x').mean('y').plot()

# %% 
