# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd
import xrft
from scipy.spatial import cKDTree

sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
import gsw
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()

# %%
def load_eddy_comp(var, tridx=0, eddy_type='anticyclonic', start_end=np.array([None, None]), subsel=None):
    path    = f'/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/{eddy_type}/ID{tridx}/{var}/*.nc'
    flist   = np.array(glob.glob(path))
    print('flist', len(flist))
    if tridx == 0 and eddy_type == 'anticyclonic':
        flist   = np.array(sorted(flist, key=lambda x: int(x.split('_')[-1].split('.')[0])))
    else:
        flist.sort()

    if start_end[0] is not None:
        flist = flist[start_end[0]:start_end[1]]
    if subsel is not None:
        flist = flist[::subsel]

    if var == 'zos':
        ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks={'time':1})
    else:
        ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=False, chunks={'time':1, 'depth':1})
    print(f'Loaded {var} data')
    ds = ds[var]
    return ds

# Eddy Spectra
# def load_data(var):
#     path    = f'/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/{var}/anticyclonic_2.5x2.5deg*.nc'
#     flist   = np.array(glob.glob(path))
#     flist   = np.array(sorted(flist, key=lambda x: int(x.split('_')[-1].split('.')[0])))
#     print('flist', len(flist))
#     if var == 'to' or var == 'v' or var == 'zos':
#         flist = flist
#     else:
#         flist = flist[1:]

#     # flist = flist[:100]
#     if var == 'zos':
#         ds_temp = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks={'time':1})
#     else:
#         ds_temp = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks={'time':1, 'depth':1})
#     # ds_temp = xr.decode_cf(ds_temp) #restores timestamp
#     print(f'Loaded {var} data')
#     return ds_temp

def compute_spectra_uchida(data, samp_freq=1):
    data['time'] = np.arange(int(data.time.size))*3600*samp_freq
    P = xrft.power_spectrum(data, dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
    P = P.isel(freq_time=slice(len(P.freq_time)//2,None)) * 2
    return P

def compute_f(data_sel):
    reload(eva)
    f_center      = eva.corfreq(data_sel.latcen)
    f_center_t0   = np.abs(f_center[0])/2/np.pi
    f_center_t1   = np.abs(f_center[-1])/2/np.pi
    f_center_mean = np.abs(f_center.mean())/2/np.pi
    f = [f_center_t0.data, f_center_t1.data, f_center_mean.data]
    print(f)
    return f

def compute_eddy_freq(data_sel,u,v,idepth,radius=1.2,itime=100):
    dx = eva.convert_degree_to_meter(data_sel.latcen)*0.01

    center_x, center_y = 0, 0
    radius    = radius*0.5
    distance  = np.sqrt((data_sel['x'] - center_x)**2 + (data_sel['y'] - center_y)**2)
    mask_cen  = distance <= radius
    vort      = 0.5*((v.isel(depth=idepth, time=itime).diff('x')/dx.isel(time=itime) - u.isel(depth=idepth, time=itime).diff('y')/dx.isel(time=itime)))
    vort_mask = vort.where(mask_cen, drop=True)
    vort_mask.plot()
    f_eddy    = vort_mask.mean(['x','y'], skipna=True).compute()
    f_eddy    = f_eddy.data*60*60*24/2/np.pi
    print(f_eddy)
    return f_eddy

def calc_mask(data_sel, radius=1.2, fig_path=None, savefig=False, itime=100):
    center_x, center_y = 0, 0
    distance = np.sqrt((data_sel['x'] - center_x)**2 + (data_sel['y'] - center_y)**2)
    mask = distance <= radius
    fig, ax = plt.subplots()
    ax.pcolormesh(data_sel.x, data_sel.y, data_sel.isel(time=itime).data)
    circle = plt.Circle((center_x, center_y), radius, color='r', fill=False)#
    ax.add_artist(circle)
    plt.title('Eddy Mask')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    if savefig==True: plt.savefig(fig_path+'mask.png', dpi=180)
    count = mask.sum()
    print(f'Number of grid points inside circle: {count.data} and outside: {mask.size-count.data}')
    return mask

def plot_spectra_uchida(P, mask, f, f_eddy, depth, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.7, fig_size_fac=4)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    omega = P.freq_time*86400 # convert from day to seconds
    ax.loglog(omega, 1e1*omega**(-2), color='k', lw=0.5, linestyle='--', label=rf'$\omega^{-2}$')
    ax.loglog(P.freq_time[1:]*86400,   P.mean(['x','y'], skipna=True)[1:], color='grey', lw=0.5, label='entire domain spectra', zorder=20)
    ax.loglog(P.freq_time[1:]*86400,   P.where(mask, drop=True).mean(['x','y'], skipna=True)[1:], label='inside eddy spectra',zorder=21)
    ax.loglog(P.freq_time[1:]*86400,   P.where(~mask, drop=True).mean(['x','y'], skipna=True)[1:], label='outside eddy spectra', zorder=22)

    freq_names = eva.tidefreqnames()
    freq_tides = eva.tidefreq()/2/np.pi
    # f
    ax.axvline(f[2], color='r', linestyle='-', lw=1, zorder=3)
    ax.text(f[2]*0.9, ax.get_ylim()[0]*1.1, f'f', fontsize=10, color='r', rotation=90, ha='center', va='bottom', zorder=3)
    ax.axvspan(f[0], f[1], color='red', alpha=0.1)
    #M2+f
    ax.axvline(freq_tides[5]+f[2], color='r', linestyle='-', lw=1, zorder=3)
    ax.text((freq_tides[5]+f[2])*0.95, ax.get_ylim()[0]*1.1, 'M2+f', fontsize=10, rotation=90, color='r', ha='center', va='bottom', zorder=3)
    # 2f
    ax.axvline(2*f[2], color='r', linestyle='--', lw=1)
    ax.text(2*f[2]*0.9, ax.get_ylim()[0]*1.1, '2f', fontsize=10, color='r', rotation=90, ha='center', va='bottom', zorder=3)
    # 2 * M2
    ax.axvline(2*freq_tides[5], color='k', linestyle=':', lw=1, zorder=3)
    ax.text(2*freq_tides[5]*0.9, ax.get_ylim()[0]*2, '2*M2', fontsize=10, color='k', rotation=90, ha='center', va='bottom', zorder=3)

    # eddy freq
    ax.axvline(f_eddy, color='k', linestyle='--', lw=1, zorder=3)
    ax.text(f_eddy*0.9, ax.get_ylim()[1]*0.07, r'$f_{eddy} = \frac{1}{2} (\frac{\partial V}{\partial x} - \frac{\partial U}{\partial y})$', fontsize=10, color='k', rotation=90, ha='center', va='bottom', zorder=3)

    # f + f_eddy
    ax.axvline(np.abs((-f[2]+f_eddy)), color='b', linestyle='-', lw=1, zorder=3)
    ax.text(np.abs((-f[2]+f_eddy))*0.9, ax.get_ylim()[1]*0.07, r'$f_{eff} = |f + f_{eddy}|$', fontsize=10, color='k', rotation=90, ha='center', va='bottom', zorder=3)
    ax.axvline(np.abs((-f[2]-f_eddy)), color='b', linestyle='-', lw=1, zorder=3)
    ax.text(np.abs((-f[2]-f_eddy))*0.9, ax.get_ylim()[1]*0.07, r'$f_{eff} = |f - f_{eddy}|$', fontsize=10, color='k', rotation=90, ha='center', va='bottom', zorder=3)

    ypos = np.array([1,3,10,1,3,10,1,50,10])*ax.get_ylim()[0]*2
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.1, ypos[i], freq_names[i], fontsize=10, rotation=90, va='bottom', ha='center', zorder=3)
        ax.axvline(freq_tides[i], color='k', linestyle=':', lw=1)
    # ax.vlines(eva.tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )

    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.set_xlim(omega[1], omega[-1])
    # ax.set_ylim(2e-2,2e3)
    ax.legend(loc='lower left')
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.set_title(f'hor. KE spectra of Anticyclone at {depth:.0f}m')
    if savefig==True: plt.savefig(fig_path+f'spectra_{depth:.0f}_0.png', dpi=180)

def plot_spectra_mountain(P1, P2, mask, f, f_eddy, depth, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.7, fig_size_fac=4)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    omega = P1.freq_time*86400 # convert from day to seconds
    ax.loglog(omega, 1e1*omega**(-2), color='k', lw=0.5, linestyle='--', label=rf'$\omega^{-2}$')
    ax.loglog(P1.freq_time[1:]*86400,   P1.mean(['x','y'], skipna=True)[1:], color='grey', lw=0.5, label='entire domain spectra topo', zorder=20)
    ax.loglog(P1.freq_time[1:]*86400,   P1.where(mask, drop=True).mean(['x','y'], skipna=True)[1:], label='inside eddy spectra topo',zorder=21)
    ax.loglog(P1.freq_time[1:]*86400,   P1.where(~mask, drop=True).mean(['x','y'], skipna=True)[1:], label='outside eddy spectra topo', zorder=22)

    ax.loglog(P2.freq_time[1:]*86400,   P2.mean(['x','y'], skipna=True)[1:], color='grey', lw=0.5, label='entire domain spectra no topo', zorder=20)
    ax.loglog(P2.freq_time[1:]*86400,   P2.where(mask, drop=True).mean(['x','y'], skipna=True)[1:], label='inside eddy spectra no topo',zorder=21)
    ax.loglog(P2.freq_time[1:]*86400,   P2.where(~mask, drop=True).mean(['x','y'], skipna=True)[1:], label='outside eddy spectra no topo', zorder=22)

    freq_names = eva.tidefreqnames()
    freq_tides = eva.tidefreq()/2/np.pi
    # f
    ax.axvline(f[2], color='r', linestyle='-', lw=1, zorder=3)
    ax.text(f[2]*0.9, ax.get_ylim()[0]*1.1, f'f', fontsize=10, color='r', rotation=90, ha='center', va='bottom', zorder=3)
    ax.axvspan(f[0], f[1], color='red', alpha=0.1)
    #M2+f
    ax.axvline(freq_tides[5]+f[2], color='r', linestyle='-', lw=1, zorder=3)
    ax.text((freq_tides[5]+f[2])*0.95, ax.get_ylim()[0]*1.1, 'M2+f', fontsize=10, rotation=90, color='r', ha='center', va='bottom', zorder=3)
    # 2f
    ax.axvline(2*f[2], color='r', linestyle='--', lw=1)
    ax.text(2*f[2]*0.9, ax.get_ylim()[0]*1.1, '2f', fontsize=10, color='r', rotation=90, ha='center', va='bottom', zorder=3)
    # 2 * M2
    ax.axvline(2*freq_tides[5], color='k', linestyle=':', lw=1, zorder=3)
    ax.text(2*freq_tides[5]*0.9, ax.get_ylim()[0]*2, '2*M2', fontsize=10, color='k', rotation=90, ha='center', va='bottom', zorder=3)

    # eddy freq
    ax.axvline(f_eddy, color='k', linestyle='--', lw=1, zorder=3)
    ax.text(f_eddy*0.9, ax.get_ylim()[1]*0.07, r'$f_{eddy} = \frac{1}{2} (\frac{\partial V}{\partial x} - \frac{\partial U}{\partial y})$', fontsize=10, color='k', rotation=90, ha='center', va='bottom', zorder=3)

    # f + f_eddy
    ax.axvline(np.abs((-f[2]+f_eddy)), color='b', linestyle='-', lw=1, zorder=3)
    ax.text(np.abs((-f[2]+f_eddy))*0.9, ax.get_ylim()[1]*0.07, r'$f_{eff} = |f + f_{eddy}|$', fontsize=10, color='k', rotation=90, ha='center', va='bottom', zorder=3)
    ax.axvline(np.abs((-f[2]-f_eddy)), color='b', linestyle='-', lw=1, zorder=3)
    ax.text(np.abs((-f[2]-f_eddy))*0.9, ax.get_ylim()[1]*0.07, r'$f_{eff} = |f - f_{eddy}|$', fontsize=10, color='k', rotation=90, ha='center', va='bottom', zorder=3)

    ypos = np.array([1,3,10,1,3,10,1,50,10])*ax.get_ylim()[0]*2
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.1, ypos[i], freq_names[i], fontsize=10, rotation=90, va='bottom', ha='center', zorder=3)
        ax.axvline(freq_tides[i], color='k', linestyle=':', lw=1)
    # ax.vlines(eva.tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )

    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.set_xlim(omega[1], omega[-1])
    # ax.set_ylim(2e-2,2e3)
    ax.legend(loc='lower left')
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.set_title(f'hor. KE spectra of Anticyclone at {depth:.0f}m')
    if savefig==True: plt.savefig(fig_path+'topo/'+f'spectra_{depth:.0f}_topo.png', dpi=180)


# Eddy Features
def plot_w(w, to, zos, depth, tridx, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=2)
    ii =-1
    ii +=1; ax = hca[ii]; cax = hcb[ii]
    clim=0.004
    hm1 = pyic.shade(w.x, w.y, w.isel(depthi=depth), ax=ax, cax=cax, cmap = 'RdBu', clim=clim)

    ax.axhline(0, color='r', linestyle='--', lw=1)
    ax.set_xlabel('x [deg]')
    ax.set_ylabel('y [deg]')
    ax.set_title(f'w {w.time.values:.13}h {w.depthi[depth].values:.2f} m')
    min_val = zos.min()
    max_val = zos.max()
    levels = np.linspace(min_val, max_val, num=10)  # Adjust 'num' for the desired number of levels
    ax.contour(zos.x, zos.y, zos, levels=levels, colors='k', linewidths=0.3)

    ii =+1; ax = hca[ii]; cax = hcb[ii]
    hm2 = pyic.shade(w.x, w.depthi, w.isel(y=250), ax=ax, cax=cax, cmap = 'RdBu', clim=clim)
    ax.set_ylim(1500,0)
    ax.set_xlabel('x [deg]')
    ax.set_ylabel('depth [m]')
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)
    ax.axhline(w.depthi[depth].values, color='r', linestyle='--', lw=1)
    ax.contour(to.x, to.depth, to.isel(y=250), levels=np.arange(0,30,1), colors='k', linewidths=0.3)
    if savefig==True: plt.savefig(fig_path+f'w_ID{tridx}.png', dpi=180)

def plot_KE(u, v, to, zos, depth, tridx, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=2)
    ii =-1
    ii +=1; ax = hca[ii]; cax = hcb[ii]
    clim=0, 0.3
    hm1 = pyic.shade(u.x, u.y, (u**2+v**2).isel(depth=depth), ax=ax, cax=cax, cmap = 'plasma', clim=clim)
    #horizontal red line at y=250
    ax.axhline(0, color='r', linestyle='--', lw=1)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title(f'KE {u.time.values:.13}h {u.depth[depth].values:.2f} m')
    min_val = zos.min()
    max_val = zos.max()
    levels = np.linspace(min_val, max_val, num=10)  # Adjust 'num' for the desired number of levels
    ax.contour(zos.x, zos.y, zos, levels=levels, colors='k', linewidths=0.3)

    ii =+1; ax = hca[ii]; cax = hcb[ii]
    hm2 = pyic.shade(u.x, u.depth, (u**2+v**2).isel(y=250), ax=ax, cax=cax, cmap = 'plasma', clim=clim)
    ax.set_ylim(1500,0)
    ax.set_xlabel('x')
    ax.set_ylabel('depth [m]')
    ax.axhline(u.depth[depth].values, color='r', linestyle='--', lw=1)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)
    ax.contour(to.x, to.depth, to.isel(y=250), levels=np.arange(0,30,1), colors='w', linewidths=0.3)
    if savefig==True: plt.savefig(fig_path+f'KE_ID{tridx}.png', dpi=180)

def plot_TKE(tke, to, zos, depth, tridx, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=2)
    ii =-1
    ii +=1; ax = hca[ii]; cax = hcb[ii]
    clim=0, 1e-4
    hm1 = pyic.shade(tke.x, tke.y, tke.isel(depthi=depth), ax=ax, cax=cax, cmap = 'inferno',  logplot=True)
    #horizontal red line at y=250
    ax.axhline(0, color='r', linestyle='--', lw=1)
    ax.set_xlabel('x [deg]')
    ax.set_ylabel('y [deg]')
    ax.set_title(f'TKE {tke.time.values:.13}h {tke.depthi[depth].values:.2f} m')
    min_val = zos.min()
    max_val = zos.max()
    levels = np.linspace(min_val, max_val, num=10)  # Adjust 'num' for the desired number of levels
    ax.contour(zos.x, zos.y, zos, levels=levels, colors='k', linewidths=0.3)

    ii =+1; ax = hca[ii]; cax = hcb[ii]
    hm2 = pyic.shade(tke.x, tke.depthi, tke.isel(y=250), ax=ax, cax=cax, cmap = 'inferno', logplot=True)
    ax.set_ylim(1500,0)
    ax.set_xlabel('x [deg]')
    ax.set_ylabel('depth [m]')
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)
    ax.axhline(tke.depthi[depth].values, color='r', linestyle='--', lw=1)
    ax.contour(to.x, to.depth, to.isel(y=250), levels=np.arange(0,30,1), colors='white', linewidths=0.3)
    if savefig==True: plt.savefig(fig_path+f'TKE_ID{tridx}.png', dpi=180)

def plot_N2(N2, to, zos, depth, tridx, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=2)
    ii =-1
    ii +=1; ax = hca[ii]; cax = hcb[ii]
    clim=0, 1e-5
    hm1 = pyic.shade(N2.x, N2.y, N2.isel(depthi=depth), ax=ax, cax=cax, cmap = 'Spectral',  logplot=True)
    #horizontal red line at y=250
    ax.axhline(0, color='r', linestyle='--', lw=1)
    ax.set_xlabel('x [deg]')
    ax.set_ylabel('y [deg]')
    ax.set_title(f'N2 {N2.time.values:.13}h {N2.depthi[depth].values:.2f} m')
    min_val = zos.min()
    max_val = zos.max()
    levels = np.linspace(min_val, max_val, num=10)  # Adjust 'num' for the desired number of levels
    ax.contour(zos.x, zos.y, zos, levels=levels, colors='k', linewidths=0.3)

    ii =+1; ax = hca[ii]; cax = hcb[ii]
    hm2 = pyic.shade(N2.x, N2.depthi, N2.isel(y=250), ax=ax, cax=cax, cmap = 'Spectral', logplot=True)
    ax.set_ylim(1500,0)
    ax.set_xlabel('x [deg]')
    ax.set_ylabel('depth [m]')
    #add red horizontal line at depth
    ax.axhline(N2.depthi[depth].values, color='r', linestyle='--', lw=1)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)
    ax.contour(to.x, to.depth, to.isel(y=250), levels=np.arange(0,30,1), colors='k', linewidths=0.3)
    if savefig==True: plt.savefig(fig_path+f'N2_ID{tridx}.png', dpi=180)


def load_data(var, id, type):

    path    = f'/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7_2/eddytrack_wv_700_se_25tracks/composite_vers2/{type}/{id}/{var}/{type}_2.5x2.5deg*.nc'
    flist   = np.array(glob.glob(path))
    # flist   = np.array(sorted(flist, key=lambda x: int(x.split('_')[-1].split('.')[0])))
    flist.sort()
    print('flist', len(flist))
    if var == 'to' or var == 'v' or var == 'zos':
        flist = flist
    else:
        flist = flist[1:]

    # flist = flist[500:600]
    if var == 'zos':
        ds_temp = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks={'time':1})
    else:
        ds_temp = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks={'time':1, 'depth':1})
    # ds_temp = xr.decode_cf(ds_temp) #restores timestamp
    print(f'Loaded {var} data')
    return ds_temp


def spherical_to_cartesian(lon, lat):
  earth_radius = 6371e3
  x = earth_radius * np.cos(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  y = earth_radius * np.sin(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  z = earth_radius * np.sin(lat*np.pi/180.)
  return x, y, z

def interp_to_reg_grid(lon_i, lat_i, lon_o, lat_o):
    xi, yi, zi = spherical_to_cartesian(lon_i, lat_i)
    xo, yo, zo = spherical_to_cartesian(lon_o, lat_o)

    lzip_i = np.concatenate((xi[:,np.newaxis],yi[:,np.newaxis],zi[:,np.newaxis]), axis=1)
    lzip_o = np.concatenate((xo[:,np.newaxis],yo[:,np.newaxis],zo[:,np.newaxis]), axis=1) 
    tree   = cKDTree(lzip_i)
    dckdtree, ickdtree = tree.query(lzip_o , k=1, workers=-1)

    Dind_dist = dict()
    Dind_dist['dckdtree_c'] = dckdtree
    Dind_dist['ickdtree_c'] = ickdtree

    return Dind_dist

# %%
def compute_circle_indces(data):
    center_x, center_y = 0, 0
    radius    = 0.7
    theta     = np.linspace(0, 2*np.pi, 500)
    x         = center_x + radius * np.cos(theta)
    y         = center_y + radius * np.sin(theta)
    lon_o     = x 
    lat_o     = y   

    plt.figure()
    plt.pcolormesh(data.x, data.y, data, cmap='plasma')
    plt.plot(lon_o, lat_o, 'g', lw=2)
    # plt.savefig(fig_path+'circle.png', dpi=180)

    #make xarray
    lon_o = xr.DataArray(lon_o, dims=['lon'])
    lat_o = xr.DataArray(lat_o, dims=['lat'])

    lon_i = data.x #+ data.loncen.data
    lat_i = data.y #+ data.latcen.data
    Lon, Lat = np.meshgrid(lon_i, lat_i)
    lon_i = Lon.flatten()
    lat_i = Lat.flatten()

    Dind_dist = interp_to_reg_grid(lon_i, lat_i, lon_o.data, lat_o.data)
    ickdtree  = Dind_dist['ickdtree_c']
    return ickdtree, theta


# %% ################## ###### apply ######################
def select_circle(data, ickdtree, theta1):
    flattened_data = data.stack(theta=('x', 'y'))
    flattened_data = flattened_data.reset_index('theta')
    hov = flattened_data.isel(theta=ickdtree)
    hov['theta'] = ('theta', theta1)
    hov.drop('x').drop('y')
    return hov

# def plot_hov(hov, savefig=False, cmap=None, vmin=None, vmax=None, fig_path=None):
#     fig = plt.figure(figsize=(20, 5))
#     gs = fig.add_gridspec(2, 2, height_ratios=[1, 5])

#     ax0 = fig.add_subplot(gs[0, 0])
#     ax0.plot(np.arange(631), hov.isel(theta=25), color='k', lw=1)
#     ax0.set_xticks([])  # Hide x-ticks for the first subplot
#     ax0.set_title(f'Hovmöller Diagram of {hov.name} at {hov.depthi.data:.0f}m', fontsize=14)
#     ax0.set_xlim(0, 631)

#     ax1 = fig.add_subplot(gs[1, 0])
#     if vmin==None and vmax==None:
#         cb = ax1.pcolormesh(np.arange(631), hov.theta / np.pi, hov.transpose(), cmap=cmap)
#     else:
#         cb = ax1.pcolormesh(np.arange(631), hov.theta / np.pi, hov.transpose(), cmap=cmap, vmin=vmin, vmax=vmax)
#         #with logscale
#         # cb = ax1.pcolormesh(np.arange(631), hov.theta / np.pi, hov.transpose(), cmap=cmap, norm=matplotlib.colors.LogNorm())
#     fig.colorbar(cb, ax=ax1, orientation='horizontal', pad=0.13, aspect=40, shrink=0.8)
#     ax1.set_xlabel('time [days]', fontsize=12)
#     ax1.set_ylabel(r'$\theta / \pi$', fontsize=12)
#     ax1.axhline(0.5, color='white', linestyle='-', linewidth=1)
#     ax1.axvline(315, color='white', linestyle='-', linewidth=1)
#     ax1.set_xticks(np.arange(0, 631, 12))
#     ax1.set_xticklabels([f'{int(x*2/24)}' for x in np.arange(0, 631, 12)], fontsize=6)
#     ax1.tick_params(axis='x', which='both', bottom=True, top=True, labelbottom=True, labeltop=True)

#     ax2 = fig.add_axes([0.522, 0.225, 0.037, 0.509]) 
#     ax2.plot(hov.isel(time=315), hov.theta/np.pi, color='k', lw=1)
#     ax2.set_ylim(0, 2)
#     ax2.set_yticks([])
#     ax2.tick_params(axis='x', labelsize=6)

#     plt.tight_layout()
#     if savefig==True: plt.savefig(fig_path+f'hov_{hov.name}_{hov.depthi.data:.0f}.png', dpi=180, bbox_inches='tight')


def plot_hov_0(hov, savefig=False, cmap=None, vmin=None, vmax=None, fig_path=None):
    tt = hov.shape[0]
    idy = 125
    iday = 315

    fig = plt.figure(figsize=(20, 5))
    gs = fig.add_gridspec(2, 2, height_ratios=[1, 5])

    ax0 = fig.add_subplot(gs[0, 0])
    ax0.plot(np.arange(tt), hov.isel(theta=idy), color='k', lw=1)
    ax0.set_xticks([])  # Hide x-ticks for the first subplot
    ax0.set_title(f'Hovmöller Diagram of {hov.name} at {hov.depth.data:.0f}m', fontsize=14)
    ax0.set_xlim(0, tt)

    ax1 = fig.add_subplot(gs[1, 0])
    if vmin==None and vmax==None:
        cb = ax1.pcolormesh(np.arange(tt), hov.theta / np.pi, hov.transpose(), cmap=cmap)
    else:
        cb = ax1.pcolormesh(np.arange(tt), hov.theta / np.pi, hov.transpose(), cmap=cmap, vmin=vmin, vmax=vmax)
    fig.colorbar(cb, ax=ax1, orientation='horizontal', pad=0.13, aspect=40, shrink=0.8)
    ax1.set_xlabel('time [days]', fontsize=12)
    ax1.set_ylabel(r'$\theta / \pi$', fontsize=12)
    ax1.axhline(hov.theta[idy].data/np.pi, color='white', linestyle='-', linewidth=1)
    ax1.axvline(iday, color='white', linestyle='-', linewidth=1)
    ax1.set_xticks(np.arange(0, tt, 12))
    ax1.set_xticklabels([f'{int(x*2/24)}' for x in np.arange(0, tt, 12)], fontsize=6)

    ax2 = fig.add_axes([0.522, 0.225, 0.037, 0.509]) 
    ax2.plot(hov.isel(time=iday), hov.theta/np.pi, color='k', lw=1)
    ax2.set_ylim(0, 2)
    ax2.set_yticks([])
    ax2.tick_params(axis='x', labelsize=6)

    plt.tight_layout()

    if savefig==True: plt.savefig(fig_path+f'hov_{hov.name}_{hov.depth.data:.0f}.png', dpi=180, bbox_inches='tight')
