# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd

sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
import gsw
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import eddy_funcs as fu


import xarray as xr
import pandas as pd

# %%
import smt_modules.init_slurm_cluster as scluster 
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, scale=2)
cluster
client
#%%
fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/eddy_features/'
savefig  = False

# %%
id   = 'ID5'
type = 'anticyclonic'
reload(fu)
to  = fu.load_data('to', id, type)
so  = fu.load_data('so', id, type)
u   = fu.load_data('u', id, type)
v   = fu.load_data('v', id, type)
w   = fu.load_data('w', id, type)
w   = w.rename({'depth':'depthi'})
tke = fu.load_data('tke', id, type)
tke = tke.rename({'depth':'depthi'})
zos = fu.load_data('zos', id, type)
t0  = u.time[0].values
t1  = u.time[-1].values
zos = zos.sel(time=slice(t0, t1))

# %%######################################### Select Circle #########################################
fig_path = f'/home/m/m300878/submesoscaletelescope/results/smt_wave/eddy_features/hovmueller/{id}/'
savefig  = False

# %%
reload(fu)
data_idx =( 0.5*(u.u**2 + v.v**2)).isel(depth=30, time=500)
data_idx = data_idx.where(data_idx!=0, np.nan)
ickdtree, theta = fu.compute_circle_indces(data_idx)

D = np.array([0,17,30,50,63,85,105])
# d = 30
# data = w.w.isel(depthi=d).compute()
# data = to.to.isel(depth=105).compute()
# data = 0.5*(u.u**2 + v.v**2)
# data = data.rename('KE')

# %%
reload(fu)
for d in D:
     ##%%
    # d=17
    print(f'selecting circle at depth {d}')
    if False:
        data = 0.5*(u.u.isel(depth=d).compute()**2 + v.v.isel(depth=d).compute()**2)
        data = data.rename('KE')
        # data = tke.tke.isel(depthi=d).compute()
        data = data.where(data!=0, np.nan)
        data = w.w.isel(depthi=d).compute()
        data = data.rename(depthi='depth')
    else:
        data = to.to.isel(depth=d).compute()
        data = data.where(data!=0, np.nan)

    hov = fu.select_circle(data, ickdtree, theta)
    print('plotting hovmöller')
    fu.plot_hov_0(hov, savefig=savefig, cmap='inferno', fig_path=fig_path)
    # fu.plot_hov_0(hov, savefig=savefig, cmap='RdBu_r', vmin=-0.002, vmax=0.002, fig_path=fig_path)

    # plot_hov(hov, savefig=savefig, cmap='RdBu_r', vmin=-0.005, vmax=0.005)
    # plot_hov(hov, savefig=savefig, cmap='inferno', vmin=1e-6, vmax=1e-3)

# %% TEST plot
reload(fu)
# d=17
# data = w.w.isel(depthi=d).compute()
# data = data.rename(depthi='depth')
data = zos.zos.compute()
hov = fu.select_circle(data, ickdtree, theta)
hov = hov.assign_coords({'depth': 0})
fu.plot_hov_0(hov, savefig=savefig, cmap='inferno',  vmin=None, vmax=None, fig_path=fig_path)
# fu.plot_hov_0(hov, savefig=savefig, cmap='RdBu_r', vmin=-0.002, vmax=0.002, fig_path=fig_path)


#%%

# data test 
mesh = np.meshgrid(data.x + data.loncen.isel(time=0), data.y+data.latcen.isel(time=0))
#make xr
dat_lon = xr.DataArray(mesh[0], dims=['y', 'x'], coords={'y': data.y, 'x': data.x})
dat_lat = xr.DataArray(mesh[1], dims=['y', 'x'], coords={'y': data.y, 'x': data.x})

lon_check = fu.select_circle(dat_lon, ickdtree, theta)
lat_check = fu.select_circle(dat_lat, ickdtree, theta)

# use colormap 
plt.scatter(lon_check, lat_check,cmap='inferno', c=np.arange(len(lon_check)))

###########################################################################
###########################################################################
# %% Bandpassed filtered Hovmöller
fig_path = fig_path+'bandpass/'
# %%
from scipy.signal import butter,filtfilt

def butter_filter(data, cutoff, fs, order, btype):
    b, a = butter(order, cutoff, btype=btype, analog=False, fs=fs)
    y = np.apply_along_axis(lambda m: filtfilt(b, a, m), axis=0, arr=data)
    return y

sampling_freq = 1/2 # 1/(2h)
cutoff        = np.array([1/6, 1/3]) # 1/(6h)
# cutoff   = np.array([f[2]*0.85, f[2]*1.15])/24
cutoff        = np.array([1/26, 1/22]) # diurnal
cutoff        = np.array([1/14, 1/10]) # semidurnal
cutoff        = np.array([1/7, 1/5]) # 1/(6h)
order         = 5
btype         = 'bandpass'
# %%
d =85
data = 0.5*(u.u.isel(depth=d).compute()**2 + v.v.isel(depth=d).compute()**2)
data = data.rename('KE')
# data = to.to.isel(depth=d).compute()
hov = fu.select_circle(data, ickdtree, theta)
# %%
data = hov
y    = butter_filter(data.fillna(0.), cutoff, sampling_freq, order, btype=btype)
data_filt = xr.DataArray(y, dims=['time', 'theta'], coords={'time': data.time, 'theta': data.theta})
data_filt = data_filt.assign_coords({'depthi': u.depth[d]})
data_filt = data_filt.rename(f'KE_{cutoff[0]:.2f}_{cutoff[1]:.2f}')
fu.plot_hov(data_filt, savefig=True, cmap='RdBu_r', vmin=-0.001, vmax=0.001, fig_path=fig_path)


# %%
if False:
    plt.figure(figsize=(15,4))
    cb = plt.pcolormesh(np.arange(631), hov.theta/np.pi, hov.transpose(), cmap='RdBu', vmin=-0.005, vmax=0.005)
    plt.colorbar(cb)
    plt.xlabel('time in 2h intervals')
    plt.ylabel(r'$\theta / \pi$')
    plt.title(f'Hovmöller Diagram to at {hov.depthi.data:.0f}m')
    # change ticks to 1day intervals
    plt.xticks(np.arange(0,631, 12), np.arange(0,631, 12)*2)
    #decrease font size
    plt.xticks(fontsize=5)

    if savefig==True: plt.savefig(fig_path+f'hov_w_{hov.depthi.data:.0f}.png', dpi=180)

    fig = plt.figure(figsize=(15, 8))
    gs = fig.add_gridspec(2, 1, height_ratios=[1, 3])

    # First subplot (smaller height)
    ax0 = fig.add_subplot(gs[0, 0])
    ax0.plot(np.arange(631), hov.isel(theta=25)/np.pi)
    ax0.set_xticks([])  # Hide x-ticks for the first subplot
    ax0.set_title(f'Hovmöller Diagram of {hov.name} at {hov.depthi.data:.0f}m', fontsize=14)
    ax0.set_xlim(0, 631)

    # Second subplot (larger height)
    ax1 = fig.add_subplot(gs[1, 0])
    cb = ax1.pcolormesh(np.arange(631), hov.theta / np.pi, hov.transpose(), cmap='RdBu', vmin=-0.005, vmax=0.005)
    #  add small colorbar below
    fig.colorbar(cb, ax=ax1, orientation='horizontal', pad=0.1, aspect=40, shrink=0.8)
    # fig.colorbar(cb, ax=ax1)
    ax1.set_xlabel('time [days]')
    ax1.set_ylabel(r'$\theta / \pi$')
    # add tab:blue line at theta = 25
    ax1.axhline(0.5, color='tab:blue', linestyle='-', linewidth=2)

    # Change ticks to 1-day intervals
    ax1.set_xticks(np.arange(0, 631, 12))
    ax1.set_xticklabels([f'{int(x*2/24)}' for x in np.arange(0, 631, 12)])

    plt.tight_layout()
    if savefig==True: plt.savefig(fig_path+f'hov_w_{hov.depthi.data:.0f}.png', dpi=180)


# %%
