# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd
import eddy_funcs as fu

sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
import gsw
import xrft


# %%
import smt_modules.init_slurm_cluster as scluster 
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00', wait=False, scale=2)
cluster
client
# %%
tridx     = 0
eddy_type = 'anticyclonic'
fig_path  = f'/home/m/m300878/submesoscaletelescope/results/smt_wave/eddy_features/spectra/{eddy_type}/ID{tridx}/'
savefig   = True

# %%
u = fu.load_eddy_comp('u', tridx=tridx, eddy_type=eddy_type)
v = fu.load_eddy_comp('v', tridx=tridx, eddy_type=eddy_type)
u, v = xr.align(u, v, join='inner')
KE       = (u**2 + v**2)
# %%
reload(fu)
depths= [0, 17, 30, 50, 80, 105]

for idepth in depths:
    data_sel = KE.isel(depth=idepth)
    if eddy_type == 'cyclonic' and tridx == 3:
        data_sel = data_sel.sel(y=slice(-1.5,2.5)) #cyclonic ID3
    if eddy_type == 'anticyclonic' and tridx == 5:
        data_sel = data_sel.sel(y=slice(-1.2,2.5), x=slice(-2.5,1.2)) #anticyclonic ID5
    if eddy_type == 'anticyclonic' and tridx == 0:
        data_sel = data_sel.sel(y=slice(-1.5,2.5)) #anticyclonic ID5

    if idepth==17: savemask = True
    else: savemask = False
    radius = 1.2
    mask   = fu.calc_mask(data_sel, radius, fig_path,  savefig=savemask, itime=100)
    f      = fu.compute_f(data_sel)
    f_eddy = fu.compute_eddy_freq(data_sel,u,v,idepth,radius=radius*0.5,itime=100)

    data_rechunk = data_sel.drop('loncen').drop('latcen').drop('depth').chunk({'time': data_sel.time.size})
    P = fu.compute_spectra_uchida(data_rechunk, samp_freq=2)

    fu.plot_spectra_uchida(P, mask, f, np.abs(f_eddy), data_sel.depth.values, fig_path, savefig)

 
#%%###################### %% Show compostion an dcontribution from seamount
if True:
    reload(fu)
    for idepth in depths:
        data_sel = KE.isel(depth=idepth)
        if eddy_type == 'anticyclonic' and tridx == 0:
            data_sel = data_sel.sel(y=slice(-1.5,2.5)) #anticyclonic ID5


        if idepth==17: savemask = True
        else: savemask = False
        mask   = fu.calc_mask(data_sel, radius, fig_path,  savefig=savemask, itime=100)
        radius = 1.4
        f      = fu.compute_f(data_sel)
        f_eddy = fu.compute_eddy_freq(data_sel,u,v,idepth,radius=radius*0.5,itime=100)

        data_sel_1 = data_sel.isel(time=slice(0,350))
        data_sel_2 = data_sel.isel(time=slice(350,-1))
        data_rechunk = data_sel_1.drop('loncen').drop('latcen').drop('depth').chunk({'time': data_sel.time.size})
        P1 = fu.compute_spectra_uchida(data_rechunk, samp_freq=2)
        data_rechunk = data_sel_2.drop('loncen').drop('latcen').drop('depth').chunk({'time': data_sel.time.size})
        P2 = fu.compute_spectra_uchida(data_rechunk, samp_freq=2)

        fu.plot_spectra_mountain(P1, P2, mask, f, np.abs(f_eddy), data_sel.depth.values, fig_path, savefig)
# %%
