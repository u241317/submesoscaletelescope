# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd
import eddy_funcs as fu

sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
import gsw
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()


# %%
import smt_modules.init_slurm_cluster as scluster 
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, scale=2, dash_address=8787)
cluster
client
#%%
tridx     = 2
eddy_type = 'anticyclonic'
fig_path  = f'/home/m/m300878/submesoscaletelescope/results/smt_wave/eddy_features/{eddy_type}/ID{tridx}/'
savefig   = True


# %%
reload(fu)
subsel = np.array([99,105])
to  = fu.load_eddy_comp('to', tridx, eddy_type, subsel)
so  = fu.load_eddy_comp('so', tridx, eddy_type, subsel)
u   = fu.load_eddy_comp('u' , tridx, eddy_type, subsel)
v   = fu.load_eddy_comp('v' , tridx, eddy_type, subsel)
w   = fu.load_eddy_comp('w' , tridx, eddy_type, subsel)
w   = w.rename({'depth':'depthi'})
tke = fu.load_eddy_comp('tke', tridx, eddy_type, subsel)
tke = tke.rename({'depth':'depthi'})
zos = fu.load_eddy_comp('zos', tridx, eddy_type, subsel)

to, so, u, v, w, tke, zos = xr.align(to, so, u, v, w, tke, zos, join='inner')

# %%
tt  = so.time[5].values
so  = so.sel(time=tt)
to  = to.sel(time=tt)
w   = w.sel(time=tt)
u   = u.sel(time=tt)
v   = v.sel(time=tt)
tke = tke.sel(time=tt)
zos = zos.sel(time=tt)

# %% ####################### compute density and N2 ############################
CT = gsw.CT_from_pt(so, to)
CT = CT.compute()
if False:
    rho = gsw.density.rho(so, CT, so.depth) #from absoulte salinity and potential temperature
    rho.isel(depth=10).compute().plot()

# %%
lat_broadcasted, so_broadcasted = xr.broadcast(so.latcen, so)
p_broadcasted = gsw.p_from_z(-so.depth, lat_broadcasted)

N2, pN2 = gsw.stability.Nsquared(so, CT, p_broadcasted, lat=lat_broadcasted, axis=0)
N2 = xr.DataArray(N2, name='N2', dims=['depthi', 'y', 'x'], coords={'depthi': w.depthi[1:-1], 'lat': so.y, 'lon': so.y})
N2 = N2.drop_vars(['lat', 'lon'])
N2 = N2.assign_coords({'x': so.x, 'y': so.y})

print('Introduction of depth error! \n mid pressure not at the interfaces of cells')
# %
if False:
    plt.figure()
    plt.semilogx(N2.isel(x=0).isel(y=250), N2.depthi)

    plt.figure()
    plt.semilogx(N2.isel(x=slice(125,375)).mean(dim='x').isel(y=250) , N2.depthi)
    plt.ylim(1500,0)
    # %
    np.sqrt(N2).isel(depthi=50).isel(y=250).plot()
# %% Depth check
if False:
    plt.figure(figsize=(3,10))
    plt.plot(np.arange(127),pN2[:,1,1],'o', label='p_new')
    plt.plot(np.arange(127), w.depthi[1:-1].data,'x', label='p_old')
    plt.ylim(1500,0)
    plt.legend()


# %% ################## visualize data ############################
reload(fu)
depth = 20
fu.plot_w(w, to, zos, depth, tridx, fig_path, savefig)
fu.plot_KE(u, v, to, zos, depth, tridx, fig_path, savefig)
fu.plot_TKE(tke, to, zos, depth, tridx, fig_path, savefig)
fu.plot_N2(N2, to, zos, depth, tridx, fig_path, savefig)
# %%

# %% ################## compute coriolis frequency and evlaute N/f ############################
f = eva.corfreq(so.y + so.latcen) # rad/day
f = f / 86400 # convert to rad/s
f = f / (2*np.pi) # conert to 1/s

omega = 7.2921159e-5 # rad/s
f_rad = 2 * omega * np.sin(np.deg2rad(so.y +so.latcen))
f_s   = f_rad / (2*np.pi)

# %%
NF = np.sqrt(np.abs(N2))/np.abs(f_s)
# %%
plt.figure()
NF.isel(depthi=depth).plot(vmin=0, vmax=1e3, cmap='Blues')
plt.contour(NF.x, NF.y, NF.isel(depthi=depth), levels=[200], colors='r', linewidths=1, label='NF=400')
proxy = [plt.Line2D([0], [0], color='r', linewidth=1)]
plt.legend(proxy, ['N/F=200'])
if savefig==True: plt.savefig(fig_path+f'NF_cont200_ID{tridx}.png', dpi=180)


# %%
plt.figure()
NF.isel(y=250).plot(vmin=0, vmax=1e3, cmap='Blues')
plt.contour(NF.x, NF.depthi, NF.isel(y=250), levels=[200], colors='r', linewidths=1)
plt.ylim(1500,0)
proxy = [plt.Line2D([0], [0], color='r', linewidth=1)]
plt.legend(proxy, ['N/F=200'])
if savefig==True: plt.savefig(fig_path+f'NF_cont200_slice_ID{tridx}.png', dpi=180)


# %%
