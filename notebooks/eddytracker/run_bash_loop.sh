#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j
#SBATCH --nodes=1
#SBATCH --ntasks=8
#SBATCH --partition=compute
#SBATCH --mem=256Gb
#SBATCH --time=08:00:00 
#SBATCH --exclusive 
#SBATCH --account=mh0033
#SBATCH --mail-type=FAIL 

module list

source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`
eddy_id=4
# loop over 7 runs
for i in {0..7}; do
    mpirun -np 8 python -u composite_tracks_3d_version2_mpi4py.py ${i} ${eddy_id} &
    echo "Job ${i} is running"
    wait
    echo "Job ${i} is finished"
done
# i=0
# mpirun -np 8 python -u composite_tracks_3d_version2_mpi4py.py ${i} &


enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

