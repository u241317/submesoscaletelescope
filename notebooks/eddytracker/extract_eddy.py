# %%
"""extracts eddy data on regular grid file for initialisation of idealised model"""
# from py_eddy_tracker.dataset.grid import RegularGridDataset
from datetime import datetime, timedelta
import numpy as np
from netCDF4 import Dataset
from matplotlib import pyplot as plt
import os 
import glob

# from py_eddy_tracker.featured_tracking.area_tracker import AreaTracker
# from py_eddy_tracker.tracking import Correspondances

import numpy as np
from datetime import datetime, timedelta
import xarray as xr
# %%
import smt_modules.all_funcs as eva
from importlib import reload
import pyicon as pyic
import sys
sys.path.insert(0, '../')
# import funcs as fu
from importlib import reload
# %%
import smt_modules.init_slurm_cluster as scluster 
reload(scluster)

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:15:00', account="bm1239", wait=True)
cluster
client

# %%
root_dir            = '/work/mh0033/m300878/eddy_tracks/agulhas_eddies/exp7/'
# path_data           = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/exp7_KE_100m_fine.nc'
path_save_composite = '/composite_exp7_KE_100m/'
varname             = 'zos'
wavelength          = 700  #choice of spatial cutoff for high pass filter in km
shape_error         = 25 #choice of shape error in km
eddy_dir            = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
eddy_type           = 'anticyclonic'
mindays             = 20 #min number of days for tracked eddy

tracker_dir=eddy_dir+'tracks/'

dlon       = 1.8
dlat       = 1.8
res        = 0.02
npts       = int(dlon/res) #number of points from centre

# anticyclone
ds = xr.open_dataset(tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+'deg_trackID_0.nc')
t = 700
# cyclone
# ds = xr.open_dataset(tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+'deg_trackID_7.nc')
# t = 150


# %%
fpath_ckdtree = '/work/mh0033/m300878/grids/smtwv_oce_2018/ckdtree/rectgrids/smtwv_oce_2018_res0.01_20W-20E_50S-10S.nc'
# fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
time   = ds.isel(time=t).time.data
latcen = ds.isel(time=t).lat.data
loncen = ds.isel(time=t).lon.data

lonmin = round(loncen-dlon,2)
lonmax = round(loncen+dlon,2)
latmin = round(latcen-dlat,2)
latmax = round(latcen+dlat,2)
# # %%
# def load_smt_wave_9(variable, it=1):
#     '''
#     strings are u,v,w,to,so,tke,2d,forcing
#     run  7-9

#     usage: da = eva.load_smt_wave('w', 8, t1=1414, t2=1415)
#     '''
#     exp = 9
#     if exp == 7: print('exp: Jul-Aug 2019')
#     elif exp==8: print('exp: Feb-Mar 2022')
#     elif exp==9: print('exp: July 2019; no tides!')

#     folder    = f'smtwv000{exp}/outdata_{variable}'
#     path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
#     # path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv0009/'

#     print('path to data', path)
#     if variable == '2d':
#         path_data = f'{path}/smtwv000{exp}_oce_2d_PT1H*.nc'
#     else:
#         path_data = f'{path}/smtwv000{exp}_oce_3d_{variable}*.nc'
#     flist     = np.array(glob.glob(path_data))
#     flist.sort()

#     chunks = dict(time=1, depth=1)
#     if variable == '2d':
#         chunks = dict(time=1)
#     print('chunks =', chunks)

#     flist = flist[1::it]

#     ds = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
#     ds = xr.decode_cf(ds) #restores timestamp
#     return ds

# ds = load_smt_wave_9('w')

# %%
ds_w = eva.load_smt_wave_9('w')
# %%
w = ds_w.sel(time=time, method='nearest')
w_regular = pyic.interp_to_rectgrid_xr(w, fpath_ckdtree, lon_reg=[lonmin, lonmax], lat_reg=[latmin, latmax])

# %%
ds_u = eva.load_smt_wave_9('u')
ds_v = eva.load_smt_wave_9('v')

# %%
u = ds_u.sel(time=time, method='nearest')
v = ds_v.sel(time=time, method='nearest')

u_regular = pyic.interp_to_rectgrid_xr(u, fpath_ckdtree, lon_reg=[lonmin, lonmax], lat_reg=[latmin, latmax])
v_regular = pyic.interp_to_rectgrid_xr(v, fpath_ckdtree, lon_reg=[lonmin, lonmax], lat_reg=[latmin, latmax])

# %%
ds_T = eva.load_smt_wave_9('to')
ds_S = eva.load_smt_wave_9('so')
# %%
to = ds_T.sel(time=time, method='nearest')
so = ds_S.sel(time=time, method='nearest')

to_regular = pyic.interp_to_rectgrid_xr(to, fpath_ckdtree, lon_reg=[lonmin, lonmax], lat_reg=[latmin, latmax])
so_regular = pyic.interp_to_rectgrid_xr(so, fpath_ckdtree, lon_reg=[lonmin, lonmax], lat_reg=[latmin, latmax])
# %%
import gsw
lat_mesh = np.meshgrid(so_regular.lat, so_regular.lon)[0]
# make xarray
lat_mesh_xr = xr.DataArray(lat_mesh, dims=['lat', 'lon'])

p = gsw.p_from_z(-so_regular.depth, lat_mesh_xr)


# %%
CT = gsw.CT_from_pt(so_regular.so, to_regular.to)
SA = gsw.SA_from_SP(so_regular.so, p, so_regular.lon, so_regular.lat)

# N2, pN2 = gsw.Nsquared(so_regular.so, to_regular.to, p, lat=so_regular.lat, axis=0)
N2, pN2 = gsw.Nsquared(SA, CT, p, lat=so_regular.lat, axis=0)
N2_xr = xr.DataArray(N2, name='N2', dims=['depth', 'lat', 'lon'], coords={'depth': so_regular.depth[:-1], 'lat': so_regular.lat, 'lon': so_regular.lon})

# %%
N2_ave = N2_xr.mean(dim='lon').mean(dim='lat')
plt.figure(figsize=(10,10))
N2_ave.plot(y='depth')
plt.ylim(500,0)
plt.xlim(-1e-5,5e-5)


# %%
dS_2d = eva.load_smt_wave_9('2d')
# %%
ds_2d = dS_2d.drop('ice_u').drop('ice_v').drop('conc').drop('hi').drop('hs').drop('stretch_c')#.drop('ice_v_cat').drop('ice_conc_cat').drop('ice_thick_cat').drop('ice_type_cat')

ds_2d = ds_2d.sel(time=to_regular.time.data, method='nearest')

field_2d = pyic.interp_to_rectgrid_xr(ds_2d, fpath_ckdtree, lon_reg=[lonmin, lonmax], lat_reg=[latmin, latmax])


# %% save datasets
path = f'/work/mh0033/m300878/crop_interpolate/smtwv/eddy_inital/exp9/anticyclone/hires/'

field_2d.to_netcdf(path+'2d.nc')
u_regular.to_netcdf(path+'u.nc')
v_regular.to_netcdf(path+'v.nc')
w_regular.to_netcdf(path+'w.nc')
to_regular.to_netcdf(path+'to.nc')
so_regular.to_netcdf(path+'so.nc')
N2_xr.to_netcdf(path+'N2.nc')
# %%

# kin = u_regular.u**2 + v_regular.v**2
# kin.isel(depth=10).plot(vmin=0, vmax=0.25)
# %%
# %%
#open w and to
# w = xr.open_dataset(path+'w.nc')
# # %%
# to = xr.open_dataset(path+'to.nc')
# # %%
