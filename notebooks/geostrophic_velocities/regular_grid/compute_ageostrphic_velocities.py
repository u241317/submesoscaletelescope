# %%
import sys
sys.path.insert(0, "../../")

import glob, os
import dask as da
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload

import pyicon as pyic
sys.path.insert(0, '../../')
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 
import cartopy

# %%
savefig       = False
# lon_reg_R0    = [-80.5, -55]
# lat_reg_R0    = [23, 38]
lon_reg       = np.array([-70, -54])
lat_reg       = np.array([23.5, 36])
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
path_data     = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/geostrophic_velocities/'
# %% get cluster
reload(scluster)
client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00', wait=False)
cluster
client
# %%
u_geo = xr.open_dataset(path_data + 'u_geo.nc', chunks={'time': 1})
v_geo = xr.open_dataset(path_data + 'v_geo.nc', chunks={'time': 1})
# %%
ds = eva.load_smt_v()

# %%
ds_uv = pyic.interp_to_rectgrid_xr(ds.isel(time=1), fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
u_ageo = ds_uv.u - u_geo.isel(time=1).__xarray_dataarray_variable__
v_ageo = ds_uv.v - v_geo.isel(time=1).__xarray_dataarray_variable__
# %%
u_ageo.isel(depthc=5).plot(vmin=-5, vmax=5, cmap='RdBu_r')
# %%
u_geo.__xarray_dataarray_variable__.isel(time=1, depthc=3).plot(vmin=-5, vmax=5, cmap='RdBu_r')

# %%
ds_uv.u.isel(depthc=5).plot(vmin=-1, vmax=1, cmap='RdBu_r')

# %%
#plot decompostion in singel figure
fig, axs = plt.subplots(1, 3, figsize=(15, 5), subplot_kw={'projection': ccrs_proj})

#plot u_geo
ax = axs[0]
u_geo.__xarray_dataarray_variable__.isel(time=1, depthc=2).plot(ax=ax, vmin=-1, vmax=1, cmap='RdBu_r')
ax.set_title('u_geo')

#plot u_ageo
ax = axs[1]
u_ageo.isel(depthc=2).plot(ax=ax, vmin=-1, vmax=1, cmap='RdBu_r')
ax.set_title('u_ageo')

#plot u
ax = axs[2]
ds_uv.u.isel(depthc=2).plot(ax=ax, vmin=-1, vmax=1, cmap='RdBu_r')
ax.set_title('u')

plt.show()
# %%
v_geo.__xarray_dataarray_variable__.isel(time=5, depthc=5).plot(vmin=-0.5, vmax=0.5, cmap='RdBu_r')
#%%
u_geo.__xarray_dataarray_variable__.isel(time=5, depthc=5).plot(vmin=-0.5, vmax=0.5, cmap='RdBu_r')

# %%
