# %%
import sys
sys.path.insert(0, "../../")

import glob, os
import dask as da
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload

import pyicon as pyic
sys.path.insert(0, '../../')
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 
import cartopy

# %%
savefig       = False
# lon_reg_R0    = [-80.5, -55]
# lat_reg_R0    = [23, 38]
lon_reg       = np.array([-70, -54])
lat_reg       = np.array([23.5, 36])
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
path_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/geostrophic_velocities/'
# %% get cluster
reload(scluster)
client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00', wait=False, dash_address='8787')
cluster
client

# %%
mask = eva.load_smt_land_mask()
mask_land = pyic.interp_to_rectgrid_xr(mask, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land = mask_land.drop('depthc')
# %%

ds = eva.load_smt_b_fast()
da = ds.b.isel(time=slice(0, 5))

# %%
print('Interpolating data')
da_interp = pyic.interp_to_rectgrid_xr(da, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
da_interp = da_interp * mask_land
# da_interp.isel(depthi=20).plot(vmin=-2e-2, vmax=0)

# %% 
b        = da_interp
b_depthc = b.interp(depthi=depthc, method='linear')
b_depthc = b_depthc.rename(depthi='depthc')
# b_depthc.isel(depthc=20).plot(vmin=-2e-2, vmax=0)

# %% check interpolation
if False:
    plt.figure()
    plt.plot(b_depthc.sel(lon=-70, lat=30, method='nearest'), depthc, 'o')
    plt.plot(b.sel(lon=-70, lat=30, method='nearest'), depthi, 'x')
# %%
rho0       = 1025
g          = 9.81
dzw_c      = xr.DataArray(dzw, dims=['depthc'])
depthc_xr  = xr.DataArray(depthc, dims=['depthc'])
b_weighted = b_depthc * dzw_c

# %% 

# %%
print('Computing Pressure')
P = np.zeros(b_weighted.shape)
P = xr.DataArray(P, dims=['time','depthc', 'lat', 'lon'])
# ancient method without time
if False:
    for i in range(1, len(depthc)):
        if i == 1:
            P[:,i,:,:] = - g * rho0 * depthc_xr[i]
        else:
            P[:,i,:,:] = - g * rho0 * depthc_xr[i] + b_weighted.isel(depthc=slice(0,i)).sum(dim='depthc', skipna=True)
else:
    # % vectorized
    P[:, 1, :, :] = - g * rho0 * depthc_xr[1]
    # Compute the rest using vectorized operations
    depthc_indices = np.arange(2, len(depthc_xr))
    P[:, 2:, :, :] = (- g * rho0 * depthc_xr[2:] + b_weighted.isel(depthc=slice(0,  len(depthc_xr)-1)).cumsum(dim='depthc', skipna=True).isel    (depthc=depthc_indices-1)).transpose('time', 'depthc', 'lat', 'lon')
# %%
P = P * mask_land
P.isel(time=1, depthc=6).plot()
# %%
print('Computing Geostrophic Velocities')
dPdx      = P.differentiate('lon')
dPdy      = P.differentiate('lat')
fcoriolis = eva.calc_coriolis_parameter(P.lat)

v_geo = 1 / fcoriolis / rho0 * dPdx
u_geo = - 1 / fcoriolis / rho0 * dPdy

# v_geo.isel(time=5, depthc=5).plot(vmin=-0.5, vmax=0.5, cmap='RdBu_r')
u_geo.isel(time=1, depthc=5).plot(vmin=-0.5, vmax=0.5, cmap='RdBu_r')
# %%
u_geo.isel(time=1, depthc=50).plot(vmin=-0.5, vmax=0.5, cmap='RdBu_r')

# %%
print('Saving data')
# u_geo.to_netcdf(path_data + 'u_geo.nc')
# v_geo.to_netcdf(path_data + 'v_geo.nc')

# %%
client.close()
cluster.close()


# %%
######################################### from to and so #########################################
def load_data():
    print('loading data')
    ds_T = eva.load_smt_T()
    ds_S = eva.load_smt_S()
    time = '2010-03-17-01:00:00'
    to   = ds_T.sel(time=time, method='nearest').compute()
    so   = ds_S.sel(time=time, method='nearest').compute()
    return to, so
to, so = load_data()
# %%
to_inter = pyic.interp_to_rectgrid_xr(to, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
so_inter = pyic.interp_to_rectgrid_xr(so, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
import gsw
rho = gsw.density.rho(so_inter, to_inter, depthc[:, np.newaxis, np.newaxis])
# %%
g       = 9.81
rho0    = 1025
# dzt_     = xr.DataArray(dzt, dims=['depthc'])

pres       = np.zeros(rho.shape)
pres[0,:]  = g/rho0 * rho[0,:]*dzt[0]
pres[1:,:] = pres[0:1,:] + g/rho0 * np.cumsum(0.5*(rho[1:,:].data+rho[:-1,:].data)*dzt[1:-1, np.newaxis, np.newaxis], axis=0)

#%%
#make xarray
pres = xr.DataArray(pres, dims=['depthc', 'lat', 'lon'])

# %%
ds = eva.load_smt_v()
ds = ds.sel(time='2010-03-17-01:00:00')
ds_uv = pyic.interp_to_rectgrid_xr(ds, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# surface contribution
zos = eva.load_smt_ssh()
zos = zos.h_sp.sel(time='2010-03-17-01:00:00').compute()
# %%
zos_inter = pyic.interp_to_rectgrid_xr(zos, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
p_surf = np.zeros_like(pres.isel(depthc=0))
#make float64
p_surf = g * zos_inter
p_surf = p_surf.astype('float64')
# %%
pres_ = pres #+ p_surf

#%%
#same in xarray
dpdx = pres_.differentiate('lon') / rho0
dpdy = pres_.differentiate('lat') / rho0

# %%
if False:
    dpdx_surf = p_surf_inter.differentiate('lon') / rho0
    dpdy_surf = p_surf_inter.differentiate('lat') / rho0

    dpdx = dpdx + dpdx_surf
    dpdy = dpdy + dpdy_surf
# %%
f = eva.calc_coriolis_parameter(rho.lat)
vg =   dpdx / f
ug = - dpdy / f

# %%
dzt_ = xr.DataArray(dzt[:-1], dims=['depthc'])

# %%
lon = 300
lat = 440

plt.figure(figsize=(5,10))
plt.plot(ds_uv.v.isel(lon=lon,lat=lat), depthc, marker='o', label='v')
plt.plot(vg.isel(lon=lon,lat=lat), depthc, marker='x', label='geostrophic')
plt.legend()
plt.grid()
plt.ylim(5500, 0)
plt.axvline(x=0, color='r')
# %%
plt.figure(figsize=(5,10))
plt.plot(ds_uv.u.isel(lon=lon,lat=lat), depthc, marker='o', label='v')
plt.plot(ug.isel(lon=lon,lat=lat), depthc, marker='x', label='geostrophic')
plt.legend()
plt.grid()
plt.ylim(5500, 0)
plt.axvline(x=0, color='r')
# %%
