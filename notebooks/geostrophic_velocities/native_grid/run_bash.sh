#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --time=03:10:00
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j
#SBATCH --partition=compute
#SBATCH --account=mh0033
#SBATCH --mem=512G  # Request 16GB of memory

module list
source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`

# python -u calc_dpdx_from_dbdx.py --slurm
# python -u compute_pressure.py --slurm
python -u calc_dp_from_p.py --slurm

enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

