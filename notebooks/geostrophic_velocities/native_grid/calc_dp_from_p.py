# %%
import sys
sys.path.insert(0, "../../")

import glob, os
import dask as da
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload

import pyicon as pyic
sys.path.insert(0, '../../')
sys.path.insert(0, '../../../')
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 
import cartopy
import gsw
from netCDF4 import Dataset


# %%
def calc_grid_coeff():

    fpath_tgrid  = '/home/m/m300602/work/icon/grids/smt/smt_tgrid.nc'
    f = Dataset(fpath_tgrid, 'r')
    clon = f.variables['clon'][:] * 180./np.pi # center longitude
    clat = f.variables['clat'][:] * 180./np.pi # center latitude
    # vlon = f.variables['vlon'][:] * 180./np.pi # vertex longitude
    # vlat = f.variables['vlat'][:] * 180./np.pi # vertex latitude
    # vertex_of_cell = f.variables['vertex_of_cell'][:].transpose()-1 # vertices of each cellcells ad
    edge_of_cell = f.variables['edge_of_cell'][:].transpose()-1 # edges of each cellvertices
    # edges_of_vertex = f.variables['edges_of_vertex'][:].transpose()-1 # edges around vertex
    dual_edge_length = f.variables['dual_edge_length'][:] # lengths of dual edges (distances between triangul 
    # edge_orientation = f.variables['edge_orientation'][:].transpose()
    edge_length = f.variables['edge_length'][:] # lengths of edges of triangular cells
    # cell_area_p = f.variables['cell_area_p'][:] # area of grid cell 
    # dual_area = f.variables['dual_area'][:] # areas of dual hexagonal/pentagonal cells
    #edge_length = f.variables['edge_length'][:]
    adjacent_cell_of_edge = f.variables['adjacent_cell_of_edge'][:].transpose()-1 # cells adjacent to each edge 
    orientation_of_normal = f.variables['orientation_of_normal'][:].transpose() # orientations of normals to triangular cell edges
    edge_vertices = f.variables['edge_vertices'][:].transpose()-1 #vertices at the end of of each edge
    f.close()

    dtype = 'float32'
    f = Dataset(fpath_tgrid, 'r')
    elon = f.variables['elon'][:] * 180./np.pi #edge midpoint longitude
    elat = f.variables['elat'][:] * 180./np.pi #edge midpoint latitude
    cell_cart_vec = np.ma.zeros((clon.size,3), dtype=dtype)
    cell_cart_vec[:,0] = f.variables['cell_circumcenter_cartesian_x'][:] # cartesian position of the prime cell circumcenter
    cell_cart_vec[:,1] = f.variables['cell_circumcenter_cartesian_y'][:]
    cell_cart_vec[:,2] = f.variables['cell_circumcenter_cartesian_z'][:]
    edge_cart_vec = np.ma.zeros((elon.size,3), dtype=dtype)
    edge_cart_vec[:,0] = f.variables['edge_middle_cartesian_x'][:] # prime edge center cartesian coordinate x on unit
    edge_cart_vec[:,1] = f.variables['edge_middle_cartesian_y'][:]
    edge_cart_vec[:,2] = f.variables['edge_middle_cartesian_z'][:]
    f.close()

    grid_sphere_radius = 6.371229e6
    dist_vector        = edge_cart_vec[edge_of_cell,:] - cell_cart_vec[:,np.newaxis,:]
    norm               = np.sqrt(pyic.scalar_product(dist_vector,dist_vector,dim=2))
    prime_edge_length  = edge_length/grid_sphere_radius
    fixed_vol_norm     = (0.5 * norm * (prime_edge_length[edge_of_cell]))
    fixed_vol_norm     = fixed_vol_norm.sum(axis=1)
    edge2cell_coeff_cc = (dist_vector * (edge_length[edge_of_cell,np.newaxis] / grid_sphere_radius) * orientation_of_normal[:,:,np.newaxis] )

    sinLon = np.sin(clon*np.pi/180.)
    cosLon = np.cos(clon*np.pi/180.)
    sinLat = np.sin(clat*np.pi/180.)
    cosLat = np.cos(clat*np.pi/180.)

    grad_coeff = (1./dual_edge_length)
    return grad_coeff, edge2cell_coeff_cc, fixed_vol_norm, edge_of_cell, adjacent_cell_of_edge


def xr_edges2cell(ds_IcD, ve, dze, dzc, edge_of_cell, edge2cell_coeff_cc=None, fixed_vol_norm=None):
    """Remaps vector from edges to cell

    Parameters
    ----------
    ds_IcD : xr.Dataset
        pyicon dataset containing coordinate info


    ve : xr.DataArray
        vector on edges

    dze : xr.DataArray
        vertical grid spacing at edge midpoint

    dzc : xr.DataArray
        vertical grid spacing at cell centre

    edge2cell_coeff_cc : xr.DataArray or None
        coefficients used in mapping edges to cells

    fixed_vol_norm : xr.DataArray or None
        volume of grid cell


    Returns
    -------
    p_vn_c : xr.DataArray
        cartesian representation of vector ve on cell centres
    """
    # if fixed_vol_norm is None:
    #     fixed_vol_norm = xr_calc_fixed_volume_norm(ds_IcD)
    # if edge2cell_coeff_cc is None:
    #     edge2cell_coeff_cc = xr_calc_edge2cell_coeff_cc(ds_IcD)
    # if ve.dims != dze.dims:
    #  raise ValueError('::: Dims of ve and dze have to be the same!:::')
    print('start sum')
    p_vn_c = (
        edge2cell_coeff_cc
        * ve.isel(edge=edge_of_cell)
        # * ds_fx.prism_thick_e.isel(edge=ds_IcD.edge_of_cell)
        * dze.isel(edge=edge_of_cell)
    ).sum(dim="ne_c")
    print('end sum')
    if "depth_c" in p_vn_c.dims:
        p_vn_c = p_vn_c.transpose("depth_c", "cell", "cart")
    print('start div')
    p_vn_c = p_vn_c / (
        fixed_vol_norm
        # * ds_fx.prism_thick_c
        * dzc
    )
    return p_vn_c
# %%
savefig       = False
lon_reg       = np.array([-70, -54])
lat_reg       = np.array([23.5, 36])
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.30_180W-180E_90S-90N.nc'
path_data     = '/work/mh0033/m300878/smt/P/'
g             = 9.81
rho0          = 1025
# %% get cluster
reload(scluster)
client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00', wait=False)
cluster
client
# %%
pres = xr.open_dataset(path_data + 'pres.nc', chunks={'depthc': 1})
pres = pres.pres.compute()

# %%
# ds_grid    = eva.load_smt_grid()
# grad_coeff = (1./ds_grid.dual_edge_length).rename('grad_coeff')
# adjacent_cell_of_edge = ds_grid.adjacent_cell_of_edge.compute()
# adjacent_cell_of_edge = adjacent_cell_of_edge.transpose()
# adjacent_cell_of_edge = adjacent_cell_of_edge[:].transpose()-1

# %%
zos  = eva.load_smt_ssh()
time = '2010-03-17T01:00:00.000000000'
zos  = zos.h_sp.sel(time=time, method='nearest').compute()
# print('check times zos', zos.time, 'and pres', pres.time)

# %%
client.close()
cluster.close()

#%%
# grad_coeff, edge2cell_coeff_cc, fixed_vol_norm, edge_of_cell, adjacent_cell_of_edge = calc_grid_coeff()
# %%
# make xarrays
# grad_coeff            = xr.DataArray(grad_coeff, dims=['edge'])
# edge2cell_coeff_cc    = xr.DataArray(edge2cell_coeff_cc, dims=['edge', 'nc_e', 'cart'])
# fixed_vol_norm        = xr.DataArray(fixed_vol_norm, dims=['cell'])
# edge_of_cell          = xr.DataArray(edge_of_cell, dims=['cell', 'nc_e'])
# adjacent_cell_of_edge = xr.DataArray(adjacent_cell_of_edge, dims=['edge', 'nc_e'])
# dze                   = xr.DataArray(dzw, dims=['depthc'])
# dzc                   = xr.DataArray(dzt, dims=['depthi'])

# %%


# %%

fpath_tgrid  = '/home/m/m300602/work/icon/grids/smt/smt_tgrid.nc'
f = Dataset(fpath_tgrid, 'r')
clon = f.variables['clon'][:] * 180./np.pi # center longitude
clat = f.variables['clat'][:] * 180./np.pi # center latitude
# vlon = f.variables['vlon'][:] * 180./np.pi # vertex longitude
# vlat = f.variables['vlat'][:] * 180./np.pi # vertex latitude
# vertex_of_cell = f.variables['vertex_of_cell'][:].transpose()-1 # vertices of each cellcells ad
edge_of_cell = f.variables['edge_of_cell'][:].transpose()-1 # edges of each cellvertices
# edges_of_vertex = f.variables['edges_of_vertex'][:].transpose()-1 # edges around vertex
dual_edge_length = f.variables['dual_edge_length'][:] # lengths of dual edges (distances between triangul 
# edge_orientation = f.variables['edge_orientation'][:].transpose()
edge_length = f.variables['edge_length'][:] # lengths of edges of triangular cells
# cell_area_p = f.variables['cell_area_p'][:] # area of grid cell 
# dual_area = f.variables['dual_area'][:] # areas of dual hexagonal/pentagonal cells
#edge_length = f.variables['edge_length'][:]
adjacent_cell_of_edge = f.variables['adjacent_cell_of_edge'][:].transpose()-1 # cells adjacent to each edge 
orientation_of_normal = f.variables['orientation_of_normal'][:].transpose() # orientations of normals to triangular cell edges
edge_vertices = f.variables['edge_vertices'][:].transpose()-1 #vertices at the end of of each edge
f.close()

dtype              = 'float32'
f                  = Dataset(fpath_tgrid, 'r')
elon               = f.variables['elon'][:] * 180./np.pi #edge midpoint longitude
elat               = f.variables['elat'][:] * 180./np.pi #edge midpoint latitude
cell_cart_vec      = np.ma.zeros((clon.size,3), dtype=dtype)
cell_cart_vec[:,0] = f.variables['cell_circumcenter_cartesian_x'][:] # cartesian position of the prime cell circumcenter
cell_cart_vec[:,1] = f.variables['cell_circumcenter_cartesian_y'][:]
cell_cart_vec[:,2] = f.variables['cell_circumcenter_cartesian_z'][:]
edge_cart_vec      = np.ma.zeros((elon.size,3), dtype=dtype)
edge_cart_vec[:,0] = f.variables['edge_middle_cartesian_x'][:] # prime edge center cartesian coordinate x on unit
edge_cart_vec[:,1] = f.variables['edge_middle_cartesian_y'][:]
edge_cart_vec[:,2] = f.variables['edge_middle_cartesian_z'][:]
f.close()

grid_sphere_radius = 6.371229e6
dist_vector        = edge_cart_vec[edge_of_cell,:] - cell_cart_vec[:,np.newaxis,:]
norm               = np.sqrt(pyic.scalar_product(dist_vector,dist_vector,dim=2))
prime_edge_length  = edge_length/grid_sphere_radius
fixed_vol_norm     = (0.5 * norm * (prime_edge_length[edge_of_cell]))
fixed_vol_norm     = fixed_vol_norm.sum(axis=1)
edge2cell_coeff_cc = (dist_vector * (edge_length[edge_of_cell,np.newaxis] / grid_sphere_radius) * orientation_of_normal[:,:,np.newaxis] )

sinLon = np.sin(clon*np.pi/180.)
cosLon = np.cos(clon*np.pi/180.)
sinLat = np.sin(clat*np.pi/180.)
cosLat = np.cos(clat*np.pi/180.)
grad_coeff = (1./dual_edge_length)
# %%

# gradh_p = (pres[:,adjacent_cell_of_edge[:,1]]-pres[:,adjacent_cell_of_edge[:,0]])*grad_coeff
# gradh_p = gradh_p.compute()


# %%
if False:
    psurf = np.tile(g * zos.h_sp.data[np.newaxis,:], [112,1])
    gradh_ps = (psurf[:,adjacent_cell_of_edge[:,1]]-psurf[:,adjacent_cell_of_edge[:,0]])*grad_coeff
# %%
psurf = g * zos.data #np.tile(g * zos.h_sp.data, [112,1])
gradh_ps = (psurf[adjacent_cell_of_edge[:,1]]-psurf[adjacent_cell_of_edge[:,0]])*grad_coeff


#%%

# p_gradh_p  = xr_edges2cell(gradh_p, dze, dze, edge_of_cell, edge2cell_coeff_cc, fixed_vol_norm)
# %%
# dpdx, dpdy = pyic.xr_calc_2dlocal_from_3d(ds_grid, p_gradh_p)
# %%

# File paths
fpath_dpdx = f'{path_data}dpdx2.nc'
fpath_dpdy = f'{path_data}dpdy2.nc'

# Create and write dpdx file
fo_dpdx = Dataset(fpath_dpdx, 'w', format='NETCDF4')
fo_dpdx.createDimension('depthc', depthc.size)
fo_dpdx.createDimension('ncells', pres.ncells.size)
nc_dpdx = fo_dpdx.createVariable('dpdx', 'f4', ('depthc', 'ncells'))
ncv_dpdx = fo_dpdx.createVariable('depthc', 'f4', 'depthc')

fo_dpdy = Dataset(fpath_dpdy, 'w', format='NETCDF4')
fo_dpdy.createDimension('depthc', depthc.size)
fo_dpdy.createDimension('ncells', pres.ncells.size)
nc_dpdy = fo_dpdy.createVariable('dpdy', 'f4', ('depthc', 'ncells'))
ncv_dpdy = fo_dpdy.createVariable('depthc', 'f4', 'depthc')


for lev in np.arange(depthc.size):
        print('level', depthc[lev])
        gradh_p = (pres[lev,adjacent_cell_of_edge[:,1]].data-pres[lev,adjacent_cell_of_edge[:,0]].data)*grad_coeff
        gradh_p += gradh_ps 
        # gradh_p = gradh_p.compute()
        print('start interp from edge to cell')
        p_vn_c = ( edge2cell_coeff_cc[:,:,:]
                * gradh_p[edge_of_cell,np.newaxis]
                * dzw[lev]
                ).sum(axis=1)
        
        p_vn_c *= 1./(fixed_vol_norm[:,np.newaxis]*dzw[lev])

        print('start calc 2d local')
        u1 = p_vn_c[:,0]
        u2 = p_vn_c[:,1]
        u3 = p_vn_c[:,2]
        dpdx =   u2*cosLon - u1*sinLon
        dpdy = -(u1*cosLon + u2*sinLon)*sinLat + u3*cosLat

        nc_dpdx[lev,:] = dpdx[:pres.ncells.size]
        nc_dpdy[lev,:] = dpdy[:pres.ncells.size]
        print('end level', depthc[lev])

# nc_dpdx[0,:]  = np.NaN
# nc_dpdy[0,:]  = np.NaN
# nc_dpdx[-1,:] = np.NaN
# nc_dpdy[-1,:] = np.NaN

fo_dpdx.close()
fo_dpdy.close()

# %%
# fpatho   = f'{path_data}dpdx.nc'
# fo = Dataset(fpatho, 'w', format='NETCDF4')
# #fo.createDimension('depthc', nz) # depth
# fo.createDimension('depthc', depthc.size) 
# fo.createDimension('ncells', pres.ncells) # new dim cell center
# nc_dpdx  = fo.createVariable('dpdx','f4',('depthc','ncells'))
# nc_dpdy  = fo.createVariable('dpdy','f4',('depthc','ncells'))
# ncv      = fo.createVariable('depthc','f4','depthc')
# # ncv[:]   = depthc[: