# %%
import sys
sys.path.insert(0, "../../")

import glob, os
import dask as da
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload

import pyicon as pyic
sys.path.insert(0, '../../')
sys.path.insert(0, '../../../')
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 
import cartopy
import gsw
from netCDF4 import Dataset

# %%
savefig       = False
lon_reg       = np.array([-70, -54])
lat_reg       = np.array([23.5, 36])
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.30_180W-180E_90S-90N.nc'
path_data     = '/work/mh0033/m300878/smt/P/'
# %% get cluster
reload(scluster)
client, cluster = scluster.init_dask_slurm_cluster(walltime='00:20:00', wait=False)
cluster
client

# %%
rho0 = 1025     # Reference density in kg/m³
g    = 9.81     # Gravitational acceleration in m/s²
if False:
    print('rho is not the same, computed from ts or from b!!!!')
    if False:
        ds = eva.load_smt_b_fast() #all
        da_sel   = ds.b.isel(time=0)
        b        = da_sel
        b_depthc = b.interp(depthi=depthc, method='linear')
        b_depthc = b_depthc.rename(depthi='depthc')
        # rechunk to depthc 
        b_depthc = b_depthc.chunk({'depthc': 1})
    else:
        b_depthc = eva.load_smt_b_depthc()
        b_depthc = b_depthc.b

    # b_depthc = b_depthc.compute()
    # %
    rho  = rho0 - (b_depthc * rho0 / g)
    rho  = rho.isel(time=0).compute()
else:
    print('loading data')
    def load_data():
        ds_T = eva.load_smt_T()
        ds_S = eva.load_smt_S()

        time = '2010-03-17-00:00:00'
        to   = ds_T.sel(time=time, method='nearest').compute()
        so   = ds_S.sel(time=time, method='nearest').compute()
        return to, so
    to, so = load_data()

    print('computing rho')
    rho = gsw.density.rho(so, to, depthc[:, np.newaxis])
    print('done')
# %%
client.close()
cluster.close()

# %%
# memory intensive
if False:
    data = 0.5 * (rho[1:, :].data + rho[:-1, :].data) * dzt[1:-1]
    pres       = np.zeros(rho.shape)
    for i in range(1, len(depthc)):
        print(i)
        if i == 1:
            pres[i,:] = g/rho0 * rho[i,:]*dzt[i,:]
        else:
            pres[i,:] = g/rho0 * rho[i,:]*dzt[i,:] + data[:i-1,:].sum(axis=0)

    pres = xr.DataArray(pres, dims=['depthc', 'ncells'])

    pres.to_netcdf(path_data + 'pres.nc')
# %%

if False: # cumsum is too memory intensive, and not easy to make it a dask problem
    # %%
    dzt        = xr.DataArray(dzt, dims=['depthc'])
    pres       = np.zeros(rho.shape)
    pres       = xr.DataArray(pres, dims=['depthc', 'ncells'])
    pres[0,:]  = g/rho0 * rho.isel(depthc=0) * dzt.isel(depthc=0)
    pres[1:,:] = pres[0:1,:] + g/rho0 * np.cumsum(0.5*(rho[1:,:]+rho[:-1,:])*dzt[1:-1], axis=0)

    # %%
    dzt_i        = xr.DataArray(dzt[:-1], dims=['depthc'])
    depthc_xr    = xr.DataArray(depthc, dims=['depthc'])
    rho_weighted = rho * dzt_i
    P            = np.zeros(rho.shape)
    P            = xr.DataArray(P, dims=['depthc', 'ncells'])
    P[1, :]      = g/rho0 * rho.isel(depthc=0) * dzt_i.isel(depthc=0)
    # Compute the rest using vectorized operations
    depthc_indices = np.arange(2, len(depthc_xr))
    P[2:, :] = P[1, :] +(g/rho0 * rho_weighted.isel(depthc=slice(0,  len(depthc_xr))).cumsum(dim='depthc', skipna=True).isel(depthc=depthc_indices-1))


# %%


# Assuming rho, g, rho0, dzt, depthc, and data are already defined
# Initialize pres with zeros
# pres = np.zeros(rho.shape)

# Create NetCDF file and define dimensions and variables
ncfile_path = f'{path_data}pres2.nc'
with Dataset(ncfile_path, 'w', format='NETCDF4') as ncfile:
    # Define dimensions
    ncfile.createDimension('depthc', len(depthc))
    ncfile.createDimension('ncells', rho.shape[1])
    #add time
    ncfile.createDimension('time', None)
    time_var = ncfile.createVariable('time', 'f4', ('time',))

    # Define variables
    depthc_var = ncfile.createVariable('depthc', 'f4', ('depthc',))
    # ncells_var = ncfile.createVariable('ncells', 'i4', ('ncells',))
    pres_var   = ncfile.createVariable('pres', 'f4', ('depthc', 'ncells'))

    # Write depthc and ncells data
    depthc_var[:] = depthc
    # ncells_var[:] = np.arange(rho.shape[1])

    # Compute and write pres layer-wise
    dzt_ = dzt[:, np.newaxis]
    # data = 0.5 * (rho[1:, :].data + rho[:-1, :].data) * dzt[1:-1]

    cumulative_sum = np.zeros(rho.shape[1])  # Initialize cumulative sum for each cell

    for i in range(0, len(depthc)-1):
        print(i)
        if i == 0:
            pres_var[i, :] = g / rho0 * rho[i, :] * dzt_[i, :]
        else:
            cumulative_sum += 0.5 * (rho[i, :] + rho[i-1, :]) * dzt_[i, :]
            pres_var[i, :] = g / rho0 * rho[0, :] * dzt_[0, :] +  g / rho0 * cumulative_sum
            # pres[i, :] = g / rho0 * rho[i, :] * dzt[i, :] + data[:i-1, :].sum(axis=0)
        
        # Write the current layer to the NetCDF file
        # pres_var[i, :] = pres[i, :]

print(f"Data saved to {ncfile_path}")
# %%
