
# %%
import sys
sys.path.insert(0, "../../")

import glob, os
import dask as da
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload

import pyicon as pyic
sys.path.insert(0, '../../')
sys.path.insert(0, '../../../')
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 
import cartopy
import gsw
from netCDF4 import Dataset

# %%
savefig       = False
lon_reg       = np.array([-70, -54])
lat_reg       = np.array([23.5, 36])
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
path_data     = '/work/mh0033/m300878/smt/P/'
# %% get cluster
reload(scluster)
client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00', wait=False)
cluster
client
# %%
#open file
# ds = xr.open_dataset(path_data+'dpdx.nc', chunks={'depthi': 1})
ds = xr.open_dataset(path_data+'dpdx2.nc', chunks={'depthc': 1})

# %%
rho0 = 1025     # Reference density in kg/m³
dp = pyic.interp_to_rectgrid_xr(ds.dpdx, fpath_ckdtree, lon_reg, lat_reg)
dp[0,:,:] = np.nan
dp = dp / rho0
# %%
dp.isel(depthi=40).plot(vmin=-1e-5, vmax=1e-5, cmap='RdBu_r')

# %%
dp.isel(lon=200, lat=200).plot()
# %%
f = eva.calc_coriolis_parameter(dp.lat)
vg =  dp/f
# %%
d=10
(vg.isel(depthi=d)/dzt[d]).plot(vmin=-0.01, vmax=0.01, cmap='RdBu_r')

# %%
(vg.isel(depthi=d)).plot(vmin=-0.01, vmax=0.01, cmap='RdBu_r')
# %%
plt.figure(figsize=(5,10))
(vg.isel(lon=300,lat=150)/depthi).plot(y='depthi', yincrease=False)
plt.grid()
plt.ylim(1500, 0)
plt.axvline(x=0, color='r')
# %%
time='2010-03-15T21:00:00.000000000'
ds = eva.load_smt_v()
ds = ds.sel(time=time)

# %%
ds_uv = pyic.interp_to_rectgrid_xr(ds, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
plt.figure(figsize=(5,10))
# (vg.isel(lon=300,lat=150)/depthi).plot(y='depthi', yincrease=False)
# ds_uv.v.isel(lon=300,lat=150).plot(y='depthc', yincrease=False)
plt.plot(ds_uv.v.isel(lon=300,lat=440), depthc, marker='o', label='v')
plt.plot(vg.isel(lon=300,lat=440)*10, depthc, marker='x', label='geostrophic')
plt.plot(vg_old.isel(lon=300,lat=440), depthi, marker='x', label='geostrophic')
plt.legend()
plt.grid()
plt.ylim(5500, 0)
plt.axvline(x=0, color='r')












# %%
ds_grid    = eva.load_smt_grid()
grad_coeff = (1./ds_grid.dual_edge_length).rename('grad_coeff')
adjacent_cell_of_edge = ds_grid.adjacent_cell_of_edge.compute()
adjacent_cell_of_edge = adjacent_cell_of_edge.transpose()
adjacent_cell_of_edge = adjacent_cell_of_edge[:].transpose()-1
# %%
g        = 9.81
zos      = eva.load_smt_ssh()
# time     = '2010-03-17T01:00:00.000000000'
zos      = zos.h_sp.sel(time=time, method='nearest').compute()
psurf    = g * zos.data #np.tile(g * zos.h_sp.data, [112,1])
gradh_ps = (psurf[adjacent_cell_of_edge[:,1]]-psurf[adjacent_cell_of_edge[:,0]])*grad_coeff

# %%

fpath_tgrid  = '/home/m/m300602/work/icon/grids/smt/smt_tgrid.nc'
f = Dataset(fpath_tgrid, 'r')
clon = f.variables['clon'][:] * 180./np.pi # center longitude
clat = f.variables['clat'][:] * 180./np.pi # center latitude
# vlon = f.variables['vlon'][:] * 180./np.pi # vertex longitude
# vlat = f.variables['vlat'][:] * 180./np.pi # vertex latitude
# vertex_of_cell = f.variables['vertex_of_cell'][:].transpose()-1 # vertices of each cellcells ad
edge_of_cell = f.variables['edge_of_cell'][:].transpose()-1 # edges of each cellvertices
# edges_of_vertex = f.variables['edges_of_vertex'][:].transpose()-1 # edges around vertex
dual_edge_length = f.variables['dual_edge_length'][:] # lengths of dual edges (distances between triangul 
# edge_orientation = f.variables['edge_orientation'][:].transpose()
edge_length = f.variables['edge_length'][:] # lengths of edges of triangular cells
# cell_area_p = f.variables['cell_area_p'][:] # area of grid cell 
# dual_area = f.variables['dual_area'][:] # areas of dual hexagonal/pentagonal cells
#edge_length = f.variables['edge_length'][:]
adjacent_cell_of_edge = f.variables['adjacent_cell_of_edge'][:].transpose()-1 # cells adjacent to each edge 
orientation_of_normal = f.variables['orientation_of_normal'][:].transpose() # orientations of normals to triangular cell edges
edge_vertices = f.variables['edge_vertices'][:].transpose()-1 #vertices at the end of of each edge
f.close()

dtype              = 'float32'
f                  = Dataset(fpath_tgrid, 'r')
elon               = f.variables['elon'][:] * 180./np.pi #edge midpoint longitude
elat               = f.variables['elat'][:] * 180./np.pi #edge midpoint latitude
cell_cart_vec      = np.ma.zeros((clon.size,3), dtype=dtype)
cell_cart_vec[:,0] = f.variables['cell_circumcenter_cartesian_x'][:] # cartesian position of the prime cell circumcenter
cell_cart_vec[:,1] = f.variables['cell_circumcenter_cartesian_y'][:]
cell_cart_vec[:,2] = f.variables['cell_circumcenter_cartesian_z'][:]
edge_cart_vec      = np.ma.zeros((elon.size,3), dtype=dtype)
edge_cart_vec[:,0] = f.variables['edge_middle_cartesian_x'][:] # prime edge center cartesian coordinate x on unit
edge_cart_vec[:,1] = f.variables['edge_middle_cartesian_y'][:]
edge_cart_vec[:,2] = f.variables['edge_middle_cartesian_z'][:]
f.close()

grid_sphere_radius = 6.371229e6
dist_vector        = edge_cart_vec[edge_of_cell,:] - cell_cart_vec[:,np.newaxis,:]
norm               = np.sqrt(pyic.scalar_product(dist_vector,dist_vector,dim=2))
prime_edge_length  = edge_length/grid_sphere_radius
fixed_vol_norm     = (0.5 * norm * (prime_edge_length[edge_of_cell]))
fixed_vol_norm     = fixed_vol_norm.sum(axis=1)
edge2cell_coeff_cc = (dist_vector * (edge_length[edge_of_cell,np.newaxis] / grid_sphere_radius) * orientation_of_normal[:,:,np.newaxis] )

sinLon = np.sin(clon*np.pi/180.)
cosLon = np.cos(clon*np.pi/180.)
sinLat = np.sin(clat*np.pi/180.)
cosLat = np.cos(clat*np.pi/180.)
grad_coeff = (1./dual_edge_length)
# %%
p_vn_c = ( edge2cell_coeff_cc[:,:,:]
        * gradh_ps[edge_of_cell,np.newaxis]
        * dzw[0]
        ).sum(axis=1)

p_vn_c *= 1./(fixed_vol_norm[:,np.newaxis]*dzw[0])
print('start calc 2d local')
u1 = p_vn_c[:,0]
u2 = p_vn_c[:,1]
u3 = p_vn_c[:,2]
dpdx_zos =   u2*cosLon - u1*sinLon
dpdy_zos = -(u1*cosLon + u2*sinLon)*sinLat + u3*cosLat