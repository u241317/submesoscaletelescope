

# %%
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset
import sys
import pyicon as pyic
import cartopy
import seawater as sw
import xarray as xr


import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload

import pyicon as pyic
sys.path.insert(0, '../../')
sys.path.insert(0, '../../../')
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 
import cartopy
import gsw
from netCDF4 import Dataset

# %% get cluster
reload(scluster)
client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00', wait=True)
cluster
client
# %%
rho0 = 1025.022
g    = 9.80665

# %%
lev = 'L128'
run = 'exp.ocean_era51h_zstar_r2b9_22255-ERA'

gname         = 'r2b9_oce_r0004'
path_data     = '/work/mh0033/m300878/run_icon/icon-oes-zstar-r2b9/experiments/exp.ocean_era51h_zstar_r2b9_22255-ERA/outdata_3d_tstep/'
path_grid     = f'/work/mh0033/m300602/icon/grids/{gname}/'
path_ckdtree  = f'{path_grid}ckdtree/'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b9_oce_r0004/ckdtree/rectgrids/r2b9_oce_r0004_res0.10_180W-180E_90S-90N.nc'
fpath_tgrid   = '/work/mh0033/m300602/icon/grids/r2b9_oce_r0004/r2b9_oce_r0004_tgrid.nc'
fpath_tgrid_2 = '/work/mh0033/m300602/icon/grids/r2b9_oce_r0004/r2b9_oce_r0004_L128_zstar_fx.nc'
# fpath_ckdtree = f'{path_grid}ckdtree/rectgrids/{gname}_res0.30_180W-180E_90S-90N.npz'
fpath_fx      = f'{path_grid}{gname}_{lev}_fx.nc'
# %%
IcD = pyic.IconData(
    fname                = run+'_????????????????.nc',
    path_data            = path_data,
    path_grid            = path_grid,
    gname                = gname,
    lev                  = lev,
    load_triangular_grid = False,
    do_triangulation     = False,
    omit_last_file       = False,
    calc_coeff           = False,
    calc_coeff_mappings  = False,
              )
IcD.grad_coeff = (1./IcD.dual_edge_length)


#%%
ds_tgrid   = xr.open_dataset(fpath_tgrid)
ds_tgrid_2 = xr.open_dataset(fpath_tgrid_2)
depthc     = ds_tgrid_2.depth.data
depthi     = ds_tgrid_2.depth_2.data
nz         = len(depthc)
dzt        = np.zeros((nz+1))
dzt[0]     = depthc[0]-depthi[0]
dzt[-1]    = depthi[-1]-depthc[-1]
dzt[1:-1]  = depthc[1:]-depthc[:-1]

adjacent_cell_of_edge = ds_tgrid.adjacent_cell_of_edge.data.transpose()-1
dual_edge_length     = ds_tgrid.dual_edge_length.data

grad_coeff = (1./dual_edge_length)
# %% load data
to                 = xr.open_dataset(f'{path_data}{run}_oce_3d_to_PT1H_20201101T021500Z.nc').to.squeeze()
so                 = xr.open_dataset(f'{path_data}{run}_oce_3d_so_PT1H_20201101T021500Z.nc').so.squeeze()
u                  = xr.open_dataset(f'{path_data}{run}_oce_3d_u_PT1H_20201101T021500Z.nc').u.squeeze()
v                  = xr.open_dataset(f'{path_data}{run}_oce_3d_v_PT1H_20201101T021500Z.nc').v.squeeze()
rho                = xr.open_dataset(f'{path_data}{run}_oce_3d_rho_PT1H_20201101T021500Z.nc').rho.squeeze()
rhopot             = xr.open_dataset(f'{path_data}{run}_oce_3d_rhopot_PT1H_20201101T021500Z.nc').rhopot.squeeze()
press_hyd          = xr.open_dataset(f'{path_data}{run}_oce_3d_press_hyd_PT1H_20201101T021500Z.nc').press_hyd.squeeze()
press_grad         = xr.open_dataset(f'{path_data}{run}_oce_3d_press_grad_PT1H_20201101T021500Z.nc').press_grad.squeeze()
ds_2d              = xr.open_dataset(f'{path_data}{run}_oce_2d_PT1H_20201101T021500Z.nc')
zos                = ds_2d.zos.squeeze()
sea_level_pressure = ds_2d.sea_level_pressure.squeeze()

ncells = 1274300 
# %%
so  = so.isel(ncells=ncells)
to  = to.isel(ncells=ncells)
rho = rho.isel(ncells=ncells).data

#%%
# potential density (should not be used)
rho_pot = sw.dens(so, to, 0.)
# in-situ density
rho_ins = sw.dens(so, to, depthc)

# %% compare profiles
plt.figure(figsize=(5,15))
plt.plot(rho[:,ncells], depthc, label='in-situ ICON')
plt.plot(rhopot[:,ncells], depthc, label='potential ICON')
plt.plot(rho_pot, depthc, ls=':', marker='o', label='potential calculated')
plt.plot(rho_ins, depthc, ls=':', marker='x', label='in-situ calculated')
plt.legend()
plt.ylim(1500,0)
plt.xlim(1025,1035)

# %%
if False:
    pres       = np.zeros(rho.shape)
    pres[0,:]  = IcD.grav/IcD.rho0 * rho[0,:]*IcD.dzt[0,:]
    pres[1:,:] = pres[0:1,:] + IcD.grav/IcD.rho0 * np.cumsum(0.5*(rho[1:,:]+rho[:-1,:])*IcD.dzt[1:-1,:], axis=0)
elif False: # 1profile
    pres       = np.zeros(rho.shape)
    pres[0]    = g/rho0 * rho[0]*dzt[0]
    pres[1:]   = pres[0:1] + g/rho0 * np.cumsum(0.5*(rho[1:]+rho[:-1])*dzt[1:-1], axis=0)
else:
    rho        = rho.data
    pres       = np.zeros(rho.shape)
    pres[0,:]  = g/rho0 * rho[0,:]*dzt[0,np.newaxis]
    pres[1:,:] = pres[0:1,:] + g/rho0 * np.cumsum(0.5*(rho[1:,:]+rho[:-1,:])*dzt[1:-1,np.newaxis], axis=0)

# %%
plt.figure(figsize=(5,15))
plt.plot(press_hyd[:,ncells], depthc, label='hydrostatic ICON')
plt.plot(pres[:,ncells], depthc, ls=':', marker='o', label='hydrostatic calculated')
plt.legend()
plt.ylim(6000,0)

# %%
gradh_p = (pres[:,adjacent_cell_of_edge[:,1]]-pres[:,adjacent_cell_of_edge[:,0]])*grad_coeff

# %% ?????????????
ncells = 14602100 # sensitve choice: extremly important how far away land is

plt.figure(figsize=(3,10))
plt.plot(press_grad[:,ncells], depthc, label='pressure gradient ICON')
plt.plot(gradh_p[:,ncells], depthc, ls=':', marker='o', label='pressure gradient calculated')

plt.legend()
plt.ylim(6000,0)


# %%
def edges2cell(ve, edge2cell_coeff_cc, edge_of_cell, prism_thick_e, prism_thick_c, fixed_vol_norm):
  p_vn_c = (   edge2cell_coeff_cc[np.newaxis,:,:,:]
             * ve[:,edge_of_cell,np.newaxis]
             * prism_thick_e[:,edge_of_cell,np.newaxis]
           ).sum(axis=2)
  # dim(fixed_vol_norm) = (nCells)
  #dist_vector = IcD.edge_cart_vec[IcD.edge_of_cell,:] - IcD.cell_cart_vec[:,np.newaxis,:]
  #fixed_vol_norm = (0.5 * np.sqrt(scalar_product(dist_vector,dist_vector,dim=2)) * IcD.edge_length[IcD.edge_of_cell]).sum(axis=1)
  #p_vn_c *= 1./(fixed_vol_norm[np.newaxis,:,np.newaxis]*IcD.prism_thick_c[:,:,np.newaxis])
  p_vn_c *= 1./(fixed_vol_norm[np.newaxis,:,np.newaxis]*prism_thick_c[:,:,np.newaxis])
  return p_vn_c

edge_of_cell          = ds_tgrid.edge_of_cell.data.transpose()-1
edge_length           = ds_tgrid.edge_length.data
clon                  = ds_tgrid.clon.data
orientation_of_normal = ds_tgrid.orientation_of_normal.data.transpose()
prism_thick_e         = ds_tgrid_2.prism_thick_e.data
prism_thick_c         = ds_tgrid_2.prism_thick_c.data

dtype = 'float32'
f = Dataset(fpath_tgrid, 'r')
elon = f.variables['elon'][:] * 180./np.pi #edge midpoint longitude
elat = f.variables['elat'][:] * 180./np.pi #edge midpoint latitude
cell_cart_vec = np.ma.zeros((clon.size,3), dtype=dtype)
cell_cart_vec[:,0] = f.variables['cell_circumcenter_cartesian_x'][:] # cartesian position of the prime cell circumcenter
cell_cart_vec[:,1] = f.variables['cell_circumcenter_cartesian_y'][:]
cell_cart_vec[:,2] = f.variables['cell_circumcenter_cartesian_z'][:]
edge_cart_vec = np.ma.zeros((elon.size,3), dtype=dtype)
edge_cart_vec[:,0] = f.variables['edge_middle_cartesian_x'][:] # prime edge center cartesian coordinate x on unit
edge_cart_vec[:,1] = f.variables['edge_middle_cartesian_y'][:]
edge_cart_vec[:,2] = f.variables['edge_middle_cartesian_z'][:]
f.close()

grid_sphere_radius = 6.371229e6
dist_vector        = edge_cart_vec[edge_of_cell,:] - cell_cart_vec[:,np.newaxis,:]
norm               = np.sqrt(pyic.scalar_product(dist_vector,dist_vector,dim=2))
prime_edge_length  = edge_length/grid_sphere_radius
fixed_vol_norm     = (0.5 * norm * (prime_edge_length[edge_of_cell]))
fixed_vol_norm     = fixed_vol_norm.sum(axis=1)
edge2cell_coeff_cc = (dist_vector * (edge_length[edge_of_cell,np.newaxis] / grid_sphere_radius) * orientation_of_normal[:,:,np.newaxis] )

# %%???????????
if False:
    p_surf = g * zos #* rho0
    p_surf.plot()
    # %%
    sea_level_pressure.plot()

    #    %%
    psurf = np.tile(g * zos.data[np.newaxis,:], [nz,1])
    gradh_ps = (psurf[:,adjacent_cell_of_edge[:,1]]-psurf[:,adjacent_cell_of_edge[:,0]])*grad_coeff
    # add surface contibution
    gradh_p += gradh_ps 
# %%  ????
%%time
p_gradh_p  = edges2cell(gradh_p, edge2cell_coeff_cc, edge_of_cell, prism_thick_e, prism_thick_c, fixed_vol_norm)

# %% ????
%%time
dpdx, dpdy = pyic.calc_2dlocal_from_3d(ds_tgrid, p_gradh_p)

# %%
%%time
p_gradh_p = pyic.edges2cell(IcD, gradh_p)
#%%
%%time
dpdx, dpdy = pyic.calc_2dlocal_from_3d(IcD, p_gradh_p)
#%%
plt.figure(figsize=(3,10))
plt.plot(press_grad[:,ncells], depthc, label='pressure gradient ICON')



# %%
clon = ds_tgrid.clon.data
clat = ds_tgrid.clat.data
f = eva.calc_coriolis_parameter(clat)

ug = -1./(f+1e-33) * dpdy
vg =  1./(f+1e-33) * dpdx

# ug[:,np.abs(clat)<5.] = np.ma.masked
# vg[:,np.abs(clat)<5.] = np.ma.masked

ic = np.argmin((clon+30)**2+(clat-26)**2)

# %%
ic = ncells
hca, hcb = pyic.arrange_axes(2,1, plot_cb=False, asp=1.3, fig_size_fac=5.,
                               sharex=False, sharey=True, ylabel="depth [m]",
                            )
ii=-1
pset = dict(marker='.')

ii+=1; ax=hca[ii]; cax=hcb[ii]
ax.plot(u[:,ic], depthc, label='$u$', **pset)
ax.plot(ug[:,ic], depthc, label='$u_g$', **pset)
ax.legend()
ax.set_title('zonal vel. component [m/s]')

ii+=1; ax=hca[ii]; cax=hcb[ii]
ax.plot(v[:,ic], depthc, label='$v$', **pset)
ax.plot(vg[:,ic], depthc, label='$v_g$', **pset)
ax.legend()
ax.set_title('meridional vel. component [m/s]')

for ax in hca:
    ax.set_ylim(6000,0)
# %%