# %%
import sys
sys.path.insert(0, "../../")

import glob, os
import dask as da
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload

import pyicon as pyic
sys.path.insert(0, '../../')
sys.path.insert(0, '../../../')
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.init_slurm_cluster as scluster 
import cartopy
import gsw
from netCDF4 import Dataset

# %%
savefig       = False
lon_reg       = np.array([-70, -54])
lat_reg       = np.array([23.5, 36])

fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
path_data     = '/work/mh0033/m300878/smt/P/gradient/'
# %% get cluster
reload(scluster)
client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00', wait=True)
cluster
client

# %%
rho0 = 1025     # Reference density in kg/m³
g    = 9.81     # Gravitational acceleration in m/s²

ds = eva.load_smt_b_fast()
dbdx = ds.dbdx

dbdx = dbdx.isel(time=0).compute()
# %%

dpdx = np.zeros(dbdx.shape)

ncfile_path_dpdx = f'{path_data}dpdx3.nc'

with Dataset(ncfile_path_dpdx, 'w', format='NETCDF4') as ncfile:
    # Define dimensions
    ncfile.createDimension('depthi', len(depthi))
    ncfile.createDimension('ncells', dbdx.shape[1])

    # Define variables
    depthi_var = ncfile.createVariable('depthi', 'f4', ('depthi',))
    ncells_var = ncfile.createVariable('ncells', 'i4', ('ncells',))
    dpdx_var   = ncfile.createVariable('dpdx', 'f4', ('depthi', 'ncells'))

    # Write depthc and ncells data
    depthi_var[:] = depthi
    ncells_var[:] = np.arange(dbdx.shape[1])

    # Compute and write pres layer-wise
    dzt_ = dzt[:, np.newaxis]

    cumulative_sum = np.zeros(dbdx.shape[1])  # Initialize cumulative sum for each cell

    for i in range(0,depthi.size):
        print(i)
        if i == 0: # zero level has nans
            dpdx[0, :] = - rho0 * dbdx[0, :] * dzt_[0, :]
        else:
            cumulative_sum += 0.5 * (dbdx[i, :] + dbdx[i-1, :]) * dzt_[i, :]
            dpdx[i, :] = dpdx[i-1, :] - rho0 * cumulative_sum
        
        dpdx_var[i, :] = dpdx[i, :]

print(f"Data saved to {ncfile_path_dpdx}")
# %%

client.close()
cluster.close()
