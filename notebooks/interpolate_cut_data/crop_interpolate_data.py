# %%
# Evaluate slope of over MLD depth and front averaged profiles
# manually selected MLD, visualization of vertical structure function
# %%
import sys

#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import dask as da

import pandas as pd
import netCDF4 as nc
import xarray as xr    
#from dask.diagnostics import ProgressBar
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
from importlib import reload
from matplotlib.ticker import FormatStrFormatter
from scipy import stats    #Used for 2D binned statistics
from scipy.ndimage.interpolation import rotate

import slope_param_vs_diagnostic_inclined_funcs as hal

# %%
grain        = False
#Reload modules:
reload(eva)
reload(tools)
reload(hal)
# %%
lon_reg_R0    = [-80.5, -55]
lat_reg_R0    = [23, 38]
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'

path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
lon_reg       = lon_reg_R0
lat_reg       = lat_reg_R0
time_averaged = '_2d_mean'
# time_averaged = ''


# %%
fpath_ckdtree_r01 = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.10_180W-180E_90S-90N.nc'
grid01            = xr.open_dataset(fpath_ckdtree_r01)
grid01_sel        = grid01.sel(lon=slice(lon_reg_R0[0], lon_reg_R0[1]), lat=slice(lat_reg_R0[0],lat_reg_R0[1]))


# %%
tt = 0
if time_averaged != '': 
    chunks_c = {'depthc': 1, 'time': 1}
    chunks_i = {'depthi': 1, 'time': 1}
else: 
    chunks_c = {'depthc': 1}
    chunks_i = {'depthi': 1}


#calculate MLD
path_data     = f'{path_root_dat}t{time_averaged}.nc'
t_mean       = xr.open_dataset(path_data, chunks=chunks_c)
# t_mean        = xr.open_dataset(path_data, chunks=dict(depthc=1, time=1))
if time_averaged != '': t_mean        = t_mean.isel(time=tt)
path_data     = f'{path_root_dat}s{time_averaged}.nc'
s_mean        = xr.open_dataset(path_data, chunks=chunks_c)
if time_averaged != '': s_mean        = s_mean.isel(time=tt)
data_t_mean           = pyic.interp_to_rectgrid_xr(t_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_s_mean           = pyic.interp_to_rectgrid_xr(s_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_rho_mean         = gsw.rho(data_s_mean.S001_sp, data_t_mean.T001_sp, depthc[2])
data_rho_mean_depthi_ = data_rho_mean.interp(depthc=depthi)
data_rho_mean_depthi_ = data_rho_mean_depthi_.rename(depthc='depthi')

# %%
if grain == True: data_rho_mean_depthi = hal.coarse_graining(data_rho_mean_depthi_, grid01_sel) 
else: data_rho_mean_depthi = data_rho_mean_depthi_


mld, mask_mld, mldx = eva.calc_mld_xr(data_rho_mean_depthi, depthi)
# mld, mask, mldx = eva.calc_mld_montegut_xr3(data_rho_mean_depthi, depthi)
# mask      = mask.rename(depthc='depthi')

data_t_mean_depthi  = data_t_mean.T001_sp.interp(depthc=depthi)
data_t_mean_depthi_ = data_t_mean_depthi.rename(depthc='depthi')
if grain == True: data_t_mean_depthi = hal.coarse_graining(data_t_mean_depthi_, grid01_sel)
else: data_t_mean_depthi = data_t_mean_depthi_
a         = data_t_mean_depthi.isel(depthi=1).where(~(data_t_mean_depthi.isel(depthi=1)==0), np.nan)
mask_land = ~np.isnan(a)
mask_land = mask_land.where(mask_land==True, np.nan)
mldx      = mldx * mask_land
mldx      = mldx.rename('mld')

#calculate Ri
path_data       = f'{path_root_dat}bx{time_averaged}.nc'
dbdx_mean       = xr.open_dataset(path_data, chunks=chunks_i)
if time_averaged != '': dbdx_mean        = dbdx_mean.isel(time=tt)

path_data       = f'{path_root_dat}by{time_averaged}.nc'
dbdy_mean       = xr.open_dataset(path_data, chunks=chunks_i)
if time_averaged != '': dbdy_mean        = dbdy_mean.isel(time=tt)
path_data       = f'{path_root_dat}n2{time_averaged}.nc'
n_mean          = xr.open_dataset(path_data, chunks=chunks_i)
if time_averaged != '': n_mean        = n_mean.isel(time=tt)
data_dbdx_mean_ = pyic.interp_to_rectgrid_xr(dbdx_mean.dbdx, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data_dbdy_mean_ = pyic.interp_to_rectgrid_xr(dbdy_mean.dbdy, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
data_n2_mean_   = pyic.interp_to_rectgrid_xr(n_mean.N2, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
#data_M2_mean_   = np.sqrt(np.power(data_dbdx_mean_,2) + np.power(data_dbdy_mean_,2))
data_M2_mean_   = np.abs(data_dbdx_mean_) + np.abs(data_dbdy_mean_)


#ri_mean = eva.calc_richardsonnumber(data_n2_mean_.lat, data_n2_mean_, data_dbdy_mean_)

path_data          = f'{path_root_dat}wb{time_averaged}_prime.nc'
wb_prime_mean      = xr.open_dataset(path_data, chunks=chunks_i)
wb_prime_mean      = wb_prime_mean.rename(__xarray_dataarray_variable__='wb_prime_mean')
if time_averaged != '': wb_prime_mean        = wb_prime_mean.isel(time=tt)
data_wb_prime_mean_ = pyic.interp_to_rectgrid_xr(wb_prime_mean.wb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

# %%
if False: # 1WEEK
    path_data          = f'{path_root_dat}vb{time_averaged}_prime.nc'
    vb_prime_mean      = xr.open_dataset(path_data, chunks=chunks_c)
    vb_prime_mean      = vb_prime_mean.rename(__xarray_dataarray_variable__='vb_prime_mean')
    vb_prime_mean      = vb_prime_mean.drop(['clon', 'clat'])
else:
    time_window   = '2D'
    var           = 'vb_prime'
    path_data     = f'{path_root_dat}{var}_{time_window}_mean.nc'
    vb_prime_mean = xr.open_dataset(path_data, chunks=chunks_c)

if time_averaged != '': vb_prime_mean        = vb_prime_mean.isel(time=tt)
data_vb_prime_mean = pyic.interp_to_rectgrid_xr(vb_prime_mean.vb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

data_vb_prime_mean_depthi  = data_vb_prime_mean.interp(depthc=depthi)
data_vb_prime_mean_depthi_ = data_vb_prime_mean_depthi.rename(depthc='depthi')
# %%
if False: # 1WEEK
    path_data          = f'{path_root_dat}ub{time_averaged}_prime.nc'
    ub_prime_mean      = xr.open_dataset(path_data, chunks=chunks_c)
    ub_prime_mean      = ub_prime_mean.rename(__xarray_dataarray_variable__='ub_prime_mean')
    ub_prime_mean      = ub_prime_mean.drop(['clon', 'clat'])
else:
    time_window   = '2D'
    var           = 'ub_prime'
    path_data     = f'{path_root_dat}{var}_{time_window}_mean.nc'
    ub_prime_mean = xr.open_dataset(path_data, chunks=chunks_c)

if time_averaged != '': ub_prime_mean        = ub_prime_mean.isel(time=tt)
data_ub_prime_mean = pyic.interp_to_rectgrid_xr(ub_prime_mean.ub_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg) #week

data_ub_prime_mean_depthi  = data_ub_prime_mean.interp(depthc=depthi)
data_ub_prime_mean_depthi_ = data_ub_prime_mean_depthi.rename(depthc='depthi')

# %% Coarse graining >> interpolate to coarser grid: horizontal smoothing
if grain == True: 
    data_wb_prime_mean        = hal.coarse_graining(data_wb_prime_mean_, grid01_sel)
    data_ub_prime_mean_depthi = hal.coarse_graining(data_ub_prime_mean_depthi_, grid01_sel)
    data_vb_prime_mean_depthi = hal.coarse_graining(data_vb_prime_mean_depthi_, grid01_sel)
    data_n2_mean              = hal.coarse_graining(data_n2_mean_, grid01_sel)
    data_dbdx_mean            = hal.coarse_graining(data_dbdx_mean_, grid01_sel)
    data_dbdy_mean            = hal.coarse_graining(data_dbdy_mean_, grid01_sel)
    data_M2_mean              = hal.coarse_graining(data_M2_mean_, grid01_sel)
    data_rho_mean_depthi      = hal.coarse_graining(data_rho_mean_depthi_, grid01_sel)
else:
    data_wb_prime_mean        = data_wb_prime_mean_
    data_ub_prime_mean_depthi = data_ub_prime_mean_depthi_
    data_vb_prime_mean_depthi = data_vb_prime_mean_depthi_
    data_n2_mean              = data_n2_mean_
    data_dbdx_mean            = data_dbdx_mean_
    data_dbdy_mean            = data_dbdy_mean_
    data_M2_mean              = data_M2_mean_
    data_rho_mean_depthi      = data_rho_mean_depthi_


# %%
path_to_interp_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/2d_mean/'

data_wb_prime_mean       .to_netcdf(f'{path_to_interp_data}/data_wb_prime_mean.nc')
data_ub_prime_mean_depthi.to_netcdf(f'{path_to_interp_data}/data_ub_prime_mean_depthi.nc')
data_vb_prime_mean_depthi.to_netcdf(f'{path_to_interp_data}/data_vb_prime_mean_depthi.nc')
data_n2_mean             .to_netcdf(f'{path_to_interp_data}/data_n2_mean.nc')
data_dbdx_mean           .to_netcdf(f'{path_to_interp_data}/data_dbdx_mean.nc')
data_dbdy_mean           .to_netcdf(f'{path_to_interp_data}/data_dbdy_mean.nc')
data_M2_mean             .to_netcdf(f'{path_to_interp_data}/data_M2_mean.nc')
data_rho_mean_depthi     .to_netcdf(f'{path_to_interp_data}/data_rho_mean_depthi.nc')
mask_land                .to_netcdf(f'{path_to_interp_data}/mask_land.nc')
mask_mld                 .to_netcdf(f'{path_to_interp_data}/mask_mld.nc')
mldx                     .to_netcdf(f'{path_to_interp_data}/mldx.nc')
# %%
