# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd

sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
import funcs2 as fu
from importlib import reload
import pyicon as pyic
import gsw
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
sys.path.insert(0, "../regional_evaluation_BI/")
import domain_funcs as domfunc
from scipy.spatial import cKDTree
import xarray as xr
import pandas as pd
# %%
import smt_modules.init_slurm_cluster as scluster 
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, scale=2)
cluster
client
#%%
fig_path      = '/home/m/m300878/submesoscaletelescope/results/smt_natl/non_frontal/'
file_path     = '/work/mh0033//work/mh0033/m300878/smt/eddy_coords_natl/'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
savefig       = False
time_ave      = '1W'
lon_reg       = np.array([-70, -54])
lat_reg       = np.array([23.5, 38])

# %% ################################### search and select eddies #########################################
vort        = eva.load_smt_vorticity()
t0          = '2010-03-15T21'
t1          = '2010-03-22T21'
vort        = vort.sel(time=slice(t0, t1))
vort        = vort.vort_f_cells_50m.mean('time').drop(['clon', 'clat'])
vort        = vort.compute()
vort_interp = pyic.interp_to_rectgrid_xr(vort, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
reload(fu)
fu.plot_sel_eddies_vort(vort_interp, lat_reg, lon_reg, fig_path, savefig)

# %%
m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx = domfunc.load_clean_dataset(time_ave)
M2 = np.sqrt(m2x_mask**2 + m2y_mask**2)
f  = eva.calc_coriolis_parameter(M2.lat)
Ri = n2_mask * f**2 / M2**2

# %% make subfigure with M2, N2, Ri and vorticity
d                 = 30
point             = [-57.53, 37.2]
a                 = 0.5
b                 = 0.38
p                 = 220
angle             = 0

d                 = 30
point             = [-62.31, 36.1]
a                 = 0.3
b                 = 0.26
p                 = 220
angle             = 0

d                 = 30
point             = [-64.4, 30.35]
a                 = 0.5
b                 = 0.4
p                 = 220
angle             = 0


d                 = 20
point             = [-64.4, 29.15]
a                 = 0.5
b                 = 0.4
p                 = 220
angle             = 0

d                 = 30
point             = [-61.76, 32.28]
a                 = 0.45
b                 = 0.35
p                 = 220
angle             = 0

d                 = 30
point             = [-62., 33.56]
a                 = 0.55
b                 = 0.45
p                 = 220
angle             = 0

d                 = 20
point             = [-64.2, 36.87]
a                 = 0.35
b                 = 0.6
p                 = 220
angle             = 32


reload(fu)
ds_eddy = fu.create_xeddy(point, a, b, p, angle)

#%%

xlim = lon_reg
ylim = lat_reg
xlim = np.array([-65, -63])
ylim = np.array([35.75, 38])
xlim = ds_eddy.xlim
ylim = ds_eddy.ylim
reload(fu)
fu.search_select_eddy(M2, n2_mask, Ri, vort_interp, d, ds_eddy, xlim, ylim, savefig, fig_path)

#%%
#save eddy coordinates
ds_eddy.to_netcdf(f'eddy_coords/eddy_alpha_{point[0]:2f}_{point[1]:2f}.nc')

#%%
path    = 'eddy_coords/eddy_alpha_*'
path    = glob.glob(path)
ds_eddy = xr.open_mfdataset(path, concat_dim='eddy')

# %%
path_pattern = 'eddy_coords/eddy_alpha_*'
file_paths = glob.glob(path_pattern)

# Load each file as an independent dataset
datasets = [xr.open_dataset(file_path) for file_path in file_paths]

# %%
eddy_num   = 7
ds_eddy = datasets[eddy_num]

xlim = ds_eddy.xlim
ylim = ds_eddy.ylim
reload(fu)
fu.search_select_eddy(M2, n2_mask, Ri, vort_interp, d, ds_eddy, xlim, ylim, savefig, fig_path, eddy_num)

# %%
reload(fu)
xlim = np.array([-66, -56])
ylim = np.array([27.5, 38])
fu.search_select_eddy_overview(M2, n2_mask, Ri, vort_interp, d, datasets, xlim, ylim, savefig, fig_path, eddy_num)

# %%
ds=xr.open_dataset('/work/mh0256/m301099/icon-mpim/experiments/exp.ocean_omip_r2b6_v2/exp.ocean_omip_r2b6_v2_P1D_kin_20200101T000000Z.nc')


# %%
pyic.interp_to_rectgrid_xr(ds.isel(time=0, depth=1), fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)


# %%

