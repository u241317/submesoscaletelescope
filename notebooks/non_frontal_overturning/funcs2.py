
# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd

from importlib import reload
import pyicon as pyic
import gsw
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()

from scipy.spatial import cKDTree
import xarray as xr
import pandas as pd
import smt_modules.all_funcs as eva

# %%
def spherical_to_cartesian(lon, lat):
  earth_radius = 6371e3
  x = earth_radius * np.cos(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  y = earth_radius * np.sin(lon*np.pi/180.) * np.cos(lat*np.pi/180.)
  z = earth_radius * np.sin(lat*np.pi/180.)
  return x, y, z

def interp_to_reg_grid(lon_i, lat_i, lon_o, lat_o):
    xi, yi, zi = spherical_to_cartesian(lon_i, lat_i)
    xo, yo, zo = spherical_to_cartesian(lon_o, lat_o)

    lzip_i = np.concatenate((xi[:,np.newaxis],yi[:,np.newaxis],zi[:,np.newaxis]), axis=1)
    lzip_o = np.concatenate((xo[:,np.newaxis],yo[:,np.newaxis],zo[:,np.newaxis]), axis=1) 
    tree   = cKDTree(lzip_i)
    dckdtree, ickdtree = tree.query(lzip_o , k=1, workers=-1)

    Dind_dist = dict()
    Dind_dist['dckdtree_c'] = dckdtree
    Dind_dist['ickdtree_c'] = ickdtree

    return Dind_dist

# %%
def compute_circle_indces(data, center_x, center_y, radius):
    theta     = np.linspace(0, 2*np.pi, 500)
    x         = center_x + radius * np.cos(theta)
    y         = center_y + radius * np.sin(theta)
    lon_o     = x 
    lat_o     = y 

    plt.figure()
    plt.pcolormesh(data.x, data.y, data, cmap='plasma')
    plt.plot(lon_o, lat_o, 'g', lw=2)
    # plt.savefig(fig_path+'circle.png', dpi=180)

    #make xarray
    lon_o = xr.DataArray(lon_o, dims=['lon'])
    lat_o = xr.DataArray(lat_o, dims=['lat'])

    lon_i = data.x #+ data.loncen.data
    lat_i = data.y #+ data.latcen.data
    Lon, Lat = np.meshgrid(lon_i, lat_i)
    lon_i = Lon.flatten()
    lat_i = Lat.flatten()

    Dind_dist = interp_to_reg_grid(lon_i, lat_i, lon_o.data, lat_o.data)
    ickdtree = Dind_dist['ickdtree_c'] 
    return ickdtree, theta

def compute_ellipse_indices(data, center_x, center_y, a, b, angle, p):
    theta     = np.linspace(0, 2*np.pi, p)
    x         = center_x + a * np.cos(theta)
    y         = center_y + b * np.sin(theta)
    lon_o     = x
    lat_o     = y
    print(lon_o, lat_o)

    plt.figure()
    plt.pcolormesh(data.lon, data.lat, data, cmap='plasma')
    plt.plot(lon_o, lat_o, 'g', lw=2)

    #make xarray
    lon_o = xr.DataArray(lon_o, dims=['lon'])
    lat_o = xr.DataArray(lat_o, dims=['lat'])

    lon_i = data.lon #+ data.loncen.data
    lat_i = data.lat #+ data.latcen.data
    Lon, Lat = np.meshgrid(lon_i, lat_i)
    lon_i = Lon.flatten()
    lat_i = Lat.flatten()

    Dind_dist = interp_to_reg_grid(lon_i, lat_i, lon_o.data, lat_o.data)
    ickdtree = Dind_dist['ickdtree_c']
    return ickdtree, theta

def create_elliptic_output_grid(ds, num_lines):
    X = []
    Y = []
    width = ds.a.data*2
    delta_s = width/num_lines
    for i in range (num_lines): # lines
        a_i = ds.a.data - width/2 + delta_s*i
        b_i = ds.b.data - width/2 + delta_s*i
        x = ds.loncen.data + a_i * np.cos(ds.theta.data)
        y = ds.latcen.data + b_i * np.sin(ds.theta.data)
        X.append(x)
        Y.append(y)

    lon_o = xr.DataArray(X, dims=['num_lines', 'num_pixels'])
    lat_o = xr.DataArray(Y, dims=['num_lines', 'num_pixels'])

    ds_orig                     = xr.Dataset()
    ds_orig.coords['longitude'] = lon_o
    ds_orig.coords['latitude']  = lat_o
    return ds_orig

def create_elliptic_output_grid_a(ds, num_lines):
    X = []
    Y = []
    scaling_factors = np.linspace(0.005,2,num_lines) # Example scaling factors

    # Generate the coordinates for each scaled ellipse
    for scale in scaling_factors:
        x_scaled = ds.loncen + (ds.a.data * scale) * np.cos(ds.theta.data) * np.cos(ds.angle.data) - (ds.b.data *    scale) * np.sin(ds.theta.data) * np.sin(ds.angle.data)
        y_scaled = ds.latcen + (ds.a.data * scale) * np.cos(ds.theta.data) * np.sin(ds.angle.data) + (ds.b.data *    scale) * np.sin(ds.theta.data) * np.cos(ds.angle.data)
        X.append(x_scaled) 
        Y.append(y_scaled)

    lon_o = xr.DataArray(X, dims=['num_lines', 'num_pixels'])
    lat_o = xr.DataArray(Y, dims=['num_lines', 'num_pixels'])

    ds_orig                     = xr.Dataset()
    ds_orig.coords['longitude'] = lon_o
    ds_orig.coords['latitude']  = lat_o
    return ds_orig



# %% ################## ###### apply ######################
def select_ellipse_from_reg_grid(data, ickdtree, theta):
    flattened_data = data.stack(theta=('lon', 'lat'))
    flattened_data = flattened_data.reset_index('theta')
    hov = flattened_data.isel(theta=ickdtree)
    hov['theta'] = ('theta', theta)
    return hov

def plot_hov(hov, fig_path, savefig=False, cmap=None, vmin=None, vmax=None):
    fig = plt.figure(figsize=(20, 5))
    gs = fig.add_gridspec(2, 2, height_ratios=[1, 5])

    ax0 = fig.add_subplot(gs[0, 0])
    ax0.plot(np.arange(631), hov.isel(theta=25), color='k', lw=1)
    ax0.set_xticks([])  # Hide x-ticks for the first subplot
    ax0.set_title(f'Hovmöller Diagram of {hov.name} at {hov.depthi.data:.0f}m', fontsize=14)
    ax0.set_xlim(0, 631)

    ax1 = fig.add_subplot(gs[1, 0])
    if vmin==None and vmax==None:
        cb = ax1.pcolormesh(np.arange(631), hov.theta / np.pi, hov.transpose(), cmap=cmap)
    else:
        cb = ax1.pcolormesh(np.arange(631), hov.theta / np.pi, hov.transpose(), cmap=cmap, vmin=vmin, vmax=vmax)
        #with logscale
        # cb = ax1.pcolormesh(np.arange(631), hov.theta / np.pi, hov.transpose(), cmap=cmap, norm=matplotlib.colors.LogNorm())
    fig.colorbar(cb, ax=ax1, orientation='horizontal', pad=0.13, aspect=40, shrink=0.8)
    ax1.set_xlabel('time [days]', fontsize=12)
    ax1.set_ylabel(r'$\theta / \pi$', fontsize=12)
    ax1.axhline(0.5, color='white', linestyle='-', linewidth=1)
    ax1.axvline(315, color='white', linestyle='-', linewidth=1)
    ax1.set_xticks(np.arange(0, 631, 12))
    ax1.set_xticklabels([f'{int(x*2/24)}' for x in np.arange(0, 631, 12)], fontsize=6)
    ax1.tick_params(axis='x', which='both', bottom=True, top=True, labelbottom=True, labeltop=True)

    ax2 = fig.add_axes([0.522, 0.225, 0.037, 0.509]) 
    ax2.plot(hov.isel(time=315), hov.theta/np.pi, color='k', lw=1)
    ax2.set_ylim(0, 2)
    ax2.set_yticks([])
    ax2.tick_params(axis='x', labelsize=6)

    plt.tight_layout()
    if savefig==True: plt.savefig(fig_path+f'hov_{hov.name}_{hov.depthi.data:.0f}.png', dpi=180, bbox_inches='tight')

def plot_hov_0(hov, fig_path, savefig=False, cmap=None, vmin=None, vmax=None):
    fig = plt.figure(figsize=(20, 5))
    gs = fig.add_gridspec(2, 2, height_ratios=[1, 5])

    ax0 = fig.add_subplot(gs[0, 0])
    ax0.plot(np.arange(631), hov.isel(theta=25), color='k', lw=1)
    ax0.set_xticks([])  # Hide x-ticks for the first subplot
    ax0.set_title(f'Hovmöller Diagram of {hov.name} at {hov.depth.data:.0f}m', fontsize=14)
    ax0.set_xlim(0, 631)

    ax1 = fig.add_subplot(gs[1, 0])
    if vmin==None and vmax==None:
        cb = ax1.pcolormesh(np.arange(631), hov.theta / np.pi, hov.transpose(), cmap=cmap)
    else:
        cb = ax1.pcolormesh(np.arange(631), hov.theta / np.pi, hov.transpose(), cmap=cmap, vmin=-0.005, vmax=0.005)
    fig.colorbar(cb, ax=ax1, orientation='horizontal', pad=0.13, aspect=40, shrink=0.8)
    ax1.set_xlabel('time [days]', fontsize=12)
    ax1.set_ylabel(r'$\theta / \pi$', fontsize=12)
    ax1.axhline(0.5, color='white', linestyle='-', linewidth=1)
    ax1.axvline(315, color='white', linestyle='-', linewidth=1)
    ax1.set_xticks(np.arange(0, 631, 12))
    ax1.set_xticklabels([f'{int(x*2/24)}' for x in np.arange(0, 631, 12)], fontsize=6)

    ax2 = fig.add_axes([0.522, 0.225, 0.037, 0.509]) 
    ax2.plot(hov.isel(time=315), hov.theta/np.pi, color='k', lw=1)
    ax2.set_ylim(0, 2)
    ax2.set_yticks([])
    ax2.tick_params(axis='x', labelsize=6)

    plt.tight_layout()

    if savefig==True: plt.savefig(fig_path+f'hov_{hov.name}_{hov.depth.data:.0f}.png', dpi=180, bbox_inches='tight')


def plot_sel_eddies_vort(data_coarse_m2, lat_reg, lon_reg, fig_path, savefig):
    asp = (lon_reg[1]-lon_reg[0])/(lat_reg[1]-lat_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1,  asp=asp, plot_cb=False, fig_size_fac=5, 
                             projection=ccrs_proj)
    ii=-1

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0.5
    cmap = 'RdBu_r'
    data_coarse = data_coarse_m2

    ax.set_title(rf'$\overline{{\zeta}}/f$ at {50}m')

    # ax.set_ylabel(rf'$|\Delta_h \overline{{b}} | ~[ 1/s^2]$')

    hm1 = pyic.shade(data_coarse.lon, data_coarse.lat, data_coarse, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)


    # points = eva.get_eddies()
    # i =0
    # for ii in range(0, int(len(points)/2)):
    #     if ii == 0: continue
    #     i = ii*2 #+1
    #     t = ax.text(points[i,0]+0.15, points[i,1], f'E{ii}', color='purple', fontsize=7, fontweight='bold')
    #     # t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    #     # above textbox but with tighter box
        
    
    pyic.plot_settings(ax, xlim=np.array([-64, -55]), ylim=np.array([29, 36])) # type: ignore
    
    ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore


    fname = 'map_eddy_vort'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=150, format='png', bbox_inches='tight')


def interp_to_reg_grid_band(lon_i, lat_i, ds):
    lon_o = ds.longitude.data.flatten()
    lat_o = ds.latitude.data.flatten()
    xi, yi, zi = spherical_to_cartesian(lon_i, lat_i)
    xo, yo, zo = spherical_to_cartesian(lon_o, lat_o)

    lzip_i = np.concatenate((xi[:,np.newaxis],yi[:,np.newaxis],zi[:,np.newaxis]), axis=1)
    lzip_o = np.concatenate((xo[:,np.newaxis],yo[:,np.newaxis],zo[:,np.newaxis]), axis=1) 
    tree   = cKDTree(lzip_i)
    dckdtree, ickdtree = tree.query(lzip_o , k=1, workers=-1)
    # dckdtree, ickdtree = tree.query(lzip_o , k=n_nearest_neighbours)

    Dind_dist = dict()
    Dind_dist['dckdtree_c'] = dckdtree
    Dind_dist['ickdtree_c'] = ickdtree

    ds_grid = xr.Dataset(coords=dict(longitude=ds.longitude, latitude=ds.latitude))
    for var in Dind_dist.keys(): 
        ds_grid[var] = xr.DataArray(Dind_dist[var].reshape(ds.num_lines.size, ds.num_pixels.size), dims=['num_lines', 'num_pixels'])

    return ds_grid

def apply_grid(ds, ds_grid):
    print('Applying grid')
    arr_interp = ds.isel(ncells=ds_grid.ickdtree_c.compute().data.flatten())
    arr_interp = arr_interp.assign_coords(ncells=pd.MultiIndex.from_product([ds_grid.num_lines.data, ds_grid.num_pixels.data], names=("num_lines", "num_pixels"))).unstack()
    arr_interp = arr_interp.drop('num_lines').drop('num_pixels')
    arr_interp = arr_interp.assign_coords(latitude=ds_grid.latitude, longitude=ds_grid.longitude)
    return arr_interp


def create_anim(x,y,data, fig_path, name, start, end, interval, vmin, vmax, cmap):
    import matplotlib.animation as animation
    from matplotlib.animation import PillowWriter
    fig, ax = plt.subplots(1, 1, subplot_kw={'projection': ccrs_proj}, figsize=(10, 10))

    def update(t):
        ax.scatter(x, y, c=data.isel(time=t), cmap=cmap, vmin=vmin, vmax=vmax)
        return ax

    ani = animation.FuncAnimation(fig, update, frames=range(start, end, interval), repeat=False)
    writer = PillowWriter(fps=8)
    ani.save(fig_path + f'{name}.gif', writer=writer)

def plot_eddy_sec(ds_band_mean, savefig, fig_path, eddy_num):
    fig, axs = plt.subplots(3, 2, figsize=(12, 12))

    x = (ds_band_mean.num_lines.data*ds_band_mean.dx.data)/1000
    # M2
    cb = axs[0, 0].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.M2_sign, cmap='PuOr_r', vmin=-1e-7, vmax=1e-7)
    fig.colorbar(cb, ax=axs[0, 0])
    axs[0, 0].set_title(r'$M^2$')

    # N2
    cb = axs[1, 0].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.n2_mean, cmap='Spectral_r', vmin=0, vmax=1e-5)
    fig.colorbar(cb, ax=axs[1, 0])
    axs[1, 0].set_title(r'$N^2$')

    # Ri
    cb = axs[2, 0].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.Ri, cmap='Blues_r', vmin=0, vmax=20)
    fig.colorbar(cb, ax=axs[2, 0])
    axs[2, 0].set_title('Ri')
    axs[2, 0].set_xlabel('distance [km]')


    # wb_prime
    cb = axs[0, 1].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.wb_prime, cmap='RdBu_r', vmin=-1e-7, vmax=1e-7)
    fig.colorbar(cb, ax=axs[0, 1])
    axs[0, 1].set_title(r'$\overline{w^\prime b^\prime}$')

    # uvb_prime
    cb = axs[1, 1].pcolormesh(x, ds_band_mean.depthc, ds_band_mean.uvb_prime, cmap='RdBu_r', vmin=-2e-5, vmax=2e-5)
    fig.colorbar(cb, ax=axs[1, 1])
    axs[1, 1].set_title(r'$\overline{\hat{v}^\prime b^\prime}$')

    # psi
    cb = axs[2, 1].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.psi, cmap='PiYG_r', vmin=-5, vmax=5)
    fig.colorbar(cb, ax=axs[2, 1])
    axs[2, 1].set_title(r'$\psi$')
    axs[2, 1].set_xlabel('distance [km]')


    ylim=np.array([800,0])
    # levels_iso = np.linspace(4,20,40)
    #for rho
    levels_iso = np.linspace(1024, 1028, 60)
    for ax in axs.flatten():
        ax.set_ylabel('depth [m]')
        ax.set_ylim(ylim)
        ax.contour(x, ds_band_mean.depthc, ds_band_mean.rho_mean, levels_iso, colors='k', linewidths=0.2)
        #plot mld
        ax.plot(x, ds_band_mean.mld, 'tab:green', lw=1)

    if savefig==True: plt.savefig(fig_path+f'band_mean_{eddy_num}.png', dpi=180, bbox_inches='tight')


def calc_angle(x, y, x0, y0):
    angle = np.arctan2(y-y0, x-x0)
    # in degrees
    angle = np.rad2deg(angle)
    # shift to 0-360
    # angle = angle % 360
    return angle

def project_uv(u, v, theta):
    theta = np.deg2rad(theta)
    uvb_proj = u*np.cos(theta) + v*np.sin(theta)
    return uvb_proj

def search_select_eddy(M2, n2_mask, Ri, vort_interp, d, ds_eddy, xlim, ylim, savefig, fig_path, eddy_num):
    fig, axs = plt.subplots(2, 2, figsize=(13,10), sharex=True, sharey=True, subplot_kw={'projection': ccrs.PlateCarree()})
    M2.isel(depthi=d).plot(vmin=0, vmax=5e-8, cmap='BuPu', ax=axs[0, 0])
    axs[0, 0].set_title('M2')

    n2_mask.isel(depthi=d).plot(vmin=0, vmax=1e-5, cmap='Spectral', ax=axs[0, 1])
    axs[0, 1].set_title('N2')

    Ri.isel(depthi=d).plot(vmin=0, vmax=50, cmap='Blues_r', ax=axs[1, 0])
    axs[1, 0].set_title('Ri')

    vort_interp.plot(vmin=-1, vmax=1, cmap='RdBu_r', ax=axs[1, 1])
    axs[1, 1].set_title('Vorticity')

    for ax in axs.flatten():
        ax.plot(ds_eddy.loncen, ds_eddy.latcen, 'ro')
        ax.plot(ds_eddy.x, ds_eddy.y, 'r')
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_xticks(np.arange(xlim[0], xlim[1], (xlim[1]-xlim[0])/5))
        ax.set_yticks(np.arange(ylim[0], ylim[1], (ylim[1]-ylim[0])/8))
        # set x ticks majorlocator
        # ax.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(1))


    if savefig==True: plt.savefig(fig_path+f'search_eddy_{ds_eddy.loncen.data}_{ds_eddy.latcen.data}_n{eddy_num}.png', dpi=180, bbox_inches='tight')

def plot_eddies(ax, datasets):
    i=0
    for ds_eddy in datasets:
        i+=1
        # ax.plot(ds_eddy.loncen, ds_eddy.latcen, 'ro')
        ax.plot(ds_eddy.x, ds_eddy.y, 'r')
        #add name
        ax.text(ds_eddy.loncen+0.6, ds_eddy.latcen, f'E{i}', color='black', fontsize=13, fontweight='bold')


def search_select_eddy_overview(M2, n2_mask, Ri, vort_interp, d, datasets, xlim, ylim, savefig, fig_path, eddy_num):
    fig, axs = plt.subplots(2, 2, figsize=(13,10), sharex=True, sharey=True, subplot_kw={'projection': ccrs.PlateCarree()})
    M2.isel(depthi=d).plot(vmin=0, vmax=5e-8, cmap='BuPu', ax=axs[0, 0])
    axs[0, 0].set_title('M2')

    n2_mask.isel(depthi=d).plot(vmin=0, vmax=1e-5, cmap='Spectral', ax=axs[0, 1])
    axs[0, 1].set_title('N2')

    Ri.isel(depthi=d).plot(vmin=0, vmax=50, cmap='Blues_r', ax=axs[1, 0])
    axs[1, 0].set_title('Ri')

    vort_interp.plot(vmin=-1, vmax=1, cmap='RdBu_r', ax=axs[1, 1])
    axs[1, 1].set_title('Vorticity')

    for ax in axs.flatten():
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_xticks(np.arange(xlim[0], xlim[1], (xlim[1]-xlim[0])/5))
        ax.set_yticks(np.arange(ylim[0], ylim[1], (ylim[1]-ylim[0])/8))
        plot_eddies(ax, datasets)

    if savefig==True: plt.savefig(fig_path+f'search_eddy_overview_n{eddy_num}.png', dpi=180, bbox_inches='tight')


def create_xeddy(point, a,b, p, angle=0):
    theta   = np.linspace(0, 2*np.pi, p)
    x_plane = a * np.cos(theta)
    y_plane = b * np.sin(theta)

    # Rotate the ellipse by the specified angle
    x = point[0] + x_plane * np.cos(angle) - y_plane * np.sin(angle)
    y = point[1] + x_plane * np.sin(angle) + y_plane * np.cos(angle)

    xlim              = np.array([point[0]-a*2, point[0]+a*2])
    ylim              = np.array([point[1]-a*2, point[1]+a*2])
    ds_eddy           = xr.Dataset()
    ds_eddy['loncen'] = xr.DataArray([point[0]], dims=['lon'])
    ds_eddy['latcen'] = xr.DataArray([point[1]], dims=['lat'])
    ds_eddy['a']      = xr.DataArray([a], dims=['a'])
    ds_eddy['b']      = xr.DataArray([b], dims=['b'])
    ds_eddy['angle']  = xr.DataArray([angle], dims=['angle'])
    ds_eddy['p']      = xr.DataArray([p], dims=['p'])
    ds_eddy['xlim']   = xr.DataArray(xlim, dims=['xlim'])
    ds_eddy['ylim']   = xr.DataArray(ylim, dims=['ylim'])
    ds_eddy['theta']  = xr.DataArray(theta, dims=['num_pixels'])
    ds_eddy['x']      = xr.DataArray(x, dims=['num_pixels'])
    ds_eddy['y']      = xr.DataArray(y, dims=['num_pixels'])

    lamda = (a-b)/(a+b)
    u = (a+b) * np.pi * (1+ (3*lamda**2)/(10+np.sqrt(4-3*lamda**2)))
    u = u * eva.convert_degree_to_meter(ds_eddy.latcen.data)/1000
    ds_eddy['u'] = xr.DataArray([u[0]], dims=['u'])
    #u = (2*np.pi*a)* eva.convert_degree_to_meter(ds_eddy.latcen.data)/1000

    return ds_eddy


def plot_streamfunc_comp(ds_band_mean, savefig, fig_path, eddy_num):
    x = (ds_band_mean.num_lines.data*ds_band_mean.dx.data)/1000

    clim = 5
    fig, ax = plt.subplots(1, 3, figsize=(15, 5))
    cb = ax[0].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.psi, cmap='PiYG_r', vmin=-clim, vmax=clim)
    fig.colorbar(cb, ax=ax[0])
    ax[0].set_title(r'$\psi_{diag} [m^2/s]$')
    ax[0].set_ylabel('depth [m]')
    ax[0].set_xlabel('distance [km]')

    cb = ax[1].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.psi_fox, cmap='PiYG_r', vmin=-clim, vmax=clim)
    fig.colorbar(cb, ax=ax[1])
    ax[1].set_title(r'$\psi_{PER} [m^2/s]$')
    ax[1].set_xlabel('distance [km]')

    cb = ax[2].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.psi_stone, cmap='PiYG_r', vmin=-clim, vmax=clim)
    fig.colorbar(cb, ax=ax[2])
    ax[2].set_title(r'$\psi_{ALS} [m^2/s]$')
    ax[2].set_xlabel('distance [km]')

    levels_iso = np.linspace(1026, 1028, 60)
    for ax in ax:
        ax.contour(x, ds_band_mean.depthc, ds_band_mean.rho_mean, levels_iso, colors='k', linewidths=0.2)
        ax.plot(x, ds_band_mean.mld, 'tab:green', lw=2)
        ax.set_ylim(500,0)
    if savefig:
        plt.savefig(fig_path + f'streamfunc_comp_{eddy_num}.png')

def plot_streamfunc_comp_paper(ds_band_mean, savefig, fig_path, eddy_num):
    x = (ds_band_mean.num_lines.data*ds_band_mean.dx.data)/1000

    clim = 5
    fig, ax = plt.subplots(1, 3, figsize=(15, 5))
    cb = ax[0].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.psi, cmap='PiYG_r', vmin=-clim, vmax=clim)
    fig.colorbar(cb, ax=ax[0])
    ax[0].set_title(r'$\psi_{diag} [m^2/s]$')
    ax[0].set_ylabel('depth [m]')
    ax[0].set_xlabel('distance [km]')

    cb = ax[1].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.psi_fox, cmap='PiYG_r', vmin=-clim, vmax=clim)
    fig.colorbar(cb, ax=ax[1])
    ax[1].set_title(r'$\psi_{PER} [m^2/s]$')
    ax[1].set_xlabel('distance [km]')

    cb = ax[2].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.psi_stone, cmap='PiYG_r', vmin=-clim, vmax=clim)
    fig.colorbar(cb, ax=ax[2])
    ax[2].set_title(r'$\psi_{ALS} [m^2/s]$')
    ax[2].set_xlabel('distance [km]')

    levels_iso = np.linspace(1026, 1028, 60)
    for ax in ax:
        ax.contour(x, ds_band_mean.depthc, ds_band_mean.rho_mean, levels_iso, colors='k', linewidths=0.2)
        ax.plot(x, ds_band_mean.mld, 'tab:green', lw=2)
        ax.set_ylim(500,0)
    if savefig:
        plt.savefig(fig_path + f'streamfunc_comp_{eddy_num}.png')


def calc_param_var(ds_band, ds_eddy):
    M2        = np.sqrt(ds_band.dbdx_mean**2 + ds_band.dbdy_mean**2)
    fcoriolis = eva.calc_coriolis_parameter(ds_band.latitude)

    ds_band['M2_mean'] = M2
    ds_band['f']       = fcoriolis
    ds_band['Ri']      = ds_band.n2_mean * fcoriolis**2 / M2**2

    theta                = calc_angle(ds_band.isel(num_lines=0).longitude, ds_band.latitude.isel(num_lines=0), ds_eddy.loncen, ds_eddy.latcen)
    uvb_prime            = project_uv(ds_band.ub_prime, ds_band.vb_prime, theta)
    ds_band['uvb_prime'] = uvb_prime.squeeze()

    grad_b            = eva.calc_abs_grad_b(ds_band.dbdx_mean, ds_band.dbdy_mean, ds_band.n2_mean)
    ds_band['grad_b'] = grad_b
    ubv_prime_depthi  = ds_band.uvb_prime.interp(depthc=ds_band.depthi, method='linear').drop('depthc')

    #projection of frontal gradient
    M2_sign            = project_uv(ds_band.dbdx_mean, ds_band.dbdy_mean, theta)
    ds_band['M2_sign'] = M2_sign.squeeze()

    psi               = eva.calc_streamfunc_full(ds_band.wb_prime, ds_band.M2_sign, ubv_prime_depthi, ds_band.n2_mean, ds_band.grad_b)
    ds_band['psi']    = psi

    CT                  = gsw.CT_from_pt(ds_band.so_mean, ds_band.to_mean) #not a abig difference
    rho_mean            = gsw.rho(ds_band.so_mean, CT, ds_band.depthc[2]).compute()
    ds_band['rho_mean'] = rho_mean

    mld_int, mld_mask, mld = eva.calc_mld_xr(rho_mean, ds_band.depthc.data, 0.2)
    ds_band['mld'] = mld
    ds_band['mld_int'] = mld_int
    ds_band['mld_mask'] = mld_mask

    ds_band_mean = ds_band.mean('num_pixels')
    return ds_band, ds_band_mean

def calc_streamfunc_param(ds_band, ds_eddy):
    rho_mean_depthi = ds_band.rho_mean.interp(depthc=ds_band.depthi, method='linear').drop('depthc')
    ds_band['rho_mean_depthi'] = rho_mean_depthi
    mld_int_i, mld_mask_i, mld_i = eva.calc_mld_xr(rho_mean_depthi, ds_band.depthc.data, 0.2)
    #iterate through variables of ds_band_mean
    for var in ds_band.data_vars:
        if 'depthi' in ds_band[var].dims:
            ds_band[var] = ds_band[var]*mld_mask_i.squeeze()
        elif 'depthc' in ds_band[var].dims:
            ds_band[var] = ds_band[var]*ds_band.mld_mask.squeeze()

    cf = 0.045
    cs = 0.27
    H       = ds_band.mld
    z       = - ds_band.depthi
    wb_fox  = eva.calc_wb_fox_param(ds_eddy.latcen, H, ds_band.M2_sign, z, cf, structure = True)
    vb_fox  = eva.calc_vb_fox_param(ds_eddy.latcen, H, ds_band.M2_sign, ds_band.Ri, z, structure = True)
    psi_fox = eva.calc_streamfunc_full(wb_fox.squeeze(), ds_band.M2_sign, vb_fox.squeeze(), ds_band.n2_mean, ds_band.grad_b)
    # calc psi Stone
    wb_stone  = eva.calc_wb_stone_param(ds_eddy.latcen, H, ds_band.M2_sign, ds_band.Ri, cs, z, structure = True)
    vb_stone  = eva.calc_vb_stone_param(ds_eddy.latcen, H, ds_band.M2_sign, ds_band.Ri, cs)
    psi_stone = eva.calc_streamfunc_full(wb_stone.squeeze(), ds_band.M2_sign, vb_stone.squeeze(), ds_band.n2_mean, ds_band.grad_b)

    # add
    ds_band['wb_fox']    = wb_fox.squeeze()
    ds_band['vb_fox']    = vb_fox.squeeze()
    ds_band['psi_fox']   = psi_fox.squeeze()
    ds_band['wb_stone']  = wb_stone.squeeze()
    ds_band['vb_stone']  = vb_stone.squeeze()
    ds_band['psi_stone'] = psi_stone.squeeze()

    ds_band_mean = ds_band.mean('num_pixels')

    for var in ['psi_fox', 'psi_stone']:
        ds_band_mean[var] = ds_band_mean[var].where(ds_band_mean[var].depthi < ds_band_mean.mld, np.nan)

    return ds_band, ds_band_mean



def flux_stream(ds_band_mean, ds_band_mean_str, savefig, fig_path, eddy_num):
    fig, axs = plt.subplots(3,3, figsize=(15,15))
    x = (ds_band_mean.num_lines.data*ds_band_mean.dx.data)/1000

    # wb_prime
    cb = axs[0, 0].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.wb_prime, cmap='RdBu_r', vmin=-1e-7, vmax=1e-7)
    fig.colorbar(cb, ax=axs[0, 1])
    axs[0, 0].set_title(r'$\overline{w^\prime b^\prime}$')
    # uvb_prime
    cb = axs[1, 0].pcolormesh(x, ds_band_mean.depthc, ds_band_mean.uvb_prime, cmap='RdBu_r', vmin=-2e-5, vmax=2e-5)
    fig.colorbar(cb, ax=axs[1, 1])
    axs[1, 0].set_title(r'$\overline{\hat{v}^\prime b^\prime}$')
    # psi
    cb = axs[2, 0].pcolormesh(x, ds_band_mean.depthi, ds_band_mean.psi, cmap='PiYG_r', vmin=-5, vmax=5)
    fig.colorbar(cb, ax=axs[2, 1])
    axs[2, 0].set_title(r'$\psi$')
    axs[2, 0].set_xlabel('distance [km]')

    #fox
    cb = axs[0, 2].pcolormesh(x, ds_band_mean_str.depthi, ds_band_mean_str.wb_fox, cmap='RdBu_r', vmin=-1e-7, vmax=1e-7)
    fig.colorbar(cb, ax=axs[0, 2])
    axs[0, 2].set_title(r'$\overline{w^\prime b^\prime}_{PER}$')
    # uvb_fox
    cb = axs[1, 2].pcolormesh(x, ds_band_mean_str.depthi, ds_band_mean_str.vb_fox, cmap='RdBu_r', vmin=-2e-5, vmax=2e-5)
    fig.colorbar(cb, ax=axs[1, 2])
    axs[1, 2].set_title(r'$\overline{\hat{v}^\prime b^\prime}_{PER}$')
    # psi_fox
    cb = axs[2, 2].pcolormesh(x, ds_band_mean_str.depthi, ds_band_mean_str.psi_fox, cmap='PiYG_r', vmin=-5, vmax=5)
    fig.colorbar(cb, ax=axs[2, 2])
    axs[2, 2].set_title(r'$\psi_{PER}$')
    axs[2, 2].set_xlabel('distance [km]')
        
    #stone
    cb = axs[0, 1].pcolormesh(x, ds_band_mean_str.depthi, ds_band_mean_str.wb_stone, cmap='RdBu_r', vmin=-1e-7, vmax=1e-7)
    fig.colorbar(cb, ax=axs[0, 0])
    axs[0, 1].set_title(r'$\overline{w^\prime b^\prime}_{ALS}$')
    # uvb_stone
    cb = axs[1, 1].pcolormesh(x, ds_band_mean_str.depthi, ds_band_mean_str.vb_stone, cmap='RdBu_r', vmin=-2e-5, vmax=2e-5)
    fig.colorbar(cb, ax=axs[1, 0])
    axs[1, 1].set_title(r'$\overline{\hat{v}^\prime b^\prime}_{ALS}$')
    # psi_stone
    cb = axs[2, 1].pcolormesh(x, ds_band_mean_str.depthi, ds_band_mean_str.psi_stone, cmap='PiYG_r', vmin=-5, vmax=5)
    fig.colorbar(cb, ax=axs[2, 0])
    axs[2, 1].set_title(r'$\psi_{ALS}$')
    axs[2, 1].set_xlabel('distance [km]')
    
    ylim=np.array([800,0])
    # levels_iso = np.linspace(4,20,40)
    #for rho
    levels_iso = np.linspace(1024, 1028, 60)
    for ax in axs.flatten():
        ax.set_ylabel('depth [m]')
        ax.set_ylim(ylim)
        ax.contour(x, ds_band_mean.depthc, ds_band_mean.rho_mean, levels_iso, colors='k', linewidths=0.2)
        #plot mld
        ax.plot(x, ds_band_mean.mld, 'tab:green', lw=1)
    if savefig==True: plt.savefig(fig_path+f'flux_streamfunc_{eddy_num}.png', dpi=180, bbox_inches='tight')




def plot_slice_eddie(ds_band_mean, savefig, fig_path, eddy_num):
    """plot vertical parameters of eddie"""
    x = (ds_band_mean.num_lines.data*ds_band_mean.dx.data)/1000

    hca, hcb = pyic.arrange_axes(3, 1, plot_cb='bottom', asp=1.4, fig_size_fac=1.8, sharey=True, axlab_kw=None, dcbt=0.25) # type: ignore
    ii=-1

    color   = 'green'
    ls      = '-'
    lw      = 2
    nclev   = 20
    cticks  = 6
    clim    = np.array([-10,10])
    cbticks = np.linspace(clim[0],clim[1],cticks)
    ylim    = [400,0]


    ii+=1; ax=hca[ii]; cax=hcb[ii]
    cb = pyic.shade(x, ds_band_mean.depthi, ds_band_mean.psi, ax=ax, cax = cax, contfs=True, nclev=nclev, cbticks=cbticks, clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi_{diag} [m^2/s]$")
    ax.set_ylabel(r'$z ~ [m]$', rotation=90)

    # clim    = np.array([-10,10])
    # cbticks = np.linspace(clim[0],clim[1],cticks)
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(x, ds_band_mean.depthi, ds_band_mean.psi_fox, ax=ax, cax = cax, contfs=True, nclev=nclev, cbticks=cbticks, clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi_{PER} [m^2/s]  $")

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(x, ds_band_mean.depthi, ds_band_mean.psi_stone, ax=ax, cax = cax, contfs=True, nclev=nclev, cbticks=cbticks, clim=clim, cmap='PiYG_r', extend='both')
    ax.set_title(r"$ \psi_{ALS} [m^2/s] $")

    for ax in hca:
        levels = np.linspace(1026, 1028, 60)
        ax.plot(x, ds_band_mean.mld, color=color, linewidth=lw, linestyle=ls, label=r'MLD')
        ax.plot([], [], 'grey', label="Isopycnal")
        ax.contour(x, ds_band_mean.depthc, ds_band_mean.rho_mean, levels, colors='gray', label='rho contour', linewidths=0.6)
        ax.set_ylim(ylim)
        ax.set_xlabel(r'$radius~[km]$')

    ax.legend(loc='lower center', fontsize = 8)
    figstr = ['(b)','(c)','(d)']
    eva.add_string_like_pyic(hca, figstr)

    if savefig == True: plt.savefig(fig_path+f'streamfunc_comp_{eddy_num}_paper.png', dpi=250, format='png', bbox_inches='tight')


def plot_eddie_overview_paper(vort, datasets, savefig, fig_path):
    """plot vertical parameters of eddie"""
    xlim = np.array([-66, -56])
    ylim = np.array([28, 38])
    asp=ylim.ptp()/xlim.ptp()

    hca, hcb = pyic.arrange_axes(1, 1, plot_cb='bottom', asp=asp, fig_size_fac=1.8, projection=ccrs_proj) # type: ignore
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(vort.lon, vort.lat, vort, ax=ax, cax=cax, clim=1, cmap='RdBu_r', extend='both', transform=ccrs_proj, rasterized=False )
    plot_eddies(ax, datasets)
    ax.set_title(r'$\overline{(\zeta/f)}$')
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    #set major locator

    for ax in hca:
        pyic.plot_settings(ax, xlim=xlim, ylim=ylim)   # type: ignore
        # ax.tick_params(labelsize=10)
        ax.xaxis.set_major_locator(plt.MultipleLocator(4))
        # ax.yaxis.set_major_locator(plt.MultipleLocator(4))
        # ax.yaxis.set_minor_locator(plt.MultipleLocator(4))

    if savefig == True: plt.savefig(fig_path+f'vort_paper.png', dpi=250, format='png', bbox_inches='tight')
