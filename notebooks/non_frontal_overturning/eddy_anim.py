# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd

sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
import funcs as fu
from importlib import reload
import pyicon as pyic
import gsw
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
sys.path.insert(0, "../regional_evaluation_BI/")
import domain_funcs as domfunc
from scipy.spatial import cKDTree
import xarray as xr
import pandas as pd
# %%
import smt_modules.init_slurm_cluster as scluster 
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, scale=2)
cluster
client
#%%
fig_path      = '/home/m/m300878/submesoscaletelescope/results/smt_natl/non_frontal/'
file_path     = '/work/mh0033//work/mh0033/m300878/smt/eddy_coords_natl/'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
savefig       = False
time_ave      = '1W'
lon_reg       = np.array([-70, -54])
lat_reg       = np.array([23.5, 38])

#%%
path    = 'eddy_coords/eddy_alpha_*'
path    = glob.glob(path)[0]
ds_eddy = xr.open_dataset(path)

# %% ######################## compute indices for eliptic band #########################################
reload(fu)
num_lines = 70
ds_out_grid = fu.create_elliptic_output_grid(ds_eddy, num_lines)

#%%
if True:
    plt.figure()
    plt.pcolormesh(M2.lon, M2.lat, M2.isel(depthi=30), cmap='plasma')
    plt.plot(ds_out_grid.longitude.data, ds_out_grid.latitude.data, 'g', lw=1, alpha=0.5)
    plt.plot(ds_eddy.loncen.data, ds_eddy.latcen.data, 'r*', ms=10)
    plt.plot(ds_out_grid.isel(num_lines=0).longitude.data, ds_out_grid.isel(num_lines=0).latitude.data, 'g', lw=1, alpha=1)
    plt.plot(ds_out_grid.isel(num_lines=num_lines-1).longitude.data, ds_out_grid.isel(num_lines=num_lines-1).latitude.data, 'g', lw=1, alpha=1)
    plt.plot(ds_eddy.x.data, ds_eddy.y.data, 'r', lw=1)
    plt.xlim(ds_eddy.xlim)
    plt.ylim(ds_eddy.ylim)

#%%
ds_tgrid = eva.load_smt_grid()
clon     = np.rad2deg(ds_tgrid.clon.compute().drop('clat'))
clat     = np.rad2deg(ds_tgrid.clat.compute().drop('clon'))

ds_2out_grid_idx   = fu.interp_to_reg_grid_band(clon.data, clat.data, ds_out_grid)


# %%  ############################ Eddy animation #########################################
ds_n2 = eva.load_smt_N2()
ds_b  = eva.load_smt_b_fast()

# %%
M2         = np.sqrt(ds_b.dbdx**2 + ds_b.dbdy**2)
M2_sel     = M2.isel(depthi=20).compute()
ds_band_m2 = fu.apply_grid(M2_sel, ds_2out_grid_idx)
# %%
# %%
ds_band_vort = fu.apply_grid(vort, ds_2out_grid_idx)
ds_band_n2   = fu.apply_grid(n2_mask, ds_2out_grid_idx)
ds_band_ri   = fu.apply_grid(Ri, ds_2out_grid_idx)
# %%
# scatter plot with value as color
ds_band = ds_band_m2
fig, ax = plt.subplots()
sc = ax.scatter(ds_band.longitude, ds_band.latitude, c=ds_band, cmap='RdBu_r')

# %%
n2_sel = ds_n2.isel(depthi=20).N2.compute()
ds_band = fu.apply_grid(n2_sel, ds_2out_grid_idx)
# %% or
M2_sel    = M2.isel(depthc=20)
ds_band   = fu.apply_grid(M2_sel, ds_2out_grid_idx)
#%% or
fcoriolis = eva.calc_coriolis_parameter(clon.data)
Ri        = n2_sel * fcoriolis**2 / M2_sel**2
ds_band   = fu.apply_grid(Ri, ds_2out_grid_idx)
ds_band   = ds_band.compute()


# %%
reload(eva)
eva.create_anim(ds_band.longitude, ds_band.latitude, ds_band, fig_path, 'M2', 0, 85, 5, 0, 3e-5, 'Spectral_r')
#%%
reload(eva)
eva.create_anim(ds_band.longitude, ds_band.latitude, ds_band, fig_path, 'Ri', 0, 85, 5, 0, 10, 'Blues_r')

