# %%
import sys
import os 
import glob
import numpy as np
import matplotlib 
from matplotlib import pyplot as plt
import xarray as xr
import dask
import pandas as pd

sys.path.insert(0, "../../")
import smt_modules.all_funcs as eva
import funcs2 as fu
from importlib import reload
import pyicon as pyic
import gsw
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
sys.path.insert(0, "../regional_evaluation_BI/")
import domain_funcs as domfunc
from scipy.spatial import cKDTree
import xarray as xr
import pandas as pd
import dask
# %%
import smt_modules.init_slurm_cluster as scluster 
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False, scale=2)
cluster
client
#%%
fig_path      = '/home/m/m300878/submesoscaletelescope/results/smt_natl/non_frontal/'
file_path     = '/work/mh0033//work/mh0033/m300878/smt/eddy_coords_natl/'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
savefig       = False

#%% load datasets
num_lines = 70
ds_tgrid  = eva.load_smt_grid()
clon      = np.rad2deg(ds_tgrid.clon.compute().drop('clat'))
clat      = np.rad2deg(ds_tgrid.clat.compute().drop('clon'))
ds        = eva.load_smt_eddy_fluxes()

#%%
path_pattern = 'eddy_coords/eddy_alpha_*'
file_paths = glob.glob(path_pattern)
# Load each file as an independent dataset
datasets = [xr.open_dataset(file_path) for file_path in file_paths]


# %% ######################## compute indices for eliptic band #########################################
for eddy_num in range(7):
for eddy_num in 1:
# #%%
#     eddy_num = 2

    print('Eddy number: ', eddy_num)
    ds_eddy = datasets[eddy_num]
    reload(fu)
    ds_out_grid = fu.create_elliptic_output_grid_a(ds_eddy, num_lines)

    if False:
        m2x_mask, m2y_mask, n2_mask, wb_mask, vb_mask, ub_mask, rho_mask, mldx = domfunc.load_clean_dataset('1W')
        M2 = np.sqrt(m2x_mask**2 + m2y_mask**2)
        #%
        fig, ax = plt.subplots(1, 1, subplot_kw={'projection': ccrs_proj}, figsize=(5, 5))
        plt.pcolormesh(M2.lon, M2.lat, M2.isel(depthi=30), cmap='Purples', vmin=0, vmax=5e-8)
        plt.plot(ds_out_grid.longitude.data, ds_out_grid.latitude.data, 'g', lw=1, alpha=0.5)
        plt.plot(ds_eddy.loncen.data, ds_eddy.latcen.data, 'r*', ms=10)
        plt.plot(ds_out_grid.isel(num_lines=0).longitude.data, ds_out_grid.isel(num_lines=0).latitude.data, 'g', lw=1, alpha=1)
        plt.plot(ds_out_grid.isel(num_lines=num_lines-1).longitude.data, ds_out_grid.isel(num_lines=num_lines-1).latitude.data, 'g', lw=1, alpha=1)
        plt.plot(ds_eddy.x.data, ds_eddy.y.data, 'r', lw=1)
        plt.xlim(ds_eddy.xlim)
        plt.ylim(ds_eddy.ylim)
    #%
    ds_2out_grid_idx   = fu.interp_to_reg_grid_band(clon.data, clat.data, ds_out_grid)

    # % ################################### Eddy evaluation #########################################
    reload(eva)
    ds_band = fu.apply_grid(ds, ds_2out_grid_idx)
    ds_band = ds_band.compute()

    # % compute additional variables
    reload(fu)
    ds_band, ds_band_mean = fu.calc_param_var(ds_band, ds_eddy)
    #add length scale
    ds_band_mean['dx'] = ds_band.longitude.isel(num_pixels=0).diff('num_lines')[0].data * eva.convert_degree_to_meter(ds_eddy.latcen.data)
    #%
    reload(fu)
    fu.plot_eddy_sec(ds_band_mean, savefig, fig_path, eddy_num)

    #% add parameterized fluxes
    reload(fu)
    ds_band_str, ds_band_mean_str = fu.calc_streamfunc_param(ds_band.copy(), ds_eddy)
    #%
    #plot 3x3
    fu.flux_stream(ds_band_mean, ds_band_mean_str, savefig, fig_path, eddy_num)
    #%
    reload(fu)
    ds_band_mean_str['rho_mean'] = ds_band_mean.rho_mean
    ds_band_mean_str['dx']       = ds_band_mean.dx
    ds_band_mean_str['psi']      = ds_band_mean.psi
    fu.plot_streamfunc_comp(ds_band_mean_str, savefig, fig_path, eddy_num)





#%%

fu.plot_slice_eddie(ds_band_mean_str, savefig, fig_path, eddy_num)


lon_reg       = np.array([-70, -54])
lat_reg       = np.array([23.5, 38])

 ################################### search and select eddies #########################################
vort        = eva.load_smt_vorticity()
t0          = '2010-03-15T21'
t1          = '2010-03-22T21'
vort        = vort.sel(time=slice(t0, t1))
vort        = vort.vort_f_cells_50m.mean('time').drop(['clon', 'clat'])
vort        = vort.compute()
vort_interp = pyic.interp_to_rectgrid_xr(vort, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

#%%

fu.plot_eddie_overview_paper(vort_interp, datasets, savefig, fig_path)


#%%
if False:
    fig, ax = plt.subplots(1, 3, figsize=(15, 5))
    cb = ax[0].scatter(ds_band.longitude, ds_band.latitude, c=ds_band.ub_prime.isel(depthc=60), cmap='RdBu_r', vmin=-9e-5, vmax=9e-5)
    cb = ax[1].scatter(ds_band.longitude, ds_band.latitude, c=ds_band.vb_prime.isel(depthc=60), cmap='RdBu_r', vmin=-9e-5, vmax=9e-5)
    cb = ax[2].scatter(ds_band.longitude, ds_band.latitude, c=ds_band.uvb_prime.isel(depthc=60), cmap='RdBu_r', vmin=-9e-5, vmax=9e-5)

#test projection
if False:
    uvb_proj = fu.project_uv(ds_band.dbdx_mean, ds_band.dbdy_mean, theta)
    fig, ax = plt.subplots(1,3, figsize=(15, 5))
    vmin = -1e-7
    vmax = 1e-7
    cb = ax[0].scatter(ds_band.longitude, ds_band.latitude, c=ds_band.dbdx_mean.isel(depthi=60), cmap='RdBu_r', vmin=vmin, vmax=vmax)
    cb = ax[1].scatter(ds_band.longitude, ds_band.latitude, c=ds_band.dbdy_mean.isel(depthi=60), cmap='RdBu_r', vmin=vmin, vmax=vmax)
# %%
if False:
    rho_mean_depthi = rho_mean.interp(depthc=ds_band.depthi, method='linear').drop('depthc')
    ds_band['rho_mean_depthi'] = rho_mean_depthi
    mld_int_i, mld_mask_i, mld_i = eva.calc_mld_xr(rho_mean_depthi, ds_band.depthc.data, 0.2)
    #check mld
    ds_band.n2_mean.isel(num_pixels=0).plot(y='depthi', cmap='Spectral_r', vmin=0, vmax=2e-5)
    ds_band.mld.isel(num_pixels=0).plot()
    plt.ylim(500,0)
    #%check mld rho
    plt.figure()
    nl = 65

    xmin=1026.5
    xmax=1027.1
    ds_band.rho_mean.isel(num_lines=nl, num_pixels=0).plot(y='depthc')
    plt.vlines(rho_surf_mean.isel(num_lines=nl, num_pixels=0).data, 0, 500)
    plt.vlines(rho_mean.isel(depthc=mld_int).isel(num_lines=nl, num_pixels=0).data, 0, 500)
    print(rho_surf_mean.isel(num_lines=nl, num_pixels=0).data-rho_mean.isel(depthc=mld_int).isel(num_lines=nl, num_pixels=0).data)
    # plt.ylim(500,0)
    # plt.xlim(xmin,xmax)
# %% ############################ create single ellipse and select data #########################################
if False:
    data_idx = M2.isel(depthi=d).compute()
    data_idx = data_idx.where(data_idx!=0, np.nan)
    ickdtree, theta = fu.compute_ellipse_indices(data_idx, point[0], point[1], a, b, 0, p)

    reload(fu)
    data = M2.isel(depthi=slice(0,40)).compute()
    hov = fu.select_ellipse_from_reg_grid(data, ickdtree, theta)
    hov.isel(depthi=20).plot()

    cb = ax[2].scatter(ds_band.longitude, ds_band.latitude, c=uvb_proj.isel(depthi=60), cmap='RdBu_r', vmin=vmin, vmax=vmax)