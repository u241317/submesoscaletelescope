

# %%
import sys
#sys.path.append("../../smt_modules")
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import dask as da

import pandas as pd
import netCDF4 as nc
import xarray as xr    
#from dask.diagnostics import ProgressBar
import numpy as np
import gsw
import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload
from matplotlib.ticker import FormatStrFormatter
from scipy import stats    #Used for 2D binned statistics
import mld_eval_funcs as mldeva
# sys.path.insert(0, "../december22/")
sys.path.insert(0, "../multiple_fronts_evaluation_BI/")
import slope_param_vs_diagnostic_inclined_funcs as hal
import smt_modules.init_slurm_cluster as scluster 


# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00')

# client, cluster = scluster.init_my_cluster()
cluster
client

# %%
reload(eva)
reload(tools)
############# load data #############
# %%
lon_reg       = np.array([-90, -10])
lat_reg       = np.array([15, 60])
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/first_paper/overview/'
path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'

time_averaged = ''
savefig       = False

# %% load climatologies
argo             = eva.load_argo_climatology()
path_mld_deBoyer = '/work/mh0033/m300878/observation/MLD/deBoyer_22/mld_dr003_ref10m.nc'
mld_deBoyer      = xr.open_dataset(path_mld_deBoyer)

# %% load weekly mean data
if False:
    path_root_dat             = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
    time_averaged             = ''
    path_data                 = f'{path_root_dat}t{time_averaged}.nc'
    t_mean                    = xr.open_dataset(path_data, chunks=dict(depthc=1))
    path_data                 = f'{path_root_dat}s{time_averaged}.nc'
    s_mean                    = xr.open_dataset(path_data, chunks=dict(depthc=1))
    data_t_mean               = pyic.interp_to_rectgrid_xr(t_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_s_mean               = pyic.interp_to_rectgrid_xr(s_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_rho_mean             = gsw.rho(data_s_mean.S001_sp, data_t_mean.T001_sp, depthc[2])
    data_rho_mean_depthi_week = data_rho_mean.interp(depthc=depthi)
    data_rho_mean_depthi_week = data_rho_mean_depthi_week.rename(depthc='depthi')
    data_rho_mean_depthi_week = data_rho_mean_depthi_week.rename('rho_mean')
    data_rho_mean_depthi_week.to_netcdf('/work/mh0033/m300878/parameterization/time_averages/one_week_march/rho_1W_mean.nc')
else:
    data_rho_mean_depthi_week = xr.open_dataset('/work/mh0033/m300878/parameterization/time_averages/one_week_march/rho_1W_mean.nc', chunks=dict(depthi=1))
    data_rho_mean_depthi_week = data_rho_mean_depthi_week.rho_mean.compute()
# %% load monthly mean data
if False: # calc
    path_root_dat              = '/work/mh0033/m300878/parameterization/time_averages/month_mean/'
    path_data                  = f'{path_root_dat}T_1M_mean.nc'
    t_mean                     = xr.open_dataset(path_data, chunks=dict(depthc=1, time=1))
    path_data                  = f'{path_root_dat}S/S_1M_mean.nc'
    s_mean                     = xr.open_dataset(path_data, chunks=dict(depthc=1, time=1))
    data_t_mean                = pyic.interp_to_rectgrid_xr(t_mean.isel(time=0), fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_s_mean                = pyic.interp_to_rectgrid_xr(s_mean.isel(time=0), fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_rho_mean              = gsw.rho(data_s_mean.S_mean, data_t_mean.T_mean, depthc[2])
    data_rho_mean_depthi_month = data_rho_mean.interp(depthc=depthi)
    data_rho_mean_depthi_month = data_rho_mean_depthi_month.rename(depthc='depthi')
    data_rho_mean_depthi_month = data_rho_mean_depthi_month.rename('rho_mean')
    data_rho_mean_depthi_month.to_netcdf('/work/mh0033/m300878/parameterization/time_averages/month_mean/rho_1M_mean.nc')
else:
    data_rho_mean_depthi_month = xr.open_dataset('/work/mh0033/m300878/parameterization/time_averages/month_mean/rho_1M_mean.nc', chunks=dict(depthi=1))
    data_rho_mean_depthi_month = data_rho_mean_depthi_month.rho_mean.compute()

# %% compute ladn mask
if False:
    data_t_mean_depthi = data_t_mean.T_mean.interp(depthc=depthi)
    # data_t_mean_depthi  = data_t_mean.T001_sp.interp(depthc=depthi)
    data_t_mean_depthi_ = data_t_mean_depthi.rename(depthc='depthi')
    data_t_mean_depthi  = data_t_mean_depthi_
    a                   = data_t_mean_depthi.isel(depthi=1).where(~(data_t_mean_depthi.isel(depthi=1)==0), np.nan)
    mask_land           = ~np.isnan(a)
    mask_land           = mask_land.where(mask_land==True, np.nan)
    mask_land           = mask_land.rename('mask_land')
    mask_land.to_netcdf('/work/mh0033/m300878/parameterization/time_averages/mask_land.nc')
else:
    mask_land = xr.open_dataset('/work/mh0033/m300878/parameterization/time_averages/mask_land.nc')
    mask_land = mask_land.mask_land

############################ apply mld algorithm ############################
# %%
reload(eva)
mld, mask, mldx = eva.calc_mld_xr(data_rho_mean_depthi_week, depthi, threshold=0.2)
mldx            = mldx * mask_land
mldx            = mldx.rename('mld')
# %%
mld, mask, mldx_03 = eva.calc_mld_xr(data_rho_mean_depthi_month, depthi, threshold=0.03) #0.03
mldx_03            = mldx_03 * mask_land
mldx_03            = mldx_03.rename('mld')

############################ wb algorithm ############################
# %%
path_data             = f'{path_root_dat}wb_prime{time_averaged}.nc'
wb_prime_mean         = xr.open_dataset(path_data, chunks=dict(depthi=1))
wb_prime_mean         = wb_prime_mean.rename({'__xarray_dataarray_variable__':'wb_prime'})
data_wb_prime_mean    = pyic.interp_to_rectgrid_xr(wb_prime_mean, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
# % wb criteria
reload(eva)
data   = data_wb_prime_mean.wb_prime * mask_land
mld_wb = eva.calc_mld_via_wb(data, depthi, 0.3)
mld_wb = mld_wb * mask_land

# %% ############################ coarse grain ############################
fpath_ckdtree_r01 = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.10_180W-180E_90S-90N.nc'
grid01            = xr.open_dataset(fpath_ckdtree_r01)
grid01_sel        = grid01.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0],lat_reg[1]))
data_wb_prime_mean_grain = hal.coarse_graining(data, grid01_sel) 

mld_wb_grain = eva.calc_mld_via_wb(data_wb_prime_mean_grain, depthi, 0.3)
mld_wb_grain = mld_wb_grain #* mask_land
mld_wb_grain = xr.DataArray(mld_wb_grain, dims=['lat', 'lon'], coords={'lat':grid01_sel.lat, 'lon':grid01_sel.lon})

############################################################################################################
################################## Figures #################################################################
# %%
data = mldx_03
mldeva.single_mld(data, lon_reg, lat_reg, fig_path,  savefig=False)
# %%
# data = mldx
mldeva.two_mld_argo(data, argo, lon_reg, lat_reg, fig_path, savefig=False)

# %%
reload(mldeva)
mldeva.two_mld(mldx_03, mld_deBoyer, lon_reg, lat_reg, fig_path, savefig)
# %%# %%
reload(mldeva)
data1=mldx_03
data2=mldx_03
mldeva.all_mld(data1, data2, argo, mld_deBoyer, lon_reg, lat_reg, fig_path, savefig)

# %%
savefig=True
reload(mldeva)
fname  = 'MLD_intercomparison2'
mldeva.all_mld_wb(mldx, mld_wb, mldx_03, mld_deBoyer, lon_reg, lat_reg, fig_path, fname, savefig)
# %%
reload(mldeva)
data1= mld_wb
data2= mld_wb_grain
mldeva.two_mld_wb_coarse(data1, data2, lon_reg, lat_reg, fig_path, savefig)
# %%
