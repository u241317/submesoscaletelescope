import sys
sys.path.insert(0, "../../")
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import dask as da

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import gsw
import matplotlib.pyplot as plt
from matplotlib import colors
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
from importlib import reload
from matplotlib.ticker import FormatStrFormatter
from scipy import stats    #Used for 2D binned statistics







def single_mld(data, lon_reg, lat_reg, fig_path,  savefig=False):
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 1, plot_cb=True, asp=asp, fig_size_fac=5, sharey=True, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    cmap = 'Blues'
    clim = 0,500
    fname='MLD_0.2rho'
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=True)
    ax.set_title('SMT MLD week mean', fontsize = 15)
    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # pyic.shade(argo.longrid.iLON, argo.latgrid.iLAT, argo.mld_da_mean.isel(iMONTH=2), ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=True)

    for ax in hca:
        ax.tick_params(labelsize=15)
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=150, format='png', bbox_inches='tight')

def two_mld_argo(data, argo, lon_reg, lat_reg, fig_path, savefig=False):
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 2, plot_cb='bottom', asp=asp, fig_size_fac=2, sharex=True, projection=ccrs_proj, axlab_kw=None,)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    cmap = 'RdYlBu_r'
    clim = 0,350
    fname='MLD_0.2rho'
    contfs = np.arange(50.,550.,50.)

    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title('SMT MLD week mean')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(argo.longrid, argo.latgrid, argo.mld_da_mean.isel(iMONTH=2), ax=ax, cax=cax, contfs=contfs, cmap = cmap, clim = clim, transform=ccrs_proj, rasterized=False)
    ax.set_title('Argo Float  mld_da_mean March 2010')

    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=150, format='png', bbox_inches='tight')

def two_mld(data, mld_deBoyer, lon_reg, lat_reg, fig_path, savefig=False):
    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])

    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 2, plot_cb='bottom', asp=asp, fig_size_fac=2, sharex=True, projection=ccrs_proj)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    levels = np.array([200])
    fs     = 12
    cmap   = 'YlGnBu'
    fname  = 'monthly_03'
    contfs = np.array([10, 15, 20, 40 ,60, 80, 100, 125, 150, 175, 200, 250, 300, 350, 400, 500, 600, 700, 800])#np.arange(50.,550.,50.)
    clim   = contfs[0], contfs[-1]

    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title(r'ICON-SMT March 2010 $\Delta \rho =0.03$', fontsize = fs)
    # add red box for regions
    ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    # ax.contour(data.lon, data.lat, data, levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(mld_deBoyer.lon, mld_deBoyer.lat, mld_deBoyer.isel(time=2).mld_dr003, ax=ax, cax=cax, contfs=contfs, cmap = cmap, clim = clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'de Boyer Montégut climatology March $\Delta \rho =0.03$', fontsize =fs)
    # ax.contour(mld_deBoyer.lon, mld_deBoyer.lat, mld_deBoyer.isel(time=2).mld_dr003, levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    ax.plot([lon_R[0], lon_R[0]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[1], lon_R[1]], [lat_R[0], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[0], lat_R[0]], color='r', transform=ccrs.PlateCarree())
    ax.plot([lon_R[0], lon_R[1]], [lat_R[1], lat_R[1]], color='r', transform=ccrs.PlateCarree())
    t = ax.text(lon_R[1]-2.5, lat_R[1]-2.5, 'R', color='red', fontsize=10)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    cax.set_title('Mixed Layer Depth [m]')
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
        ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore

    if savefig == True: plt.savefig(f'{fig_path}{fname}_new.png', dpi=150, format='png', bbox_inches='tight')


def two_mld_wb_coarse(data1, data2, lon_reg, lat_reg, fig_path, savefig=False):
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 2, plot_cb='bottom', asp=asp, fig_size_fac=5, sharex=True, projection=ccrs_proj)
    ii=-1
    cmap   = 'gist_rainbow_r'
    fname  = 'wb_coarse_fine'
    contfs = np.array([10, 15, 20, 40 ,60, 80, 100, 125, 150, 175, 200, 250, 300, 350, 400, 500, 600, 700, 800])#np.arange(50.,550.,50.)
    clim   = contfs[0], contfs[-1]

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data1.lon, data1.lat, data1, ax=ax, cax=cax, clim=clim, contfs=contfs,  transform=ccrs_proj, rasterized=False, cmap=cmap)
    ax.set_title(r'$\overline{w^\prime b^\prime}$ weekly mean 0.02° grid')
    
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data2.lon, data2.lat, data2, ax=ax, cax=cax, cmap = cmap, clim = clim, contfs=contfs,  transform=ccrs_proj, rasterized=False)
    ax.set_title(r'$\overline{w^\prime b^\prime}$ weekly mean 0.10° grid')

    cax.set_title('Mixed Layer Depth [m]')
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=150, format='png', bbox_inches='tight')


def all_mld(data1, data2, argo, mld_deBoyer, lon_reg, lat_reg, fig_path, savefig=False):
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 4, plot_cb='bottom', asp=asp, fig_size_fac=3, projection=ccrs_proj, daxt=1.1, dcbt=0.2)
    ii     = -1
    fs     = 15
    levels = np.array([200])
    cmap   = 'gist_rainbow_r'
    fname  = 'MLD_intercomparison'
    contfs = np.array([10, 15, 20, 40 ,60, 80, 100, 125, 150, 175, 200, 250, 300, 350, 400, 500, 600, 700, 800])#np.arange(50.,550.,50.)
    clim   = contfs[0], contfs[-1]

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data1.lon, data1.lat, data1, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title(r'SMT week mean $\Delta \rho =0.2$', fontsize = fs)
    ax.contour(data1.lon, data1.lat, data1, levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data2.lon, data2.lat, data2, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title(r'SMT week mean  $\Delta \rho =0.03$', fontsize = fs)
    ax.contour(data2.lon, data2.lat, data2, levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(argo.longrid, argo.latgrid, argo.mld_da_mean.isel(iMONTH=2), ax=ax, cax=cax, contfs=contfs, cmap = cmap, clim = clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'Holte 2017 climatology march 2010 $\Delta \rho =0.03$', fontsize = fs)
    ax.contour(argo.longrid, argo.latgrid, argo.mld_da_mean.isel(iMONTH=2), levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(mld_deBoyer.lon, mld_deBoyer.lat, mld_deBoyer.isel(time=2).mld_dr003, ax=ax, cax=cax, contfs=contfs, cmap = cmap, clim = clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'deBoyer Montegut 2022 climatology march $\Delta \rho =0.03$', fontsize =fs)
    ax.contour(mld_deBoyer.lon, mld_deBoyer.lat, mld_deBoyer.isel(time=2).mld_dr003, levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    cax.set_title('Mixed Layer Depth [m]', fontsize=fs)
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=150, format='png', bbox_inches='tight')

def all_mld_wb(data, mld_wb, mldx_03, mld_deBoyer, lon_reg, lat_reg, fig_path, fname, savefig=False):
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 4, plot_cb='bottom', asp=asp, fig_size_fac=3, sharey=True, projection=ccrs_proj, dcbt=1.3, daxt=1.5)
    ii     = -1
    fs     = 15
    levels = np.array([200])
    cmap   = 'gist_rainbow_r'
    contfs = np.array([10, 15, 20, 40 ,60, 80, 100, 125, 150, 175, 200, 250, 300, 350, 400, 500, 600, 700, 800])#np.arange(50.,550.,50.)
    clim   = contfs[0], contfs[-1]

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title(r'SMT weekly mean $\Delta \rho =0.2$', fontsize = fs)
    ax.contour(data.lon, data.lat, data, levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data.lon, data.lat, mld_wb, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title(r'SMT weekly mean  $\overline{w^\prime b^\prime}$', fontsize = fs)
    # ax.contour(data.lon, data.lat, mld, levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data.lon, data.lat, mldx_03, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title(r'SMT weekly mean  $\Delta \rho =0.03$', fontsize = fs)
    ax.contour(data.lon, data.lat, mldx_03, levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(mld_deBoyer.lon, mld_deBoyer.lat, mld_deBoyer.isel(time=2).mld_dr003, ax=ax, cax=cax, contfs=contfs, cmap = cmap, clim = clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'deBoyer Montegut 2022 climatology march $\Delta \rho =0.03$', fontsize =fs)
    ax.contour(mld_deBoyer.lon, mld_deBoyer.lat, mld_deBoyer.isel(time=2).mld_dr003, levels=levels, colors='k', linewidths=1, transform=ccrs_proj)

    cax.set_title('Mixed Layer Depth [m]', fontsize=fs)
    for ax in hca:
        ax.tick_params(labelsize=20)
        cax.tick_params(labelsize=25)
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=150, format='png', bbox_inches='tight')

def wb_mld_3(data1, data2, data3, lon_reg, lat_reg, fig_path, fname, savefig=False):
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1, 3, plot_cb='bottom', asp=asp, fig_size_fac=3, projection=ccrs_proj, daxt=1.1, dcbt=0.2)
    ii     = -1
    fs     = 15
    cmap   = 'gist_rainbow_r'
    fname  = 'MLD_intercomparison'
    contfs = np.array([10, 15, 20, 40 ,60, 80, 100, 125, 150, 175, 200, 250, 300, 350, 400, 500, 600, 700, 800])#np.arange(50.,550.,50.)
    clim   = contfs[0], contfs[-1]

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data1.lon, data1.lat, data1, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title(r'$\overline{w^\prime b^\prime}$ 40%', fontsize = fs)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data2.lon, data2.lat, data2, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title(r'$\overline{w^\prime b^\prime}$ 30%', fontsize = fs)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data3.lon, data3.lat, data3, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap, contfs=contfs)
    ax.set_title(r'$\overline{w^\prime b^\prime}$ 5%', fontsize = fs)


    cax.set_title('Mixed Layer Depth [m]', fontsize=fs)
    for ax in hca:
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=150, format='png', bbox_inches='tight')

