# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='00:50:00')
cluster
client

# %%
############## plot config ##############
fig_path      = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/KE/'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'

fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'
lon_reg = np.array([-40, 30])
lat_reg = np.array([-55, -15])

# zoom1
lon_reg1 = np.array([-5, 10])
lat_reg1 = np.array([-42, -32])
lon_reg2 = np.array([-2, 0.5])
lat_reg2 = np.array([-38, -36.75])

savefig = False

# %%
lon_reg = [-25, 30]
lat_reg = [-60, -5]
gg      = eva.load_smt_wave_grid()
res     = np.sqrt(gg.cell_area_p)
res     = res.rename(cell='ncells')
data_r  = pyic.interp_to_rectgrid_xr(res, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

# %%
# idepth     = 17
# idepth_lev = 1
# idepth     = 76
# idepth_lev = 3

idepth     = 26
idepth_lev = 1

#################### horizontal KE ####################
############################################################
reload(eva) # 2h interval combining 3d and 2d output

begin = '2019-07-06T01:15:00'
end   = '2019-10-17T23:15:00'

ds_u1     = eva.load_smt_wave_all('u', exp=7, remove_bnds=True)
ds_u1     = ds_u1.isel(depth=idepth).u
ds_u1     = ds_u1.drop('clon').drop('clat')
ds_levels = eva.load_smt_wave_levels(7, it=2)
ds_levels = ds_levels.rename({'ncells_2': 'ncells'})
ds_u2     = ds_levels.isel(depth=idepth_lev).u
ds_u      = xr.concat([ds_u1, ds_u2], dim='time').sel(time=slice(begin, end)) 

ds_v1 = eva.load_smt_wave_all('v', exp=7, remove_bnds=True)
ds_v1 = ds_v1.isel(depth=idepth).v
ds_v1 = ds_v1.drop('clon').drop('clat')
ds_v2 = ds_levels.isel(depth=idepth_lev).v
ds_v  = xr.concat([ds_v1, ds_v2], dim='time').sel(time=slice(begin, end)) 

############################################################
# %% only 2d output but 1h interval all!
############################################################
ds_levels = eva.load_smt_wave_levels(7, it=1)
ds_levels = ds_levels.rename({'ncells_2': 'ncells'})
ds_u      = ds_levels.isel(depth=idepth_lev).u
ds_v      = ds_levels.isel(depth=idepth_lev).v

############################################################

# %% depth eval 1h mid Oct -Jan
############################################################
# begin = pd.to_datetime('2019-10-18T00:15:00')
# end   = pd.to_datetime('2020-01-27T23:15:00')
begin   = pd.to_datetime('2020-01-27T23:15:00')
end   = pd.to_datetime('2020-06-13T23:15:00')

# %% old depth levels
# idepth_lev = 2
ds_levels = eva.load_smt_wave_levels(7, it=1)
ds_levels = ds_levels.rename({'ncells_2': 'ncells'})
# %% new depth levels
reload(eva)
ds_levels = eva.load_smt_wave_levels_2(7, it=1)
# %%
reload(fu)
idepth_lev = 5
ds_u      = ds_levels.isel(depth=idepth_lev).u
ds_v      = ds_levels.isel(depth=idepth_lev).v

# %
# select time
# ds_u = ds_u.sel(time=slice(begin, end))
# ds_v = ds_v.sel(time=slice(begin, end))
################################################################

# % ##################### compute and save cropped data #####################
# path_2_data = '/work/mh0033/m300878/spectra/KE/sa_600m/'
# path_2_data = '/work/mh0033/m300878/spectra/KE/sa_1200m/'
# path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/
# path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/1h_Oct_Jan_depth/'
path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/1h_oct_jul/'


reload(fu)
fname       = f'smtwv0007_spectra_hor_KE_10deg_{ds_u.depth.data}'
# fu.calc_interp_KE(ds_u, ds_v, data_r, fname, path_2_data, res=1200)
fu.calc_interp_KE_close(ds_u, ds_v, fname, path_2_data)

# %% exp9
begin = '2019-07-01T03:15:00'
end   = '2019-10-19T23:15:00'

reload(eva)
ds_levels = eva.load_smt_wave_levels(9, it=2)
ds_levels = ds_levels.rename({'ncells_2': 'ncells'})
# %%

ds_u1 = eva.load_smt_wave_9('u', it=1)
ds_u1 = ds_u1.isel(depth=idepth).u
ds_u2 = ds_levels.isel(depth=idepth_lev).u
ds_u  = xr.concat([ds_u1, ds_u2], dim='time').sel(time=slice(begin, end))

ds_v1 = eva.load_smt_wave_9('v', it=1)
ds_v1 = ds_v1.isel(depth=idepth).v
ds_v2 = ds_levels.isel(depth=idepth_lev).v
ds_v  = xr.concat([ds_v1, ds_v2], dim='time').sel(time=slice(begin, end))

# %%
reload(fu)
fname       = f'smtwv0009_spectra_hor_KE_all_{ds_u1.depth.data}'

fu.calc_interp_KE(ds_u, ds_v, data_r, fname, path_2_data, res=1200)
# fu.calc_interp_KE_close(ds_u, ds_v, fname, path_2_data)

# %% exp8
begin = '2022-02-01T01:15:00'
end   = '2022-03-31T23:15:00'

ds_u1 = eva.load_smt_wave_all('u', exp=8, remove_bnds=True)
ds_u1 = ds_u1.isel(depth=idepth).u
ds_u1 = ds_u1.drop('clon').drop('clat')
ds_u1 = ds_u1.sel(time=slice(begin, end))


ds_v1 = eva.load_smt_wave_all('v', exp=8, remove_bnds=True)
ds_v1 = ds_v1.isel(depth=idepth).v
ds_v1 = ds_v1.drop('clon').drop('clat')
ds_v1 = ds_v1.sel(time=slice(begin, end)) 

# %%
reload(fu)
fname       = f'smtwv0008_spectra_hor_KE_all_{ds_u1.depth.data}'

fu.calc_interp_KE(ds_u1, ds_v1, data_r, fname, path_2_data, res=1200)
# fu.calc_interp_KE_close(ds_u1, ds_v1, fname, path_2_data)

# %% ############################ R2B9  ############################
reload(eva)

ds = eva.load_r2b9('outdata_2d_lev_0')

# %%
reload(fu)
depth_lev = 2
path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/r2b9/'
fname       = f'r2b9_spectra_hor_KE_10deg_{ds.u.depth[depth_lev]:.2f}'


fu.calc_interp_KE_close_r2b9(ds.u.isel(depth=depth_lev), ds.v.isel(depth=depth_lev), fname, path_2_data)
# %%
reload(fu)
path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/r2b9/all/ke/'
fname       = f'r2b9_spectra_hor_KE_10deg_ke'

fu.calc_interp_KE_r2b9(ds.u, ds.v, fname, path_2_data)
# %%


path = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/1h_oct_jul/smtwv0007_spectra_hor_KE_10deg_1.0.nc'
ds = xr.open_dataset(path)
# %%
