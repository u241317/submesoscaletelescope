# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
import math

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 



# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='02:00:00', wait=False)
cluster
client
# %%
fig_path    = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/KE/'
savefig     = False
path_2_data = '/work/mh0033/m300878/spectra/KE/sa_600m/'
# %%
reload(fu)
ds_1, ds_2 = fu.load_mooring_data()
m_pos = [(ds_1.longitude.data, ds_1.latitude.data), (ds_2.longitude.data, ds_2.latitude.data)]


# %%
if True:
    ds_obs = ds_1

    P = []
    for i in range(ds_obs.depth.size):
        idepth = i
        KE_obs = ds_obs.isel(depth=idepth).ucur**2 / 2 + ds_obs.isel(depth=idepth).vcur**2 /2
        f, p   = signal.welch(KE_obs.fillna(0.), fs=1/(3600*0.5), window='hann', nperseg=1024, noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=-1)
        P.append(p)

    # concat numpy array
    P     = np.array(P)

    reload(fu)
    lat_mean = -40
    fname    = 'obs_spectra_m1'
    fu.plot_obs_spectra(ds_obs, f, P, lat_mean, fig_path, fname, savefig)

# %%
if False:
    # season selection
    ds_1_sel1 = ds_1.sel(time=slice('2021-07-01', '2021-10-31'))
    ds_1_sel2 = ds_1.sel(time=slice('2022-07-01', '2022-10-31'))

    # ds_1_sel1 = ds_1.sel(time=slice('2022-02-01', '2022-04-30'))
    # ds_1_sel2 = ds_1.sel(time=slice('2023-02-01', '2023-04-30'))

    ds_1_sel1.ucur.isel(depth=3).plot()
    ds_1_sel2.ucur.isel(depth=3).plot()

    t_window = 5
    PP_ds1_1                      = fu.compute_mooring_spectra(ds_1_sel1, t_window, samp_freq=0.5)
    PP_ds1_2                      = fu.compute_mooring_spectra(ds_1_sel2, t_window, samp_freq=0.5)

    PP_ds1 = xr.concat([PP_ds1_1, PP_ds1_2], dim='n')
    #average
    PP_ds1 = PP_ds1.mean(dim='n')
    d0 = 1; dend = 9
    P_dmean_ds1                 = PP_ds1_1.isel(depth=slice(d0,dend)).mean(dim='depth')
    P_dmean_ds1['nreal']        = t_window * (dend-d0)
    P_dmean_ds1.attrs['depths'] = PP_ds1.depth[d0:dend].values

# %% Mooring 1
t_window = 40
PP_ds1                      = fu.compute_mooring_spectra(ds_1, t_window, samp_freq=0.5)

d0 = 0; dend = 1
d0 = 1; dend = 9 #9 
d0 = 2; dend = 4
P_dmean_ds1                 = PP_ds1.isel(depth=slice(d0,dend)).mean(dim='depth')
P_dmean_ds1['nreal']        = t_window * (dend-d0)
P_dmean_ds1.attrs['depths'] = PP_ds1.depth[d0:dend].values

# %%
reload(fu)
PP_ds2 = fu.compute_mooring_spectra(ds_2, t_window, samp_freq=0.25)

d0 = 1; dend= 4 #50m
d0 = 3; dend= 20  #20
d0 = 6; dend= 12 #75m
P_dmean_ds2                 = PP_ds2.isel(depth=slice(d0,dend)).mean(dim='depth')
P_dmean_ds2['nreal']        = t_window * (dend-d0)
P_dmean_ds2.attrs['depths'] = PP_ds2.depth[d0:dend].values


# %%
lon_reg = np.array([-40, 30])
lat_reg = np.array([-55, -15])
reload(eva)

data          = eva.load_smt_wave_land_mask()
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'
mask_land     = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg, lat_reg)
mask_land     = mask_land > 0

# %%
gg      = eva.load_smt_wave_grid()
res     = np.sqrt(gg.cell_area_p)
res     = res.rename(cell='ncells')
data_r  = pyic.interp_to_rectgrid_xr(res, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)


# %%
path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/' #high res
path_2_data = '/work/mh0033/m300878/spectra/KE/sa_1200m/' #low res

dstings = ['_51.50000000000001', '_98.15', '_1008.95']

def load_smt_spectra(exp, eval_depth):
    fname     = f'smtwv000{exp}_spectra_hor_KE_all{eval_depth}'
    data_exp  = xr.open_dataset(f'{path_2_data}/{fname}.nc')
    data_exp  = data_exp.rename(__xarray_dataarray_variable__='KE')
    # data_exp  = data_exp.drop('clon').drop('clat')
    return data_exp

# %% #compute averaged spectra for all experiments and depths
P7 = []
P8 = []
P9 = []
ni = 1
for i in range(2):
    eval_depth = dstings[i]
    data_exp7  = load_smt_spectra(7, eval_depth)
    data_exp8  = load_smt_spectra(8, eval_depth)
    data_exp9  = load_smt_spectra(9, eval_depth)
    P_exp7     = fu.compute_spectra_uchida(data_exp7.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=2)
    P_exp8     = fu.compute_spectra_uchida(data_exp8.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=2)
    P_exp9     = fu.compute_spectra_uchida(data_exp9.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=2)
    P7.append(P_exp7)
    P8.append(P_exp8)
    P9.append(P_exp9)

P7 = xr.concat(P7, dim='n')
P8 = xr.concat(P8, dim='n')
P9 = xr.concat(P9, dim='n')

P7_mean = P7.mean(dim='n')
P8_mean = P8.mean(dim='n')
P9_mean = P9.mean(dim='n')


# %% compute spectra at single depth for EXP7
path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/1h_int_sep_jan/'
eval_depth  = dstings[0]
data_exp7   = load_smt_spectra(7, eval_depth)
P_exp7      = fu.compute_spectra_uchida(data_exp7.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=1)

# %%
pos         = np.array([5, -32.5])
radius      = 5
mask_circle = (data_exp7.lon - pos[0])**2 + (data_exp7.lat - pos[1])**2 < radius**2
count       = np.sum(mask_circle)

reload(fu)
lat_mean = -32.5
fname='KE_spectra_51.5_1h'
fu.plot_spectra_KE_1h(P_exp7.where(mask_circle), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)



 # %% alternative spectra at mooring locations #########################
######################################################################

# mooring1
reload(fu)
fu.plot_tseries(data_exp7, data_exp8, data_exp9, ds_1, ds_2, m_pos, fig_path, savefig)

e7_m1 = data_exp7.KE.sel(lon=m_pos[0][0], lat=m_pos[0][1], method='nearest')
e7_m2 = data_exp7.KE.sel(lon=m_pos[1][0], lat=m_pos[1][1], method='nearest')
e8_m1 = data_exp8.KE.sel(lon=m_pos[0][0], lat=m_pos[0][1], method='nearest')
e8_m2 = data_exp8.KE.sel(lon=m_pos[1][0], lat=m_pos[1][1], method='nearest')
e9_m1 = data_exp9.KE.sel(lon=m_pos[0][0], lat=m_pos[0][1], method='nearest')
e9_m2 = data_exp9.KE.sel(lon=m_pos[1][0], lat=m_pos[1][1], method='nearest')

# %%
# select a circle around mooring 1
pos_m1 = np.array([m_pos[0][0], m_pos[0][1]])
pos_m2 = np.array([m_pos[1][0], m_pos[1][1]])
radius = 0.1

x = data_exp7.lon
y = data_exp7.lat

X, Y = np.meshgrid(x, y)
mask_circle_m1 = (X - pos_m1[0])**2 + (Y - pos_m1[1])**2 < radius**2
mask_circle_m2 = (X - pos_m2[0])**2 + (Y - pos_m2[1])**2 < radius**2

# mask_circle_m1 = (data_exp7.lon - pos_m1[0])**2 + (data_exp7.lat - pos_m1[1])**2 < radius**2
# mask_circle_m2 = (data_exp7.lon - pos_m2[0])**2 + (data_exp7.lat - pos_m2[1])**2 < radius**2
# data_interp = data_exp7.KE.sel(lon=slice(m_pos[0][0]-radius, m_pos[0][0]+radius), lat=slice(m_pos[0][1]-radius, m_pos[0][1]+radius))

mask_sa = mask_circle_m1 | mask_circle_m2
# convert to xarray
mask_sa = xr.DataArray(mask_sa, dims=['lon', 'lat'], coords={'lon': data_exp7.lon, 'lat': data_exp7.lat})

# %%
ds_EM = xr.concat([e7_m1, e7_m2, e8_m1, e8_m2, e9_m1, e9_m2], dim='exp')

string = [ 'e7_m1', 'e7_m2', 'e8_m1', 'e8_m2', 'e9_m1', 'e9_m2']


t_window = 15

P = []
for i in range(6):
    sel  = ds_EM.isel(exp=i)
    time = int(sel.size)
    dt   = math.floor(time/t_window)
    sel  = sel.isel(time=slice(0,dt*t_window))
    p = []
    for j in range(t_window):
        p.append(fu.compute_spectra_uchida(sel.isel(time=slice(j*dt, (j+1)*dt)).fillna(0.), samp_freq=2))
    p = xr.concat(p, dim='n')
    p = p.mean(dim='n')
    P.append(p)

P_EM = xr.concat(P, dim='exp')

# %%
P_EM = fu.compute_spectra_uchida(ds_EM.fillna(0.), samp_freq=2)
for i in range(6):
    plt.loglog(P_EM.freq_time[1:]*86400, P_EM.isel(exp=i)[1:], label=f'{string[i]}')
plt.legend()

# %%
e7_m1_P = P7.isel(n=1).where(mask_circle_m1).mean(['lon','lat'], skipna=True)
e7_m2_P = P7.isel(n=1).where(mask_circle_m2).mean(['lon','lat'], skipna=True)
e8_m1_P = P8.isel(n=1).where(mask_circle_m1).mean(['lon','lat'], skipna=True)
e8_m2_P = P8.isel(n=1).where(mask_circle_m2).mean(['lon','lat'], skipna=True)
e9_m1_P = P9.isel(n=1).where(mask_circle_m1).mean(['lon','lat'], skipna=True)
e9_m2_P = P9.isel(n=1).where(mask_circle_m2).mean(['lon','lat'], skipna=True)

EXP = [e7_m1_P, e7_m2_P, e8_m1_P, e8_m2_P, e9_m1_P, e9_m2_P]
# %%
for i in range(6):
    plt.loglog(EXP[i].freq_time[1:]*86400, EXP[i][1:], label=f'{string[i]}')
plt.legend()
# %%
reload(fu)
fname    = f'/mooring_pos/single_pos_at_mooring_{radius}'
lat_mean = (m_pos[0][1] + m_pos[1][1])/2
count    = np.count_nonzero(mask_circle_m1)
fu.plot_spectra_KE_single_pos(EXP, P_dmean_ds1, P_dmean_ds2, lat_mean, radius, count, fig_path, fname, savefig)

fname = f'/mooring_pos/domain_{radius}'
fu.plot_domain_masked(data, mask_sa, m_pos, fig_path, fname, savefig)
######################################################################
######################################################################

# %%
if False:
    path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/'
    data_exp7_n = load_smt_spectra(7, eval_depth)
    mask_sa2 = ~np.isnan(data_exp7_n.KE.isel(time=0))
    exp7_n =  fu.compute_spectra_uchida(data_exp7_n.KE.fillna(0.), samp_freq=2)

    # %%
    P_exp7 = P7.isel(n=1).where(mask_sa)
    plt.loglog(P_exp7.freq_time[1:]*86400,   P_exp7.mean(['lon','lat'], skipna=True)[1:], label=f'tides Jul-Oct 2019 (t={P_exp7.freq_time.size*2/12:.0f}d) large')

    P_exp7 = exp7_n.where(mask_sa2)
    plt.loglog(P_exp7.freq_time[1:]*86400,   P_exp7.mean(['lon','lat'], skipna=True)[1:], label=f'tides Jul-Oct 2019 (t={P_exp7.freq_time.size*2/12:.0f}d) small')

    plt.legend()


    # %%

    data_exp8 = load_smt_spectra(8, eval_depth)
    data_exp9 = load_smt_spectra(9, eval_depth)
    # %%
    P_exp7   = fu.compute_spectra_uchida(data_exp7.KE.fillna(0.), samp_freq=2)
    P_exp8   = fu.compute_spectra_uchida(data_exp8.KE.fillna(0.), samp_freq=2)
    P_exp9   = fu.compute_spectra_uchida(data_exp9.KE.fillna(0.), samp_freq=2)


# %%    ##### Influence of Ringpath domain on KE spectra #####
##############################################################
data_exp7 = load_smt_spectra(7, dstings[1])
data      = data_exp7.KE.drop('time').mean('time')
# data_exp8 = load_smt_spectra(8, dstings[0])
# data      = data_exp8.KE.drop('time').mean('time')
# %%
pos         = np.array([5, -32.5])
radius      = 8
data_interp = data_exp7.KE.isel(time=0).drop('depth').drop('time')
mask_circle = (data_interp.lon - pos[0])**2 + (data_interp.lat - pos[1])**2 < radius**2

reload(fu)
reg = 'm'
ap  = 'm'
Z   = fu.imbuto_di_dante(data.lon, data.lat, reg, ap)

# %%
reload(fu)
if True:
    res = 700
    mask_res = (data_r < res) & (data_r > 650) & (data_r.lon > 10) & (data_r.lat < -35)

    res = 1200
    mask_res = (data_r < res) & (data_r.lat > -34) & (data_r.lat < -25) & (data_r.lon < 10) & (data_r.lon > -5)
    mask_res = mask_res.drop('clon').drop('clat')

    res = 580
    mask_res = (data_r < res)
    mask_sa  = mask_res * mask_land
else:
    mask_sa = mask_land * Z
count   = np.count_nonzero(mask_sa)

# %%
reload(fu)
lat_mean = -32.5
fname = f'KE_spectra_{reg}_ap{ap}_dselect'
# fname  = 'KE_100m_final'
n     = 1
path_adapt = fig_path + '/domain_var/res_var/imbuto_di_dante/'
fu.plot_spectra_KE(P7.isel(n=n).where(mask_sa), P8.isel(n=n).where(mask_sa), P9.isel(n=n).where(mask_sa), P_dmean_ds1, P_dmean_ds2, lat_mean, count, path_adapt, fname, savefig)


fname = f'domain_{reg}_ap{ap}'
fu.plot_domain_masked(data, mask_sa, m_pos, path_adapt, fname, savefig)


# %% ########################## Spectra Depth averaged ###########################
reload(fu)
fname    = f'KE_spectra_dmean_580_e79_alexa'
lat_mean = -32.5
n = 1
# KE_obs = None
fu.plot_spectra_KE(P_exp7.where(mask_sa), P_exp8.where(mask_sa), P_exp9.where(mask_sa), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)
fu.plot_spectra_KE_alexa(P7.isel(n=n).where(mask_sa), P8.isel(n=n).where(mask_sa), P9.isel(n=n).where(mask_sa), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)

fu.plot_spectra_KE(P7_mean.where(mask_sa), P8_mean.where(mask_sa), P9_mean.where(mask_sa), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)

 # %% domain
reload(fu)
fname = 'domain_mooring_close'
fu.plot_domain(data_exp7, m_pos, fig_path, fname, savefig)
# %%
exp = 'exp7'
fname = f'spectral_section_{exp}'
a = P7.isel(n=n).where(mask_sa).isel(lon=300)
plt.pcolormesh(a.lat, a.freq_time, a, norm=colors.LogNorm(vmin=1e-2, vmax=1e3))
plt.colorbar()
plt.xlabel('lat')
plt.ylabel('freq')
plt.title(f'{exp}, lon = {a.lon.data:.2f}')
# plt.savefig(f'{fig_path}/sections/{fname}.png', dpi=150, bbox_inches='tight')
# %%
