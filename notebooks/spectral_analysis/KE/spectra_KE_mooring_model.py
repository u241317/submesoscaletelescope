# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
import math

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
cluster
client

# %%
fig_path    = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/KE/mod_obs'
savefig     = False
# %% compute spectra at single depth for EXP7
def load_smt_spectra(exp, eval_depth):
    fname     = f'smtwv000{exp}_spectra_hor_KE_all{eval_depth}'
    # fname     = f'smtwv000{exp}_spectra_hor_KE_10deg{eval_depth}'
    data_exp  = xr.open_dataset(f'{path_2_data}/{fname}.nc')
    data_exp  = data_exp.rename(__xarray_dataarray_variable__='KE')
    # data_exp  = data_exp.drop('clon').drop('clat')
    return data_exp


# %% ######################## Configuration ################
interp_gaps = True
d          = 1
dstings    = ['_51.50000000000001', '_98.15', '_1008.95']
eval_depth = dstings[d]
t_window   = 30#100
samp_freq  = 1

#%%     ######################### load 1h interval data ############################
path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/1h_int_sep_jan/'
ni          = 1
if False:
    # 2h sample
    data_exp7   = load_smt_spectra(7, eval_depth)
    data_exp7   = data_exp7.isel(time=slice(0, None, 2))
    P_exp7      = fu.compute_spectra_uchida(data_exp7.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=2)
    # 2h sample
    data_exp7   = load_smt_spectra(7, eval_depth)
    data_exp7   = data_exp7.isel(time=slice(0, None, 4))
    P_exp7      = fu.compute_spectra_uchida(data_exp7.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=4)
else:
    fname        = f'smtwv0007_spectra_hor_KE_10deg{eval_depth}'
    data_exp     = xr.open_dataset(f'{path_2_data}/{fname}.nc')
    data_exp7_01 = data_exp.rename(__xarray_dataarray_variable__='KE')
    path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/1h_oct_jul/'
    fname        = f'smtwv0007_spectra_hor_KE_10deg{eval_depth}'
    data_exp7_02 = xr.open_dataset(f'{path_2_data}/{fname}.nc')
    data_exp7_02 = data_exp7_02.rename(__xarray_dataarray_variable__='KE')
    data_exp7_1y = xr.concat([data_exp7_01, data_exp7_02], dim='time')
    data_exp7_1y = data_exp7_1y.drop_duplicates('time')
    P_exp7       = fu.compute_spectra_uchida(data_exp7_1y.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=1)
    P_exp7_1h    = P_exp7

# %%  ######################### load 1h R2B9 data ############################
depth_lev   = 98.15
path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/r2b9/'
fname       = f'r2b9_spectra_hor_KE_10deg_{depth_lev}'
data_r2b9   = xr.open_dataset(f'{path_2_data}/{fname}.nc')
data_r2b9   = data_r2b9.rename(__xarray_dataarray_variable__='KE')
data_r2b9   = data_r2b9.where(np.isfinite(data_r2b9), 0)
# select same time period as exp7
data_r2b9   = data_r2b9.sel(time=slice(data_exp7_1y.time[0], data_exp7_1y.time[-1]))
P_r2b9      = fu.compute_spectra_uchida(data_r2b9.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=1)



# %% Spectra along Line
if False:
    # %%
    # replace zeros with nans
    radius = 10
    pos         = np.array([5, -32.5])
    mask01 = (data_exp7_01.lon - pos[0])**2 + (data_exp7_01.lat - pos[1])**2 < radius**2
    # draw line between two points
    ds = P_exp7_1h.where(mask01)#.isel(lat=110)
    p0 = np.array([ds.lon.min(), ds.lat.min()])
    p1 = np.array([ds.lon.max(), ds.lat.max()])
    # p0 = np.array([ds.lon.mean(), ds.lat.min()])
    # p1 = np.array([ds.lon.mean(), ds.lat.max()])
    # p0 = np.array([ds.lon.min(), ds.lat.mean()])
    # p1 = np.array([ds.lon.max(), ds.lat.mean()])

    lon, lat, dist_sec = pyic.derive_section_points(p0.data, p1.data, 200)

    A = []
    for i in range(len(lon)):
        # for ii in range(len(lat)):
            nearest_neighbor = np.argmin((ds.lon.data - lon[i])**2 + (ds.lat.data - lat[i])**2)
            A.append(nearest_neighbor)
    # make array of int from A
    A = np.array(A)
    # reverse order of A
    # A = A[::-1]
    ds_sel = []
    for i in range(len(A)):
        ds_sel.append(ds.isel(lon=A[i]).isel(lat=A[i]))
    ds_sel = xr.concat(ds_sel, dim='section')

    # ds_sel = ds.isel(lon=A).isel(lat=A)

    # %%
    #plot section in map
    plt.figure()
    plt.plot(ds_sel.lon, ds_sel.lat, 'k.')

    # %%
    a =ds_sel
    plt.pcolormesh(a.section, a.freq_time, a.transpose(), norm=colors.LogNorm(vmin=1e-3, vmax=1e2), cmap='plasma')
    plt.colorbar()
    plt.xlabel('sec')
    plt.ylabel('freq')
    # plt.title(f'{exp}, lon = {a.lon.data:.2f}')

    # %%
    ds = P_exp7_1h.where(mask01).isel(lat=110)
    a =ds
    plt.pcolormesh(a.lon, a.freq_time, a, norm=colors.LogNorm(vmin=1e-3, vmax=1e2), cmap='plasma')
    plt.colorbar()
    plt.xlabel('sec')
    plt.ylabel('freq')
    # plt.title(f'{exp}, 
    plt.figure()
    ds = P_exp7_1h.where(mask01).isel(lon=110)
    a =ds
    plt.pcolormesh(a.lat, a.freq_time, a, norm=colors.LogNorm(vmin=1e-3, vmax=1e2), cmap='plasma')
    plt.colorbar()
    plt.xlabel('sec')
    plt.ylabel('freq')


#%% ## Influence Seasonal Cycle #########
timesteps = data_exp7_1y.time.size
steps = math.floor(timesteps/4)

P_exp7_1h_S0 = fu.compute_spectra_uchida(data_exp7_1y.KE.fillna(0.).isel(time=slice(0,steps)), samp_freq=1)
# add string with begin and end date
P_exp7_1h_S1 = fu.compute_spectra_uchida(data_exp7_1y.KE.fillna(0.).isel(time=slice(steps,2*steps)), samp_freq=1)
P_exp7_1h_S2 = fu.compute_spectra_uchida(data_exp7_1y.KE.fillna(0.).isel(time=slice(2*steps,3*steps)), samp_freq=1)
P_exp7_1h_S3 = fu.compute_spectra_uchida(data_exp7_1y.KE.fillna(0.).isel(time=slice(3*steps,timesteps)), samp_freq=1)
P_exp7_1h_S0.attrs['time'] = f'{data_exp7_1y.time[0].data:.7} - {data_exp7_1y.time[steps].data:.7}'
P_exp7_1h_S1.attrs['time'] = f'{data_exp7_1y.time[steps].data:.7} - {data_exp7_1y.time[2*steps].data:.7}'
P_exp7_1h_S2.attrs['time'] = f'{data_exp7_1y.time[2*steps].data:.7} - {data_exp7_1y.time[3*steps].data:.7}'
P_exp7_1h_S3.attrs['time'] = f'{data_exp7_1y.time[3*steps].data:.7} - {data_exp7_1y.time[-1].data:.7}'
# %%
pos         = np.array([5, -32.5])
radius      = 5
mask_circle = (data_exp7_01.lon - pos[0])**2 + (data_exp7_01.lat - pos[1])**2 < radius**2
count       = np.sum(mask_circle)

# %% ################## Domain Sensitivity for D=0.1 ##################
if False:
    plt.figure()
    colors = plt.cm.Blues(np.linspace(0.3,1,10))
    for i in range(0, 10):
        radius = [0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9][i] 
        mask_circle = (data_exp7.lon - pos[0])**2 + (data_exp7.lat - pos[1])**2 < radius**2
        data = P_exp7.where(mask_circle)
        if i == 5: plt.loglog(data.freq_time[1:]*86400,   data.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 r = {radius}deg', color='tab:red')
        else: plt.loglog(data.freq_time[1:]*86400,   data.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 r = {radius}deg', color=colors[i])

    plt.grid()
    plt.legend(fontsize=8)
    plt.title('Domain Sensitivity for D=0.1')
    if savefig==True: plt.savefig(f'{fig_path}/domain_sensitivity.png')
# %% #compute averaged spectra for all experiments and depths 2h interval data
path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/' #high res
path_2_data = '/work/mh0033/m300878/spectra/KE/sa_1200m/' #low res large area

P7 = []
P8 = []
P9 = []
ni = 1
for i in range(2):
    eval_depth = dstings[i]
    data_exp7  = load_smt_spectra(7, eval_depth)
    data_exp8  = load_smt_spectra(8, eval_depth)
    data_exp9  = load_smt_spectra(9, eval_depth)
    P_exp7     = fu.compute_spectra_uchida(data_exp7.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=2)
    P_exp8     = fu.compute_spectra_uchida(data_exp8.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=2)
    P_exp9     = fu.compute_spectra_uchida(data_exp9.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=2)
    P7.append(P_exp7)
    P8.append(P_exp8)
    P9.append(P_exp9)

P7 = xr.concat(P7, dim='n')
P8 = xr.concat(P8, dim='n')
P9 = xr.concat(P9, dim='n')

P7_mean = P7.mean(dim='n')
P8_mean = P8.mean(dim='n')
P9_mean = P9.mean(dim='n')


# %% ############################### load observations ###############################
reload(fu)
ds_1, ds_2 = fu.load_mooring_data()
m_pos = [(ds_1.longitude.data, ds_1.latitude.data), (ds_2.longitude.data, ds_2.latitude.data)]
lat_mean = m_pos[0][1].mean()
# %% Mooring 1

if interp_gaps==True:
    ds_1_filled = ds_1.interpolate_na(dim='time', method='linear')
else: ds_1_filled = ds_1

if samp_freq == 2:
    subsample = 4
    ds_1_sub1 = ds_1_filled.isel(time=slice(0, None, 4))
    PP_ds1_1  = fu.compute_mooring_spectra(ds_1_sub1, t_window, samp_freq=samp_freq)
    ds_1_sub2 = ds_1_filled.isel(time=slice(1, None, 4))
    PP_ds1_2  = fu.compute_mooring_spectra(ds_1_sub2, t_window, samp_freq=samp_freq)
    ds_1_sub3 = ds_1_filled.isel(time=slice(2, None, 4))
    PP_ds1_3  = fu.compute_mooring_spectra(ds_1_sub3, t_window, samp_freq=samp_freq)
    ds_1_sub4 = ds_1_filled.isel(time=slice(3, None, 4))
    PP_ds1_4  = fu.compute_mooring_spectra(ds_1_sub4, t_window, samp_freq=samp_freq)
    PP_ds1    = xr.concat([PP_ds1_1, PP_ds1_2, PP_ds1_3, PP_ds1_4], dim='n')
    PP_ds1    = PP_ds1.mean(dim='n')
elif samp_freq == 1:
    subsample = 2
    ds_1_sub1 = ds_1_filled.isel(time=slice(0, None, 2))
    PP_ds1_1  = fu.compute_mooring_spectra(ds_1_sub1, t_window, samp_freq=samp_freq)
    ds_1_sub2 = ds_1_filled.isel(time=slice(1, None, 2))
    PP_ds1_2  = fu.compute_mooring_spectra(ds_1_sub2, t_window, samp_freq=samp_freq)
    PP_ds1    = xr.concat([PP_ds1_1, PP_ds1_2], dim='n')
    PP_ds1    = PP_ds1.mean(dim='n')
elif samp_freq == 'max' or samp_freq == 0.5:
    subsample = 1
    PP_ds1     = fu.compute_mooring_spectra(ds_1_filled, t_window, samp_freq=0.5)


# d0 = 0; dend = 1
# d0 = 1; dend = 9 #9 
if d ==1: d0 = 2; dend = 4
elif d ==0: d0 = 0; dend = 1
P_dmean_ds1                 = PP_ds1.isel(depth=slice(d0,dend)).mean(dim='depth')
P_dmean_ds1['nreal']        = t_window * (dend-d0) * subsample
P_dmean_ds1.attrs['depths'] = PP_ds1.depth[d0:dend].values

# %%
reload(fu)
# subsample every second timestetp

if interp_gaps==True:
    ds_2_filled = ds_2.interpolate_na(dim='time', method='linear')
else:
    ds_2_filled = ds_2

if samp_freq == 2:
    print('subsample to 2h and average spectra')
    subsample = 8 
    ds_2_sub1 = ds_2_filled.isel(time=slice(0, None, 8))
    PP_ds2_s1 = fu.compute_mooring_spectra(ds_2_sub1, t_window, samp_freq=samp_freq)
    ds_2_sub2 = ds_2_filled.isel(time=slice(1, None, 8))
    PP_ds2_s2 = fu.compute_mooring_spectra(ds_2_sub2, t_window, samp_freq=samp_freq)
    ds_2_sub3 = ds_2_filled.isel(time=slice(2, None, 8))
    PP_ds2_s3 = fu.compute_mooring_spectra(ds_2_sub3, t_window, samp_freq=samp_freq)
    ds_2_sub4 = ds_2_filled.isel(time=slice(3, None, 8))
    PP_ds2_s4 = fu.compute_mooring_spectra(ds_2_sub4, t_window, samp_freq=samp_freq)
    ds_2_sub5 = ds_2_filled.isel(time=slice(4, None, 8))
    PP_ds2_s5 = fu.compute_mooring_spectra(ds_2_sub5, t_window, samp_freq=samp_freq)
    ds_2_sub6 = ds_2_filled.isel(time=slice(5, None, 8))
    PP_ds2_s6 = fu.compute_mooring_spectra(ds_2_sub6, t_window, samp_freq=samp_freq)
    ds_2_sub7 = ds_2_filled.isel(time=slice(6, None, 8))
    PP_ds2_s7 = fu.compute_mooring_spectra(ds_2_sub7, t_window, samp_freq=samp_freq)
    ds_2_sub8 = ds_2_filled.isel(time=slice(7, None, 8))
    PP_ds2_s8 = fu.compute_mooring_spectra(ds_2_sub8, t_window, samp_freq=samp_freq)
    PP_ds2    = xr.concat([PP_ds2_s1, PP_ds2_s2, PP_ds2_s3, PP_ds2_s4, PP_ds2_s5, PP_ds2_s6, PP_ds2_s7, PP_ds2_s8], dim='n')
    PP_ds2    = PP_ds2.mean(dim='n')
elif samp_freq == 1:
    print('subsample to 1h and average spectra')
    subsample = 4
    ds_2_sub1 = ds_2_filled.isel(time=slice(0, None, 4))
    PP_ds2_s1 = fu.compute_mooring_spectra(ds_2_sub1, t_window, samp_freq=samp_freq)
    ds_2_sub2 = ds_2_filled.isel(time=slice(1, None, 4))
    PP_ds2_s2 = fu.compute_mooring_spectra(ds_2_sub2, t_window, samp_freq=samp_freq)
    ds_2_sub3 = ds_2_filled.isel(time=slice(2, None, 4))
    PP_ds2_s3 = fu.compute_mooring_spectra(ds_2_sub3, t_window, samp_freq=samp_freq)
    ds_2_sub4 = ds_2_filled.isel(time=slice(3, None, 4))
    PP_ds2_s4 = fu.compute_mooring_spectra(ds_2_sub4, t_window, samp_freq=samp_freq)
    PP_ds2    = xr.concat([PP_ds2_s1, PP_ds2_s2, PP_ds2_s3, PP_ds2_s4], dim='n')
    PP_ds2    = PP_ds2.mean(dim='n')
elif samp_freq == 0.5:
    print('subsample to 30min and average spectra')
    subsample = 2
    ds_2_sub1 = ds_2_filled.isel(time=slice(0, None, 2))
    PP_ds2_s1 = fu.compute_mooring_spectra(ds_2_sub1, t_window, samp_freq=samp_freq)
    ds_2_sub2 = ds_2_filled.isel(time=slice(1, None, 2))
    PP_ds2_s2 = fu.compute_mooring_spectra(ds_2_sub2, t_window, samp_freq=samp_freq)
    PP_ds2    = xr.concat([PP_ds2_s1, PP_ds2_s2], dim='n')
    PP_ds2    = PP_ds2.mean(dim='n')
    
elif samp_freq == 'max':
    print('no subsampling')
    subsample = 1
    PP_ds2 = fu.compute_mooring_spectra(ds_2, t_window, samp_freq=0.25)

if d ==1: d0 = 8; dend = 10
elif d ==0: d0 = 2; dend = 4
P_dmean_ds2                 = PP_ds2.isel(depth=slice(d0,dend)).mean(dim='depth')
P_dmean_ds2['nreal']        = t_window * (dend-d0) * subsample
P_dmean_ds2.attrs['depths'] = PP_ds2.depth[d0:dend].values

##################################### Visualize #####################################
radius = 10
mask01 = (data_exp7_01.lon - pos[0])**2 + (data_exp7_01.lat - pos[1])**2 < radius**2
# %%
reload(fu)
fname  = f'KE_{eval_depth}_{samp_freq}_{t_window}_radius{radius}d'

fu.plot_spectra_KE_1h(P_exp7_1h.where(mask01), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)

# %% Seasons
# %%
reload(fu)
fname  = f'KE_seasons'
fu.plot_spectra_KE_1h_seasons(P_exp7_1h_S0.where(mask01), P_exp7_1h_S1.where(mask01), P_exp7_1h_S2.where(mask01), P_exp7_1h_S3.where(mask01), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)

# %%
reload(fu)
fname =f'KE_{eval_depth}_subsampled_{t_window}'
fu.plot_spectra_KE_subsampeld(P_exp7_1h.where(mask_circle), P_exp7_2h.where(mask_circle), P_exp7_4h.where(mask_circle), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)

# %%
reload(fu)
radius = 10
mask03 = (data_exp7.lon - pos[0])**2 + (data_exp7.lat - pos[1])**2 < radius**2
mask01 = (data_exp7_01.lon - pos[0])**2 + (data_exp7_01.lat - pos[1])**2 < radius**2

fname  = f'KE_{eval_depth}_{t_window}_1h_radius{radius}d'
fu.plot_spectra_KE_new(P_exp7_1h.where(mask01), P_exp7.where(mask03), P_exp8.where(mask03), P_exp9.where(mask03), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)

# %% hopefully last update
reload(fu)
radius = 10
mask03 = (data_exp7.lon - pos[0])**2 + (data_exp7.lat - pos[1])**2 < radius**2
mask01 = (data_exp7_01.lon - pos[0])**2 + (data_exp7_01.lat - pos[1])**2 < radius**2

fname  = f'KE_{eval_depth}_{t_window}_1h_radius{radius}d_final2'
fu.plot_spectra_KE_new_haha(P_exp7_1h.where(mask01), P_r2b9.where(mask01), P_exp9.where(mask03), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)

# %% ############################# Evaluate all depths #############################
########################################################################################
path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/1h_Oct_Jan_depth/'
dstrings = ['_1.0','_47.400000000000006','_51.50000000000001','_55.80000000000001','_92.0', '_98.15','_104.6','_197.55', '_208.45000000000002','_219.75', '_1008.95', '_1990.4500000000005', '_3008.9000000000005','_3946.9000000000005']
depth = np.array([1.0, 47.4, 51.5, 55.8, 92.0, 98.15, 104.6, 197.55, 208.45, 219.75, 1008.95, 1990.45, 3008.9, 3946.9])
P=[]
for i in range(0, len(dstrings)):
    data   = load_smt_spectra(7, dstrings[i])
    P.append(data)
P = xr.concat(P, dim='depth')
P = P.assign_coords({'depth':depth})

# %%
P_exp7      = fu.compute_spectra_uchida(P.KE.fillna(0.), samp_freq=1)

# %%
reload(fu)
fname =f'KE_depths_{samp_freq}_{t_window}'
# fu.plot_spectra_KE_1h_depths(P_exp7.where(mask_circle), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)
fu.plot_spectra_KE_1h_depths(P_exp7, P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)

########################################################################################
# %% ######### Plot spectra 2h ##############
reload(fu)
fname    = f'KE_spectra_dmean_580_e79_alexa'
lat_mean = -32.5
n = 1
count=10
fu.plot_spectra_KE(P_exp7.where(mask03), P_exp8.where(mask03), P_exp9.where(mask03), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)


# %% ############################# Eddy Spectra ########################################
if False:
    reload(fu)
    ds_exp7, ds_exp9 = fu.load_eddy_KE_spectra()
    #rename coordinate x to lon and y to lat
    ds_exp7 = ds_exp7.KE.rename({'x':'lon'}).rename({'y':'lat'})
    ds_exp9 = ds_exp9.KE.rename({'x':'lon'}).rename({'y':'lat'})

    # %%
    reload(eva)
    P_exp7_eddy = eva.compute_spectra_uchida(ds_exp7.fillna(0.), samp_freq=2)
    P_exp9_eddy = eva.compute_spectra_uchida(ds_exp9.fillna(0.), samp_freq=2)

    # %%
    reload(fu)
    fname = 'background_eddy_spectra_KE'
    fu.plot_spectra_KE_vs_eddy(P_exp7_eddy, P_exp9_eddy, P_exp7.where(mask03), P_exp8.where(mask03), P_exp9.where(mask03), P_dmean_ds1, P_dmean_ds2, lat_mean, fig_path, fname, savefig)

# %% ###########################################################################
