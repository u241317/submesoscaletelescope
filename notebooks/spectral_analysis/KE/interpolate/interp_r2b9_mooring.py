# %%
import sys
import glob, os
import pyicon as pyic

sys.path.insert(0, '../../../../')
import smt_modules.all_funcs as eva
import smt_modules.tools as tools
sys.path.insert(0, '../../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster_gpu(walltime='01:00:00')
cluster
client

# %%
reload(eva)

ds = eva.load_r2b9('outdata_2d_lev_0')
#%%
var='w'
path_2_data = f'/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/r2b9/all/{var}/'
fname       = f'r2b9_spectra_hor_KE_10deg_{var}'
if var=='ke':
    reload(fu)
    fu.calc_interp_KE_r2b9(ds.u, ds.v, fname, path_2_data)
else:
    fu.calc_interp_var_r2b9(ds.w, fname, path_2_data)

# %%
client.close()
cluster.close()