# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
import math

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 


# %%
fig_path    = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/KE/obs/'
savefig     = False
path_2_data = '/work/mh0033/m300878/spectra/KE/sa_600m/'
# %%
reload(fu)
ds_1, ds_2 = fu.load_mooring_data()
m_pos = [(ds_1.longitude.data, ds_1.latitude.data), (ds_2.longitude.data, ds_2.latitude.data)]

lat_mean = m_pos[0][1].mean()
# %%
if False: #first glance at spectra
    data_obs = ds_2
    idepth   = 0
    KE_obs   = data_obs.isel(depth=idepth).ucur**2 / 2 + data_obs.isel(depth=idepth).vcur**2 /2
    # KE_obs   = KE_obs.isel(longitude=0, latitude=0)
    P_obs    = fu.compute_spectra_uchida(KE_obs.fillna(0.), samp_freq=0.5)
    plt.loglog(P_obs.freq_time[1:]*86400, P_obs[1:], label='obs')

    KE_obs['time'] = np.arange(int(KE_obs.time.size))*3600*0.5

    f, P = signal.welch(KE_obs.fillna(0.), fs=1/(3600*0.5), window='hann', nperseg=1024, noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=-1)
    plt.loglog(f*86400,P)
    plt.legend()

    # %% plot Timeseries of Mooring
    a = ds_1.isel(depth=3).ucur#**2 / 2 + ds_1.isel(depth=3).vcur**2 /2
    b = ds_2.isel(depth=7).ucur#**2 / 2 + ds_2.isel(depth=7).vcur**2 /2
    # c = data_exp7.KE.isel(lon=310, lat=300)
    # d = data_exp8.KE.isel(lon=310, lat=300)
    # e = data_exp9.KE.isel(lon=300, lat=300)

    # mean over lon
    # c = data_exp7.KE.mean(['lon','lat'], skipna=True)
    # d = data_exp8.KE.mean(['lon','lat'], skipna=True)
    # e = data_exp9.KE.mean(['lon','lat'], skipna=True)

    #low pass filter
    a = a.rolling(time=24*5, center=True).mean()
    b = b.rolling(time=24*5, center=True).mean()
    # c = c.rolling(time=24*3, center=True).mean()
    # d = d.rolling(time=24*3, center=True).mean()
    # e = e.rolling(time=24*3, center=True).mean()

    plt.plot(np.arange(0,a.time.size/2, 0.5), a, label='mooring 1')
    plt.plot(np.arange(0,b.time.size/4, 0.25), b, label='mooring 2')
    # plt.plot(np.arange(0,c.time.size*2, 2), c, label='exp7')
    # plt.plot(np.arange(0,d.time.size*2, 2), d, label='exp8')
    # plt.plot(np.arange(0,e.time.size*2, 2), e, label='exp9')
    plt.legend()


# %% plot welch spectra
if False:
    ds_obs = ds_1
    P = []
    for i in range(ds_obs.depth.size):
        idepth = i
        KE_obs = ds_obs.isel(depth=idepth).ucur**2 / 2 + ds_obs.isel(depth=idepth).vcur**2 /2
        # KE_obs = ds_obs.KE.isel(depth=idepth)
        f, p   = signal.welch(KE_obs.fillna(0.), fs=1/(3600*0.5), window='hann', nperseg=1024, noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=-1)
        P.append(p)

    # concat numpy array
    P     = np.array(P)

    reload(fu)
    lat_mean = -40
    fname    = 'obs_spectra_m1'
    fu.plot_obs_spectra(ds_obs, f, P, lat_mean, fig_path, fname, savefig)



# %%
if False:
    # season selection
    ds_1_sel1 = ds_1.sel(time=slice('2021-07-01', '2021-10-31'))
    ds_1_sel2 = ds_1.sel(time=slice('2022-07-01', '2022-10-31'))

    # ds_1_sel1 = ds_1.sel(time=slice('2022-02-01', '2022-04-30'))
    # ds_1_sel2 = ds_1.sel(time=slice('2023-02-01', '2023-04-30'))

    ds_1_sel1.ucur.isel(depth=3).plot()
    ds_1_sel2.ucur.isel(depth=3).plot()

    t_window = 5
    PP_ds1_1                      = fu.compute_mooring_spectra(ds_1_sel1, t_window, samp_freq=0.5)
    PP_ds1_2                      = fu.compute_mooring_spectra(ds_1_sel2, t_window, samp_freq=0.5)

    PP_ds1 = xr.concat([PP_ds1_1, PP_ds1_2], dim='n')
    #average
    PP_ds1 = PP_ds1.mean(dim='n')
    d0 = 1; dend = 9
    P_dmean_ds1                 = PP_ds1_1.isel(depth=slice(d0,dend)).mean(dim='depth')
    P_dmean_ds1['nreal']        = t_window * (dend-d0)
    P_dmean_ds1.attrs['depths'] = PP_ds1.depth[d0:dend].values

# %% Mooring 1
t_window = 40#100
if True:
    ds_1_filled = ds_1.interpolate_na(dim='time', method='linear')
    PP_ds1      = fu.compute_mooring_spectra(ds_1_filled, t_window, samp_freq=0.5)
else:
    PP_ds1      = fu.compute_mooring_spectra(ds_1, t_window, samp_freq=0.5)

d0 = 0; dend = 1
d0 = 1; dend = 9 #9 
d0 = 2; dend = 4
P_dmean_ds1                 = PP_ds1.isel(depth=slice(d0,dend)).mean(dim='depth')
P_dmean_ds1['nreal']        = t_window * (dend-d0)
P_dmean_ds1.attrs['depths'] = PP_ds1.depth[d0:dend].values

# %%

def average_2_levels(data, sums):
    d0   = 0
    dend = sums
    P    = []
    size = data.depth.size
    n    = size // sums
    for i in range(n):
        p                 = data.isel(depth=slice(d0, dend)).mean(dim='depth')
        p.attrs['depths'] = data.depth.values
        p.attrs['range']  = sums
        P.append(p)
        d0 += sums
        dend += sums
    P = xr.concat(P, dim='depth_levs')
    return P

P1_mean = average_2_levels(PP_ds1, 4)

# %%
reload(fu)
# subsample every second timestetp

if True:
    ds_2_filled = ds_2.interpolate_na(dim='time', method='linear')
else:
    ds_2_filled = ds_2

samp_freq = 0.5
d0 = 8; dend = 10
if samp_freq == 1:
    print('subsample to 1h and average spectra')
    ds_2_sub1 = ds_2_filled.isel(time=slice(0, None, 4))
    PP_ds2_s1 = fu.compute_mooring_spectra(ds_2_sub1, t_window, samp_freq=samp_freq)
    ds_2_sub2 = ds_2_filled.isel(time=slice(1, None, 4))
    PP_ds2_s2 = fu.compute_mooring_spectra(ds_2_sub2, t_window, samp_freq=samp_freq)
    ds_2_sub3 = ds_2_filled.isel(time=slice(2, None, 4))
    PP_ds2_s3 = fu.compute_mooring_spectra(ds_2_sub3, t_window, samp_freq=samp_freq)
    ds_2_sub4 = ds_2_filled.isel(time=slice(3, None, 4))
    PP_ds2_s4 = fu.compute_mooring_spectra(ds_2_sub4, t_window, samp_freq=samp_freq)
    PP_ds2    = xr.concat([PP_ds2_s1, PP_ds2_s2, PP_ds2_s3, PP_ds2_s4], dim='n')
    PP_ds2    = PP_ds2.mean(dim='n')

    P_dmean_ds2_sub1                 = PP_ds2.isel(depth=slice(d0,dend)).mean(dim='depth')
    P_dmean_ds2_sub1['nreal']        = t_window * (dend-d0)
    P_dmean_ds2_sub1.attrs['depths'] = PP_ds2.depth[d0:dend].values
elif samp_freq == 0.5:
    ds_2_sub1 = ds_2_filled.isel(time=slice(0, None, 2))
    PP_ds2_s1 = fu.compute_mooring_spectra(ds_2_sub1, t_window, samp_freq=samp_freq)
    ds_2_sub2 = ds_2_filled.isel(time=slice(1, None, 2))
    PP_ds2_s2 = fu.compute_mooring_spectra(ds_2_sub2, t_window, samp_freq=samp_freq)
    PP_ds2    = xr.concat([PP_ds2_s1, PP_ds2_s2], dim='n')
    PP_ds2    = PP_ds2.mean(dim='n')
    P_dmean_ds2_sub05                 = PP_ds2.isel(depth=slice(d0,dend)).mean(dim='depth')
    P_dmean_ds2_sub05['nreal']        = t_window * (dend-d0)
    P_dmean_ds2_sub05.attrs['depths'] = PP_ds2.depth[d0:dend].values
else:
    PP_ds2 = fu.compute_mooring_spectra(ds_2, t_window, samp_freq=0.25)

    P_dmean_ds2                 = PP_ds2.isel(depth=slice(d0,dend)).mean(dim='depth')
    P_dmean_ds2['nreal']        = t_window * (dend-d0)
    P_dmean_ds2.attrs['depths'] = PP_ds2.depth[d0:dend].values

# %%
P2_mean = average_2_levels(PP_ds2, 8)

# %%
reload(fu)
fu.plot_mooring_spectra_levs_both_sub(P_dmean_ds2, P_dmean_ds2_sub05, P_dmean_ds2_sub1, lat_mean, fig_path, savefig)

# %%
reload(fu)
fu.plot_mooring_spectra_levs(PP_ds2, f'M2_{t_window}_sub', lat_mean, fig_path, savefig)

# %%
reload(fu)
fu.plot_mooring_spectra_levs_both(PP_ds1, PP_ds2, lat_mean, fig_path, savefig)

# %%
reload(fu)
fu.plot_mooring_spectra_levs_ave(P1_mean, P2_mean, lat_mean, fig_path, savefig)

# %%
