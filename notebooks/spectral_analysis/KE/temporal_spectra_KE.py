
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
########################## OUTDATED ##########################  
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#

# uses my own from scratch calculation of spectra
# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='03:00:00')
cluster
client

# %%
############## plot config ##############
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/KE/'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'

fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'
lon_reg = np.array([-40, 30])
lat_reg = np.array([-55, -15])

# zoom1
lon_reg1 = np.array([-5, 10])
lat_reg1 = np.array([-42, -32])
lon_reg2 = np.array([-2, 0.5])
lat_reg2 = np.array([-38, -36.75])

savefig = False


# %% smt wave with tides
begin = '2019-07-06T13:15:00'
end   = '2019-08-07T06:15:00'
idepth = 17

#################### vertical KE ####################
if False:
# %% vertical velocity over several depths
    path_2_data = '/work/mh0033/m300878/spectra/'
    if False:
        reload(eva)
        ds_w = eva.load_smt_wave_all('w', exp=7, remove_bnds=True)
        ds_w = ds_w.w.isel(depth=slice(1,20))
        ds_w = ds_w.drop('clon').drop('clat')

        data = ds_w**2 /2 
        data = data.sel(time=slice(begin, end)) # same timeframe
        # data = data.isel(depth=slice(1, 10))

        data_interp_tides  = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg1, lat_reg=lat_reg1)
        data_interp_tides.to_netcdf(f'{path_2_data}/smtwv0007_spectra_KE_w_2d.nc')
    else:
        data_interp_tides = xr.open_dataset(f'{path_2_data}/smtwv0007_spectra_KE_w_2d.nc')
        data_interp_tides = data_interp_tides.rename(w='KE')

    # %%
    islice = data_interp_tides.depth.size
    idx=-1
    for i in range(0, islice):
        idx += 1
        f, Pxx, lat_mean = fu.calc_spectra(data_interp_tides.KE.isel(depth=i), samp_freq=12)
        if idx == 0:
            spectra_2d = np.zeros((len(f), islice))
        spectra_2d[:,idx] = Pxx
        print(i, 'done')
    # %
    reload(fu)
    fu.plot_2d_spectra(f, spectra_2d, lat_mean, fig_path, savefig=savefig)


# %% vertical velocity
path_2_data = '/work/mh0033/m300878/spectra/'
if False:
    reload(eva)
    ds_w = eva.load_smt_wave_all('w', exp=7, remove_bnds=True)
    ds_w = ds_w.isel(depth=idepth).w
    ds_w = ds_w.drop('clon').drop('clat')
     
    data = ds_w**2 /2 
    data = data.sel(time=slice(begin, end)) # same timeframe
    # data = data.isel(depth=slice(1, 10))

    data_interp_tides  = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg1, lat_reg=lat_reg1)
    data_interp_tides.to_netcdf(f'{path_2_data}/smtwv0007_spectra_KE_w.nc')
else:
    data_interp_tides = xr.open_dataset(f'{path_2_data}/smtwv0007_spectra_KE_w.nc')
    data_interp_tides = data_interp_tides.rename(w='KE')
# %%
if False:
    reload(eva)
    ds_w = eva.load_smt_wave_9('w', it=2)
    ds_w = ds_w.isel(depth=idepth).w

    data = ds_w**2 /2 
    data = data.sel(time=slice(begin, end)) # same timeframe

    data_interp_notides  = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg1, lat_reg=lat_reg1)
    data_interp_notides.to_netcdf(f'{path_2_data}/smtwv0009_spectra_KE_w.nc')
else:
    data_interp_notides = xr.open_dataset(f'{path_2_data}/smtwv0009_spectra_KE_w.nc')
    data_interp_notides = data_interp_notides.rename(w='KE')

# %%
#################### horizontal KE ####################

path_2_data = '/work/mh0033/m300878/spectra/'
if False:
    reload(eva)
    ds_u = eva.load_smt_wave_all('u', exp=7, remove_bnds=True)
    ds_u = ds_u.isel(depth=idepth).u
    ds_u = ds_u.drop('clon').drop('clat')
    
    ds_v = eva.load_smt_wave_all('v', exp=7, remove_bnds=True)
    ds_v = ds_v.isel(depth=idepth).v
    ds_v = ds_v.drop('clon').drop('clat')
    
    data = ds_u**2 /2 + ds_v**2 /2
    data = data.sel(time=slice(begin, end)) # same timeframe
    data_interp_tides  = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg1, lat_reg=lat_reg1)
    data_interp_tides.to_netcdf(f'{path_2_data}/smtwv0007_spectra_KE_sel.nc')
else:
    data_interp_tides = xr.open_dataset(f'{path_2_data}/smtwv0007_spectra_KE_sel.nc')
    data_interp_tides = data_interp_tides.rename(__xarray_dataarray_variable__='KE')


# %% smt wave no tides

# %% 
path_2_data = '/work/mh0033/m300878/spectra/'
if False:
    reload(eva)
    ds_u = eva.load_smt_wave_9('u', it=2)
    ds_u = ds_u.isel(depth=idepth).u

    ds_v = eva.load_smt_wave_9('v', it=2)
    ds_v = ds_v.isel(depth=idepth).v

    data = ds_u**2 /2 + ds_v**2 /2
    data = data.sel(time=slice(begin, end)) # same timeframe

    data_interp_notides  = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg1, lat_reg=lat_reg1)
    data_interp_notides.to_netcdf(f'{path_2_data}/smtwv0009_spectra_KE_sel.nc')
else:
    data_interp_notides = xr.open_dataset(f'{path_2_data}/smtwv0009_spectra_KE_sel.nc')
    data_interp_notides = data_interp_notides.rename(__xarray_dataarray_variable__='KE')

#########################################################################################

if False:
    # %%
    reload(fu)
    f, Pxx, lat_mean = fu.calc_spectra(data_interp_tides, samp_freq=12)

    # %%
    reload(fu)
    var = 'KE'
    fu.plot_spectra(f, Pxx, lat_mean, fig_path, var, savefig=savefig)


    # %%
    f1, Pxx1, lat_mean = fu.calc_spectra(data_interp_notides.KE, samp_freq=12)
    f2, Pxx2, lat_mean = fu.calc_spectra(data_interp_tides.KE, samp_freq=12)

    # %%
    reload(fu)
    var = 'KE'
    fu.plot_spectra_tides_notides_KE(f1, Pxx1, f2, Pxx2, lat_mean, fig_path, var, savefig=savefig)
    # %%
    reload(fu)
    var = 'vertical_KE'
    fu.plot_spectra_tides_notides_KE_w(f1, Pxx1, f2, Pxx2, lat_mean, fig_path, var, savefig=savefig)
    # %%
