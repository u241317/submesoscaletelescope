#%%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
import math

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

print('use compute node - memoeory usage')
# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
cluster
client

# %%
fig_path    = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/KE/mod_obs'
savefig     = False
# %%
root = '/work/mh0033/m300878/spectra/KE/sa_1200m/'

name07 = 'smtwv0007_spectra_hor_KE_all_98.15.nc'
name08 = 'smtwv0008_spectra_hor_KE_all_98.15.nc'
#%%
ds7 = xr.open_dataset(root + name07).drop('clon').drop('clat').__xarray_dataarray_variable__
ds8 = xr.open_dataset(root + name08).drop('clon').drop('clat').__xarray_dataarray_variable__
# %%
pos    = np.array([5, -32.5])
radius = 10

mask03 = (ds8.lon - pos[0])**2 + (ds8.lat - pos[1])**2 < radius**2

# %%
ds7g = ds7.groupby('time.month').groups[10]
ds7g = ds7.isel(time=ds7g)
ds8g = ds8.groupby('time.month').groups[2]
ds8g = ds8.isel(time=ds8g)

P7    = fu.compute_spectra_uchida(ds7g.fillna(0.), samp_freq=2)
P8    = fu.compute_spectra_uchida(ds8.fillna(0.), samp_freq=2)

P7    = P7.where(mask03)
P8    = P8.where(mask03)

lat_mean = P7.lat.mean().values
# %%
reload(fu)

fname ='seasons_10deg'
fu.plot_spectra_KE_season(P7, P8, lat_mean, fname, fig_path,  savefig)

# %%
reload(eva)
ds_tides_tmean = eva.load_smt_wave_2d_exp7_07(it=1)
begin          = '2019-07-09T00:00:00'
end            = '2019-07-18T23:15:00'
p1             = ds_tides_tmean.sel(time=slice(begin, end))
p1             = p1.drop_duplicates('time')
p1             = p1.isel(time=slice(0,239)) #  smtwv0007_oce_2d_PT1H_20190718T231500Z.nc defect
p1             = p1.drop('clon').drop('clat')

# %%
reload(eva)
ds_tides_tmean_2 = eva.load_smt_wave_2d_exp7_07_part2(it=1)
begin            = '2019-07-19T00:00:00'
end              = '2019-08-31T23:15:00'
p2               = ds_tides_tmean_2.sel(time=slice(begin, end))
p2               = p2.drop('clon').drop('clat')

# %%
ds_tides_tmean_3 = eva.load_smt_wave_2d_exp7_07_part3(it=1)
begin            = '2019-09-01T00:00:00'
end              = '2020-06-13T23:15:00'
p3               = ds_tides_tmean_3.sel(time=slice(begin, end))
# %%
pke1 = p1.u**2 + p1.v**2
pke2 = p2.u**2 + p2.v**2
pke3 = p3.u**2 + p3.v**2


exp7 = xr.concat([pke1, pke2, pke3], dim='time')
exp7 = exp7.drop_duplicates('time')

# %%
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'
lon_reg       = [-5,15]
lat_reg       = [-42.5,-22.5]
data          = pyic.interp_to_rectgrid_xr(exp7.isel(depth=1), fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

data.to_netcdf('/work/mh0033/m300878/spectra/KE/sa_1200m/smtwv0007_spectra_hor_KE_10deg_50m_1y_1h.nc')

#%%load
data = xr.open_dataset('/work/mh0033/m300878/spectra/KE/sa_1200m/smtwv0007_spectra_hor_KE_10deg_50m_1y_1h.nc').__xarray_dataarray_variable__


# %%
mask03 = (data.lon - pos[0])**2 + (data.lat - pos[1])**2 < radius**2
data.where(mask03).isel(time=5600).plot()
# %%
reload(fu)
data_g = data.groupby('time.month')

dseasons = []
for i in np.arange(12):
    sel = data_g.groups[i+1]
    data_sel = data.isel(time=sel)
    P = fu.compute_spectra_uchida(data_sel.fillna(0.), samp_freq=1)
    dseasons.append(P)
# dseasons = xr.concat(dseasons, dim='month')

#%%
lat_mean = data.lat.mean().values
#%%
reload(fu)
fname ='seasons_10deg2'
fu.plot_spectra_KE_season(dseasons,  lat_mean, fname, fig_path,  savefig)

# %%
reload(fu)
data_g = data.groupby('time.month')

jfm = data_g.groups[1] + data_g.groups[2] + data_g.groups[3]
amj = data_g.groups[4] + data_g.groups[5] + data_g.groups[6]
jas = data_g.groups[7] + data_g.groups[8] + data_g.groups[9]
ond = data_g.groups[10] + data_g.groups[11] + data_g.groups[12]

seas = [jfm, amj, jas, ond]

dseasons = []
for i in np.arange(4):
    data_sel = data.isel(time=seas[i])
    P = fu.compute_spectra_uchida(data_sel.fillna(0.), samp_freq=1)
    dseasons.append(P)
# %%
reload(fu)
fname ='seasons_10deg'
fu.plot_spectra_KE_season2(dseasons,  lat_mean, fname, fig_path,  savefig)

# %%
