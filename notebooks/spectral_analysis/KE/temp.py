# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw
import math

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

print('use compute node - memoeory usage')
# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
cluster
client

# %%
fig_path    = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/KE/mod_obs'
savefig     = False
# %% compute spectra at single depth for EXP7
def load_smt_spectra(exp, eval_depth):
    fname     = f'smtwv000{exp}_spectra_hor_KE_all{eval_depth}'
    # fname     = f'smtwv000{exp}_spectra_hor_KE_10deg{eval_depth}'
    data_exp  = xr.open_dataset(f'{path_2_data}/{fname}.nc')
    data_exp  = data_exp.rename(__xarray_dataarray_variable__='KE')
    # data_exp  = data_exp.drop('clon').drop('clat')
    return data_exp


# %% ######################## Configuration ################
interp_gaps = True
t_window   = 30#100
samp_freq  = 1
pos         = np.array([5, -32.5])

# dstrings    = ['_1.0','_51.50000000000001','_98.15','_208.45000000000002','_1008.95', '_1990.4500000000005', '_3008.9000000000005','_3946.9000000000005']
dstrings    = ['_1.0','_51.50000000000001','_98.15','_1008.95', '_1990.4500000000005', '_3008.9000000000005','_3946.9000000000005']


#%%     ######################### load 1h interval data ############################
path_2_data  = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/1h_oct_jul/'
ni           = 1
P = []
for eval_depth in dstrings:
    print(eval_depth)
    fname        = f'smtwv0007_spectra_hor_KE_10deg{eval_depth}'
    data_exp7_02 = xr.open_dataset(f'{path_2_data}/{fname}.nc')
    data_exp7_02 = data_exp7_02.rename(__xarray_dataarray_variable__='KE')
    data_exp7_02 = data_exp7_02.transpose('time', 'lat', 'lon')
    P_exp7       = fu.compute_spectra_uchida(data_exp7_02.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=1)
    print(P_exp7.shape)
    P.append(P_exp7.compute())
P_exp7_1h_depths = xr.concat(P, dim='depth')

P_exp7_1h_depths = P_exp7_1h_depths.compute()
# %%  ######################### load 1h R2B9 data ############################
path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/r2b9/all/ke/'
fname       = f'r2b9_spectra_hor_KE_10deg_ke_month_*'
flist      = glob.glob(f'{path_2_data}/{fname}')
flist.sort()

data_r2b9 = xr.open_mfdataset(flist, combine='nested', concat_dim='time')
data_r2b9 = data_r2b9.rename(__xarray_dataarray_variable__='KE')
data_r2b9 = data_r2b9.sortby('time')
# rechunk for continious time
# data_r2b9 = data_r2b9.chunk({'time':-1})
int_sel = [0,2,5,8,10,11,12,13] #same like smt
data_r2b9 = data_r2b9.isel(depth=int_sel)
data_r2b9 = data_r2b9.compute()


data_r2b9   = data_r2b9.where(np.isfinite(data_r2b9), np.nan)
# select same time period as exp7
data_r2b9   = data_r2b9.sel(time=slice(data_exp7_02.time[0], data_exp7_02.time[-1]))
P_r2b9      = []
for depth in range(data_r2b9.depth.size):
    print(depth)
    P      = fu.compute_spectra_uchida(data_r2b9.isel(depth=depth).KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)).compute(), samp_freq=1)
    P_r2b9.append(P)
P_r2b9 = xr.concat(P_r2b9, dim='depth')

P_r2b9 = P_r2b9.compute()

# %% #compute averaged spectra for all experiments and depths 2h interval data
# path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/' #high res
path_2_data = '/work/mh0033/m300878/spectra/KE/sa_1200m/' #low res large area


P9 = []
ni = 1
for i in [1,2]:
    eval_depth = dstrings[i]
    data_exp9  = load_smt_spectra(9, eval_depth)
    P_exp9     = fu.compute_spectra_uchida(data_exp9.KE.fillna(0.).isel(lon=slice(0,None,ni), lat=slice(0,None,ni)), samp_freq=2)
    P9.append(P_exp9)

P9 = xr.concat(P9, dim='depth')

# P9_mean = P9.mean(dim='n')


# %% ############################### load observations ###############################
reload(fu)
PP_ds1, PP_ds2 = fu.load_obs_spectra(samp_freq, t_window)

reload(fu)
P1_mean = fu.average_2_levels(PP_ds1, 4)
P2_mean = fu.average_2_levels(PP_ds2, 8)


#%%

def calc_dmean(PP_ds1, PP_ds2, d):
    if d ==1: d0 = 2; dend = 4
    elif d ==0: d0 = 0; dend = 1
    P_dmean_ds1                 = PP_ds1.isel(depth=slice(d0,dend)).mean(dim='depth')
    P_dmean_ds1['nreal']        = t_window * (dend-d0)
    P_dmean_ds1.attrs['depths'] = PP_ds1.depth[d0:dend].values
    if d ==1: d0 = 8; dend = 10
    elif d ==0: d0 = 2; dend = 4
    P_dmean_ds2                 = PP_ds2.isel(depth=slice(d0,dend)).mean(dim='depth')
    P_dmean_ds2['nreal']        = t_window * (dend-d0)
    P_dmean_ds2.attrs['depths'] = PP_ds2.depth[d0:dend].values
    return P_dmean_ds1, P_dmean_ds2

P1_50m, P2_50m = calc_dmean(PP_ds1, PP_ds2, 0)
P1_100m, P2_100m = calc_dmean(PP_ds1, PP_ds2, 1)

P1_mean = xr.concat([P1_50m, P1_100m], dim='depth_levs')
P2_mean = xr.concat([P2_50m, P2_100m], dim='depth_levs')


##################################### Visualize #####################################
# %% hopefully last update
count=10
lat_mean = pos[1]

reload(fu)
radius = 10
mask03 = (data_exp9.lon - pos[0])**2 + (data_exp9.lat - pos[1])**2 < radius**2
mask01 = (data_exp7_02.lon - pos[0])**2 + (data_exp7_02.lat - pos[1])**2 < radius**2
#%%
fname  = f'KE_{eval_depth}_{t_window}_1h_radius{radius}d_final2'
fu.plot_spectra_KE_new_haha(P_exp7_1h_depths.isel(depth=0).where(mask01), P_r2b9.isel(depth=0).where(mask01), P_exp9.where(mask03), P1_mean.isel(depth_levs=2), P2_mean.isel(depth_levs=2), lat_mean, count, fig_path, fname, savefig)

#%% all depths together
reload(fu)
path_2_data = path_2_data = '/work/mh0033/m300878/spectra/KE/all_depths/'

if True:
    #save data
    P_exp7_1h_depths.to_netcdf(f'{path_2_data}/P_exp7_1h_depths.nc')
    P_r2b9.to_netcdf(f'{path_2_data}/P_r2b9.nc')
    P9.to_netcdf(f'{path_2_data}/P9.nc')
    mask01.to_netcdf(f'{path_2_data}/mask01.nc')
    mask03.to_netcdf(f'{path_2_data}/mask03.nc')
else:
    #load data
    P_exp7_1h_depths = xr.open_dataset(f'{path_2_data}/P_exp7_1h_depths.nc')
    P_r2b9 = xr.open_dataset(f'{path_2_data}/P_r2b9.nc')
    P9 = xr.open_dataset(f'{path_2_data}/P9.nc')

#%%
reload(fu)
# fu.plot_KE_spec_depths(P_exp7_1h_depths.where(mask01.transpose()), P_r2b9.where(mask01), P9.where(mask03), P1_mean, P2_mean, lat_mean, fig_path, fname, savefig)

# fu.plot_KE_spec_depths_2(P_exp7_1h_depths.where(mask01.transpose()), P_r2b9.where(mask01), P9.where(mask03), P1_mean, P2_mean, lat_mean, fig_path, fname, savefig)
fname  = f'KE_{eval_depth}_{t_window}_1h_radius{radius}d_all_depth'
fu.plot_KE_spec_depths_2(P_exp7_1h_depths.where(mask01.transpose()), P_r2b9.where(mask01), P9.where(mask03), P1_mean, P2_mean, lat_mean, fig_path, fname, savefig)


# %% ############################# Evaluate all depths #############################
########################################################################################
path_2_data = '/work/mh0033/m300878/spectra/KE/5degree_mooring_pos/1h_Oct_Jan_depth/'
dstrings = ['_1.0','_47.400000000000006','_51.50000000000001','_55.80000000000001','_92.0', '_98.15','_104.6','_197.55', '_208.45000000000002','_219.75', '_1008.95', '_1990.4500000000005', '_3008.9000000000005','_3946.9000000000005']
depth = np.array([1.0, 47.4, 51.5, 55.8, 92.0, 98.15, 104.6, 197.55, 208.45, 219.75, 1008.95, 1990.45, 3008.9, 3946.9])
P=[]
for i in range(0, len(dstrings)):
    data   = load_smt_spectra(7, dstrings[i])
    P.append(data)
P = xr.concat(P, dim='depth')
P = P.assign_coords({'depth':depth})

# %%
P_exp7      = fu.compute_spectra_uchida(P.KE.fillna(0.), samp_freq=1)

# %%
reload(fu)
fname =f'KE_depths_{samp_freq}_{t_window}'
# fu.plot_spectra_KE_1h_depths(P_exp7.where(mask_circle), P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)
fu.plot_spectra_KE_1h_depths(P_exp7, P_dmean_ds1, P_dmean_ds2, lat_mean, count, fig_path, fname, savefig)

########################################################################################
