# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='00:30:00')
cluster
client

# %%
############## plot config ##############
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/'
# fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'

fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'
lon_reg = np.array([-40, 30])
lat_reg = np.array([-55, -15])

# zoom1
lon_reg1 = np.array([-5, 10])
lat_reg1 = np.array([-42, -32])
lon_reg2 = np.array([-2, 0.5])
lat_reg2 = np.array([-38, -36.75])

savefig = False


    # %% open exp7 data
if False:
    exp       = 7
    variable  = '2d'
    folder    = f'smtwv000{exp}/outdata_{variable}'
    path      = f'/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4/experiments/{folder}'
    path_data = f'{path}/smtwv000{exp}_oce_2d_PT1H_2019*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()
    # flist  = flist[288:1344]
    flist = flist[221:1372] #drop first 2 weeks and linked data >> only july, august
    chunks = dict(time=1)

    ds1 = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds1 = xr.decode_cf(ds1) #restores timestamp

    #%%
    ds1 = ds1.drop('clon').drop('clat').drop('vlon').drop('vlat')

    # %%
    path      = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv0007/outdata_2d'
    path_data = f'{path}/smtwv000{exp}_oce_2d_PT1H_20*.nc'
    flist     = np.array(glob.glob(path_data))
    flist.sort()

    chunks = dict(time=1)

    ds2 = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=True, chunks=chunks, decode_cf=False) #decode_cf speeds up by 100 times but destroy timestamp
    ds2 = xr.decode_cf(ds2) #restores timestamp

    # %% concat over time
    ds = xr.concat([ds1, ds2], dim='time')
    tstring = 'June_2019-January_2020'

    exp7 = ds.zos
    # %%
    reload(fu)
    path_2_data = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'
    fname       = f'smtwv0007_spectra_ssh_{tstring}.nc'
    data        = exp7
    fu.select_interpolate_save(data, data_r, path_2_data, fname)

# %% lOAD R2B9
reload(fu)
if True:
    ds_r2b9     = eva.load_r2b9_ssh()
    path_2_data = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'
    size        = ds_r2b9.time.size
    for i in np.arange(10):
        fname       = f'/r2b9/r2b9_ssh_tseries_{i}.nc'
        data        = ds_r2b9.isel(time=slice(i*size//10, (i+1)*size//10))
        fu.select_interpolate_save_r2b9(data, path_2_data, fname)



#%% calc
if True:
    # %% smt wave with tides
    reload(eva)
    ds_tides_tmean = eva.load_smt_wave_2d_exp7_07(it=1)
    begin          = '2019-07-09T00:00:00'
    end            = '2019-07-18T23:15:00'
    p1             = ds_tides_tmean.zos.sel(time=slice(begin, end))
    p1             = p1.drop_duplicates('time')
    p1             = p1.isel(time=slice(0,239)) #  smtwv0007_oce_2d_PT1H_20190718T231500Z.nc defect
    p1             = p1.drop('clon').drop('clat')

    # %%
    reload(eva)
    ds_tides_tmean_2 = eva.load_smt_wave_2d_exp7_07_part2(it=1)
    begin            = '2019-07-19T00:00:00'
    end              = '2019-08-31T23:15:00'
    p2               = ds_tides_tmean_2.zos.sel(time=slice(begin, end))
    p2               = p2.drop('clon').drop('clat')

    # %%
    ds_tides_tmean_3 = eva.load_smt_wave_2d_exp7_07_part3(it=1)
    begin            = '2019-09-01T00:00:00'
    end              = '2020-06-13T23:15:00'
    p3               = ds_tides_tmean_3.zos.sel(time=slice(begin, end))
    # %%
    exp7 = xr.concat([p1, p2, p3], dim='time')
    exp7 = exp7.drop_duplicates('time')

    # %%

    ###### no tides
    exp9 = eva.load_smt_wave_9('2d', it=1)
    exp9 = exp9.zos

    begin = '2019-07-19T00:15:00'
    end   = '2019-10-19T23:15:00'

    # %% tides 2022
    ds_tides_22 = eva.load_smt_wave_all('2d', exp=8, it=1)
    exp8         = ds_tides_22.drop('clon').drop('clat').zos
    exp8         = exp8.drop_duplicates('time')

    # last 6 weeks
    begin   = '2022-02-18T00:00:00'
    end     = '2022-03-31T23:15:00'
    tstring = 'last6weeks'

    # last 6 weeks
    begin   = '2019-07-19T00:15:00'
    end     = '2019-08-31T23:15:00'
    tstring = 'last6weeks'


    # %%
    lon_reg       = [-25, 30]
    lat_reg       = [-60, -5]
    gg     = eva.load_smt_wave_grid()
    res    = np.sqrt(gg.cell_area_p)
    res    = res.rename(cell='ncells')
    data_r = pyic.interp_to_rectgrid_xr(res, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_r = data_r.drop('clon').drop('clat')
    # %% interpolation and selection
    # experiment 8
    reload(fu)
    path_2_data = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'
    fname       = f'smtwv0008_spectra_ssh_{tstring}.nc'
    data        = exp8.sel(time=slice(begin,end))
    fu.select_interpolate_save(data, data_r, path_2_data, fname)

    # %% experiment 7
    reload(fu)
    tstring = 'July_2019-January_2020'
    begin   = '2019-07-09T00:00:00'
    end     = '2020-06-13T23:15:00'

    path_2_data = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'
    fname       = f'smtwv0007_spectra_ssh_{tstring}_full_year.nc'
    data        = exp7.sel(time=slice(begin,end))
    fu.select_interpolate_save(data, data_r, path_2_data, fname)

    # %% experiment 9
    reload(fu)
    path_2_data = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'
    fname       = f'smtwv0009_spectra_ssh_{tstring}.nc'
    data        = exp9.sel(time=slice(begin,end))
    fu.select_interpolate_save(data, data_r, path_2_data, fname)

    # %% SMT NATL
    exp0 = eva.load_smt_ssh()
    exp0 = exp0.h_sp
    # %%
    lon_reg       = [-80, -40]
    lat_reg       = [20, 50]
    gg            = eva.load_smt_grid()
    res           = np.sqrt(gg.cell_area_p)
    res           = res.rename(cell='ncells')
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.30_180W-180E_90S-90N.nc'
    data_r        = pyic.interp_to_rectgrid_xr(res, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    # %%
    reload(fu)
    tstring = 'All'
    begin   = '2010-01-09T01:00:00'
    end     = '2010-03-31T23:00:00'

    path_2_data = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'
    fname       = f'smt0000_spectra_ssh_{tstring}.nc'
    data        = exp0.sel(time=slice(begin,end))
    fu.select_interpolate_save_natl(data, data_r, path_2_data, fname)


    # %%
    #same rectangular as uchida for SMT-NATL
    lon_reg       = [-78, -68]
    lat_reg       = [30, 40]
    fname         = f'smt0000_spectra_ssh_rect.nc'
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.30_180W-180E_90S-90N.nc'
    data          = exp0.sel(time=slice(begin,end))
    data_interp   = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_interp.to_netcdf(f'{path_2_data}/{fname}')

    # %% Aviso
    path_2_data = '/work/mh0033/m300878/spectra/ssh/aviso/'

    ds_aviso = eva.load_aviso_aqua()
    lon_reg       = [-80, -40]
    lat_reg       = [20, 50]
    ssh_satellite = ds_aviso.sel(longitude=slice(lon_reg[0], lon_reg[1]), latitude=slice(lat_reg[0], lat_reg[1])).adt
    ssh_satellite = ssh_satellite.chunk({'time': -1, 'latitude': 120, 'longitude': 160})

    x        = ssh_satellite.isel(time=0)
    mask_sat = ~np.isnan(x)
    P_aviso  = fu.compute_spectra_uchida(ssh_satellite.fillna(0.), samp_freq=24)
    P_aviso.to_netcdf(f'{path_2_data}/aviso_spectra_ssh_na.nc')
    mask_sat.to_netcdf(f'{path_2_data}/mask_aviso_na.nc')

