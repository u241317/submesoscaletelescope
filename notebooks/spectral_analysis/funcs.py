# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import matplotlib.dates as mdates

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

import scipy.signal as signal
import xrft
import math


def simple_spectra(y0, samp_freq):
    y0    = y0 - y0.mean() # remove mean
    scale = 1.0/(samp_freq*(y0.size/2))
    # use hann window
    y0    = y0 * np.hanning(y0.size)
    FT    = np.fft.fft(y0, n=None, axis=- 1, norm=None) #one d discrete Fast Fourier Transfom fft
    ffs   = np.fft.rfftfreq(n=y0.size, d=1/samp_freq) # get frequencies # Return the Discrete Fourier Transform sample frequencies
    FT    = FT * np.conjugate(FT) * scale # get power spectrum

    FT  = FT[0:int(y0.size/2)]
    ffs = ffs[1:]
    # same like         f, p = signal.welch(data_interp[:,i,j], fs=24, nperseg=time, detrend='linear', window='boxcar', scaling='density', axis=0)

    return ffs, FT


#define functions to return the Coriolis and tidal frequqncies
def corfreq(lat):
    """
    The Coriolis frequency in rad / day at a given latitude.
    
    Args: 
        lat: Latitude in degree
        
    Returns:
        The Coriolis frequecy at latitude lat
    """    
    omega = 7.2921159e-5
    return 2*np.sin(np.abs(lat)*2*np.pi/360)*omega*(3600)*24


def tidefreq():
    """
    Eight major tidal frequencies in rad / day.  See Gill (1982) page 335.
    
    Args: 
        None
        
    Returns:
        An array of eight frequencies
    """    
    f = 24*2*np.pi/np.array([327.85,25.8194,24.0659,23.9344,12.6584,12.4206,12.0000,11.9673])
    names = ['Mr','O1','P1', 'K1', 'N2', 'M2', 'S2', 'K2']
    return f

def tidefreqnames():
    """
    Eight major tidal frequencies in rad / day.  See Gill (1982) page 335.
    
    Args: 
        None
        
    Returns:
        An array of eight frequencies
    """    
    f = 24*2*np.pi/np.array([327.85,25.8194,24.0659,23.9344,12.6584,12.4206,12.0000,11.9673])
    names = ['Mr','O1','P1', 'K1', 'N2', 'M2', 'S2', 'K2']
    return names

def plot_spectra(f, Pxx, lat_mean, fig_path, var, savefig=False):
    Pxx = Pxx * 24 * 3600 # convert from day to seconds

    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.9, fig_size_fac=3)
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    plt.loglog(f, Pxx, color='k', lw=1.7)

    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ m^2/s^{-1}$')
    if var == 'KE': plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.xlim(1e-1, 13)
    plt.ylim(1e-2,1e3)
    plt.legend()
    plt.grid(which='both', linestyle='--')
    plt.title(f'PSD of {var}')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black")
    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    if savefig==True: plt.savefig(f'{fig_path}/spectra_{var}.png', dpi=100, bbox_inches='tight')

def plot_spectra_tides_notides(f1, Pxx1, f2, Pxx2, lat_mean, fname, fig_path, savefig=False):
    Pxx1 = Pxx1 * 24 * 3600
    Pxx2 = Pxx2 * 24 * 3600

    t1 = f1.shape[0] * 2
    t2 = f2.shape[0] * 2

    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=1.1, fig_size_fac=3)
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # add minus 2 slope
    plt.loglog(f1, 1e1*f1**(-2), color='k', lw=0.5, linestyle='--', label=r'$f^{-2}$')
    plt.loglog(f1, Pxx1, color='k', lw=1.7, label=f'no tides ({t1}h)', zorder=10)
    plt.loglog(f2, Pxx2, color='r', lw=1.7, label=f'with tides ({t2}h)', zorder=11)

    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ [m^2/s]$')
    plt.xlim(1e-2, 13)
    plt.ylim(1e-4,1e6)
    plt.legend()
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title(f'PSD of SSH {fname[12:]} \n ( '+r'$\approx$ 0.3° spacing; res < 600m)')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", )
    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=100, bbox_inches='tight')

def plot_spectra_tides_notides2(f1, Pxx1, f2, Pxx2, f3, Pxx3, lat_mean, fname, fig_path, savefig=False):
    Pxx1 = Pxx1 * 24 * 3600
    Pxx2 = Pxx2 * 24 * 3600
    Pxx3 = Pxx3 * 24 * 3600

    t1 = f1.shape[0] * 2
    t2 = f2.shape[0] * 2
    t3 = f3.shape[0] * 2

    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=1.1, fig_size_fac=3)
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # add minus 2 slope
    plt.loglog(f1, 1e1*f1**(-2), color='k', lw=0.5, linestyle='--', label=r'$f^{-2}$')
    plt.loglog(f1, Pxx1, color='k', lw=1.7, label=f'no tides 19 ({t1}h)', zorder=10)
    plt.loglog(f2, Pxx2, color='r', lw=1.7, label=f'with tides 19 ({t2}h)', zorder=11)
    plt.loglog(f3, Pxx3, color='royalblue', lw=1.7, label=f'with tides 22 ({t3}h)', zorder=11)


    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ [m^2/s]$')
    plt.xlim(1e-2, 13)
    plt.ylim(1e-4,1e6)
    plt.legend()
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title(f'PSD of SSH {fname[12:]} \n ( '+r'$\approx$ 0.3° spacing; res < 600m)')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", )
    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=100, bbox_inches='tight')



def plot_spectra_tides_notides_KE(f1, Pxx1, f2, Pxx2, lat_mean, fig_path, var, savefig=False):
    Pxx1 = Pxx1 * 24 * 3600
    Pxx2 = Pxx2 * 24 * 3600

    t1 = f1.shape[0] * 2
    t2 = f2.shape[0] * 2

    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.9, fig_size_fac=3)
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # add minus 2 slope
    plt.loglog(f1, 1e2*f1**(-2), color='k', lw=0.5, linestyle='--', label=r'$f^{-2}$')
    plt.loglog(f1, Pxx1, color='k', lw=1.7, label=f'no tides (t={t1})', zorder=10)
    plt.loglog(f2, Pxx2, color='r', lw=1.7, label=f'with tides (t={t2})', zorder=11)

    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')

    plt.xlim(5e-2, 7)
    plt.ylim(1e-2,5e3)
    plt.legend(loc='lower left')
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title(f'PSD of {var} timeseries July/August \n (mutltiple realisations (1650) '+r'$\approx$ 30km spacing)')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", )
    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    if savefig==True: plt.savefig(f'{fig_path}/{var}_spectra.png', dpi=100, bbox_inches='tight')


def plot_spectra_tides_notides_KE_w(f1, Pxx1, f2, Pxx2, lat_mean, fig_path, var, savefig=False):
    Pxx1 = Pxx1 * 24 * 3600
    Pxx2 = Pxx2 * 24 * 3600

    t1 = f1.shape[0] * 2
    t2 = f2.shape[0] * 2

    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.9, fig_size_fac=3)
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # add minus 2 slope
    plt.loglog(f1, 1e-9*f1**(-2), color='k', lw=0.5, linestyle='--', label=r'$f^{-2}$')
    plt.loglog(f1, Pxx1, color='k', lw=1.7, label=f'no tides (t={t1})', zorder=10)
    plt.loglog(f2, Pxx2, color='r', lw=1.7, label=f'with tides (t={t2})', zorder=11)

    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')

    plt.xlim(5e-2, 7)
    plt.ylim(1e-10, 1e-8)
    plt.legend(loc='lower left')
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title(f'PSD of {var} timeseries July/August \n (mutltiple realisations (1650) '+r'$\approx$ 30km spacing)')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", )
    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    if savefig==True: plt.savefig(f'{fig_path}/{var}_spectra.png', dpi=100, bbox_inches='tight')

def plot_spectra_KE(P_exp7, P_exp8, P_exp9, P1, P2, lat_mean, count, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=3)
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.loglog(P_exp7.freq_time[1:]*86400,   P_exp7.mean(['lon','lat'], skipna=True)[1:], label=f'tides Jul-Oct 2019 (t={P_exp7.freq_time.size*2/12:.0f}d)')
    ax.loglog(P_exp8.freq_time[1:]*86400,   P_exp8.mean(['lon','lat'], skipna=True)[1:], label=f'tides Feb/Mar 2022 (t={P_exp8.freq_time.size*2/12:.0f}d)')
    ax.loglog(P_exp9.freq_time[1:]*86400,   P_exp9.mean(['lon','lat'], skipna=True)[1:], label=f'no tides Jul-Oct 2019 (t={P_exp9.freq_time.size*2/12:.0f}d)')

    # if KE_obs[100] != None:
    #     f, P = signal.welch(KE_obs.fillna(0.), fs=1/(3600*0.5), window='hann', nperseg=1024, noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=-1)
    #     ax.loglog(f[1:]*86400,P[1:], label=f'Mooring: 7.1°E 32.7°S (t=2Y), d={KE_obs.depth.data}m')


    ###################### compute freq.
    ax.loglog(P1.freq_time[1:]*86400, P1[1:], color='tab:red', label=f'Mooring n={P1.nreal.data} (t={P1.freq_time.size*2/48:.0f}d)')
    ax.loglog(P2.freq_time[1:]*86400, P2[1:], label=f'Mooring 2 n={P2.nreal.data} (t={P2.freq_time.size*2/48/2:.0f}d)')



    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    # plt.xlim(5e-2, 7)
    # plt.xlim(2e-2, 3e1)
    plt.ylim(1e-2,5e3)
    # plt.ylim(5e-5,1e2)
    plt.legend(loc='upper right')
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title('Power Spectral Density of KE at 98m')
    # plt.title(f'PSD of KE at {P_exp7.depth.data:.0f}m \n (spat. realisations {count}'+r'$\approx$ 30km spacing < 600m)')
    # plt.title(f'PSD of KE (mean of 50m & 100m) \n (spat. realisations {count}'+r'$\approx$ 30km spacing < 600m)')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )
    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-2
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')



def plot_spectra_KE_new(P_exp7_1h, P_exp7, P_exp8, P_exp9, P1, P2, lat_mean, count, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=2, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    alpha=0.75
    ax.loglog(P_exp7_1h.freq_time[1:]*86400,   P_exp7_1h.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 Sep-Jan 19/20 (t={P_exp7_1h.freq_time.size*2/12:.0f}d)', color='tab:blue', alpha=alpha)
    ax.loglog(P_exp7.freq_time[1:]*86400,   P_exp7.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 Jul-Oct 2019 (t={P_exp7.freq_time.size*2/12:.0f}d)', color='tab:blue', alpha=alpha, ls=':')
    ax.loglog(P_exp8.freq_time[1:]*86400,   P_exp8.mean(['lon','lat'], skipna=True)[1:], label=f'EXP8 Feb/Mar 2022 (t={P_exp8.freq_time.size*2/12:.0f}d)', color='tab:green', alpha=alpha)
    ax.loglog(P_exp9.freq_time[1:]*86400,   P_exp9.mean(['lon','lat'], skipna=True)[1:], label=f'EXP9 Jul-Oct 2019 (t={P_exp9.freq_time.size*2/12:.0f}d)', color='grey', alpha=alpha)

    ###################### compute freq.
    ax.loglog(P1.freq_time[1:]*86400, P1[1:], color='tab:red', label=f'M1 n={P1.nreal.data}', alpha=alpha)
    ax.loglog(P2.freq_time[1:]*86400, P2[1:], color='tab:orange', label=f'M2 n={P2.nreal.data}', alpha=alpha)

    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.xlim(3e-2, 2e1)
    plt.ylim(1e-3,5e3)
    plt.legend(loc='upper right', fontsize=8)
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title('Power Spectral Density of KE at 98m')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )
    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-2
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')


def plot_spectra_KE_new_haha(P_exp7_1h, P_r2b9, P_exp9, P1, P2, lat_mean, count, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=2, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    alpha=1
    f = P_exp7_1h.freq_time*86400 # convert from second to day

    x = np.linspace(1e-2, 2e1, 100)
    y = 1e0*x**-2
    plt.loglog(x, y, color='black', linestyle='--', linewidth=1)
    plt.text(1.3e-2, 4.5e3, r'${-2}$', fontsize=12)
    y = 1e0*x**-3
    plt.loglog(x, y, color='black', linestyle=':', linewidth=1)
    plt.text(5.3e-2, 4.5e3, r'${-3}$', fontsize=12)

    # ax.loglog(f, 1e0*f**(-2), color='k', lw=0.5, linestyle='-')
    # ax.text(0.1, 1e2, r'$f^{-2}$', fontsize=8, rotation=0, va='bottom', ha='center')
    # ax.loglog(f, 1e0*f**(-3), color='k', lw=0.5, linestyle='--')
    # ax.text(0.1, 1e3, r'$f^{-3}$', fontsize=8, rotation=0, va='bottom', ha='center')
    ax.loglog(P_r2b9.freq_time[1:]*86400,   P_r2b9.mean(['lon','lat'], skipna=True)[1:], label=f'ICON-R2B9-Wave 1Y ', color='tab:green', alpha=alpha)
    ax.loglog(P_exp9.freq_time[1:]*86400,   P_exp9.mean(['lon','lat'], skipna=True)[1:], label=f'ICON-SMT 4M ', color='grey', alpha=alpha)
    ax.loglog(P_exp7_1h.freq_time[1:]*86400,   P_exp7_1h.mean(['lon','lat'], skipna=True)[1:], label=f'ICON-SMT-Wave 1Y', color='tab:blue', alpha=alpha)

    ###################### compute freq.
    ax.loglog(P1.freq_time[1:]*86400, P1[1:], color='tab:red', label=f'M1 2Y', alpha=alpha)
    ax.loglog(P2.freq_time[1:]*86400, P2[1:], color='tab:orange', label=f'M2 2Y', alpha=alpha)

    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.xlim(1e-2, 1.3e1)
    plt.ylim(8e-7,3e4)
    plt.legend(loc='lower left', fontsize=8)
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title('Power Spectral Density of KE at 98m')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=0 )
    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-5
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="tab:red",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')
    # ax.autoscale(enable=True, axis='x', tight=True)

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')


def plot_spectra_KE_season(P7, lat_mean, fname, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=2.8, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    alpha=0.75

    x = np.linspace(1e-2, 2e1, 100)
    y = 1e0*x**-2
    plt.loglog(x, y, color='black', linestyle='--', linewidth=1)
    y = 1e0*x**-3
    plt.loglog(x, y, color='black', linestyle=':', linewidth=1)

    colors = plt.cm.jet(np.linspace(0,1,12))
    strings = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    for i in np.arange(12):
        print(i)
        ds = P7[i]
        ax.loglog(ds.freq_time[1:]*86400,   ds.mean(['lon','lat'], skipna=True)[1:], label=f'{strings[i]}.', color=colors[i], alpha=alpha)


    plt.xlabel('frequency [cpd]', fontsize=15)
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$', fontsize=15)
    plt.xlim(4e-2, 1.3e1)
    plt.ylim(1e-2,1e4)
    plt.legend(loc='lower left', fontsize=13, ncol=2)
    plt.grid(which='both', linestyle='--', lw=0.3)
    # plt.title('Power Spectral Density of KE at 98m')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=0 )

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="tab:red",linewidth=2, zorder=1)

    ax.text(0.9, 0.95, '50m', transform=ax.transAxes, fontsize=15, verticalalignment='top', zorder=100,)

    ax3 = ax.secondary_xaxis('top', functions=(lambda x: 1/x, lambda x: 1/x))
    ax3.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax3.set_xticks([30, 7, 1, 1/24])
    ax3.set_xticklabels(['month', 'week', 'day', 'hour'])
    ax3.tick_params(axis='x', labelsize=15)

    #increase ticks
    ax.tick_params(axis='both', which='major', labelsize=15)

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')


def plot_spectra_KE_season2(P7, lat_mean, fname, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=2.8, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    alpha=0.75

    x = np.linspace(1e-2, 2e1, 100)
    y = 1e0*x**-2
    plt.loglog(x, y, color='black', linestyle='--', linewidth=1)
    y = 1e0*x**-3
    plt.loglog(x, y, color='black', linestyle=':', linewidth=1)

    colors = plt.cm.jet(np.linspace(0,1,4))
    strings = ['JFM','AMJ','JAS','OND']
    for i in np.arange(4):
        print(i)
        ds = P7[i]
        ax.loglog(ds.freq_time[1:]*86400,   ds.mean(['lon','lat'], skipna=True)[1:], label=f'{strings[i]}.', color=colors[i], alpha=alpha)


    plt.xlabel('frequency [cpd]', fontsize=15)
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$', fontsize=15)
    plt.xlim(4e-2, 1.3e1)
    plt.ylim(1e-2,1e4)
    plt.legend(loc='lower left', fontsize=13, ncol=2)
    plt.grid(which='both', linestyle='--', lw=0.3)
    # plt.title('Power Spectral Density of KE at 98m')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=0 )
    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="tab:red",linewidth=2, zorder=1)

    ax.text(0.9, 0.95, '50m', transform=ax.transAxes, fontsize=15, verticalalignment='top', zorder=100,)

    ax3 = ax.secondary_xaxis('top', functions=(lambda x: 1/x, lambda x: 1/x))
    ax3.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax3.set_xticks([30, 7, 1, 1/24])
    ax3.set_xticklabels(['month', 'week', 'day', 'hour'])
    ax3.tick_params(axis='x', labelsize=15)

    #increase ticks
    ax.tick_params(axis='both', which='major', labelsize=15)

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')


def plot_spectra_KE_vs_eddy(P_exp7_eddy, P_exp9_eddy, P_exp7, P_exp8, P_exp9, P1, P2, lat_mean, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=2, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    alpha=0.75
    #### Domain
    ax.loglog(P_exp7.freq_time[1:]*86400,   P_exp7.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 Jul-Oct 2019)', color='tab:blue', alpha=alpha)
    # ax.loglog(P_exp8.freq_time[1:]*86400,   P_exp8.mean(['lon','lat'], skipna=True)[1:], label=f'EXP8 Feb/Mar 2022 (t={P_exp8.freq_time.size*2/12:.0f}d)', color='tab:green', alpha=alpha)
    ax.loglog(P_exp9.freq_time[1:]*86400,   P_exp9.mean(['lon','lat'], skipna=True)[1:], label=f'EXP9 Jul-Oct 2019)', color='grey', alpha=alpha)
    #### Eddies
    ax.loglog(P_exp7_eddy.freq_time[1:]*86400,   P_exp7_eddy.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 Anticyclone', color='tab:purple', alpha=alpha)
    ax.loglog(P_exp9_eddy.freq_time[1:]*86400,   P_exp9_eddy.mean(['lon','lat'], skipna=True)[1:], label=f'EXP9 Anticyclone', color='tab:brown', alpha=alpha)

    #### Moorings
    # ax.loglog(P1.freq_time[1:]*86400, P1[1:], color='tab:red', label=f'M1 n={P1.nreal.data}', alpha=alpha)
    # ax.loglog(P2.freq_time[1:]*86400, P2[1:], color='tab:orange', label=f'M2 n={P2.nreal.data}', alpha=alpha)


    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.xlim(3e-2, 2e1)
    plt.ylim(5e-3,5e3)
    plt.legend(loc='upper right', fontsize=8)
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title('Power Spectral Density of KE at 98m')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=0 )
    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-2
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2, zorder=0)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')


def plot_spectra_KE_alexa(P_exp7, P_exp8, P_exp9, P1, P2, lat_mean, count, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.8, fig_size_fac=2.7, axlab_kw=None)
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.loglog(P_exp7.freq_time[1:]*86400,   P_exp7.mean(['lon','lat'], skipna=True)[1:], label=f'tides Jul-Oct 2019')
    ax.loglog(P_exp8.freq_time[1:]*86400,   P_exp8.mean(['lon','lat'], skipna=True)[1:], label=f'tides Feb/Mar 2022')
    ax.loglog(P_exp9.freq_time[1:]*86400,   P_exp9.mean(['lon','lat'], skipna=True)[1:], label=f'no tides Jul-Oct 2019')

    ###################### compute freq.
    ax.loglog(P1.freq_time[1:]*86400, P1[1:], color='tab:red', label=f'Mooring 2021-2023')
    # ax.loglog(P2.freq_time[1:]*86400, P2[1:], label=f'Mooring 2 n={P2.nreal.data} (t={P2.freq_time.size*2/48/2:.0f}d)')



    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    # plt.xlim(5e-2, 7)
    plt.xlim(1e-1, 3e1)
    plt.ylim(1e-2,1e3)
    # plt.ylim(5e-5,1e2)
    plt.legend(loc='upper right')
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title('Power Spectral Density of KE at 98m')
    # plt.title(f'PSD of KE at {P_exp7.depth.data:.0f}m \n (spat. realisations {count}'+r'$\approx$ 30km spacing < 600m)')
    # plt.title(f'PSD of KE (mean of 50m & 100m) \n (spat. realisations {count}'+r'$\approx$ 30km spacing < 600m)')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )
    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-2
    for i in range(len(freq_tides)):
        if i ==0: continue
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')

def plot_spectra_KE_1h(P_exp7, P1, P2, lat_mean, count, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.6, fig_size_fac=2, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax.loglog(P_exp7.freq_time[1:]*86400,   P_exp7.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7', zorder=5)
    ax.loglog(P1.freq_time[1:]*86400, P1[1:], label=f'M1 n={P1.nreal.data} ', color='tab:red')
    ax.loglog(P2.freq_time[1:]*86400, P2[1:], label=f'M2 n={P2.nreal.data}', color='tab:orange')

    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.xlim(5e-2, 2e1)
    plt.ylim(1e-3,1e3)
    plt.legend(loc='upper right')
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title(f'PSD of KE at {P_exp7.depth.values:.0f}m \n M1:{P1.attrs}  \n M2:{P2.attrs} \n  Sep 2019 - Jan 2020 ')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )
    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-2
    for i in range(len(freq_tides)):
        if i ==0: continue
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')


def plot_spectra_KE_1h_seasons(P_exp7_S0, P_exp7_S1, P_exp7_S2, P_exp7_S3, P1, P2, lat_mean, count, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=2.5, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    f = P_exp7_S0.freq_time*86400 # convert from second to day
    ax.loglog(f, 1e0*f**(-2), color='k', lw=0.5, linestyle='-')
    ax.text(0.1, 1e2, r'$f^{-2}$', fontsize=8, rotation=0, va='bottom', ha='center')
    ax.loglog(f, 1e0*f**(-3), color='k', lw=0.5, linestyle='--')
    ax.text(0.1, 1e3, r'$f^{-3}$', fontsize=8, rotation=0, va='bottom', ha='center')
    ax.loglog(P_exp7_S0.freq_time[1:]*86400,   P_exp7_S0.mean(['lon','lat'], skipna=True)[1:], label=P_exp7_S0.attrs['time'], zorder=5)
    ax.loglog(P_exp7_S1.freq_time[1:]*86400,   P_exp7_S1.mean(['lon','lat'], skipna=True)[1:], label=P_exp7_S1.attrs['time'], zorder=5)
    ax.loglog(P_exp7_S2.freq_time[1:]*86400,   P_exp7_S2.mean(['lon','lat'], skipna=True)[1:], label=P_exp7_S2.attrs['time'], zorder=5)
    ax.loglog(P_exp7_S3.freq_time[1:]*86400,   P_exp7_S3.mean(['lon','lat'], skipna=True)[1:], label=P_exp7_S3.attrs['time'], zorder=5)
    # ax.loglog(P1.freq_time[1:]*86400, P1[1:], label=f'M1 n={P1.nreal.data} ', color='grey')
    # ax.loglog(P2.freq_time[1:]*86400, P2[1:], label=f'M2 n={P2.nreal.data}', color='grey')

    plt.xlabel('Frequency [cpd]')   
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.xlim(3e-2, 1.3e1)
    plt.ylim(1e-3,5e4)
    plt.legend(loc='lower left')
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title(f'PSD of KE at {P_exp7_S1.depth.values:.0f}m ')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )
    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-2
    for i in range(len(freq_tides)):
        if i ==0: continue
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')
    ax.autoscale(enable=True, axis='x', tight=True)

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')



def plot_spectra_KE_subsampeld(P_exp7, P_exp7_2h, P_exp7_4h, P1, P2, lat_mean, count, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.8, fig_size_fac=2.7, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax.loglog(P_exp7.freq_time[1:]*86400,   P_exp7.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 1h', zorder=5)
    ax.loglog(P_exp7_2h.freq_time[1:]*86400,   P_exp7_2h.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 2h', zorder=6)
    ax.loglog(P_exp7_4h.freq_time[1:]*86400,   P_exp7_4h.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 4h', zorder=7)
    ax.loglog(P1.freq_time[1:]*86400, P1[1:], label=f'M1 n={P1.nreal.data} ')
    ax.loglog(P2.freq_time[1:]*86400, P2[1:], label=f'M2 n={P2.nreal.data}')

    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.xlim(5e-2, 3e1)
    plt.ylim(1e-3,1e3)
    plt.legend(loc='upper right')
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title(f'PSD of KE at {P_exp7.depth.values:.0f}m \n M1:{P1.attrs["depths"]}  \n M2:{P2.attrs["depths"]} \n  Sep 2019 - Jan 2020 ')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )
    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')


def plot_spectra_KE_1h_depths(P_exp7, P1, P2, lat_mean, count, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.8, fig_size_fac=2.7, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # colors from clormap plasma
    colors = plt.cm.plasma(np.linspace(0,1,len(P_exp7.depth)))
    for i in range(0, len(P_exp7.depth)):
        ds = P_exp7.isel(depth=i)
        ax.loglog(ds.freq_time[1:]*86400, ds.mean(['lon','lat'], skipna=True)[1:], label=f'{P_exp7.depth[i].values:.0f}m', color=colors[i])
    # ax.loglog(P1.freq_time[1:]*86400, P1[1:], label=f'M1 n={P1.nreal.data} ')
    # ax.loglog(P2.freq_time[1:]*86400, P2[1:], label=f'M2 n={P2.nreal.data}')

    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.xlim(1e-2, 2e1)
    plt.ylim(1e-6,1e4)
    plt.legend(loc='upper right', fontsize=6)
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title(f'PSD of KE \n  Oct 2019 - Jan 2020 ')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )
    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-2
    for i in range(len(freq_tides)):
        if i ==0: continue
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')


def plot_spectra_KE_single_pos(EXP, P1, P2, lat_mean, radius,  count, fig_path, fname, savefig):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=3)
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    string = [ 'e7_m1', 'e7_m2', 'e8_m1', 'e8_m2', 'e9_m1', 'e9_m2']

    for i in range(6):
        ax.loglog(EXP[i].freq_time[1:]*86400, EXP[i][1:], label=f'{string[i]} (t={EXP[i].freq_time.size*2/12:.0f}d)')

    ###################### compute freq.
    ax.loglog(P1.freq_time[1:]*86400, P1[1:], label=f'Mooring 1 n={P1.nreal.data} (t={P1.freq_time.size*2/48:.0f}d)')
    ax.loglog(P2.freq_time[1:]*86400, P2[1:], label=f'Mooring 2 n={P2.nreal.data} (t={P2.freq_time.size*2/48/2:.0f}d)')

    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.ylim(1e-2,5e3)

    plt.legend(loc='upper right')
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title(f'PSD of KE at {EXP[0].depth.data:.0f}m \n (spat. realisations {count}'+rf'radius: {radius}')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )
    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-2
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')


def plot_obs_spectra(data, f, P, lat_mean, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=2)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    for i in range(data.depth.size):
        ax.loglog(f[1:]*86400,P[i][1:], alpha=.7, label=f'{data.depth[i].values} m')
    plt.legend(fontsize=7)
    # split legend into 2 rows
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)


    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.title('PSD of KE over different depths')
    plt.ylim(1e-2,1e3)

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black" )
    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-2
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 2e-2, 'Coriolis', fontsize=8, rotation=90, va='bottom', ha='center')

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')




def compute_ncell_idx(lon, lat):
    grid = eva.load_smt_wave_grid()
    clon = np.rad2deg(grid.clon.data)
    clat = np.rad2deg(grid.clat.data)
    idx  = np.argmin(((clon-lon)**2 + (clat-lat)**2))
    return idx

def calc_spectra(data_interp, samp_freq):
    shape = data_interp.shape
    time  = shape[0]
    lon   = shape[1]
    lat   = shape[2]

    idx = -1
    for i in range(lon):
        for j in range(lat):
            idx += 1
            if False:
                f, p = signal.welch(data_interp[:,i,j], fs=samp_freq, nperseg=time, detrend='constant', window='hann', scaling='density', axis=0)
            else: f, p = simple_spectra(data_interp[:,i,j], samp_freq=samp_freq)

            if idx == 0:
                spectra = np.zeros((len(f), lon*lat))
            spectra[:,idx] = p

    Pxx      = spectra.mean(axis=1)
    lat_mean = data_interp.lat.mean().data
    return f, Pxx, lat_mean

def calc_spectra2(data_interp, samp_freq):
    shape = data_interp.shape
    time  = shape[0]
    lon   = shape[1]
    lat   = shape[2]

    spectra = np.zeros((int(time/2), lon*lat))
    idx     = -1
    count   = 0
    for i in range(lon):
        for j in range(lat):
            idx += 1
            if data_interp[0,i,j] == np.nan:
                continue
            else:
                f, p = simple_spectra(data_interp[:,i,j], samp_freq=samp_freq)
                spectra[:,idx] = p
                count += 1
                
    print(f'Averaged realisations: {count}')
    Pxx      = np.nanmean(spectra, axis=1)
    lat_mean = data_interp.lat.mean().data
    return f, Pxx, lat_mean, count

def plot_2d_spectra(f, spectra_2d, lat_mean, fig_path, savefig=False):
    islice = spectra_2d.shape[1]
    depth  = eva.get_smt_wave_depth()

    hca, hcb = pyic.arrange_axes(1,1, plot_cb = True, asp=0.5, fig_size_fac=2)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    xx, yy = np.meshgrid(f, depth[1+np.arange(islice)])
    data = np.transpose(spectra_2d)  * 24 * 3600

    pyic.shade(xx, -yy, data, ax=ax, cax=cax, cmap='Spectral_r', norm=colors.LogNorm(vmin=1e-12, vmax=5e-9))

    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel('Depth [m]')
    ax.set_title(r'PSD of KE of vertical velocity $ [(m^2/s^2)/s^{-1}]$')
    ax.set_xticks(np.array([0.1, 0.5, 1e0, 2, 3, 4, 5e0]))  

    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", lw=0.5)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=1)
    if savefig==True: plt.savefig(f'{fig_path}/smtwv0007_spectra_KE_w_2d.png', dpi=150, bbox_inches='tight')

def select_interpolate_save(data, data_r, path_2_data, fname, res=600):
    lon_reg       = [-25, 30]
    lat_reg       = [-60, -5]
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'

    data_interp   = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_sel      = data_interp.where(data_r<=res)
    data_sel.to_netcdf(f'{path_2_data}/{fname}')

def select_interpolate_save_r2b9(data, path_2_data, fname):
    lon_reg       = [-10, 15]
    lat_reg       = [-45, -20]
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b9_oce_r0004/ckdtree/rectgrids/r2b9_oce_r0004_res0.30_180W-180E_90S-90N.nc'
    data_interp   = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_interp.to_netcdf(f'{path_2_data}/{fname}')

def select_interpolate_save_natl(data, data_r, path_2_data, fname, res=600):
    lon_reg       = [-80, -40]
    lat_reg       = [20, 50]
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.30_180W-180E_90S-90N.nc'
    data_interp   = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_sel      = data_interp.where(data_r<=res)
    data_sel.to_netcdf(f'{path_2_data}/{fname}')


def compute_spectra_uchida(data, samp_freq=1):
    data['time'] = np.arange(int(data.time.size))*3600*samp_freq
    P = xrft.power_spectrum(data, dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
    P = P.isel(freq_time=slice(len(P.freq_time)//2,None)) * 2
    return P

def plot_spectra_uchida(P_tides19, P_tides22, P_notides19, lat_mean, count, fname, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=1.1, fig_size_fac=4)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    amp = 24 #convert hours to days
    f = P_tides19.freq_time*86400 # convert from day to seconds

    ax.loglog(f, 1e1*f**(-2), color='k', lw=0.5, linestyle='--', label=r'$f^{-2}$')
    ax.loglog(P_tides19.freq_time[1:]*86400,   amp*P_tides19.mean(['lon','lat'], skipna=True)[1:], label=f'tides Jul/Aug 2019 (t={P_tides19.freq_time.size*2}h)')
    ax.loglog(P_notides19.freq_time[1:]*86400, amp*P_notides19.mean(['lon','lat'], skipna=True)[1:], label=f'no tides Jul/Aug 2019 (t={P_notides19.freq_time.size*2}h)')
    ax.loglog(P_tides22.freq_time[1:]*86400,   amp*P_tides22.mean(['lon','lat'], skipna=True)[1:], label=f'tides Feb/Mar 2022 (t={P_tides22.freq_time.size*2}h)')

    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.set_xlim(1e-2, 13)
    ax.set_ylim(1e-4,1e6)
    ax.legend(loc='lower left')
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.set_title(f'PSD of SSH {fname[23:]} \n (spat. realisations ({count.data}) '+r'$\approx$ 0.3° spacing; res < 600m)')

    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", )
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)

    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=200, bbox_inches='tight')


def plot_spectra_uchida_sa(P_tides19, P_tides22, P_notides19, data_obs, lat_mean, count, fname, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.6, fig_size_fac=4)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    f = P_tides19.freq_time*86400 # convert from second to day

    ax.loglog(f, 1e1*f**(-2), color='k', lw=0.5, linestyle='--', label=r'$f^{-2}$')
    ax.loglog(P_tides19.freq_time[1:]*86400,   P_tides19.mean(['lon','lat'], skipna=True)[1:], label=f'tides Jul-Oct 2019 (t={int(P_tides19.freq_time.size*2/24)}D)')
    ax.loglog(P_notides19.freq_time[1:]*86400, P_notides19.mean(['lon','lat'], skipna=True)[1:], label=f'no tides Jul-Oct 2019 (t={int(P_notides19.freq_time.size*2/24)}D)')
    ax.loglog(P_tides22.freq_time[1:]*86400,   P_tides22.mean(['lon','lat'], skipna=True)[1:], label=f'tides Feb/Mar 2022 (t={int(P_tides22.freq_time.size*2/24)}D)')


    f, P = signal.welch(data_obs.ssh_anomaly.fillna(0.), fs=1/(3600*1), window='hann', nperseg=2024, noverlap=None, nfft=None, detrend='linear', return_onesided=True, scaling='density', axis=-1)
    ax.loglog(f[1:]*86400,P[1:], label=f'PIES: 04:2021-04:2023 (t=2Y)')

    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.set_xlim(1e-2, 13)
    ax.set_ylim(1e-5,1e6)
    ax.legend(loc='lower left')
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.set_title(f'PSD of SSH SA \n (spat. realisations ({count.data}) '+r'$\approx$ 0.3° spacing; res < 600m)')

    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([100,3,10,1,3,10,1,100,10])*2e-5
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.1, ypos[i], freq_names[i], fontsize=10, rotation=90, va='bottom', ha='center')

    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", )
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 2e-5, 'Coriolis', fontsize=10, rotation=90, va='bottom', ha='center')

    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=200, bbox_inches='tight')


def plot_spectra_uchida_sa2(P_tides19, P_tides22, P_notides19, P_r2b9, ds_pies, lat_mean, count, fname, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.50, fig_size_fac=2.8, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    lw=1
    alpha=0.9
    f = P_tides19.freq_time*86400 # convert from second to day

    ax.loglog(f, 1e-1*f**(-2), color='k', lw=1, linestyle='--', zorder=1)
    # ax.text(1.3e-2, 1e2, r'$-2$', fontsize=12, rotation=0, va='bottom', ha='center')
    ax.loglog(f, 3e-2*f**(-3), color='k', lw=1, linestyle=':', zorder=1)
    # ax.text(1.3e-2, 3e4, r'$-3$', fontsize=12, rotation=0, va='bottom', ha='center')
    # ax.loglog(P_r2b9.freq_time[1:]*86400,     P_r2b9.mean(['lon','lat'], skipna=True)[1:], label=f'R2B9 {P_r2b9.tstring.values}  (t={int(P_r2b9.freq_time.size*2/24)}D)', color='tab:orange')
    # ax.loglog(P_notides19.freq_time[1:]*86400, P_notides19.mean(['lon','lat'], skipna=True)[1:], label=f'EXP9 {P_notides19.tstring.values}  (t={int(P_notides19.freq_time.size*2/24)}D)', color='grey')
    # ax.loglog(P_tides19.freq_time[1:]*86400,   P_tides19.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 {P_tides19.tstring.values} (t={int(P_tides19.freq_time.size*2/24)}D)')
    # ax.loglog(P_tides22.freq_time[1:]*86400,   P_tides22.mean(['lon','lat'], skipna=True)[1:], label=f'EXP8 {P_tides22.tstring.values}  (t={int(P_tides22.freq_time.size*2/24)}D)', color='tab:green')
    # ax.loglog(P_r2b9.freq_time[1:]*86400,     P_r2b9.mean(['lon','lat'], skipna=True)[1:], lw=lw, label=f'ICON-R2B9-Wave  (t={int(P_r2b9.freq_time.size*2/24)}D)', color='tab:green')
    # ax.loglog(P_notides19.freq_time[1:]*86400, P_notides19.mean(['lon','lat'], skipna=True)[1:], lw=lw, label=f'ICON-SMT   (t={int(P_notides19.freq_time.size*2/24)}D)', color='grey')
    # ax.loglog(P_tides19.freq_time[1:]*86400,   P_tides19.mean(['lon','lat'], skipna=True)[1:], lw=lw, label=f'ICON-SMT-Wave (t={int(P_tides19.freq_time.size*2/24)}D)')
    ax.loglog(P_r2b9.freq_time[1:]*86400,     P_r2b9.mean(['lon','lat'], skipna=True)[1:], lw=lw, label=f'ICON-R2B9-Wave', color='tab:green', alpha=alpha)
    ax.loglog(P_notides19.freq_time[1:]*86400, P_notides19.mean(['lon','lat'], skipna=True)[1:], lw=lw, label=f'ICON-SMT', color='grey', alpha=alpha)
    ax.loglog(P_tides19.freq_time[1:]*86400,   P_tides19.mean(['lon','lat'], skipna=True)[1:], lw=lw, label=f'ICON-SMT-Wave', alpha=alpha)
    # ax.loglog(P_tides22.freq_time[1:]*86400,   P_tides22.mean(['lon','lat'], skipna=True)[1:], label=f'EXP8   (t={int(P_tides22.freq_time.size*2/24)}D)', color='tab:green')

    ########################### PIES
    begin ='2021-04-12T11:00:00.000000000'
    end   ='2023-03-31T19:00:00.000000000'
    # devide time window into 5 parts
    t_window = 6
    T        = ds_pies.time.size
    dt       = math.floor(T/t_window)
    t0   = '2021-04-12T11:00:00.000000000'
    t0   = pd.to_datetime(t0)
    time = pd.date_range(t0, periods=t_window+1, freq=f'{dt}H')

    P = []
    for i in range(5):
        ds = ds_pies.isel(n=i)
        ds = ds.sel(time=slice(begin, end))
        for ii in range(t_window):
            ds_sel = ds.sel(time=slice(time[ii], time[ii+1]))
            p = compute_spectra_uchida(ds_sel.ssh_anomaly, samp_freq=1)
            P.append(p)

    # averagae
    P = xr.concat(P, dim='string')
    P_mean = P.mean('string', skipna=True)
    # ax.loglog(P_mean.freq_time[1:]*86400, P_mean[1:],  label=f'5 Pies (2Y: 04.2021-04.2023): {t_window*5} real.', alpha=1, color='tab:red')
    ax.loglog(P_mean.freq_time[1:]*86400, P_mean[1:], lw=lw, label=f'PIES', alpha=1, color='tab:red')

    ####################################    

    ax.set_xlabel('frequency [cpd]', fontsize=13)
    ax.set_ylabel(r'PSD $ [m^2/s]$', fontsize=13)
    ax.set_xlim(1e-2, 1.3e1)
    ax.set_ylim(1e-5,1e6)
    ax.legend(loc='lower left', fontsize=13)
    ax.grid(which='both', linestyle='--', lw=0.3)

    # plt.grid()
    # ax.set_title(f'PSD of SSH SA \n (spat. realisations ({count.data}) '+r'$\approx$ 0.3° spacing; res < 600m)')
    # ax.set_title(f'Power Spectral Density of SSH')


    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([100,3,10,1,3,10,1,100,10])*2e-5
    # for i in range(len(freq_tides)):
    #     ax.text(freq_tides[i]-freq_tides[i]*0.1, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="tab:red",linewidth=2, zorder=1)
    # vertical line at f+m2
    ax.vlines((tidefreq()/2/np.pi)[5]+freq_tides[1],ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="red", zorder=1)
    # ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 2e-5, 'f', fontsize=10, rotation=90, va='bottom', ha='center')
    # ax.autoscale(enable=True, axis='x', tight=True)
    ax3 = ax.secondary_xaxis('top', functions=(lambda x: 1/x, lambda x: 1/x))
    # ax3.set_xlabel('Period [days]')
    ax3.set_xlim(ax.get_xlim())
    #ticks month week day hour
    ax3.set_xticks([30, 7, 1, 1/24])
    ax3.set_xticklabels(['month', 'week', 'day', 'hour'], fontsize=13)

    #increase all ticks
    ax.tick_params(axis='both', which='major', labelsize=13)


    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=200, bbox_inches='tight')



def plot_spectra_seasons(P0, P1, P2, P3, lat_mean, count, fname, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=2.5)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    f = P0.freq_time*86400 # convert from second to day
    ax.loglog(f, 1e1*f**(-2), color='k', lw=0.5, linestyle='-')
    ax.text(0.1, 1e3, r'$f^{-2}$', fontsize=8, rotation=0, va='bottom', ha='center')
    ax.loglog(f, 1e1*f**(-3), color='k', lw=0.5, linestyle='--')
    ax.text(0.1, 1e4, r'$f^{-3}$', fontsize=8, rotation=0, va='bottom', ha='center')
    ax.loglog(P0.freq_time[1:]*86400,   P0.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 {P0.tstring.values} (t={int(P0.freq_time.size*2/24)}D)', color='tab:blue')
    ax.loglog(P1.freq_time[1:]*86400,   P1.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 {P1.tstring.values} (t={int(P1.freq_time.size*2/24)}D)', color='tab:orange')
    ax.loglog(P2.freq_time[1:]*86400,   P2.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 {P2.tstring.values} (t={int(P2.freq_time.size*2/24)}D)', color='tab:brown')
    ax.loglog(P3.freq_time[1:]*86400,   P3.mean(['lon','lat'], skipna=True)[1:], label=f'EXP7 {P3.tstring.values} (t={int(P3.freq_time.size*2/24)}D)', color='tab:green')

    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.set_xlim(1e-2, 13)
    ax.set_ylim(1e-5,1e6)
    ax.legend(loc='lower left')
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.set_title(f'PSD SSH SA \n (spat. real. ({count.data}) '+'6°radius around Mooring Positions')

    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([100,3,10,1,3,10,1,100,10])*2e-5
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.1, ypos[i], freq_names[i], fontsize=10, rotation=90, va='bottom', ha='center')

    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", )
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 2e-5, 'Coriolis', fontsize=10, rotation=90, va='bottom', ha='center')
    ax.autoscale(enable=True, axis='x', tight=True)

    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=200, bbox_inches='tight')


def plot_spectra_uchida_NA(P_notides10, ds_ref, lat_mean, count, fname, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=1.1, fig_size_fac=4)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    f = P_notides10.freq_time*86400 # convert from second to day

    ax.loglog(f, 1e1*f**(-2), color='k', lw=0.5, linestyle='--', label=r'$f^{-2}$')

    ax.loglog(P_notides10.freq_time[1:]*86400, P_notides10.mean(['lon','lat'], skipna=True)[1:], ls='-', alpha =.7, label=f'ICON-SMT (JFM t={int(P_notides10.freq_time.size*2/12)}d) 2h output', lw=1.5, zorder=90)
    strings = ['Hycom50 ', 'FESOM GS', 'eNATL60', 'FIO-COM32', 'ORCA36']

    for i in range(len(ds_ref)):
        # if i == 0 or i == 2 or i == 3: continue
        data = ds_ref[i]
        ax.plot(data.freq_time[1:]*86400, data[1:], alpha=.7, ls='-', label=rf'{strings[i]} FMA')


    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.set_xlim(1e-2, 13)
    ax.set_ylim(1e-4,1e6)
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.set_title(f'PSD of SSH NA \n (spat. realisations ({count.data})) ')#+r'$\approx$ 0.3° spacing; res < 600m)
    ax.legend(loc='lower left')

    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,0.1,10])*1e-2
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.1, ypos[i], freq_names[i], fontsize=15, rotation=90, va='bottom', ha='center')

    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2 , label='Coriolis')
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 2e-4, 'Coriolis', fontsize=12, rotation=90, va='bottom', ha='center')

    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=250, bbox_inches='tight')


def plot_spectra_uchida_NA_zoom(P_notides10, ds_ref, lat_mean, count, fname, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = False, asp=1.1, fig_size_fac=2.4)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    f = ds_ref[0].freq_time[1:]*86400

    ax.loglog(f, 1e1*f**(-2), color='k', lw=0.5, linestyle='--', label=r'$f^{-2}$')

    # ax.loglog(P_notides10.freq_time[1:]*86400, P_notides10.mean(['lon','lat'], skipna=True)[1:], ls='-', alpha =.7, label=f'ICON-SMT (JFM t={int(P_notides10.freq_time.size*2/12)}d) 2h output', lw=1.5, zorder=90)
    ax.loglog(P_notides10.freq_time[1:]*86400, P_notides10.mean(['lon','lat'], skipna=True)[1:], ls='-', alpha =.7, label=rf"ICON-SMT $\quad \Delta x<0.7$km JFM", lw=1.5, zorder=90)
    # strings = ['Hycom50', 'FESOM GS', 'eNATL60', 'FIO-COM32', 'ORCA36']
    # strings = [r'Hycom50 $\Delta x \approx 1.8$km', r'FESOM GS $\Delta x \approx 1$km', r'eNATL60 $\Delta x \approx 1.5$km', r'FIO-COM32 $\Delta x \approx 2.8$km', r'ORCA36 $\Delta x \approx 2.5$km']
    strings = [
        r'Hycom50 $\quad\;\, \Delta x \approx 1.8$km',  # Using \quad for space
        r'FESOM GS $\;\;\;\Delta x \approx 1.0$km',
        r'eNATL60 $\quad\;\;\Delta x \approx 1.5$km',
        r'FIO-COM32 $\;\;\Delta x \approx 2.8$km',
        r'ORCA36 $\qquad\Delta x \approx 2.5$km'
    ]

    for i in range(len(ds_ref)):
        # if i == 0 or i == 2 or i == 3: continue
        data = ds_ref[i]
        ax.plot(data.freq_time[1:]*86400, data[1:], alpha=.7, ls='-', label=rf'{strings[i]} FMA')


    ax.set_xlabel('frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.set_xlim(1e-2, 13)
    ax.set_ylim(1e-4,1e6)
    ax.grid(which='both', linestyle='--', lw=0.3)
    # ax.set_title(f'PSD of SSH ')#+r'$\approx$ 0.3° spacing; res < 600m)
    ax.legend(loc='lower left', fontsize=7)

    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,0.1,10])*1e-2
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.1, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2 , label='Coriolis')
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 2e-4, 'Coriolis', fontsize=10, rotation=90, va='bottom', ha='center')

    xlim_box = (1e0, 13)
    ylim_box = (1e-4,1e2)
    # draw grey box
    ax.fill_between([xlim_box[0], xlim_box[1]], ylim_box[0], ylim_box[1], color='grey', alpha=0.2)


    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax.loglog(f, 1e1*f**(-2), color='k', lw=0.5, linestyle='--', label=r'$f^{-2}$')

    ax.loglog(P_notides10.freq_time[1:]*86400, P_notides10.mean(['lon','lat'], skipna=True)[1:], ls='-', alpha =.7, label=f'ICON-SMT (JFM t={int(P_notides10.freq_time.size*2/12)}d) 2h output', lw=1.5, zorder=90)
    strings = ['Hycom50', 'FESOM GS', 'eNATL60', 'FIO-COM32', 'ORCA36']

    for i in range(len(ds_ref)):
        # if i == 0 or i == 2 or i == 3: continue
        data = ds_ref[i]
        ax.plot(data.freq_time[1:]*86400, data[1:], alpha=.7, ls='-', label=rf'{strings[i]} FMA')


    ax.set_xlabel('frequency [cpd]')
    # ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.set_title(f'zoom')
    # ax.legend(loc='lower left')

    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,0.1,10])*1e-2
    ii =0
    for i in range(len(freq_tides)):
        if ii < 4:
            ii+=1
            continue
        print(i)
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2 , label='Coriolis')
    ax.text(corfreq(lat_mean)/2/np.pi-0.05*corfreq(lat_mean)/2/np.pi, 2e-4, 'Coriolis', fontsize=10, rotation=90, va='bottom', ha='center')

    ax.set_xlim(1e0, 13)
    ax.set_ylim(1e-4,1e2)

    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=250, bbox_inches='tight')



def plot_spectra_ssh_convergence(spectra_exp7, spectra_exp9, Tstring, fname, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=4)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    amp = 1 #convert hours to days
    f   = spectra_exp7[0].freq_time[1:]*86400
    ax.loglog(f, 1e1*f**(-2), color='k', lw=0.5, linestyle='--', label=r'$f^{-2}$')
    lat_mean = spectra_exp7[0].lat.mean().data
    s        = spectra_exp7[0].shape
    count    = s[1]*s[2]

    ls = ['-', '--', '-.', ':']
    alpha = [0.4, 0.6, 0.8, 1]

    for i in range(len(spectra_exp7)):
        ax.loglog(spectra_exp7[i].freq_time[1:]*86400, amp*spectra_exp7[i].mean(['lon','lat'], skipna=True)[1:], color='tab:blue', alpha=alpha[i], ls=ls[i], label=f'(t={spectra_exp7[i].freq_time.size*2}h)')
    for i in range(len(spectra_exp7)):
        ax.loglog(spectra_exp9[i].freq_time[1:]*86400, amp*spectra_exp9[i].mean(['lon','lat'], skipna=True)[1:], color='tab:orange', alpha=alpha[i], ls=ls[i], label=f'(t={spectra_exp9[i].freq_time.size*2}h)')

    # s

    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s]$')
    min = spectra_exp7[0].freq_time[1:].min().data*86400
    max = spectra_exp7[0].freq_time[1:].max().data*86400
    ax.set_xlim(min, max)
    ax.set_ylim(1e-5,5e5)
    ax.legend(loc='lower left')
    ax.grid(which='both', linestyle='--', lw=0.3)
    lat = [spectra_exp7[0].lat.min().data, spectra_exp7[0].lat.max().data]
    lon = [spectra_exp7[0].lon.min().data, spectra_exp7[0].lon.max().data]
    ax.set_title(f'PSD of SSH {fname[23:]} \n (spat. realisations ({count}) '+rf'$\approx$ 0.3° spacing; Domain: 42°S 32°S, 5°W 10°E)')

    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", )
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)

    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=200, bbox_inches='tight')



def plot_spectra_ssh_convergence_exp7(spectra_exp7, Tstring, fname, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=0.5, fig_size_fac=4)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    amp = 1 #convert hours to days
    f   = spectra_exp7[0].freq_time[1:]*86400
    ax.loglog(f, 1e1*f**(-2), color='k', lw=0.5, linestyle='--', label=r'$f^{-2}$')
    lat_mean = spectra_exp7[0].lat.mean().data

    for i in range(len(spectra_exp7)):
        # ax.loglog(spectra_exp7[i].freq_time[1:]*86400, amp*spectra_exp7[i].mean(['lon','lat'], skipna=True)[1:], label=f'(t={int(spectra_exp7[i].freq_time.size*2/24)} days)')
        ax.loglog(spectra_exp7[i].freq_time[1:]*86400, amp*spectra_exp7[i].mean(['lon','lat'], skipna=True)[1:], label=f'{Tstring[i]}')

    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s]$')
    min = spectra_exp7[0].freq_time[1:].min().data*86400
    max = spectra_exp7[0].freq_time[1:].max().data*86400
    ax.set_xlim(min, max)
    ax.set_ylim(1e-5,5e5)
    ax.legend(loc='lower left')
    ax.grid(which='both', linestyle='--', lw=0.3)
    lat = [spectra_exp7[0].lat.min().data, spectra_exp7[0].lat.max().data]
    lon = [spectra_exp7[0].lon.min().data, spectra_exp7[0].lon.max().data]
    ax.set_title(f'PSD of SSH {fname[23:]} \n (spat. realisations (1650) '+rf'$\approx$ 0.3° spacing; Domain: 42°S 32°S, 5°W 10°E)')

    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", )
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2)

    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=200, bbox_inches='tight')


def load_spectra_uchida():
    # % open Hyom data
    nh = 12
    SCRATCH = '/work/mh0033/m300878/spectra/ssh/uchida_reference_data/spectra_calc/'
    Fhycomw_H = xr.open_dataarray(f'{SCRATCH}/hycom50/'+f'SSH_omega_fma.nc')
    Fhycoms_H = xr.open_dataarray(f'{SCRATCH}/hycom50/'+f'SSH_omega_aso.nc')
    maskh = xr.open_dataarray(f'{SCRATCH}/hycom50/'+f'mask.nc')
    plt.loglog(Fhycomw_H.freq_time, Fhycomw_H.where(maskh[::nh,::nh]).mean(['lat','lon'],skipna=True))
    plt.loglog(Fhycomw_H.freq_time, Fhycomw_H, ls = '--')
    plt.loglog(Fhycoms_H.freq_time, Fhycoms_H, ls = '--')

    # % open FESOM Data
    nf        = 16
    Ffesomw_H = xr.open_dataarray(f'{SCRATCH}/FESOM/'+f'SSH_omega_fma.nc')
    Ffesoms_H = xr.open_dataarray(f'{SCRATCH}/FESOM/'+f'SSH_omega_aso.nc')
    tmask     = xr.open_dataarray(f'{SCRATCH}/FESOM/'+f'mask.nc')
    plt.loglog(Ffesomw_H.freq_time, Ffesomw_H.where(tmask[::nf,::nf]).mean(['lat','lon'],skipna=True))

    # %open eNATL60 Data
    ne         = 16
    Fenatlw_H  = xr.open_dataarray(f'{SCRATCH}/eNATL60/'+f'SSH_omega_fma.nc')
    Fenatlws_H = xr.open_dataarray(f'{SCRATCH}/eNATL60/'+f'SSH_omega_aso.nc')
    # tmask      = xr.open_dataarray(f'{SCRATCH}/eNATL60/'+f'mask.nc')
    # plt.loglog(Fenatlw_H.freq_time, Fenatlw_H.where(tmask[::ne,::ne]).mean(['lat','lon'],skipna=True))
    plt.loglog(Fenatlw_H.freq_time, Fenatlw_H)

    # % open FIO-COM
    nf        = 16
    Ffiow_H   = xr.open_dataarray(f'{SCRATCH}/FIO-COM32/'+f'SSH_omega_fma.nc')
    Ffios_H   = xr.open_dataarray(f'{SCRATCH}/FIO-COM32/'+f'SSH_omega_aso.nc')
    plt.loglog(Ffiow_H.freq_time, Ffiow_H)

    # % open ORCA36
    Forcaw_H  = xr.open_dataarray(f'{SCRATCH}/ORCA36/'+f'SSH_omega_fma.nc')
    Forcas_H  = xr.open_dataarray(f'{SCRATCH}/ORCA36/'+f'SSH_omega_aso.nc')
    plt.loglog(Forcaw_H.freq_time, Forcaw_H)
    # %
    ds_ref_w = []
    ds_ref_w.append(Fhycomw_H)
    ds_ref_w.append(Ffesomw_H)
    ds_ref_w.append(Fenatlw_H)
    ds_ref_w.append(Ffiow_H)
    ds_ref_w.append(Forcaw_H)

    strings = ['Hycom50', 'FESOM GS', 'eNATL60', 'FIO-COM32', 'ORCA36']

    ds_ref_s = []
    ds_ref_s.append(Fhycoms_H)
    ds_ref_s.append(Ffesoms_H)
    ds_ref_s.append(Fenatlws_H)
    ds_ref_s.append(Ffios_H)
    ds_ref_s.append(Forcas_H)

    return ds_ref_w, ds_ref_s, strings


def calc_interp_KE(ds_u, ds_v, data_r, fname, path_2_data, res=600):
    data          = ds_u**2 /2 + ds_v**2 /2
    lon_reg       = np.array([-40, 30])
    lat_reg       = np.array([-55, -15])
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'
    data_interp   = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_sel      = data_interp.where(data_r<=res)
    data_sel.to_netcdf(f'{path_2_data}/{fname}.nc')

def calc_interp_KE_close(ds_u, ds_v, fname, path_2_data):
    print('start selecting data')
    data          = ds_u**2 /2 + ds_v**2 /2
    pos           = np.array([5, -32.5])
    radius        = 10
    lon_reg       = np.array([pos[0]-radius - 1, pos[0] + radius + 1])
    lat_reg       = np.array([pos[1]-radius - 1, pos[1] + radius + 1])
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'
    data_interp   = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

    # mask = (data_interp.lon - pos[0])**2 + (data_interp.lat - pos[1])**2 < radius**2
    # data_sel      = data_interp.where(mask==True)
    # data_sel.to_netcdf(f'{path_2_data}/{fname}.nc')
    data_interp.to_netcdf(f'{path_2_data}/{fname}.nc')

def calc_interp_KE_close_r2b9(ds_u, ds_v, fname, path_2_data):
    data          = ds_u**2 /2 + ds_v**2 /2
    pos           = np.array([5, -32.5])
    radius        = 10
    lon_reg       = np.array([pos[0]-radius - 1, pos[0] + radius + 1])
    lat_reg       = np.array([pos[1]-radius - 1, pos[1] + radius + 1])
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b9_oce_r0004/ckdtree/rectgrids/r2b9_oce_r0004_res0.10_180W-180E_90S-90N.nc'
    data_interp   = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

    mask = (data_interp.lon - pos[0])**2 + (data_interp.lat - pos[1])**2 < radius**2
    data_sel      = data_interp.where(mask==True)
    data_sel.to_netcdf(f'{path_2_data}/{fname}.nc')

def calc_interp_KE_r2b9(ds_u, ds_v, fname, path_2_data):
    data          = ds_u**2 /2 + ds_v**2 /2
    pos           = np.array([5, -32.5])
    radius        = 10
    lon_reg       = np.array([pos[0]-radius - 1, pos[0] + radius + 1])
    lat_reg       = np.array([pos[1]-radius - 1, pos[1] + radius + 1])
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b9_oce_r0004/ckdtree/rectgrids/r2b9_oce_r0004_res0.10_180W-180E_90S-90N.nc'
    data_group    = data.groupby('time.month')
    for t  in range(3):
        t = t+1+9
        print('interpolating month:', t)
        data_interp   = pyic.interp_to_rectgrid_xr(data_group[t], fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
        # if t == 0:
        #     mask = (data_interp.lon - pos[0])**2 + (data_interp.lat - pos[1])**2 < radius**2
        # data_sel      = data_interp.where(mask==True)
        data_interp.to_netcdf(f'{path_2_data}/{fname}_month_{t}.nc')

def calc_interp_var_r2b9(var, fname, path_2_data):
    data          = var
    pos           = np.array([5, -32.5])
    radius        = 10
    lon_reg       = np.array([pos[0]-radius - 1, pos[0] + radius + 1])
    lat_reg       = np.array([pos[1]-radius - 1, pos[1] + radius + 1])
    fpath_ckdtree = '/work/mh0033/m300602/icon/grids/r2b9_oce_r0004/ckdtree/rectgrids/r2b9_oce_r0004_res0.10_180W-180E_90S-90N.nc'
    data_group    = data.groupby('time.month')
    for t  in range(12):
        t = t+1
        print('interpolating month:', t)
        data_interp   = pyic.interp_to_rectgrid_xr(data_group[t], fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
        data_interp.to_netcdf(f'{path_2_data}/{fname}_month_{t}.nc')


def plot_domain(data, pos, fig_path, fname, savefig):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = True, asp=1, fig_size_fac=2, projection=ccrs_proj)
    lon_reg=[-10,15]; lat_reg=[-45, -20]
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0, 0.5
    pyic.shade(data.lon, data.lat, data.KE.isel(time=0), ax=ax, cax=cax, clim=clim, transform=ccrs_proj,     cmap='viridis', extend='both')
    ax.plot(pos[0][0], pos[0][1],  'r*', markersize=20, transform=ccrs_proj, label='Mooring Position')
    ax.plot(pos[1][0], pos[1][1],  'r*', markersize=20, transform=ccrs_proj)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    # add legend
    ax.legend(loc='lower left')
    if savefig:
        plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')

def load_pies(path):
    #open file
    data_obs = pd.read_csv(path, sep='\s+', skiprows=5, header=None)
    #rename columns
    data_obs.columns = ['year', 'month', 'day', 'hour', 'travel_time', 'pressure', 'temperature']
    #convert to datetime
    data_obs['time'] = pd.to_datetime(data_obs[['year', 'month', 'day', 'hour']])
    #set datetime as index
    data_obs = data_obs.set_index('time')
    #convert to xarray dataset
    data_obs = data_obs.to_xarray()
    # add atributes
    data_obs['pressure'].attrs    = {'long_name': 'pressure', 'units': 'dbar'}
    data_obs['travel_time'].attrs = {'long_name': 'accustic travel time', 'units': 's'}
    
    p                       = data_obs['pressure']
    
    p                       = p * 1e4 # pressure from dbar into Pa
    rho                     = 1030
    g                       = 9.81
    data_obs['ssh_anomaly'] = (p - np.mean(p))/(rho * g)

    return data_obs

def plot_spectra_pies_welch(ds_all, fig_path, savefig):
    string = ['ies201', 'ies235', 'ies240', 'ies271', 'ies302']

    hca, hcb = pyic.arrange_axes(2,1, plot_cb = False, asp=1.1, fig_size_fac=2.8)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    nperseg = 4*30*24
    for i in range(5):
        ds = ds_all.isel(n=i)
        f, P = signal.welch(ds.ssh_anomaly.fillna(0.), fs=1/(3600*1), window='hann', nperseg=nperseg, noverlap=None, nfft=None, detrend='linear', return_onesided=True, scaling='density', axis=-1)
        ax.loglog(f[1:]*86400,P[1:], label=f'{string[i]}', lw=1.5, alpha=.7)
    plt.legend()
    ax.grid()
    ax.set_title(f'PSD of SSH anomaly at PIES 04.2021-04.2023 \n nperseg={nperseg}')
    ax.set_xlabel('frequency [cpd]')
    ax.set_ylabel('PSD [m²/s]')

    xlim_box = (1e0, 13)
    ylim_box = (5e-3,1e1)
    # draw grey box
    ax.fill_between([xlim_box[0], xlim_box[1]], ylim_box[0], ylim_box[1], color='grey', alpha=0.2)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    nperseg = 1/10*4*30*24
    for i in range(5):
        ds = ds_all.isel(n=i)
        f, P = signal.welch(ds.ssh_anomaly.fillna(0.), fs=1/(3600*1), window='hann', nperseg=nperseg, noverlap=None, nfft=None, detrend='linear', return_onesided=True, scaling='density', axis=-1)
        ax.loglog(f[1:]*86400,P[1:], label=f'{string[i]}', lw=1.5, alpha=.7)
    plt.legend()
    ax.grid()
    ax.set_title(f'zoom; nperseg={nperseg}')
    ax.set_xlabel('frequency [cpd]')

    ax.set_xlim(xlim_box)
    ax.set_ylim(ylim_box)

    if savefig ==True: plt.savefig(f'{fig_path}/spectra_ssh_pies_2.png', dpi=140, bbox_inches='tight')


def plot_spectra_pies_uchida(ds_all, fig_path, savefig):
    string = ['ies201', 'ies235', 'ies240', 'ies271', 'ies302']

    hca, hcb = pyic.arrange_axes(2,1, plot_cb = False, asp=1.1, fig_size_fac=2.8)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    T        = ds_all.time.size
    t0       = '2021-04-12T11:00:00.000000000'
    t_window = 10
    dt       = math.floor(T/t_window)
    t0       = pd.to_datetime(t0)
    time     = pd.date_range(t0, periods=t_window+1, freq=f'{dt}H')

    P = []
    for i in range(5):
        ds = ds_all.isel(n=i)
        S = []
        for yy in range(t_window):
            ds_sel = ds.sel(time=slice(time[yy], time[yy+1]))
            p = compute_spectra_uchida(ds_sel.ssh_anomaly, samp_freq=1)
            # ax.loglog(p.freq_time[1:]*86400, p[1:],  label=f'{string[i]}', lw=1, alpha=.7)
            P.append(p)
            S.append(p)
        S = xr.concat(S, dim='n')
        S_mean = S.mean(dim='n')
        ax.loglog(S_mean.freq_time[1:]*86400, S_mean[1:],  label=f'{string[i]}', lw=1, alpha=.7)
    P      = xr.concat(P, dim='string')
    P_mean = P.mean('string', skipna=True)
    ax.loglog(P_mean.freq_time[1:]*86400, P_mean[1:],  label='mean', lw=1, alpha=.7)

    ax.grid()
    ax.set_title(f'PSD of SSH PIES 04.2021-04.2023\n average window {dt}h')
    ax.set_xlabel('frequency [cpd]')
    ax.set_ylabel('PSD [m²/s]')

    xlim_box = (1e0, 13)
    ylim_box = (5e-3,1e1)
    # draw grey box
    ax.fill_between([xlim_box[0], xlim_box[1]], ylim_box[0], ylim_box[1], color='grey', alpha=0.2)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    t_window = 51
    dt       = math.floor(T/t_window)
    t0       = pd.to_datetime(t0)
    time     = pd.date_range(t0, periods=t_window+1, freq=f'{dt}H')

    P = []
    for i in range(5):
        ds = ds_all.isel(n=i)
        S = []
        for yy in range(t_window):
            ds_sel = ds.sel(time=slice(time[yy], time[yy+1]))
            p = compute_spectra_uchida(ds_sel.ssh_anomaly, samp_freq=1)
            # ax.loglog(p.freq_time[1:]*86400, p[1:],  label=f'{string[i]}', lw=1, alpha=.7)
            P.append(p)
            S.append(p)
        S = xr.concat(S, dim='n')
        S_mean = S.mean(dim='n')
        ax.loglog(S_mean.freq_time[1:]*86400, S_mean[1:],  label=f'{string[i]}', lw=1, alpha=.7)
    P      = xr.concat(P, dim='string')
    P_mean = P.mean('string', skipna=True)
    ax.loglog(P_mean.freq_time[1:]*86400, P_mean[1:],  label='mean', lw=1, alpha=.7)

    plt.legend()
    ax.grid()
    ax.set_title(f'zoom; average window {dt}h')
    ax.set_xlabel('frequency [cpd]')

    ax.set_xlim(xlim_box)
    ax.set_ylim(ylim_box)

    if savefig ==True: plt.savefig(f'{fig_path}/spectra_ssh_pies_2_uchida.png', dpi=140, bbox_inches='tight')

def plot_segment_influence(data_obs, fig_path, savefig):
    p = compute_spectra_uchida(data_obs.ssh_anomaly, samp_freq=1)
    plt.loglog(p.freq_time[1:]*86400, p[1:], label='uchida', lw=1.5, alpha=.7)
    segments = np.array([10, 100, 1000, 2000, 10000, 17291])
    for i in range(5):
        f, P = signal.welch(data_obs.ssh_anomaly.fillna(0.), fs=1/(3600*1), window='hann', nperseg=segments[5-i], noverlap=None, nfft=None, detrend='linear', return_onesided=True, scaling='density', axis=-1)
        plt.loglog(f[1:]*86400,P[1:], label=f'{segments[5-i]}', lw=1.5, alpha=.7)
    plt.legend()
    plt.title('Influence of segment length on spectra')
    plt.xlabel('frequency [cpd]')
    plt.ylabel('PSD [m²/s]')
    if savefig ==True: plt.savefig(f'{fig_path}/spectra_ssh_uchida_segment_length.png', dpi=100, bbox_inches='tight')



def compute_mooring_spectra(ds_obs, t_window, samp_freq):
    KE_obs = ds_obs.ucur**2 / 2 + ds_obs.vcur**2 /2
    T      = KE_obs.time.size
    dt     = math.floor(T/t_window)
    print(f'time steps: {T}, dt: {dt}, t_window: {t_window}')

    PP=[]

    for d in range(ds_obs.depth.size):
        P=[]
        idepth = d
        KE_sel = KE_obs.isel(depth=idepth)
        for i in range(t_window):
            ds_sel = KE_sel.isel(time=slice(i*dt, (i+1)*dt))
            p      = compute_spectra_uchida(ds_sel.fillna(0.), samp_freq=samp_freq)
            P.append(p)

        P = xr.concat(P, dim='n')
        P_mean = P.mean(dim='n')
        PP.append(P_mean)

    PP = xr.concat(PP, dim='depth')
    return PP

def plot_mooring_spectra_levs(M1, string, lat_mean, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = False, asp=1.1, fig_size_fac=2.8)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # use plasma colorbar and septrate into 16 colors
    colors = plt.cm.plasma(np.linspace(0, 1, M1.depth.size))
    for i in range(M1.depth.size):
        ds = M1.isel(depth=i)
        ax.loglog(ds.freq_time[1:]*86400, ds[1:], label=f'M 1 {ds.depth.data}m', color=colors[i], lw=1.5, alpha=.7)

    ax.set_title(f'PSD of KE: {string} ')
    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s^2]$')
    ax.set_xlim(5e-2, 5e1)
    ax.set_ylim(3e-3,1e2)
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.legend(loc='lower left', fontsize=5)


    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2 , label='Coriolis')

    xlim_box = (2e0, 5e1)
    ylim_box = (1e-2,5e-1)
    # draw grey box
    ax.fill_between([xlim_box[0], xlim_box[1]], ylim_box[0], ylim_box[1], color='grey', alpha=0.2)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    colors = plt.cm.plasma(np.linspace(0, 1, M1.depth.size))
    for i in range(M1.depth.size):
        ds = M1.isel(depth=i)
        ax.loglog(ds.freq_time[1:]*86400, ds[1:], label=f'M 1 {ds.depth.data}m', color=colors[i], lw=1.5, alpha=.7)

    ax.set_xlabel('Frequency [cpd]')
    # ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.set_title(f'zoom')

    ax.set_xlim(xlim_box)
    ax.set_ylim(ylim_box)
    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2 , label='Coriolis')
    # ax.text(corfreq(lat_mean)/2/np.pi-0.05*corfreq(lat_mean)/2/np.pi, 2e-4, 'Coriolis', fontsize=10, rotation=90, va='bottom', ha='center')

    fname    = f'spectra_KE_{string}'
    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=250, bbox_inches='tight')


def plot_mooring_spectra_levs_both(M1, M2, lat_mean, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = False, asp=1.1, fig_size_fac=2.8)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # use plasma colorbar and septrate into 16 colors
    for i in range(M2.depth.size):
        ds = M2.isel(depth=i)
        ax.loglog(ds.freq_time[1:]*86400, ds[1:], label=f'M 2 {ds.depth.data}m', color='tab:blue', lw=1, alpha=.5)

    for i in range(M1.depth.size):
        ds = M1.isel(depth=i)
        ax.loglog(ds.freq_time[1:]*86400, ds[1:], label=f'M 1 {ds.depth.data}m', color='tab:red', lw=1, alpha=.5)


    ax.set_title(f'PSD of KE ')
    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s^2]$')
    ax.set_xlim(2e-1, 5e1)
    ax.set_ylim(3e-3,2e1)
    ax.grid(which='both', linestyle='--', lw=0.3)
    # ax.legend(loc='lower left', fontsize=5)


    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2 , label='Coriolis')

    xlim_box = (2e0, 5e1)
    ylim_box = (1e-2,5e-1)
    # draw grey box
    ax.fill_between([xlim_box[0], xlim_box[1]], ylim_box[0], ylim_box[1], color='grey', alpha=0.2)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    for i in range(M2.depth.size):
        ds = M2.isel(depth=i)
        ax.loglog(ds.freq_time[1:]*86400, ds[1:], label=f'M 2 {ds.depth.data}m', color='tab:blue', lw=1, alpha=.5)

    for i in range(M1.depth.size):
        ds = M1.isel(depth=i)
        ax.loglog(ds.freq_time[1:]*86400, ds[1:], label=f'M 1 {ds.depth.data}m', color='tab:red', lw=1, alpha=.5)

    ax.set_xlabel('Frequency [cpd]')
    # ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.set_title(f'zoom')

    ax.set_xlim(xlim_box)
    ax.set_ylim(ylim_box)
    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2 , label='Coriolis')
    # ax.text(corfreq(lat_mean)/2/np.pi-0.05*corfreq(lat_mean)/2/np.pi, 2e-4, 'Coriolis', fontsize=10, rotation=90, va='bottom', ha='center')

    fname    = f'spectra_KE_subsampleinf'
    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=250, bbox_inches='tight')


def plot_mooring_spectra_levs_ave(M1, M2, lat_mean, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = False, asp=1.1, fig_size_fac=2.8)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # use plasma colorbar and septrate into 16 colors
    colors = plt.cm.Blues(np.linspace(0.5, 1, 4))
    for i in range(M2.depth_levs.size):
        d0 = i*M2.attrs['range']
        dend = (i+1)*M2.attrs['range']
        print(d0, dend)
        ds = M2.isel(depth_levs=i)
        ax.loglog(ds.freq_time[1:]*86400, ds[1:], label=f'M2 {ds.attrs["depths"][d0:dend]}', color=colors[i], lw=1, alpha=.75)

    colors = plt.cm.Reds(np.linspace(0.5, 1, 4))
    for i in range(M1.depth_levs.size):
        d0 = i*M1.attrs['range']
        dend = (i+1)*M1.attrs['range']
        print(d0, dend)
        ds = M1.isel(depth_levs=i)
        ax.loglog(ds.freq_time[1:]*86400, ds[1:], label=f'M1 {ds.attrs["depths"][d0:dend]}', color=colors[i], lw=1, alpha=.75)


    ax.set_title(f'PSD of KE ')
    ax.set_xlabel('Frequency [cpd]')
    ax.set_ylabel(r'PSD $ [m^2/s^2]$')
    ax.set_xlim(2e-1, 5e1)
    ax.set_ylim(3e-3,2e1)
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.legend(loc='upper right', fontsize=6)


    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2 , label='Coriolis')

    xlim_box = (2e0, 5e1)
    ylim_box = (1e-2,5e-1)
    # draw grey box
    ax.fill_between([xlim_box[0], xlim_box[1]], ylim_box[0], ylim_box[1], color='grey', alpha=0.2)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # use plasma colorbar and septrate into 16 colors
    colors = plt.cm.Blues(np.linspace(0.5, 1, 4))
    for i in range(M2.depth_levs.size):
        d0 = i*M2.attrs['range']
        dend = (i+1)*M2.attrs['range']
        print(d0, dend)
        ds = M2.isel(depth_levs=i)
        ax.loglog(ds.freq_time[1:]*86400, ds[1:], label=f'M2 {ds.attrs["depths"][d0:dend]}', color=colors[i], lw=1, alpha=.75)

    colors = plt.cm.Reds(np.linspace(0.5, 1, 4))
    for i in range(M1.depth_levs.size):
        d0 = i*M1.attrs['range']
        dend = (i+1)*M1.attrs['range']
        print(d0, dend)
        ds = M1.isel(depth_levs=i)
        ax.loglog(ds.freq_time[1:]*86400, ds[1:], label=f'M1 {ds.attrs["depths"][d0:dend]}', color=colors[i], lw=1, alpha=.75)

    ax.set_xlabel('Frequency [cpd]')
    # ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.set_title(f'zoom')

    ax.set_xlim(xlim_box)
    ax.set_ylim(ylim_box)
    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2 , label='Coriolis')
    # ax.text(corfreq(lat_mean)/2/np.pi-0.05*corfreq(lat_mean)/2/np.pi, 2e-4, 'Coriolis', fontsize=10, rotation=90, va='bottom', ha='center')

    fname    = f'spectra_KE_depth averaged'
    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=250, bbox_inches='tight')



def plot_mooring_spectra_levs_both_sub(M2, M2_sub, M2_sub2, lat_mean, fig_path, savefig=False):
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = False, asp=1.1, fig_size_fac=2.8)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax.loglog(M2.freq_time[1:]*86400, M2[1:], label=f'M2 1/15min', color='tab:red', lw=1, alpha=.9)
    ax.loglog(M2_sub.freq_time[1:]*86400, M2_sub[1:], label=f'M2 1/30min', color='tab:orange', lw=1, alpha=.9)
    ax.loglog(M2_sub2.freq_time[1:]*86400, M2_sub2[1:], label=f'M2 1/60min', color='tab:blue', lw=1, alpha=.9)

    ax.set_title(f'PSD of KE: \n M2: {M2.attrs} \n nreal:{M2.nreal.values} ')
    ax.set_xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    ax.set_xlim(5e-2, 5e1)
    ax.set_ylim(1e-2,1e2)
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.legend(loc='lower left', fontsize=10)


    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2 , label='Coriolis', zorder=1)

    xlim_box = (2e0, 5e1)
    ylim_box = (1e-2,5e-1)
    # draw grey box
    ax.fill_between([xlim_box[0], xlim_box[1]], ylim_box[0], ylim_box[1], color='grey', alpha=0.2)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax.loglog(M2.freq_time[1:]*86400, M2[1:], label=f'M2 1/15min', color='tab:red', lw=1, alpha=.9)
    ax.loglog(M2_sub.freq_time[1:]*86400, M2_sub[1:], label=f'M2 1/30min', color='tab:orange', lw=1, alpha=.9)
    ax.loglog(M2_sub2.freq_time[1:]*86400, M2_sub2[1:], label=f'M2 1/60min', color='tab:blue', lw=1, alpha=.9)

    ax.set_xlabel('Frequency [cpd]')
    # ax.set_ylabel(r'PSD $ [m^2/s]$')
    ax.grid(which='both', linestyle='--', lw=0.3)
    ax.set_title(f'zoom')

    ax.set_xlim(xlim_box)
    ax.set_ylim(ylim_box)
    ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=1)
    ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="gray",linewidth=2 , label='Coriolis')
    # ax.text(corfreq(lat_mean)/2/np.pi-0.05*corfreq(lat_mean)/2/np.pi, 2e-4, 'Coriolis', fontsize=10, rotation=90, va='bottom', ha='center')

    fname    = f'spectra_KE_subsampleinf_t40'
    if savefig == True: plt.savefig(f'{fig_path}/{fname}', dpi=250, bbox_inches='tight')


# gemetric function of a funnel
def funnel(x, y, x0, y0, r):
    return (x-x0)**2 + (y-y0)**2 < r**2

# %
#draw a line into south east direction
def line(x, y, x0, y0, m):
    return y > m * (x - x0) + y0

def imbuto_di_dante(X, Y, reg, ap):
    x0 = 20
    y0 = -40

    if ap == 's':
        Z1 = line(X, Y, x0, y0, -0.4)
        Z2 = line(X, Y, x0, y0, -0.8)
    elif ap == 'm':
        Z1 = line(X, Y, x0, y0, -0.09)
        Z2 = line(X, Y, x0, y0, -1.3)
    elif ap == 'l':
        Z1 = line(X, Y, x0, y0, -0.009)
        Z2 = line(X, Y, x0, y0, -2)

    if reg == 'm':
        Zc1 = funnel(X, Y, x0, y0, 13)
        Zc2 = funnel(X, Y, x0, y0, 25)  #13
    elif reg == 'ml':
        Zc1 = funnel(X, Y, x0, y0, 13)
        Zc2 = funnel(X, Y, x0, y0, 45)  #13
    elif reg == 'l':
        Zc1 = funnel(X, Y, x0, y0, 25)
        Zc2 = funnel(X, Y, x0, y0, 45)
    elif reg == 's':
        Zc1 = funnel(X, Y, x0, y0, 0)
        Zc2 = funnel(X, Y, x0, y0, 13)

    Zc = ~Zc1 * Zc2

    Z = Z1 * ~Z2 * Zc
    plt.contourf(X, Y, Z)
    return Z

def plot_domain_masked(data, mask_sa, pos, fig_path, fname, savefig):
    # lon_reg=[-25,30]; lat_reg=[-55, -15]
    lon_reg=[-10,20]; lat_reg=[-40, -25]
    asp =  (lat_reg[1]-lat_reg[0]) / (lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = True, asp=asp, fig_size_fac=2, projection=ccrs_proj)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0, 0.5
    pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, transform=ccrs_proj,     cmap='plasma', extend='both')
    ax.plot(pos[0][0], pos[0][1],  'r*', markersize=20, transform=ccrs_proj, label='Mooring Position')
    ax.plot(pos[1][0], pos[1][1],  'r*', markersize=20, transform=ccrs_proj)
    ax.pcolormesh(mask_sa.lon, mask_sa.lat, mask_sa.where(mask_sa==False), alpha=.5, cmap='Greys')
    ax.set_title(f'mean hor. KE at {data.depth.data:.0f}m')
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    # add legend
    ax.legend(loc='lower left')
    if savefig:
        plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')

def load_mooring_data():
    path         = '/work/mh0033/m300878/spectra/KE/observation_sonett_cruise/et4_adcp.nc'
    ds_1         = xr.open_dataset(path)
    datenum      = ds_1.time.values
    timestamps   = pd.to_datetime(datenum-719529, unit='D')
    ds_1['time'] = timestamps
    ds_1         = ds_1.isel(longitude=0, latitude=0)
    KE           = ds_1.ucur**2 / 2 + ds_1.vcur**2 / 2
    ds_1['KE']   = KE

    path         = '/work/mh0033/m300878/spectra/KE/observation_sonett_cruise/et3_adcp.nc'
    ds_2         = xr.open_dataset(path)
    datenum      = ds_2.time.values
    timestamps   = pd.to_datetime(datenum-719529, unit='D')
    ds_2['time'] = timestamps
    ds_2         = ds_2.isel(longitude=0, latitude=0)
    KE           = ds_2.ucur**2 / 2 + ds_2.vcur**2 / 2
    ds_2['KE']   = KE

    return ds_1, ds_2

def plot_tseries(data_exp7, data_exp8, data_exp9, ds_1, ds_2, m_pos, fig_path, savefig):
    e7_m1 = data_exp7.KE.sel(lon=m_pos[0][0], lat=m_pos[0][1], method='nearest')
    e7_m2 = data_exp7.KE.sel(lon=m_pos[1][0], lat=m_pos[1][1], method='nearest')
    e8_m1 = data_exp8.KE.sel(lon=m_pos[0][0], lat=m_pos[0][1], method='nearest')
    e8_m2 = data_exp8.KE.sel(lon=m_pos[1][0], lat=m_pos[1][1], method='nearest')
    e9_m1 = data_exp9.KE.sel(lon=m_pos[0][0], lat=m_pos[0][1], method='nearest')
    e9_m2 = data_exp9.KE.sel(lon=m_pos[1][0], lat=m_pos[1][1], method='nearest')

    #Exp7 9
    plt.figure()
    plt.title(f'TSeries: KE of Exp7 and Exp9 at mooring locations at {e7_m1.depth.data:.0f}m')
    plt.plot(e7_m1.time, e7_m1, 'tab:red', alpha=.7, label='M1 E7')
    plt.plot(e9_m1.time, e9_m1, 'tab:orange', alpha=.7, ls=':', label='M1 E9')
    plt.plot(e7_m2.time, e7_m2, 'tab:blue', alpha=.7, label='M2 E7')
    plt.semilogy(e9_m2.time, e9_m2, 'grey', alpha=.7, ls=':',label='M2 E9')
    plt.legend()
    plt.ylim(1e-5,3)
    if savefig: plt.savefig(f'{fig_path}/tseries/tseries_KE_e7_e9.png', dpi=150, bbox_inches='tight')
    # Exp8
    plt.figure()
    plt.title(f'TSeries: KE of Exp8 at mooring locations at {e8_m1.depth.data:.0f}m')
    plt.semilogy(e8_m1.time, e8_m1, 'tab:red', alpha=.7, label='M1 E8')
    plt.semilogy(e8_m2.time, e8_m2, 'tab:orange', alpha=.7, label='M2 E8')
    plt.legend()
    plt.ylim(1e-5,3)
    if savefig: plt.savefig(f'{fig_path}/tseries/tseries_KE_e8.png', dpi=150, bbox_inches='tight')
    
    # % Mooring
    plt.figure(figsize=(30,5))
    plt.title('TSeries: KE Moorings')
    plt.semilogy(ds_1.time, ds_1.KE.isel(depth=3), 'tab:red', alpha=.7, label=f'M1 obs {ds_1.KE.isel(depth=3).depth.data:.0f}m')
    plt.semilogy(ds_2.time, ds_2.KE.isel(depth=8), 'tab:blue', alpha=.7, label=f'M2 obs {ds_2.KE.isel(depth=8).depth.data:.0f}m')
    plt.legend()
    plt.ylim(1e-5,3)
    if savefig: plt.savefig(f'{fig_path}/tseries/tseries_KE_moorings.png', dpi=150, bbox_inches='tight')

    plt.figure(figsize=(30,5))
    plt.title('TSeries: KE Moorings')
    plt.semilogy(ds_1.time, ds_1.KE, 'tab:red', alpha=.1, label=f'M1 obs {ds_1.KE.isel(depth=3).depth.data:.0f}m')
    plt.semilogy(ds_2.time, ds_2.KE, 'tab:blue', alpha=.1, label=f'M2 obs {ds_2.KE.isel(depth=8).depth.data:.0f}m')
    plt.ylim(1e-5,3)
    if savefig: plt.savefig(f'{fig_path}/tseries/tseries_KE_moorings_all.png', dpi=150, bbox_inches='tight')


def plot_b_filter(x,y,b_sel, b_filtered_to_30km, b_diff, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(3,1, plot_cb=True, asp=1, dcbr=1.8, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, sharey=True, axlab_kw=None,
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = [-0.02, -0.0008]
    pyic.shade(x,y,b_sel, ax=ax, cax=cax, cmap='hot', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title('b unfiltered')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(x,y,b_filtered_to_30km, ax=ax, cax=cax, cmap='hot', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title('b filtered to 30km')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = [-0.005, 0.005]
    pyic.shade(x,y,b_diff, ax=ax, cax=cax, cmap='seismic', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title('b unfiltered b filtered to 30km')
    # set number of x ticks
    for ax in hca:
        pyic.plot_settings(ax=ax)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))

    plt.savefig(f'{fig_path}b_filtered_to_30km.png', dpi=150, bbox_inches='tight')

def plot_w_filter(x,y,w_sel, w_filtered_to_30km, w_diff, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(3,1, plot_cb='right', asp=1, dcbr=1.8, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, sharey=True, axlab_kw=None,
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    scale = 60*60*24
    clim = 60#100#0.0008
    pyic.shade(x,y,w_sel*scale, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title('w unfiltered')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(x,y,w_filtered_to_30km*scale, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title('w filtered to 30km')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # clim = 0.0008
    cb = pyic.shade(x,y,w_diff*scale, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title('w unfiltered w filtered to 30km')
    # colorbar title
    cax.set_title(r'$[m/day]$')
    for ax in hca:
        pyic.plot_settings(ax=ax)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))

    if savefig==True: plt.savefig(f'{fig_path}w_filtered_to_30km.png', dpi=150, bbox_inches='tight')


def plot_u_filter(x,y,ds_sel, ds_filtered_to_30km, ds_diff, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(3,1, plot_cb='right', asp=1, dcbr=1.8, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, sharey=True, axlab_kw=None,
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    scale = 1#60*60*24
    clim = 0.9#60#100#0.0008
    name = ds_sel.name
    pyic.shade(x,y,ds_sel*scale, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(f'{name} unfiltered')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(x,y,ds_filtered_to_30km*scale, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(f'{name} filtered to 30km')
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    # clim = 0.5
    cb = pyic.shade(x,y,ds_diff*scale, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(f'{name} unfiltered {name} filtered to 30km')
    # colorbar title
    cax.set_title(r'$[m/s]$')
    for ax in hca:
        pyic.plot_settings(ax=ax)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))

    if savefig==True: plt.savefig(f'{fig_path}{name}_filtered_to_30km.png', dpi=150, bbox_inches='tight')


def plot_wb_prime(x,y,data_sel, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb='right', asp=1, dcbr=1.8, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, sharey=True, axlab_kw=None,
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 5e-7
    pyic.shade(x,y,data_sel, ax=ax, cax=cax, cmap='seismic', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'$\overline{w^{\prime}b^{\prime}}$')

    cax.set_title(r'$m/s^2$')
    for ax in hca:
        pyic.plot_settings(ax=ax)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))

    if savefig==True: plt.savefig(f'{fig_path}wb_prime2.png', dpi=250, bbox_inches='tight')


def plot_vb_proj_prime(x,y,data_sel, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb='right', asp=1, dcbr=1.8, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, sharey=True, axlab_kw=None,
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 2e-5
    pyic.shade(x,y,data_sel, ax=ax, cax=cax, cmap='seismic', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'crossfront: $\overline{v^{\prime}b^{\prime}}$')

    cax.set_title(r'$m/s^2$')
    for ax in hca:
        pyic.plot_settings(ax=ax)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))

    if savefig==True: plt.savefig(f'{fig_path}vb_proj_prime.png', dpi=250, bbox_inches='tight')


def plot_wb_prime_MLI_uchida(aMLI, wb_prime_coars, fig_path, savefig):
    fig, (ax1,ax2) = plt.subplots(figsize=(7,3),nrows=1,ncols=2)
    fig.set_tight_layout(True)

    im1 = ax1.pcolormesh(aMLI.lon, aMLI.lat, wb_prime_coars,
                         vmin=-2e-7, vmax=2e-7, cmap='seismic', rasterized=True)
    im2 = ax2.pcolormesh(aMLI.lon, aMLI.lat, aMLI,
                         vmin=-2e-7, vmax=2e-7, cmap='seismic', rasterized=True)


    # ax1.set_xlim([-78,-68])
    # ax1.set_ylim([30,40])
    # ax2.set_xlim([-78,-68])
    # ax2.set_ylim([30,40])

    ax1.set_title(r"$\overline{w^sb^s}^z$", fontsize=14)
    ax1.set_xlabel(r"Longitude", fontsize=12)
    ax1.set_ylabel(r"Latitude", fontsize=12)
    ax2.set_title(r"$C_{PER}(t)\times$MLI", fontsize=14)
    ax2.set_xlabel(r"Longitude", fontsize=12)
    ax2.set_ylabel(r"Latitude", fontsize=12)

    cbar1 = fig.colorbar(im1, ax=ax1, shrink=.7)
    cbar2 = fig.colorbar(im2, ax=ax2, shrink=.7)
    cbar1.set_label(r"[m$^2$ s$^{-3}$]", fontsize=11)
    cbar2.set_label(r"[m$^2$ s$^{-3}$]", fontsize=11)

    if savefig==True: plt.savefig(f'{fig_path}wb_prime_diag_param_2h_stone.png', dpi=250, bbox_inches='tight')



def plot_vb_prime_MLI(aMLI, vb_prime_coars, fig_path, savefig):
    fig, (ax1,ax2) = plt.subplots(figsize=(7,3),nrows=1,ncols=2)
    fig.set_tight_layout(True)

    im1 = ax1.pcolormesh(aMLI.lon, aMLI.lat, vb_prime_coars,
                         vmin=-2e-5, vmax=2e-5, cmap='seismic', rasterized=True)
    im2 = ax2.pcolormesh(aMLI.lon, aMLI.lat, aMLI,
                         vmin=-1e-5, vmax=1e-5, cmap='seismic', rasterized=True)

    ax1.set_title(r"$\overline{\hat{v}^sb^s}^z$", fontsize=14)
    ax1.set_xlabel(r"Longitude", fontsize=12)
    ax1.set_ylabel(r"Latitude", fontsize=12)
    ax2.set_title(r"$C_{PER}(t)\times$MLI", fontsize=14)
    ax2.set_xlabel(r"Longitude", fontsize=12)
    ax2.set_ylabel(r"Latitude", fontsize=12)

    cbar1 = fig.colorbar(im1, ax=ax1, shrink=.7)
    cbar2 = fig.colorbar(im2, ax=ax2, shrink=.7)
    cbar1.set_label(r"[m$^2$ s$^{-3}$]", fontsize=11)
    cbar2.set_label(r"[m$^2$ s$^{-3}$]", fontsize=11)

    if savefig==True: plt.savefig(f'{fig_path}vb_prime_diag_param.png', dpi=250, bbox_inches='tight')


def plot_tseries_diag_param(wpbp_coar, aMLI, MLI_alpha, alpha, fig_path, savefig, var=''):
# fu.plot_tseries_diag_param(time, wpbp_coar_median, aMLI_median, MLI_alpha_median, alpha, fig_path, savefig)

    fig, ax = plt.subplots(figsize=(7,4))
    fig.set_tight_layout(True)
    ax2 = ax.twinx()

    time = wpbp_coar.time.data
    ax.plot(time, wpbp_coar, c='k', label=r"$\overline{w^sb^s}^z$")
    ax.plot(time, aMLI, c='r', label=r'$C_{PER}(t) \times MLI_{PER}$')
    ax.plot(time, MLI_alpha, c='r', ls='--', label=r'$C_{PER} \times MLI_{PER}$')

    ax2.plot(time, alpha, c='b', lw=0.5)

    # ax.set_ylim([1e-8,7.5e-8])
    ax.set_ylabel(r"[m$^2$ s$^{-3}$]", fontsize=12)

    ax2.set_ylabel(r"$C_{PER}(t)$", fontsize=12, c='b')
    ax2.spines['right'].set_color('b')
    ax2.tick_params(axis='y', colors='b')
    legend_1 = ax.legend(loc='upper right', fontsize=12)
    legend_1.remove()
    ax2.legend(loc='upper right')
    ax2.add_artist(legend_1)
    if savefig==True: plt.savefig(f'{fig_path}ub_diag_param_tseries_2h{var}.png', dpi=150, bbox_inches='tight')


def plot_tseries_diag_param_stone(wpbp_coar, aMLI, MLI_alpha, alpha, aMLI_Stone, aMLI_alpha_Stone, alpha_Stone, fig_path, savefig):
    fig, ax = plt.subplots(figsize=(7,4))
    fig.set_tight_layout(True)
    ax2 = ax.twinx()

    time = wpbp_coar.time
    # time = pd.to_datetime(time.data)
    # select day and hour
    # time = time.strftime('20%y-%m-%d-%H')
    ax.plot(time, wpbp_coar, c='k', label=r"$\overline{w^sb^s}^z$")

    ax.plot(time, aMLI, c='tab:blue', label=r'$C_{PER}(t) \times MLI_{PER}$')
    ax.plot(time, MLI_alpha, c='tab:blue', ls='--', label=r'$C_{PER} \times MLI_{PER}$')
    ax2.plot(time, alpha, c='tab:blue', ls=':', label=r'$C_{PER}(t)$')

    ax.plot(time, aMLI_Stone, c='tab:orange', label=r'$C_{ALS}(t) \times MLI_{ALS}$')
    ax.plot(time, aMLI_alpha_Stone, c='tab:orange', ls='--', label=r'$C_{ALS} \times MLI_{ALS}$')
    ax2.plot(time, alpha_Stone, c='tab:orange', ls=':', label=r'$C_{ALS}(t)$')

    ax.set_xlabel('time')
    # ax.set_ylim([1e-8,7.5e-8])
    ax.set_ylabel(r"$\overline{w^sb^s}^z$ [m$^2$ s$^{-3}$]", fontsize=9)

    # reduce number of x ticks
    # ax.set_xticks(np.arange(0, len(time), 12))
    #rotate x labels
    ax.tick_params(axis='x', rotation=15)
    #reduce string of xticks
    # ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))


    ax2.set_ylabel(r"$C(t)$", fontsize=12, c='k')
    ax2.spines['right'].set_color('k')
    ax2.tick_params(axis='y', colors='k')
    legend_1 = ax.legend(loc='lower left', fontsize=7)
    legend_1.remove()
    ax2.legend(loc='upper right', fontsize=7)
    ax2.add_artist(legend_1)
    ax.grid(which='major', linestyle='-', linewidth=0.8)
    ax.grid(which='minor', linestyle=':', linewidth=0.5)
    # Optionally, you can set the minor ticks to be more frequent
    ax = plt.gca()  # Get the current Axes instance
    ax.minorticks_on()  # Turn on the minor ticks
    ax.xaxis.set_minor_locator(plt.MultipleLocator(1))  # Set minor ticks 
    ax.yaxis.set_minor_locator(plt.MultipleLocator(1))  # Set minor ticks 
    ax.set_xlim([time[0], time[-1]])
    #an a in backets in the upper left corner
    ax.text(-0.06, 1.1, '(b)', transform=ax.transAxes, fontsize=12,   verticalalignment='top', zorder=100)

    if savefig==True: plt.savefig(f'{fig_path}wb_diag_param_tseries_2h_stone.png', dpi=150, bbox_inches='tight')



def plot_tseries_diag_param_stone_vb(wpbp_coar, aMLI, MLI_alpha, alpha, aMLI_Stone, aMLI_alpha_Stone, alpha_Stone, fig_path, savefig):
    fig, ax = plt.subplots(figsize=(7,4))
    fig.set_tight_layout(True)
    ax2 = ax.twinx()

    time = wpbp_coar.time
    # time = pd.to_datetime(time.data)
    # select day and hour
    # time = time.strftime('20%y-%m-%d-%H')
    ax.plot(time, wpbp_coar, c='k', label=r"$\overline{\hat{v}^sb^s}^z$")

    ax.plot(time, aMLI, c='tab:blue', label=r'$C_{PER}(t) \times MLI_{PER}$')
    ax.plot(time, MLI_alpha, c='tab:blue', ls='--', label=r'$C_{PER} \times MLI_{PER}$')
    ax2.plot(time, alpha, c='tab:blue', ls=':', label=r'$C_{PER}(t)$')

    ax.plot(time, aMLI_Stone, c='tab:orange', label=r'$C_{ALS}(t) \times MLI_{ALS}$')
    ax.plot(time, aMLI_alpha_Stone, c='tab:orange', ls='--', label=r'$C_{ALS} \times MLI_{ALS}$')
    ax2.plot(time, alpha_Stone, c='tab:orange', ls=':', label=r'$C_{ALS}(t)$')

    ax.set_xlabel('time')
    # ax.set_ylim([1e-8,7.5e-8])
    ax.set_ylabel(r"$\overline{\hat{v}^sb^s}^z$ [m$^2$ s$^{-3}$]", fontsize=9)

    # reduce number of x ticks
    # ax.set_xticks(np.arange(0, len(time), 12))
    #rotate x labels
    ax.tick_params(axis='x', rotation=15)
    #reduce string of xticks
    # ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))


    ax2.set_ylabel(r"$C(t)$", fontsize=12, c='k')
    ax2.spines['right'].set_color('k')
    ax2.tick_params(axis='y', colors='k')
    legend_1 = ax.legend(loc='lower left', fontsize=7)
    legend_1.remove()
    ax2.legend(loc='upper right', fontsize=7)
    ax2.add_artist(legend_1)
    ax.grid(which='major', linestyle='-', linewidth=0.8)
    ax.grid(which='minor', linestyle=':', linewidth=0.5)
    # Optionally, you can set the minor ticks to be more frequent
    ax = plt.gca()  # Get the current Axes instance
    ax.minorticks_on()  # Turn on the minor ticks
    ax.xaxis.set_minor_locator(plt.MultipleLocator(1))  # Set minor ticks 
    ax.yaxis.set_minor_locator(plt.MultipleLocator(1))  # Set minor ticks 
    #hide x ticks
    # ax.set_xticks([])
    ax.set_xlim([time[0], time[-1]])
    ax.text(-0.06, 1.1, '(a)', transform=ax.transAxes, fontsize=12,   verticalalignment='top', zorder=100)

    if savefig==True: plt.savefig(f'{fig_path}vb_diag_param_tseries_2h_stone.png', dpi=150, bbox_inches='tight')


def load_eddy_KE_spectra():
    wavelength  = 700  #choice of spatial cutoff for high pass filter in km
    shape_error = 25 #choice of shape error in km
    dlon        = 1.8
    dlat        = 1.8
    eddy_type   = 'anticyclonic'
    ID          = 0

    exp                 = 'exp9'
    root_dir            = f'/work/mh0033/m300878/eddy_tracks/agulhas_eddies/{exp}/'
    path_save_composite = f'/composite_{exp}_KE_100m/'
    eddy_dir            = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
    tracker_dir         = eddy_dir+'tracks/'

    ds_exp9 = xr.open_dataset(tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+f'deg_trackID_{ID}.nc')


    exp                 = 'exp7'
    root_dir            = f'/work/mh0033/m300878/eddy_tracks/agulhas_eddies/{exp}/'
    path_save_composite = f'/composite_{exp}_KE_100m/'
    eddy_dir            = f'{root_dir}'+'eddytrack_wv_'+str(int(wavelength))+'_se_'+str(int(shape_error))
    tracker_dir         = eddy_dir+'tracks/'

    ds_exp7 = xr.open_dataset(tracker_dir+path_save_composite+eddy_type+'_'+str(dlon)+'x'+str(dlat)+f'deg_trackID_{ID}.nc')

    return ds_exp7, ds_exp9

def average_2_levels(data, sums):
    d0   = 0
    dend = sums
    P    = []
    size = data.depth.size
    n    = size // sums
    for i in range(n):
        p                 = data.isel(depth=slice(d0, dend)).mean(dim='depth')
        p.attrs['depths'] = data.depth.values
        p.attrs['range']  = sums
        P.append(p)
        d0 += sums
        dend += sums
    P = xr.concat(P, dim='depth_levs')
    return P

def load_obs_spectra(samp_freq, t_window):
    interp_gaps = True
    # d=1 # for 100m average 0 fro 50m average
    ds_1, ds_2 = load_mooring_data()
    # m_pos = [(ds_1.longitude.data, ds_1.latitude.data), (ds_2.longitude.data, ds_2. latitude.data)]
    # lat_mean = m_pos[0][1].mean()
    # % Mooring 1

    if interp_gaps==True:
        ds_1_filled = ds_1.interpolate_na(dim='time', method='linear')
    else: ds_1_filled = ds_1

    if samp_freq == 2:
        # subsample = 4
        ds_1_sub1 = ds_1_filled.isel(time=slice(0, None, 4))
        PP_ds1_1  = compute_mooring_spectra(ds_1_sub1, t_window,     samp_freq=samp_freq)
        ds_1_sub2 = ds_1_filled.isel(time=slice(1, None, 4))
        PP_ds1_2  = compute_mooring_spectra(ds_1_sub2, t_window,     samp_freq=samp_freq)
        ds_1_sub3 = ds_1_filled.isel(time=slice(2, None, 4))
        PP_ds1_3  = compute_mooring_spectra(ds_1_sub3, t_window,     samp_freq=samp_freq)
        ds_1_sub4 = ds_1_filled.isel(time=slice(3, None, 4))
        PP_ds1_4  = compute_mooring_spectra(ds_1_sub4, t_window,     samp_freq=samp_freq)
        PP_ds1    = xr.concat([PP_ds1_1, PP_ds1_2, PP_ds1_3, PP_ds1_4], dim='n')
        PP_ds1    = PP_ds1.mean(dim='n')
    elif samp_freq == 1:
        # subsample = 2
        ds_1_sub1 = ds_1_filled.isel(time=slice(0, None, 2))
        PP_ds1_1  = compute_mooring_spectra(ds_1_sub1, t_window,     samp_freq=samp_freq)
        ds_1_sub2 = ds_1_filled.isel(time=slice(1, None, 2))
        PP_ds1_2  = compute_mooring_spectra(ds_1_sub2, t_window,     samp_freq=samp_freq)
        PP_ds1    = xr.concat([PP_ds1_1, PP_ds1_2], dim='n')
        PP_ds1    = PP_ds1.mean(dim='n')
    elif samp_freq == 'max' or samp_freq == 0.5:
        subsample = 1
        PP_ds1     = compute_mooring_spectra(ds_1_filled, t_window, samp_freq=0.5)

    # subsample every second timestetp

    if interp_gaps==True:
        ds_2_filled = ds_2.interpolate_na(dim='time', method='linear')
    else:
        ds_2_filled = ds_2

    if samp_freq == 2:
        print('subsample to 2h and average spectra')
        # subsample = 8 
        ds_2_sub1 = ds_2_filled.isel(time=slice(0, None, 8))
        PP_ds2_s1 = compute_mooring_spectra(ds_2_sub1, t_window,     samp_freq=samp_freq)
        ds_2_sub2 = ds_2_filled.isel(time=slice(1, None, 8))
        PP_ds2_s2 = compute_mooring_spectra(ds_2_sub2, t_window,     samp_freq=samp_freq)
        ds_2_sub3 = ds_2_filled.i(time=slice(2, None, 8))
        PP_ds2_s3 = compute_mooring_spectra(ds_2_sub3, t_window,     samp_freq=samp_freq)
        ds_2_sub4 = ds_2_filled.isel(time=slice(3, None, 8))
        PP_ds2_s4 = compute_mooring_spectra(ds_2_sub4, t_window,     samp_freq=samp_freq)
        ds_2_sub5 = ds_2_filled.isel(time=slice(4, None, 8))
        PP_ds2_s5 = compute_mooring_spectra(ds_2_sub5, t_window,     samp_freq=samp_freq)
        ds_2_sub6 = ds_2_filled.isel(time=slice(5, None, 8))
        PP_ds2_s6 = compute_mooring_spectra(ds_2_sub6, t_window,     samp_freq=samp_freq)
        ds_2_sub7 = ds_2_filled.isel(time=slice(6, None, 8))
        PP_ds2_s7 = compute_mooring_spectra(ds_2_sub7, t_window,     samp_freq=samp_freq)
        ds_2_sub8 = ds_2_filled.isel(time=slice(7, None, 8))
        PP_ds2_s8 = compute_mooring_spectra(ds_2_sub8, t_window,     samp_freq=samp_freq)
        PP_ds2    = xr.concat([PP_ds2_s1, PP_ds2_s2, PP_ds2_s3, PP_ds2_s4,  PP_ds2_s5, PP_ds2_s6, PP_ds2_s7, PP_ds2_s8], dim='n')
        PP_ds2    = PP_ds2.mean(dim='n')
    elif samp_freq == 1:
        print('subsample to 1h and average spectra')
        # subsample = 4
        ds_2_sub1 = ds_2_filled.isel(time=slice(0, None, 4))
        PP_ds2_s1 = compute_mooring_spectra(ds_2_sub1, t_window,     samp_freq=samp_freq)
        ds_2_sub2 = ds_2_filled.isel(time=slice(1, None, 4))
        PP_ds2_s2 = compute_mooring_spectra(ds_2_sub2, t_window,     samp_freq=samp_freq)
        ds_2_sub3 = ds_2_filled.isel(time=slice(2, None, 4))
        PP_ds2_s3 = compute_mooring_spectra(ds_2_sub3, t_window,     samp_freq=samp_freq)
        ds_2_sub4 = ds_2_filled.isel(time=slice(3, None, 4))
        PP_ds2_s4 = compute_mooring_spectra(ds_2_sub4, t_window,     samp_freq=samp_freq)
        PP_ds2    = xr.concat([PP_ds2_s1, PP_ds2_s2, PP_ds2_s3, PP_ds2_s4], dim='n')
        PP_ds2    = PP_ds2.mean(dim='n')
    elif samp_freq == 0.5:
        print('subsample to 30min and average spectra')
        # subsample = 2
        ds_2_sub1 = ds_2_filled.isel(time=slice(0, None, 2))
        PP_ds2_s1 = compute_mooring_spectra(ds_2_sub1, t_window,     samp_freq=samp_freq)
        ds_2_sub2 = ds_2_filled.isel(time=slice(1, None, 2))
        PP_ds2_s2 = compute_mooring_spectra(ds_2_sub2, t_window,     samp_freq=samp_freq)
        PP_ds2    = xr.concat([PP_ds2_s1, PP_ds2_s2], dim='n')
        PP_ds2    = PP_ds2.mean(dim='n')

    elif samp_freq == 'max':
        print('no subsampling')
        # subsample = 1
        PP_ds2 = compute_mooring_spectra(ds_2, t_window, samp_freq=0.25)

    return PP_ds1, PP_ds2


def plot_KE_spec_depths(P_exp7_1h, P_r2b9, P_exp9, P1, P2, lat_mean, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=1, fig_size_fac=5, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    alpha=1
    f = P_exp7_1h.freq_time*86400 # convert from second to day

    x = np.linspace(1e-2, 2e1, 100)
    y = 1e0*x**-2
    plt.loglog(x, y, color='black', linestyle='--', linewidth=1)
    plt.text(1.3e-2, 4.5e3, r'${-2}$', fontsize=12)
    y = 1e0*x**-3
    plt.loglog(x, y, color='black', linestyle=':', linewidth=1)
    plt.text(5.3e-2, 4.5e3, r'${-3}$', fontsize=12)

    colors = plt.cm.Greens(np.linspace(0.5, 1, P_r2b9.depth.size))
    for i in range(P_r2b9.depth.size):
        ax.loglog(P_r2b9.freq_time[1:]*86400,   P_r2b9.isel(depth=i).mean(['lon','lat'], skipna=True)[1:], color=colors[i], alpha=alpha)
    colors = plt.cm.Blues(np.linspace(0.5, 1, P_exp7_1h.depth.size))
    for i in range(P_exp7_1h.depth.size):
        ax.loglog(P_exp7_1h.freq_time[1:]*86400,   P_exp7_1h.isel(depth=i).mean(['lon','lat'], skipna=True)[1:], color=colors[i], alpha=alpha)
    # colors = plt.cm.Greys(np.linspace(0.5, 1, P_exp9.depth.size))
    for i in range(P_exp9.depth.size):
        ax.loglog(P_exp9.freq_time[1:]*86400,   P_exp9.isel(depth=i).mean(['lon','lat'], skipna=True)[1:], color='grey', alpha=alpha)

    ###################### compute freq.
    colors = plt.cm.Oranges(np.linspace(0.5, 1, P1.depth_levs.size))
    for i in range(P1.depth_levs.size):
        ax.loglog(P1.freq_time[1:]*86400, P1.isel(depth_levs=i)[1:], color=colors[i], label=f'M1 2Y', alpha=alpha)
    colors = plt.cm.Reds(np.linspace(0.5, 1, P2.depth_levs.size))
    for i in range(P2.depth_levs.size):
        ax.loglog(P2.freq_time[1:]*86400, P2.isel(depth_levs=i)[1:], color=colors[i], label=f'M2 2Y', alpha=alpha)

    plt.xlabel('Frequency [cpd]')
    plt.ylabel(r'PSD $ (m^2/s^2)/s^{-1}$')
    plt.xlim(1e-2, 1.3e1)
    plt.ylim(8e-7,3e4)
    plt.legend(loc='lower left', fontsize=8)
    plt.grid(which='both', linestyle='--', lw=0.3)
    plt.title('Power Spectral Density of KE at 98m')

    plt.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=0 )
    freq_names = tidefreqnames()
    freq_tides = tidefreq()/2/np.pi
    ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-5
    for i in range(len(freq_tides)):
        ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')

    plt.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="tab:red",linewidth=2)
    ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')

    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=150, bbox_inches='tight')




def plot_KE_spec_depths_2(P_exp7_1h, P_r2b9, P_exp9, P1, P2, lat_mean, fig_path, fname, savefig=False):
    hca, hcb = pyic.arrange_axes(1,3, plot_cb = False, asp=0.5, fig_size_fac=2.8, axlab_kw=None, daxt=0.01)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    alpha=1
    f = P_exp7_1h.freq_time*86400 # convert from second to day

    x = np.linspace(1e-2, 2e1, 100)
    y = 1e0*x**-2
    ax.loglog(x, y, color='black', linestyle='--', linewidth=1)
    # ax.text(1.3e-2, 4.5e3, r'${-2}$', fontsize=15)
    y = 1e0*x**-3
    ax.loglog(x, y, color='black', linestyle=':', linewidth=1)
    # ax.text(5.3e-2, 4.5e3, r'${-3}$', fontsize=15)

    ax.loglog(P_r2b9.freq_time[1:]*86400,   P_r2b9.isel(depth=2).mean(['lon','lat'], skipna=True)[1:], color='tab:green', alpha=alpha, label=f'{P_r2b9.isel(depth=2).depth.data:.0f}m')
    ax.loglog(P_exp7_1h.freq_time[1:]*86400,   P_exp7_1h.isel(depth=2).mean(['lon','lat'], skipna=True)[1:], color='tab:blue', alpha=alpha, label=f'{P_exp7_1h.isel(depth=2).depth.data:.0f}m')
    ax.loglog(P_exp9.freq_time[1:]*86400,   P_exp9.isel(depth=1).mean(['lon','lat'], skipna=True)[1:], color='grey', alpha=alpha, label=f'{P_exp9.isel(depth=1).depth.data:.0f}m')

    # # ###################### compute freq.
    lstring1 = ['M1 50m', 'M1 100m']
    lstring2 = ['M2 50m', 'M2 100m']

    ax.loglog(P1.freq_time[1:]*86400, P1.isel(depth_levs=1)[1:], color='tab:red', label=lstring1[1], alpha=alpha)
    ax.loglog(P2.freq_time[1:]*86400, P2.isel(depth_levs=1)[1:], color='tab:orange', label=lstring2[1], alpha=alpha)

    ax.set_ylim(2e-3,1e4)
    ax.legend(loc='lower left', fontsize=8)
    handles, labels = ax.get_legend_handles_labels()
    labels= ['ICON-R2B9-Wave', 'ICON-SMT-Wave', 'ICON-SMT', 'M1', 'M2']
    # ax.legend(handles[::-3], labels[::-3], loc='lower left', fontsize=8)
    #make second legend upper right croner
    #keep first legend
    ax.legend(handles[:5], labels[:], loc='lower left', fontsize=13, ncol=2)

    #add text with 100m upper right
    ax.text(0.9, 0.95, '100m', transform=ax.transAxes, fontsize=15, verticalalignment='top', zorder=100,)

    ax3 = ax.secondary_xaxis('top', functions=(lambda x: 1/x, lambda x: 1/x))
    ax3.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    # ax3.set_xlabel('period', fontsize=15)
    # Exchange ticks with month, week, day, hour
    ax3.set_xticks([30, 7, 1, 1/24])
    ax3.set_xticklabels(['month', 'week', 'day', 'hour'])
    #increase fontsize
    ax3.tick_params(axis='x', labelsize=15)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    # alpha=1
    # f = P_exp7_1h.freq_time*86400 # convert from second to day

    x = np.linspace(1e-2, 2e1, 100)
    y = 1e0*x**-2
    ax.loglog(x, y, color='black', linestyle='--', linewidth=1)
    # ax.text(2e-2, 1e3, r'${-2}$', fontsize=15)
    y = 1e0*x**-3
    ax.loglog(x, y, color='black', linestyle=':', linewidth=1)
    # ax.text(7e-2, 1e3, r'${-3}$', fontsize=15)

    colors = plt.cm.Greens(np.linspace(0.5, 1, P_r2b9.depth.size))
    for i in range(P_r2b9.depth[:4].size):
        ax.loglog(P_r2b9.freq_time[1:]*86400,   P_r2b9.isel(depth=i).mean(['lon','lat'], skipna=True)[1:], color=colors[i], alpha=alpha, label=f'{P_r2b9.isel(depth=i).depth.data:.0f}m')
    colors = plt.cm.Blues(np.linspace(0.5, 1, P_exp7_1h.depth.size))
    for i in range(P_exp7_1h.depth[:3].size):
        ax.loglog(P_exp7_1h.freq_time[1:]*86400,   P_exp7_1h.isel(depth=i).mean(['lon','lat'], skipna=True)[1:], color=colors[i], alpha=alpha, label=f'{P_exp7_1h.isel(depth=i).depth.data:.0f}m')
    # colors = plt.cm.Greys(np.linspace(0.5, 1, P_exp9.depth.size))
    for i in range(P_exp9.depth[:4].size):
        ax.loglog(P_exp9.freq_time[1:]*86400,   P_exp9.isel(depth=i).mean(['lon','lat'], skipna=True)[1:], color='grey', alpha=alpha, label=f'{P_exp9.isel(depth=i).depth.data:.0f}m')

    # ###################### compute freq.
    lstring1 = ['50m', '100m']
    lstring2 = ['50m', '100m']

    colors = plt.cm.Oranges(np.linspace(0.5, 1, P1.depth_levs.size))
    for i in range(P1.depth_levs.size):
        ax.loglog(P1.freq_time[1:]*86400, P1.isel(depth_levs=i)[1:], color=colors[i], label=lstring1[i], alpha=alpha)
    colors = plt.cm.Reds(np.linspace(0.5, 1, P2.depth_levs.size))
    for i in range(P2.depth_levs.size):
        ax.loglog(P2.freq_time[1:]*86400, P2.isel(depth_levs=i)[1:], color=colors[i], label=lstring2[i], alpha=alpha)

    ax.set_ylim(1e-6,1e4)
    handles, labels = ax.get_legend_handles_labels()
    #fill in blanks
    handles = [handles[0], handles[1], handles[2], handles[3],  None, handles[4], handles[5], handles[6],  None, handles[7], handles[8], handles[9], handles[10], handles[11], handles[12]]
    labels = [labels[0], labels[1], labels[2], labels[3],  None, labels[4], labels[5], labels[6],  None, labels[7], labels[8], labels[9], labels[10], labels[11], labels[12]]
    ax.legend(handles[::], labels[::], loc='lower left', fontsize=13, ncol=2)
    # ax.legend(loc='lower left', fontsize=11, ncol=5)
    ax.text(0.85, 0.95, 'upper ocean', transform=ax.transAxes, fontsize=15, verticalalignment='top', zorder=100,)

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    x = np.linspace(1e-2, 2e1, 100)
    y = 1e0*x**-2
    ax.loglog(x, y, color='black', linestyle='--', linewidth=1)
    # ax.text(1.3e-2, 4.5e3, r'${-2}$', fontsize=12)
    y = 1e0*x**-3
    ax.loglog(x, y, color='black', linestyle=':', linewidth=1)
    # ax.text(5.3e-2, 4.5e3, r'${-3}$', fontsize=12)

    colors = plt.cm.Greens(np.linspace(0.5, 1, P_r2b9.depth.size))
    for i in range(P_r2b9.depth[4:].size):
        ax.loglog(P_r2b9.freq_time[1:]*86400,   P_r2b9.isel(depth=i+4).mean(['lon','lat'], skipna=True)[1:], color=colors[i], alpha=alpha, label=f'{P_r2b9.isel(depth=i+4).depth.data:.0f}m')
    colors = plt.cm.Blues(np.linspace(0.5, 1, P_exp7_1h.depth.size))
    for i in range(P_exp7_1h.depth[3:].size):
        ax.loglog(P_exp7_1h.freq_time[1:]*86400,   P_exp7_1h.isel(depth=i+3).mean(['lon','lat'], skipna=True)[1:], color=colors[i], alpha=alpha, label=f'{P_exp7_1h.isel(depth=i+3).depth.data:.0f}m')
    # # colors = plt.cm.Greys(np.linspace(0.5, 1, P_exp9.depth.size))
    # for i in range(P_exp9.depth[4:].size):
    #     ax.loglog(P_exp9.freq_time[1:]*86400,   P_exp9.isel(depth=i+4).mean(['lon','lat'], skipna=True)[1:], color='grey', alpha=alpha, label=f'{P_exp9.isel(depth=i+4).depth.data:.0f}m')

    ax.set_ylim(1e-8,1e1)
    ax.legend(loc='lower left', fontsize=13, ncol=2)
    ax.set_xlabel('frequency [cpd]', fontsize=15)
    ax.text(0.85, 0.95, 'deep ocean', transform=ax.transAxes, fontsize=15, verticalalignment='top', zorder=100,)

    i=0
    for ax in hca:
        ax.text(-0.1, 1.02, '('+string.ascii_lowercase[i]+')', transform=ax.transAxes, size=20)
        i+=1
        #increase fontsize of sticks
        ax.tick_params(axis='both', which='major', labelsize=15)
        ax.set_ylabel(r'PSD $ (m^2/s^2)/s^{-1}$', fontsize=15)
        ax.set_xlim(1e-2, 1.3e1)
        ax.grid(which='both', linestyle='--', lw=0.3)
        # ax.set_title('Power Spectral Density of KE at 98m')
        ax.vlines(tidefreq()/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle=":", color="black", zorder=0 )
        # freq_names = tidefreqnames()
        # freq_tides = tidefreq()/2/np.pi
        # ypos = np.array([1,3,10,1,3,10,1,2,10])*2e-5
        # for i in range(len(freq_tides)):
        #     ax.text(freq_tides[i]-freq_tides[i]*0.08, ypos[i], freq_names[i], fontsize=8, rotation=90, va='bottom', ha='center')
        ax.vlines(corfreq(lat_mean)/2/np.pi,ax.get_ylim()[0],ax.get_ylim()[1],linestyle="--", color="tab:red",linewidth=2,zorder=0)
        # ax.text(corfreq(lat_mean)/2/np.pi-0.1*corfreq(lat_mean)/2/np.pi, 1.5e-2, 'f', fontsize=8, rotation=90, va='bottom', ha='center')

    # ax=hca[0]
    # ax2 = ax.twiny()
    # ax2.set_position(ax.get_position())
    # ax2.set_xscale('log')
    # ax2.set_xlim(ax.get_xlim())
    # ax2.set_xticks([1/30, 1/7, 1, 24])
    # ax2.set_xticklabels(['1/month', '1/week', '1/day', '1/hour'], fontsize=15)


    if savefig==True: plt.savefig(f'{fig_path}/{fname}.png', dpi=250, bbox_inches='tight')
