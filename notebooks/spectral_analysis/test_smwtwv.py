# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import smt_modules.maps_icon_smt_vort as smt_vort
import smt_modules.maps_icon_smt_temp as smt_temp
import funcs as fu
import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %%
name          = 'smtwv0007_oce_2d_lev_2_PT1H_20191025T001500Z.nc'
path          = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv0007/'
ds            = xr.open_dataset(path+name)
name2         = 'smtwv0007_oce_2d_lev_PT1H_20191025T001500Z.nc'
ds2           = xr.open_dataset(path+name2)
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'
# %%
ds_inter = pyic.interp_to_rectgrid_xr(ds.to.isel(depth=0), fpath_ckdtree)#
ds_inter.plot()
ds2_inter = pyic.interp_to_rectgrid_xr(ds2.to.isel(depth=0), fpath_ckdtree)#
ds2_inter.plot()


# %%
name_old = 'smtwv0007_oce_2d_PT1H_20191031T231500Z.nc'
name_new = 'smtwv0007_oce_2d_PT1H_20191101T001500Z.nc'
ds_old = xr.open_dataset(path+name_old)
ds_new = xr.open_dataset(path+name_new)
# %%
# interpolate and plot
ds_old_inter = pyic.interp_to_rectgrid_xr(ds_old.to.isel(depth=0), fpath_ckdtree)#
ds_old_inter.plot()
ds_new_inter = pyic.interp_to_rectgrid_xr(ds_new.to.isel(depth=0), fpath_ckdtree)#
ds_new_inter.plot()
# %%
# compare forcing
name_old = 'smtwv0007_oce_2d_forcing_PT1H_20191031T231500Z.nc'
name_new = 'smtwv0007_oce_2d_forcing_PT1H_20191101T001500Z.nc'
ds_old = xr.open_dataset(path+name_old)
ds_new = xr.open_dataset(path+name_new)

# %%
name = 'smtwv0007_oce_2d_forcing_PT1H_20191*'
flist     = np.array(glob.glob(path+name))
flist.sort()
flist = flist[280:400]
# %%
ds = xr.open_mfdataset(flist, chunks={'time': 1})
# %%
t0 = datetime.datetime(2019, 11, 1, 0, 0)

plt.figure()
ds.isel(ncells=20200023).HeatFlux_Total.plot()
plt.axvline(t0, color='r')

plt.figure()
ds.isel(ncells=20200023).atmos_fluxes_stress_xw.plot()
plt.axvline(t0, color='r')

plt.figure()
ds.isel(ncells=20200023).FrshFlux_Precipitation.plot()
plt.axvline(t0, color='r')

plt.figure()
ds.isel(ncells=20200023).HeatFlux_Shortwave.plot()
plt.axvline(t0, color='r')
# %%
plt.figure()
ds.isel(ncells=20200023).heatflux_rainevaprunoff.plot()
plt.axvline(t0, color='r')

#%%
plt.figure()
ds.isel(ncells=20200023).HeatFlux_Latent.plot()
# %%
# interpolate and plot
ds_old_inter = pyic.interp_to_rectgrid_xr(ds_old.FrshFlux_Precipitation, fpath_ckdtree)#
ds_old_inter.plot()
ds_new_inter = pyic.interp_to_rectgrid_xr(ds_new.FrshFlux_Precipitation, fpath_ckdtree)#
plt.figure()
ds_new_inter.plot()

# %%
name_2d       = 'smtwv0007_oce_2d_PT1H_20191101T001500Z.nc'
name_2d_lev   = 'smtwv0007_oce_2d_lev_PT1H_20191101T001500Z.nc'
name_2d_lev_2 = 'smtwv0007_oce_2d_lev_2_PT1H_20191101T001500Z.nc'
ds_2d       = xr.open_dataset(path+name_2d)
ds_2d_lev   = xr.open_dataset(path+name_2d_lev)
ds_2d_lev_2 = xr.open_dataset(path+name_2d_lev_2)
# %%
