# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='03:00:00')
cluster
client

# %%
############## plot config ##############
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/'
# fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'

fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'
lon_reg = np.array([-40, 30])
lat_reg = np.array([-55, -15])

# zoom1
lon_reg1 = np.array([-5, 10])
lat_reg1 = np.array([-42, -32])
lon_reg2 = np.array([-2, 0.5])
lat_reg2 = np.array([-38, -36.75])

savefig = False


# %% smt wave with tides
reload(eva)
ds_tides_tmean = eva.load_smt_wave_2d_exp7_07(it=1)
begin          = '2019-07-09T00:00:00'
end            = '2019-07-18T23:15:00'
p1             = ds_tides_tmean.zos.sel(time=slice(begin, end))
p1             = p1.drop_duplicates('time')
p1             = p1.isel(time=slice(0,239)) #  smtwv0007_oce_2d_PT1H_20190718T231500Z.nc defect
p1             = p1.drop('clon').drop('clat')

ds_tides_tmean_2 = eva.load_smt_wave_2d_exp7_07_part2(it=1)
begin            = '2019-07-19T00:00:00'
end              = '2019-08-31T23:15:00'
p2               = ds_tides_tmean_2.zos.sel(time=slice(begin, end))

exp7 = xr.concat([p1, p2], dim='time')
exp7 = exp7.drop_duplicates('time')

# %%

###### no tides
# ds_notides = eva.load_smt_wave_2d_exp9_07(it=1)
exp9 = eva.load_smt_wave_9('2d', it=1)
exp9 = exp9.zos

# %% tides 2022
ds_tides_22 = eva.load_smt_wave_all('2d', exp=8, it=1)
exp8         = ds_tides_22.drop('clon').drop('clat').zos
exp8         = exp8.drop_duplicates('time')

# last 6 weeks
begin   = '2022-02-18T00:00:00'
end     = '2022-03-31T23:15:00'
tstring = 'last6weeks'



# %%
# begin = '2019-07-09T00:15:00'
# end   = '2019-08-07T06:15:00'

# August
begin = '2019-08-01T00:15:00'
end   = '2019-08-31T23:15:00'
tstring = 'august'

# last 4 weeks
begin   = '2019-08-03T00:15:00'
end     = '2019-08-31T23:15:00'
tstring = 'last4weeks'

# last 14 days
begin   = '2019-08-18T00:15:00'
end     = '2019-08-31T23:15:00'
tstring = 'last14days'

# all available data
begin   = '2019-07-09T00:15:00'
end     = '2019-08-31T23:15:00'
tstring = 'all'

# last 7 days
begin   = '2019-08-25T00:15:00'
end     = '2019-08-31T23:15:00'
tstring = 'last7days'

# last 6 weeks
begin   = '2019-07-19T00:15:00'
end     = '2019-08-31T23:15:00'
tstring = 'last6weeks'


# %%
lon_reg = [-25, 30]
lat_reg = [-60, -5]
gg      = eva.load_smt_wave_grid()
res     = np.sqrt(gg.cell_area_p)
res     = res.rename(cell='ncells')
data_r  = pyic.interp_to_rectgrid_xr(res, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)

# %% interpolation and selection
# experiment 8
reload(fu)
path_2_data = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'
fname       = f'smtwv0008_spectra_ssh_{tstring}.nc'
data        = exp8.sel(time=slice(begin,end))
fu.select_interpolate_save(data, data_r, path_2_data, fname)

# %% experiment 7
reload(fu)
path_2_data = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'
fname       = f'smtwv0007_spectra_ssh_{tstring}.nc'
data        = exp7.sel(time=slice(begin,end))
fu.select_interpolate_save(data, data_r, path_2_data, fname)

# %% experiment 9
reload(fu)
path_2_data = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'
fname       = f'smtwv0009_spectra_ssh_{tstring}.nc'
data        = exp9.sel(time=slice(begin,end))
fu.select_interpolate_save(data, data_r, path_2_data, fname)

# %%
path_2_data = '/work/mh0033/m300878/spectra/ssh/'
if True:
    p_sel        = exp7.sel(time=slice(begin, end)) # same timeframe
    data_interp  = pyic.interp_to_rectgrid_xr(p_sel, fpath_ckdtree, lon_reg=lon_reg1, lat_reg=lat_reg1)
    data_interp.to_netcdf(f'{path_2_data}/smtwv0007_spectra_ssh_{tstring}.nc')

# % load smt wave without tides
    p_notides   = exp9.sel(time=slice(begin, end))
    p_notides   = p_notides.drop_duplicates('time')
    data_interp = pyic.interp_to_rectgrid_xr(p_notides, fpath_ckdtree, lon_reg=lon_reg1, lat_reg=lat_reg1)
    data_interp.to_netcdf(f'{path_2_data}/smtwv0009_spectra_ssh_{tstring}.nc')

# %%
path_2_data = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'
data_interp_tides_22 = xr.open_dataset(f'{path_2_data}/smtwv0008_spectra_ssh_{tstring}.nc')
data_interp_tides    = xr.open_dataset(f'{path_2_data}/smtwv0007_spectra_ssh_{tstring}.nc')
data_interp_notides  = xr.open_dataset(f'{path_2_data}/smtwv0009_spectra_ssh_{tstring}.nc')
# %%
# count number of non nan cells of x 
x = data_interp_notides.zos.isel(time=0)
x = x.drop('clon').drop('clat')
mask = ~np.isnan(x)
lon, lat = np.meshgrid(x.lon, x.lat)
count = np.sum(~np.isnan(x))

lat_mean = np.nanmean(lat*mask)
lat_mean = x.lat.mean().values

# %%
path_2_data         = '/work/mh0033/m300878/spectra/ssh/'
# data_interp_tides   = xr.open_dataset(f'{path_2_data}/smtwv0007_spectra_ssh_{tstring}.nc')
data_interp_tides   = xr.open_dataset(f'{path_2_data}/smtwv0007_spectra_ssh_test.nc')
data_interp_notides = xr.open_dataset(f'{path_2_data}/smtwv0009_spectra_ssh_{tstring}.nc')

# %%
reload(fu)
P_tides19   = fu.compute_spectra_uchida(data_interp_tides.zos.fillna(0.))
P_tides22   = fu.compute_spectra_uchida(data_interp_tides_22.zos.fillna(0.))
P_notides19 = fu.compute_spectra_uchida(data_interp_notides.zos.fillna(0.))

#%%
reload(fu)
fname = f'spectra_ssh_uchida_{tstring}'
fu.plot_spectra_uchida(P_tides19, P_tides22, P_notides19, lat_mean, count, fname, fig_path, savefig)



# %%
f1, Pxx1, lat_mean, count = fu.calc_spectra2(data_interp_notides.zos, samp_freq=24)
# %%
reload(fu)
f2, Pxx2, lat_mean, count = fu.calc_spectra2(data_interp_tides.zos, samp_freq=24)

# %%
f3, Pxx3, lat_mean, count = fu.calc_spectra2(data_interp_tides_22.zos, samp_freq=24)
# %%
reload(fu)
fname = f'spectra_ssh_{tstring}'
fu.plot_spectra_tides_notides(f1, Pxx1, f2, Pxx2, lat_mean, fname, fig_path, savefig=savefig)

# %%
reload(fu)
fname = f'spectra_ssh_{tstring}'
fu.plot_spectra_tides_notides2(f1, Pxx1, f2, Pxx2, f3, Pxx3, lat_mean, fname, fig_path, savefig=savefig)

# %%
if False:
    reload(fu)
    f, Pxx, lat_mean = fu.calc_spectra(data_interp)

    # %%
    reload(fu)
    fu.plot_spectra(f, Pxx, lat_mean, fig_path, savefig=savefig)

# %%
client.close()
cluster.close()
# %%


# Load data for convergence analysis
# %%
path_2_data = '/work/mh0033/m300878/spectra/ssh/'
Tstring     = ['last6weeks', 'august', 'last14days', 'last7days']

spectra_exp7 = []
spectra_exp9 = []

for tstring in Tstring:
    data_exp7  = xr.open_dataset(f'{path_2_data}/smtwv0007_spectra_ssh_{tstring}.nc')
    data_exp9  = xr.open_dataset(f'{path_2_data}/smtwv0009_spectra_ssh_{tstring}.nc')

    exp7 = fu.compute_spectra_uchida(data_exp7.zos.fillna(0.))
    exp9 = fu.compute_spectra_uchida(data_exp9.zos.fillna(0.))

    spectra_exp7.append(exp7)
    spectra_exp9.append(exp9)

exp7_6weeks = spectra_exp7[0]
exp7_august = spectra_exp7[1]
exp7_14days = spectra_exp7[2]
exp7_7days  = spectra_exp7[3]
exp9_6weeks = spectra_exp9[0]
exp9_august = spectra_exp9[1]
exp9_14days = spectra_exp9[2]
exp9_7days  = spectra_exp9[3]

# %%
reload(fu)
fname = 'spectra_ssh_convergence'
fu.plot_spectra_ssh_convergence(spectra_exp7, spectra_exp9, Tstring, fname, fig_path, savefig)
# %%
