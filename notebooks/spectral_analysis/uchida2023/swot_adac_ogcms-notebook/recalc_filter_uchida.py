# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 


# %% Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
from validate_catalog import all_params
params_dict, cat = all_params()
params_dict.keys()
import gcm_filters

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster_gpu(walltime='01:25:00', wait=False)
cluster
client

# %%
t_space       = 1
savefig       = False
lat_reg       = 30,  40
lon_reg       = -78, -68
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
fig_path      = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/'
path_2_data   = '/work/mh0033/m300878/smt/uchida_tseries/data/'

############################### Compute/Load ##############################
# %% b and lateral gradient
if False:
    ds   = eva.load_smt_b()
    ds   = ds.drop('clon').drop('clat')
    ds   = ds.isel(time=slice(0, len(ds.time), t_space))
    b    = pyic.interp_to_rectgrid_xr(ds.b, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    b.to_netcdf(path_2_data + 'b.nc')
    dbdx = pyic.interp_to_rectgrid_xr(ds.dbdx, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    dbdx.to_netcdf(path_2_data + 'dbdx.nc')
    dbdy = pyic.interp_to_rectgrid_xr(ds.dbdy, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    dbdy.to_netcdf(path_2_data + 'dbdy.nc')
else:
    b    = xr.open_dataarray(path_2_data + 'b.nc', chunks={'time': 1, 'depthi': 1})
    dbdx = xr.open_dataarray(path_2_data + 'dbdx.nc', chunks={'time': 1, 'depthi': 1})
    dbdy = xr.open_dataarray(path_2_data + 'dbdy.nc', chunks={'time': 1, 'depthi': 1})
    b    = b.compute()
    dbdx = dbdx.compute()
    dbdy = dbdy.compute()
    

# %% N2
if False:
    ds = eva.load_smt_N2()
    ds = ds.drop('clon').drop('clat')
    ds = ds.isel(time=slice(0, len(ds.time), t_space))
    N2 = pyic.interp_to_rectgrid_xr(ds.N2, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    N2.to_netcdf(path_2_data + 'N2.nc')
else:
    N2 = xr.open_dataarray(path_2_data + 'N2.nc', chunks={'time': 1, 'depthi': 1})
    N2 = N2.compute()

# %% w
if False:
    ds   = eva.load_smt_w()
    ds_w = ds.w.sel(time=b.time.data, method='nearest')
    ds_w = ds_w.drop('clon').drop('clat')
    w    = pyic.interp_to_rectgrid_xr(ds_w, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    w.to_netcdf(path_2_data + 'w.nc')
else:
    w = xr.open_dataarray(path_2_data + 'w.nc', chunks={'time': 1, 'depthi': 1})
    w = w.compute()

################################## Filter Mask ##################################
# %%
reload(eva)
mask_land = eva.load_smt_land_mask()
mask_land = pyic.interp_to_rectgrid_xr(mask_land, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land = mask_land.drop('depthc')

reload(eva)
mask_land_100 = eva.load_smt_land_mask(depthc=32)
mask_land_100 = pyic.interp_to_rectgrid_xr(mask_land_100, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land_100 = mask_land_100.drop('depthc')
# %%
b    = b * mask_land
dbdx = dbdx * mask_land
dbdy = dbdy * mask_land
M2   = np.sqrt(dbdx**2 + dbdy**2)

# %% ######################### REGULAR WITH LAND '#############################
if False:
    conversion_factor = eva.convert_degree_to_meter(int(b.lat.mean().data))
    dx_min   = (b.lat[1]-b.lat[0]).data * conversion_factor
    wet_mask = mask_land

    filter_30km = gcm_filters.Filter(
        filter_scale = 3e4,                                    # 30 km
        dx_min       = dx_min,
        filter_shape = gcm_filters.FilterShape.GAUSSIAN,
        grid_type    = gcm_filters.GridType.REGULAR_WITH_LAND,
        grid_vars    = { 'wet_mask': wet_mask}
    )
    filter_30km

else:
    # % ############################# IRRGULAR WITH LAND '#############################
    wet_mask = mask_land_100
    wet_mask = wet_mask.fillna(0) #wet mask needs to be ones and zeros

    dxw, dyw, dxs, dys, area, kappa_w, kappa_s = eva.compute_grid_vars_gcm(b, wet_mask)

    dx_min         = min(dxw.min(), dyw.min(), dxs.min(), dys.min()).data
    wet_mask[0,:]  = np.zeros_like(wet_mask[0,:] )   # fix impact from southern boundary
    wet_mask[:,-1] = np.zeros_like(wet_mask[:,-1] )  # fix impact from eastern boundary

    mask_land_100_new =  wet_mask.where(wet_mask>0)

    filter_30km      = gcm_filters.Filter(
        filter_scale = 30000,
        dx_min       = dx_min,
        filter_shape = gcm_filters.FilterShape.GAUSSIAN,
        grid_type    = gcm_filters.GridType.IRREGULAR_WITH_LAND,
        grid_vars    = {'wet_mask': wet_mask, 
                        'dxw': dxw,
                        'dyw': dyw,
                        'dxs': dxs,
                        'dys': dys, 
                        'area': area,
                        'kappa_w': kappa_w, 
                        'kappa_s': kappa_s})
    filter_30km

# %% Compute b prime
b_sel              = b[:-1] * mask_land_100_new
b_filtered_to_30km = filter_30km.apply(b_sel, dims=['lat', 'lon'])
b_filtered_to_30km = b_filtered_to_30km * mask_land_100_new
b_diff             = b_filtered_to_30km - b_sel

# %%
x = b_diff.lon
y = b_diff.lat
fu.plot_b_filter(x,y, b_sel.isel(depthi=10, time=6), b_filtered_to_30km.isel(depthi=10, time=6), b_diff.isel(depthi=10, time=6), fig_path, savefig)

# %% compute w prime
# %%
w                  = w * mask_land_100_new
w_sel              = w[:-1]
w_filtered_to_30km = filter_30km.apply(w_sel, dims=['lat', 'lon'])
w_filtered_to_30km = w_filtered_to_30km * mask_land_100_new
w_diff             = w_filtered_to_30km - w_sel

fu.plot_w_filter(x,y, w_sel.isel(depthi=10, time=6), w_filtered_to_30km.isel(depthi=10, time=6), w_diff.isel(depthi=10, time=6), fig_path, savefig)

# %% N2
N2 = N2 * mask_land_100_new


# %%  ################################### Compute MLD ##################################
if False:
    time   = b.time
    ds_T   = eva.load_smt_T()
    t      = ds_T.sel(time=time.data, method='nearest')
    data_t = pyic.interp_to_rectgrid_xr(t, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_t.to_netcdf(path_2_data + 'data_t.nc')
    ds_S   = eva.load_smt_S()
    s      = ds_S.sel(time=time.data, method='nearest')
    data_s = pyic.interp_to_rectgrid_xr(s, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_s.to_netcdf(path_2_data + 'data_s.nc')
else:
    data_t = xr.open_dataarray(path_2_data + 'data_t.nc', chunks={'time': 1, 'depthc': 1})
    data_s = xr.open_dataarray(path_2_data + 'data_s.nc', chunks={'time': 1, 'depthc': 1})

# %% 
if False:
    data_rho         = gsw.rho(data_s, data_t, depthc[2])
    data_rho_depthi_ = data_rho.interp(depthc=depthi)
    data_rho_depthi_ = data_rho_depthi_.rename(depthc='depthi')
    data_rho_depthi_.to_netcdf(path_2_data + 'data_rho_depthi_.nc')
else:
    data_rho_depthi_ = xr.open_dataarray(path_2_data + 'data_rho_depthi_.nc', chunks={'time': 1, 'depthi': 1})
# %%
data_rho_depthi_ = data_rho_depthi_.compute()
# %%
reload(eva)
# run below line parallel for each time step
# mld, mask_mld, mldx = eva.calc_mld_xr(data_rho_depthi_, depthi, threshold=0.03)

MLD      = []
Mask_MLD = []
MLDX     = []
for i in range(len(data_rho_depthi_.time)):
    mld, mask_mld, mldx = eva.calc_mld_xr(data_rho_depthi_.isel(time=i), depthi, threshold=0.2)
    MLD.append(mld)
    Mask_MLD.append(mask_mld)
    MLDX.append(mldx)

MLD      = xr.concat(MLD, dim='time')
Mask_MLD = xr.concat(Mask_MLD, dim='time')
MLDX     = xr.concat(MLDX, dim='time')

# %%
MLD = MLDX * mask_land_100_new
plt.figure()
MLD.isel(time=7).plot(vmin=0, vmax=275, cmap='Blues')
if savefig==True: plt.savefig(fig_path + 'MLD.png')


# %% ############################# Compute wb prime over MLD ############################
depthi      = xr.DataArray(depthi, dims='depthi')
dz_diff     = depthi.diff(dim='depthi')
dz_diff_ext = np.append(dz_diff, np.nan)
dz_diff_ext = xr.DataArray(dz_diff_ext, dims='depthi')

wb_dat     = ((w_sel - w_filtered_to_30km) * (b_sel - b_filtered_to_30km)) * dz_diff_ext * Mask_MLD
wb_dat_sum = wb_dat.sum(dim='depthi', skipna=True)
wb_prime   = wb_dat_sum / MLD
wb_prime   = wb_prime * mask_land_100

fu.plot_wb_prime(x,y,wb_prime.isel(time=6), fig_path, savefig)

# %% Compute MLI
f = eva.calc_coriolis_parameter(b.lat)

M2_mean = (M2 * Mask_MLD).sum(dim='depthi', skipna=True) 
# M2_mean = (M2 * mask_mld * cell_area).sum(dim='depthi', skipna=True) / cell_area
MLI = ((M2_mean * M2_mean) / f) * mask_land_100

# MLI.isel(time=6).plot(vmin =0, vmax=2e-7, cmap='viridis')
# %%  #################################### Coarsen Data ####################################
ncoars = 4
MLI_coars = MLI.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
# MLI_coars.isel(time=0).plot(vmin =0, vmax=2e-7, cmap='Reds')

wb_prime_coars = wb_prime.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
# plt.figure()
# wb_prime_coars.isel(time=0).plot(vmin=-8e-7, vmax=8e-7, cmap='RdBu_r')
# %% Compute Tuning Coefficient
Ce = wb_prime_coars / MLI_coars

plt.figure()
Ce.isel(time=6).plot(vmin=-10, vmax=10, cmap='RdBu_r')
if savefig==True:  plt.savefig(fig_path + 'Ce.png')

alpha = Ce.median(['lat', 'lon'],skipna=True)
print(alpha.data)

# %%
aMLI = MLI_coars * alpha
fu.plot_wb_prime_MLI_uchida(aMLI.isel(time=0), wb_prime_coars.isel(time=0), fig_path, savefig)
# %%
reload(fu)

if False:
    wpbp_coar = wb_prime_coars
    time             = wb_prime_coars.time.data
    wpbp_coar_median = wpbp_coar.median(['lat','lon'],skipna=True)
    wpbp_coar_median.to_netcdf(path_2_data + 'wpbp_coar_median.nc')
    aMLI_median      = aMLI.median(['lat','lon'],skipna=True)
    aMLI_median.to_netcdf(path_2_data + 'aMLI_median.nc')
    MLI_alpha_median = (alpha.mean('time')*MLI).median(['lat','lon'],skipna=True)
    MLI_alpha_median.to_netcdf(path_2_data + 'MLI_alpha_median.nc')
    alpha.to_netcdf(path_2_data + 'alpha.nc')
else:
    wpbp_coar_median = xr.open_dataarray(path_2_data + 'wpbp_coar_median.nc', chunks={'time': 1})
    aMLI_median      = xr.open_dataarray(path_2_data + 'aMLI_median.nc', chunks={'time': 1})
    MLI_alpha_median = xr.open_dataarray(path_2_data + 'MLI_alpha_median.nc', chunks={'time': 1})
    alpha            = xr.open_dataarray(path_2_data + 'alpha.nc', chunks={'time': 1})

 # %%
reload(fu)
fu.plot_tseries_diag_param(wpbp_coar_median, aMLI_median, MLI_alpha_median[:-1], alpha, fig_path, savefig)

# %% ############################### MLI Stone ##############################
N2_mean   = (N2 * Mask_MLD).sum(dim='depthi', skipna=True)
Ri        = N2_mean * f**2 / M2_mean**2
MLI_Stone = 1/np.sqrt(1+Ri) * ((M2_mean * M2_mean) / f) * mask_land_100
# MLI_Stone.isel(time=6).plot(vmin =0, vmax=2e-7, cmap='viridis')

# %%
a  = 1/np.sqrt(1+Ri) * mask_land_100_new
A = a.isel(time=6).compute()
# %%
plt.figure()
A.plot(vmin =0, vmax=1, cmap='Reds_r')
# add title
plt.title('1/sqrt(1+Ri)')
plt.savefig(fig_path + 'diff_stone_fox-kemper.png')
# %%
Ri = Ri.isel(time=6).compute() * mask_land_100_new
# %%
plt.figure()
Ri.plot(vmin =0, vmax=0.75, cmap='Blues_r')
# add title
plt.title('Ri')
plt.savefig(fig_path + 'Ri.png')
# %%
N2_mean = N2_mean.isel(time=6).compute() * mask_land_100_new
# %%
plt.figure()
N2_mean.plot(vmin=1e-4, vmax=8e-4, cmap='plasma')
plt.title('N2 MLD mean')
plt.savefig(fig_path + 'N2_mean.png')
# %%  #################################### Coarsen Data ####################################
ncoars = 4
MLI_Stone_coars = MLI_Stone.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
MLI_Stone_coars.isel(time=0).plot(vmin =0, vmax=2e-7, cmap='Reds')

# %% Compute Tuning Coefficient
Cs = wb_prime_coars / MLI_Stone_coars
plt.figure()
Cs.isel(time=6).plot(vmin=-10, vmax=10, cmap='RdBu_r')   
plt.savefig(fig_path + 'Cs.png')
alpha_Stone = Cs.median(['lat', 'lon'],skipna=True)
print(alpha.data)

# %%
savefig = True
aMLI_Stone = MLI_Stone_coars * alpha_Stone
fu.plot_wb_prime_MLI_uchida(aMLI_Stone.isel(time=0), wb_prime_coars.isel(time=0), fig_path, savefig)

# %%
if False:
    aMLI_median_Stone      = aMLI_Stone.median(['lat','lon'],skipna=True)
    aMLI_median_Stone.to_netcdf(path_2_data + 'aMLI_median_Stone.nc')
    MLI_alpha_median_Stone = (alpha_Stone.mean('time')*MLI_Stone).median(['lat','lon'],skipna=True)
    MLI_alpha_median_Stone.to_netcdf(path_2_data + 'MLI_alpha_median_Stone.nc')
    alpha_Stone.to_netcdf(path_2_data + 'alpha_Stone.nc')
else:
    aMLI_median_Stone      = xr.open_dataarray(path_2_data + 'aMLI_median_Stone.nc', chunks={'time': 1})
    MLI_alpha_median_Stone = xr.open_dataarray(path_2_data + 'MLI_alpha_median_Stone.nc', chunks={'time': 1})
    alpha_Stone            = xr.open_dataarray(path_2_data + 'alpha_Stone.nc', chunks={'time': 1})
# %%
reload(fu)
fu.plot_tseries_diag_param_stone(wpbp_coar_median, aMLI_median, MLI_alpha_median[:-1], alpha, aMLI_median_Stone, MLI_alpha_median_Stone[:-1], alpha_Stone, fig_path, savefig)

# %%
