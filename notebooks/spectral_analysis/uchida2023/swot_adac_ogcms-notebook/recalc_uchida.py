# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 


# %% Load Uchida data
import numpy as np
import xarray as xr
import pandas as pd
from scipy.ndimage import rotate
from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
%matplotlib inline
import intake
import os
import gcsfs
# add path
from validate_catalog import all_params
params_dict, cat = all_params()
params_dict.keys()

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='02:00:00')
cluster
client
# %% load hycom50 data
hycom01w     = cat.HYCOM50(region='1_GS', datatype='surf', season='fma', decode_times=False).to_dask()
hycom01s     = cat.HYCOM50(region='1_GS', datatype='surf', season='aso', decode_times=False).to_dask()
hycom50_grid = cat.HYCOM50(region='1_GS', datatype='grid', grid='grid').to_dask()
hycom50_grid

# %%
maskh = xr.DataArray(~np.ma.masked_invalid(hycom01w.ssh.isel(time=0)).mask, 
                     dims=hycom01w.ssh.isel(time=0).dims,
                     coords=hycom01w.ssh.isel(time=0).coords)

# %%
hycom01w_H = xr.DataArray(hycom01w.ssh.data*1e-2, dims=['time','lat','lon'],
                          coords={'time':np.arange(len(hycom01w.time))*3600}
                         ).chunk({'time':-1})
hycom01s_H = xr.DataArray(hycom01s.ssh.data*1e-2, dims=['time','lat','lon'],
                          coords={'time':np.arange(len(hycom01s.time))*3600}
                         ).chunk({'time':-1})

hycom01w_H

# %%
nh = 12
Fhycomw_H = xrft.power_spectrum(hycom01w_H[:,::nh,::nh].fillna(0.), 
                                dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True
                               )
Fhycomw_H = Fhycomw_H.isel(freq_time=slice(len(Fhycomw_H.freq_time)//2,None)) * 2

Fhycoms_H = xrft.power_spectrum(hycom01s_H[:,::nh,::nh].fillna(0.), 
                                dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True
                               )
Fhycoms_H = Fhycoms_H.isel(freq_time=slice(len(Fhycoms_H.freq_time)//2,None)) * 2

Fhycoms_H

# %%
import intake
import os
import gcsfs

gcs = gcsfs.GCSFileSystem(requester_pays=True)
SCRATCH = '/scratch/m/m300878/spectra_calc/'

# %%

tmp_path = f'{SCRATCH}'+f'SSH_omega_fma.zarr'
store_tmp = gcs.get_mapper(tmp_path)
Fhycomw_H.where(maskh[::nh,::nh]).mean(['lat','lon'],skipna=True).to_dataset(name='spectra').to_zarr(store_tmp, mode='w')
print(tmp_path)

tmp_path = f'{SCRATCH}'+f'SSH_omega_aso.zarr'
store_tmp = gcs.get_mapper(tmp_path)
Fhycoms_H.where(maskh[::nh,::nh]).mean(['lat','lon'],skipna=True).to_dataset(name='spectra').to_zarr(store_tmp, mode='w')
print(tmp_path)

# %% alternative

Fhycomw_H.where(maskh[::nh,::nh]).mean(['lat','lon'],skipna=True).to_netcdf(f'{SCRATCH}'+f'SSH_omega_fma.nc')
Fhycoms_H.where(maskh[::nh,::nh]).mean(['lat','lon'],skipna=True).to_netcdf(f'{SCRATCH}'+f'SSH_omega_aso.nc')

# %%
maskh.to_netcdf(f'{SCRATCH}'+f'mask.nc')
# %% FESOM
fesom01w = cat.FESOM(datatype='surf', season='fma').to_dask().chunk({'lat':100,'lon':100})
fesom01s = cat.FESOM(datatype='surf', season='aso').to_dask().chunk({'lat':100,'lon':100})
fesom01s

# %%
maskf = xr.DataArray(~np.ma.masked_invalid(fesom01w.ssh.isel(time=0)).mask, 
                     dims=fesom01w.ssh.isel(time=0).dims,
                     coords=fesom01w.ssh.isel(time=0).coords
                    )
maskf.plot()
# %%
fesom01s_H = xr.DataArray(fesom01s.ssh.data, dims=['time','lat','lon'],
                          coords={'time':np.arange(len(fesom01s.time))*3600}
                         ).chunk({'time':-1})
fesom01w_H = xr.DataArray(fesom01w.ssh.data, dims=['time','lat','lon'],
                          coords={'time':np.arange(len(fesom01w.time))*3600}
                         ).chunk({'time':-1})

fesom01s_H
# %%
nf = 16
Ffesomw_H = xrft.power_spectrum(fesom01w_H[:,::nf,::nf].chunk({'lat':9,'lon':9}).fillna(0.),
                                dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True,
                                window_correction=True
            )
Ffesomw_H = Ffesomw_H.isel(freq_time=slice(len(Ffesomw_H.freq_time)//2,None)) * 2

Ffesoms_H = xrft.power_spectrum(fesom01s_H[:,::nf,::nf].chunk({'lat':9,'lon':9}).fillna(0.),
                                dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True,
                                window_correction=True
            )
Ffesoms_H = Ffesoms_H.isel(freq_time=slice(len(Ffesoms_H.freq_time)//2,None)) * 2

Ffesoms_H
# %%
tmp_path = f'{SCRATCH}/FESOM/'+f'SSH_omega_fma.zarr'
store_tmp = gcs.get_mapper(tmp_path)
Ffesomw_H.where(maskf[::nf,::nf]).mean(['lat','lon'], skipna=True).to_dataset(name='spectra').to_zarr(store_tmp, mode='w')
print(tmp_path)

tmp_path = f'{SCRATCH}/FESOM/'+f'SSH_omega_aso.zarr'
store_tmp = gcs.get_mapper(tmp_path)
Ffesoms_H.where(maskf[::nf,::nf]).mean(['lat','lon'], skipna=True).to_dataset(name='spectra').to_zarr(store_tmp, mode='w')
print(tmp_path)

# %% alternative

Ffesomw_H.where(maskf[::nf,::nf]).mean(['lat','lon'],skipna=True).to_netcdf(f'{SCRATCH}/FESOM/'+f'SSH_omega_fma.nc')
Ffesoms_H.where(maskf[::nf,::nf]).mean(['lat','lon'],skipna=True).to_netcdf(f'{SCRATCH}/FESOM/'+f'SSH_omega_aso.nc')
maskf.to_netcdf(f'{SCRATCH}/FESOM/'+f'mask.nc')
# %%
#### eNATL60
enatl01w = cat.eNATL60(region='1',datatype='surface_hourly', season='fma').to_dask()
enatl01s = cat.eNATL60(region='1',datatype='surface_hourly', season='aso').to_dask()
enatl01w
# %%
enatl01s_H = xr.DataArray(enatl01s.sossheig.data, dims=['time','y','x'],
                          coords={'time':np.arange(len(enatl01s.time_counter))*3600}
                         ).chunk({'time':-1})
enatl01w_H = xr.DataArray(enatl01w.sossheig.data, dims=['time','y','x'],
                          coords={'time':np.arange(len(enatl01w.time_counter))*3600}
                         ).chunk({'time':-1})
enatl01s_H
# %%
ne = 15
Fenatlw_H = xrft.power_spectrum(enatl01w_H[:,::ne,::ne].fillna(0.),
                                dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True,
                                window_correction=True
                               )
Fenatlw_H = Fenatlw_H.isel(freq_time=slice(len(Fenatlw_H.freq_time)//2,None)) * 2

Fenatls_H = xrft.power_spectrum(enatl01s_H[:,::ne,::ne].fillna(0.),
                                dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True,
                                window_correction=True
                               )
Fenatls_H = Fenatls_H.isel(freq_time=slice(len(Fenatls_H.freq_time)//2,None)) * 2

Fenatls_H
# %%
tmp_path = f'{SCRATCH}/region01/eNATL60/'+f'SSH_omega_fma.zarr'
store_tmp = gcs.get_mapper(tmp_path)
Fenatlw_H.where(enatl01w.tmask[::ne,::ne]!=0.).mean(['y','x'],skipna=True).to_dataset(name='spectra').to_zarr(store_tmp, mode='w')
print(tmp_path)

tmp_path = f'{SCRATCH}/region01/eNATL60/'+f'SSH_omega_aso.zarr'
store_tmp = gcs.get_mapper(tmp_path)
Fenatls_H.where(enatl01s.tmask[::ne,::ne]!=0.).mean(['y','x'],skipna=True).to_dataset(name='spectra').to_zarr(store_tmp, mode='w')
print(tmp_path)

# %% alternative
Fenatlw_H.where(enatl01w.tmask[::ne,::ne]!=0.).mean(['y','x'],skipna=True).to_netcdf(f'{SCRATCH}/eNATL60/'+f'SSH_omega_fma.nc')
Fenatls_H.where(enatl01s.tmask[::ne,::ne]!=0.).mean(['y','x'],skipna=True).to_netcdf(f'{SCRATCH}/eNATL60/'+f'SSH_omega_aso.nc')
# %%
enatl01w.tmask.to_netcdf(f'{SCRATCH}/eNATL60/'+f'mask.nc')

# %%
##### ORCA36

orca01w = cat.ORCA36(region='1',datatype='surface_hourly', season='fma').to_dask().isel(y=slice(None,450))
orca01s = cat.ORCA36(region='1',datatype='surface_hourly', season='aso').to_dask().isel(y=slice(None,450))
orca01_grid = cat.ORCA36(region='1',datatype='grid',grid='meshmask').to_dask().isel(time_counter=0,y=slice(None,450))
orca01_grid

# %%
orca01s_H = xr.DataArray(orca01s.sossheig.data, 
                         dims=['time','y','x'],
                         coords={'time':np.arange(len(orca01s.time_counter))*3600}
                        ).chunk({'time':-1})
orca01w_H = xr.DataArray(orca01w.sossheig.data, 
                         dims=['time','y','x'],
                         coords={'time':np.arange(len(orca01w.time_counter))*3600}
                        ).chunk({'time':-1})
orca01s_H
# %%
no = 10

Forcaw_H = xrft.power_spectrum(orca01w_H[:,::no,::no].fillna(0.),
                               dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True,
                               window_correction=True
                   )
Forcaw_H = Forcaw_H.isel(freq_time=slice(len(Forcaw_H.freq_time)//2,None)) * 2

Forcas_H = xrft.power_spectrum(orca01s_H[:,::no,::no].fillna(0.), 
                               dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True,
                               window_correction=True
                   )
Forcas_H = Forcas_H.isel(freq_time=slice(len(Forcas_H.freq_time)//2,None)) * 2

Forcas_H
# %%
tmp_path = f'{SCRATCH}/region01/ORCA36/'+f'SSH_omega_fma.zarr'
store_tmp = gcs.get_mapper(tmp_path)
Forcaw_H.where(orca01_grid.tmask[0,::no,::no]).mean(['y','x'],skipna=True).to_dataset(name='spectra').to_zarr(store_tmp, mode='w')
print(tmp_path)

tmp_path = f'{SCRATCH}/region01/ORCA36/'+f'SSH_omega_aso.zarr'
store_tmp = gcs.get_mapper(tmp_path)
Forcas_H.where(orca01_grid.tmask[0,::no,::no]).mean(['y','x'],skipna=True).to_dataset(name='spectra').to_zarr(store_tmp, mode='w')
print(tmp_path)

# %% alternative
Forcaw_H.where(orca01_grid.tmask[0,::no,::no]).mean(['y','x'],skipna=True).to_netcdf(f'{SCRATCH}/ORCA36/'+f'SSH_omega_fma.nc')
Forcas_H.where(orca01_grid.tmask[0,::no,::no]).mean(['y','x'],skipna=True).to_netcdf(f'{SCRATCH}/ORCA36/'+f'SSH_omega_aso.nc')
# %%
orca01_grid.tmask.to_netcdf(f'{SCRATCH}/ORCA36/'+f'mask.nc')
# %%
###### FIOCOM32
fio01w = cat['FIO-COM32'](region='1',datatype='surface_hourly', season='fma'
                      ).to_dask()
fio01s = cat['FIO-COM32'](region='1',datatype='surface_hourly', season='aso'
                      ).to_dask()
# %%
fio01s_H = xr.DataArray(fio01s.eta_t.data, dims=['time','yt_ocean','xt_ocean'],
                        coords={'time':np.arange(len(fio01s.time))*3600}
                       ).chunk({'time':-1})
fio01w_H = xr.DataArray(fio01w.eta_t.data, dims=['time','yt_ocean','xt_ocean'],
                        coords={'time':np.arange(len(fio01w.time))*3600}
                       ).chunk({'time':-1})
fio01s_H
# %%
nfi = 3
Ffiow_H = xrft.power_spectrum(fio01w_H[:,::nfi,::nfi].fillna(0.),
                              dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True,
                              window_correction=True
                             )
Ffiow_H = Ffiow_H.isel(freq_time=slice(len(Ffiow_H.freq_time)//2,None)) * 2

Ffios_H = xrft.power_spectrum(fio01s_H[:,::nfi,::nfi].fillna(0.),
                              dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True,
                              window_correction=True
                             )
Ffios_H = Ffios_H.isel(freq_time=slice(len(Ffios_H.freq_time)//2,None)) * 2

Ffios_H
# %%
mask = np.ma.masked_invalid(fio01w.surface_temp.isel(time=0)).mask
# %%
tmp_path = f'{SCRATCH}/region01/FIO-COM32/'+f'SSH_omega_fma.zarr'
store_tmp = gcs.get_mapper(tmp_path)
Ffiow_H.where(~mask[::nfi,::nfi]).mean(['yt_ocean','xt_ocean'],skipna=True).to_dataset(name='spectra').to_zarr(store_tmp, mode='w')
print(tmp_path)

tmp_path = f'{SCRATCH}/region01/FIO-COM32/'+f'SSH_omega_aso.zarr'
store_tmp = gcs.get_mapper(tmp_path)
Ffios_H.where(~mask[::nfi,::nfi]).mean(['yt_ocean','xt_ocean'],skipna=True).to_dataset(name='spectra').to_zarr(store_tmp, mode='w')
print(tmp_path)

# %% alternative
Ffiow_H.where(~mask[::nfi,::nfi]).mean(['yt_ocean','xt_ocean'],skipna=True).to_netcdf(f'{SCRATCH}/FIO-COM32/'+f'SSH_omega_fma.nc')
Ffios_H.where(~mask[::nfi,::nfi]).mean(['yt_ocean','xt_ocean'],skipna=True).to_netcdf(f'{SCRATCH}/FIO-COM32/'+f'SSH_omega_aso.nc')
# %%
