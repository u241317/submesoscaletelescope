# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import math

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
cluster
client

# %%
############## plot config ##############
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/'

lon_reg = np.array([-40, 30])
lat_reg = np.array([-55, -15])

savefig = False

# %% load mooring data
reload(fu)
path     = '/work/mh0033/m300878/spectra/ssh/observation/pies/ies201_004.dat'
ds_201   = fu.load_pies(path)
path     = '/work/mh0033/m300878/spectra/ssh/observation/pies/ies235_004.dat'
ds_235   = fu.load_pies(path)
path     = '/work/mh0033/m300878/spectra/ssh/observation/pies/ies240_004.dat'
ds_240   = fu.load_pies(path)
path     = '/work/mh0033/m300878/spectra/ssh/observation/pies/ies271_004.dat'
ds_271   = fu.load_pies(path)
path     = '/work/mh0033/m300878/spectra/ssh/observation/pies/ies302_005.dat'
ds_302   = fu.load_pies(path)

begin ='2021-04-12T11:00:00.000000000'
end   ='2023-03-31T19:00:00.000000000'
ds_pies = xr.concat([ds_201.sel(time=slice(begin, end)), ds_235.sel(time=slice(begin, end)), ds_240.sel(time=slice(begin, end)), ds_271.sel(time=slice(begin, end)), ds_302.sel(time=slice(begin, end))], dim='n')
# common time
# %%
reload(fu)
fu.plot_spectra_pies_welch(ds_pies, fig_path, savefig)
# %%
reload(fu)
fu.plot_spectra_pies_uchida(ds_pies, fig_path, savefig)

# %% Methods
reload(fu)
fu.plot_segment_influence(ds_201, fig_path, savefig)


# %%
tstring                = 'last6weeks'
path_2_data            = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'
data_interp_tides_22   = xr.open_dataset(f'{path_2_data}/smtwv0008_spectra_ssh_{tstring}.nc')
tstring                = 'J-O'
data_interp_notides    = xr.open_dataset(f'{path_2_data}/smtwv0009_spectra_ssh_{tstring}.nc')
tstring                = 'July_2019-January_2020'
data_interp_tides      = xr.open_dataset(f'{path_2_data}/smtwv0007_spectra_ssh_{tstring}_full_year.nc')

# %%
path_r2b9 = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/r2b9/r2b9_ssh_tseries*'
flist    = np.array(glob.glob(path_r2b9))
flist.sort()
data_r2b9 = xr.open_mfdataset(flist, combine='nested', concat_dim='time', parallel=False) #decode_cf speeds up by 100 times but destroy timestamp
#rechunk to single time chunk
data_r2b9 = data_r2b9.chunk(dict(time=-1))
#seelect same time as data_interp_tides
data_r2b9 = data_r2b9.sel(time=slice(data_interp_tides.zos.time[0], data_interp_tides.zos.time[-1]))
# %%
reload(fu)
P_tides19   = fu.compute_spectra_uchida(data_interp_tides.zos.fillna(0.))
P_tides22   = fu.compute_spectra_uchida(data_interp_tides_22.zos.fillna(0.))
P_notides19 = fu.compute_spectra_uchida(data_interp_notides.zos.fillna(0.))
P_tides_r2b9 = fu.compute_spectra_uchida(data_r2b9.zos.fillna(0.))

# %% add tstring

t0 = data_interp_tides.zos.time[0].values
t1 = data_interp_tides.zos.time[-1].values

P_tides19['tstring']   = f'{pd.to_datetime(t0).strftime("%Y-%m-%d")}-{pd.to_datetime(t1).strftime("%Y-%m-%d")}'

t0 = data_interp_tides_22.zos.time[0].values
t1 = data_interp_tides_22.zos.time[-1].values
P_tides22['tstring']   = f'{pd.to_datetime(t0).strftime("%Y-%m-%d")}-{pd.to_datetime(t1).strftime("%Y-%m-%d")}'

t0 = data_interp_notides.zos.time[0].values
t1 = data_interp_notides.zos.time[-1].values
P_notides19['tstring'] = f'{pd.to_datetime(t0).strftime("%Y-%m-%d")}-{pd.to_datetime(t1).strftime("%Y-%m-%d")}'

t0 = data_r2b9.zos.time[0].values
t1 = data_r2b9.zos.time[-1].values
P_tides_r2b9['tstring'] = f'{pd.to_datetime(t0).strftime("%Y-%m-%d")}-{pd.to_datetime(t1).strftime("%Y-%m-%d")}'

# %% ########################## Evaluation of South Atlantic ############################
# % get mask 
# count number of non nan cells of x 
x           = data_interp_notides.zos.isel(time=0)
x           = x.drop('clon').drop('clat')
mask_res600 = ~np.isnan(x)
# count       = np.sum(~np.isnan(x))

#mooring positions
m_pos       = np.array([(7.11666667, -32.69666667),(4.63333333, -32.18833333)])
pos         = np.mean(m_pos, axis=0)
radius      = 10#2
data_interp = data_interp_tides_22.zos.isel(time=0).drop('time')
mask_circle = (data_interp.lon - pos[0])**2 + (data_interp.lat - pos[1])**2 < radius**2
count       = np.sum(mask_circle)

if False:
    plt.figure()
    mask_res600.plot(x='lon',y='lat')
    plt.xlim(-10,15)
    plt.ylim(-45,-20)
    #plot circle into map
    plt.plot(pos[0], pos[1], 'ro', markersize=5)
    circle = plt.Circle((pos[0], pos[1]), radius, color='r', fill=False)
    plt.gca().add_artist(circle)

mask_sa = mask_circle

# %%
reload(fu)
lat_mean = pos[1]
fname    = f'spectra_ssh_uch1da_pies_SA_25real_large'
fname    = 'ssh_spectra_sa_update'
fu.plot_spectra_uchida_sa2(P_tides19.where(mask_sa), P_tides22.where(mask_sa), P_notides19.where(mask_sa), P_tides_r2b9.where(mask_sa), ds_pies, lat_mean, count, fname, fig_path, savefig)



    # %% #old dont use
if False:
    reload(fu)
    lat_mean = -32
    fname = f'spectra_ssh_uchida_{tstring}'
    fu.plot_spectra_uchida(P_tides19, P_tides22, P_notides19, lat_mean, count, fname, fig_path, savefig)
    reload(fu)
    fname    = f'spectra_ssh_uch1da_5all_{tstring}_SA'
    fu.plot_spectra_uchida_sa(P_tides19.where(mask_sa), P_tides22.where(mask_sa), P_notides19.where(mask_sa), data_obs, lat_mean, count, fname, fig_path, savefig)

# %% ########################## Evaluation of Seasonality ############################
# interval for seasonality eval
# begin   = '2019-07-09T00:00:00' #defect file in first week
begin   = '2019-07-20T00:15:00' #broken/heavy influence data at 07-19
end     = '2020-06-13T23:15:00'

days  = (pd.to_datetime(end) - pd.to_datetime(begin)).days
chunk = round(days/4)
# devide into 3 equal time chunks

t0 = pd.to_datetime(begin)
t1 = t0 + pd.DateOffset(days=chunk)
t2 = t1 + pd.DateOffset(days=chunk)
t3 = t2 + pd.DateOffset(days=chunk)
t4 = t3 + pd.DateOffset(days=chunk)

# %%
ds  = data_interp_tides.zos.fillna(0.)

ds0 = ds.sel(time=slice(t0, t1))
ds1 = ds.sel(time=slice(t1, t2))
ds2 = ds.sel(time=slice(t2, t3))
ds3 = ds.sel(time=slice(t3, t4))

P_0   = fu.compute_spectra_uchida(ds0)
P_1   = fu.compute_spectra_uchida(ds1)
P_2   = fu.compute_spectra_uchida(ds2)
P_3   = fu.compute_spectra_uchida(ds3)
# add timestrings to P_i
P_0['tstring'] = f'{t0.strftime("%Y-%m-%d")}-{t1.strftime("%Y-%m-%d")}'
P_1['tstring'] = f'{t1.strftime("%Y-%m-%d")}-{t2.strftime("%Y-%m-%d")}'
P_2['tstring'] = f'{t2.strftime("%Y-%m-%d")}-{t3.strftime("%Y-%m-%d")}'
P_3['tstring'] = f'{t3.strftime("%Y-%m-%d")}-{t4.strftime("%Y-%m-%d")}'

# %%
reload(fu)
# begin  = '2021-06-20T00:15:00'
# end    = '2021-09-22T00:15:00'
# ds_sel = ds_pies.sel(time=slice(begin,end))
ds_sel = ds_pies

fname  = 'ssh_spec_seasons_update'
fu.plot_spectra_seasons(P_0.where(mask_sa), P_1.where(mask_sa), P_2.where(mask_sa), P_3.where(mask_sa), lat_mean, count, fname, fig_path, savefig)


# %% ########################## Evaluation of North Atlantic ############################
data_interp_notides_10 = xr.open_dataset(f'{path_2_data}/smt0000_spectra_ssh_rect.nc')
P_notides10            = fu.compute_spectra_uchida(data_interp_notides_10.h_sp.fillna(0.), samp_freq=2)

ds_ref_w, ds_ref_s, strings = fu.load_spectra_uchida()

# % get mask for NA
x         = data_interp_notides_10.h_sp.isel(time=0)
count     = np.sum(~np.isnan(x))
mask_natl = ~np.isnan(x)

# %%
reload(fu)
lat_mean = 40
fname    = f'spectra_ssh_uchida_na_notides_2'
fu.plot_spectra_uchida_NA(P_notides10.where(mask_natl), ds_ref_w, lat_mean, count, fname, fig_path, savefig)

# %%
reload(fu)
lat_mean = 40
fname    = f'spectra_ssh_uchida_na_notides_zoom'
fu.plot_spectra_uchida_NA_zoom(P_notides10.where(mask_natl), ds_ref_w, lat_mean, count, fname, fig_path, savefig)



# %%
data = data_interp_tides.zos.drop('clon').drop('clat')
data = data.mean('time', skipna=True)
reg = 'ml'
ap  = 'm'
Z   = fu.imbuto_di_dante(data.lon, data.lat, reg, ap)
m_pos = np.array([(7.11666667, -32.69666667),(4.63333333, -32.18833333)])
fname = f'domain_{reg}_ap{ap}'
fu.plot_domain_masked(data, mask_sa, m_pos, fig_path, fname, savefig)


    # %%
if False:
    f1, Pxx1, lat_mean, count = fu.calc_spectra2(data_interp_notides.zos, samp_freq=24)
    # %%
    reload(fu)
    f2, Pxx2, lat_mean, count = fu.calc_spectra2(data_interp_tides.zos, samp_freq=24)

    # %%
    f3, Pxx3, lat_mean, count = fu.calc_spectra2(data_interp_tides_22.zos, samp_freq=24)
    # %%
    reload(fu)
    fname = f'spectra_ssh_{tstring}'
    fu.plot_spectra_tides_notides(f1, Pxx1, f2, Pxx2, lat_mean, fname, fig_path, savefig=savefig)

    # %%
    reload(fu)
    fname = f'spectra_ssh_{tstring}'
    fu.plot_spectra_tides_notides2(f1, Pxx1, f2, Pxx2, f3, Pxx3, lat_mean, fname, fig_path, savefig=savefig)

    # %%
    reload(fu)
    f, Pxx, lat_mean = fu.calc_spectra(data_interp)

    # %%
    reload(fu)
    fu.plot_spectra(f, Pxx, lat_mean, fig_path, savefig=savefig)

# %%
client.close()
cluster.close()
# %%
