# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import funcs_filter as fu
sys.path.insert(0, '../')

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

import dask.array as da
import xrft
import numpy as np
import matplotlib
from scipy.signal import butter,filtfilt
from multiprocessing import Pool

# %%
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='02:00:00', wait=False)
cluster
client

# %%
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/bandpass_filtered/section/N1/video/'
savefig        = False

# %%
path_data = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/exp7_KE_100m_fine.nc'
ds        = xr.open_dataset(path_data, chunks={'lon':10, 'lat':10})


# %% ##################### load time series ########################
# %%
idepth     = 26
idepth_lev = 2

begin = '2019-07-06T01:15:00'
end   = '2019-10-17T23:15:00'

ds_u1     = eva.load_smt_wave_all('u', exp=7, remove_bnds=True)
ds_u1     = ds_u1.isel(depth=idepth).u
ds_u1     = ds_u1.drop('clon').drop('clat')
ds_levels = eva.load_smt_wave_levels(7, it=2)
ds_levels = ds_levels.rename({'ncells_2': 'ncells'})
ds_u2     = ds_levels.isel(depth=idepth_lev).u
ds_u      = xr.concat([ds_u1, ds_u2], dim='time').sel(time=slice(begin, end)) 

ds_v1 = eva.load_smt_wave_all('v', exp=7, remove_bnds=True)
ds_v1 = ds_v1.isel(depth=idepth).v
ds_v1 = ds_v1.drop('clon').drop('clat')

time_missing = '2019-07-08T01:15:00' #20190708T0115
infilled_data_set = ds_v1.resample(time="2H").asfreq()
infilled_data_set = infilled_data_set.assign_coords(time=infilled_data_set.time.data + (ds_v1.time.data[0] - infilled_data_set.time.data[0]))

ds_v2 = ds_levels.isel(depth=idepth_lev).v
ds_v  = xr.concat([infilled_data_set, ds_v2], dim='time').sel(time=slice(begin, end)) 

# %%
time = pd.to_datetime(ds.KE.isel(time=503).time.values)
to = ds_levels.to.sel(time=time, method='nearest').isel(depth=1)

fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'

to_interp = pyic.interp_to_rectgrid_xr(to, fpath_ckdtree= fpath_ckdtreeZ, lon_reg=[-15,12], lat_reg=[-32,-25])
to_interp.plot(cmap='plasma', vmin=16,vmax=19)

# %%
path = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv0007/outdata_2d/smtwv0007_oce_2d_PT1H_20190901T011500Z.nc'
ds_ssh = xr.open_dataset(path)

ssh_interp = pyic.interp_to_rectgrid_xr(ds_ssh.zos, fpath_ckdtree= fpath_ckdtreeZ, lon_reg=[7.5,12.5], lat_reg=[-26,-24])
# %%
(ssh_interp - ssh_interp.mean()).plot(vmin=-0.5, vmax=0.5, cmap='RdBu_r')
# %%
data        = ds_u**2 /2 + ds_v**2 /2
data_tsel   = data.isel(time=slice(0,data.time.shape[0],1))
# %%
time0   = pd.to_datetime('2019-08-28T21:15:00')
time1   = pd.to_datetime('2019-08-31T21:15:00')

data_tsel = data_tsel.sel(time=slice(time0, time1))

#%%
lon_cyclone = 7.8,11
lat_cyclone = -26.5,-23.5
# quater
# lon_cyclone = 9.4,10.6
# lat_cyclone = -25,-24

# %
# location anticyclone
lon_reg = lon_cyclone
lat_reg = lat_cyclone
lon_reg = [6,7]
lat_reg = [-31,-30]
lon_reg = [5,7]
lat_reg = [-32,-30]
# anticyclone aand cyclone t503
lat_reg = [-36,-32.1]
lon_reg = [10.5,14]
lat_reg = [-36.5,-32]#[-36,-32.1]
lon_reg = [8.5,14.5]

#Anticyclone N1 end august
# lon_reg = [3,6.4]
# lat_reg = [-32,-28.5]

lon_reg = [10.2,13.2]
lat_reg = [-36.5, -33.5]

# %%
reload(fu)

ind_reg, vertex_of_cell_reg, vlon, vlat = fu.crop_grid(lon_reg, lat_reg)
#%%
DATA = data_tsel[:,ind_reg].compute()
# %%
%%time
# DATA = DATA_all.isel(time=slice(665,700))

sampling_freq = 1/2 # 1/(2h)
cutoff        = 1/6 # 1/(6h)
order         = 10
btype         = 'high'

y    = fu.butter_filter(DATA.fillna(0.), cutoff, sampling_freq, order, btype=btype)
y_xr = xr.DataArray(y, dims=['time', 'ncells'], coords={'time': DATA.time, 'ncells': DATA.ncells})

# %%
reload(fu)
data = y[20,:] #503 #694
Tri = matplotlib.tri.Triangulation(vlon, vlat, triangles=vertex_of_cell_reg)
fig_path = f'/home/m/m300878/submesoscaletelescope/results/smt_wave/bandpass_filtered/filter_test/'
fu.plot_filtered_field_hires(Tri, data, cutoff, lon_reg, lat_reg, btype, fig_path, savefig)


# %% #### Bandpass Filter
%%time
sampling_freq = 1/2 # 1/(2h)
cutoff        = np.array([1/26, 1/20]) # 1/(6h)
order         = 5
btype         = 'bandpass'

y    = fu.butter_filter(DATA.fillna(0.), cutoff, sampling_freq, order, btype=btype)
y_xr = xr.DataArray(y, dims=['time', 'ncells'], coords={'time': DATA.time, 'ncells': DATA.ncells})

# %%
reload(fu)
data = y[20,:] #503 #694
Tri = matplotlib.tri.Triangulation(vlon, vlat, triangles=vertex_of_cell_reg)
fig_path = f'/home/m/m300878/submesoscaletelescope/results/smt_wave/bandpass_filtered/filter_test/'
fu.plot_filtered_field_hires(Tri, data, cutoff, lon_reg, lat_reg, btype, fig_path, savefig)





# %%
reload(fu)
# data = y[694,:] #503
data = y[-6,:] #503
Tri = matplotlib.tri.Triangulation(vlon, vlat, triangles=vertex_of_cell_reg)
fu.plot_filtered_field_acyc(Tri, data, cutoff, lon_reg, lat_reg, btype, fig_path, savefig)




# %% ############################ Animation #########################
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
plt.rcParams['animation.ffmpeg_path'] = '/work/mh0033/m300602/miniconda3/envs/pyicon_py39_exp/bin/ffmpeg'
FFwriter = animation.FFMpegWriter(fps=5, extra_args=['-vcodec', 'libx264'])

Tri  = matplotlib.tri.Triangulation(vlon, vlat, triangles=vertex_of_cell_reg)
asp  = (lat_reg[1]-lat_reg[0]) / (lon_reg[1]-lon_reg[0])
clim = 0.01


# def update(frame):
#     hm3[0].set_array(y_xr.isel(time=frame).data.flatten())
#     title.set_text(f'Eddy section {y_xr.time[frame].values:.13}')
#     # return hm3

run='exp7'
for frame in range(0, y_xr.time.shape[0], 1):
    if frame==0:
        plt.close('all')
        hca, hcb = pyic.arrange_axes(1,1, plot_cb = True, asp=asp, fig_size_fac=3, projection=ccrs_proj, axlab_kw=None)
        ii=-1
        ii+=1; ax=hca[ii]; cax=hcb[ii]
        title = ax.set_title(f'{btype}-pass KE at {y_xr.time[0].values:.13}')
        hm3 = pyic.shade(Tri, y_xr.isel(time=0).data, ax=ax, cax=cax, cmap = 'RdBu_r', clim=clim,  transform=ccrs_proj, rasterized=False)
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    else:
        hm3[0].set_array(y_xr.isel(time=frame).data.flatten())
        title.set_text(f'{btype}-pass KE at {y_xr.time[frame].values:.13}')
    fpath = '%s%s_%s_%04d.jpg' % (fig_path,__file__.split('/')[-1][:-3], run, frame)
    print('save figure: %s' % (fpath))
    plt.savefig(fpath, dpi=300)


# ani = FuncAnimation(fig, update, frames=np.arange(0, y_xr.time.shape[0], 1), interval=1)
# ani.save(f'{fig_path}/eddy_n1.mp4', writer=FFwriter, dpi=150)


# if savefig==True: plt.savefig(f'{fig_path}/{btype}_pass_{cutoff:.2f}_anti&cyclone_3.png', dpi=200, bbox_inches='tight')


# %%
########################################################################################
# %%
fu.plot_KE(Tri, DATA[30,:], lon_reg, lat_reg, fig_path, savefig)
# %%
path = '/work/bm1102/m300602/proj_smtwv/icon-oes-zstar4-02/experiments/smtwv0007/outdata_lev/smtwv0007_oce_2d_lev_PT1H_20190901T211500Z.nc'
ds = xr.open_dataset(path)
# %%
lon_reg = [10.2,13.2]
lat_reg = [-36.5, -33.5]
Tri, data_reg = eva.calc_triangulation_obj_wave(ds.tke.isel(depth_2=1).isel(time=0), lon_reg, lat_reg)

#%%
lon_reg = [12.5, 14.5]
lat_reg = [-34, -32]
Tri_c, data_reg_c = eva.calc_triangulation_obj_wave(ds.tke.isel(depth_2=1).isel(time=0), lon_reg, lat_reg)

# %%
hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=2, projection=ccrs_proj)
ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
ax.set_title(f'TKE in Anticyclone')

clim = 0, 2e-4
hm1 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, cmap = 'plasma', clim=clim,  transform=ccrs_proj, rasterized=False)
lon_reg = [10.2,13.2]
lat_reg = [-36.5, -33.5]
pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

ii+=1; ax=hca[ii]; cax=hcb[ii]
ax.set_title(f'TKE in Cyclone')

hm2 = pyic.shade(Tri_c, data_reg_c, ax=ax, cax=cax, cmap = 'plasma', clim=clim,  transform=ccrs_proj, rasterized=False)
lon_reg = [12.5, 14.5]
lat_reg = [-34, -32]
hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
hm2[1].formatter.set_useMathText(True)
pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
if savefig==True: plt.savefig(f'{fig_path}/TKE_anti&cyclone_52_50.png', dpi=200, bbox_inches='tight')

# %%
fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
lat_reg = [-36,-32.1]
lon_reg = [10.5,14]
ds_interp = pyic.interp_to_rectgrid_xr(ds_ssh.mlotst.isel(time=0), fpath_ckdtree= fpath_ckdtreeZ, lon_reg=lon_reg, lat_reg=lat_reg)
# %%
plt.figure()
ds_interp.plot(
)
# add contourline at 110m
# plt.contour(ds_interp.lon, ds_interp.lat, ds_interp, levels=[100], colors='r')
if savefig==True: plt.savefig(f'{fig_path}/TKE_anti&cyclone_mld.png', dpi=200, bbox_inches='tight')
# %%
