
# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../../')
# import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 


# %% Load Uchida data
from scipy.ndimage import rotate
from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs

import gcm_filters

# %%
ny, nx = (128, 256)

mask_data = np.ones((ny, nx))

mask_data[(ny // 4):(3 * ny // 4), (nx // 4):(3 * nx // 4)] = 0

wet_mask = xr.DataArray(mask_data, dims=['y', 'x'])

wet_mask.plot()

nt = 10

data = np.random.rand(nt, ny, nx)

da = xr.DataArray(data, dims=['time', 'y', 'x'])
# %%
filter = gcm_filters.Filter(

    filter_scale=4,

    dx_min=1,

    filter_shape=gcm_filters.FilterShape.TAPER,

    grid_type=gcm_filters.GridType.REGULAR_WITH_LAND,

    grid_vars={'wet_mask': wet_mask}

)
filter
# %%
da_masked = da.where(wet_mask)
da_masked.isel(time=0).plot()
# %%
%time da_filtered = filter.apply(da_masked, dims=['y', 'x'])
da_filtered.isel(time=0).plot()

# %%
t_space       = 1
savefig       = False
lat_reg       =  30,  40
lon_reg       = -78, -68
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
fig_path      = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/'
path_2_data   = '/work/mh0033/m300878/smt/uchida_tseries/data/'

time   = 80
idepth = 10
# %%
mask_land_100 = eva.load_smt_land_mask(depthc=32)
mask_land_100 = pyic.interp_to_rectgrid_xr(mask_land_100, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land_100 = mask_land_100.drop('depthc')
# %%
path_2_data = '/work/mh0033/m300878/smt/uchida_tseries/data/'
b           = xr.open_dataarray(path_2_data + 'b.nc', chunks={'time': 1, 'depthi': 1})
b           = b * mask_land_100
# %% ###################################### Using REGULAR_WITH_LAND ######################################
conversion_factor = eva.convert_degree_to_meter(int(b.lat.mean().data))
dx_min   = (b.lat[1]-b.lat[0]).data * conversion_factor
wet_mask = mask_land_100
#replace nans with zeros
wet_mask = wet_mask.fillna(0)
# %%
filter = gcm_filters.Filter(

    filter_scale=30000,

    dx_min=dx_min,

    filter_shape=gcm_filters.FilterShape.GAUSSIAN,

    grid_type=gcm_filters.GridType.REGULAR_WITH_LAND,

    grid_vars={'wet_mask': wet_mask}

)
filter
# %%
da_filtered = filter.apply(b.isel(time=time), dims=['lat', 'lon'])
da_filtered.isel(depthi=idepth).plot()

# %% ################################### USING IRREGULAR_WITH_LAND ######################################
import gsw
xx, yy = np.meshgrid(b.lon, 
                     b.lat
                    )
ny, nx = xx.shape
# computes spacing between two grid points depending on latitude in m; extrapolation to maintain same shape as b
dx = xr.DataArray(xr.DataArray(gsw.distance(xx, yy), dims=['lat','lon'],
                               coords={'lat':np.arange(ny),'lon':np.arange(.5,nx-1,1)}
                              ).interp(lon=np.arange(nx), method="linear",
                                       kwargs={"fill_value": "extrapolate"}).data,
                  dims=['lat','lon'], 
                  coords={'lat':b.lat.data,'lon':b.lon.data}
                 )
# computes spacing between two grid points depending on longitude in m; extrapolation to maintain same shape as b
# in this example constant since grid is only irregular in meridional direction
dy = xr.DataArray(xr.DataArray(gsw.distance(xx, yy, axis=0), dims=['lat','lon'],
                               coords={'lat':np.arange(.5,ny-1,1),'lon':np.arange(nx)}
                              ).interp(lat=np.arange(ny), method="linear",
                                       kwargs={"fill_value": "extrapolate"}).data,
                  dims=['lat','lon'], 
                  coords={'lat':b.lat.data,'lon':b.lon.data}
                 )
area = (dx * dy)
dx
# %%

# here no filtersize changes are applied; only the grid is irregular; Rossby Radisu dependent filter properties would go in here
kappa_w = xr.ones_like(wet_mask)
kappa_s = xr.ones_like(wet_mask)

dlat = b.lat.diff('lat')[0]
dlon = b.lon.diff('lon')[0]

dxw = xr.DataArray(dx.interp(lon=np.arange(dx.lon.min(),dx.lon.max()+dlon,dlon),
                             method='linear',
                             kwargs={"fill_value": "extrapolate"}).data,
                   dims=dx.dims, coords=dx.coords
                  ) # x-spacing centered at western cell edge

dyw = xr.DataArray(dy.interp(lon=np.arange(dy.lon.min(),dy.lon.max()+dlon,dlon),
                             method='linear',
                             kwargs={"fill_value": "extrapolate"}).data,
                   dims=dy.dims, coords=dy.coords
                  ) # y-spacing centered at western cell edge

dxs = xr.DataArray(dx.interp(lat=np.arange(dx.lat.min(),dx.lat.max()+dlat,dlat),
                             method='linear',
                             kwargs={"fill_value": "extrapolate"}).data,
                   dims=dx.dims, coords=dx.coords
                  ) # x-spacing centered at southern cell edge

dys = xr.DataArray(dy.interp(lat=np.arange(dy.lat.min(),dy.lat.max()+dlat,dlat),
                             method='linear',
                             kwargs={"fill_value": "extrapolate"}).data,
                   dims=dy.dims, coords=dy.coords
                  ) # y-spacing centered at southern cell edge
dxw

# %% in this example only latitudinal spacing is irregular (in meter) thus most coefficients are constant
dxs = dxw = dx
dys = dyw = dy


# %%
dx_min = min(dxw.min(), dyw.min(), dxs.min(), dys.min()).data
wet_mask = mask_land_100

wet_mask       = wet_mask.fillna(0) #wet mask needs to be ones and zeros
wet_mask[0,:]  = np.zeros_like(wet_mask[0,:] )   # fix impact from southern boundary
wet_mask[:,-1] = np.zeros_like(wet_mask[:,-1] ) # fix impact from eastern boundary

mask_land_100_new =  wet_mask.where(wet_mask>0)

filter = gcm_filters.Filter(

    filter_scale=30000,

    dx_min=dx_min,

    filter_shape=gcm_filters.FilterShape.GAUSSIAN,

    grid_type=gcm_filters.GridType.IRREGULAR_WITH_LAND,

    grid_vars={'wet_mask': wet_mask, 
               'dxw': dxw,
               'dyw': dyw,
               'dxs': dxs,
               'dys': dys, 
               'area': area,
               'kappa_w': kappa_w, 
               'kappa_s': kappa_s}

)
filter
# %%
time   = 80
idepth = 10

da_filtered = filter.apply(b.isel(time=time), dims=['lat', 'lon'])
da_filtered = da_filtered * mask_land_100_new


(da_filtered).isel(depthi=idepth).plot()
# %%
(b).isel(time=time).isel(depthi=idepth).plot()
# %%
((b*wet_mask - da_filtered)).isel(time=time).isel(depthi=idepth).plot(vmin=-5e-3,vmax=5e-3, cmap='RdBu_r')
# %%
