# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../')
import notebooks.spectral_analysis.map_filtering.funcs_filter as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

import dask.array as da
import xrft
import numpy as np
from scipy.signal import butter,filtfilt
from multiprocessing import Pool
from netCDF4 import Dataset
#

def crop_grid(lon_reg, lat_reg):
    print('Deriving triangulation object, this can take a while...')
    f = Dataset('/home/m/m300602/work/icon/grids/smtwv_oce_2022/smtwv_oce_2022_tgrid.nc', 'r')
    clon = f.variables['clon'][:] * 180./np.pi
    clat = f.variables['clat'][:] * 180./np.pi
    vlon = f.variables['vlon'][:] * 180./np.pi
    vlat = f.variables['vlat'][:] * 180./np.pi
    vertex_of_cell = f.variables['vertex_of_cell'][:].transpose()-1
    f.close()

    ind_reg = np.where(   (clon>lon_reg[0])
                        & (clon<=lon_reg[1])
                        & (clat>lat_reg[0])
                        & (clat<=lat_reg[1]) )[0]
    vertex_of_cell_reg = vertex_of_cell[ind_reg,:]

    return ind_reg, vertex_of_cell_reg, vlon, vlat

def crop_2_section(lon, lat, npoints):
    fname_tgrid  = '/smtwv_oce_2022_tgrid.nc'
    path_tgrid   = '/home/m/m300602/work/icon/grids/smtwv_oce_2022/'
    path_ckdtree = '/scratch/m/m300878/ckdtree/'
    sname        = 'sec'
    tgname       = 'smt_wave'
    gname        = 'smtwv_oce_2022'

    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[lon[0],lat[0]], p2=[lon[1],lat[1]], npoints=npoints,
        fname_tgrid  = fname_tgrid,
        path_tgrid   = path_tgrid,
        path_ckdtree = path_ckdtree,
        sname        = sname,
        gname        = gname,
        tgname       = tgname,
        load_egrid   = False,
        load_vgrid   = False,
    )
    return dckdtree, ickdtree, lon_sec, lat_sec, dist_sec

def butter_filter(data, cutoff, fs, order, btype):
    # Get the filter coefficients 
    b, a = butter(order, cutoff, btype=btype, analog=False, fs=fs)
    # y = signal.lfilter(b, a, data) #does not hit peaks
    # y = filtfilt(b, a, data) # for and back no phase shift
    # rewrtie above line for multiple dimensions
    y = np.apply_along_axis(lambda m: filtfilt(b, a, m), axis=0, arr=data)
    return y

def plot_filtered_field_hires(Tri, data_reg, cutoff, lon_reg, lat_reg, btype, fig_path, fname, savefig):
    asp = (lat_reg[1]-lat_reg[0]) / (lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = True, asp=asp, fig_size_fac=4, projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    if btype == "bandpass": ax.set_title(f'{btype}-pass filtered {fname} in Anticyclone {cutoff[0]:.2f}-{cutoff[1]:.2f} [1/h]')
    else: ax.set_title(f'{btype}-pass filtered KE in Anticyclone {cutoff:.2f} [1/h]')
    
    # clim = np.max(np.array([np.abs(data_reg.min()), data_reg.max()]))*0.4
    clim=1e-3
    hm3 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, cmap = 'RdBu_r', clim=clim, transform=ccrs_proj, rasterized=False)
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    # if savefig==True: plt.savefig(f'{fig_path}/{btype}_pass_{cutoff:.2f}.png', dpi=200, bbox_inches='tight')
    if btype == "bandpass": 
        if savefig==True: plt.savefig(f'{fig_path}/{btype}_pass_{cutoff[0]:.2f}-{cutoff[1]:.2f}.png', dpi=200, bbox_inches='tight')
    else: 
        if savefig==True: plt.savefig(f'{fig_path}/{btype}_pass_{cutoff:.2f}_{fname}.png', dpi=200, bbox_inches='tight')



def plot_filtered_field_acyc(Tri, data_reg, cutoff, lon_reg, lat_reg, btype, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2,1, plot_cb = 'right', asp=1, fig_size_fac=2, projection=ccrs_proj)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax.set_title(f'{btype}-pass filtered KE in Anticyclone')
    
    clim = np.max(np.array([np.abs(data_reg.min()), data_reg.max()]))*0.4
    hm3 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, cmap = 'RdBu_r', clim=clim,  transform=ccrs_proj, rasterized=False)

    lon_reg = [10.2,13.2]
    lat_reg = [-36.5, -33.5]
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    ii+=1; ax=hca[ii]; cax=hcb[ii]

    ax.set_title(f'{btype}-pass filtered KE in Cyclone')
    clim = np.max(np.array([np.abs(data_reg.min()), data_reg.max()]))*0.4
    hm3 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, cmap = 'RdBu_r', clim=clim,  transform=ccrs_proj, rasterized=False)
    
    lon_reg = [12.5, 14.5]
    lat_reg = [-34, -32]
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    if savefig==True: plt.savefig(f'{fig_path}/{btype}_pass_{cutoff:.2f}_anti&cyclone__filt_length33.png', dpi=200, bbox_inches='tight')

def plot_KE(Tri, data_reg, lon_reg, lat_reg,fig_path, savefig):
    asp = (lat_reg[1]-lat_reg[0]) / (lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = True, asp=asp, fig_size_fac=3, projection=ccrs_proj)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(f'KE ')
    
    hm3 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, cmap = 'plasma',   transform=ccrs_proj, rasterized=False)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    if savefig==True: plt.savefig(f'{fig_path}/KE_anti&cyclone.png', dpi=200, bbox_inches='tight')

def plot_uv(u, v, dist_sec, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2, 1, sharey=True, plot_cb ='right', asp=1, fig_size_fac=2)
    ii=-1
    ii=ii+1; ax=hca[ii]; cax=hcb[ii]

    clim = 1
    pyic.shade(dist_sec/1e3, u.depth, u.data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, rasterized=False)
    ax.set_title('u')
    ax.set_ylabel('depth [m]')

    ii=ii+1; ax=hca[ii]; cax=hcb[ii]

    pyic.shade(dist_sec/1e3, v.depth, v.data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, rasterized=False)
    ax.set_title(f'v at {v.time.values:.13}')

    for ax in hca:
        ax.set_xlabel('Distance [km]')
        ax.set_ylim(1500,0)
        # ax.set_xlim(0,240)

    if savefig==True: plt.savefig(f'{fig_path}/eddy_section_uv_zonal_sec.png', dpi=150)

def plot_uv_diff(u, v, dudz, dvdz, dist_sec, sec, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2, 2, sharey=True, sharex=True, plot_cb =[0,1,0,1], asp=1, fig_size_fac=2)
    ii=-1
    ii=ii+1; ax=hca[ii]; cax=hcb[ii]
    clim = 1
    pyic.shade(dist_sec/1e3, u.depth, u.data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, rasterized=False)
    ax.set_title('u')
    ax.set_ylabel('depth [m]')

    ii=ii+1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(dist_sec/1e3, v.depth, v.data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, rasterized=False)
    ax.set_title(f'v at {v.time.values:.13}')

    ii=ii+1; ax=hca[ii]; cax=hcb[ii]
    clim = 0.005
    pyic.shade(dist_sec/1e3, dudz.depth, dudz.data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, rasterized=False)
    ax.set_title('du/dz')
    ax.set_ylabel('depth [m]')
    ax.set_xlabel('Distance [km]')

    ii=ii+1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(dist_sec/1e3, dvdz.depth, dvdz.data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim, rasterized=False)
    ax.set_title(f'dv/dz')
    ax.set_xlabel('Distance [km]')
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    for ax in hca:
        ax.set_ylim(500,0)
        # ax.set_xlim(0,240)

    if savefig==True: plt.savefig(f'{fig_path}/eddy_n1_section_uv_{sec}_shear.png')


def plot_N2(N2_xr, sec, savefig, fig_path):
    hca, hcb = pyic.arrange_axes(1, 1, sharey=True, plot_cb ='right', asp=0.75, fig_size_fac=2)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]

    hm3 = pyic.shade(N2_xr.lon, N2_xr.depthi, N2_xr, ax=ax, cax=cax, cmap = 'Greys', norm=colors.LogNorm(vmin=1e-8, vmax=1e-4), rasterized=False)
    ax.set_ylim(1500,0)
    ax.set_xlabel('lon')
    ax.set_ylabel('depth [m]')
    ax.set_title('N2')

    if savefig==True: plt.savefig(f'{fig_path}/N2_{sec}_n1.png', dpi=150, bbox_inches='tight')
