# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import funcs_filter as fu
sys.path.insert(0, '../')

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

import dask.array as da
import xrft
import numpy as np
import matplotlib
from scipy.signal import butter,filtfilt
from multiprocessing import Pool

# %%
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='02:00:00', wait=False)
cluster
client

# %%
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/bandpass_filtered/section/'
savefig        = False

# %%
time0   = pd.to_datetime('2019-08-28T21:15:00')
time1   = pd.to_datetime('2019-08-31T21:15:00')
# %% filter config
sampling_freq = 1/2 # 1/(2h)
cutoff        = 1/6 # 1/(6h)
order         = 10
btype         = 'high'

# %% ##################### Section of Eddy ############################
%%time
sec = 'zonal'
reload(fu)
# Anticyclonic Eddy close to creation with lots of structure:
lon_reg = np.array([10.2,13.2])
lat_reg = [-36.5, -33.5]
# Anticyclinic Eddy much more coherent
lon_reg = [4,6.4]
lat_reg = [-32,-29.5]

if sec=='zonal':
    lat_reg = [np.mean(lat_reg), np.mean(lat_reg)]
elif sec=='meridional':
    lon_reg = [np.mean(lon_reg), np.mean(lon_reg)]
dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = fu.crop_2_section(lon_reg, lat_reg, npoints=500)

if sec=='zonal': sec_dir = lon_sec; xlabel = 'lon'
elif sec=='meridional': sec_dir = lat_sec; xlabel = 'lat'


# %% ##################### U, V and KE ############################
ds_u1     = eva.load_smt_wave_all('u', exp=7, remove_bnds=True)
ds_v1     = eva.load_smt_wave_all('v', exp=7, remove_bnds=True)
# %%
ds_u1 = ds_u1.drop('clon').drop('clat')
ds_v1 = ds_v1.drop('clon').drop('clat')
#%%
%%time
ds_sel = ds_u1.u**2/2 + ds_v1.v**2/2
# rename var
ds_sel = ds_sel.rename('KE')
ds_sel = ds_sel.sel(time=slice(time0, time1))
# %%
DATA = ds_sel.isel(ncells=ickdtree).compute()
# %%
y    = fu.butter_filter(DATA.fillna(0.), cutoff, sampling_freq, order, btype=btype)
y_xr = xr.DataArray(y, dims=['time', 'depth', 'ncells'], coords={'time': DATA.time, 'ncells': DATA.ncells, 'depth': DATA.depth})

# %% ############################# Animation KE ############################
#create short video clip over time
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
plt.rcParams['animation.ffmpeg_path'] = '/work/mh0033/m300602/miniconda3/envs/pyicon_py39_exp/bin/ffmpeg'
FFwriter = animation.FFMpegWriter(fps=5, extra_args=['-vcodec', 'libx264'])

fig, ax = plt.subplots(figsize=(10,10))
# ax.set_xlim(0, 380)
ax.set_ylim(1500, 0)
ax.set_xlabel(xlabel)
ax.set_ylabel('depth [m]')
title = ax.set_title(f'Eddy section {y_xr.time[0].values:.13}')
pcm = ax.pcolormesh(sec_dir, y_xr.depth, y_xr.isel(time=0).data, cmap='RdBu_r', vmin=-0.04, vmax=0.04)
# pcm = plt.pcolormesh(y_xr.ncells, y_xr.depth, y_xr.isel(time=34).data, cmap='RdBu_r', norm=colors.SymLogNorm(vmin=-0.2, vmax=0.2, linthresh=0.01))
plt.colorbar(pcm)
def update(frame):
    pcm.set_array(y_xr.isel(time=frame).data.flatten())
    title.set_text(f'Eddy section {y_xr.time[frame].values:.13}')
    return pcm

ani = FuncAnimation(fig, update, frames=np.arange(0, y_xr.time.shape[0], 1), interval=1)
ani.save(f'{fig_path}/eddy_n1_{sec}_deep.mp4', writer=FFwriter, dpi=150)

# %% ############################# dudz dvdz ############################
%%time
u = ds_u1.sel(time=time1).u
u = u.isel(ncells=ickdtree).compute()
v = ds_v1.sel(time=time1).v
v = v.isel(ncells=ickdtree).compute()

# %%
reload(fu)
fu.plot_uv(u, v, dist_sec, fig_path, savefig)

#%%
dudz = u.differentiate('depth')
dvdz = v.differentiate('depth')

# %%
reload(fu)
fu.plot_uv_diff(u, v, dudz, dvdz, dist_sec, sec, fig_path, savefig)

# %% ########################## Compute N2 ################################
to = eva.load_smt_wave_all('to', exp=7, remove_bnds=True)
so = eva.load_smt_wave_all('so', exp=7, remove_bnds=True)
to = to.drop('clon').drop('clat')
so = so.drop('clon').drop('clat')

# %%
to_sel = to.sel(time=time1)
so_sel = so.sel(time=time1)
to_sel = to_sel.to.isel(ncells=ickdtree).compute()
so_sel = so_sel.so.isel(ncells=ickdtree).compute()
# %%
# overwrite ncells with lon_sec as new coordinate and rename lon
to_sel = to_sel.assign_coords(ncells=lon_sec)
so_sel = so_sel.assign_coords(ncells=lon_sec)
to_sel = to_sel.rename({'ncells': 'lon'})
so_sel = so_sel.rename({'ncells': 'lon'})
to_sel = to_sel.assign_coords(lat=lat_sec[0])
so_sel = so_sel.assign_coords(lat=lat_sec[0])

# %% use gsw to compute density
reload(fu)
p  = gsw.p_from_z(-so_sel.depth, so_sel.lat)
CT = gsw.CT_from_pt(so_sel, to_sel)
SA = gsw.SA_from_SP(so_sel, p, so_sel.lon, so_sel.lat)
# expand p to match the shape of CT
p = np.expand_dims(p, axis=1)
p = np.repeat(p, CT.shape[1], axis=1)
p = xr.DataArray(p, dims=['depth', 'lon'], coords={'depth': so_sel.depth, 'lon': so_sel.lon})

N2, pmid = gsw.Nsquared(SA, CT, p, so_sel.lat, axis=0)
N2_xr    = xr.DataArray(N2, dims=['depthi', 'lon'], coords={'depthi': pmid[:,0], 'lon': so_sel.lon})

# %%
reload(fu)
fu.plot_N2(N2_xr, sec, savefig, fig_path)

# %% #################################### FIND EDDY ##############################
lon_reg = 0,20
lat_reg = -40,-20
lon_reg = [4,6.4]
lat_reg = [-32,-29.5]
u  = ds_u1.sel(time=time1).u
v  = ds_v1.sel(time=time1).v
KE = u**2/2 + v**2/2
KE = KE.isel(depth=17)
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
data = pyic.interp_to_rectgrid_xr(KE, fpath_ckdtree= fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
data.plot()
# %%
