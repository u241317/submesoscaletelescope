# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import funcs_filter as fu
sys.path.insert(0, '../')


import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

import dask.array as da
import xrft
import numpy as np
from scipy.signal import butter,filtfilt
from multiprocessing import Pool

# %%
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
cluster
client

# %%
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/bandpass_filtered/'
savefig        = False

# %%
path_data = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/exp7_KE_100m_fine.nc'
ds        = xr.open_dataset(path_data, chunks={'lon':10, 'lat':10})
# %%
sel = ds.KE.isel(lon=50,lat=50)

# transform intp frequency domain
ds_fft = xrft.dft(sel.fillna(0.), dim=['time'], shift=False, detrend='linear')

sampling = 2. #hours
nyq      = sampling / 2.
# %%
# apply lowpass filter with signal
ds_filtered = signal.lfilter(signal.firwin(10, 0.1), 1.0, ds_fft)
# to xarray
ds_filtered = xr.DataArray(ds_filtered, dims=['freq_time'], coords={'freq_time': ds_fft.freq_time})
# transform back to time domain
ds_filtered = xrft.idft(ds_filtered, true_phase=True, dim=['freq_time'], shift=False, detrend='linear')

# multiply with complex conjugate
ds_filtered = ds_filtered * ds_fft.conj()

# %%
# transfrom time series to frequency domain and apply lowpass filter then transform back to time domain
data = sel
da_fft = xrft.fft(data.fillna(0.), dim='time', shift=False, detrend='linear').compute()

# %%


def butter_lowpass_filter(data, cutoff, fs, order):
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y




# %%
import numpy as np
from scipy.signal import butter,filtfilt
# %%
import xrscipy.other.signal as dsp


# %%
data = sel.compute()
# %%
sampling_freq = 2/24 #every 2 hours
# % low pass filter

order  = 8
cutoff = 1/(10*24)

def butter_lowpass_filter(data, cutoff, fs, order):
    b, a = butter(order, cutoff, btype='high', analog=False, fs=fs)
    # y = signal.lfilter(b, a, data) #does not hit peaks
    y = filtfilt(b, a, data) # for and back no phase shift
    return y

y = butter_lowpass_filter(data.fillna(0.).data, cutoff, sampling_freq, order)
y_xr = xr.DataArray(y, dims=['time', 'lat', 'lon'], coords={'time': data.time, 'lat': data.lat, 'lon': data.lon})
# %%

plt.figure()
plt.plot(np.linspace(0, len(data), len(data)), data.data)
plt.plot(y)
# %%
# optimze butter_lowpass_filter with numba
from numba import jit
@jit(nopython=True)
def butter_lowpass_filter_numba(data, cutoff, fs, order):
    # Get the filter coefficients 
    b, a = butter(order, cutoff, btype='high', analog=False, fs=fs)
    # y = signal.lfilter(b, a, data) #does not hit peaks
    y = filtfilt(b, a, data)
# %%
data = ds.KE.isel(lon=slice(0,10),lat=slice(0,10)).compute()
# %%
y = butter_lowpass_filter_numba(data.fillna(0.).data, cutoff, sampling_freq, order)

# %% Fast compute
path_data = '/work/mh0033/m300878/crop_interpolate/smtwv/ssh/002deg/exp7_KE_100m_fine.nc'
ds        = xr.open_dataset(path_data, chunks={'lon':10, 'lat':10})
# %%
# data = ds.KE.compute() #anoying
# data = ds.KE.isel(lon=slice(0,100),lat=slice(0,100))#.compute()
data = ds.KE.sel(lon=slice(4,8), lat=slice(-32,-30))
# data = ds.KE.sel(lon=slice(0,10), lat=slice(-34,-25))
lon_reg = [4,8]
lat_reg = [-32,-30]
# data = ds.KE
# %%
%%time
data = data.compute()
# %%


# %%

%%time
sampling_freq = 1/2 #every 2 hours

order  = 10
cutoff = 1/(6) #

# def butter_lowpass_filter(data, cutoff, fs, order):
#     # Get the filter coefficients 
#     b, a = butter(order, cutoff, btype='low', analog=False, fs=fs)
#     # y = signal.lfilter(b, a, data) #does not hit peaks
#     # y = filtfilt(b, a, data) # for and back no phase shift
#     # rewrtie above line for multiple dimensions
#     y = np.apply_along_axis(lambda m: filtfilt(b, a, m), axis=0, arr=data.compute())
#     # rewrtie above line for parallel computing
#     return y

b, a = butter(order, cutoff, btype='high', analog=False, fs=sampling_freq)
def apply_filtfilt(m):
    return filtfilt(b, a, m)
def butter_lowpass_filter(data, cutoff, fs, order):
    # Get the filter coefficients 

    # Create a pool of workers
    print('get pool')
    if __name__ == "__main__":
        with Pool(processes=8) as p:
            # Apply the function in parallel
            print('apply')
            y = np.array(p.map(apply_filtfilt, data))
        return y


y = butter_lowpass_filter(data.fillna(0.), cutoff, sampling_freq, order)
y_xr = xr.DataArray(y, dims=['time', 'lat', 'lon'], coords={'time': data.time, 'lat': data.lat, 'lon': data.lon})
# %%

y_xr.isel(time=500).plot()
# %%
def plot_filtered_field(data, t, fig_path, savefig):
    lon_reg = [data.lon.min(), data.lon.max()]
    lat_reg = [data.lat.min(), data.lat.max()]
    asp = (data.lat.max()-data.lat.min()) / (data.lon.max()-data.lon.min())
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = False, asp=asp, fig_size_fac=3, projection=ccrs_proj)
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(data.lon,data.lat, data.isel(time=t), ax=ax, cax=cax, cmap='RdBu_r', extend='both', projection=ccrs_proj)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    if savefig==True: plt.savefig(f'{fig_path}/spectra.png', dpi=200, bbox_inches='tight')

# %%
plot_filtered_field(y_xr, 500, 'None', False)
# %%
idepth     = 26
idepth_lev = 2

begin = '2019-07-06T01:15:00'
end   = '2019-10-17T23:15:00'

ds_u1     = eva.load_smt_wave_all('u', exp=7, remove_bnds=True)
ds_u1     = ds_u1.isel(depth=idepth).u
ds_u1     = ds_u1.drop('clon').drop('clat')
ds_levels = eva.load_smt_wave_levels(7, it=2)
ds_levels = ds_levels.rename({'ncells_2': 'ncells'})
ds_u2     = ds_levels.isel(depth=idepth_lev).u
ds_u      = xr.concat([ds_u1, ds_u2], dim='time').sel(time=slice(begin, end)) 

ds_v1 = eva.load_smt_wave_all('v', exp=7, remove_bnds=True)
ds_v1 = ds_v1.isel(depth=idepth).v
ds_v1 = ds_v1.drop('clon').drop('clat')

time_missing = '2019-07-08T01:15:00' #20190708T0115
infilled_data_set = ds_v1.resample(time="2H").asfreq()
infilled_data_set = infilled_data_set.assign_coords(time=infilled_data_set.time.data + (ds_v1.time.data[0] - infilled_data_set.time.data[0]))

ds_v2 = ds_levels.isel(depth=idepth_lev).v
ds_v  = xr.concat([infilled_data_set, ds_v2], dim='time').sel(time=slice(begin, end)) 

# %%
data        = ds_u**2 /2 + ds_v**2 /2
data_tsel   = data.isel(time=slice(0,data.time.shape[0],1))

# %%
test = data_tsel.isel(time=500)
# %%

# %%
grid = eva.load_smt_wave_grid()
# %%
clon = np.rad2deg(grid.clon.data)
clat = np.rad2deg(grid.clat.data)
# %%
#create data array with lon and lat
test = test.assign_coords(clon=(['ncells'], clon), clat=(['ncells'], clat))
# %%
# crop region
Tri, data_reg = eva.calc_triangulation_obj_wave(test, lon_reg, lat_reg)


# %%
def plot_filtered_field_hires(Tri, data_reg, cutoff, fig_path, savefig):
    asp = (lat_reg[1]-lat_reg[0]) / (lon_reg[1]-lon_reg[0])
    hca, hcb = pyic.arrange_axes(1,1, plot_cb = True, asp=asp, fig_size_fac=3, projection=ccrs_proj)
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(f'High-pass filtered KE cutoff:{cutoff:.2f}')
    
    clim = np.max(np.array([np.abs(data_reg.min()), data_reg.max()]))
    hm3 = pyic.shade(Tri, data_reg, ax=ax, cax=cax, cmap = 'RdBu_r', clim=clim,  transform=ccrs_proj, rasterized=False)
    # pyic.shade(data.lon,data.lat, data.isel(time=t), ax=ax, cax=cax, cmap='RdBu_r', extend='both', projection=ccrs_proj)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg)
    if savefig==True: plt.savefig(f'{fig_path}/low_pass_{cutoff:.2f}.png', dpi=200, bbox_inches='tight')

# %%
reload(fu)
lon_reg = [6,7]
lat_reg = [-31,-30]
ind_reg, vertex_of_cell_reg, vlon, vlat = fu.crop_grid(lon_reg, lat_reg)
# %%
DATA = data_tsel[:,ind_reg].compute()
# %%
%%time
sampling_freq = 1/2 #every 2 hours

order  = 10
cutoff = 1/(6) #

def butter_lowpass_filter(data, cutoff, fs, order, btype):
    # Get the filter coefficients 
    b, a = butter(order, cutoff, btype=btype, analog=False, fs=fs)
    # y = signal.lfilter(b, a, data) #does not hit peaks
    # y = filtfilt(b, a, data) # for and back no phase shift
    # rewrtie above line for multiple dimensions
    y = np.apply_along_axis(lambda m: filtfilt(b, a, m), axis=0, arr=data)
    return y

# b, a = butter(order, cutoff, btype='high', analog=False, fs=sampling_freq)
# def apply_filtfilt(m):
#     return filtfilt(b, a, m)
# def butter_lowpass_filter(data, cutoff, fs, order):
#     # Get the filter coefficients 

#     # Create a pool of workers
#     print('get pool')
#     if __name__ == "__main__":
#         with Pool(processes=4) as p:
#             # Apply the function in parallel
#             print('apply')
#             y = np.array(p.map(apply_filtfilt, data))
#         return y


y = butter_lowpass_filter(DATA.fillna(0.), cutoff, sampling_freq, order, btype='high')
y_xr = xr.DataArray(y, dims=['time', 'ncells'], coords={'time': DATA.time, 'ncells': DATA.ncells})

# %%
import matplotlib
data = y[503,:] #503
Tri = matplotlib.tri.Triangulation(vlon, vlat, triangles=vertex_of_cell_reg)
plot_filtered_field_hires(Tri, data, cutoff, fig_path, savefig=False)


# %% Example
from scipy import signal
import matplotlib.pyplot as plt
import numpy as np
b, a = signal.butter(4, 100, 'low', analog=True)
w, h = signal.freqs(b, a)
plt.semilogx(w, 20 * np.log10(abs(h)))
plt.title('Butterworth filter frequency response')
plt.xlabel('Frequency [radians / second]')
plt.ylabel('Amplitude [dB]')
plt.margins(0, 0.1)
plt.grid(which='both', axis='both')
plt.axvline(100, color='green') # cutoff frequency
plt.show()
# %%
t = np.linspace(0, 1, 1000, False)  # 1 second
sig = np.sin(2*np.pi*10*t) + np.sin(2*np.pi*20*t)
fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
ax1.plot(t, sig)
ax1.set_title('10 Hz and 20 Hz sinusoids')
ax1.axis([0, 1, -2, 2])
sos = signal.butter(10, 15, 'hp', fs=1000, output='sos')
filtered = signal.sosfilt(sos, sig)
ax2.plot(t, filtered)
ax2.set_title('After 15 Hz high-pass filter')
ax2.axis([0, 1, -2, 2])
ax2.set_xlabel('Time [seconds]')
plt.tight_layout()
plt.show()
# %%
