# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '../../')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import math

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=False)
cluster
client

# %%
############## plot config ##############
fig_path = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/'
savefig  = False


# %% ########################## Evaluation of North Atlantic ############################
path_2_data            = '/work/mh0033/m300878/spectra/ssh/resolution_dependent/'

data_interp_notides_10 = xr.open_dataset(f'{path_2_data}/smt0000_spectra_ssh_rect.nc')
P_notides10            = fu.compute_spectra_uchida(data_interp_notides_10.h_sp.fillna(0.), samp_freq=2)

ds_ref_w, ds_ref_s, strings = fu.load_spectra_uchida()

# % get mask for NA
x         = data_interp_notides_10.h_sp.isel(time=0)
count     = np.sum(~np.isnan(x))
mask_natl = ~np.isnan(x)

# %%
reload(fu)
lat_mean = 40
fname    = f'spectra_ssh_uchida_na_notides_2'
fu.plot_spectra_uchida_NA(P_notides10.where(mask_natl), ds_ref_w, lat_mean, count, fname, fig_path, savefig)

# %%
reload(fu)
lat_mean = 40
fname    = f'spectra_ssh_uchida_na_notides_zoom'
fu.plot_spectra_uchida_NA_zoom(P_notides10.where(mask_natl), ds_ref_w, lat_mean, count, fname, fig_path, savefig)


# %%
client.close()
cluster.close()
# %%
