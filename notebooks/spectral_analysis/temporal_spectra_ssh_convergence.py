# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='03:00:00')
cluster
client

# %%
############## plot config ##############
fig_path       = '/home/m/m300878/submesoscaletelescope/results/smt_wave/spectra/ssh/resolution_dependent/'
# fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.02_180W-180E_90S-90N.nc'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.30_180W-180E_90S-90N.nc'

fpath_ckdtreeZ = '/work/mh0033/m300602/icon/grids/smtwv_oce_2022/ckdtree/rectgrids/smtwv_oce_2022_res0.10_180W-180E_90S-90N.nc'
lon_reg = np.array([-40, 30])
lat_reg = np.array([-55, -15])

# zoom1
lon_reg1 = np.array([-5, 10])
lat_reg1 = np.array([-42, -32])
lon_reg2 = np.array([-2, 0.5])
lat_reg2 = np.array([-38, -36.75])

savefig = False


# %% smt wave with tides
reload(eva)
ds_tides_tmean = eva.load_smt_wave_2d_exp7_07(it=1)
begin          = '2019-07-09T00:00:00'
end            = '2019-07-18T23:15:00'
p1             = ds_tides_tmean.zos.sel(time=slice(begin, end))
p1             = p1.drop_duplicates('time')
p1             = p1.isel(time=slice(0,239)) #  smtwv0007_oce_2d_PT1H_20190718T231500Z.nc defect
p1             = p1.drop('clon').drop('clat')

ds_tides_tmean_2 = eva.load_smt_wave_2d_exp7_07_part2(it=1)
begin            = '2019-07-19T00:00:00'
end              = '2019-08-31T23:15:00'
p2               = ds_tides_tmean_2.zos.sel(time=slice(begin, end))

exp7 = xr.concat([p1, p2], dim='time')
exp7 = exp7.drop_duplicates('time')


# %%
# %% tides 2022
reload(eva)
ds_tides_22 = eva.load_smt_wave_all('2d', exp=8, it=1)
exp8         = ds_tides_22.drop('clon').drop('clat').zos
exp8         = exp8.drop_duplicates('time')


# %%
reload(fu)
path_2_data = '/work/mh0033/m300878/spectra/ssh/convergence/'
Tstring = ['7days', '14days', '21days', '28days', '35days', '42days', '49days']
begin   = '2019-07-14T00:15:00'

for tstring in Tstring:
    if tstring == '7days':
        end     = '2019-07-20T23:15:00'
    elif tstring == '14days':
        end     = '2019-07-27T23:15:00'
    elif tstring == '21days':
        end     = '2019-08-03T23:15:00'
    elif tstring == '28days':
        end     = '2019-08-10T23:15:00'
    elif tstring == '35days':
        end     = '2019-08-17T23:15:00'
    elif tstring == '42days':
        end     = '2019-08-24T23:15:00'
    elif tstring == '49days':
        end     = '2019-08-31T23:15:00'

    fname       = f'smtwv0007_spectra_ssh_{tstring}.nc'
    data        = exp7.sel(time=slice(begin,end))
    data_interp = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_interp.to_netcdf(f'{path_2_data}/{fname}')


# %%
reload(fu)
path_2_data = '/work/mh0033/m300878/spectra/ssh/convergence/start3rdweek/'
Tstring = ['7days', '14days', '21days', '28days', '35days', '42days']
begin   = '2019-07-21T00:15:00'

for tstring in Tstring:
    if tstring == '7days':
        end     = '2019-07-27T23:15:00'
    elif tstring == '14days':
        end     = '2019-08-03T23:15:00'
    elif tstring == '21days':
        end     = '2019-08-10T23:15:00'
    elif tstring == '28days':
        end     = '2019-08-17T23:15:00'
    elif tstring == '35days':
        end     = '2019-08-24T23:15:00'
    elif tstring == '42days':
        end     = '2019-08-31T23:15:00'

    fname       = f'smtwv0007_spectra_ssh_{tstring}.nc'
    data        = exp7.sel(time=slice(begin,end))
    data_interp = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_interp.to_netcdf(f'{path_2_data}/{fname}')

# %%
reload(fu)
path_2_data = '/work/mh0033/m300878/spectra/ssh/convergence/1week/'
Tstring     = ['7days_0', '7days_1', '7days_2', '7days_3', '7days_4', '7days_5', '7days_6', '7days_7', '7days_8']

for tstring in Tstring:
    if tstring == '7days_0':
        begin   = '2019-07-09T00:15:00'
        end     = '2019-07-15T23:15:00'
    elif tstring == '7days_1':
        begin   = '2019-07-16T00:15:00'
        end     = '2019-07-22T23:15:00'
    elif tstring == '7days_2':
        begin   = '2019-07-23T00:15:00'
        end     = '2019-07-29T23:15:00'
    elif tstring == '7days_3':
        begin   = '2019-07-30T00:15:00'
        end     = '2019-08-05T23:15:00'
    elif tstring == '7days_4':
        begin   = '2019-08-06T00:15:00'
        end     = '2019-08-12T23:15:00'
    elif tstring == '7days_5':
        begin   = '2019-08-13T00:15:00'
        end     = '2019-08-19T23:15:00'
    elif tstring == '7days_6':
        begin   = '2019-08-20T00:15:00'
        end     = '2019-08-26T23:15:00'

    fname       = f'smtwv0007_spectra_ssh_{tstring}.nc'
    data        = exp7.sel(time=slice(begin,end))
    data_interp = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_interp.to_netcdf(f'{path_2_data}/{fname}')

# %%
reload(fu)
path_2_data = '/work/mh0033/m300878/spectra/ssh/convergence/1week_exp8/'
Tstring     = ['7days_0', '7days_1', '7days_2', '7days_3', '7days_4', '7days_5', '7days_6', '7days_7', '7days_8']

for tstring in Tstring:
    if tstring == '7days_0':
        begin  = '2022-02-01T00:15:00'
        end    = '2022-02-07T23:15:00'
    elif tstring == '7days_1':
        begin  = '2022-02-08T00:15:00'
        end    = '2022-02-14T23:15:00'
    elif tstring == '7days_2':
        begin  = '2022-02-15T00:15:00'
        end    = '2022-02-21T23:15:00'
    elif tstring == '7days_3':
        begin  = '2022-02-22T00:15:00'
        end    = '2022-02-28T23:15:00'
    elif tstring == '7days_4':
        begin  = '2022-03-01T00:15:00'
        end    = '2022-03-07T23:15:00'
    elif tstring == '7days_5':
        begin  = '2022-03-08T00:15:00'
        end    = '2022-03-14T23:15:00'
    elif tstring == '7days_6':
        begin  = '2022-03-15T00:15:00'
        end    = '2022-03-21T23:15:00'
    elif tstring == '7days_7':
        begin  = '2022-03-22T00:15:00'
        end    = '2022-03-28T23:15:00'

    fname       = f'smtwv0008_spectra_ssh_{tstring}.nc'
    data        = exp8.sel(time=slice(begin,end))
    data_interp = pyic.interp_to_rectgrid_xr(data, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    data_interp.to_netcdf(f'{path_2_data}/{fname}')


# %%
client.close()
cluster.close()
# %%


# Load data for convergence analysis
# %%
path_2_data = '/work/mh0033/m300878/spectra/ssh/convergence/1week_exp8/'

spectra_exp7 = []

for tstring in Tstring:
    data_exp7  = xr.open_dataset(f'{path_2_data}/smtwv0008_spectra_ssh_{tstring}.nc')
    exp7 = fu.compute_spectra_uchida(data_exp7.zos.fillna(0.))

    spectra_exp7.append(exp7)


# %%
reload(fu)
fname = 'spectra_ssh_convergence_exp8_1week_0201'
fu.plot_spectra_ssh_convergence_exp7(spectra_exp7, Tstring, fname, fig_path, savefig)
# %%
