# %%
%%capture
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

from netCDF4 import Dataset, num2date
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import funcs as fu

from scipy import stats    #Used for 2D binned statistics
from importlib import reload
# %%
#Reload modules:
reload(eva)
reload(tools)

# %%
#---------------------------------Functions---------------------------------#



# %% load windstress
tstart  = np.datetime64('2010-03-15T21:00:00')
tend    = np.datetime64('2010-03-22T21:00:00')
lon_reg_0 = np.array([-90, -10])
lat_reg_0 = np.array([15, 60])
# lon_reg_2 = np.array([-75, -46])
# lat_reg_2 = np.array([23.5, 40])
lon_reg_2 = np.array([-70, -54])
lat_reg_2 = np.array([23.5, 36])
# path_data = '/work/mh0033/from_Mistral/mh0033/u241317/smt/forcing/ustressSelL.nc'

path_data = '/work/mh0033/from_Mistral/mh0033/u241317/smt/forcing/ustress.nc'
ustress   = xr.open_dataset(path_data)
ustress   = ustress.loc[dict(time=slice(tstart, tend))]
ustress   = ustress.assign_coords(lon=(((ustress.lon + 180) % 360) - 180))
ustress   = ustress.sel(lon=slice(lon_reg_0[0],lon_reg_0[1]))
ustress   = ustress.sel(lat=slice(lat_reg_0[1],lat_reg_0[0]))
ustress   = ustress / 3600
lonw      = ustress.lon #.data
latw      = ustress.lat #.data

# path_data  = '/work/mh0033/from_Mistral/mh0033/u241317/smt/forcing/vstressSelL.nc'

path_data  = '/work/mh0033/from_Mistral/mh0033/u241317/smt/forcing/vstress.nc'
vstress    = xr.open_dataset(path_data)
vstress    = vstress.loc[dict(time=slice(tstart, tend))]
vstress    = vstress.assign_coords(lon=(((vstress.lon + 180) % 360) - 180))
vstress    = vstress.sel(lon=slice(lon_reg_0[0],lon_reg_0[1]))
vstress    = vstress.sel(lat=slice(lat_reg_0[1],lat_reg_0[0]))
vstress    = vstress / 3600
vstress    = vstress.rename({'var181': 'var180'}) # only necessary for corrupted dims
windstress = np.sqrt(ustress**2 + vstress**2)

fig_path =  '/home/m/m300878/submesoscaletelescope/notebooks/images/forcing/'
savefig  = False
# %%
rh0 = 1024
fe = 2.* 2.*np.pi/86400. * np.sin(latw*np.pi/180.)
# c_drag = 0.1

lon_reg_all, lat_reg_all = eva.get_fronts()

dy_tau_x         = ustress.differentiate("lat")
dx_tau_y         = vstress.differentiate("lon")
wind_stress_curl = - dy_tau_x + dx_tau_y

tau_x_scaled = ustress/rh0/fe
tau_y_scaled = vstress/rh0/fe
dy_tau_x     = tau_x_scaled.differentiate("lat")
dx_tau_y     = tau_y_scaled.differentiate("lon")
w_ek         = - dy_tau_x + dx_tau_y

# %% 
#--------------------------------------Time Averages-----------------------------#
w_ek_week = w_ek.mean(dim='time')
w_ek_week.attrs["time averaged"] = "2010-03-15T21 - 2010-03-22T21"
w_ek_week.attrs["time steps"] = "169"

w_ek_day = w_ek.resample(time='1D').mean()
w_ek_day = w_ek_day.isel(time=slice(1,7)) #select only days with 12 mean members
w_ek_day.attrs["time averaged"] = "2010-03-16T01 - 2010-03-21T23"
w_ek_day.attrs["time steps"] = "24 on each day"
w_ek_2d = w_ek_day.resample(time='2D').mean()

ustress_week = ustress.mean(dim='time')
ustress_week.attrs["time averaged"] = "2010-03-15T21 - 2010-03-22T21"
ustress_week.attrs["time steps"] = "169"

ustress_day = ustress.resample(time='1D').mean()
ustress_day = ustress_day.isel(time=slice(1,7)) #select only days with 12 mean members
ustress_day.attrs["time averaged"] = "2010-03-16T01 - 2010-03-21T23"
ustress_day.attrs["time steps"] = "24 on each day"
ustress_2d = ustress_day.resample(time='2D').mean()

vstress_week = vstress.mean(dim='time')
vstress_week.attrs["time averaged"] = "2010-03-15T21 - 2010-03-22T21"
vstress_week.attrs["time steps"] = "169"

vstress_day = vstress.resample(time='1D').mean()
vstress_day = vstress_day.isel(time=slice(1,7)) #select only days with 12 mean members
vstress_day.attrs["time averaged"] = "2010-03-16T01 - 2010-03-21T23"
vstress_day.attrs["time steps"] = "24 on each day"
vstress_2d = vstress_day.resample(time='2D').mean()

# %%
w_ek_week    = w_ek_week.compute()
ustress_week = ustress_week.compute()
vstress_week = vstress_week.compute()

# %%
path_data = f'/work/mh0033/m300878/observation/pressure/'
search_str = f'E5sf00_1H_2010-03-*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
ds = xr.open_mfdataset(flist, engine='cfgrib', combine='nested', concat_dim='time' )
ds = ds.assign_coords(longitude=(((ds.longitude + 180) % 360) - 180))
ds = ds.sortby(ds.longitude)
ds = ds.sel(longitude=slice(lon_reg_0[0],lon_reg_0[1]))
ds = ds.sel(latitude=slice(lat_reg_0[1],lat_reg_0[0]))
ds['msl'] = ds['msl'] / 100

# %%
ds_mean = ds.msl.isel(time=slice(14,21)).mean(dim='step').mean(dim='time')
# %%
#--------------------------------------Plotting----------------------------------#

lon     = lonw
lat     = latw
lon_reg = lon_reg_0
lat_reg = lat_reg_0
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])

t   = 168
hca, hcb = pyic.arrange_axes(2, 1, plot_cb='right', asp=asp, fig_size_fac=2, projection=ccrs_proj, axlab_kw=None, sharey=True)

ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 0.75
pyic.shade(lon, lat, ustress.isel(time=t).var180, ax=ax, cax=cax,  transform=ccrs_proj, clim=clim, rasterized=False, cmap='RdBu_r')
ar=8
ax.quiver(lonw[::ar], latw[::ar], ustress.isel(time=t).var180[::ar,::ar], vstress.isel(time=t).var180[::ar,::ar])

ax.set_title(f'u stress at surface in N/m^2 s at {ustress.time[t].data:.13}')

ii+=1; ax=hca[ii]; cax=hcb[ii]
pyic.shade(lon, lat, vstress.isel(time=t).var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
ar=8
ax.quiver(lonw[::ar], latw[::ar], ustress.isel(time=t).var180[::ar,::ar], vstress.isel(time=t).var180[::ar,::ar])
ax.set_title(f'v stress at surface in N/m^2 s')
for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) 

if savefig == True: plt.savefig(fig_path +f'wind_stress_large_{t}.png', dpi=150, format='png', bbox_inches='tight')

# %%
hca, hcb = pyic.arrange_axes(2, 1, plot_cb='right', asp=asp, fig_size_fac=2, projection=ccrs_proj, axlab_kw=None, sharey=True)

scale = 3
ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 0.75
pyic.shade(lon, lat, ustress_week.var180, ax=ax, cax=cax,  transform=ccrs_proj, clim=clim, rasterized=False, cmap='RdBu_r')
ar=8
# ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar], vstress_week.var180[::ar,::ar])
l = 0.3
ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar].where(ustress_week.var180[::ar,::ar] > -l).where(ustress_week.var180[::ar,::ar] < l), vstress_week.var180[::ar,::ar].where(vstress_week.var180[::ar,::ar] > -l).where(vstress_week.var180[::ar,::ar] < l), scale=scale, label=f'arrow scale ={scale}')
ax.set_title(f'u stress at surface in N/m^2 s at {ustress.time[t].data:.13}')

ii+=1; ax=hca[ii]; cax=hcb[ii]
pyic.shade(lon, lat, vstress_week.var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
ar=8
# ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar], vstress_week.var180[::ar,::ar])
ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar].where(ustress_week.var180[::ar,::ar] > -l).where(ustress_week.var180[::ar,::ar] < l), vstress_week.var180[::ar,::ar].where(vstress_week.var180[::ar,::ar] > -l).where(vstress_week.var180[::ar,::ar] < l), scale=scale, label=f'arrow scale ={scale}')

ax.set_title(f'v stress at surface in N/m^2 s')
for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) 

if savefig == True: plt.savefig(fig_path +'wind_stress_large_mean_week.png', dpi=150, format='png', bbox_inches='tight')


# %%

t   = 0
hca, hcb = pyic.arrange_axes(2, 1, plot_cb=True, asp=asp, fig_size_fac=2, projection=ccrs_proj, axlab_kw=None, sharey=True)
ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 1
pyic.shade(lon, lat, wind_stress_curl.isel(time=t).var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj,  rasterized=False, cmap='RdBu_r')
ax.set_title(f'wind_stress_curl at {wind_stress_curl.time[t].data:.13}')

ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 10
pyic.shade(lon, lat, w_ek.isel(time=t).var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
ax.set_title(f'Ekman Pumping: w_ek')
for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) 

if savefig == True: plt.savefig(fig_path +f'wind_stress_curl_large_{t}.png', dpi=150, format='png', bbox_inches='tight')

# %% 1day 2day week
lon = lonw
lat = latw
lon_reg = lon_reg_2
lat_reg = lat_reg_2
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
t = 0
hca, hcb = pyic.arrange_axes(1, 3, plot_cb=True, asp=asp, fig_size_fac=4, projection=ccrs_proj, sharex=True, axlab_kw=None)

ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
ar=3
scale = 8
clim= 2
pyic.shade(lon, lat, w_ek_day.isel(time=0).var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
ax.quiver(lonw[::ar], latw[::ar], ustress_day.isel(time=t).var180[::ar,::ar], vstress_day.isel(time=t).var180[::ar,::ar], scale=scale, label=f'arrow scale ={scale}')
fu.add_front_lines_box(ax)
ax.set_title(f'w_ek 1 day: {ustress_day.time[0].data}', fontsize=20)
ax.legend()

ii+=1; ax=hca[ii]; cax=hcb[ii]
pyic.shade(lon, lat, w_ek_2d.isel(time=0).var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
ax.quiver(lonw[::ar], latw[::ar], ustress_2d.isel(time=t).var180[::ar,::ar], vstress_2d.isel(time=t).var180[::ar,::ar], scale=scale)
fu.add_front_lines_box(ax)
ax.set_title(f'w_ek 2 days: 16th & 17th ', fontsize=20)

ii+=1; ax=hca[ii]; cax=hcb[ii]
clim= 1
pyic.shade(lon, lat, w_ek_week.var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
scale = 3
ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar], vstress_week.var180[::ar,::ar], scale=scale, label=f'arrow scale ={scale}')
fu.add_front_lines_box(ax)
ax.set_title(f'w_ek: 1 week', fontsize=20)
ax.legend()
for ax in hca:
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) 
    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)

if savefig == True: plt.savefig(fig_path +'pumping_fronts_large.png', dpi=150, format='png', bbox_inches='tight')

# %% old

lon = lonw
lat = latw
lon_reg = np.array([-80,-30])
lat_reg = np.array([15,54])
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
print(asp)
t = 0
hca, hcb = pyic.arrange_axes(2, 1, plot_cb='right', asp=asp, fig_size_fac=4, projection=ccrs_proj, axlab_kw=None)

ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
ar=5
clim= 1
pyic.shade(lon, lat, w_ek_week.var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
scale = 3
l = 0.19
ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar].where(ustress_week.var180[::ar,::ar] > -l).where(ustress_week.var180[::ar,::ar] < l), vstress_week.var180[::ar,::ar].where(ustress_week.var180[::ar,::ar] > -l).where(vstress_week.var180[::ar,::ar] < l), scale=scale, label=f'arrow scale ={scale}')
fu.add_rectangles_red(ax, lon_reg_2, lat_reg_2, 'a')
ax.set_title(r'$w_{ek}$: in m/s week mean', fontsize=20)
pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) 

ii+=1; ax=hca[ii]; cax=hcb[ii]
ar=3
pyic.shade(lon, lat, w_ek_week.var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
scale = 3
ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar], vstress_week.var180[::ar,::ar], scale=scale, label=f'arrow scale ={scale}')
fu.add_front_lines_box(ax)
ax.set_title(f'a', fontsize=20)
ax.legend()
pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) 

for ax in hca:
    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)

if savefig == True: plt.savefig(fig_path +'pumping_fronts_week.png', dpi=100, format='png', bbox_inches='tight')

# %%

lon = lonw
lat = latw
lon_reg = lon_reg_0
lat_reg = lat_reg_0
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
print(asp)
t = 0
hca, hcb = pyic.arrange_axes(2, 1, plot_cb='right', asp=asp, fig_size_fac=4, projection=ccrs_proj, axlab_kw=None)

ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
ar=7
clim= 1
pyic.shade(lon, lat, w_ek_week.var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
scale = 3
l = 0.25
ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar].where(ustress_week.var180[::ar,::ar] > -l).where(ustress_week.var180[::ar,::ar] < l), vstress_week.var180[::ar,::ar].where(vstress_week.var180[::ar,::ar] > -l).where(vstress_week.var180[::ar,::ar] < l), scale=scale, label=f'arrow scale ={scale}')
fu.add_rectangles_red(ax, lon_reg_2, lat_reg_2, 'a')
ax.set_title(r'$w_{ek}$: in m/s week mean', fontsize=20)
pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) 

ii+=1; ax=hca[ii]; cax=hcb[ii]
ar=3
pyic.shade(lon, lat, w_ek_week.var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
scale = 3
ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar], vstress_week.var180[::ar,::ar], scale=scale, label=f'arrow scale ={scale}')
fu.add_front_lines_box(ax)
ax.set_title(f'a', fontsize=20)
ax.legend()
pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) 

for ax in hca:
    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)

if savefig == True: plt.savefig(fig_path +'pumping_fronts_week_large.png', dpi=150, format='png', bbox_inches='tight')

# %% week mean pumping

lon_reg = lon_reg_0
lat_reg = lat_reg_0
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
print(asp)
t = 0
hca, hcb = pyic.arrange_axes(1, 1, plot_cb='right', asp=asp, fig_size_fac=4, projection=ccrs_proj, axlab_kw=None)

ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
ar=10
clim= 1
scale = 3
l = 0.25
levels=15

pyic.shade(lon, lat, w_ek_week.var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar].where(ustress_week.var180[::ar,::ar] > -l).where(ustress_week.var180[::ar,::ar] < l), vstress_week.var180[::ar,::ar].where(vstress_week.var180[::ar,::ar] > -l).where(vstress_week.var180[::ar,::ar] < l), color='grey', scale=scale, label=f'arrow scale ={scale}')
CS = ax.contour(ds.longitude, ds.latitude, ds_mean, levels=levels, linewidths=1, colors='k')
ax.clabel(CS, inline=True, fontsize=10, zorder=2)
fu.add_rectangles_red(ax, lon_reg_2, lat_reg_2, 'a')
ax.set_title(r'$w_{ek}$: in m/s week mean', fontsize=20)

for ax in hca:
    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) 

if savefig == True: plt.savefig(fig_path +'pumping_fronts_week_large.png', dpi=150, format='png', bbox_inches='tight')
# %% instant pumping
t = 0

lon_reg = lon_reg_0
lat_reg = lat_reg_0
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
print(asp)
t = 0
hca, hcb = pyic.arrange_axes(2, 1, plot_cb='right', asp=asp, fig_size_fac=4, projection=ccrs_proj, axlab_kw=None, sharey=True)

ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]
ar=10#7
clim= 3
scale = 3
l = 0.25
levels=15

pyic.shade(lon, lat, w_ek.isel(time=t).var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
ax.quiver(lonw[::ar], latw[::ar], ustress.isel(time=t).var180[::ar,::ar].where(ustress.isel(time=t).var180[::ar,::ar] > -l).where(ustress.isel(time=t).var180[::ar,::ar] < l), vstress.isel(time=t).var180[::ar,::ar].where(vstress.isel(time=t).var180[::ar,::ar] > -l).where(vstress.isel(time=t).var180[::ar,::ar] < l), color='grey', scale=scale, label=f'arrow scale ={scale}')
ax.set_title(r'$w_{ek}$: in m/s at'+f'{ustress.time[t].data:.13}', fontsize=20)
CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=14).isel(step=21), levels=levels, linewidths=1, colors='k')
ax.clabel(CS, inline=True, fontsize=10, zorder=2)
fu.add_rectangles_red(ax, lon_reg_2, lat_reg_2, 'a')


ii+=1; ax=hca[ii]; cax=hcb[ii]
t = 168
pyic.shade(lon, lat, w_ek.isel(time=t).var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
ax.quiver(lonw[::ar], latw[::ar], ustress.isel(time=t).var180[::ar,::ar].where(ustress.isel(time=t).var180[::ar,::ar] > -l).where(ustress.isel(time=t).var180[::ar,::ar] < l), vstress.isel(time=t).var180[::ar,::ar].where(vstress.isel(time=t).var180[::ar,::ar] > -l).where(vstress.isel(time=t).var180[::ar,::ar] < l), color='grey', scale=scale, label=f'arrow scale ={scale}')
ax.set_title(r'$w_{ek}$: in m/s at'+f'{ustress.time[t].data:.13}', fontsize=20)
CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=21).isel(step=21), levels=levels, linewidths=1, colors='k')
ax.clabel(CS, inline=True, fontsize=10, zorder=2)
fu.add_rectangles_red(ax, lon_reg_2, lat_reg_2, 'a')


for ax in hca:
    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)
    pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) 

if savefig == True: plt.savefig(fig_path + f'pumping_fronts_week_large_{t}.png', dpi=150, format='png', bbox_inches='tight')


# %% week mean zoom
lon_reg = lon_reg_2
lat_reg = lat_reg_2
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])


hca, hcb = pyic.arrange_axes(1, 1, plot_cb='right', asp=asp, fig_size_fac=4, projection=ccrs_proj, axlab_kw=None)
ii=-1

ar    = 7
clim  = 1
ar    = 3
scale = 3
levles=30


ii+=1; ax=hca[ii]; cax=hcb[ii]
pyic.shade(lon, lat, w_ek_week.var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar], vstress_week.var180[::ar,::ar], color='grey', scale=scale, label=f'arrow scale ={scale}')
CS = ax.contour(ds.longitude, ds.latitude, ds_mean, levels=levels, linewidths=1, colors='k')
ax.clabel(CS, inline=True, fontsize=10, zorder=2)
fu.add_front_lines_box(ax)
ax.set_title(f'a', fontsize=20)
ax.legend()

for ax in hca:
    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)
    pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) 

if savefig == True: plt.savefig(fig_path +'pumping_fronts_week_zoom.png', dpi=150, format='png', bbox_inches='tight')

# %% zoom instant
lon_reg = lon_reg_2
lat_reg = lat_reg_2
asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
t = 0
hca, hcb = pyic.arrange_axes(2, 1, plot_cb='right', asp=asp, fig_size_fac=4, projection=ccrs_proj, axlab_kw=None, sharey=True)
ii=-1

ar    = 7
clim  = 3
ar    = 3
scale = 3

t = 0
ii+=1; ax=hca[ii]; cax=hcb[ii]
pyic.shade(lon, lat, w_ek.isel(time=t).var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
ax.quiver(lonw[::ar], latw[::ar], ustress.isel(time=t).var180[::ar,::ar], vstress.isel(time=t).var180[::ar,::ar], color='grey', scale=scale, label=f'arrow scale ={scale}')
CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=14).isel(step=21), levels=levels, linewidths=1, colors='k')
ax.clabel(CS, inline=True, fontsize=10, zorder=2)
fu.add_front_lines_box(ax)
ax.set_title(f'a:' + r'$w_{ek}$: in m/s at'+f'{ustress.time[t].data:.13}', fontsize=20)

t = 168
ii+=1; ax=hca[ii]; cax=hcb[ii]
pyic.shade(lon, lat, w_ek.isel(time=t).var180, ax=ax, cax=cax, clim=clim,  transform=ccrs_proj, rasterized=False, cmap='RdBu_r')
ax.quiver(lonw[::ar], latw[::ar], ustress.isel(time=t).var180[::ar,::ar], vstress.isel(time=t).var180[::ar,::ar], color='grey', scale=scale, label=f'arrow scale ={scale}')
CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=21).isel(step=21), levels=levels, linewidths=1, colors='k')
ax.clabel(CS, inline=True, fontsize=10, zorder=2)
fu.add_front_lines_box(ax)
ax.set_title(f'a:' + r'$w_{ek}$: in m/s at'+f'{ustress.time[t].data:.13}', fontsize=20)
ax.legend()

for ax in hca:
    ax.tick_params(labelsize=15)
    cax.tick_params(labelsize=15)
    pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) 

if savefig == True: plt.savefig(fig_path + f'pumping_fronts_week_zoom_{t}.png', dpi=150, format='png', bbox_inches='tight')

# %%
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
# lon = lonw
# lat = latw



# %%
lonw      = ustress.lon #.data
latw      = ustress.lat #.data

# %%
# path = '/work/mh0033/m300878/observation/test.nc'
# # path = '/work/mh0033/m300878/observation/pressure/E5sf00_1H_2010-03-01.nc'

# ds = xr.open_dataset(path, engine='cfgrib')
# %%
path_data = f'/work/mh0033/m300878/observation/pressure/'
search_str = f'E5sf00_1H_2010-03-*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
ds = xr.open_mfdataset(flist, engine='cfgrib', combine='nested', concat_dim='time' )
ds = ds.assign_coords(longitude=(((ds.longitude + 180) % 360) - 180))
ds = ds.sortby(ds.longitude)
ds = ds.sel(longitude=slice(lon_reg_0[0],lon_reg_0[1]))
ds = ds.sel(latitude=slice(lat_reg_0[1],lat_reg_0[0]))
ds['msl'] = ds.msl / 1e2



# %% zoom instant
reload(fu)
fu.plot_forcing_state(lon_reg_2, lat_reg_2, lonw, latw, ustress, vstress, ustress_week, vstress_week, w_ek, w_ek_week, ds, ds_mean, levels, fig_path,  savefig)

# %%
#animation
fu.create_wind_animation(lon_reg_2, lat_reg_2, lonw, latw, ustress, vstress, fig_path)
# %%

# %% Correct Pressure field  - adpat time to match the wind stress
if False:
    path_data = '/pool/data/ERA5/E5/sf/an/1H/151/E5sf00_1H_2010-03-15_151.grb'
    path_data = '/home/m/m300878/testgrid.nc'
    pressure = xr.open_dataset(path_data, engine='cfgrib', backend_kwargs=  {'indexpath': ''})

    # %%
    pressure.isel(time=0).msl

    hca, hcb = pyic.arrange_axes(1, 1, plot_cb='right', fig_size_fac=4,     projection=ccrs_proj, axlab_kw=None)
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    plt.contourf(pressure.longitude, pressure.latitude, pressure.isel(time=0).msl)

    # %%
    path = '/work/mh0033/m300878/observation/test.nc'
    # path = '/work/mh0033/m300878/observation/pressure/E5sf00_1H_2010-03-01.nc'

    ds = xr.open_dataset(path, engine='cfgrib')
    # %%
    path_data = f'/work/mh0033/m300878/observation/pressure/'
    search_str = f'E5sf00_1H_2010-03-*.nc' 
    flist      = np.array(glob.glob(path_data+search_str))
    flist.sort()
    ds = xr.open_mfdataset(flist, engine='cfgrib', combine='nested',    concat_dim='time' )
    ds = ds.assign_coords(longitude=(((ds.longitude + 180) % 360) - 180))
    ds = ds.sortby(ds.longitude)
    ds = ds.sel(longitude=slice(lon_reg_0[0],lon_reg_0[1]))
    ds = ds.sel(latitude=slice(lat_reg_0[1],lat_reg_0[0]))
    # %%
    ds['msl'] = ds.msl / 1e2
    # %%
