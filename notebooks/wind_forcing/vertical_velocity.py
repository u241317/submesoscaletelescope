# %%
%%capture
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

from netCDF4 import Dataset, num2date
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics
from importlib import reload
# %%
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import cartopy.crs as ccrs
projection = ccrs.PlateCarree()
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from   cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import cmocean

def calc_triangulation_obj(data, ds_grid, lon_reg, lat_reg):
    """create triangulation object for highres plot
    Example:
    pyic.shade(Tri, data_reg...)
    Todo: make it a full xarray calculation
    """ 
    print('Deriving triangulation object, this can take a while...')
    clon = np.rad2deg(ds_grid.clon.values)
    clat = np.rad2deg(ds_grid.clat.values)
    vlon = np.rad2deg(ds_grid.vlon.values)
    vlat = np.rad2deg(ds_grid.vlat.values)
    # vertex_of_cell = ds_grid.vertex_of_cell.drop('cell_sea_land_mask').values.transpose()-1
    vertex_of_cell = ds_grid.vertex_of_cell.values.transpose()-1

    ind_reg = np.where(   (clon >  lon_reg[0])
                        & (clon <= lon_reg[1])
                        & (clat >  lat_reg[0])
                        & (clat <= lat_reg[1]) )[0]
    vertex_of_cell_reg = vertex_of_cell[ind_reg,:]
    Tri = matplotlib.tri.Triangulation(vlon, vlat, triangles=vertex_of_cell_reg)
    data_reg = data.compute().data[ind_reg]
    print('Done deriving triangulation object.')
    return Tri, data_reg


def plot_triangles(Tri, data_reg, cmap, clim, string):
    plt.figure(figsize=(20,20))
    ax = plt.axes(projection=projection)
    cb = ax.tripcolor(Tri, data_reg, shading='flat', cmap=cmap, vmin=clim[0], vmax=clim[1], rasterized=True, transform=projection)
    plt.colorbar(cb, orientation = 'vertical', label=f'{string}')
    ax.add_feature(cfeature.LAND, facecolor='lightgrey', zorder=6)
    ax.add_feature(cfeature.COASTLINE, linewidth=0.8, zorder=7)
    plt.xlim(lon_reg)
    plt.ylim(lat_reg)
    ax.set_xticks(np.arange(lon_reg[0], lon_reg[1], 4), crs=projection)
    ax.set_yticks(np.arange(lat_reg[0], lat_reg[1], 4), crs=projection)
    ax.xaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
    ax.yaxis.set_major_locator(plt.MaxNLocator(5)) # type: ignore
    ax.xaxis.set_major_formatter(LONGITUDE_FORMATTER)
    ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)
# %%



# %%
ds = eva.load_smt_w()

# %%
lon_reg = -80, -60
lat_reg = 30, 40
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'

ds_reg = pyic.interp_to_rectgrid_xr(ds.isel(time=0).isel(depthi=10).w, fpath_ckdtree, lon_reg, lat_reg)
# %%
import cmocean
plt.figure(figsize=(10,5))
ds_reg.plot(vmin=-1e-3,vmax=1e-3, cmap = cmocean.cm.balance )
# %%
#feature
ds_tgrid = eva.load_smt_grid()
ds_sel   = ds.isel(time=0).isel(depthi=10).w
lon_reg  = -70, -65
lat_reg  = 35,  40


Tri, data_reg = calc_triangulation_obj(ds_sel, ds_tgrid, lon_reg, lat_reg)

# %%
clim = -2e-3, 2e-3
cmap = cmocean.cm.balance
plot_triangles(Tri, data_reg, cmap, clim, string=r'T')
# %%
