# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

from netCDF4 import Dataset, num2date
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt

import matplotlib.animation as animation
from matplotlib.animation import PillowWriter
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

from scipy import stats    #Used for 2D binned statistics
from importlib import reload

def add_front_lines_box(ax, color='r'):
    points = eva.get_points_of_inclined_fronts()
    for ii in np.arange(int(points.shape[0]/2)):
        P = points[2*ii:2*ii+2,:]
        y, x, m, b = eva.calc_line(P)
        delta = 0.8*0.1 # 0.8 view 0.1 actual front
        lw    = 1.5
        ls    = ':'
        # ax.plot(x,y, lw=lw, color=color, ls=ls)
        ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
        ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
        ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
        ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
        if ii == 11 or ii == 12: continue
        t = ax.text(P[0,0]-0.05, P[0,1]+0.2, f'F{ii+1}', color='black', fontsize=7)
        t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white', pad=0.1))

# def add_front_lines_box(ax):
#     points = eva.get_points_of_inclined_fronts()
#     for ii in np.arange(int(points.shape[0]/2)):
#         P = points[2*ii:2*ii+2,:]
#         y, x, m, b = eva.calc_line(P)
#         delta=0.8*0.1 # 0.8 view 0.1 actual front
#         color = 'purple'
#         lw = 3
#         ls = '-'
#         # ax.plot(x,y, lw=lw, color=color, ls=ls)
#         ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
#         ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
#         ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
#         ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
#         t = ax.text(P[0,0]-0.05, P[0,1], f'F{ii+1}', color='black', fontsize=15)
#         t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


def add_rectangles_red(ax, lon_reg, lat_reg, txt):
    rect, right, top = eva.draw_rect(lat_reg, lon_reg,  color='r')
    ax.add_patch(rect)
    ax.text(lon_reg[1]+0.2, lat_reg[0]-0.3,  f'{txt}', color='r', fontsize=20)



def crop_quiver(ax, lon_reg, lat_reg, data_x, data_y, lonw, latw, ar ):
    lon    = lonw.where((lonw > lon_reg[0]) & (lonw < lon_reg[1]), drop=True)
    lat    = latw.where((latw > lat_reg[0]) & (latw < lat_reg[1]), drop=True)
    data_x = data_x.where((lonw > lon_reg[0]) & (lonw < lon_reg[1]) & (latw > lat_reg[0]) & (latw < lat_reg[1]), drop=True)
    data_y = data_y.where((lonw > lon_reg[0]) & (lonw < lon_reg[1]) & (latw > lat_reg[0]) & (latw < lat_reg[1]), drop=True)
    ax.quiver(lon[::ar], lat[::ar], data_x[::ar,::ar], data_y[::ar,::ar], color='grey')

def plot_forcing_state(lon_reg_2, lat_reg_2, lonw, latw, ustress, vstress, ustress_week, vstress_week, w_ek, w_ek_week, ds, ds_mean, levels, fig_path,  savefig):

    lon_reg = lon_reg_2
    lat_reg = lat_reg_2
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    t = 0
    hca, hcb = pyic.arrange_axes(2, 2, plot_cb='bottom', asp=asp, sharex=False,     fig_size_fac=1.6, projection=ccrs_proj, daxl=0.7, daxt=0.3,  dcbt=0.25) #   daxt=1.2,
    ii=-1

    clim   = 5e-4
    ar     = 10
    scale  = 1
    cmap   = 'twilight_shifted'
    levels = 20

    print(asp)
    lon0 = [-80, -30]
    lat0 = [ 15,  54]
    asp = (lat0[1]-lat0[0])/(lon0[1]-lon0[0])
    print(asp)

    per_time = 1/60*60

    ii+=1; ax=hca[ii]; cax=hcb[ii]


    if False:
        pyic.shade(lonw, latw, w_ek_week.var180/per_time, ax=ax, cax=cax,   clim=clim, contfs=True,  transform=ccrs_proj, rasterized=False, cmap=cmap)
        crop_quiver(ax, lon0, lat0, ustress_week.var180/per_time, vstress_week. var180/per_time, lonw, latw, ar=8)
        CS = ax.contour(ds.longitude, ds.latitude, ds_mean, levels=levels,  linewidths=1, colors='k')
        ax.clabel(CS, inline=True, zorder=2)
        add_rectangles_red(ax, lon_reg_2, lat_reg_2, 'R')
        ax.set_title(r'weekly mean')
    else:
        t = 84
        TIME = w_ek.isel(time=t).time.values
        pyic.shade(lonw, latw, w_ek.isel(time=t).var180/per_time, ax=ax, cax=cax,   clim=clim, contfs=True,  transform=ccrs_proj, rasterized=False, cmap=cmap)
        # ax.quiver(lonw[::ar], latw[::ar], ustress.isel(time=t).var180[::ar,::ar],     vstress.isel(time=t).var180[::ar,::ar], color='grey', scale=scale,  label=f'arrow scale ={scale}')
        crop_quiver(ax, lon0, lat0, ustress.isel(time=t).var180/per_time, vstress.  isel(time=t).var180/per_time, lonw, latw, ar=8)
        CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=18).isel    (step=9), levels=levels, linewidths=1, colors='k')
        ax.clabel(CS, inline=True, zorder=2)
        add_rectangles_red(ax, lon_reg_2, lat_reg_2, 'R')
        ax.set_title( rf'{ustress.time[t].data:.13}')



    ii+=1; ax=hca[ii]; cax=hcb[ii]
    levels = 20
    # ar    = 4
    pyic.shade(lonw, latw, w_ek_week.var180/per_time, ax=ax, cax=cax, clim=clim,    contfs=True,  transform=ccrs_proj, rasterized=False, cmap=cmap)
    # ax.quiver(lonw[::ar], latw[::ar], ustress_week.var180[::ar,::ar],     vstress_week.var180[::ar,::ar], color='grey')
    crop_quiver(ax, lon_reg_2, lat_reg_2, ustress_week.var180/per_time,     vstress_week.var180/per_time, lonw, latw, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds_mean, levels=levels,  linewidths=1, colors='k')
    ax.clabel(CS, inline=True, zorder=2)
    add_front_lines_box(ax)
    ax.set_title(f'weekly mean')
    # ax.legend()
    te = ax.text(lon_reg_2[1]-1, lat_reg_2[1]-1.1, 'R', color='red', fontsize=13)
    te.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


    ii+=1; ax=hca[ii]; cax=hcb[ii]
    t = 0
    pyic.shade(lonw, latw, w_ek.isel(time=t).var180/per_time, ax=ax, cax=cax,   clim=clim, contfs=True,  transform=ccrs_proj, rasterized=False, cmap=cmap)
    # ax.quiver(lonw[::ar], latw[::ar], ustress.isel(time=t).var180[::ar,::ar],     vstress.isel(time=t).var180[::ar,::ar], color='grey')
    crop_quiver(ax, lon_reg_2, lat_reg_2, ustress.isel(time=t).var180/per_time,     vstress.isel(time=t).var180/per_time, lonw, latw, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=14).isel(step=21),  levels=levels, linewidths=1, colors='k')
    ax.clabel(CS, inline=True, zorder=2)
    ax.set_title( rf'{ustress.time[t].data:.13}')
    te = ax.text(lon_reg_2[1]-1, lat_reg_2[1]-1.1, 'R', color='red', fontsize=13)
    te.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    t = 168
    hm2 = pyic.shade(lonw, latw, w_ek.isel(time=t).var180/per_time, ax=ax, cax=cax,     clim=clim, contfs=True, transform=ccrs_proj, rasterized=False, cmap=cmap,   cbtitle='Ekman pumping [m/h]' ) #'Ekman \n pumping \n [m/h] \n .'

    # ax.quiver(lonw[::ar], latw[::ar], ustress.isel(time=t).var180[::ar,::ar],     vstress.isel(time=t).var180[::ar,::ar], color='grey')
    crop_quiver(ax, lon_reg_2, lat_reg_2, ustress.isel(time=t).var180/per_time,     vstress.isel(time=t).var180/per_time, lonw, latw, ar=4)

    CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=21).isel(step=21),  levels=levels, linewidths=1, colors='k')
    # hm2[1].ax.yaxis.offsetText.set_position((-2.1,0))

    ax.clabel(CS, inline=True, zorder=2)
    ax.set_title( rf'{ustress.time[t].data:.13}')
    # ax.legend()
    te = ax.text(lon_reg_2[1]-1, lat_reg_2[1]-1.1, 'R', color='red', fontsize=13)
    te.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


    for i, ax in enumerate(hca):
        if i == 0:
            pyic.plot_settings(ax, xlim=lon0, ylim=lat0) 
        else:
            add_front_lines_box(ax)
            pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) 
    
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    
    
    if savefig == True: plt.savefig(fig_path + f'windforcing_temp_{t}_v3.png',  dpi=250, format='png', bbox_inches='tight')
    
    

def create_wind_animation(lon_reg_2, lat_reg_2, lonw, latw, ustress, vstress, fig_path):
    fig, ax = plt.subplots(1, 1, subplot_kw={'projection': ccrs_proj}, figsize=(10, 10))
    per_time = 1/60*60
    ar=4
    def update(t):
        ax.clear()
        add_front_lines_box(ax)
        crop_quiver(ax, lon_reg_2, lat_reg_2, ustress.isel(time=t).var180/per_time,     vstress.isel(time=t).var180/per_time, lonw, latw, ar=ar)
        ax.set_title(f'Wind stress at {ustress.time[t].data:.13}')
        return ax

    ani = animation.FuncAnimation(fig, update, frames=range(0, 169, 1), repeat=False)
    writer = PillowWriter(fps=8)
    ani.save(fig_path + 'wind_stress.gif', writer=writer)

def compute_tau_xy_from_windstress(ustress, vstress):
    latw         = ustress.lat #.data
    rh0          = 1024
    fe           = 2.* 2.*np.pi/86400. * np.sin(latw*np.pi/180.) # in 1/s
    tau_x_scaled = ustress/rh0/fe # wind stress scaled by coriolis parameter
    tau_y_scaled = vstress/rh0/fe
    return tau_x_scaled, tau_y_scaled

def plot_single_front_margins(ax,    color = 'r'):
    points = eva.get_points_of_inclined_fronts()
    P = points[0:2,:]
    y, x, m, b = eva.calc_line(P)
    delta = 0.8*0.1 # 0.8 view 0.1 actual front

    lw    = 3
    ls    = ':'
    ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
    ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
    ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
    ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
    t = ax.text(P[0,0]-0.05, P[0,1]+0.2, f'F{1}', color='black', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

def plot_single_front_margins_fn(ax, fn):
    points = eva.get_points_of_inclined_fronts()
    P = points[(2*fn-2):2*fn,:]

    y, x, m, b = eva.calc_line(P)
    delta = 0.8*0.1 # 0.8 view 0.1 actual front
    color = 'r'
    lw    = 3
    ls    = ':'
    ax.plot(x,y+delta, lw=lw, color=color, ls=ls)
    ax.plot(x,y-delta, lw=lw, color=color, ls=ls)
    ax.vlines(x[0],(y+delta)[0], (y-delta)[0], lw=lw, color=color, ls=ls)
    ax.vlines(x[-1],(y+delta)[-1], (y-delta)[-1], lw=lw, color=color, ls=ls)
    t = ax.text(P[0,0]-0.05, P[0,1]+0.2, f'F{fn}', color='black', fontsize=20)
    t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


def add_rectangles(ax, lon_reg, lat_reg, txt, color='black'):
    rect, right, top = eva.draw_rect(lat_reg, lon_reg,  color=color)
    ax.add_patch(rect)
    ax.text(lon_reg[1], lat_reg[1], f'{txt}', color=color, fontsize=20)



def plot_EBF(data, savefig, fig_path, twindow):
    lon_reg_2 = np.array([data.lon.min(), data.lon.max()])
    lat_reg_2 = np.array([data.lat.min(), data.lat.max()])
    asp2 = (lat_reg_2[1]-lat_reg_2[0])/(lon_reg_2[1]-lon_reg_2[0])

    hca, hcb = pyic.arrange_axes(2, 1,  asp=asp2, plot_cb=[0,1], fig_size_fac=2, daxl=0.9,  # type: ignore
                                 projection=ccrs_proj,
                                 daxt=1, f_dcbt=1.5)
    ii=-1

    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 5e-8
    cmap = 'twilight_shifted'
    ax.set_title(rf'$EBF~[ m^2/s^3]$ ')

    hm1 = pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, cmap=cmap,
                    transform=ccrs_proj, rasterized=False)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)


    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = np.array([-63.9, -61.5])
    lat_RF1 = np.array([34, 35.875])
    lon_reg_3 = np.array([-63.9, -61.5])
    lat_reg_3 = np.array([34, 35.875])

    i=0
    for ax in hca:
        i += 1
        if i%2 == 1:
            add_front_lines_box(ax, color='purple')
            add_rectangles(ax, lon_reg_3, lat_reg_3, '', color='red')
            t = ax.text(lon_R[1]-0.9, lat_R[1]-1, 'R', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
            pyic.plot_settings(ax, xlim=lon_reg_2, ylim=lat_reg_2) # type: ignore
        else:
            pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3) # type: ignore
            plot_single_front_margins(ax, color='purple')
            t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore

    fname = f'EBF_{twindow}_s'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250, format='png', bbox_inches='tight')

def plot_EBF_comp(EBF_x, EBF_y, savefig, fig_path, twindow):
    asp =  (EBF_x.lat.max() - EBF_x.lat.min()) / (EBF_x.lon.max() - EBF_x.lon.min()) 
    hca, hcb = pyic.arrange_axes(2, 1,  plot_cb='right', asp=asp, fig_size_fac=1.5, projection=ccrs_proj, sharey=True)
    cmap = 'RdBu_r'
    clim = 3e-8
    ii=-1

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(EBF_x.lon, EBF_x.lat, EBF_x, ax=ax, cax=cax,   clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap)
    ax.set_title(rf'$EBF_x ~[m^2/s^3]$')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(EBF_y.lon, EBF_y.lat, EBF_y, ax=ax, cax=cax,   clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap)
    ax.set_title(r'$EBF_y ~[m^2/s^3]$')
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    for ax in hca:
        pyic.plot_settings(ax)
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        add_front_lines_box(ax, color='purple')

    if savefig == True: plt.savefig(fig_path + f'ebf_comp_{twindow}.png',  dpi=250, format='png', bbox_inches='tight')


def plot_forcing_state_EBF_4(lon_reg, lat_reg, lonw, latw, ustress, vstress, ustress_week, vstress_week, EBF_inst0, EBF_inst1, EBF_inst2, EBF_mean, ds, ds_mean, fig_path,  savefig):
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    t = 0
    hca, hcb = pyic.arrange_axes(2, 2, plot_cb='bottom', asp=asp, sharex=False,  fig_size_fac=1.6, projection=ccrs_proj, daxl=0.7, daxt=0.3,  dcbt=0.25) #   daxt=1.2,
    ii=-1

    clim   = 3e-7
    cmap   = 'twilight_shifted'
    levels = 20

    print(asp)
    lon0 = [-80, -30]
    lat0 = [ 15,  54]
    asp = (lat0[1]-lat0[0])/(lon0[1]-lon0[0])
    print(asp)
    ii+=1; ax=hca[ii]; cax=hcb[ii]


    if False:
        # pyic.shade(lonw, latw, w_ek_week.var180, ax=ax, cax=cax,   clim=clim, contfs=True,  transform=ccrs_proj, rasterized=False, cmap=cmap)
        crop_quiver(ax, lon0, lat0, ustress_week.var180, vstress_week. var180/per_time, lonw, latw, ar=8)
        CS = ax.contour(ds.longitude, ds.latitude, ds_mean, levels=levels,  linewidths=1, colors='k')
        ax.clabel(CS, inline=True, zorder=2)
        add_rectangles_red(ax, lon_reg_2, lat_reg_2, 'R')
        ax.set_title(r'weekly mean')
    else:
        t = 84
        # TIME = w_ek.isel(time=t).time.values
        pyic.shade(EBF_inst1.lon, EBF_inst1.lat, EBF_inst1, ax=ax, cax=cax,   clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap)
        # ax.quiver(lonw[::ar], latw[::ar], ustress.isel(time=t).var180[::ar,::ar],     vstress.isel(time=t).var180[::ar,::ar], color='grey', scale=scale,  label=f'arrow scale ={scale}')
        crop_quiver(ax, lon0, lat0, ustress.isel(time=t).var180, vstress.isel(time=t).var180, lonw, latw, ar=8)
        CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=18).isel(step=9), levels=levels, linewidths=1, colors='k')
        ax.clabel(CS, inline=True, zorder=2)
        add_rectangles_red(ax, lon_reg, lat_reg, 'R')
        ax.set_title( rf'{ustress.time[t].data:.13}')



    ii+=1; ax=hca[ii]; cax=hcb[ii]
    levels = 20
    pyic.shade(EBF_mean.lon, EBF_mean.lat, EBF_mean, ax=ax, cax=cax, clim=clim,     transform=ccrs_proj, rasterized=False, cmap=cmap)
    crop_quiver(ax, lon_reg, lat_reg, ustress_week.var180,     vstress_week.var180, lonw, latw, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds_mean, levels=levels,  linewidths=1, colors='k')
    ax.clabel(CS, inline=True, zorder=2)
    add_front_lines_box(ax)
    ax.set_title(f'weekly mean')
    te = ax.text(lon_reg[1]-1, lat_reg[1]-1.1, 'R', color='red', fontsize=13)
    te.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


    ii+=1; ax=hca[ii]; cax=hcb[ii]
    t = 0
    pyic.shade(EBF_mean.lon, EBF_mean.lat, EBF_inst0, ax=ax, cax=cax,   clim=clim,   transform=ccrs_proj, rasterized=False, cmap=cmap)
    crop_quiver(ax, lon_reg, lat_reg, ustress.isel(time=t).var180,     vstress.isel(time=t).var180, lonw, latw, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=14).isel(step=21),  levels=levels, linewidths=1, colors='k')
    ax.clabel(CS, inline=True, zorder=2)
    ax.set_title( rf'{ustress.time[t].data:.13}')
    te = ax.text(lon_reg[1]-1, lat_reg[1]-1.1, 'R', color='red', fontsize=13)
    te.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    t = 168
    hm2 = pyic.shade(EBF_mean.lon, EBF_mean.lat, EBF_inst2, ax=ax, cax=cax,     clim=clim, transform=ccrs_proj, rasterized=False, cmap=cmap,   cbtitle=r'EBF $[m^2/s^3]$' ) #'Ekman \n pumping \n [m/h] \n .'
    crop_quiver(ax, lon_reg, lat_reg, ustress.isel(time=t).var180,     vstress.isel(time=t).var180, lonw, latw, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=21).isel(step=21),  levels=levels, linewidths=1, colors='k')

    ax.clabel(CS, inline=True, zorder=2)
    ax.set_title( rf'{ustress.time[t].data:.13}')
    te = ax.text(lon_reg[1]-1, lat_reg[1]-1.1, 'R', color='red', fontsize=13)
    te.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


    for i, ax in enumerate(hca):
        if i == 0:
            pyic.plot_settings(ax, xlim=lon0, ylim=lat0) 
        else:
            add_front_lines_box(ax)
            pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) 
    
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    
    
    if savefig == True: plt.savefig(fig_path + f'forcing_EBF_sing_4.png',  dpi=250, format='png', bbox_inches='tight')
    

def plot_forcing_state_EBF(lon_reg, lat_reg, lonw, latw, ustress, vstress, ustress_week, vstress_week,EBF_inst1,  EBF_mean, ds, ds_mean, fig_path,  savefig):
    asp = (lat_reg[1]-lat_reg[0])/(lon_reg[1]-lon_reg[0])
    t = 0
    hca, hcb = pyic.arrange_axes(2, 1, plot_cb=[1,1], asp=asp, sharex=False,  fig_size_fac=1.6, projection=ccrs_proj, daxl=0.7, daxt=0.3,  dcbr=1.9) #   daxt=1.2,
    ii=-1

    clim   = 3e-7/3600
    cmap   = 'twilight_shifted'
    levels = 20

    print(asp)
    lon0 = [-80, -30]
    lat0 = [ 15,  54]
    asp = (lat0[1]-lat0[0])/(lon0[1]-lon0[0])
    print(asp)
    ii+=1; ax=hca[ii]; cax=hcb[ii]


    if False:
        # pyic.shade(lonw, latw, w_ek_week.var180, ax=ax, cax=cax,   clim=clim, contfs=True,  transform=ccrs_proj, rasterized=False, cmap=cmap)
        crop_quiver(ax, lon0, lat0, ustress_week.var180, vstress_week. var180/per_time, lonw, latw, ar=8)
        CS = ax.contour(ds.longitude, ds.latitude, ds_mean, levels=levels,  linewidths=1, colors='k')
        ax.clabel(CS, inline=True, zorder=2)
        add_rectangles_red(ax, lon_reg_2, lat_reg_2, 'R')
        ax.set_title(r'weekly mean')
    else:
        t = 84
        # TIME = w_ek.isel(time=t).time.values
        hm2 = pyic.shade(EBF_inst1.lon, EBF_inst1.lat, EBF_inst1, ax=ax, cax=cax,   clim=clim,  transform=ccrs_proj, rasterized=False, cmap=cmap)
        # ax.quiver(lonw[::ar], latw[::ar], ustress.isel(time=t).var180[::ar,::ar],     vstress.isel(time=t).var180[::ar,::ar], color='grey', scale=scale,  label=f'arrow scale ={scale}')
        crop_quiver(ax, lon0, lat0, ustress.isel(time=t).var180, vstress.isel(time=t).var180, lonw, latw, ar=8)
        CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=18).isel(step=9), levels=levels, linewidths=1, colors='k')
        ax.clabel(CS, inline=True, zorder=2)
        add_rectangles_red(ax, lon_reg, lat_reg, 'R')
        ax.set_title( rf'snapshot {ustress.time[t].data:.13}')
        hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
        hm2[1].formatter.set_useMathText(True)



    ii+=1; ax=hca[ii]; cax=hcb[ii]
    levels = 20
    clim = 5e-8/3600
    hm2 = pyic.shade(EBF_mean.lon, EBF_mean.lat, EBF_mean, ax=ax, cax=cax, clim=clim,     transform=ccrs_proj, rasterized=False, cmap=cmap)
    crop_quiver(ax, lon_reg, lat_reg, ustress_week.var180,     vstress_week.var180, lonw, latw, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds_mean, levels=levels,  linewidths=1, colors='k')
    ax.clabel(CS, inline=True, zorder=2)
    add_front_lines_box(ax)
    ax.set_title(f'week mean')
    te = ax.text(lon_reg[1]-1, lat_reg[1]-1.1, 'R', color='red', fontsize=13)
    te.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)


    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # t = 0
    # pyic.shade(EBF_mean.lon, EBF_mean.lat, EBF_inst0, ax=ax, cax=cax,   clim=clim,   transform=ccrs_proj, rasterized=False, cmap=cmap)
    # crop_quiver(ax, lon_reg, lat_reg, ustress.isel(time=t).var180,     vstress.isel(time=t).var180, lonw, latw, ar=4)
    # CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=14).isel(step=21),  levels=levels, linewidths=1, colors='k')
    # ax.clabel(CS, inline=True, zorder=2)
    # ax.set_title( rf'{ustress.time[t].data:.13}')
    # te = ax.text(lon_reg[1]-1, lat_reg[1]-1.1, 'R', color='red', fontsize=13)
    # te.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # t = 168
    # hm2 = pyic.shade(EBF_mean.lon, EBF_mean.lat, EBF_inst2, ax=ax, cax=cax,     clim=clim, transform=ccrs_proj, rasterized=False, cmap=cmap,   cbtitle=r'EBF $[m^2/s^3]$' ) #'Ekman \n pumping \n [m/h] \n .'
    # crop_quiver(ax, lon_reg, lat_reg, ustress.isel(time=t).var180,     vstress.isel(time=t).var180, lonw, latw, ar=4)
    # CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=21).isel(step=21),  levels=levels, linewidths=1, colors='k')

    # ax.clabel(CS, inline=True, zorder=2)
    # ax.set_title( rf'{ustress.time[t].data:.13}')
    # te = ax.text(lon_reg[1]-1, lat_reg[1]-1.1, 'R', color='red', fontsize=13)
    # te.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))


    for i, ax in enumerate(hca):
        if i == 0:
            pyic.plot_settings(ax, xlim=lon0, ylim=lat0) 
        else:
            add_front_lines_box(ax)
            pyic.plot_settings(ax, xlim=lon_reg, ylim=lat_reg) 
    
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    
    
    if savefig == True: plt.savefig(fig_path + f'forcing_EBF_sing_s.png',  dpi=250, format='png', bbox_inches='tight')
    


def plot_EBF_all(lon_reg, lat_reg, lonw, latw, data, EBF_inst0, EBF_inst2,  ustress, vstress, ustress_week, vstress_week, ds, ds_mean, savefig, fig_path, twindow):
    lon_reg_mean = np.array([data.lon.min(), data.lon.max()])
    lat_reg_mean = np.array([data.lat.min(), data.lat.max()])
    lon_reg = lon_reg_mean.data
    lat_reg = lat_reg_mean.data

    lon0 = [-80, -30]
    lat0 = [ 15,  54]
    asp = (lat0[1]-lat0[0])/(lon0[1]-lon0[0])
    print(asp)
    hca, hcb = pyic.arrange_axes(2, 2,  asp=asp, plot_cb=[0,1,0,1], fig_size_fac=2, daxl=0.9,  # type: ignore
                                 projection=ccrs_proj,
                                 daxt=1, f_dcbt=1.5)
    ii=-1

    EBF_mean = data
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 5e-7
    cmap = 'twilight_shifted'
    t = 0
    levels = 20
    pyic.shade(EBF_inst0.lon, EBF_inst0.lat, EBF_inst0, ax=ax, cax=cax,   clim=clim,   transform=ccrs_proj, rasterized=False, cmap=cmap)
    # crop_quiver(ax, lon_reg, lat_reg, ustress.isel(time=t).var180,     vstress.isel(time=t).var180, lonw, latw, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=14).isel(step=21),  levels=levels, linewidths=1, colors='k')
    ax.clabel(CS, inline=True, zorder=2)
    ax.set_title( rf'{ustress.time[t].data:.13}')
    add_rectangles_red(ax, lon_reg, lat_reg, 'R')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    t = 168
    hm2 = pyic.shade(EBF_inst2.lon, EBF_inst2.lat, EBF_inst2, ax=ax, cax=cax,     clim=clim, transform=ccrs_proj, rasterized=False, cmap=cmap,   cbtitle=r'$[m^2/s^3] $'+'\n .' ) #'Ekman \n pumping \n [m/h] \n .'
    # crop_quiver(ax, lon_reg, lat_reg, ustress.isel(time=t).var180,     vstress.isel(time=t).var180, lonw, latw, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=21).isel(step=21),  levels=levels, linewidths=1, colors='k')

    ax.clabel(CS, inline=True, zorder=2)
    ax.set_title( rf'{ustress.time[t].data:.13}')
    add_rectangles_red(ax, lon_reg, lat_reg, 'R')

    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)


    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 5e-8
    ax.set_title(rf'week mean ')

    hm1 = pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, cmap=cmap, 
                    transform=ccrs_proj, rasterized=False)
    # crop_quiver(ax, lon_reg, lat_reg, ustress_week.var180,     vstress_week.var180, lonw, latw, ar=4)
    crop_quiver(ax, lon_reg, lat_reg, ustress_week,     vstress_week, lonw, latw, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds_mean, levels=levels,  linewidths=0.5, colors='k')
    ax.clabel(CS, inline=True, zorder=2)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(rf'week mean (zoom) ')

    hm2 = pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, cmap=cmap, contfs=True, nclev=21, transform=ccrs_proj, rasterized=False,   cbtitle=r'$[m^2/s^3]$'+'\n .' )
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)


    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = np.array([-63.9, -61.5])
    lat_RF1 = np.array([34, 35.875])
    lon_reg_3 = np.array([-63.9, -61.5])
    lat_reg_3 = np.array([34, 35.875])

    for i, ax in enumerate(hca):
        if i <= 1:
            pyic.plot_settings(ax, xlim=lon0, ylim=lat0) 
        elif i == 2:
            add_rectangles(ax, lon_reg_3, lat_reg_3, '', color='red')
            t = ax.text(lon_R[1]-0.9, lat_R[1]-1, 'R', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

            add_front_lines_box(ax)
            pyic.plot_settings(ax, xlim=lon_reg_mean, ylim=lat_reg_mean) 
        elif i == 3:
            plot_single_front_margins(ax, color='r')
            t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, 'RF1', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
            pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3)

        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    
    fname = f'EBF_{twindow}_new'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250, format='png', bbox_inches='tight')




def plot_EBF_all_f(lon_reg, lat_reg, lonw, latw, data, EBF_inst0, EBF_inst2,  ustress, vstress, ustress_week, vstress_week, ds, ds_mean, savefig, fig_path, twindow, fn, lon_reg_3, lat_reg_3):
    lon_reg_mean = np.array([data.lon.min(), data.lon.max()])
    lat_reg_mean = np.array([data.lat.min(), data.lat.max()])
    lon_reg = lon_reg_mean.data
    lat_reg = lat_reg_mean.data

    lon0 = [-80, -30]
    lat0 = [ 15,  54]
    asp = (lat0[1]-lat0[0])/(lon0[1]-lon0[0])
    print(asp)
    hca, hcb = pyic.arrange_axes(2, 2,  asp=asp, plot_cb=[0,1,0,1], sharex=False, fig_size_fac=2, daxl=0.9, projection=ccrs_proj, daxt=0.5, f_dcbt=1.5)
    ii=-1

    EBF_mean = data
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 0,0.7
    cmap = 'YlOrRd'
    t = 0
    levels = 20
    pyic.shade(ustress.lon, ustress.lat, (np.sqrt(ustress**2 + vstress**2)).isel(time=t), ax=ax, cax=cax,   clim=clim,   transform=ccrs_proj, rasterized=False, cmap=cmap)
    crop_quiver(ax, lon0, lat0, ustress.isel(time=t),     vstress.isel(time=t), ustress.lon, ustress.lat, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=14).isel(step=21),  levels=levels, linewidths=1, colors='k')
    ax.clabel(CS, inline=True, zorder=2)
    ax.set_title( rf'{ustress.time[t].data:.13}')
    add_rectangles_red(ax, lon_reg, lat_reg, 'R')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    t = 168
    hm2 = pyic.shade(vstress.lon, vstress.lat, (np.sqrt(ustress**2 + vstress**2)).isel(time=t), ax=ax, cax=cax,     clim=clim, transform=ccrs_proj, rasterized=False, cmap=cmap,   cbtitle=r'$[N/m^2] $'+'\n .' ) 
    crop_quiver(ax, lon0, lat0, ustress.isel(time=t),     vstress.isel(time=t), ustress.lon, ustress.lat, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds.msl.isel(time=21).isel(step=21),  levels=levels, linewidths=1, colors='k')

    ax.clabel(CS, inline=True, zorder=2)
    ax.set_title( rf'{ustress.time[t].data:.13}')
    add_rectangles_red(ax, lon_reg, lat_reg, 'R')

    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)


    ############################################
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 8e-8
    cmap = 'twilight_shifted'
    ax.set_title(rf'week mean ')

    hm1 = pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, cmap=cmap, 
                    transform=ccrs_proj, rasterized=False)
    # crop_quiver(ax, lon_reg, lat_reg, ustress_week.var180,     vstress_week.var180, lonw, latw, ar=4)
    crop_quiver(ax, lon_reg, lat_reg, ustress_week,     vstress_week, lonw, latw, ar=4)
    CS = ax.contour(ds.longitude, ds.latitude, ds_mean, levels=levels,  linewidths=0.5, colors='k')
    ax.clabel(CS, inline=True, zorder=2)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(rf'zoom')

    hm2 = pyic.shade(data.lon, data.lat, data, ax=ax, cax=cax, clim=clim, cmap=cmap, contfs=True, nclev=21, transform=ccrs_proj, rasterized=False,   cbtitle=r'$[m^2/s^3]$'+'\n .' )
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)


    lon_R   = np.array([-70, -54])
    lat_R   = np.array([23.5, 36])
    lon_RF1 = lon_reg_3
    lat_RF1 = lat_reg_3

    crop_quiver(ax, lon_RF1, lat_RF1, ustress_week,  vstress_week, lonw, latw, ar=2)

    for i, ax in enumerate(hca):
        if i <= 1:
            pyic.plot_settings(ax, xlim=lon0, ylim=lat0) 
        elif i == 2:
            add_rectangles(ax, lon_reg_3, lat_reg_3, '', color='red')
            t = ax.text(lon_R[1]-0.9, lat_R[1]-1, 'R', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))

            add_front_lines_box(ax)
            pyic.plot_settings(ax, xlim=lon_reg_mean, ylim=lat_reg_mean) 
        elif i == 3:
            plot_single_front_margins_fn(ax, fn)
            t = ax.text(lon_RF1[1]-0.3, lat_RF1[1]-0.15, f'RF{fn}', color='red', fontsize=15)
            t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
            pyic.plot_settings(ax, xlim=lon_reg_3, ylim=lat_reg_3)

        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) # type: ignore
    
    fname = f'EBF_{twindow}_new{fn}'
    if savefig == True: plt.savefig(f'{fig_path}{fname}.png', dpi=250, format='png', bbox_inches='tight')
