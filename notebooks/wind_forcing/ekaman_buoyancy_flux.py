# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools

from netCDF4 import Dataset, num2date
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import funcs as fu

from scipy import stats    #Used for 2D binned statistics
from importlib import reload
# %%
#Reload modules:
reload(eva)
reload(tools)
# %%
import smt_modules.init_slurm_cluster as scluster 

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:10:00', wait=False)
cluster
client
# %%
#---------------------------------Genreral---------------------------------#
fig_path      = '/home/m/m300878/submesoscaletelescope/notebooks/images/forcing/'
savefig       = False
twindow       = 'instant'
path_root_dat = '/work/mh0033/m300878/parameterization/time_averages/one_week_march/'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
time_averaged = ''

tstart  = np.datetime64('2010-03-15T21:00:00')
tend    = np.datetime64('2010-03-22T21:00:00')
lon_reg_0 = np.array([-90, -10])
lat_reg_0 = np.array([15, 60])
# lon_reg_2 = np.array([-75, -46])
# lat_reg_2 = np.array([23.5, 40])
lon_reg_2 = np.array([-70, -54])
lat_reg_2 = np.array([23.5, 36])

# %%
path_data = f'/work/mh0033/m300878/observation/pressure/'
search_str = f'E5sf00_1H_2010-03-*.nc' 
flist      = np.array(glob.glob(path_data+search_str))
flist.sort()
ds = xr.open_mfdataset(flist, engine='cfgrib', combine='nested', concat_dim='time' )
ds = ds.assign_coords(longitude=(((ds.longitude + 180) % 360) - 180))
ds = ds.sortby(ds.longitude)
ds = ds.sel(longitude=slice(lon_reg_0[0],lon_reg_0[1]))
ds = ds.sel(latitude=slice(lat_reg_0[1],lat_reg_0[0]))
ds['msl'] = ds['msl'] / 100

ds_mean = ds.msl.isel(time=slice(14,21)).mean(dim='step').mean(dim='time')
# %% load windstress
#https://codes.ecmwf.int/grib/param-db/180
path_data = '/work/mh0033/from_Mistral/mh0033/u241317/smt/forcing/ustress.nc'
ustress   = xr.open_dataset(path_data)
ustress   = ustress.loc[dict(time=slice(tstart, tend))]
ustress   = ustress.assign_coords(lon=(((ustress.lon + 180) % 360) - 180))
ustress   = ustress.sel(lon=slice(lon_reg_0[0],lon_reg_0[1]))
ustress   = ustress.sel(lat=slice(lat_reg_0[1],lat_reg_0[0]))
ustress   = ustress / 3600 #convertt accumulation of single hour to per second
lonw      = ustress.lon #.data
latw      = ustress.lat #.data

path_data  = '/work/mh0033/from_Mistral/mh0033/u241317/smt/forcing/vstress.nc'
vstress    = xr.open_dataset(path_data)
vstress    = vstress.loc[dict(time=slice(tstart, tend))]
vstress    = vstress.assign_coords(lon=(((vstress.lon + 180) % 360) - 180))
vstress    = vstress.sel(lon=slice(lon_reg_0[0],lon_reg_0[1]))
vstress    = vstress.sel(lat=slice(lat_reg_0[1],lat_reg_0[0]))
vstress    = vstress / 3600
vstress    = vstress.rename({'var181': 'var180'}) # only necessary for 


# %%
path_data  = '/work/mh0033/m211054/projects/ERA5/ERA5_ustress_1h_20100101_20101231.grb'
ds_ustress = xr.open_dataset(path_data, engine='cfgrib')
ustress    = ds_ustress.loc[dict(time=slice('2010-03-13T01', '2010-03-23T01'))]

U = []
t = []
for i in range(ustress.time.size):
    for j in range(ustress.step.size):
        U.append(ustress.ewss.isel(time=i, step=j).values)
        t.append(ustress.isel(time=i, step=j).valid_time.values)
U = np.array(U)
U = xr.DataArray(U, dims=['time', 'latitude', 'longitude'], coords={'time':t,  'latitude':ustress.latitude, 'longitude':ustress.longitude})

ustress   = U.loc[dict(time=slice(tstart, tend))]
ustress   = ustress.assign_coords(longitude=(((ustress.longitude + 180) % 360) - 180))
ustress   = ustress.sel(longitude=slice(lon_reg_0[0],lon_reg_0[1]))
ustress   = ustress.sel(latitude=slice(lat_reg_0[1],lat_reg_0[0]))
ustress   = ustress / 3600 #convert accumulation of single hour to per second

ustress = ustress.rename({'latitude': 'lat'})
ustress = ustress.rename({'longitude': 'lon'})
lonw      = ustress.lon
latw      = ustress.lat
# # name
# ustress.name = 'var180'


# %%
path_data  = '/work/mh0033/m211054/projects/ERA5/ERA5_vstress_1h_20100101_20101231.grb'
ds_vstress = xr.open_dataset(path_data, engine='cfgrib')
vstress    = ds_vstress.loc[dict(time=slice('2010-03-13T01', '2010-03-23T01'))]

V = []
t = []
for i in range(vstress.time.size):
    for j in range(vstress.step.size):
        V.append(vstress.nsss.isel(time=i, step=j).values)
        t.append(vstress.isel(time=i, step=j).valid_time.values)
V = np.array(V)
V = xr.DataArray(V, dims=['time', 'latitude', 'longitude'], coords={'time':t,  'latitude':vstress.latitude, 'longitude':vstress.longitude})

vstress   = V.loc[dict(time=slice(tstart, tend))]
vstress   = vstress.assign_coords(longitude=(((vstress.longitude + 180) % 360) - 180))
vstress   = vstress.sel(longitude=slice(lon_reg_0[0],lon_reg_0[1]))
vstress   = vstress.sel(latitude=slice(lat_reg_0[1],lat_reg_0[0]))
vstress   = vstress / 3600
# vstress   = vstress.rename({'nsss': 'var180'}) # only necessary for 
vstress = vstress.rename({'latitude': 'lat'})
vstress = vstress.rename({'longitude': 'lon'})
# vstress.name = 'var180'
# %%
ustress_week = ustress.mean(dim='time')
ustress_week.attrs["time averaged"] = "2010-03-15T21 - 2010-03-22T21"
ustress_week.attrs["time steps"] = "169"
vstress_week = vstress.mean(dim='time')
vstress_week.attrs["time averaged"] = "2010-03-15T21 - 2010-03-22T21"
vstress_week.attrs["time steps"] = "169"

#%%
def calc_EBF(time_sel, twindow):
    # % load bouyancy week average
    # time_sel = 84
    if twindow == 'instant':
       ds_b     = eva.load_smt_b()
       ds_b     = ds_b.isel(time=time_sel).isel(depthi=1)
       dbdx     = pyic.interp_to_rectgrid_xr(ds_b.dbdx, fpath_ckdtree, lon_reg=lon_reg_0,    lat_reg=lat_reg_0)
       dbdy     = pyic.interp_to_rectgrid_xr(ds_b.dbdy, fpath_ckdtree, lon_reg=lon_reg_0,    lat_reg=lat_reg_0)
    else:
       path_data = f'{path_root_dat}bx{time_averaged}.nc'
       dbdx_mean = xr.open_dataset(path_data, chunks=dict(depthi=1))
       path_data = f'{path_root_dat}by{time_averaged}.nc'
       dbdy_mean = xr.open_dataset(path_data, chunks=dict(depthi=1))
       dbdx = pyic.interp_to_rectgrid_xr(dbdx_mean.dbdx.isel(depthi=1),fpath_ckdtree,     lon_reg=lon_reg_2, lat_reg=lat_reg_2)
       dbdy = pyic.interp_to_rectgrid_xr(dbdy_mean.dbdy.isel(depthi=1), fpath_ckdtree,     lon_reg=lon_reg_2, lat_reg=lat_reg_2)
    
    # %
    reload(fu)
    if twindow == 'instant':
        tau_x_scaled, tau_y_scaled = fu.compute_tau_xy_from_windstress(ustress, vstress)
    else:
        tau_x_scaled, tau_y_scaled = fu.compute_tau_xy_from_windstress(ustress_week, vstress_week)

    if twindow == 'instant':
        tau_x_scaled = tau_x_scaled.sel(time=dbdx.time)
        tau_y_scaled = tau_y_scaled.sel(time=dbdx.time)
    #% Interpolate tau to the same grid as dbdx and dbdy

    if False:
        plt.figure(figsize=(10, 10))
        tau_y_scaled.plot()
        plt.plot([lon_reg_2[0], lon_reg_2[0]], [lat_reg_2[0], lat_reg_2[1]], color='purple')
        plt.plot([lon_reg_2[1], lon_reg_2[1]], [lat_reg_2[0], lat_reg_2[1]], color='purple')
        plt.plot([lon_reg_2[0], lon_reg_2[1]], [lat_reg_2[0], lat_reg_2[0]], color='purple')
        plt.plot([lon_reg_2[0], lon_reg_2[1]], [lat_reg_2[1], lat_reg_2[1]], color='purple')
    # %
    tau_x_scaled_interp = tau_x_scaled.interp_like(dbdy)
    tau_y_scaled_interp = tau_y_scaled.interp_like(dbdx)
    tau_y_scaled_interp.plot()

    EBF_x        = tau_y_scaled_interp * dbdx
    EBF_y        = - tau_x_scaled_interp * dbdy
    EBF          = EBF_x + EBF_y

    return EBF
# %%
EBF_inst0 = calc_EBF(time_sel=0, twindow='instant')
EBF_inst2 = calc_EBF(time_sel=84, twindow='instant')


EBF_mean  = calc_EBF(time_sel=None, twindow='mean')

# %%
# if twindow == 'instant':
#     if time_sel == 84:
#         EBF_inst2 = EBF
#     elif time_sel == 0:
#         EBF_inst0 = EBF
# else:
#     EBF_mean = EBF


# %% ----------------------- Visualize ----------------------- #

reload(fu)
fu.plot_EBF_comp(EBF_x, EBF_y, savefig, fig_path, twindow)
# %%
if False:
    reload(fu)
    fig, ax = plt.subplots(figsize=(10, 10))
    EBF.plot(vmin=-2e-8, vmax=2e-8, cmap='RdBu_r')
    fu.add_front_lines_box(ax, color='purple')
    ax.set_title('Ekman Buoyancy flux')
    if savefig==True: plt.savefig(f'{fig_path}EBF_{twindow}.png', dpi=300)

# %%
reload(fu)
fu.plot_EBF(EBF_mean, savefig, fig_path, twindow)
# %%
reload(fu)
fu.plot_forcing_state_EBF_4(lon_reg_2, lat_reg_2, lonw, latw, ustress, vstress, ustress_week, vstress_week, EBF_inst0, EBF_inst1, EBF_inst2, EBF_mean, ds, ds_mean, fig_path,  savefig)
# %%
reload(fu)
fu.plot_forcing_state_EBF(lon_reg_2, lat_reg_2, lonw, latw, ustress, vstress, ustress_week, vstress_week,  EBF_inst1, EBF_mean, ds, ds_mean, fig_path,  savefig)


# %%
reload(fu)
fu.plot_EBF_all(lon_reg_2, lat_reg_2, lonw, latw, EBF_mean, EBF_inst0, EBF_inst2,  ustress, vstress, ustress_week, vstress_week, ds, ds_mean, savefig, fig_path, twindow)

# %%
fn     = 2
hedge  = 1.25
asp3   = 1.279999999
points = eva.get_points_of_inclined_fronts()

P = points[(2*fn-2):2*fn,:]
P_lon_mean = np.mean(P[:,0])
P_lat_mean = np.mean(P[:,1])
lon_reg_3 = np.array([P_lon_mean-hedge, P_lon_mean+hedge])
lat_reg_3 = np.array([P_lat_mean-hedge/asp3, P_lat_mean+hedge/asp3])

reload(fu)
fu.plot_EBF_all_f(lon_reg_2, lat_reg_2, lonw, latw, EBF_mean, EBF_inst0, EBF_inst2,  ustress, vstress, ustress_week, vstress_week, ds, ds_mean, savefig, fig_path, twindow, fn, lon_reg_3, lat_reg_3)

 # %%
