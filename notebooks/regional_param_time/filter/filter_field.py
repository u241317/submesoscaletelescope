# %%
import sys
import glob, os
import pyicon as pyic
sys.path.insert(0, '../../')
sys.path.insert(0, '../../../')
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import xarray as xr    
import numpy as np
from importlib import reload
import smt_modules.init_slurm_cluster as scluster 

# %% 
client, cluster = scluster.init_dask_slurm_cluster(walltime='00:10:00', wait=True)
cluster
client

# %%

domain        = 'our_domain'
savefig       = False
var           = 'b_c'
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
if domain=='uchida_domain':
    lat_reg          = 30,  40
    lon_reg          = -78, -68
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/uchida_domain/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/uchida_domain/'
elif domain=='our_domain':
    lon_reg          = np.array([-70, -54])
    lat_reg          = np.array([23.5, 36])
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/our_domain/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/our_domain/'

path_2_data      = f'{path_2_data_root}gridded/'
path_2_data_filt = f'{path_2_data_root}filtered/'
path_results     = f'{path_2_data_root}results/'

print(f'var: {var}')
print(f'path_2_data: {path_2_data}')
print(f'path_2_data_filt: {path_2_data_filt}')
print(f'path_results: {path_results}')
#%%
reload(eva)
mask_land_100 = eva.load_smt_land_mask(depthc=32)
mask_land_100 = pyic.interp_to_rectgrid_xr(mask_land_100, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land_100 = mask_land_100.drop('depthc')
#%%

# ds = xr.open_dataarray(path_2_data + f'{var}.nc', chunks={'time': 1, 'depthi': 1})
ds = xr.open_dataarray(path_2_data + f'{var}.nc', chunks={'time': 1, 'depthc': 1})

ds = ds.compute()

filter_30km, mask_land_100_new = eva.create_gcm_filter(ds, 'irregular', mask_land_100)

ds_sel = ds * mask_land_100_new
# %%
client.close()
cluster.close()
# %%
for i in np.arange((ds_sel.time.size)):
    print(i)
    ds_filtered_to_30km = filter_30km.apply(ds_sel.isel(time=i), dims=['lat', 'lon'])
    ds_filtered_to_30km = ds_filtered_to_30km.rename(f'{var}_filt')
    ds_filtered_to_30km.to_netcdf(path_2_data_filt + f'{var}_filt/{var}_filtered_to_30km_{ds_sel.isel(time=i).time.values}.nc')

print('done')