#! /bin/bash
#SBATCH --job-name=pysmt
#SBATCH --time=01:30:00
#SBATCH --output=log.o%j
#SBATCH --error=log.e%j
#SBATCH --partition=compute
#SBATCH --account=mh0033
#SBATCH --mem=250G 

module list
source /work/mh0033/m300878/pyicon/tools/conda_act_mistral_pyicon_env.sh
which python

startdate=`date +%Y-%m-%d\ %H:%M:%S`

python -u grid_data.py --slurm

enddate=`date +%Y-%m-%d\ %H:%M:%S`
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

