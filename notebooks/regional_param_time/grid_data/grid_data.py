# %%
import sys
import glob, os
import pyicon as pyic
sys.path.insert(0, '../../')
sys.path.insert(0, '../../../')
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '/home/m/m300878/submesoscaletelescope/notebooks/spectral_analysis/')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# %% Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
# from validate_catalog import all_params
# params_dict, cat = all_params()
# params_dict.keys()
import gcm_filters

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:00:00', wait=True)
cluster
client

# %%
domain        = 'our_domain'
grid_data     = True
t_space       = 1
savefig       = False
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
if domain=='uchida_domain':
    lat_reg          = 30,  40
    lon_reg          = -78, -68
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/uchida_domain/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/uchida_domain/'
elif domain=='our_domain':
    lon_reg          = np.array([-70, -54])
    lat_reg          = np.array([23.5, 36])
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/our_domain/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/our_domain/'

path_2_data      = f'{path_2_data_root}gridded/'
path_2_data_filt = f'{path_2_data_root}filtered/'
path_results     = f'{path_2_data_root}results/'

print(f'path_2_data: {path_2_data}')
print(f'path_2_data_filt: {path_2_data_filt}')
print(f'path_results: {path_results}')

print('load and grid b and lateral gradient')
if False: #old b
    ds   = eva.load_smt_b()
    ds   = ds.isel(time=slice(0, len(ds.time), t_space))
    b    = pyic.interp_to_rectgrid_xr(ds.b, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    b.to_netcdf(path_2_data + 'b.nc')
    dbdx = pyic.interp_to_rectgrid_xr(ds.dbdx, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    dbdx.to_netcdf(path_2_data + 'dbdx.nc')
    dbdy = pyic.interp_to_rectgrid_xr(ds.dbdy, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    dbdy.to_netcdf(path_2_data + 'dbdy.nc')
elif False: #new buoancy interface
    ds = eva.load_smt_b_depthi_interp('b')
    b    = pyic.interp_to_rectgrid_xr(ds.b, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    b.to_netcdf(path_2_data + 'b.nc')
    ds = eva.load_smt_b_depthi_interp('dbdx')
    dbdx = pyic.interp_to_rectgrid_xr(ds.dbdx, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    dbdx.to_netcdf(path_2_data + 'dbdx.nc')
    ds = eva.load_smt_b_depthi_interp('dbdy')
    dbdy = pyic.interp_to_rectgrid_xr(ds.dbdy, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    dbdy.to_netcdf(path_2_data + 'dbdy.nc')
elif True:
    ds = eva.load_smt_b_depthc_no_interp('b')
    b  = pyic.interp_to_rectgrid_xr(ds.b, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    # b.to_netcdf(path_2_data + 'b_c.nc')
    # ds = eva.load_smt_b_depthc_no_interp('dbdx')
    # dbdx = pyic.interp_to_rectgrid_xr(ds.dbdx, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    # dbdx.to_netcdf(path_2_data + 'dbdx_c.nc')
    # ds = eva.load_smt_b_depthc_no_interp('dbdy')
    # dbdy = pyic.interp_to_rectgrid_xr(ds.dbdy, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    # dbdy.to_netcdf(path_2_data + 'dbdy_c.nc')



if False:
    print('load and grid N2')
    ds = eva.load_smt_N2()
    ds = ds.drop('clon').drop('clat')
    ds = ds.isel(time=slice(0, len(ds.time), t_space))
    N2 = pyic.interp_to_rectgrid_xr(ds.N2, fpath_ckdtree=fpath_ckdtree,     lon_reg=lon_reg, lat_reg=lat_reg)
    N2.to_netcdf(path_2_data + 'N2.nc')


    # %% w #TODO: check broken time step or exclude last time step
    print('load and grid w')
    ds   = eva.load_smt_w()
    ds_w = ds.w.sel(time=b.time.data, method='nearest')
    ds_w = ds_w.drop('clon').drop('clat')
    w    = pyic.interp_to_rectgrid_xr(ds_w, fpath_ckdtree=fpath_ckdtree,    lon_reg=lon_reg, lat_reg=lat_reg)
    w.to_netcdf(path_2_data + 'w.nc')

if True:
    print('load and grid u and v')
    ds    = eva.load_smt_v()
    ds_u  = ds.u.sel(time=b.time.data, method='nearest')
    u     = pyic.interp_to_rectgrid_xr(ds_u, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    u.to_netcdf(path_2_data + 'u.nc')
    ds_v  = ds.v.sel(time=b.time.data, method='nearest')
    v     = pyic.interp_to_rectgrid_xr(ds_v, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    v.to_netcdf(path_2_data + 'v.nc')



client.close()
cluster.close()

