# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '/home/m/m300878/submesoscaletelescope/notebooks/spectral_analysis/')
sys.path.insert(0, '../spectral_analysis/')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# % Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
# from validate_catalog import all_params
# params_dict, cat = all_params()
# params_dict.keys()
import gcm_filters
print('Warning: use node with more than 256Gb RAM')
# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:25:00', wait=False)
cluster
client

# %%
domain        = 'our_domain'
t_space       = 1
savefig       = False
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
if domain=='uchida_domain':
    lat_reg          = 30,  40
    lon_reg          = -78, -68
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/uchida_domain/vb/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/uchida_domain/'
elif domain=='our_domain':
    lon_reg          = np.array([-70, -54])
    lat_reg          = np.array([23.5, 36])
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/our_domain/vb/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/our_domain/'

path_2_data      = f'{path_2_data_root}gridded/'
path_2_data_filt = f'{path_2_data_root}filtered/'
path_results     = f'{path_2_data_root}results/vb/'

############################### Compute/Load ##############################
# %%
reload(eva)
mask_land_100 = eva.load_smt_land_mask(depthc=32)
mask_land_100 = pyic.interp_to_rectgrid_xr(mask_land_100, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land_100 = mask_land_100.drop('depthc')

tstep=1

# %%#load b on depthc
b_c      = xr.open_dataarray(path_2_data + 'b_c.nc', chunks={'time': 1, 'depthc': 1}).isel(time=slice(0, None, tstep))
b_c      = b_c.compute()
b_c      = b_c * mask_land_100
b_c      = b_c[:-1]
#%%
dbdx_c   = xr.open_dataarray(path_2_data + 'dbdx_c.nc', chunks={'time': 1, 'depthc': 1}).isel(time=slice(0, None, tstep))
dbdy_c   = xr.open_dataarray(path_2_data + 'dbdy_c.nc', chunks={'time': 1, 'depthc': 1}).isel(time=slice(0, None, tstep))
dbdx_c   = dbdx_c.compute()
dbdy_c   = dbdy_c.compute()
dbdx_c   = dbdx_c * mask_land_100
dbdx_c   = dbdx_c[:-1]
dbdy_c   = dbdy_c * mask_land_100
dbdy_c   = dbdy_c[:-1]
M2_c     = np.sqrt(dbdx_c**2 + dbdy_c**2)

#%%
var      = 'b_c'
path     = f'{path_2_data_filt}{var}_filt/'
b_filt_c = xr.open_mfdataset(path + f'{var}_filt*', combine='nested'  , concat_dim='time', chunks={'time': 1, 'depthc': 1}).isel(time=slice(0, None, tstep))
b_filt_c = b_filt_c.b_c_filt.compute()
b_filt_c = b_filt_c * mask_land_100
b_filt_c = b_filt_c[:-1]

# %% N2
N2           = xr.open_dataarray(path_2_data + 'N2.nc', chunks={'time': 1, 'depthi': 1}).isel(time=slice(0, None, tstep))
N2           = N2.compute()
N2[:,0,:,:]  = np.nan
N2[:,-1,:,:] = np.nan
N2           = N2 * mask_land_100
N2           = N2.where(~(N2 == 0), np.nan) # remove zeros
N2           = N2[:-1]


# %% 
u = xr.open_dataarray(path_2_data + 'u.nc', chunks={'time': 1, 'depthc': 1}).isel(time=slice(0, None, tstep))
u = u.compute()
u = u[:-1]
v = xr.open_dataarray(path_2_data + 'v.nc', chunks={'time': 1, 'depthc': 1}).isel(time=slice(0, None, tstep))
v = v.compute()
v = v[:-1]

var    = 'u'
path   = f'{path_2_data_filt}{var}_filt/'
u_filt = xr.open_mfdataset(path + f'{var}_filt*', combine='nested'  , concat_dim='time', chunks={'time': 1, 'depthc': 1}).isel(time=slice(0, None, tstep))
u_filt = u_filt.u_filt.compute()
u_filt = u_filt * mask_land_100
u_filt = u_filt[:-1]

var    = 'v'
path   = f'{path_2_data_filt}{var}_filt/'
v_filt = xr.open_mfdataset(path + f'{var}_filt*', combine='nested'  , concat_dim='time', chunks={'time': 1, 'depthc': 1}).isel(time=slice(0, None, tstep))
v_filt = v_filt.v_filt.compute()
v_filt = v_filt * mask_land_100
v_filt = v_filt[:-1]


# %%
depth_level = 'depthc'
filter_30km, mask_land_100_new = eva.create_gcm_filter(b_c, 'irregular', mask_land_100)

MLD_c      = xr.open_dataarray(path_2_data + f'MLD_{depth_level}.nc', chunks={'time': 1}).isel(time=slice(0, None, tstep))
Mask_MLD_c = xr.open_dataarray(path_2_data + f'Mask_MLD_{depth_level}.nc', chunks={'time': 1}).isel(time=slice(0, None, tstep))
MLD_c      = MLD_c.compute()
Mask_MLD_c = Mask_MLD_c.compute()
MLD_c      = MLD_c * mask_land_100_new
MLD_c      = MLD_c[:-1]
Mask_MLD_c = Mask_MLD_c[:-1]
Mask_MLD_c = Mask_MLD_c.transpose('time', 'depthc', 'lat', 'lon')

#%% average true false mask
Mask_MLD_c= Mask_MLD_c.mean(dim='time', skipna=True)
#%%
MLD_c = MLD_c.mean(dim='time', skipna=True)
#%%
MLD      = xr.open_dataarray(path_2_data + 'MLD.nc', chunks={'time': 1})
MLD_int  = xr.open_dataarray(path_2_data + 'MLD_int.nc', chunks={'time': 1})
Mask_MLD = xr.open_dataarray(path_2_data + 'Mask_MLD.nc', chunks={'time': 1})
MLD      = MLD.compute()
Mask_MLD = Mask_MLD.compute()
MLD      = MLD * mask_land_100_new
MLD      = MLD[:-1]
Mask_MLD = Mask_MLD[:-1]

# %%
MLD = MLD.mean(dim='time', skipna=True)
Mask_MLD = Mask_MLD.mean(dim='time', skipna=True)

#%%
# path_to_interp_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/'

# mask_mld                 = xr.open_dataarray(f'{path_to_interp_data}/mask_mld.nc')
# mldx                     = xr.open_dataarray(f'{path_to_interp_data}/mldx.nc')


#%%
if False:
    b_diff = b - b_filt 
    fu.plot_b_filter(b.lon,b.lat, b.isel(depthi=5, time=1), b_filt.isel(depthi=5, time=1), b_diff.isel(depthi=5, time=1), fig_path, savefig)
    w_diff = w - w_filt
    fu.plot_w_filter(w.lon,w.lat, w.isel(depthi=5, time=1), w_filt.isel(depthi=5, time=1), w_diff.isel(depthi=5, time=1), fig_path, savefig)
    # %
    u_diff = u - u_filt
    fu.plot_u_filter(u.lon,u.lat, u.isel(depthc=5, time=1), u_filt.isel(depthc=5, time=1), u_diff.isel(depthc=5, time=1), fig_path, savefig)
    # %
    reload(fu)
    v_diff = v - v_filt
    fu.plot_u_filter(v.lon,v.lat, v.isel(depthc=5, time=1), v_filt.isel(depthc=5, time=1), v_diff.isel(depthc=5, time=1), fig_path, savefig)


# %%  #################################### Coarsen Data ####################################
f               = eva.calc_coriolis_parameter(b_c.lat)
ncoars          = 4
mask_land_coars = mask_land_100.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)


# %% ############################### VB Prime ########################################
dzw_xr = xr.DataArray(dzw, dims='depthc')
print('overwrite MLD')
MLD_c = (Mask_MLD_c * dzw_xr).sum(dim='depthc', skipna=True) * mask_land_100
MLD_c_coars     = MLD_c.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)

#%%
if True:
    vb_dat     = ((v - v_filt) * (b_c - b_filt_c)) 
    if True:
        vb_dat.to_netcdf(f'{path_results}vb_dat.nc')
        vb_prime_space_mean = vb_dat.mean(dim='time')
        vb_prime_space_mean.to_netcdf(f'{path_2_data}vb_prime_space_mean.nc')
        vb_prime_space_mean_depthi = vb_prime_space_mean.interp(depthc=depthi, method='linear', kwargs={'fill_value': 'extrapolate'})
        vb_prime_space_mean_depthi = vb_prime_space_mean_depthi.rename({'depthc':'depthi'})
        vb_prime_space_mean_depthi.to_netcdf(f'{path_2_data}vb_prime_space_mean_depthi.nc')
    vb_dat     =  vb_dat * dzw_xr * Mask_MLD_c
    vb_dat_sum = (vb_dat).sum(dim='depthc', skipna=True)
    vb_prime   = vb_dat_sum / MLD_c 
    vb_prime   = vb_prime * mask_land_100
    vb_prime.to_netcdf(f'{path_results}vb_prime.nc')
else:
    vb_prime = xr.open_dataarray(f'{path_results}vb_prime.nc').compute()
# %%
if True:
    ub_dat     = ((u - u_filt) * (b_c - b_filt_c))
    if True:
        ub_dat.to_netcdf(f'{path_results}ub_dat.nc')
        ub_prime_space_mean = ub_dat.mean(dim='time')
        ub_prime_space_mean.to_netcdf(f'{path_2_data}ub_prime_space_mean.nc')
        ub_prime_space_mean_depthi = ub_prime_space_mean.interp(depthc=depthi, method='linear', kwargs={'fill_value': 'extrapolate'})
        ub_prime_space_mean_depthi = ub_prime_space_mean_depthi.rename({'depthc':'depthi'})
        ub_prime_space_mean_depthi.to_netcdf(f'{path_2_data}ub_prime_space_mean_depthi.nc')
    ub_dat     = ub_dat * dzw_xr * Mask_MLD_c
    ub_dat_sum = (ub_dat).sum(dim='depthc', skipna=True)
    ub_prime   = ub_dat_sum / MLD_c 
    ub_prime   = ub_prime * mask_land_100
    ub_prime.to_netcdf(f'{path_results}ub_prime.nc')
else:
    ub_prime = xr.open_dataarray(f'{path_results}ub_prime.nc').compute()
# %%
if True:
    vb_prime.isel(time=0).plot(vmin=-2e-5, vmax=2e-5, cmap='RdBu_r')
    if savefig==True: plt.savefig(fig_path + 'vb_prime.png')
    # %%
    ub_prime.isel(time=0).plot(vmin=-2e-5, vmax=2e-5, cmap='RdBu_r')
    if savefig==True: plt.savefig(fig_path + 'ub_prime.png')
# %%
m2x_mean      = (dbdx_c * Mask_MLD_c * dzw_xr).sum(dim='depthc', skipna=True) / MLD_c
m2y_mean      = (dbdy_c * Mask_MLD_c * dzw_xr).sum(dim='depthc', skipna=True) / MLD_c
m2_mean       = (np.sqrt(np.power(m2x_mean,2) + np.power(m2y_mean,2)))
m2_mean_coars = m2_mean.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)

#%%
if True:
    # m2x_mean.median(['lat','lon'],skipna=True).plot()
    # m2y_mean.median(['lat','lon'],skipna=True).plot()
    (m2_mean).median(['lat','lon'],skipna=True).plot()

#%%
vb_prime_proj = (m2x_mean * ub_prime + m2y_mean* vb_prime) / (np.sqrt(np.power(m2x_mean,2) + np.power(m2y_mean,2)))
# %%
if False:
    vb_prime_proj_3d = (dbdx_c * ub_dat + dbdy_c* vb_dat) / (np.sqrt(np.power(dbdx_c,2) + np.power(dbdy_c,2)))
    vb_prime_proj_3d = vb_prime_proj_3d * mask_land_100 * Mask_MLD_c
    vb_prime_proj_3d_mean = (vb_prime_proj_3d * dzw_xr).sum(dim='depthc', skipna=True) / MLD_c
#%%
# vb_prime_proj = vb_prime_proj_3d_mean.where(vb_prime_proj_3d_mean <0, np.nan)
#%%
    (vb_prime_proj_3d_mean.where(vb_prime_proj_3d_mean >0, np.nan).median(['lat','lon'],skipna=True)).plot()
    (vb_prime_proj_3d_mean.where(vb_prime_proj_3d_mean <0, np.nan).median(['lat','lon'],skipna=True)*-1).plot()
    np.abs(vb_prime_proj_3d_mean).median(['lat','lon'],skipna=True).plot()
#%%
if True:
    vb_prime_proj.isel(time=0).plot(vmin=-2e-5, vmax=2e-5, cmap='RdBu_r')
    if savefig==True: plt.savefig(fig_path + 'vb_prime_proj_3d.png')
    #%%
    (vb_prime_proj.where(vb_prime_proj >0, np.nan).median(['lat','lon'],skipna=True)).plot()
    (vb_prime_proj.where(vb_prime_proj <0, np.nan).median(['lat','lon'],skipna=True)*-1).plot()
    np.abs(vb_prime_proj).median(['lat','lon'],skipna=True).plot()

#%%
reload(fu)
fu.plot_vb_proj_prime(b_c.lon, b_c.lat, vb_prime_proj.isel(time=0), fig_path, savefig)
# %%
print('take negative values from projection - or the relevant part')
vb_prime_proj_s     = vb_prime_proj.where( vb_prime_proj<0,np.nan)
vb_prime_proj_coars = vb_prime_proj_s.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
vb_prime_proj_coars.median(['lat','lon'],skipna=True).plot()
# vb_prime_proj_coars_n = -np.abs(vb_prime_proj_coars)
vb_prime_proj_coars_n = vb_prime_proj_coars


#%%
if False:
    # M2_coars        = (M2_c* Mask_MLD_c).coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
    # M2_H_mean_coars = ((M2_coars ) * dzw_xr).sum(dim='depthc', skipna=True) #/ MLD_coars  #not divided by MLD in Uchida TODO:WHY?
    # f_coars         = eva.calc_coriolis_parameter(M2_coars.lat)
    # MLI_coars       = (M2_H_mean_coars**2/f_coars)

    M2_c_coars        = m2_mean_coars
    f_coars           = eva.calc_coriolis_parameter(M2_c_coars.lat)
    MLI_coars         = (M2_c_coars/f_coars**2)**2 * MLD_c_coars**2 * f_coars**3
    MLI_coars_mean    = MLI_coars  
else:
    M2_coars        = (M2_c* Mask_MLD_c).coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
    M2_coars        = (M2_coars * dzw_xr).sum(dim='depthc', skipna=True) / MLD_c_coars
    f_coars         = eva.calc_coriolis_parameter(M2_coars.lat)
    MLI_coars       = (M2_coars/f_coars**2)**2 * MLD_c_coars**2 * f_coars**3
    MLI_coars_mean  = MLI_coars  
    M2_c_coars = M2_coars

MLI_coars.isel(time=0).plot(vmin =0, vmax=4e-6, cmap='Reds')
if savefig==True: plt.savefig(fig_path + 'MLI_coars_c.png')
# %% 
N2_coars   = (N2*Mask_MLD).coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
if False: #both methods are the same
    N2_c_coars = N2_coars.interp(depthi=depthc, method='linear', kwargs={'fill_value': 'extrapolate'})
    N2_c_coars = N2_c_coars.rename({'depthi':'depthc'})
    N2_c_coars = (N2_c_coars* dzw_xr).sum(dim='depthc', skipna=True) / MLD_c_coars
else:
    depthi_     = xr.DataArray(depthi, dims='depthi')
    dz_diff     = depthi_.diff(dim='depthi')
    dz_diff_ext = np.append(dz_diff, np.nan)
    dz_diff_ext = xr.DataArray(dz_diff_ext, dims='depthi')
    N2_c_coars = (N2_coars* dz_diff_ext).sum(dim='depthi', skipna=True) / MLD_c_coars

#%%
if False:
    (N2_c_coars).mean(dim='depthi').isel(time=0).plot(vmin=-1e-5, vmax=1e-5, cmap='Spectral')
    #%%
    N2_c_coars.isel(time=10, lon=50).plot(vmin=0, vmax=1e-5)
    plt.ylim(500,0)

# %%
alpha_coars = (M2_c_coars )/ f_coars**2 
Ri_coars    = N2_c_coars * f_coars**2/ (M2_c_coars )**2
# MLI_vb      = ((-2 * Ri_coars * alpha_coars * MLI_coars) * mask_land_coars * dzw_xr).sum(dim='depthc', skipna=True) / MLD_c_coars #only 3d
MLI_vb      = ((-2 * Ri_coars * alpha_coars * MLI_coars) * mask_land_coars)

if False:
    MLI_vb = -2* N2_c_coars * M2_c_coars / f_coars * MLD_c_coars**2
    MLI_vb = (MLI_vb * mask_mld_coars * mask_land_coars * dzw_xr).sum(dim='depthc', skipna=True) / MLD_c_coars

#%%
MLI_vb = MLI_vb.where(MLI_vb < 0, np.nan) * mask_land_coars #remove ladn effects
MLI_vb.isel(time=0).plot(vmin=-1e-3, vmax=1e-3, cmap='RdBu_r')
if savefig==True: plt.savefig(fig_path + 'MLI_vb_c.png')
#%%
if True:
    np.abs(MLI_vb).median(['lat','lon'],skipna=True).plot()
    #%%
    # np.abs((alpha_coars).mean(dim='depthc').median(['lat','lon'],skipna=True)).plot()
    np.abs((alpha_coars).median(['lat','lon'],skipna=True)).plot()
    #%%
    M2_c_coars.median(['lat','lon'],skipna=True).plot()
# %%
Ce = vb_prime_proj_coars_n / MLI_vb
if True:
    plt.figure()
    Ce.isel(time=0).plot(vmin=-1, vmax=1, cmap='RdBu_r')
    if savefig==True:  plt.savefig(fig_path + 'Ce.png')

alpha = Ce.median(['lat', 'lon'],skipna=True)
print(alpha.data)
aMLI = MLI_vb * alpha
# %%
reload(fu)
fu.plot_vb_prime_MLI(aMLI.isel(time=0), vb_prime_proj_coars_n.isel(time=0), fig_path, savefig)# %%
vpbp_coar_median = vb_prime_proj_coars_n.median(['lat','lon'],skipna=True)
aMLI_median      = aMLI.median(['lat','lon'],skipna=True)
MLI_alpha_median = (alpha.mean('time')*MLI_vb).median(['lat','lon'],skipna=True)

reload(fu)
fu.plot_tseries_diag_param(vpbp_coar_median, aMLI_median, MLI_alpha_median, alpha, fig_path, savefig, 'vb_prime_FK')

# %% MLI STONE
# MLI_vb_stone = ((-8/5 * np.sqrt(1+(N2_c_coars * f_coars**2/ M2_c_coars**2) ) * M2_c_coars / f_coars**2 * MLI_coars)  * mask_land_coars * dzw_xr).sum(dim='depthc', skipna=True) / MLD_c_coars
MLI_vb_stone = ((-8/5 * np.sqrt(1+(N2_c_coars * f_coars**2/ M2_c_coars**2) ) * M2_c_coars / f_coars**2 * MLI_coars)  * mask_land_coars )

MLI_vb_stone.isel(time=0).plot(vmin=-4e-4, vmax=0, cmap='Reds_r')

# %%
Cs = vb_prime_proj_coars_n / MLI_vb_stone
if True:
    plt.figure()
    Cs.isel(time=1).plot(vmin=-1, vmax=1, cmap='RdBu_r')   
    plt.savefig(fig_path + 'Cs.png')

alpha_Stone = Cs.median(['lat', 'lon'],skipna=True)
aMLI_Stone  = MLI_vb_stone * alpha_Stone
print(alpha_Stone.data)
# %%
aMLI_median_Stone = aMLI_Stone.median(['lat','lon'],skipna=True)
MLI_alpha_median_Stone = (alpha_Stone.mean('time')*MLI_vb_stone).median(['lat','lon'],skipna=True)
reload(fu)
fu.plot_tseries_diag_param(vpbp_coar_median, aMLI_median_Stone, MLI_alpha_median_Stone, alpha_Stone, fig_path, savefig, 'vb_prime_ST')

# %%
if True:
    vpbp_coar_median.to_netcdf(f'{path_results}vpbp_coar_median.nc')
    aMLI_median.to_netcdf(f'{path_results}aMLI_median.nc')
    MLI_alpha_median.to_netcdf(f'{path_results}MLI_alpha_median.nc')
    alpha.to_netcdf(f'{path_results}alpha.nc')
    aMLI_median_Stone.to_netcdf(f'{path_results}aMLI_median_Stone.nc')
    MLI_alpha_median_Stone.to_netcdf(f'{path_results}MLI_alpha_median_Stone.nc')
    alpha_Stone.to_netcdf(f'{path_results}alpha_Stone.nc')
    Ce.to_netcdf(f'{path_results}Ce.nc')
    Cs.to_netcdf(f'{path_results}Cs.nc')
elif False: #like above with string _c
    vpbp_coar_median.to_netcdf(f'{path_results}vpbp_coar_median_c.nc')
    aMLI_median.to_netcdf(f'{path_results}aMLI_median_c.nc')
    MLI_alpha_median.to_netcdf(f'{path_results}MLI_alpha_median_c.nc')
    alpha.to_netcdf(f'{path_results}alpha_c.nc')
    aMLI_median_Stone.to_netcdf(f'{path_results}aMLI_median_Stone_c.nc')
    MLI_alpha_median_Stone.to_netcdf(f'{path_results}MLI_alpha_median_Stone_c.nc')
    alpha_Stone.to_netcdf(f'{path_results}alpha_Stone_c.nc')
    Ce.to_netcdf(f'{path_results}Ce_c.nc')
    Cs.to_netcdf(f'{path_results}Cs_c.nc')
else:
    vpbp_coar_median       = xr.open_dataarray(f'{path_results}vpbp_coar_median.nc')
    aMLI_median            = xr.open_dataarray(f'{path_results}aMLI_median.nc')
    MLI_alpha_median       = xr.open_dataarray(f'{path_results}MLI_alpha_median.nc')
    alpha                  = xr.open_dataarray(f'{path_results}alpha.nc')
    aMLI_median_Stone      = xr.open_dataarray(f'{path_results}aMLI_median_Stone.nc')
    MLI_alpha_median_Stone = xr.open_dataarray(f'{path_results}MLI_alpha_median_Stone.nc')
    alpha_Stone            = xr.open_dataarray(f'{path_results}alpha_Stone.nc')
    Ce                     = xr.open_dataarray(f'{path_results}Ce.nc')
    Cs                     = xr.open_dataarray(f'{path_results}Cs.nc')


#%%
print(f'alpha ALS: {alpha_Stone.mean(dim="time").data:2f}')
print(f'alpha PER: {alpha.mean(dim="time").data:2f}')

reload(fu)
fu.plot_tseries_diag_param_stone_vb(vpbp_coar_median, aMLI_median, MLI_alpha_median, alpha, aMLI_median_Stone, MLI_alpha_median_Stone, alpha_Stone, fig_path, savefig)
# %%
