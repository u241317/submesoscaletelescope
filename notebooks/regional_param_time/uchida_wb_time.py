# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '/home/m/m300878/submesoscaletelescope/notebooks/spectral_analysis/')
sys.path.insert(0, '../spectral_analysis/')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# % Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
# from validate_catalog import all_params
# params_dict, cat = all_params()
# params_dict.keys()
import gcm_filters
print('Warning: use node with more than 256Gb RAM')
# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:25:00', wait=False)
cluster
client

# %%
domain        = 'our_domain'
t_space       = 1
savefig       = False
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
if domain=='uchida_domain':
    lat_reg          = 30,  40
    lon_reg          = -78, -68
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/uchida_domain/wb/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/uchida_domain/'
elif domain=='our_domain':
    lon_reg          = np.array([-70, -54])
    lat_reg          = np.array([23.5, 36])
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/our_domain/wb/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/our_domain/'

path_2_data      = f'{path_2_data_root}gridded/'
path_2_data_filt = f'{path_2_data_root}filtered/'
path_results     = f'{path_2_data_root}results/'

############################### Compute/Load ##############################
# %%
reload(eva)
mask_land_100 = eva.load_smt_land_mask(depthc=32)
mask_land_100 = pyic.interp_to_rectgrid_xr(mask_land_100, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land_100 = mask_land_100.drop('depthc')


# %%
if False:
    ro = eva.load_smt_vorticity()
    ro_intero = pyic.interp_to_rectgrid_xr(ro.vort_f_cells_50m.sel(time='2010-03-15T21:00:00.000000000'), fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    ro_intero.plot(vmin=-2, vmax=2, cmap='seismic')
    plt.savefig(fig_path + 'vorticity.png')
# %% load b on interfaces:

b      = xr.open_dataarray(path_2_data + 'b.nc', chunks={'time': 1, 'depthi': 1})
b      = b.compute()
b      = b * mask_land_100
b      = b[:-1]
dbdx   = xr.open_dataarray(path_2_data + 'dbdx.nc', chunks={'time': 1, 'depthi': 1})
dbdy   = xr.open_dataarray(path_2_data + 'dbdy.nc', chunks={'time': 1, 'depthi': 1})
dbdx   = dbdx.compute()
dbdy   = dbdy.compute()
dbdx   = dbdx * mask_land_100
dbdy   = dbdy * mask_land_100
M2     = np.sqrt(dbdx[:-1]**2 + dbdy[:-1]**2)
var    = 'b'
path   = f'{path_2_data_filt}{var}_filt/'
b_filt = xr.open_mfdataset(path + f'{var}_filt*', combine='nested'  , concat_dim='time', chunks={'time': 1, 'depthi': 1})
b_filt = b_filt.b_filt.compute()
b_filt = b_filt * mask_land_100
b_filt = b_filt[:-1]

# %% N2

N2           = xr.open_dataarray(path_2_data + 'N2.nc', chunks={'time': 1, 'depthi': 1})
N2           = N2.compute()
N2[:,0,:,:]  = np.nan
N2[:,-1,:,:] = np.nan
N2           = N2 * mask_land_100
N2           = N2.where(~(N2 == 0), np.nan) # remove zeros


# %%
w      = xr.open_dataarray(path_2_data + 'w.nc', chunks={'time': 1, 'depthi': 1})
w      = w.compute()
w      = w * mask_land_100
w      = w.where(~(w == 0), np.nan) # remove zeros
w      = w.where(np.abs(w) < 1e-2, np.nan) # remove extreme values
w      = w[:-1]

var    = 'w'
path   = f'{path_2_data_filt}{var}_filt/'
w_filt = xr.open_mfdataset(path + f'{var}_filt*', combine='nested'  , concat_dim='time', chunks={'time': 1, 'depthi': 1})
w_filt = w_filt.w_filt.compute()
w_filt = w_filt * mask_land_100


# %%
filter_30km, mask_land_100_new = eva.create_gcm_filter(b, 'irregular', mask_land_100)

# %%
if False:
    %%time
    b              = b[:-1] * mask_land_100_new
    b_filt = filter_30km.apply(b.isel(time=0), dims=['lat', 'lon'])
    # %
    b_filt = b_filt * mask_land_100_new
    b_diff             = b_filt - b.isel(time=0)

    # %%
    %%time
    w                  = w * mask_land_100_new
    w              = w[:-1]
    w_filt = filter_30km.apply(w, dims=['lat', 'lon'])
    w_filt = w_filt * mask_land_100_new
    w_diff             = w_filt - w


    # %%
    x = b_diff.lon
    y = b_diff.lat


# %%  ################################### Compute MLD ################################## 

depth_level = 'depthi'

def compute_mld(depth_level='depthi'):
    if False:
        if False:
            time   = b.time
            ds_T   = eva.load_smt_T()
            t      = ds_T.sel(time=time.data, method='nearest')
            data_t = pyic.interp_to_rectgrid_xr(t, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
            data_t.to_netcdf(path_2_data + 'data_t.nc')
            ds_S   = eva.load_smt_S()
            s      = ds_S.sel(time=time.data, method='nearest')
            data_s = pyic.interp_to_rectgrid_xr(s, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
            data_s.to_netcdf(path_2_data + 'data_s.nc')
        else:
            data_t = xr.open_dataarray(path_2_data + 'data_t.nc', chunks={'time': 1, 'depthc': 1})
            data_s = xr.open_dataarray(path_2_data + 'data_s.nc', chunks={'time': 1, 'depthc': 1})
        data_rho_depthc         = gsw.rho(data_s, data_t, depthc[2])
        if depth_level == 'depthc':
            data_rho_depthc.to_netcdf(path_2_data + 'data_rho_depthc.nc')
        else:
            data_rho_depthi_ = data_rho_depthc.interp(depthc=depthi)
            data_rho_depthi_ = data_rho_depthi_.rename(depthc='depthi')
            data_rho_depthi_.to_netcdf(path_2_data + 'data_rho_depthi_.nc')
    else:
        if depth_level == 'depthc':
            data_rho_depthc = xr.open_dataarray(path_2_data + 'data_rho_depthc.nc', chunks={'time': 1, 'depthc': 1})
            data_rho_depthc = data_rho_depthc.compute()
        else:
            data_rho_depthi_ = xr.open_dataarray(path_2_data + 'data_rho_depthi_.nc', chunks={'time': 1, 'depthi': 1})
            data_rho_depthi_ = data_rho_depthi_.compute()


    MLD_int  = []
    Mask_MLD = []
    MLD      = []
    if depth_level == 'depthc':
        for i in range(len(data_rho_depthc.time)):
            mld_int, mask_mld, mld = eva.calc_mld_xr(data_rho_depthc.isel(time=i), depthc, threshold=0.2)
            MLD_int.append(mld_int)
            Mask_MLD.append(mask_mld)
            MLD.append(mld)
    else:
        for i in range(len(data_rho_depthi_.time)):
            mld_int, mask_mld, mld = eva.calc_mld_xr(data_rho_depthi_.isel(time=i), depthi, threshold=0.2)
            MLD_int.append(mld_int)
            Mask_MLD.append(mask_mld)
            MLD.append(mld)

    MLD_int  = xr.concat(MLD_int, dim='time')
    Mask_MLD = xr.concat(Mask_MLD, dim='time')
    MLD      = xr.concat(MLD, dim='time')

    if True:
        MLD = MLD * mask_land_100_new
        plt.figure()
        MLD.isel(time=7).plot(vmin=0, vmax=450, cmap='Spectral_r')
        if savefig==True: plt.savefig(fig_path + 'MLD_c.png', dpi=250)

    return MLD_int, Mask_MLD, MLD

print('use 0.2 threshold for MLD')
if False:
    if depth_level == 'depthc':
        MLD_int, Mask_MLD, MLD = compute_mld(depth_level)
        MLD_int.to_netcdf(path_2_data + f'MLD_int_{depth_level}.nc')
        Mask_MLD.to_netcdf(path_2_data + f'Mask_MLD_{depth_level}.nc')
        MLD.to_netcdf(path_2_data + f'MLD_{depth_level}.nc')
    else:
        MLD_int, Mask_MLD, MLD = compute_mld()
        MLD_int.to_netcdf(path_2_data + 'MLD_int.nc')
        Mask_MLD.to_netcdf(path_2_data + 'Mask_MLD.nc')
        MLD.to_netcdf(path_2_data + 'MLD.nc')
else:
    if depth_level == 'depthc':
       MLD_c        = xr.open_dataarray(path_2_data + f'MLD_{depth_level}.nc', chunks={'time': 1})
       MLD_int_c    = xr.open_dataarray(path_2_data + f'MLD_int_{depth_level}.nc', chunks={'time': 1})
       Mask_MLD_c   = xr.open_dataarray(path_2_data + f'Mask_MLD_{depth_level}.nc', chunks={'time': 1})
       MLD_c        = MLD_c.compute()
       MLD_int_c    = MLD_int_c.compute()
       Mask_MLD_c   = Mask_MLD_c.compute()
       MLDC         = MLD_c * mask_land_100_new
       MLD_c        = MLD_c[:-1]
       Mask_MLD_c   = Mask_MLD_c[:-1]
    else:
        MLD      = xr.open_dataarray(path_2_data + 'MLD.nc', chunks={'time': 1})
        MLD_int  = xr.open_dataarray(path_2_data + 'MLD_int.nc', chunks={'time': 1})
        Mask_MLD = xr.open_dataarray(path_2_data + 'Mask_MLD.nc', chunks={'time': 1})
        MLD      = MLD.compute()
        Mask_MLD = Mask_MLD.compute()
        MLD      = MLD * mask_land_100_new
        MLD      = MLD[:-1]
        Mask_MLD = Mask_MLD[:-1]
        Mask_MLD = Mask_MLD.transpose('time', 'depthi', 'lat', 'lon')


#TODO Double check MLD diagnostic with porfiles
#%%test
if False:
    t = 10; lat=280
    plt.figure(figsize=(4,8))
    N2.isel(time=t, lon=250, lat=lat, depthi=slice(0,100)).plot(y='depthi')
    xmin= N2.isel(time=t, lon=250, lat=lat, depthi=slice(0,100)).min().data
    xmax= N2.isel(time=t, lon=250, lat=lat, depthi=slice(0,100)).max().data
    plt.hlines(MLD.isel(time=t, lon=250, lat=lat), xmin, xmax, colors='r')
    plt.ylim(500,0)

# %% ############################# Compute wb prime over MLD ############################
depthi_     = xr.DataArray(depthi, dims='depthi')
dz_diff     = depthi_.diff(dim='depthi')
dz_diff_ext = np.append(dz_diff, np.nan)
dz_diff_ext = xr.DataArray(dz_diff_ext, dims='depthi')
# TODO: repeat evaluation on cell centers
print('warning: dz_diff_ext is for values at interfaces, not cell centers')

wb_dat     = ((w - w_filt) * (b - b_filt)) 
if True:
    wb_dat.to_netcdf(path_2_data + 'wb_dat.nc')
else:
    wb_dat = xr.open_dataarray(path_2_data + 'wb_dat.nc', chunks={'time': 1})
    wb_dat = wb_dat.compute()
#%%
wb_dat     = wb_dat * dz_diff_ext * Mask_MLD
wb_dat_sum = wb_dat.sum(dim='depthi', skipna=True)
wb_prime   = wb_dat_sum / MLD
wb_prime   = wb_prime * mask_land_100

if True:
    wb_prime.to_netcdf(path_2_data + 'wb_prime.nc')

# %%
reload(fu)
fu.plot_wb_prime(b.lon,b.lat,wb_prime.isel(time=0), fig_path, savefig)
# %%
#check mld
if False:
    plt.figure(figsize=(15,8))
    data = wb_dat_sel.isel( lon=400)
    plt.pcolormesh(data.lat, data.depthi, data, cmap='RdBu_r', vmin=-1e-6, vmax=1e-6)
    plt.plot(MLD.isel(time=0, lon=400).lat ,MLD.isel(time=0, lon=400), color='blue', label='0.03', lw=2)
    plt.plot(MLD2.isel(time=0, lon=400).lat ,MLD2.isel(time=0, lon=400), color='green', label='0.2', lw=2)
    plt.ylim(1150,0)
    plt.legend()
    plt.xlabel('lat')
    plt.ylabel('depth')
    if savefig==True: plt.savefig(fig_path + 'MLD_method_wb.png')
    #%%
    plt.figure(figsize=(15,8))
    data = N2.isel(time=0, lon=400)
    plt.pcolormesh(data.lat, data.depthi, data, cmap='RdBu_r', vmin=-3e-5, vmax=3e-5)
    plt.plot(MLD.isel(time=0, lon=400).lat ,MLD.isel(time=0, lon=400), color='blue', label='0.03', lw=2)
    plt.plot(MLD2.isel(time=0, lon=400).lat ,MLD2.isel(time=0, lon=400), color='green', label='0.2', lw=2)
    plt.ylim(1150,0)
    plt.legend()
    plt.xlabel('lat')
    plt.ylabel('depth')
    if savefig==True: plt.savefig(fig_path + 'MLD_method.png')
# %%  #################################### Coarsen Data ####################################
f = eva.calc_coriolis_parameter(b.lat)
ncoars          = 4
wb_prime_coars  = wb_prime.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
print('coarsening to', (wb_prime_coars.lat[1]-wb_prime_coars.lat[0]).data, 'degrees')
mask_land_coars = mask_land_100.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)

MLD_coars = MLD.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
#%%
if True: #their way
    M2_coars        = (M2* Mask_MLD).coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
    M2_H_mean_coars = ((M2_coars ) * dz_diff_ext).sum(dim='depthi', skipna=True) #/ MLD_coars  #not divided by MLD in Uchida TODO:WHY?
    f_coars         = eva.calc_coriolis_parameter(M2_coars.lat)
    MLI_coars       = (M2_H_mean_coars**2/f_coars)
    # MLI_coars       = MLI_coars.where(MLI_coars < 0.0001, np.nan)*mask_land_coars
else:
    M2_coars        = (M2* Mask_MLD).coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
    M2_coars        = (M2_coars * dz_diff_ext).sum(dim='depthi', skipna=True) / MLD_coars
    f_coars         = eva.calc_coriolis_parameter(M2_coars.lat)
    MLI_coars       = (M2_coars/f_coars**2)**2 * MLD_coars**2 * f_coars**3
# else: # a little bit cleaner but weaker by a factor of 2 TODO: can we make the same difference for wb?
#     m2x_mean  = (dbdx * Mask_MLD * dz_diff_ext).sum(dim='depthi', skipna=True) / MLD
#     m2y_mean  = (dbdy * Mask_MLD * dz_diff_ext).sum(dim='depthi', skipna=True) / MLD
#     m2_mean   = (np.sqrt(np.power(m2x_mean,2) + np.power(m2y_mean,2)))
#     M2_coars  = m2_mean.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
#     f_coars   = eva.calc_coriolis_parameter(M2_coars.lat)
#     MLI_coars = (M2_coars/f_coars**2)**2 * MLD_coars**2 * f_coars**3
#     MLI_coars = MLI_coars * mask_land_coars

#%%
MLI_coars.isel(time=0).plot(vmin =0, vmax=4e-6, cmap='Reds')
if savefig==True: plt.savefig(fig_path + 'MLI_coars.png')



# %% Compute Tuning Coefficient
Ce = wb_prime_coars / MLI_coars
if True:
    plt.figure()
    Ce.isel(time=28).plot(vmin=-1, vmax=1, cmap='RdBu_r')
    if savefig==True:  plt.savefig(fig_path + 'Ce.png')
#remove outliers
# Ce = Ce.where(Ce < 1e2, np.nan)
alpha = Ce.median(['lat', 'lon'],skipna=True)
print(alpha.data)
aMLI = MLI_coars * alpha

#%%
reload(fu)
fu.plot_wb_prime_MLI_uchida(aMLI.isel(time=0), wb_prime_coars.isel(time=0), fig_path, savefig)
# %%
reload(fu)
wpbp_coar        = wb_prime_coars
time             = wb_prime_coars.time.data
wpbp_coar_median = wpbp_coar.median(['lat','lon'],skipna=True)
aMLI_median      = aMLI.median(['lat','lon'],skipna=True)
MLI_alpha_median = (alpha.mean('time')*MLI_coars).median(['lat','lon'],skipna=True)
#%%
if True:
    wpbp_coar_median.to_netcdf(path_results + 'wpbp_coar_median.nc')
    aMLI_median.to_netcdf(path_results + 'aMLI_median.nc')
    MLI_alpha_median.to_netcdf(path_results + 'MLI_alpha_median.nc')
    alpha.to_netcdf(path_results + 'alpha.nc')
else:
    wpbp_coar_median = xr.open_dataarray(path_results + 'wpbp_coar_median.nc', chunks={'time': 1})
    aMLI_median      = xr.open_dataarray(path_results + 'aMLI_median.nc', chunks={'time': 1})
    MLI_alpha_median = xr.open_dataarray(path_results + 'MLI_alpha_median.nc', chunks={'time': 1})
    alpha            = xr.open_dataarray(path_results + 'alpha.nc', chunks={'time': 1}).compute()

 # %%
reload(fu)
fu.plot_tseries_diag_param(wpbp_coar_median, aMLI_median, MLI_alpha_median, alpha, fig_path, savefig)





# %% ############################### MLI Stone ##############################
if True:
    # Ri = (N2 * f**2 / M2**2) * mask_land_100 * Mask_MLD
    # Ri_coars = Ri.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
    # Ri_mean_coars = (Ri_coars*mask_mld_coars * dz_diff_ext).sum(dim='depthi', skipna=True) / MLD_coars

    N2_coars = (N2 * Mask_MLD).coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
    N2_coars = (N2_coars*dz_diff_ext).sum(dim='depthi', skipna=True) / MLD_coars
    # M2_coars = (M2_coars * dz_diff_ext).sum(dim='depthi', skipna=True) / MLD_coars
    # Ri_coars        = N2_coars * f_coars**2 / M2_H_mean_coars**2
    # Ri_coars        = N2_coars * f_coars**2 / M2_mean_coars**2
    Ri_coars        = N2_coars * f_coars**2 / M2_coars**2
    # Ri_mean_coars   = (Ri_coars * dz_diff_ext).sum(dim='depthi', skipna=True) / MLD_coars
    MLI_Stone_coars = 1/np.sqrt(1+Ri_coars) * MLI_coars * mask_land_coars
    Ri_coars.isel(time=0).plot(vmin =0, vmax=10, cmap='Blues_r')
    if savefig==True: plt.savefig(fig_path + 'Ri_mean_coars.png')
    plt.figure()
    (1/np.sqrt(1+Ri_coars)).isel(time=0).plot(vmin =0, vmax=1, cmap='Reds')
    if savefig==True: plt.savefig(fig_path + '1_sqrt_Ri_mean_coars.png')
else:
    f = eva.calc_coriolis_parameter(N2.lat)
    N2_mean   = (N2 * Mask_MLD).sum(dim='depthi', skipna=True)
    Ri        = N2_mean * f**2 / M2_mean**2 
    MLI_Stone = 1/np.sqrt(1+Ri) * ((M2_mean * M2_mean) / f) * mask_land_100
    # MLI_Stone.isel(time=6).plot(vmin =0, vmax=2e-7, cmap='viridis')

    MLI_Stone_coars = MLI_Stone.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
    MLI_Stone_coars.isel(time=0).plot(vmin =0, vmax=2e-7, cmap='Reds')

#%%
MLI_coars.isel(time=0) .plot(vmin =0, vmax=4e-6, cmap='Reds')
if savefig==True: plt.savefig(fig_path + 'MLI_FK_coars.png', dpi=150)
#%%
MLI_Stone_coars.isel(time=0).plot(vmin =0, vmax=4e-6, cmap='Reds')
if savefig==True: plt.savefig(fig_path + 'MLI_Stone_coars.png', dpi=150)
# %%    evaluate difference in tuning coeff
if False:
    # %
    a  = 1/np.sqrt(1+Ri) * mask_land_100_new
    A = a.isel(time=6).compute()
    plt.figure()
    A.plot(vmin =0.8, vmax=1.2, cmap='RdBu')
    plt.title('1/sqrt(1+Ri)')
    if savefig==True:plt.savefig(fig_path + 'diff_stone_fox-kemper.png')
    # %
    Ris = Ri.isel(time=6).compute() * mask_land_100_new *100
    plt.figure()
    Ris.plot(vmin =0, vmax=50, cmap='Blues_r')
    plt.title('Ri')
    if savefig==True:plt.savefig(fig_path + 'Ri.png')
    # %
    plt.figure()
    M2_means = M2_mean.isel(time=6).compute() * mask_land_100_new
    M2_means.plot(vmin=0, vmax=1e-5, cmap='viridis')
    # %
    plt.figure()
    N2_means = N2_mean.isel(time=6).compute() * mask_land_100_new
    N2_means.plot(vmin=1e-4, vmax=8e-4, cmap='plasma')
    plt.title('N2 MLD mean')
    if savefig==True:plt.savefig(fig_path + 'N2_mean.png')
# %%  #################################### Coarsen Data ####################################

# %% Compute Tuning Coefficient
Cs = wb_prime_coars / MLI_Stone_coars
if True:
    plt.figure()
    Cs.isel(time=1).plot(vmin=-1, vmax=1, cmap='RdBu_r')   
    plt.savefig(fig_path + 'Cs.png')
#remove outliers
Cs = Cs.where(np.abs(Cs) < 1e2, np.nan)

alpha_Stone = Cs.median(['lat', 'lon'],skipna=True)
aMLI_Stone  = MLI_Stone_coars * alpha_Stone
print(alpha_Stone.data)

# %%
# savefig = True
reload(fu)
fu.plot_wb_prime_MLI_uchida(aMLI_Stone.isel(time=0), wb_prime_coars.isel(time=0), fig_path, savefig)

# %%
aMLI_median_Stone      = aMLI_Stone.median(['lat','lon'],skipna=True)
MLI_alpha_median_Stone = (alpha_Stone.mean('time')*MLI_Stone_coars).median(['lat','lon'],skipna=True)
#%%
if True:
    aMLI_median_Stone.to_netcdf(path_results + 'aMLI_median_Stone.nc')
    MLI_alpha_median_Stone.to_netcdf(path_results + 'MLI_alpha_median_Stone.nc')
    alpha_Stone.to_netcdf(path_results + 'alpha_Stone.nc')
else:
    aMLI_median_Stone      = xr.open_dataarray(path_results + 'aMLI_median_Stone.nc', chunks={'time': 1})
    MLI_alpha_median_Stone = xr.open_dataarray(path_results + 'MLI_alpha_median_Stone.nc', chunks={'time': 1})
    alpha_Stone            = xr.open_dataarray(path_results + 'alpha_Stone.nc', chunks={'time': 1}).compute()

#%%
print(f'alpha ALS: {alpha_Stone.mean(dim="time").data:2f}')
print(f'alpha PER: {alpha.mean(dim="time").data:2f}')
# %%
reload(fu)
fu.plot_tseries_diag_param_stone(wpbp_coar_median, aMLI_median, MLI_alpha_median, alpha, aMLI_median_Stone, MLI_alpha_median_Stone, alpha_Stone, fig_path, savefig)

# %%
plt.figure()
plt.plot(Ce.median(['lat', 'lon'],skipna=True), color='tab:blue',   ls=':', label='PER Ce median')
plt.plot(Cs.median(['lat', 'lon'],skipna=True), color='tab:orange', ls=':', label='ALS Cs median')
plt.plot(Ce.mean(['lat', 'lon'],skipna=True)  , color='tab:blue',   label='PER Ce mean')
plt.plot(Cs.mean(['lat', 'lon'],skipna=True)  , color='tab:orange', label='ALS Cs mean')
plt.legend()
plt.yscale('log')
# plt.ylim(0,0.2)
if savefig==True: plt.savefig(fig_path + 'Ce_Cs_mean median.png')

#%%
#share colorbar
fig, ax = plt.subplots(1, 3, figsize=(18, 5), sharey=True, subplot_kw={'projection': ccrs.PlateCarree()})
Ce.isel(time=0).plot(ax=ax[0],vmin=-1, vmax=1, cmap='RdBu_r')
Cs.isel(time=0).plot(ax=ax[1], vmin=-1, vmax=1, cmap='RdBu_r')
(Ce-Cs).isel(time=0).plot(ax=ax[2], vmin=-1, vmax=1, cmap='RdBu_r')
ax[0].set_title('Ce')
ax[1].set_title('Cs')
ax[2].set_title('Ce-Cs')
if savefig==True: plt.savefig(fig_path + 'Ce_Cs.png',dpi=250)

# %%
