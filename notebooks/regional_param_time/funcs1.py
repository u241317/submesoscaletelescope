# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools


import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

import matplotlib.colors as clr


def plot_N2_wb(N2, wb_prime_mean, mld_03, mld_20, mld_20_mean, fig_path, tt=0, savefig=False):
    hca, hcb = pyic.arrange_axes(1,2, plot_cb=True, asp=0.5, fig_size_fac=2, sharey=True, sharex=True)
    ii=-1

    clim = 0,3e-5
    cmap = 'Reds'
    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    # ax.set_title(r'mean $N2$')
    # pyic.shade(data_n2_mean.lat.data, data_n2_mean.depthi.data, data_n2_mean.isel(lon=400).data, ax=ax, cax=cax, cmap=cmap, clim=clim)
    # ax.plot(mld_20_mean.lat.data, mld_20_mean.isel(lon=400).data, color='tab:green', label='MLD 0.20')


    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(rf'$N^2$ at {N2.time.values:.13} ')
    hm2=pyic.shade(N2.lat.data, N2.depthi.data, N2.isel(lon=400).data, ax=ax, cax=cax, cmap=cmap, clim=clim)
    ax.plot(mld_03.lat.data, mld_03.isel(time =tt,lon=400).data, label=r'$\Delta \rho = 0.03$')
    ax.plot(mld_20.lat.data, mld_20.isel(time =tt,lon=400).data, label=r'$\Delta \rho = 0.20$', color='tab:green')
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)
    cax.set_title(r'$1/s^2$', pad=10)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1e-7
    hm2 = pyic.shade(wb_prime_mean.lat.data, wb_prime_mean.depthi.data, wb_prime_mean.isel(lon=400).data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim)
    ax.plot(mld_20_mean.lat.data, mld_20_mean.isel(lon=400).data, color='tab:green', label=r'$\Delta \rho = 0.20$')
    ax.set_title(r'mean $\overline{w^{\prime}b^{\prime}}$')
    cax.set_title(r'$m^2/s^3$', pad=10)
    ax.set_xlabel('latitude')

    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    for ax in hca:
        pyic.plot_settings(ax=ax)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) #type: ignore
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) #type: ignore
        ax.set_ylim(1200,0)
        ax.legend(loc='lower left')
        ax.set_ylabel('depth [m]')

    if savefig == True: plt.savefig(f'{fig_path}wb_n2_prime_mean_vs_snapshot{N2.isel(lon=400).lon.values:.0f}.png', dpi=300)




def plot_wb_prime(ds1, ds2, xlim, ylim, fig_path, savefig):
    asp = (ylim[1]-ylim[0])/(xlim[1]-xlim[0])
    hca, hcb = pyic.arrange_axes(2,1, plot_cb='right', asp=asp, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, sharey=True, axlab_kw=None,
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1.2e-7
    cmap = 'RdBu_r'
    pyic.shade(ds1.lon, ds1.lat, ds1.wb, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'$\overline{<w^{\prime}b^{\prime}>}$'+f' at {ds2.depthi:.0f} m')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(ds2.lon, ds2.lat, ds2.wb, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'$\overline{w^{\prime}b^{\prime}}$'+f' at {ds1.depthi:.0f} m')


    for ax in hca:
        pyic.plot_settings(ax=ax, xlim=xlim, ylim=ylim)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))

    if savefig==True: plt.savefig(f'{fig_path}wb_comp.png', dpi=250, bbox_inches='tight')


def plot_wb_prime_scatter(ds1, ds2, xlim, ylim, fig_path, savefig):
    asp = (ylim[1]-ylim[0])/(xlim[1]-xlim[0])
    hca, hcb = pyic.arrange_axes(2,1, plot_cb=[0,1], asp=asp, fig_size_fac=2, # type: ignore
                                 projection=ccrs_proj,
                                 dfigt=1.0, sharey=True,
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1.2e-7
    cmap = 'RdBu_r'
    pyic.shade(ds1.lon, ds1.lat, ds1, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'$\overline{<w^{\prime}b^{\prime}>}$')



    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(ds2.lon, ds2.lat, ds2, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'$<\overline{w^{\prime}b^{\prime}}>$')

    cax.set_title(r'$m/s^2$', pad=10)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    for ax in hca:
        pyic.plot_settings(ax=ax, xlim=xlim, ylim=ylim)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))


    if savefig==True: plt.savefig(f'{fig_path}wb_comp_ml_mean.png', dpi=250, bbox_inches='tight')


def scatter_wb(ds1, ds2, fig_path, savefig):

    xdata_flat  = ds1.values.flatten()
    ydata_flat  = ds2.values.flatten()
    mask        = ~np.isnan(xdata_flat) & ~np.isnan(ydata_flat)
    xdata_clean = xdata_flat[mask]
    ydata_clean = ydata_flat[mask]
    
    H, x_edge, y_edge = np.histogram2d(np.log10(np.abs(xdata_clean)), np.log10(np.abs(ydata_clean)), bins=1100)

    fig, ax = plt.subplots(figsize=(6   ,6))
    
    mask = H.T == 0
    H.T[mask] = np.nan

    cb = plt.pcolormesh(10**x_edge, 10**y_edge, H.T, cmap='Blues')
    #mask nan
    #plot pcolormersh wwith white colrer where H.T is zero
    # plt.pcolormesh(10**x_edge, 10**y_edge, H.T, )
    ax.set_xscale('log')
    ax.set_yscale('log')
    # plot colorbar
    # cbar = plt.colorbar(cb, ax=ax, orientation='vertical', pad=0.02)
    # cbar.set_label('count')
    plt.grid()
    ax.set_xlim([1e-10,2.6e-7])
    ax.set_ylim([1e-10,2.6e-7])
    #plot diagonal
    ax.plot([1e-11, 1e-6], [1e-11, 1e-6], color='black', linestyle='--')

    ax.set_xlabel(r'$\overline{<w^{\prime}b^{\prime}>}$')
    ax.set_ylabel(r'$<\overline{w^{\prime}b^{\prime}}>$')

    #label like pyic
    posx=[-0.00]
    posy=[1.05]
    string = ['(c)']
    fontdic = None
    ax.text(posx[0], posy[0], string[0], 
                      transform = ax.transAxes, 
                      horizontalalignment = 'right',
                      fontdict=fontdic) 

    if savefig==True: plt.savefig(f'{fig_path}wb_scatter.png', dpi=250, bbox_inches='tight')


def scatter_wb_log(ds1, ds2, fig_path, savefig):

    xdata_flat  = ds1.values.flatten()
    ydata_flat  = ds2.values.flatten()
    mask        = ~np.isnan(xdata_flat) & ~np.isnan(ydata_flat)
    xdata_clean = xdata_flat[mask]
    ydata_clean = ydata_flat[mask]
    
    H, x_edge, y_edge = np.histogram2d(np.log10(np.abs(xdata_clean)), np.log10(np.abs(ydata_clean)), bins=1000)

    fig, ax = plt.subplots(figsize=(6, 6))
    cb = plt.pcolormesh(10**x_edge, 10**y_edge, H.T, cmap='Spectral_r', norm=clr.LogNorm())
    # ax.set_xscale('log')
    # ax.set_yscale('log')
    # plot colorbar
    cbar = plt.colorbar(cb, ax=ax, orientation='vertical', pad=0.02)
    cbar.set_label('count')
    plt.grid()
    ax.set_xlim([1e-11,1e-6])
    ax.set_ylim([1e-11,1e-6])
    #plot diagonal
    ax.plot([1e-11, 1e-6], [1e-11, 1e-6], color='black', linestyle='--')

    #apply lin reg
    # slope, intercept, r_value, p_value, std_err = stats.linregress(np.log10(np.abs(xdata_clean)), np.log10(np.abs(ydata_clean)))
    # x = np.array([1e-11, 1e-6])
    # y = 10**(slope*np.log10(x) + intercept)
    # ax.plot(x, y, color='black', linestyle='-', linewidth=1)
    # ax.text(1e-10, 1e-6, f'$R^2={r_value**2:.2f}$', fontsize=10)

    X, err_covX, CI, R2, rXY, sigma_e = linear_regression_matrix_simple(x_edge,y_edge,)
    a1 = X[0]
    a0 = X[1]

    x = x_edge
    slope = rf'{X[0]:.2f} \pm {err_covX[0]:.2f}'
    intercept = rf'{10**X[1]:.2f} \pm {10**err_covX[1]:.2f}'
    ax.plot(x, a0 + a1*x, color=f'k', linewidth=3, label=rf'$ ({intercept} )~Ri^{{{slope}}}$ ', zorder=20)



    ax.set_xlabel(r'$\overline{<w^{\prime}b^{\prime}>}$')
    ax.set_ylabel(r'$<\overline{w^{\prime}b^{\prime}}>$')

    #label like pyic
    posx=[-0.00]
    posy=[1.05]
    string = ['(c)']
    fontdic = None
    ax.text(posx[0], posy[0], string[0], 
                      transform = ax.transAxes, 
                      horizontalalignment = 'right',
                      fontdict=fontdic) 

    if savefig==True: plt.savefig(f'{fig_path}wb_scatter.png', dpi=250, bbox_inches='tight')



def linear_regression_matrix_simple(x, y, sigma_sq=np.array([1])):
    """
    linear regression for Y +-E = A * X 
    X: predictor variable
    Y: response variable
    sigma: individual standard deviation of the response variable
    """
    if len(sigma_sq) == int(1): print('sigma is None or 1, the data is not normalized') # type: ignore
    else: print('data is normalized with sigma_sq')

    n          = len(x)                     # number of data points
    A          = np.ones((n,2))             # Matrix A design
    A[:,0]     = x/sigma_sq                    # first column of A is the predictor variable normalized by the standard deviation
    A[:,1]     = np.ones(n)/sigma_sq           # second column of A is a vector of ones normalized by the standard deviation
    d          = y/sigma_sq                    # response variable normalized by the standard deviation
    AT         = np.transpose(A)
    ATA        = np.dot(AT,A)               # A^T * A # matrix of the normal equations
    ATA_inv    = np.linalg.inv(ATA)         # covariance of X 
    ATA_inv_AT = np.dot(ATA_inv,AT)
    X          = np.dot(ATA_inv_AT,d)       # X = (A^T * A)^-1 * A^T * d # sloepe and intercept of the linear regression

    err_covX = np.sqrt(np.diag(ATA_inv))    # standard deviation of the slope and intercept

    # print(f'SSE {np.sum((y - X[0]*x - X[1])**2)}')
    sigma_e    = np.sqrt(np.sum((y - X[0]*x - X[1])**2)/(n-2))   # standard deviation of the error rms_fit

    R2         = 1 - np.sum((y - X[0]*x - X[1])**2)/np.sum((y - np.mean(y))**2) # coefficent of determination "interpretation is that it is an estimate of the model ability to specify unrealized values of the response variable Y. A value of 1 indicates that the model perfectly predicts all responses, while a value of 0 indicates no linear relationship between the response and predictor variables."
    rXY        = np.sum((x - np.mean(x))*(y - np.mean(y)))/(np.sqrt(np.sum((x - np.mean(x))**2))*np.sqrt(np.sum((y - np.mean(y))**2))) # correlation coefficient: a coefficient of correlation close to 1 indicates a strong positive linear relationship while a value close to 0 indicates no relationship. A value close to -1 indicates a strong negative linear relationship.

    # confidence interval for the response variable
    q          = stats.t.ppf(0.975, n-2)               # quantile for t(n-2) with 95% probability
    # print('q', q)
    # x_mean     = np.mean(x)
    # SXX        = np.sum((x - x_mean)**2)
    # sigma_mu_y = sigma_e * sigma_e * np.sqrt(1 + 1/n + (x-x_mean)**2/SXX)
    # CI         = q * np.sqrt(sigma_mu_y)
    # print(f'CI: {CI}')

    CI = q * np.sqrt(np.diag(ATA_inv))              # confidence interval for the slope and intercept: including normalization
    # print(f'CI: {CI}')

    #print(f' sigma_mu_y: {sigma_mu_y} \n sigma y_sq: {sigma_y_sq}')

    # print(f'X: {X} err_covX: {err_covX} sigma_e: {sigma_e:.2f} \n R2: {R2:.4f} rXY: {rXY:.4f}')
    # print('returns CI from Intercept')
    return X, err_covX, CI[1], R2, rXY, sigma_e


def plot_wb_prime_inst(ds1, xlim, ylim, fig_path, savefig):
    asp = (ylim[1]-ylim[0])/(xlim[1]-xlim[0])
    hca, hcb = pyic.arrange_axes(1,1, plot_cb='right', asp=asp, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, sharey=True, axlab_kw=None,
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1.2e-7
    cmap = 'RdBu_r'
    pyic.shade(ds1.lon, ds1.lat, ds1.wb, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'$<w^{\prime}b^{\prime}>$'+f' at {ds1.time.data:.13} and {ds1.depthi:.0f} m')

    for ax in hca:
        pyic.plot_settings(ax=ax, xlim=xlim, ylim=ylim)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))

    if savefig==True: plt.savefig(f'{fig_path}wb_inst.png', dpi=250, bbox_inches='tight')


def plot_wb_prime_inst_ml_mean(ds1, xlim, ylim, fig_path, savefig):
    asp = (ylim[1]-ylim[0])/(xlim[1]-xlim[0])
    hca, hcb = pyic.arrange_axes(1,1, plot_cb='right', asp=asp, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, sharey=True, axlab_kw=None,
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1.2e-7
    cmap = 'RdBu_r'
    pyic.shade(ds1.lon, ds1.lat, ds1.wb_mld_mean, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'$<w^{\prime}b^{\prime}>$'+f' at {ds1.time.data:.13}, ML averaged')

    for ax in hca:
        pyic.plot_settings(ax=ax, xlim=xlim, ylim=ylim)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))

    if savefig==True: plt.savefig(f'{fig_path}wb_ml_mean_inst.png', dpi=250, bbox_inches='tight')


def plot_wb_prime_mld_mean(ds1, ds2, xlim, ylim, fig_path, savefig):
    asp = (ylim[1]-ylim[0])/(xlim[1]-xlim[0])
    hca, hcb = pyic.arrange_axes(2,1, plot_cb='right', asp=asp, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 projection=ccrs_proj,
                                 dfigt=1.0, sharey=True, 
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1.2e-7
    cmap = 'RdBu_r'
    pyic.shade(ds1.lon, ds1.lat, ds1.wb_mld_mean, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'$\overline{<w^{\prime}b^{\prime}>}$'+f' ML averaged')


    ii+=1; ax=hca[ii]; cax=hcb[ii]
    hm2 = pyic.shade(ds2.lon, ds2.lat, ds2.wb_mld_mean, ax=ax, cax=cax, cmap=cmap, clim=clim, transform=ccrs_proj, rasterized=False)
    ax.set_title(r'$\overline{w^{\prime}b^{\prime}}$'+f' ML averaged')
    cax.set_title(r'$m/s^2$', pad=10)
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    for ax in hca:
        pyic.plot_settings(ax=ax, xlim=xlim, ylim=ylim)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))

    if savefig==True: plt.savefig(f'{fig_path}wb_comp_ml_ave.png', dpi=250, bbox_inches='tight')


def plot_wb_prime_sec(ds1, ds2, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(2,1, plot_cb='right', asp=0.7, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 dfigt=1.0, sharey=True, 
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1.2e-7
    cmap = 'RdBu_r'
    pyic.shade(ds1.lat, ds1.depthi, ds1.wb, ax=ax, cax=cax, cmap=cmap, clim=clim)
    ax.set_title(r'$\overline{<w^{\prime}b^{\prime}>}$'+f' at {ds1.lon.data:.0f}°')
    # cax.set_title(r'$m/s^2$')
    ax.plot(ds1.lat, ds1.mld, color='tab:green', linestyle='-', linewidth=1)
    ax.set_ylabel('Depth [m]')

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    pyic.shade(ds2.lat, ds2.depthi, ds2.wb, ax=ax, cax=cax, cmap=cmap, clim=clim)
    ax.set_title(r'$\overline{w^{\prime}b^{\prime}}$'+f' at {ds2.lon.data:.0f}° ')
    # cax.set_title(r'$m/s^2$')
    ax.plot(ds2.lat, ds2.mld, color='tab:green', linestyle='-', linewidth=1)

    for ax in hca:
        pyic.plot_settings(ax=ax)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))
        ax.set_ylim([1200, 0])
        ax.set_xlabel('Latitude [°]')

    if savefig==True: plt.savefig(f'{fig_path}wb_comp_sec.png', dpi=250, bbox_inches='tight')


def plot_wb_prime_sec_inst(ds1, fig_path, savefig):
    hca, hcb = pyic.arrange_axes(1,1, plot_cb='right', asp=0.7, fig_size_fac=2, # type: ignore
                                  xlabel="", ylabel="",
                                 dfigt=1.0, sharey=True, 
                                 )
    ii=-1
    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1.2e-7
    cmap = 'RdBu_r'
    pyic.shade(ds1.lat, ds1.depthi, ds1.wb, ax=ax, cax=cax, cmap=cmap, clim=clim)
    ax.set_title(r'$<w^{\prime}b^{\prime}>$'+f' at {ds1.lon.data:.0f}° and {ds1.time.data:.13}')
    # cax.set_title(r'$m/s^2$')
    ax.plot(ds1.lat, ds1.mld, color='tab:green', linestyle='-', linewidth=1)
    ax.set_ylabel('Depth [m]')
    #

    for ax in hca:
        pyic.plot_settings(ax=ax)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(4))
        ax.set_ylim([1200, 0])
        ax.set_xlabel('Latitude [°]')

    if savefig==True: plt.savefig(f'{fig_path}wb_comp_sec_inst.png', dpi=250, bbox_inches='tight')

# %%
