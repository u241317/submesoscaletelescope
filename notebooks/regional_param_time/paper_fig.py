# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '/home/m/m300878/submesoscaletelescope/notebooks/spectral_analysis/')
sys.path.insert(0, '../spectral_analysis/')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# % Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os


# %%
domain        = 'our_domain'
t_space       = 1
savefig       = False
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
if domain=='uchida_domain':
    lat_reg          = 30,  40
    lon_reg          = -78, -68
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/uchida_domain/vb/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/uchida_domain/'
elif domain=='our_domain':
    lon_reg          = np.array([-70, -54])
    lat_reg          = np.array([23.5, 36])
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/our_domain/vb/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/our_domain/'

path_2_data      = f'{path_2_data_root}gridded/'
path_2_data_filt = f'{path_2_data_root}filtered/'
path_results     = f'{path_2_data_root}results/vb/'


# %%
#clean
string = ''

vb_vpbp_coar_median       = xr.open_dataarray(f'{path_results}vpbp_coar_median{string}.nc')
vb_aMLI_median            = xr.open_dataarray(f'{path_results}aMLI_median{string}.nc')
vb_MLI_alpha_median       = xr.open_dataarray(f'{path_results}MLI_alpha_median{string}.nc')
vb_alpha                  = xr.open_dataarray(f'{path_results}alpha{string}.nc')
vb_aMLI_median_Stone      = xr.open_dataarray(f'{path_results}aMLI_median_Stone{string}.nc')
vb_MLI_alpha_median_Stone = xr.open_dataarray(f'{path_results}MLI_alpha_median_Stone{string}.nc')
vb_alpha_Stone            = xr.open_dataarray(f'{path_results}alpha_Stone{string}.nc')
vb_Ce                     = xr.open_dataarray(f'{path_results}Ce{string}.nc')
vb_Cs                     = xr.open_dataarray(f'{path_results}Cs{string}.nc')


#%%
print(f'alpha ALS: {vb_alpha_Stone.mean(dim="time").data:2f}')
print(f'alpha PER: {vb_alpha.mean(dim="time").data:2f}')

reload(fu)
# fu.plot_tseries_diag_param_stone_vb(vpbp_coar_median, aMLI_median, MLI_alpha_median, alpha, aMLI_median_Stone, MLI_alpha_median_Stone, alpha_Stone, fig_path, savefig)
fu.plot_tseries_diag_param_stone_vb(vb_vpbp_coar_median, vb_aMLI_median, vb_MLI_alpha_median, vb_alpha, vb_aMLI_median_Stone, vb_MLI_alpha_median_Stone, vb_alpha_Stone, fig_path, savefig)
# %%
path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/our_domain/'
path_results     = f'{path_2_data_root}results/'

wpbp_coar_median = xr.open_dataarray(path_results + 'wpbp_coar_median.nc', chunks={'time': 1})
aMLI_median      = xr.open_dataarray(path_results + 'aMLI_median.nc', chunks={'time': 1})
MLI_alpha_median = xr.open_dataarray(path_results + 'MLI_alpha_median.nc', chunks={'time': 1})
alpha            = xr.open_dataarray(path_results + 'alpha.nc', chunks={'time': 1}).compute()
aMLI_median_Stone      = xr.open_dataarray(path_results + 'aMLI_median_Stone.nc', chunks={'time': 1})
MLI_alpha_median_Stone = xr.open_dataarray(path_results + 'MLI_alpha_median_Stone.nc', chunks={'time': 1})
alpha_Stone            = xr.open_dataarray(path_results + 'alpha_Stone.nc', chunks={'time': 1}).compute()

#%%
print(f'alpha ALS: {alpha_Stone.mean(dim="time").data:2f}')
print(f'alpha PER: {alpha.mean(dim="time").data:2f}')
# %%
reload(fu)
fu.plot_tseries_diag_param_stone(wpbp_coar_median, aMLI_median, MLI_alpha_median, alpha, aMLI_median_Stone, MLI_alpha_median_Stone, alpha_Stone, fig_path, savefig)

# %%



def plot_tseries_diag_param_stone(wpbp_coar, aMLI, MLI_alpha, alpha, aMLI_Stone, aMLI_alpha_Stone, alpha_Stone, fig_path, savefig):
    fig, ax = plt.subplots(figsize=(7,4))
    fig.set_tight_layout(True)
    ax2 = ax.twinx()

    time = wpbp_coar.time
    ax.plot(time, wpbp_coar, c='k', label=r"$\overline{w^sb^s}^z$")

    ax.plot(time, aMLI, c='tab:blue', label=r'$C_{PER}(t) \times MLI_{PER}$')
    ax.plot(time, MLI_alpha, c='tab:blue', ls='--', label=r'$C_{PER} \times MLI_{PER}$')
    ax2.plot(time, alpha, c='tab:blue', ls=':', label=r'$C_{PER}(t)$')

    ax.plot(time, aMLI_Stone, c='tab:orange', label=r'$C_{ALS}(t) \times MLI_{ALS}$')
    ax.plot(time, aMLI_alpha_Stone, c='tab:orange', ls='--', label=r'$C_{ALS} \times MLI_{ALS}$')
    ax2.plot(time, alpha_Stone, c='tab:orange', ls=':', label=r'$C_{ALS}(t)$')

    ax.set_xlabel('time')
    ax.set_ylabel(r"$\overline{w^sb^s}^z$ [m$^2$ s$^{-3}$]", fontsize=9)

    ax.tick_params(axis='x', rotation=15)


    ax2.set_ylabel(r"$C(t)$", fontsize=12, c='k')
    ax2.spines['right'].set_color('k')
    ax2.tick_params(axis='y', colors='k')
    legend_1 = ax.legend(loc='lower left', fontsize=7)
    legend_1.remove()
    ax2.legend(loc='upper right', fontsize=7)
    ax2.add_artist(legend_1)
    ax.grid(which='major', linestyle='-', linewidth=0.8)
    ax.grid(which='minor', linestyle=':', linewidth=0.5)
    # Optionally, you can set the minor ticks to be more frequent
    ax = plt.gca()  # Get the current Axes instance
    ax.minorticks_on()  # Turn on the minor ticks
    ax.xaxis.set_minor_locator(plt.MultipleLocator(1))  # Set minor ticks 
    ax.yaxis.set_minor_locator(plt.MultipleLocator(1))  # Set minor ticks 
    ax.set_xlim([time[0], time[-1]])
    #an a in backets in the upper left corner
    ax.text(-0.06, 1.1, '(b)', transform=ax.transAxes, fontsize=12,   verticalalignment='top', zorder=100)

    if savefig==True: plt.savefig(f'{fig_path}wb_diag_param_tseries_2h_stone.png', dpi=150, bbox_inches='tight')


#%%
def plot_vb_wb_param(vb_vpbp_coar_median, vb_aMLI_median, vb_MLI_alpha_median, vb_alpha, vb_aMLI_median_Stone, vb_MLI_alpha_median_Stone, vb_alpha_Stone, wpbp_coar_median, aMLI_median, MLI_alpha_median, alpha, aMLI_median_Stone, MLI_alpha_median_Stone, alpha_Stone, fig_path, savefig):
    # hca, hcb = pyic.arrange_axes(1,2, asp=0.5, fig_size_fac=2)
    # ii=-1
    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    fig, axs = plt.subplots(2, 1, figsize=(7, 8))
    ax = axs[0]
    time = vb_vpbp_coar_median.time
    ax.plot(time, vb_vpbp_coar_median, c='k', label=r"$\overline{\hat{v}^sb^s}^z$")
    ax.plot(time, vb_aMLI_median, c='tab:blue', label=r'$C_{PER}(t) \times MLI_{PER}$')
    ax.plot(time, vb_MLI_alpha_median, c='tab:blue', ls='--', label=r'$C_{PER} \times MLI_{PER}$')
    ax2 = ax.twinx( )
    # ax2 = ax.secondary_yaxis('right')
    ax2.plot(time, vb_alpha, c='tab:blue', ls=':', label=r'$C_{PER}(t)$')
    ax.plot(time, vb_aMLI_median_Stone, c='tab:orange', label=r'$C_{ALS}(t) \times MLI_{ALS}$')
    ax.plot(time, vb_MLI_alpha_median_Stone, c='tab:orange', ls='--', label=r'$C_{ALS} \times MLI_{ALS}$')
    ax2.plot(time, vb_alpha_Stone, c='tab:orange', ls=':', label=r'$C_{ALS}(t)$')

    ax.set_ylabel(r"$\overline{\hat{v}^sb^s}^z$ [m$^2$ s$^{-3}$]", fontsize=9)
    ax2.set_ylabel(r"$C(t)$", fontsize=12, c='k')
    ax2.spines['right'].set_color('k')
    ax2.tick_params(axis='y', colors='k')
    legend_1 = ax.legend(loc='lower left', fontsize=7)
    legend_1.remove()
    ax2.legend(loc='upper right', fontsize=7)
    ax2.add_artist(legend_1)
    ax.set_xlim([time[0], time[-1]])
    ax.grid(which='major', linestyle='-', linewidth=0.8)
    ax.grid(which='minor', linestyle=':', linewidth=0.5)
    ax = plt.gca()  # Get the current Axes instance 
    ax.minorticks_on()  # Turn on the minor ticks
    ax.xaxis.set_minor_locator(plt.MultipleLocator(1))  # Set minor ticks
    # ax.yaxis.set_minor_locator(plt.MultipleLocator(1))  # Set minor ticks
    ax.set_xticks([])
    ax.text(-0.06, 1.1, '(a)', transform=ax.transAxes, fontsize=12,   verticalalignment='top', zorder=100)
    ax.legend(loc='upper left', fontsize=7)

    # ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax = axs[1]
    time = wpbp_coar_median.time
    ax.plot(time, wpbp_coar_median, c='k', label=r"$\overline{w^sb^s}^z$")
    ax.plot(time, aMLI_median, c='tab:blue', label=r'$C_{PER}(t) \times MLI_{PER}$')
    ax.plot(time, MLI_alpha_median, c='tab:blue', ls='--', label=r'$C_{PER} \times MLI_{PER}$')
    ax2 = ax.twinx()

    ax2.plot(time, alpha, c='tab:blue', ls=':', label=r'$C_{PER}(t)$')
    ax.plot(time, aMLI_median_Stone, c='tab:orange', label=r'$C_{ALS}(t) \times MLI_{ALS}$')
    ax.plot(time, MLI_alpha_median_Stone, c='tab:orange', ls='--', label=r'$C_{ALS} \times MLI_{ALS}$')
    ax2.plot(time, alpha_Stone, c='tab:orange', ls=':', label=r'$C_{ALS}(t)$')

    ax.set_ylabel(r"$\overline{w^sb^s}^z$ [m$^2$ s$^{-3}$]", fontsize=9)
    ax2.set_ylabel(r"$C(t)$", fontsize=12, c='k')
    ax2.spines['right'].set_color('k')
    ax2.tick_params(axis='y', colors='k')
    # ax.set_xlabel('time')
    ax.tick_params(axis='x', rotation=15)
    ax.set_xlim([time[0], time[-1]])
    legend_1 = ax.legend(loc='lower left', fontsize=7)
    legend_1.remove()
    ax2.legend(loc='upper right', fontsize=7)
    ax2.add_artist(legend_1)
    ax.grid(which='major', linestyle='-', linewidth=0.8)
    ax.grid(which='minor', linestyle=':', linewidth=0.5)
    ax = plt.gca()  # Get the current Axes instance
    ax.minorticks_on()  # Turn on the minor ticks
    ax.xaxis.set_minor_locator(plt.MultipleLocator(1))  # Set minor ticks
    ax.yaxis.set_minor_locator(plt.MultipleLocator(1))  # Set minor ticks
    ax.text(-0.06, 1.1, '(b)', transform=ax.transAxes, fontsize=12,   verticalalignment='top', zorder=100)
    ax.legend(loc='upper left', fontsize=7)
    if savefig==True: plt.savefig(f'{fig_path}wb_vb_param_time.png', dpi=150, bbox_inches='tight')
                

plot_vb_wb_param(vb_vpbp_coar_median, vb_aMLI_median, vb_MLI_alpha_median, vb_alpha, vb_aMLI_median_Stone, vb_MLI_alpha_median_Stone, vb_alpha_Stone, wpbp_coar_median, aMLI_median, MLI_alpha_median, alpha, aMLI_median_Stone, MLI_alpha_median_Stone, alpha_Stone, fig_path, savefig)
# %%
