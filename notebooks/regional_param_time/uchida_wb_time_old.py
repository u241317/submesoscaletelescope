# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '/home/m/m300878/submesoscaletelescope/notebooks/spectral_analysis/')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# %% Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
# from validate_catalog import all_params
# params_dict, cat = all_params()
# params_dict.keys()
import gcm_filters

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:25:00', wait=False)
cluster
client

# %%
domain        = 'uchida_domain'
grid_data     = False
t_space       = 1
savefig       = False
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
if domain=='uchida_domain':
    lat_reg          = 30,  40
    lon_reg          = -78, -68
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/uchida_domain/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/uchida_domain/'
elif domain=='our_domain':
    lon_reg          = np.array([-70, -54])
    lat_reg          = np.array([23.5, 36])
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/our_domain/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/our_domain/'

path_2_data      = f'{path_2_data_root}gridded/'
path_2_data_filt = f'{path_2_data_root}filtered/'
path_results     = f'{path_2_data_root}results/'

############################### Compute/Load ##############################
# %%
reload(eva)
mask_land_100 = eva.load_smt_land_mask(depthc=32)
mask_land_100 = pyic.interp_to_rectgrid_xr(mask_land_100, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land_100 = mask_land_100.drop('depthc')

# %%
if False:
    ro = eva.load_smt_vorticity()
    ro_intero = pyic.interp_to_rectgrid_xr(ro.vort_f_cells_50m.sel(time='2010-03-15T21:00:00.000000000'), fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    ro_intero.plot(vmin=-2, vmax=2, cmap='seismic')
    plt.savefig(fig_path + 'vorticity.png')
# %% b and lateral gradient
if grid_data == True:
    print('load and grid b and lateral gradient')
    ds   = eva.load_smt_b()
    # ds   = ds.drop('clon').drop('clat')
    ds   = ds.isel(time=slice(0, len(ds.time), t_space))
    b    = pyic.interp_to_rectgrid_xr(ds.b, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    b.to_netcdf(path_2_data + 'b.nc')
    dbdx = pyic.interp_to_rectgrid_xr(ds.dbdx, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    dbdx.to_netcdf(path_2_data + 'dbdx.nc')
    dbdy = pyic.interp_to_rectgrid_xr(ds.dbdy, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    dbdy.to_netcdf(path_2_data + 'dbdy.nc')
else:
    b    = xr.open_dataarray(path_2_data + 'b.nc', chunks={'time': 1, 'depthi': 1})
    dbdx = xr.open_dataarray(path_2_data + 'dbdx.nc', chunks={'time': 1, 'depthi': 1})
    dbdy = xr.open_dataarray(path_2_data + 'dbdy.nc', chunks={'time': 1, 'depthi': 1})
    b    = b.compute()
    dbdx = dbdx.compute()
    dbdy = dbdy.compute()
    
    b    = b * mask_land_100
    dbdx = dbdx * mask_land_100
    dbdy = dbdy * mask_land_100
    M2   = np.sqrt(dbdx[:-1]**2 + dbdy[:-1]**2)

    var    = 'b'
    path   = f'{path_2_data_filt}{var}_filt/'
    b_filt = xr.open_mfdataset(path + f'{var}_filt*', combine='nested'  , concat_dim='time', chunks={'time': 1, 'depthi': 1})
    b_filt = b_filt.b_filt.compute()
    b_filt = b_filt * mask_land_100


# %% N2
if grid_data == True:
    print('load and grid N2')
    ds = eva.load_smt_N2()
    ds = ds.drop('clon').drop('clat')
    ds = ds.isel(time=slice(0, len(ds.time), t_space))
    N2 = pyic.interp_to_rectgrid_xr(ds.N2, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    N2.to_netcdf(path_2_data + 'N2.nc')
else:
    N2 = xr.open_dataarray(path_2_data + 'N2.nc', chunks={'time': 1, 'depthi': 1})
    N2 = N2.compute()

    N2[:,0,:,:]  = np.nan
    N2[:,-1,:,:] = np.nan
    N2           = N2 * mask_land_100
    N2           = N2.where(~(N2 == 0), np.nan) # remove zeros


# %% w #TODO: check broken time step or exclude last time step
if grid_data == True:
    print('load and grid w')
    ds   = eva.load_smt_w()
    ds_w = ds.w.sel(time=b.time.data, method='nearest')
    ds_w = ds_w.drop('clon').drop('clat')
    w    = pyic.interp_to_rectgrid_xr(ds_w, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    w.to_netcdf(path_2_data + 'w.nc')
else:
    w = xr.open_dataarray(path_2_data + 'w.nc', chunks={'time': 1, 'depthi': 1})
    w = w.compute()
    w = w * mask_land_100
    w = w.where(~(w == 0), np.nan) # remove zeros
    w = w.where(np.abs(w) < 1e-2, np.nan) # remove extreme values

    var    = 'w'
    path   = f'{path_2_data_filt}{var}_filt/'
    w_filt = xr.open_mfdataset(path + f'{var}_filt*', combine='nested'  , concat_dim='time', chunks={'time': 1, 'depthi': 1})
    w_filt = w_filt.w_filt.compute()
    w_filt = w_filt * mask_land_100


# %% 
if grid_data == True:
    print('load and grid u and v')
    ds    = eva.load_smt_v()
    # begin = '2010-03-15T21:00:00.000000000'
    # end   = '2010-03-22T21:00:00.000000000'
    # # ds    = ds.sel(time=slice(begin, end))
    ds_u  = ds.u.sel(time=b.time.data, method='nearest')
    u     = pyic.interp_to_rectgrid_xr(ds_u, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    u.to_netcdf(path_2_data + 'u.nc')
    ds_v  = ds.v.sel(time=b.time.data, method='nearest')
    v     = pyic.interp_to_rectgrid_xr(ds_v, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
    v.to_netcdf(path_2_data + 'v.nc')
else:
    u = xr.open_dataarray(path_2_data + 'u.nc', chunks={'time': 1, 'depthi': 1})
    u = u.compute()
    v = xr.open_dataarray(path_2_data + 'v.nc', chunks={'time': 1, 'depthi': 1})
    v = v.compute()

#%%
if grid_data == True:
    print('gridding data finished')
    client.close()
    cluster.close()
    sys.exit(1)

# %% ################################# Filter Mask ##################################
reload(eva)
filter_30km, mask_land_100_new = eva.create_gcm_filter(b, 'irregular', mask_land_100)

# %%
b_sel = b[:-1]
w_sel = w[:-1]
w_filtered_to_30km = w_filt
b_filtered_to_30km = b_filt

b_diff = b_sel - b_filtered_to_30km 
w_diff = w_sel - w_filtered_to_30km
#%%
fu.plot_b_filter(b_sel.lon,b_sel.lat, b_sel.isel(depthi=5, time=1), b_filtered_to_30km.isel(depthi=5, time=1), b_diff.isel(depthi=5, time=1), fig_path, savefig)
# %%
reload(fu)
fu.plot_w_filter(w_sel.lon,w_sel.lat, w_sel.isel(depthi=5, time=1), w_filtered_to_30km.isel(depthi=5, time=1), w_diff.isel(depthi=5, time=1), fig_path, savefig)

# %%
if False:
    %%time
    b_sel              = b[:-1] * mask_land_100_new
    b_filtered_to_30km = filter_30km.apply(b_sel.isel(time=0), dims=['lat', 'lon'])
    # %%
    b_filtered_to_30km = b_filtered_to_30km * mask_land_100_new
    b_diff             = b_filtered_to_30km - b_sel.isel(time=0)

    # %%
    %%time
    w                  = w * mask_land_100_new
    w_sel              = w[:-1]
    w_filtered_to_30km = filter_30km.apply(w_sel, dims=['lat', 'lon'])
    w_filtered_to_30km = w_filtered_to_30km * mask_land_100_new
    w_diff             = w_filtered_to_30km - w_sel


    # %%
    x = b_diff.lon
    y = b_diff.lat


# %%  ################################### Compute MLD ################################## 
def compute_mld():
    if False:
        if False:
            time   = b.time
            ds_T   = eva.load_smt_T()
            t      = ds_T.sel(time=time.data, method='nearest')
            data_t = pyic.interp_to_rectgrid_xr(t, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
            data_t.to_netcdf(path_2_data + 'data_t.nc')
            ds_S   = eva.load_smt_S()
            s      = ds_S.sel(time=time.data, method='nearest')
            data_s = pyic.interp_to_rectgrid_xr(s, fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
            data_s.to_netcdf(path_2_data + 'data_s.nc')
        else:
            data_t = xr.open_dataarray(path_2_data + 'data_t.nc', chunks={'time': 1, 'depthc': 1})
            data_s = xr.open_dataarray(path_2_data + 'data_s.nc', chunks={'time': 1, 'depthc': 1})
        data_rho         = gsw.rho(data_s, data_t, depthc[2])
        data_rho_depthi_ = data_rho.interp(depthc=depthi)
        data_rho_depthi_ = data_rho_depthi_.rename(depthc='depthi')
        data_rho_depthi_.to_netcdf(path_2_data + 'data_rho_depthi_.nc')
    else:
        data_rho_depthi_ = xr.open_dataarray(path_2_data + 'data_rho_depthi_.nc', chunks={'time': 1, 'depthi': 1})
        data_rho_depthi_ = data_rho_depthi_.compute()


    # run below line parallel for each time step
    # mld, mask_mld, mldx = eva.calc_mld_xr(data_rho_depthi_, depthi, threshold=0.03)

    MLD_int  = []
    Mask_MLD = []
    MLD      = []
    for i in range(len(data_rho_depthi_.time)):
        mld_int, mask_mld, mld = eva.calc_mld_xr(data_rho_depthi_.isel(time=i), depthi, threshold=0.2)
        MLD_int.append(mld_int)
        Mask_MLD.append(mask_mld)
        MLD.append(mld)

    MLD_int  = xr.concat(MLD_int, dim='time')
    Mask_MLD = xr.concat(Mask_MLD, dim='time')
    MLD      = xr.concat(MLD, dim='time')

    if True:
        MLD = MLD * mask_land_100_new
        plt.figure()
        MLD.isel(time=7).plot(vmin=0, vmax=450, cmap='Spectral_r')
        if savefig==True: plt.savefig(fig_path + 'MLD.png', dpi=250)

    return MLD_int, Mask_MLD, MLD

print('use 0.2 threshold for MLD')
if False:
    MLD_int, Mask_MLD, MLD = compute_mld()
    MLD_int.to_netcdf(path_2_data + 'MLD_int.nc')
    Mask_MLD.to_netcdf(path_2_data + 'Mask_MLD.nc')
    MLD.to_netcdf(path_2_data + 'MLD.nc')
else:
    MLD      = xr.open_dataarray(path_2_data + 'MLD.nc', chunks={'time': 1})
    MLD      = MLD.compute()
    Mask_MLD = xr.open_dataarray(path_2_data + 'Mask_MLD.nc', chunks={'time': 1})
    Mask_MLD = Mask_MLD.compute()
    MLD      = MLD * mask_land_100_new
    MLD      = MLD[:-1]
    Mask_MLD = Mask_MLD[:-1]

#TODO Double check MLD diagnostic with porfiles


# %% ############################# Compute wb prime over MLD ############################
depthi_     = xr.DataArray(depthi, dims='depthi')
dz_diff     = depthi_.diff(dim='depthi')
dz_diff_ext = np.append(dz_diff, np.nan)
dz_diff_ext = xr.DataArray(dz_diff_ext, dims='depthi')
# TODO: repeat evaluation on cell centers
print('warning: dz_diff_ext is for values at interfaces, not cell centers')

wb_dat     = ((w_sel - w_filtered_to_30km) * (b_sel - b_filtered_to_30km)) * dz_diff_ext * Mask_MLD
wb_dat_sum = wb_dat.sum(dim='depthi', skipna=True)
wb_prime   = wb_dat_sum / MLD
wb_prime   = wb_prime * mask_land_100

# %%
reload(fu)
fu.plot_wb_prime(b.lon,b.lat,wb_prime.isel(time=10), fig_path, savefig)
# %%
#check mld
if False:
    plt.figure(figsize=(15,8))
    data = wb_dat_sel.isel( lon=400)
    plt.pcolormesh(data.lat, data.depthi, data, cmap='RdBu_r', vmin=-1e-6, vmax=1e-6)
    plt.plot(MLD.isel(time=0, lon=400).lat ,MLD.isel(time=0, lon=400), color='blue', label='0.03', lw=2)
    plt.plot(MLD2.isel(time=0, lon=400).lat ,MLD2.isel(time=0, lon=400), color='green', label='0.2', lw=2)
    plt.ylim(1150,0)
    plt.legend()
    plt.xlabel('lat')
    plt.ylabel('depth')
    if savefig==True: plt.savefig(fig_path + 'MLD_method_wb.png')
    #%%
    plt.figure(figsize=(15,8))
    data = N2.isel(time=0, lon=400)
    plt.pcolormesh(data.lat, data.depthi, data, cmap='RdBu_r', vmin=-3e-5, vmax=3e-5)
    plt.plot(MLD.isel(time=0, lon=400).lat ,MLD.isel(time=0, lon=400), color='blue', label='0.03', lw=2)
    plt.plot(MLD2.isel(time=0, lon=400).lat ,MLD2.isel(time=0, lon=400), color='green', label='0.2', lw=2)
    plt.ylim(1150,0)
    plt.legend()
    plt.xlabel('lat')
    plt.ylabel('depth')
    if savefig==True: plt.savefig(fig_path + 'MLD_method.png')
# %%  #################################### Coarsen Data ####################################
f = eva.calc_coriolis_parameter(b.lat)
ncoars          = 4
wb_prime_coars  = wb_prime.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
print('coarsening to', (wb_prime_coars.lat[1]-wb_prime_coars.lat[0]).data, 'degrees')
mask_land_coars = mask_land_100.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
mask_mld_coars  = Mask_MLD.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)

if True:
    M2_coars        = (M2* Mask_MLD).coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
    M2_mean_coars   = ((M2_coars*mask_mld_coars ) * dz_diff_ext).sum(dim='depthi', skipna=True) #/ MLD.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)  #not divided by MLD in Uchida TODO:WHY?
    f_coars         = eva.calc_coriolis_parameter(M2_coars.lat)
    MLI_coars       = (M2_mean_coars**2/f_coars)

    MLI_coars = MLI_coars.where(MLI_coars < 0.0001, np.nan)*mask_land_coars
    MLI_coars.isel(time=0).plot(vmin =0, vmax=4e-6, cmap='Reds')
else:
    M2_mean = (M2 * Mask_MLD).sum(dim='depthi', skipna=True) 
    MLI     = ((M2_mean**2) / f) * mask_land_100
    # M2_mean = (M2 * mask_mld * cell_area).sum(dim='depthi', skipna=True) / cell_area # scale with cell area is done but divions by sum of depths is not done
    #%
    MLI.isel(time=6).plot(vmin =0, vmax=4e-6, cmap='Reds')
    MLI_coars = MLI.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
    MLI_coars.isel(time=0).plot(vmin =0, vmax=4e-6, cmap='Reds')


# %% Compute Tuning Coefficient
Ce = wb_prime_coars / MLI_coars
if True:
    plt.figure()
    Ce.isel(time=28).plot(vmin=-1, vmax=1, cmap='RdBu_r')
    if savefig==True:  plt.savefig(fig_path + 'Ce.png')
alpha = Ce.median(['lat', 'lon'],skipna=True)
print(alpha.data)
aMLI = MLI_coars * alpha

#%%
reload(fu)
fu.plot_wb_prime_MLI_uchida(aMLI.isel(time=1), wb_prime_coars.isel(time=1), fig_path, savefig)
# %%
reload(fu)
wpbp_coar        = wb_prime_coars
time             = wb_prime_coars.time.data
wpbp_coar_median = wpbp_coar.median(['lat','lon'],skipna=True)
aMLI_median      = aMLI.median(['lat','lon'],skipna=True)
MLI_alpha_median = (alpha.mean('time')*MLI_coars).median(['lat','lon'],skipna=True)
#%%
if True:
    wpbp_coar_median.to_netcdf(path_results + 'wpbp_coar_median.nc')
    aMLI_median.to_netcdf(path_results + 'aMLI_median.nc')
    MLI_alpha_median.to_netcdf(path_results + 'MLI_alpha_median.nc')
    alpha.to_netcdf(path_results + 'alpha.nc')
else:
    wpbp_coar_median = xr.open_dataarray(path_results + 'wpbp_coar_median.nc', chunks={'time': 1})
    aMLI_median      = xr.open_dataarray(path_results + 'aMLI_median.nc', chunks={'time': 1})
    MLI_alpha_median = xr.open_dataarray(path_results + 'MLI_alpha_median.nc', chunks={'time': 1})
    alpha            = xr.open_dataarray(path_results + 'alpha.nc', chunks={'time': 1})

 # %%
reload(fu)
fu.plot_tseries_diag_param(wpbp_coar_median, aMLI_median, MLI_alpha_median, alpha, fig_path, savefig)

# %% ############################### MLI Stone ##############################
if True:
    N2_coars        = (N2 * Mask_MLD).coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
    Ri_coars        = N2_coars * f_coars**2 / M2_mean_coars**2
    Ri_mean_coars   = (Ri_coars*mask_mld_coars * dz_diff_ext).sum(dim='depthi', skipna=True)
    MLI_Stone_coars = 1/np.sqrt(1+Ri_mean_coars) * MLI_coars * mask_land_coars

else:
    f = eva.calc_coriolis_parameter(N2.lat)
    N2_mean   = (N2 * Mask_MLD).sum(dim='depthi', skipna=True)
    Ri        = N2_mean * f**2 / M2_mean**2 
    MLI_Stone = 1/np.sqrt(1+Ri) * ((M2_mean * M2_mean) / f) * mask_land_100
    # MLI_Stone.isel(time=6).plot(vmin =0, vmax=2e-7, cmap='viridis')

    MLI_Stone_coars = MLI_Stone.coarsen(lat=ncoars, lon=ncoars, boundary='trim').mean(skipna=True)
    MLI_Stone_coars.isel(time=0).plot(vmin =0, vmax=2e-7, cmap='Reds')

#%%
MLI_Stone_coars.isel(time=0).plot(vmin =0, vmax=4e-6, cmap='Reds')
if savefig==True: plt.savefig(fig_path + 'MLI_Stone_coars.png', dpi=250)
# %%    evaluate difference in tuning coeff
if False:
    # %
    a  = 1/np.sqrt(1+Ri) * mask_land_100_new
    A = a.isel(time=6).compute()
    plt.figure()
    A.plot(vmin =0.8, vmax=1.2, cmap='RdBu')
    plt.title('1/sqrt(1+Ri)')
    if savefig==True:plt.savefig(fig_path + 'diff_stone_fox-kemper.png')
    # %
    Ris = Ri.isel(time=6).compute() * mask_land_100_new *100
    plt.figure()
    Ris.plot(vmin =0, vmax=50, cmap='Blues_r')
    plt.title('Ri')
    if savefig==True:plt.savefig(fig_path + 'Ri.png')
    # %
    plt.figure()
    M2_means = M2_mean.isel(time=6).compute() * mask_land_100_new
    M2_means.plot(vmin=0, vmax=1e-5, cmap='viridis')
    # %
    plt.figure()
    N2_means = N2_mean.isel(time=6).compute() * mask_land_100_new
    N2_means.plot(vmin=1e-4, vmax=8e-4, cmap='plasma')
    plt.title('N2 MLD mean')
    if savefig==True:plt.savefig(fig_path + 'N2_mean.png')
# %%  #################################### Coarsen Data ####################################

# %% Compute Tuning Coefficient
Cs = wb_prime_coars / MLI_Stone_coars
if True:
    plt.figure()
    Cs.isel(time=1).plot(vmin=-1, vmax=1, cmap='RdBu_r')   
    plt.savefig(fig_path + 'Cs.png')
alpha_Stone = Cs.median(['lat', 'lon'],skipna=True)
aMLI_Stone  = MLI_Stone_coars * alpha_Stone
print(alpha_Stone.data)

#%%
#share colorbar
fig, ax = plt.subplots(1, 3, figsize=(18, 5), sharey=True, subplot_kw={'projection': ccrs.PlateCarree()})
Ce.isel(time=0).plot(ax=ax[0],vmin=-1, vmax=1, cmap='RdBu_r')
Cs.isel(time=0).plot(ax=ax[1], vmin=-1, vmax=1, cmap='RdBu_r')
(Ce-Cs).isel(time=0).plot(ax=ax[2], vmin=-1, vmax=1, cmap='RdBu_r')
ax[0].set_title('Ce')
ax[1].set_title('Cs')
ax[2].set_title('Ce-Cs')
if savefig==True: plt.savefig(fig_path + 'Ce_Cs.png',dpi=250)
# %%
# savefig = True
fu.plot_wb_prime_MLI_uchida(aMLI_Stone.isel(time=0), wb_prime_coars.isel(time=0), fig_path, savefig)

# %%
if True:
    aMLI_median_Stone      = aMLI_Stone.median(['lat','lon'],skipna=True)
    aMLI_median_Stone.to_netcdf(path_results + 'aMLI_median_Stone.nc')
    MLI_alpha_median_Stone = (alpha_Stone.mean('time')*MLI_Stone_coars).median(['lat','lon'],skipna=True)
    MLI_alpha_median_Stone.to_netcdf(path_results + 'MLI_alpha_median_Stone.nc')
    alpha_Stone.to_netcdf(path_results + 'alpha_Stone.nc')
else:
    aMLI_median_Stone      = xr.open_dataarray(path_results + 'aMLI_median_Stone.nc', chunks={'time': 1})
    MLI_alpha_median_Stone = xr.open_dataarray(path_results + 'MLI_alpha_median_Stone.nc', chunks={'time': 1})
    alpha_Stone            = xr.open_dataarray(path_results + 'alpha_Stone.nc', chunks={'time': 1})
# %%
reload(fu)
fu.plot_tseries_diag_param_stone(wpbp_coar_median, aMLI_median, MLI_alpha_median, alpha, aMLI_median_Stone, MLI_alpha_median_Stone, alpha_Stone, fig_path, savefig)

# %%
plt.figure()
plt.plot(Ce.median(['lat', 'lon'],skipna=True), color='tab:blue',   ls=':', label='PER Ce median')
plt.plot(Cs.median(['lat', 'lon'],skipna=True), color='tab:orange', ls=':', label='ALS Cs median')
plt.plot(Ce.mean(['lat', 'lon'],skipna=True), color='tab:blue',   label='PER Ce mean')
plt.plot(Cs.mean(['lat', 'lon'],skipna=True), color='tab:orange', label='ALS Cs mean')
plt.legend()
# plt.ylim(0,0.2)
if savefig==True: plt.savefig(fig_path + 'Ce_Cs_mean median.png')
# %%
