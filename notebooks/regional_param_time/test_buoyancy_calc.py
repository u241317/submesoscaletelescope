# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
sys.path.insert(0, '/home/m/m300878/submesoscaletelescope/notebooks/spectral_analysis/')
import funcs as fu

import string
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw


# %% get cluster
from importlib import reload
import smt_modules.init_slurm_cluster as scluster 
client, cluster = scluster.init_dask_slurm_cluster(walltime='00:15:00', wait=False)
cluster
client
# %%
ds_old = eva.load_smt_b_fast()
# %%
ds_classic = eva.load_smt_b()
# %%
reload(eva)
ds_interp_to_depthc = eva.load_smt_b_depthc()

# %%
# reload(eva)
ds_depthc_new = eva.load_smt_b_depthc_no_interp('b')


# %%
reload(eva)
ds_depthi_new = eva.load_smt_b_depthi_interp('b')

#%%
plt.figure(figsize=(5, 10))
plt.plot(ds_depthc_new.b.isel(ncells=1004328, time=10), depthc, marker='o', ls='-', label='b at depthc (correct depth)')
# plt.plot(di.b.isel(ncells=1004328), depthi, marker='x', ls=':', label='interpolated to depthi')
plt.plot(ds_depthi_new.b.isel(ncells=1004328, time=10), depthi, marker='x', ls=':', label='interpolated to depthi')
plt.legend()
plt.ylim(5000,0)

# %%
di = xr.open_dataset('/work/mh0033/m300878/smt/b/b_depthi/b_depthi_2010-03-15T21.nc')
# %
plt.figure(figsize=(5, 10))
plt.plot(ds_depthc_new.b.isel(ncells=1004328, time=0), depthc, marker='o', ls='-', label='b at depthc (correct depth)')
plt.plot(di.b.isel(ncells=1004328), depthi, marker='x', ls=':', label='interpolated to depthi')
plt.legend()
plt.ylim(5000,0)


# %%
ncells=1104328
plt.figure(figsize=(5, 15))
plt.plot(ds_old.b.isel(ncells=ncells,  time=40),    depthi,  marker='o', label='b at depthi (wrong depth)')
# plt.plot(ds_classic.b.isel(ncells=ncells, time=40), depthi,  marker='x', ls=':', label='load_classic')
plt.plot(ds_interp_to_depthc.b.isel(ncells=ncells, time=40), depthc, marker='x', ls=':', label='interpolated to depthc')
plt.plot(ds_depthc_new.b.isel(ncells=ncells, time=40), depthc, marker='o', ls='-.', label='b at depthc (correct depth)')
plt.ylim=(0,6000)
# %%
# above code with subfigure with ylim 0,500
ncells=2004328
fig, ax = plt.subplots(1, 3, figsize=(20, 15))
ax[0].plot(ds_old.b.isel(ncells=ncells,  time=40),    depthi,  marker='o', label='b at depthi (wrong depth)')
ax[0].plot(ds_interp_to_depthc.b.isel(ncells=ncells, time=40), depthc, marker='x', ls=':', label='interpolated to depthc')
ax[0].plot(ds_depthc_new.b.isel(ncells=ncells, time=40), depthc, marker='o', ls='-.', label='b at depthc (correct depth)')
ax[0].set_ylim(6000,0)
ax[0].set_ylabel('depth [m]')
ax[0].set_xlabel('b [m/s^2]')
ax[0].legend()

ax[1].plot(ds_old.b.isel(ncells=ncells,  time=40),    depthi,  marker='o', label='b at depthi (wrong depth)')
ax[1].plot(ds_interp_to_depthc.b.isel(ncells=ncells, time=40), depthc, marker='x', ls=':', label='interpolated to depthc')
ax[1].plot(ds_depthc_new.b.isel(ncells=ncells, time=40), depthc, marker='o', ls='-.', label='b at depthc (correct depth)')
ax[1].set_ylim(600,0)
ax[1].set_xlim(-0.05,-0.01)
ax[1].set_ylabel('depth [m]')
ax[1].set_xlabel('b [m/s^2]')

ax[2].plot(ds_old.b.isel(ncells=ncells,  time=40),    depthi,  marker='o', label='b at depthi (wrong depth)')
ax[2].plot(ds_interp_to_depthc.b.isel(ncells=ncells, time=40), depthc, marker='x', ls=':', label='interpolated to depthc')
ax[2].plot(ds_depthc_new.b.isel(ncells=ncells, time=40), depthc, marker='o', ls='-.', label='b at depthc (correct depth)')
ax[2].set_ylim(60,0)
ax[2].set_xlim(-0.016,-0.0125)
ax[2].set_ylabel('depth [m]')
ax[2].set_xlabel('b [m/s^2]')

plt.savefig('b_depth_calc_check.png')
# %%
lat_reg       = 30,  40
lon_reg       = -78, -68
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.10_180W-180E_90S-90N.nc'
b    = pyic.interp_to_rectgrid_xr(ds_depthc_new.isel(time=40, depthc=33).b, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land_100 = eva.load_smt_land_mask(depthc=35)
mask_land_100 = pyic.interp_to_rectgrid_xr(mask_land_100, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land_100 = mask_land_100.drop('depthc')
b = b *mask_land_100
b.plot()

# %%

# %%
