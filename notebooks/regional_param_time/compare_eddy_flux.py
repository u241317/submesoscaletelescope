#%%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import funcs1 as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# % Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
# from validate_catalog import all_params
# params_dict, cat = all_params()
# params_dict.keys()
import gcm_filters
print('Warning: use node with more than 256Gb RAM')
# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='01:25:00', wait=False)
cluster
client

# %%
domain        = 'our_domain'
t_space       = 1
savefig       = False
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
if domain=='uchida_domain':
    lat_reg          = 30,  40
    lon_reg          = -78, -68
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/uchida_domain/wb/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/uchida_domain/'
elif domain=='our_domain':
    lon_reg          = np.array([-70, -54])
    lat_reg          = np.array([23.5, 36])
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/our_domain/wb/comparison/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/our_domain/'

path_2_data      = f'{path_2_data_root}gridded/'
path_2_data_filt = f'{path_2_data_root}filtered/'
path_results     = f'{path_2_data_root}results/'


#%%
reload(eva)
mask_land_100 = eva.load_smt_land_mask(depthc=32)
mask_land_100 = pyic.interp_to_rectgrid_xr(mask_land_100, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land_100 = mask_land_100.drop('depthc')

depthi_     = xr.DataArray(depthi, dims='depthi')
dz_diff     = depthi_.diff(dim='depthi')
dz_diff_ext = np.append(dz_diff, np.nan)
dz_diff_ext = xr.DataArray(dz_diff_ext, dims='depthi')
#%% load dataset1
wb_prime_space = xr.open_dataarray(path_2_data + 'wb_dat.nc', chunks={'time': 1})
wb_prime_space = wb_prime_space.compute()
wb_prime_space_mean = wb_prime_space.mean(dim='time')


MLD           = xr.open_dataarray(path_2_data + 'MLD.nc', chunks={'time': 1})
MLD_int       = xr.open_dataarray(path_2_data + 'MLD_int.nc', chunks={'time': 1})
Mask_MLD      = xr.open_dataarray(path_2_data + 'Mask_MLD.nc', chunks={'time': 1})
MLD           = MLD.compute()
Mask_MLD      = Mask_MLD.compute()
MLD           = MLD * mask_land_100
MLD           = MLD[:-1]
Mask_MLD      = Mask_MLD[:-1]
Mask_MLD      = Mask_MLD.transpose('time', 'depthi', 'lat', 'lon')
MLD_mean      = MLD.mean(dim='time')
Mask_MLD_mean = Mask_MLD.mean(dim='time')

#%%

ds1_inst                = xr.Dataset({'wb': wb_prime_space, 'mld': MLD, 'mask_mld': Mask_MLD})
ds1                     = xr.Dataset({'wb': wb_prime_space_mean, 'mld': MLD_mean, 'mask_mld': Mask_MLD_mean})
ds1                     = ds1.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0], lat_reg[1]))
wb_mld_mean             = (ds1.wb * ds1.mask_mld * dz_diff_ext).sum(dim='depthi') / ds1.mld
ds1['wb_mld_mean']      = wb_mld_mean
wb_mld_mean_inst        = (ds1_inst.wb * ds1_inst.mask_mld * dz_diff_ext).sum(dim='depthi') / ds1_inst.mld
ds1_inst['wb_mld_mean'] = wb_mld_mean_inst

#%%
path_to_interp_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/'
wb_prime_time_mean  = xr.open_dataarray(f'{path_to_interp_data}/data_wb_prime_mean.nc')
mask_mld            = xr.open_dataarray(f'{path_to_interp_data}/mask_mld.nc')
mldx                = xr.open_dataarray(f'{path_to_interp_data}/mldx.nc')

#%%
ds2                = xr.Dataset({'wb': wb_prime_time_mean, 'mld': mldx, 'mask_mld': mask_mld})
ds2                = ds2.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0], lat_reg[1]))
wb_mld_mean        = (ds2.wb * ds2.mask_mld * dz_diff_ext).sum(dim='depthi') / ds2.mld
ds2['wb_mld_mean'] = wb_mld_mean

lon_reg = np.array([ds2.lon.min(), ds2.lon.max()])
lat_reg = np.array([ds2.lat.min(), ds2.lat.max()])
mask_land_sel = mask_land_100.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0], lat_reg[1]))
ds = ds2.wb_mld_mean
filter_30km, mask_land_100_new = eva.create_gcm_filter(ds, 'irregular', mask_land_sel)
ds_filtered_to_30km = filter_30km.apply(ds, dims=['lat', 'lon'])
ds2['wb_mld_mean_spatial_end'] = ds_filtered_to_30km * mask_land_sel
#%%
ds_filtered_to_30km_3d = filter_30km.apply(ds2.wb, dims=['lat', 'lon'])
wb_mld_mean_spatial = (ds_filtered_to_30km_3d * ds2.mask_mld * dz_diff_ext).sum(dim='depthi') / ds2.mld
ds2['wb_mld_mean_spatial'] = wb_mld_mean_spatial

#%%
#comparison
(ds2['wb_mld_mean_spatial']-ds2['wb_mld_mean_spatial_end']).plot()
#%%
#select lon_reg from ds2 in ds1
ds1 = ds1.sel(lon=ds2.lon, lat=ds2.lat)


# %%
reload(fu)
lon=-64
fu.plot_wb_prime_sec(ds1.sel(lon=lon, method='nearest'), ds2.sel(lon=lon, method='nearest'), fig_path, savefig)
fu.plot_wb_prime(ds1.isel(depthi=20), ds2.isel(depthi=20), lon_reg, lat_reg, fig_path, savefig)
fu.plot_wb_prime_mld_mean(ds1, ds2, lon_reg, lat_reg, fig_path, savefig)
fu.plot_wb_prime_sec_inst(ds1_inst.isel(time=40).sel( lon=lon, method='nearest'), fig_path, savefig)
fu.plot_wb_prime_inst(ds1_inst.isel(time=40, depthi=20), lon_reg, lat_reg, fig_path, savefig)
fu.plot_wb_prime_inst_ml_mean(ds1_inst.isel(time=40), lon_reg, lat_reg, fig_path, savefig)
# %%
reload(fu)
fu.plot_wb_prime_scatter(ds1.wb_mld_mean, ds2.wb_mld_mean_spatial, lon_reg, lat_reg, fig_path, savefig)

#%%
reload(fu)

fu.scatter_wb(ds1.wb_mld_mean, ds2.wb_mld_mean_spatial, fig_path, savefig)

#%%
if False:
    lon_reg = np.array([wb_prime_time_mean.lon.min(), wb_prime_time_mean.lon.max()])
    lat_reg = np.array([wb_prime_time_mean.lat.min(), wb_prime_time_mean.lat.max()])

    ds = wb_prime_space_mean.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0], lat_reg[1]))
    #%%
    ds.to_netcdf(f'{path_2_data}wb_prime_space_mean.nc')
    # %%


#%%



#%%

fig, ax= plt.subplots(figsize=(10, 10))
plt.grid()

ax.scatter(ds1.wb_mld_mean.data.flatten(), ds2.wb_mld_mean_spatial.data.flatten(), s=0.01)
#lolog log scale
# plt.xscale('log')
# plt.yscale('log')
# diagonal line
# plt.plot([1e-8, 1e-5], [1e-8, 1e-5], color='black', linestyle='--')
# diagonal with data lim
xlim = ax.get_xlim()
ylim = ax.get_ylim()
#true x y scale
plt.axis('equal')
plt.plot([xlim[0], xlim[1]], [xlim[0], xlim[1]], color='black', linestyle='--')
xlim= np.array([-1,1])*1e-7
ylim= np.array([-1,1])*1e-7
ax.set_xlim(xlim)
ax.set_ylim(ylim)
ax.set_xlabel('wb_prime_space_mean')
ax.set_ylabel('wb_prime_time_mean')
# %%


# %%
ds_filtered_to_30km.plot(vmin=-1.2e-7, vmax=1.2e-7, cmap='RdBu_r')
# %%
fig, ax= plt.subplots(figsize=(10, 10))
plt.grid()

ax.scatter(ds1.wb_mld_mean.data.flatten()[::10], ds_filtered_to_30km.data.flatten()[::10], s=1)
#lolog log scale
# plt.xscale('log')
# plt.yscale('log')
# diagonal line
# plt.plot([1e-8, 1e-5], [1e-8, 1e-5], color='black', linestyle='--')
# diagonal with data lim
xlim = ax.get_xlim()
ylim = ax.get_ylim()
#true x y scale
plt.axis('equal')
plt.plot([xlim[0], xlim[1]], [xlim[0], xlim[1]], color='black', linestyle='--')
xlim= np.array([-1,1])*2e-7
ylim= np.array([-1,1])*2e-7
ax.set_xlim(xlim)
ax.set_ylim(ylim)
ax.set_xlabel('wb_prime_space_mean')
ax.set_ylabel('wb_prime_time_mean')
# %%

hca, hcb = pyic.arrange_axes(1, 1, plot_cb=False, asp=1, fig_size_fac=2.6, axlab_kw=None)
ii=-1
ii+=1; ax=hca[ii]; cax=hcb[ii]

ax.scatter(ds1.wb_mld_mean.data.flatten()[::10], ds2.wb_mld_mean.data.flatten()[::10], s=1, alpha=0.25)

plt.xscale('log')
plt.yscale('log')
xlim = ax.get_xlim()
ylim = ax.get_ylim()

plt.axis('equal')
plt.plot([xlim[0], xlim[1]], [xlim[0], xlim[1]], color='black', lw=0.8)
xlim= np.array([1e-11,1e-6])
ylim= np.array([1e-11,1e-6])
ax.set_xlim(xlim)
ax.set_ylim(ylim)
plt.grid()

ax.set_xlabel(r'$\overline{<w^{\prime}b^{\prime}>}$'+f' spatial filter')
ax.set_ylabel(r'$<\overline{w^{\prime}b^{\prime}}>$'+f' temporal filter')

eva.add_string_like_pyic(hca, ['(c)'])

if savefig==True: plt.savefig(f'{fig_path}wb_log_comp.png', dpi=250, bbox_inches='tight')
# %%
