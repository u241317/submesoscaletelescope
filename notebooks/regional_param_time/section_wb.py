# %%
import sys
import glob, os
import pyicon as pyic
import smt_modules.all_funcs as eva
from smt_modules.icon_smt_levels import dzw, dzt, depthc, depthi
import smt_modules.tools as tools
import funcs1 as fu

import string
from matplotlib.ticker import FormatStrFormatter

import math
import pandas as pd
import netCDF4 as nc
import xarray as xr    
import numpy as np
import datetime          #https://docs.python.org/3/library/datetime.html

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d  #for converting cell to grid-centered coordinates # type: ignore
import cartopy
import cartopy.crs as ccrs
ccrs_proj = ccrs.PlateCarree()
import gsw

import scipy.signal as signal
from scipy import stats    #Used for 2D binned statistics # type: ignore
from importlib import reload
import matplotlib.patches as patches
import matplotlib.colors as colors
import smt_modules.init_slurm_cluster as scluster 
import dask

# %% Load Uchida data
from scipy.ndimage import rotate
# from xgcm.grid import Grid
import xrft
import s3fs
import matplotlib.colors as clr
import matplotlib.pyplot as plt
plt.rcParams['pcolor.shading'] = 'auto'
import intake
import os
import gcsfs
# add path
# from validate_catalog import all_params
# params_dict, cat = all_params()
# params_dict.keys()
import gcm_filters

# %% get cluster
reload(scluster)

client, cluster = scluster.init_dask_slurm_cluster(walltime='00:25:00', wait=False)
cluster
client
#%%
domain        = 'our_domain'
grid_data     = False
t_space       = 1
savefig       = False
fpath_ckdtree = '/work/mh0033/m300602/icon/grids/smt/ckdtree/rectgrids/smt_res0.02_180W-180E_90S-90N.nc'
if domain=='uchida_domain':
    lat_reg          = 30,  40
    lon_reg          = -78, -68
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/uchida_domain/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/uchida_domain/'
elif domain=='our_domain':
    lon_reg          = np.array([-70, -54])
    lat_reg          = np.array([23.5, 36])
    fig_path         = '/home/m/m300878/submesoscaletelescope/results/smt_natl/uchida_eval/our_domain/'
    path_2_data_root = '/work/mh0033/m300878/smt/uchida_tseries/our_domain/'

path_2_data      = f'{path_2_data_root}gridded/'
path_2_data_filt = f'{path_2_data_root}filtered/'
path_results     = f'{path_2_data_root}results/'


# %%
reload(eva)
mask_land_100 = eva.load_smt_land_mask(depthc=32)
mask_land_100 = pyic.interp_to_rectgrid_xr(mask_land_100, fpath_ckdtree=fpath_ckdtree, lon_reg=lon_reg, lat_reg=lat_reg)
mask_land_100 = mask_land_100.drop('depthc')

#%%
def load_mld(string):
    if string==0.03:
        MLD      = xr.open_dataarray(path_2_data + 'MLD_003.nc', chunks={'time': 1})
        Mask_MLD = xr.open_dataarray(path_2_data + 'Mask_MLD_003.nc', chunks={'time': 1})
    elif string==0.2:
        MLD      = xr.open_dataarray(path_2_data + 'MLD.nc', chunks={'time': 1})
        Mask_MLD = xr.open_dataarray(path_2_data + 'Mask_MLD.nc', chunks={'time': 1})

    Mask_MLD = Mask_MLD.compute()
    MLD      = MLD.compute()
    MLD      = MLD * mask_land_100
    MLD      = MLD[:-1]
    return MLD, Mask_MLD

mld_03, Mask_MLD_03 = load_mld(0.03)
mld_20, Mask_MLD_20 = load_mld(0.2)

#%%
tt = 0
if False:
    b    = xr.open_dataarray(path_2_data + 'b.nc', chunks={'time': 1, 'depthi': 1})
    b    = b.isel(time=tt)
    b    = b.compute()
    b    = b * mask_land_100

    var    = 'b'
    path   = f'{path_2_data_filt}{var}_filt/'
    b_filt = xr.open_mfdataset(path + f'{var}_filt*', combine='nested'  ,   concat_dim='time', chunks={'time': 1, 'depthi': 1})
    b_filt = b_filt.isel(time=tt)
    b_filt = b_filt.b_filt.compute()
    b_filt = b_filt * mask_land_100

    w = xr.open_dataarray(path_2_data + 'w.nc', chunks={'time': 1, 'depthi': 1})
    w = w.isel(time=tt)
    w = w.compute()
    w = w * mask_land_100
    w = w.where(~(w == 0), np.nan) # remove zeros
    w = w.where(np.abs(w) < 1e-2, np.nan) # remove extreme values

    var    = 'w'
    path   = f'{path_2_data_filt}{var}_filt/'
    w_filt = xr.open_mfdataset(path + f'{var}_filt*', combine='nested'  ,   concat_dim='time', chunks={'time': 1, 'depthi': 1})
    w_filt = w_filt.isel(time=tt)
    w_filt = w_filt.w_filt.compute()
    w_filt = w_filt * mask_land_100

    # TODO: repeat evaluation on cell centers
    print('warning: dz_diff_ext is for values at interfaces, not cell centers')

    b_sel = b[:-1]
    w_sel = w[:-1]
    w_filtered_to_30km = w_filt
    b_filtered_to_30km = b_filt

    wb_dat      = ((w_sel - w_filtered_to_30km) * (b_sel - b_filtered_to_30km)) 
else:
    wb_dat = xr.open_dataarray(f'{path_2_data}/wb_prime_space_mean.nc')
    wb_dat = wb_dat.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0],lat_reg[1]))

# %%
N2         = xr.open_dataarray(path_2_data + 'N2.nc', chunks={'time': 1, 'depthi': 1})
if True:
    N2         = N2.isel(time=tt)
    N2         = N2.compute()
    N2         = N2 * mask_land_100
    N2[0,:,:]  = np.nan 
    N2[-1,:,:] = np.nan
    N2         = N2.where(~(N2 == 0), np.nan) # remove zeros
else:  
    N2       = N2.isel(lon=400, lat=10)
    N2         = N2.compute()
    N2       = N2[:-1]
    N2[:,0]  = np.nan
    N2[:,-1] = np.nan

#%% load time average data
path_to_interp_data = '/work/mh0033/m300878/crop_interpolate/NA_002dgr/'
wb_prime_mean       = xr.open_dataarray(f'{path_to_interp_data}/data_wb_prime_mean.nc')
mld_20_mean         = xr.open_dataarray(f'{path_to_interp_data}/mldx.nc')
wb_prime_mean       = wb_prime_mean.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0],lat_reg[1]))
mld_20_mean         = mld_20_mean.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0],lat_reg[1]))
data_n2_mean        = xr.open_dataarray(f'{path_to_interp_data}/data_n2_mean.nc')
data_n2_mean        = data_n2_mean.sel(lon=slice(lon_reg[0], lon_reg[1]), lat=slice(lat_reg[0],lat_reg[1]))

# %% functions

hca, hcb = pyic.arrange_axes(1,2, plot_cb='bottom', asp=0.5, fig_size_fac=2, sharey=True, sharex=True)
ii=-1


ii+=1; ax=hca[ii]; cax=hcb[ii]
clim = 1e-7
pyic.shade(wb_prime_mean.lat.data, wb_prime_mean.depthi.data, wb_prime_mean.isel(lon=400).data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim)
ax.plot(mld_20_mean.lat.data, mld_20_mean.isel(lon=400).data, color='tab:green', label='MLD 0.20')
ax.set_title(r'mean $\overline{w^{\prime}b^{\prime}}$')
ax.legend()

ii+=1; ax=hca[ii]; cax=hcb[ii]
pyic.shade(wb_dat.lat.data, wb_dat.depthi.data, wb_dat.isel(lon=400).data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim)
ax.plot(mld_03.lat.data, mld_03.isel(time =tt,lon=400).data, label='MLD 0.03')
ax.plot(mld_20.lat.data, mld_20.isel(time =tt,lon=400).data, label='MLD 0.20', color='tab:green')
ax.set_title(r'snapshot $\overline{w^{\prime}b^{\prime}}$')
cax.set_title(r'$m/s^2$')
ax.legend()

for ax in hca:
    pyic.plot_settings(ax=ax)
    ax.xaxis.set_major_locator(plt.MaxNLocator(4))
    ax.yaxis.set_major_locator(plt.MaxNLocator(4))
    # ax.set_legend()
    ax.set_ylim(1200,0)

plt.savefig(f'{fig_path}wb_prime_mean_vs_snapshot.png', dpi=300)
# %%

hca, hcb = pyic.arrange_axes(1,2, plot_cb='bottom', asp=0.5, fig_size_fac=2, sharey=True, sharex=True)
ii=-1

ii+=1; ax=hca[ii]; cax=hcb[ii]
clim = 0,3e-5
cmap = 'Reds'
ax.set_title(r'mean $N2$')
pyic.shade(data_n2_mean.lat.data, data_n2_mean.depthi.data, data_n2_mean.isel(lon=400).data, ax=ax, cax=cax, cmap=cmap, clim=clim)
ax.plot(mld_20_mean.lat.data, mld_20_mean.isel(lon=400).data, color='tab:green', label='MLD 0.20')
ax.legend(loc='lower left')


ii+=1; ax=hca[ii]; cax=hcb[ii]
ax.set_title(r'snapshot $N2$')
pyic.shade(N2.lat.data, N2.depthi.data, N2.isel(lon=400).data, ax=ax, cax=cax, cmap=cmap, clim=clim)
ax.plot(mld_03.lat.data, mld_03.isel(time =tt,lon=400).data, label='MLD 0.03')
ax.plot(mld_20.lat.data, mld_20.isel(time =tt,lon=400).data, label='MLD 0.20', color='tab:green')
cax.set_title(r'$m/s^2$')
ax.legend()

for ax in hca:
    pyic.plot_settings(ax=ax)
    ax.xaxis.set_major_locator(plt.MaxNLocator(4))
    ax.yaxis.set_major_locator(plt.MaxNLocator(4))
    ax.set_ylim(500,0)

plt.savefig(f'{fig_path}n2_prime_mean_vs_snapshot_z.png', dpi=300)
# %%
# %%


hca, hcb = pyic.arrange_axes(1,1, plot_cb=True, asp=0.5, fig_size_fac=2, sharey=True, sharex=True)
ii=-1

clim = 0,2e-4
cmap = 'Reds'

ii+=1; ax=hca[ii]; cax=hcb[ii]
ax.set_title(rf'$N^2$  ')
hm2=pyic.shade(N2.time, N2.depthi, N2.transpose(), ax=ax, cax=cax, cmap=cmap, clim=clim)
ax.plot(mld_03.time, mld_03.isel(lat=10,lon=400), label=r'$\Delta \rho = 0.03$')
ax.plot(mld_20.time, mld_20.isel(lat=10,lon=400), label=r'$\Delta \rho = 0.20$', color='tab:green')
hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
hm2[1].formatter.set_useMathText(True)
cax.set_title(r'$m/s^2$', pad=10)

for ax in hca:
    pyic.plot_settings(ax=ax)
    ax.xaxis.set_major_locator(plt.MaxNLocator(4))
    ax.yaxis.set_major_locator(plt.MaxNLocator(4))
    ax.set_ylim(80,0)
    ax.legend(loc='lower left')
    ax.set_ylabel('depth [m]')

plt.savefig(f'{fig_path}wb_n2_time_mean_vs_snapshot_z2.png', dpi=300)
# %%
plt.pcolormesh(mld_03.time, mld_03.isel(lon=30).lat, mld_03.isel(lon=30).transpose(), label=r'$\Delta \rho = 0.03$')
plt.savefig(f'{fig_path}mld_03_lon30.png', dpi=100)
def compute_spectra_uchida(data, samp_freq=1):
    data['time'] = np.arange(int(data.time.size))*3600*samp_freq
    P = xrft.power_spectrum(data, dim=['time'], window='hann', detrend='linear', true_phase=True, true_amplitude=True)
    P = P.isel(freq_time=slice(len(P.freq_time)//2,None)) * 2
    return P

#%%
P = compute_spectra_uchida(mld_03.isel(lon=30).fillna(0), samp_freq=2)
# %%
plt.figure()
plt.loglog(P.freq_time*3600*24, P.mean('lat'))
plt.grid()
plt.xlabel('frequency [1/d]')
plt.ylabel('power')
plt.savefig(f'{fig_path}mld_03_lon30_spectra.png', dpi=100)
# %%

#%%
fu.plot_N2_wb(N2, wb_prime_mean, mld_03, mld_20, mld_20_mean, fig_path, tt, savefig)


#%%
ds = eva.load_smt_N2()
path = '/home/m/m300602/work/icon/grids/smt/smt_tgrid.nc'

rev           = f''
tgname        = f'smt_natl'
gname         = f'smt_tgrid'
path_tgrid    = f'/home/m/m300602/work/icon/grids/smt/' 
fname_tgrid   = f'{gname}.nc'
path_ckdtree  = f'/work/mh0033/m300878/grids/{tgname}/ckdtree/'
path_rgrid    = path_ckdtree + 'rectgrids/' 
path_sections = path_ckdtree + 'sections/' 
sname         = '62W_200pts'


dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[-62,lat_reg[0]], p2=[-62,lat_reg[1]], npoints=1600,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname = sname,
                      gname = gname,
                      tgname = tgname,
                      )

#%%apply
data  = ds.N2.isel(time=0)
ds_n2_sec = data[:,ickdtree]
ds_n2_sec = ds_n2_sec.compute()
ds_n2_sec[0,:] = np.nan
ds_n2_sec[-1,:] = np.nan

ds_n2_sec = ds_n2_sec.rename({'ncells': 'lat'})
# add lat coordinate to dataarray
ds_n2_sec['lat'] = xr.DataArray(lat_sec, dims='lat')

#%%
ds_n2_sec.plot(y='depthi', yincrease=False, vmin=0, vmax=3e-5, cmap='Reds')
plt.ylim(1200,0)
# %%

def plot_N2_wb_paper(N2, wb_prime_mean, wb_dat, mld_03, mld_20, mld_20_mean, fig_path, tt=0, savefig=False):
    hca, hcb = pyic.arrange_axes(1,3, plot_cb=True, asp=0.25, fig_size_fac=1.3, sharey=True, sharex=True, daxt=1)
    ii=-1

    clim = 0,3e-5
    cmap = 'Reds'

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    ax.set_title(rf'$N^2$ at {N2.time.values:.13} ')
    # hm2=pyic.shade(N2.lat.data, N2.depthi.data, N2.isel(lon=400).data, ax=ax, cax=cax, cmap=cmap, clim=clim)
    hm2=pyic.shade(N2.lat.data, N2.depthi.data, N2.data, ax=ax, cax=cax, cmap=cmap, clim=clim)

    ax.plot(mld_03.lat.data, mld_03.isel(time =tt,lon=400).data, label=r'$\Delta \rho = 0.03$')
    ax.plot(mld_20.lat.data, mld_20.isel(time =tt,lon=400).data, label=r'$\Delta \rho = 0.20$', color='tab:green')
    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)
    cax.set_title(r'$1/s^2$', pad=10)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1e-7
    hm2 = pyic.shade(wb_prime_mean.lat.data, wb_prime_mean.depthi.data, wb_prime_mean.isel(lon=400).data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim)
    ax.plot(mld_20_mean.lat.data, mld_20_mean.isel(lon=400).data, color='tab:green', label=r'$\Delta \rho = 0.20$')
    ax.set_title(r'$\overline{w^{\prime}b^{\prime}}$')
    cax.set_title(r'$m^2/s^3$', pad=10)

    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    ii+=1; ax=hca[ii]; cax=hcb[ii]
    clim = 1e-7
    hm2 = pyic.shade(wb_dat.lat.data, wb_dat.depthi.data, wb_dat.isel(lon=400).data, ax=ax, cax=cax, cmap='RdBu_r', clim=clim)
    ax.plot(mld_20_mean.lat.data, mld_20_mean.isel(lon=400).data, color='tab:green', label=r'$\Delta \rho = 0.20$')
    ax.set_title(r' $\overline{<w^{\prime}b^{\prime}>}$')
    cax.set_title(r'$m^2/s^3$', pad=10)
    # ax.set_xlabel('latitude')
    #set xticks like 30°E etc
    ax.set_xticks(np.arange(24,36,4))
    ax.set_xticklabels([f'{i}°N' for i in np.arange(24,36,4)])

    hm2[1].ax.yaxis.offsetText.set_position((2.1,0))
    hm2[1].formatter.set_useMathText(True)

    for ax in hca:
        pyic.plot_settings(ax=ax)
        ax.xaxis.set_major_locator(plt.MaxNLocator(4)) #type: ignore
        ax.yaxis.set_major_locator(plt.MaxNLocator(4)) #type: ignore
        ax.set_ylim(1200,0)
        ax.legend(loc='lower left')
        ax.set_ylabel('depth [m]')

    if savefig == True: plt.savefig(f'{fig_path}wb_n2_prime_mean_vs_snapshot{wb_dat.isel(lon=400).lon.values:.0f}_paper.png', dpi=300)

# %%
plot_N2_wb_paper(N2, wb_prime_mean, wb_dat, mld_03, mld_20, mld_20_mean, fig_path, tt, savefig)
# %%
plot_N2_wb_paper(ds_n2_sec, wb_prime_mean, wb_dat, mld_03, mld_20, mld_20_mean, fig_path, tt, savefig)

# %%
